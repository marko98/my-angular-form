import { ɵɵinject, ɵɵdefineInjectable, ɵsetClassMetadata, Injectable, ɵɵelementStart, ɵɵtext, ɵɵelementEnd, ɵɵnextContext, ɵɵadvance, ɵɵtextInterpolate, ɵɵproperty, ɵɵdefineComponent, ɵɵProvidersFeature, forwardRef, ɵɵtemplate, ɵɵlistener, Component, Input, ɵɵtextInterpolate1, ɵɵtextInterpolate3, ɵɵtextInterpolate2, ɵɵgetCurrentView, ɵɵrestoreView, ɵɵreference, EventEmitter, ɵɵdefineDirective, ɵɵstyleProp, Directive, Output, HostBinding, HostListener, ɵɵdirectiveInject, Renderer2, ɵɵviewQuery, ɵɵqueryRefresh, ɵɵloadQuery, ViewChild, ɵɵelement, ɵɵelementContainerStart, ɵɵelementContainerEnd, ɵɵpipe, ɵɵpureFunction1, ɵɵpipeBind1, ɵɵNgOnChangesFeature, ɵɵdefineNgModule, ɵɵdefineInjector, ɵɵsetNgModuleScope, NgModule } from '@angular/core';
import { DeviceDetectorService, DeviceDetectorModule } from 'ngx-device-detector';
import { NG_VALUE_ACCESSOR, NgControlStatus, NgModel, ɵangular_packages_forms_forms_y, NgControlStatusGroup, FormGroupDirective, FormControlName, FormsModule, ReactiveFormsModule, Validators, FormControl, FormGroup } from '@angular/forms';
import { DefaultLayoutDirective, DefaultLayoutAlignDirective, DefaultLayoutGapDirective, DefaultFlexDirective } from '@angular/flex-layout/flex';
import { NgIf, NgForOf, AsyncPipe, CommonModule } from '@angular/common';
import { MatRadioGroup, MatRadioButton, MatRadioModule } from '@angular/material/radio';
import { MatCheckbox, MatCheckboxModule } from '@angular/material/checkbox';
import { __extends, __spread } from 'tslib';
import { MatFormField, MatLabel, MatPrefix, MatSuffix, MatHint, MatError, MatFormFieldModule } from '@angular/material/form-field';
import { MatInput, MatInputModule } from '@angular/material/input';
import { MatIcon, MatIconModule } from '@angular/material/icon';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { MatButton, MatButtonModule } from '@angular/material/button';
import { MatSelectionList, MatListOption, MatListModule, MatList, MatListItem } from '@angular/material/list';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_LOCALE, MAT_DATE_FORMATS, MatOption, MatOptgroup, MatNativeDateModule, MatLine } from '@angular/material/core';
import { MatDatepickerInput, MatDatepicker, MatDatepickerToggle, MatDatepickerToggleIcon, MatDatepickerModule } from '@angular/material/datepicker';
import { NgxTimepickerFieldComponent, NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { MatSelect, MatSelectTrigger, MatSelectModule } from '@angular/material/select';
import { MatSlider, MatSliderModule } from '@angular/material/slider';
import { MatSlideToggle, MatSlideToggleModule } from '@angular/material/slide-toggle';
import { AutocompleteComponent as AutocompleteComponent$1, AutocompleteModule } from 'autocomplete';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatDialogModule, MatDialogRef, MatDialogTitle, MatDialogContent, MatDialogActions, MatDialogClose } from '@angular/material/dialog';
import { MatProgressBarModule, MatProgressBar } from '@angular/material/progress-bar';
import { HttpRequest, HttpEventType, HttpResponse, HttpClient, HttpClientModule } from '@angular/common/http';
import { Subject, forkJoin, BehaviorSubject } from 'rxjs';
import { isFunction } from 'util';

var DeviceService = /** @class */ (function () {
    function DeviceService(deviceService) {
        var _this = this;
        this.deviceService = deviceService;
        this.isDeviceMobile = function () {
            return _this.deviceService.isMobile();
        };
        this.isDeviceTablet = function () {
            return _this.deviceService.isTablet();
        };
        this.isDeviceDesktop = function () {
            return _this.deviceService.isDesktop();
        };
        this.getDeviceInfo = function () {
            return _this.deviceService.getDeviceInfo();
        };
    }
    DeviceService.ɵfac = function DeviceService_Factory(t) { return new (t || DeviceService)(ɵɵinject(DeviceDetectorService)); };
    DeviceService.ɵprov = ɵɵdefineInjectable({ token: DeviceService, factory: DeviceService.ɵfac, providedIn: 'root' });
    return DeviceService;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(DeviceService, [{
        type: Injectable,
        args: [{ providedIn: 'root' }]
    }], function () { return [{ type: DeviceDetectorService }]; }, null); })();

function RadioButtonComponent_label_1_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "label", 4);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r0 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r0.radioButtonGroup.getLabel().context);
} }
function RadioButtonComponent_mat_radio_button_3_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-radio-button", 5);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var option_r3 = ctx.$implicit;
    ɵɵproperty("value", option_r3.value);
    ɵɵadvance(1);
    ɵɵtextInterpolate(option_r3.name);
} }
function RadioButtonComponent_label_4_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "label", 4);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r2 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r2.radioButtonGroup.getLabel().context);
} }
// za vise o ControlValueAccessor, pogledaj:
// https://coryrylan.com/blog/angular-custom-form-controls-with-reactive-forms-and-ngmodel
var RadioButtonComponent = /** @class */ (function () {
    function RadioButtonComponent() {
        var _this = this;
        this.value = false;
        this.onChange = function () { };
        this.onTouched = function () { };
        this.onNgModelChange = function () {
            _this.radioButtonGroup.setDefaultValue(_this.value);
            _this.onChange(_this.value);
            _this.onTouched();
            // console.log(this.radioButtonGroup);
        };
    }
    RadioButtonComponent.prototype.registerOnChange = function (fn) {
        this.onChange = fn;
    };
    RadioButtonComponent.prototype.registerOnTouched = function (fn) {
        this.onTouched = fn;
    };
    RadioButtonComponent.prototype.writeValue = function (value) {
        if (value) {
            this.value = value;
        }
    };
    RadioButtonComponent.prototype.ngOnInit = function () {
        // console.log(this.radioButtonGroup);
        this.value = this.radioButtonGroup.getDefaultValue();
        // console.log('RadioButtonComponent init');
    };
    RadioButtonComponent.prototype.ngOnDestroy = function () {
        // console.log('RadioButtonComponent destroyed');
    };
    RadioButtonComponent.ɵfac = function RadioButtonComponent_Factory(t) { return new (t || RadioButtonComponent)(); };
    RadioButtonComponent.ɵcmp = ɵɵdefineComponent({ type: RadioButtonComponent, selectors: [["app-radio-button"]], inputs: { radioButtonGroup: "radioButtonGroup" }, features: [ɵɵProvidersFeature([
                {
                    provide: NG_VALUE_ACCESSOR,
                    multi: true,
                    useExisting: forwardRef(function () { return RadioButtonComponent; }),
                },
            ])], decls: 5, vars: 7, consts: [["fxLayout", "column", "fxLayoutAlign", "center center", "fxLayoutGap", "15px"], ["id", "radio-group-label", 4, "ngIf"], ["aria-labelledby", "radio-group-label", "fxLayoutAlign", "start start", "fxLayoutGap", "20px", 3, "ngModel", "disabled", "labelPosition", "fxLayout", "ngModelChange"], [3, "value", 4, "ngFor", "ngForOf"], ["id", "radio-group-label"], [3, "value"]], template: function RadioButtonComponent_Template(rf, ctx) { if (rf & 1) {
            ɵɵelementStart(0, "section", 0);
            ɵɵtemplate(1, RadioButtonComponent_label_1_Template, 2, 1, "label", 1);
            ɵɵelementStart(2, "mat-radio-group", 2);
            ɵɵlistener("ngModelChange", function RadioButtonComponent_Template_mat_radio_group_ngModelChange_2_listener($event) { return ctx.value = $event; })("ngModelChange", function RadioButtonComponent_Template_mat_radio_group_ngModelChange_2_listener() { return ctx.onNgModelChange(); });
            ɵɵtemplate(3, RadioButtonComponent_mat_radio_button_3_Template, 2, 2, "mat-radio-button", 3);
            ɵɵelementEnd();
            ɵɵtemplate(4, RadioButtonComponent_label_4_Template, 2, 1, "label", 1);
            ɵɵelementEnd();
        } if (rf & 2) {
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.radioButtonGroup.getLabel().position != "bellow");
            ɵɵadvance(1);
            ɵɵproperty("ngModel", ctx.value)("disabled", ctx.radioButtonGroup.getDisabled())("labelPosition", ctx.radioButtonGroup.getLabelPosition())("fxLayout", ctx.radioButtonGroup.getDirection());
            ɵɵadvance(1);
            ɵɵproperty("ngForOf", ctx.radioButtonGroup.getChildren());
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.radioButtonGroup.getLabel().position == "bellow");
        } }, directives: [DefaultLayoutDirective, DefaultLayoutAlignDirective, DefaultLayoutGapDirective, NgIf, MatRadioGroup, NgControlStatus, NgModel, NgForOf, MatRadioButton], styles: ["", ".no-margin[_ngcontent-%COMP%] {\n        margin: 0;\n    }"] });
    return RadioButtonComponent;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(RadioButtonComponent, [{
        type: Component,
        args: [{
                selector: 'app-radio-button',
                templateUrl: './radio-button.component.html',
                styleUrls: ['./radio-button.component.css'],
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        multi: true,
                        useExisting: forwardRef(function () { return RadioButtonComponent; }),
                    },
                ],
            }]
    }], function () { return []; }, { radioButtonGroup: [{
            type: Input
        }] }); })();

var CheckboxComponent = /** @class */ (function () {
    function CheckboxComponent() {
        var _this = this;
        this.onChange = function () { };
        this.onTouched = function () { };
        this.onClick = function () {
            _this.checkbox.setDefaultValue(!_this.checkbox.getDefaultValue());
            _this.onChange(_this.checkbox.getDefaultValue());
            _this.onTouched();
            // console.log(this.checkbox);
        };
    }
    CheckboxComponent.prototype.registerOnChange = function (fn) {
        this.onChange = fn;
    };
    CheckboxComponent.prototype.registerOnTouched = function (fn) {
        this.onTouched = fn;
    };
    CheckboxComponent.prototype.writeValue = function (value) {
        if (value) {
            this.checkbox.setDefaultValue(value);
        }
    };
    CheckboxComponent.prototype.ngOnInit = function () {
        // console.log(this.checkbox);
        // console.log('CheckboxComponent init');
    };
    CheckboxComponent.prototype.ngOnDestroy = function () {
        // console.log('CheckboxComponent destroyed');
    };
    CheckboxComponent.ɵfac = function CheckboxComponent_Factory(t) { return new (t || CheckboxComponent)(); };
    CheckboxComponent.ɵcmp = ɵɵdefineComponent({ type: CheckboxComponent, selectors: [["app-checkbox"]], inputs: { checkbox: "checkbox" }, features: [ɵɵProvidersFeature([
                {
                    provide: NG_VALUE_ACCESSOR,
                    multi: true,
                    useExisting: forwardRef(function () { return CheckboxComponent; }),
                },
            ])], decls: 3, vars: 5, consts: [["fxLayoutAlign", "center center"], [3, "value", "labelPosition", "disabled", "color", "click"]], template: function CheckboxComponent_Template(rf, ctx) { if (rf & 1) {
            ɵɵelementStart(0, "section", 0);
            ɵɵelementStart(1, "mat-checkbox", 1);
            ɵɵlistener("click", function CheckboxComponent_Template_mat_checkbox_click_1_listener() { return ctx.onClick(); });
            ɵɵtext(2);
            ɵɵelementEnd();
            ɵɵelementEnd();
        } if (rf & 2) {
            ɵɵadvance(1);
            ɵɵproperty("value", ctx.checkbox.getDefaultValue())("labelPosition", ctx.checkbox.getLabelPosition())("disabled", ctx.checkbox.getDisabled())("color", ctx.checkbox.getColor());
            ɵɵadvance(1);
            ɵɵtextInterpolate(ctx.checkbox.getName());
        } }, directives: [DefaultLayoutAlignDirective, MatCheckbox], styles: ["", ""] });
    return CheckboxComponent;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(CheckboxComponent, [{
        type: Component,
        args: [{
                selector: 'app-checkbox',
                templateUrl: './checkbox.component.html',
                styleUrls: ['./checkbox.component.css'],
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        multi: true,
                        useExisting: forwardRef(function () { return CheckboxComponent; }),
                    },
                ],
            }]
    }], function () { return []; }, { checkbox: [{
            type: Input
        }] }); })();

var Collection = /** @class */ (function () {
    function Collection() {
        var _this = this;
        this.children = [];
        this.parent = undefined;
        this.getChildren = function () {
            // vracamo kopiju
            // return this.children.slice();
            return _this.children;
        };
        this.getParent = function () {
            return _this.parent;
        };
        this.setParent = function (parent) {
            _this.parent = parent;
            return true;
        };
    }
    return Collection;
}());

var VALIDATOR_NAMES;
(function (VALIDATOR_NAMES) {
    VALIDATOR_NAMES["MIN_LENGTH"] = "minlength";
    VALIDATOR_NAMES["MAX_LENGTH"] = "maxlength";
    VALIDATOR_NAMES["REQUIRED"] = "required";
    VALIDATOR_NAMES["MIN"] = "min";
    VALIDATOR_NAMES["MAX"] = "max";
    VALIDATOR_NAMES["PATTERN"] = "pattern";
    VALIDATOR_NAMES["EMAIL"] = "email";
})(VALIDATOR_NAMES || (VALIDATOR_NAMES = {}));
var ROW_ITEM_TYPE;
(function (ROW_ITEM_TYPE) {
    ROW_ITEM_TYPE["CHECKBOX"] = "CHECKBOX";
    ROW_ITEM_TYPE["DATEPICKER"] = "DATEPICKER";
    ROW_ITEM_TYPE["TIMEPICKER"] = "TIMEPICKER";
    // FORM_FIELD_INPUT, // text, number, file, email, password
    ROW_ITEM_TYPE["FORM_FIELD_INPUT"] = "FORM_FIELD_INPUT";
    ROW_ITEM_TYPE["FORM_FIELD_SELECT"] = "FORM_FIELD_SELECT";
    ROW_ITEM_TYPE["FORM_FIELD_TEXTAREA"] = "FORM_FIELD_TEXTAREA";
    ROW_ITEM_TYPE["RADIO_BUTTON_GROUP"] = "RADIO_BUTTON_GROUP";
    ROW_ITEM_TYPE["SLIDER"] = "SLIDER";
    ROW_ITEM_TYPE["SLIDE_TOGGLE"] = "SLIDE_TOGGLE";
    ROW_ITEM_TYPE["BUTTON"] = "BUTTON";
    ROW_ITEM_TYPE["BUTTON_TOGGLE"] = "BUTTON_TOGGLE";
    ROW_ITEM_TYPE["ICON"] = "ICON";
    ROW_ITEM_TYPE["AUTOCOMPLETE"] = "AUTOCOMPLETE";
    // treba dodati input sa autocomplete-om i da kako se jedan input popuni drugom se na osnovu toga odredjue autocomplete(drzava - grad, univerzitet - faks)
})(ROW_ITEM_TYPE || (ROW_ITEM_TYPE = {}));
var FormRowItem = /** @class */ (function (_super) {
    __extends(FormRowItem, _super);
    function FormRowItem() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.validators = [];
        // private static controlNames: string[] = [];
        _this.disabled = false;
        _this.getType = function () {
            return _this.type;
        };
        _this.setType = function (type) {
            _this.type = type;
        };
        _this.getControlName = function () {
            return _this.controlName;
        };
        _this.setControlName = function (controlName) {
            _this.controlName = controlName;
            // if (FormRowItem.controlNames.find((cN) => cN === controlName)) {
            //   throw Error('Control name must be unique');
            // } else {
            //   FormRowItem.controlNames.push(controlName);
            //   this.controlName = controlName;
            // }
        };
        _this.getDefaultValue = function () {
            return _this.defaultValue;
        };
        _this.setDefaultValue = function (defaultValue) {
            _this.defaultValue = defaultValue;
        };
        _this.getValidators = function () {
            // console.log(this.validators);
            return _this.validators;
        };
        _this.addValidator = function (validator) {
            _this.validators.push(validator);
        };
        _this.getValidatorFns = function () {
            var validatorFns = [];
            _this.validators.forEach(function (validator) {
                validatorFns.push(validator.validatorFn);
            });
            // console.log(validatorFns);
            return validatorFns;
        };
        _this.setValidators = function (validators) {
            _this.validators = validators;
        };
        _this.getDisabled = function () {
            return _this.disabled;
        };
        _this.setDisabled = function (disabled) {
            _this.disabled = disabled;
        };
        return _this;
    }
    FormRowItem.prototype.addChild = function (child) {
        return true;
    };
    FormRowItem.prototype.removeChild = function (child) {
        return true;
    };
    FormRowItem.prototype.shouldHaveChildren = function () {
        return false;
    };
    return FormRowItem;
}(Collection));

function FormFieldInputTextComponent_mat_label_2_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-label");
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r0 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r0.formFieldInputText.getLabelName());
} }
function FormFieldInputTextComponent_mat_icon_5_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-icon", 9);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r2 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r2.formFieldInputText.getMatPrefixImgText());
} }
function FormFieldInputTextComponent_mat_icon_6_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-icon", 10);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r3 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r3.formFieldInputText.getMatSuffixImgText());
} }
function FormFieldInputTextComponent_span_7_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "span", 9);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r4 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate1("", ctx_r4.formFieldInputText.getTextPrefix(), "\u00A0");
} }
function FormFieldInputTextComponent_span_8_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "span", 10);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r5 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r5.formFieldInputText.getTextSuffix());
} }
function FormFieldInputTextComponent_mat_hint_9_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-hint", 11);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r6 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r6.formFieldInputText.getLeftHintLabel());
} }
function FormFieldInputTextComponent_mat_hint_10_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-hint", 12);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r7 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r7.formFieldInputText.getRightHintLabel());
} }
function FormFieldInputTextComponent_mat_hint_11_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-hint", 12);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r8 = ɵɵnextContext();
    var tmp_0_0 = null;
    ɵɵadvance(1);
    ɵɵtextInterpolate3("", ((tmp_0_0 = ctx_r8.getMinLengthValidator()) == null ? null : tmp_0_0.length) ? ctx_r8.getMinLengthValidator().length + " / " : "", "", (tmp_0_0 = ctx_r8.formFieldInputText.getDefaultValue()) == null ? null : tmp_0_0.length, "", ((tmp_0_0 = ctx_r8.getMaxLengthValidator()) == null ? null : tmp_0_0.length) ? " / " + ctx_r8.getMaxLengthValidator().length : "", "");
} }
function FormFieldInputTextComponent_mat_hint_12_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-hint", 12);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r9 = ɵɵnextContext();
    var tmp_0_0 = null;
    ɵɵadvance(1);
    ɵɵtextInterpolate2("", (tmp_0_0 = ctx_r9.formFieldInputText.getDefaultValue()) == null ? null : tmp_0_0.length, "", ((tmp_0_0 = ctx_r9.getMinLengthValidator()) == null ? null : tmp_0_0.length) ? " / " + ctx_r9.getMinLengthValidator().length : "", "");
} }
function FormFieldInputTextComponent_mat_hint_13_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-hint", 12);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r10 = ɵɵnextContext();
    var tmp_0_0 = null;
    ɵɵadvance(1);
    ɵɵtextInterpolate2("", (tmp_0_0 = ctx_r10.formFieldInputText.getDefaultValue()) == null ? null : tmp_0_0.length, "", ((tmp_0_0 = ctx_r10.getMaxLengthValidator()) == null ? null : tmp_0_0.length) ? " / " + ctx_r10.getMaxLengthValidator().length : "", "");
} }
var FormFieldInputTextComponent = /** @class */ (function () {
    function FormFieldInputTextComponent() {
        var _this = this;
        this.onChange = function () { };
        this.onTouched = function () { };
        this.onKeyUp = function (input) {
            // console.log(input.value);
            _this.formFieldInputText.setDefaultValue(input.value);
            _this.onChange(_this.formFieldInputText.getDefaultValue());
            _this.onTouched();
        };
        this.getMinLengthValidator = function () {
            var theValidator;
            _this.formFieldInputText
                .getValidators()
                .forEach(function (validator) {
                if (validator.name === VALIDATOR_NAMES.MIN_LENGTH)
                    theValidator = validator;
            });
            return theValidator;
        };
        this.getMaxLengthValidator = function () {
            var theValidator;
            _this.formFieldInputText
                .getValidators()
                .forEach(function (validator) {
                if (validator.name === VALIDATOR_NAMES.MAX_LENGTH)
                    theValidator = validator;
            });
            return theValidator;
        };
    }
    FormFieldInputTextComponent.prototype.registerOnChange = function (fn) {
        this.onChange = fn;
    };
    FormFieldInputTextComponent.prototype.registerOnTouched = function (fn) {
        this.onTouched = fn;
    };
    FormFieldInputTextComponent.prototype.writeValue = function (value) {
        if (value) {
            this.formFieldInputText.setDefaultValue(value);
        }
    };
    FormFieldInputTextComponent.prototype.ngOnInit = function () {
        // console.log('FormFieldInputTextComponent init');
    };
    FormFieldInputTextComponent.prototype.ngOnDestroy = function () {
        // console.log('FormFieldInputTextComponent destroyed');
    };
    FormFieldInputTextComponent.ɵfac = function FormFieldInputTextComponent_Factory(t) { return new (t || FormFieldInputTextComponent)(); };
    FormFieldInputTextComponent.ɵcmp = ɵɵdefineComponent({ type: FormFieldInputTextComponent, selectors: [["app-form-field-input-text"]], inputs: { formFieldInputText: "formFieldInputText" }, features: [ɵɵProvidersFeature([
                {
                    provide: NG_VALUE_ACCESSOR,
                    multi: true,
                    useExisting: forwardRef(function () { return FormFieldInputTextComponent; }),
                },
            ])], decls: 14, vars: 18, consts: [["fxLayoutAlign", "center center"], [3, "appearance", "hideRequiredMarker"], [4, "ngIf"], ["matInput", "", 3, "type", "placeholder", "value", "disabled", "readonly", "required", "keyup", "blur"], ["input", ""], ["matPrefix", "", 4, "ngIf"], ["matSuffix", "", 4, "ngIf"], ["align", "start", 4, "ngIf"], ["align", "end", 4, "ngIf"], ["matPrefix", ""], ["matSuffix", ""], ["align", "start"], ["align", "end"]], template: function FormFieldInputTextComponent_Template(rf, ctx) { if (rf & 1) {
            var _r11 = ɵɵgetCurrentView();
            ɵɵelementStart(0, "section", 0);
            ɵɵelementStart(1, "mat-form-field", 1);
            ɵɵtemplate(2, FormFieldInputTextComponent_mat_label_2_Template, 2, 1, "mat-label", 2);
            ɵɵelementStart(3, "input", 3, 4);
            ɵɵlistener("keyup", function FormFieldInputTextComponent_Template_input_keyup_3_listener() { ɵɵrestoreView(_r11); var _r1 = ɵɵreference(4); return ctx.onKeyUp(_r1); })("blur", function FormFieldInputTextComponent_Template_input_blur_3_listener() { ɵɵrestoreView(_r11); var _r1 = ɵɵreference(4); return ctx.onKeyUp(_r1); });
            ɵɵelementEnd();
            ɵɵtemplate(5, FormFieldInputTextComponent_mat_icon_5_Template, 2, 1, "mat-icon", 5);
            ɵɵtemplate(6, FormFieldInputTextComponent_mat_icon_6_Template, 2, 1, "mat-icon", 6);
            ɵɵtemplate(7, FormFieldInputTextComponent_span_7_Template, 2, 1, "span", 5);
            ɵɵtemplate(8, FormFieldInputTextComponent_span_8_Template, 2, 1, "span", 6);
            ɵɵtemplate(9, FormFieldInputTextComponent_mat_hint_9_Template, 2, 1, "mat-hint", 7);
            ɵɵtemplate(10, FormFieldInputTextComponent_mat_hint_10_Template, 2, 1, "mat-hint", 8);
            ɵɵtemplate(11, FormFieldInputTextComponent_mat_hint_11_Template, 2, 3, "mat-hint", 8);
            ɵɵtemplate(12, FormFieldInputTextComponent_mat_hint_12_Template, 2, 2, "mat-hint", 8);
            ɵɵtemplate(13, FormFieldInputTextComponent_mat_hint_13_Template, 2, 2, "mat-hint", 8);
            ɵɵelementEnd();
            ɵɵelementEnd();
        } if (rf & 2) {
            ɵɵadvance(1);
            ɵɵproperty("appearance", ctx.formFieldInputText.getAppearance())("hideRequiredMarker", ctx.formFieldInputText.getRequired().hideRequiredMarker);
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.formFieldInputText.getLabelName());
            ɵɵadvance(1);
            ɵɵproperty("type", ctx.formFieldInputText.getFormFieldInputType())("placeholder", ctx.formFieldInputText.getPlaceholder() ? ctx.formFieldInputText.getPlaceholder() : "")("value", ctx.formFieldInputText.getDefaultValue())("disabled", ctx.formFieldInputText.getDisabled())("readonly", ctx.formFieldInputText.getReadonly())("required", ctx.formFieldInputText.getRequired().required);
            ɵɵadvance(2);
            ɵɵproperty("ngIf", ctx.formFieldInputText.getMatPrefixImgText());
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.formFieldInputText.getMatSuffixImgText());
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.formFieldInputText.getTextPrefix());
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.formFieldInputText.getTextSuffix());
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.formFieldInputText.getLeftHintLabel());
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.formFieldInputText.getRightHintLabel());
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.formFieldInputText.getShowHintAboutMinMaxLength() && ctx.getMinLengthValidator() && ctx.getMaxLengthValidator());
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.formFieldInputText.getShowHintAboutMinMaxLength() && ctx.getMinLengthValidator() && !ctx.getMaxLengthValidator());
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.formFieldInputText.getShowHintAboutMinMaxLength() && !ctx.getMinLengthValidator() && ctx.getMaxLengthValidator());
        } }, directives: [DefaultLayoutAlignDirective, MatFormField, NgIf, MatInput, MatLabel, MatIcon, MatPrefix, MatSuffix, MatHint], styles: ["", ".cursor[_ngcontent-%COMP%] {\n        cursor: pointer;\n    }\n\n    input[type=\"file\"][_ngcontent-%COMP%] {\n        display: none;\n    }\n\n    h3[_ngcontent-%COMP%] {\n        font-size: inherit;\n        margin: 0;\n        font-weight: inherit;\n    }\n\n    mat-form-field[_ngcontent-%COMP%] {\n        min-width: 250px;\n    }"] });
    return FormFieldInputTextComponent;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(FormFieldInputTextComponent, [{
        type: Component,
        args: [{
                selector: "app-form-field-input-text",
                templateUrl: "./form-field-input-text.component.html",
                styleUrls: ["./form-field-input-text.component.css"],
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        multi: true,
                        useExisting: forwardRef(function () { return FormFieldInputTextComponent; }),
                    },
                ],
            }]
    }], function () { return []; }, { formFieldInputText: [{
            type: Input
        }] }); })();

function FormFieldInputPasswordComponent_mat_label_2_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-label");
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r0 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r0.formFieldInputPassword.getLabelName());
} }
function FormFieldInputPasswordComponent_mat_icon_5_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-icon", 10);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r2 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r2.formFieldInputPassword.getMatPrefixImgText());
} }
function FormFieldInputPasswordComponent_mat_icon_6_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-icon", 11);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r3 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r3.formFieldInputPassword.getMatSuffixImgText());
} }
function FormFieldInputPasswordComponent_mat_icon_7_Template(rf, ctx) { if (rf & 1) {
    var _r13 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "mat-icon", 12);
    ɵɵlistener("click", function FormFieldInputPasswordComponent_mat_icon_7_Template_mat_icon_click_0_listener() { ɵɵrestoreView(_r13); var ctx_r12 = ɵɵnextContext(); return ctx_r12.formFieldInputPassword.onShowPassword(); });
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r4 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", ctx_r4.formFieldInputPassword.getShowPassword() ? "visibility" : "visibility_off", " ");
} }
function FormFieldInputPasswordComponent_span_8_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "span", 10);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r5 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate1("", ctx_r5.formFieldInputPassword.getTextPrefix(), "\u00A0");
} }
function FormFieldInputPasswordComponent_span_9_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "span", 11);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r6 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r6.formFieldInputPassword.getTextSuffix());
} }
function FormFieldInputPasswordComponent_mat_hint_10_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-hint", 13);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r7 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r7.formFieldInputPassword.getLeftHintLabel());
} }
function FormFieldInputPasswordComponent_mat_hint_11_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-hint", 14);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r8 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r8.formFieldInputPassword.getRightHintLabel());
} }
function FormFieldInputPasswordComponent_mat_hint_12_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-hint", 14);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r9 = ɵɵnextContext();
    var tmp_0_0 = null;
    ɵɵadvance(1);
    ɵɵtextInterpolate3("", ((tmp_0_0 = ctx_r9.getMinLengthValidator()) == null ? null : tmp_0_0.length) ? ctx_r9.getMinLengthValidator().length + " / " : "", "", (tmp_0_0 = ctx_r9.formFieldInputPassword.getDefaultValue()) == null ? null : tmp_0_0.length, "", ((tmp_0_0 = ctx_r9.getMaxLengthValidator()) == null ? null : tmp_0_0.length) ? " / " + ctx_r9.getMaxLengthValidator().length : "", "");
} }
function FormFieldInputPasswordComponent_mat_hint_13_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-hint", 14);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r10 = ɵɵnextContext();
    var tmp_0_0 = null;
    ɵɵadvance(1);
    ɵɵtextInterpolate2("", (tmp_0_0 = ctx_r10.formFieldInputPassword.getDefaultValue()) == null ? null : tmp_0_0.length, "", ((tmp_0_0 = ctx_r10.getMinLengthValidator()) == null ? null : tmp_0_0.length) ? " / " + ctx_r10.getMinLengthValidator().length : "", "");
} }
function FormFieldInputPasswordComponent_mat_hint_14_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-hint", 14);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r11 = ɵɵnextContext();
    var tmp_0_0 = null;
    ɵɵadvance(1);
    ɵɵtextInterpolate2("", (tmp_0_0 = ctx_r11.formFieldInputPassword.getDefaultValue()) == null ? null : tmp_0_0.length, "", ((tmp_0_0 = ctx_r11.getMaxLengthValidator()) == null ? null : tmp_0_0.length) ? " / " + ctx_r11.getMaxLengthValidator().length : "", "");
} }
var FormFieldInputPasswordComponent = /** @class */ (function () {
    function FormFieldInputPasswordComponent() {
        var _this = this;
        this.onChange = function () { };
        this.onTouched = function () { };
        this.onKeyUp = function (input) {
            _this.formFieldInputPassword.setDefaultValue(input.value);
            _this.onChange(_this.formFieldInputPassword.getDefaultValue());
            _this.onTouched();
        };
        this.getMinLengthValidator = function () {
            var theValidator;
            _this.formFieldInputPassword
                .getValidators()
                .forEach(function (validator) {
                if (validator.name === VALIDATOR_NAMES.MIN_LENGTH)
                    theValidator = validator;
            });
            return theValidator;
        };
        this.getMaxLengthValidator = function () {
            var theValidator;
            _this.formFieldInputPassword
                .getValidators()
                .forEach(function (validator) {
                if (validator.name === VALIDATOR_NAMES.MAX_LENGTH)
                    theValidator = validator;
            });
            return theValidator;
        };
    }
    FormFieldInputPasswordComponent.prototype.registerOnChange = function (fn) {
        this.onChange = fn;
    };
    FormFieldInputPasswordComponent.prototype.registerOnTouched = function (fn) {
        this.onTouched = fn;
    };
    FormFieldInputPasswordComponent.prototype.writeValue = function (value) {
        if (value) {
            this.formFieldInputPassword.setDefaultValue(value);
        }
    };
    FormFieldInputPasswordComponent.prototype.ngOnInit = function () {
        // console.log('FormFieldInputPasswordComponent init');
    };
    FormFieldInputPasswordComponent.prototype.ngOnDestroy = function () {
        // console.log('FormFieldInputPasswordComponent destroyed');
    };
    FormFieldInputPasswordComponent.ɵfac = function FormFieldInputPasswordComponent_Factory(t) { return new (t || FormFieldInputPasswordComponent)(); };
    FormFieldInputPasswordComponent.ɵcmp = ɵɵdefineComponent({ type: FormFieldInputPasswordComponent, selectors: [["app-form-field-input-password"]], inputs: { formFieldInputPassword: "formFieldInputPassword" }, features: [ɵɵProvidersFeature([
                {
                    provide: NG_VALUE_ACCESSOR,
                    multi: true,
                    useExisting: forwardRef(function () { return FormFieldInputPasswordComponent; }),
                },
            ])], decls: 15, vars: 19, consts: [["fxLayoutAlign", "center center"], [3, "appearance", "hideRequiredMarker"], [4, "ngIf"], ["matInput", "", 3, "type", "placeholder", "value", "disabled", "readonly", "required", "keyup", "blur"], ["input", ""], ["matPrefix", "", 4, "ngIf"], ["matSuffix", "", 4, "ngIf"], ["class", "cursor", "matSuffix", "", 3, "click", 4, "ngIf"], ["align", "start", 4, "ngIf"], ["align", "end", 4, "ngIf"], ["matPrefix", ""], ["matSuffix", ""], ["matSuffix", "", 1, "cursor", 3, "click"], ["align", "start"], ["align", "end"]], template: function FormFieldInputPasswordComponent_Template(rf, ctx) { if (rf & 1) {
            var _r14 = ɵɵgetCurrentView();
            ɵɵelementStart(0, "section", 0);
            ɵɵelementStart(1, "mat-form-field", 1);
            ɵɵtemplate(2, FormFieldInputPasswordComponent_mat_label_2_Template, 2, 1, "mat-label", 2);
            ɵɵelementStart(3, "input", 3, 4);
            ɵɵlistener("keyup", function FormFieldInputPasswordComponent_Template_input_keyup_3_listener() { ɵɵrestoreView(_r14); var _r1 = ɵɵreference(4); return ctx.onKeyUp(_r1); })("blur", function FormFieldInputPasswordComponent_Template_input_blur_3_listener() { ɵɵrestoreView(_r14); var _r1 = ɵɵreference(4); return ctx.onKeyUp(_r1); });
            ɵɵelementEnd();
            ɵɵtemplate(5, FormFieldInputPasswordComponent_mat_icon_5_Template, 2, 1, "mat-icon", 5);
            ɵɵtemplate(6, FormFieldInputPasswordComponent_mat_icon_6_Template, 2, 1, "mat-icon", 6);
            ɵɵtemplate(7, FormFieldInputPasswordComponent_mat_icon_7_Template, 2, 1, "mat-icon", 7);
            ɵɵtemplate(8, FormFieldInputPasswordComponent_span_8_Template, 2, 1, "span", 5);
            ɵɵtemplate(9, FormFieldInputPasswordComponent_span_9_Template, 2, 1, "span", 6);
            ɵɵtemplate(10, FormFieldInputPasswordComponent_mat_hint_10_Template, 2, 1, "mat-hint", 8);
            ɵɵtemplate(11, FormFieldInputPasswordComponent_mat_hint_11_Template, 2, 1, "mat-hint", 9);
            ɵɵtemplate(12, FormFieldInputPasswordComponent_mat_hint_12_Template, 2, 3, "mat-hint", 9);
            ɵɵtemplate(13, FormFieldInputPasswordComponent_mat_hint_13_Template, 2, 2, "mat-hint", 9);
            ɵɵtemplate(14, FormFieldInputPasswordComponent_mat_hint_14_Template, 2, 2, "mat-hint", 9);
            ɵɵelementEnd();
            ɵɵelementEnd();
        } if (rf & 2) {
            ɵɵadvance(1);
            ɵɵproperty("appearance", ctx.formFieldInputPassword.getAppearance())("hideRequiredMarker", ctx.formFieldInputPassword.getRequired().hideRequiredMarker);
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.formFieldInputPassword.getLabelName());
            ɵɵadvance(1);
            ɵɵproperty("type", ctx.formFieldInputPassword.getShowPasswordInMs() && ctx.formFieldInputPassword.getShowPassword() ? "text" : ctx.formFieldInputPassword.getFormFieldInputType())("placeholder", ctx.formFieldInputPassword.getPlaceholder() ? ctx.formFieldInputPassword.getPlaceholder() : "")("value", ctx.formFieldInputPassword.getDefaultValue())("disabled", ctx.formFieldInputPassword.getDisabled())("readonly", ctx.formFieldInputPassword.getReadonly())("required", ctx.formFieldInputPassword.getRequired().required);
            ɵɵadvance(2);
            ɵɵproperty("ngIf", ctx.formFieldInputPassword.getMatPrefixImgText());
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.formFieldInputPassword.getMatSuffixImgText());
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.formFieldInputPassword.getShowPasswordInMs());
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.formFieldInputPassword.getTextPrefix());
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.formFieldInputPassword.getTextSuffix());
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.formFieldInputPassword.getLeftHintLabel());
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.formFieldInputPassword.getRightHintLabel());
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.formFieldInputPassword.getShowHintAboutMinMaxLength() && ctx.getMinLengthValidator() && ctx.getMaxLengthValidator());
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.formFieldInputPassword.getShowHintAboutMinMaxLength() && ctx.getMinLengthValidator() && !ctx.getMaxLengthValidator());
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.formFieldInputPassword.getShowHintAboutMinMaxLength() && !ctx.getMinLengthValidator() && ctx.getMaxLengthValidator());
        } }, directives: [DefaultLayoutAlignDirective, MatFormField, NgIf, MatInput, MatLabel, MatIcon, MatPrefix, MatSuffix, MatHint], styles: ["", ".cursor[_ngcontent-%COMP%] {\n        cursor: pointer;\n    }\n\n    input[type=\"file\"][_ngcontent-%COMP%] {\n        display: none;\n    }\n\n    h3[_ngcontent-%COMP%] {\n        font-size: inherit;\n        margin: 0;\n        font-weight: inherit;\n    }\n\n    mat-form-field[_ngcontent-%COMP%] {\n        min-width: 250px;\n    }"] });
    return FormFieldInputPasswordComponent;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(FormFieldInputPasswordComponent, [{
        type: Component,
        args: [{
                selector: "app-form-field-input-password",
                templateUrl: "./form-field-input-password.component.html",
                styleUrls: ["./form-field-input-password.component.css"],
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        multi: true,
                        useExisting: forwardRef(function () { return FormFieldInputPasswordComponent; }),
                    },
                ],
            }]
    }], function () { return []; }, { formFieldInputPassword: [{
            type: Input
        }] }); })();

function FormFieldInputNumberComponent_mat_label_2_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-label");
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r0 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r0.formFieldInputNumber.getLabelName());
} }
function FormFieldInputNumberComponent_mat_icon_5_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-icon", 9);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r2 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r2.formFieldInputNumber.getMatPrefixImgText());
} }
function FormFieldInputNumberComponent_mat_icon_6_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-icon", 10);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r3 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r3.formFieldInputNumber.getMatSuffixImgText());
} }
function FormFieldInputNumberComponent_span_7_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "span", 9);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r4 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate1("", ctx_r4.formFieldInputNumber.getTextPrefix(), "\u00A0");
} }
function FormFieldInputNumberComponent_span_8_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "span", 10);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r5 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r5.formFieldInputNumber.getTextSuffix());
} }
function FormFieldInputNumberComponent_mat_hint_9_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-hint", 11);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r6 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r6.formFieldInputNumber.getLeftHintLabel());
} }
function FormFieldInputNumberComponent_mat_hint_10_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-hint", 12);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r7 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r7.formFieldInputNumber.getRightHintLabel());
} }
var FormFieldInputNumberComponent = /** @class */ (function () {
    function FormFieldInputNumberComponent() {
        var _this = this;
        this.onChange = function () { };
        this.onTouched = function () { };
        this.onKeyUp = function (input) {
            // console.log(input.value);
            _this.formFieldInputNumber.setDefaultValue(input.value);
            _this.onChange(_this.formFieldInputNumber.getDefaultValue());
            _this.onTouched();
        };
    }
    FormFieldInputNumberComponent.prototype.registerOnChange = function (fn) {
        this.onChange = fn;
    };
    FormFieldInputNumberComponent.prototype.registerOnTouched = function (fn) {
        this.onTouched = fn;
    };
    FormFieldInputNumberComponent.prototype.writeValue = function (value) {
        if (value) {
            this.formFieldInputNumber.setDefaultValue(value);
        }
    };
    FormFieldInputNumberComponent.prototype.ngOnInit = function () {
        // console.log('FormFieldInputNumberComponent init');
    };
    FormFieldInputNumberComponent.prototype.ngOnDestroy = function () {
        // console.log('FormFieldInputNumberComponent destroyed');
    };
    FormFieldInputNumberComponent.ɵfac = function FormFieldInputNumberComponent_Factory(t) { return new (t || FormFieldInputNumberComponent)(); };
    FormFieldInputNumberComponent.ɵcmp = ɵɵdefineComponent({ type: FormFieldInputNumberComponent, selectors: [["app-form-field-input-number"]], inputs: { formFieldInputNumber: "formFieldInputNumber" }, features: [ɵɵProvidersFeature([
                {
                    provide: NG_VALUE_ACCESSOR,
                    multi: true,
                    useExisting: forwardRef(function () { return FormFieldInputNumberComponent; }),
                },
            ])], decls: 11, vars: 16, consts: [["fxLayoutAlign", "center center"], [3, "appearance", "hideRequiredMarker"], [4, "ngIf"], ["matInput", "", 3, "type", "placeholder", "value", "step", "disabled", "readonly", "required", "keyup", "blur"], ["input", ""], ["matPrefix", "", 4, "ngIf"], ["matSuffix", "", 4, "ngIf"], ["align", "start", 4, "ngIf"], ["align", "end", 4, "ngIf"], ["matPrefix", ""], ["matSuffix", ""], ["align", "start"], ["align", "end"]], template: function FormFieldInputNumberComponent_Template(rf, ctx) { if (rf & 1) {
            var _r8 = ɵɵgetCurrentView();
            ɵɵelementStart(0, "section", 0);
            ɵɵelementStart(1, "mat-form-field", 1);
            ɵɵtemplate(2, FormFieldInputNumberComponent_mat_label_2_Template, 2, 1, "mat-label", 2);
            ɵɵelementStart(3, "input", 3, 4);
            ɵɵlistener("keyup", function FormFieldInputNumberComponent_Template_input_keyup_3_listener() { ɵɵrestoreView(_r8); var _r1 = ɵɵreference(4); return ctx.onKeyUp(_r1); })("blur", function FormFieldInputNumberComponent_Template_input_blur_3_listener() { ɵɵrestoreView(_r8); var _r1 = ɵɵreference(4); return ctx.onKeyUp(_r1); });
            ɵɵelementEnd();
            ɵɵtemplate(5, FormFieldInputNumberComponent_mat_icon_5_Template, 2, 1, "mat-icon", 5);
            ɵɵtemplate(6, FormFieldInputNumberComponent_mat_icon_6_Template, 2, 1, "mat-icon", 6);
            ɵɵtemplate(7, FormFieldInputNumberComponent_span_7_Template, 2, 1, "span", 5);
            ɵɵtemplate(8, FormFieldInputNumberComponent_span_8_Template, 2, 1, "span", 6);
            ɵɵtemplate(9, FormFieldInputNumberComponent_mat_hint_9_Template, 2, 1, "mat-hint", 7);
            ɵɵtemplate(10, FormFieldInputNumberComponent_mat_hint_10_Template, 2, 1, "mat-hint", 8);
            ɵɵelementEnd();
            ɵɵelementEnd();
        } if (rf & 2) {
            ɵɵadvance(1);
            ɵɵproperty("appearance", ctx.formFieldInputNumber.getAppearance())("hideRequiredMarker", ctx.formFieldInputNumber.getRequired().hideRequiredMarker);
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.formFieldInputNumber.getLabelName());
            ɵɵadvance(1);
            ɵɵproperty("type", ctx.formFieldInputNumber.getFormFieldInputType())("placeholder", ctx.formFieldInputNumber.getPlaceholder() ? ctx.formFieldInputNumber.getPlaceholder() : "")("value", ctx.formFieldInputNumber.getDefaultValue())("step", ctx.formFieldInputNumber.getStep())("disabled", ctx.formFieldInputNumber.getDisabled())("readonly", ctx.formFieldInputNumber.getReadonly())("required", ctx.formFieldInputNumber.getRequired().required);
            ɵɵadvance(2);
            ɵɵproperty("ngIf", ctx.formFieldInputNumber.getMatPrefixImgText());
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.formFieldInputNumber.getMatSuffixImgText());
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.formFieldInputNumber.getTextPrefix());
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.formFieldInputNumber.getTextSuffix());
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.formFieldInputNumber.getLeftHintLabel());
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.formFieldInputNumber.getRightHintLabel());
        } }, directives: [DefaultLayoutAlignDirective, MatFormField, NgIf, MatInput, MatLabel, MatIcon, MatPrefix, MatSuffix, MatHint], styles: ["", ".cursor[_ngcontent-%COMP%] {\n        cursor: pointer;\n    }\n\n    input[type=\"file\"][_ngcontent-%COMP%] {\n        display: none;\n    }\n\n    h3[_ngcontent-%COMP%] {\n        font-size: inherit;\n        margin: 0;\n        font-weight: inherit;\n    }\n\n    mat-form-field[_ngcontent-%COMP%] {\n        min-width: 250px;\n    }"] });
    return FormFieldInputNumberComponent;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(FormFieldInputNumberComponent, [{
        type: Component,
        args: [{
                selector: "app-form-field-input-number",
                templateUrl: "./form-field-input-number.component.html",
                styleUrls: ["./form-field-input-number.component.css"],
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        multi: true,
                        useExisting: forwardRef(function () { return FormFieldInputNumberComponent; }),
                    },
                ],
            }]
    }], function () { return []; }, { formFieldInputNumber: [{
            type: Input
        }] }); })();

function FormFieldInputEmailComponent_mat_label_2_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-label");
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r0 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r0.formFieldInputEmail.getLabelName());
} }
function FormFieldInputEmailComponent_mat_icon_5_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-icon", 9);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r2 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r2.formFieldInputEmail.getMatPrefixImgText());
} }
function FormFieldInputEmailComponent_mat_icon_6_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-icon", 10);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r3 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r3.formFieldInputEmail.getMatSuffixImgText());
} }
function FormFieldInputEmailComponent_span_7_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "span", 9);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r4 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate1("", ctx_r4.formFieldInputEmail.getTextPrefix(), "\u00A0");
} }
function FormFieldInputEmailComponent_span_8_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "span", 10);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r5 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r5.formFieldInputEmail.getTextSuffix());
} }
function FormFieldInputEmailComponent_mat_hint_9_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-hint", 11);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r6 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r6.formFieldInputEmail.getLeftHintLabel());
} }
function FormFieldInputEmailComponent_mat_hint_10_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-hint", 12);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r7 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r7.formFieldInputEmail.getRightHintLabel());
} }
var FormFieldInputEmailComponent = /** @class */ (function () {
    function FormFieldInputEmailComponent() {
        var _this = this;
        this.onChange = function () { };
        this.onTouched = function () { };
        this.onKeyUp = function (input) {
            _this.formFieldInputEmail.setDefaultValue(input.value);
            _this.onChange(_this.formFieldInputEmail.getDefaultValue());
            _this.onTouched();
        };
    }
    FormFieldInputEmailComponent.prototype.registerOnChange = function (fn) {
        this.onChange = fn;
    };
    FormFieldInputEmailComponent.prototype.registerOnTouched = function (fn) {
        this.onTouched = fn;
    };
    FormFieldInputEmailComponent.prototype.writeValue = function (value) {
        if (value) {
            this.formFieldInputEmail.setDefaultValue(value);
        }
    };
    FormFieldInputEmailComponent.prototype.ngOnInit = function () {
        // console.log('FormFieldInputEmailComponent init');
    };
    FormFieldInputEmailComponent.prototype.ngOnDestroy = function () {
        // console.log('FormFieldInputEmailComponent destroyed');
    };
    FormFieldInputEmailComponent.ɵfac = function FormFieldInputEmailComponent_Factory(t) { return new (t || FormFieldInputEmailComponent)(); };
    FormFieldInputEmailComponent.ɵcmp = ɵɵdefineComponent({ type: FormFieldInputEmailComponent, selectors: [["app-form-field-input-email"]], inputs: { formFieldInputEmail: "formFieldInputEmail" }, features: [ɵɵProvidersFeature([
                {
                    provide: NG_VALUE_ACCESSOR,
                    multi: true,
                    useExisting: forwardRef(function () { return FormFieldInputEmailComponent; }),
                },
            ])], decls: 11, vars: 15, consts: [["fxLayoutAlign", "center center"], [3, "appearance", "hideRequiredMarker"], [4, "ngIf"], ["matInput", "", 3, "type", "placeholder", "value", "disabled", "readonly", "required", "keyup", "blur"], ["input", ""], ["matPrefix", "", 4, "ngIf"], ["matSuffix", "", 4, "ngIf"], ["align", "start", 4, "ngIf"], ["align", "end", 4, "ngIf"], ["matPrefix", ""], ["matSuffix", ""], ["align", "start"], ["align", "end"]], template: function FormFieldInputEmailComponent_Template(rf, ctx) { if (rf & 1) {
            var _r8 = ɵɵgetCurrentView();
            ɵɵelementStart(0, "section", 0);
            ɵɵelementStart(1, "mat-form-field", 1);
            ɵɵtemplate(2, FormFieldInputEmailComponent_mat_label_2_Template, 2, 1, "mat-label", 2);
            ɵɵelementStart(3, "input", 3, 4);
            ɵɵlistener("keyup", function FormFieldInputEmailComponent_Template_input_keyup_3_listener() { ɵɵrestoreView(_r8); var _r1 = ɵɵreference(4); return ctx.onKeyUp(_r1); })("blur", function FormFieldInputEmailComponent_Template_input_blur_3_listener() { ɵɵrestoreView(_r8); var _r1 = ɵɵreference(4); return ctx.onKeyUp(_r1); });
            ɵɵelementEnd();
            ɵɵtemplate(5, FormFieldInputEmailComponent_mat_icon_5_Template, 2, 1, "mat-icon", 5);
            ɵɵtemplate(6, FormFieldInputEmailComponent_mat_icon_6_Template, 2, 1, "mat-icon", 6);
            ɵɵtemplate(7, FormFieldInputEmailComponent_span_7_Template, 2, 1, "span", 5);
            ɵɵtemplate(8, FormFieldInputEmailComponent_span_8_Template, 2, 1, "span", 6);
            ɵɵtemplate(9, FormFieldInputEmailComponent_mat_hint_9_Template, 2, 1, "mat-hint", 7);
            ɵɵtemplate(10, FormFieldInputEmailComponent_mat_hint_10_Template, 2, 1, "mat-hint", 8);
            ɵɵelementEnd();
            ɵɵelementEnd();
        } if (rf & 2) {
            ɵɵadvance(1);
            ɵɵproperty("appearance", ctx.formFieldInputEmail.getAppearance())("hideRequiredMarker", ctx.formFieldInputEmail.getRequired().hideRequiredMarker);
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.formFieldInputEmail.getLabelName());
            ɵɵadvance(1);
            ɵɵproperty("type", ctx.formFieldInputEmail.getFormFieldInputType())("placeholder", ctx.formFieldInputEmail.getPlaceholder() ? ctx.formFieldInputEmail.getPlaceholder() : "")("value", ctx.formFieldInputEmail.getDefaultValue())("disabled", ctx.formFieldInputEmail.getDisabled())("readonly", ctx.formFieldInputEmail.getReadonly())("required", ctx.formFieldInputEmail.getRequired().required);
            ɵɵadvance(2);
            ɵɵproperty("ngIf", ctx.formFieldInputEmail.getMatPrefixImgText());
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.formFieldInputEmail.getMatSuffixImgText());
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.formFieldInputEmail.getTextPrefix());
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.formFieldInputEmail.getTextSuffix());
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.formFieldInputEmail.getLeftHintLabel());
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.formFieldInputEmail.getRightHintLabel());
        } }, directives: [DefaultLayoutAlignDirective, MatFormField, NgIf, MatInput, MatLabel, MatIcon, MatPrefix, MatSuffix, MatHint], styles: ["", ".cursor[_ngcontent-%COMP%] {\n        cursor: pointer;\n    }\n\n    input[type=\"file\"][_ngcontent-%COMP%] {\n        display: none;\n    }\n\n    h3[_ngcontent-%COMP%] {\n        font-size: inherit;\n        margin: 0;\n        font-weight: inherit;\n    }\n\n    .upload-file-container[_ngcontent-%COMP%] {\n        min-height: 200px;\n        width: 80%;\n        margin: 20px auto;        \n        border-radius: 10px;\n    }\n\n    mat-form-field[_ngcontent-%COMP%] {\n        min-width: 250px;\n    }"] });
    return FormFieldInputEmailComponent;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(FormFieldInputEmailComponent, [{
        type: Component,
        args: [{
                selector: "app-form-field-input-email",
                templateUrl: "./form-field-input-email.component.html",
                styleUrls: ["./form-field-input-email.component.css"],
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        multi: true,
                        useExisting: forwardRef(function () { return FormFieldInputEmailComponent; }),
                    },
                ],
            }]
    }], function () { return []; }, { formFieldInputEmail: [{
            type: Input
        }] }); })();

var UiService = /** @class */ (function () {
    function UiService(snackBar) {
        var _this = this;
        this.snackBar = snackBar;
        this.onShowSnackBar = function (message, action, duration) {
            _this.snackBar.open(message, action, {
                duration: duration,
            });
        };
    }
    UiService.ɵfac = function UiService_Factory(t) { return new (t || UiService)(ɵɵinject(MatSnackBar)); };
    UiService.ɵprov = ɵɵdefineInjectable({ token: UiService, factory: UiService.ɵfac, providedIn: "root" });
    return UiService;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(UiService, [{
        type: Injectable,
        args: [{ providedIn: "root" }]
    }], function () { return [{ type: MatSnackBar }]; }, null); })();

// https://medium.com/@mariemchabeni/angular-7-drag-and-drop-simple-file-uploadin-in-less-than-5-minutes-d57eb010c0dc
var DragDropDirective = /** @class */ (function () {
    function DragDropDirective() {
        this.onFileDropped = new EventEmitter();
        this.opacity = '1';
    }
    //Dragover listener
    DragDropDirective.prototype.onDragOver = function (evt) {
        evt.preventDefault();
        evt.stopPropagation();
        this.opacity = '0.5';
    };
    //Dragleave listener
    DragDropDirective.prototype.onDragLeave = function (evt) {
        evt.preventDefault();
        evt.stopPropagation();
        this.opacity = '1';
    };
    //Drop listener
    DragDropDirective.prototype.onDrop = function (evt) {
        evt.preventDefault();
        evt.stopPropagation();
        this.opacity = '1';
        var files = evt.dataTransfer.files;
        if (files.length > 0) {
            this.onFileDropped.emit(files);
        }
    };
    DragDropDirective.ɵfac = function DragDropDirective_Factory(t) { return new (t || DragDropDirective)(); };
    DragDropDirective.ɵdir = ɵɵdefineDirective({ type: DragDropDirective, selectors: [["", "appDragDrop", ""]], hostVars: 2, hostBindings: function DragDropDirective_HostBindings(rf, ctx) { if (rf & 1) {
            ɵɵlistener("dragover", function DragDropDirective_dragover_HostBindingHandler($event) { return ctx.onDragOver($event); })("dragleave", function DragDropDirective_dragleave_HostBindingHandler($event) { return ctx.onDragLeave($event); })("drop", function DragDropDirective_drop_HostBindingHandler($event) { return ctx.onDrop($event); });
        } if (rf & 2) {
            ɵɵstyleProp("opacity", ctx.opacity);
        } }, outputs: { onFileDropped: "onFileDropped" } });
    return DragDropDirective;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(DragDropDirective, [{
        type: Directive,
        args: [{
                selector: '[appDragDrop]',
            }]
    }], null, { onFileDropped: [{
            type: Output
        }], opacity: [{
            type: HostBinding,
            args: ['style.opacity']
        }], onDragOver: [{
            type: HostListener,
            args: ['dragover', ['$event']]
        }], onDragLeave: [{
            type: HostListener,
            args: ['dragleave', ['$event']]
        }], onDrop: [{
            type: HostListener,
            args: ['drop', ['$event']]
        }] }); })();

var _c0 = ["appDragDrop"];
var _c1 = ["file"];
function FormFieldInputFileComponent_section_0_mat_selection_list_4_mat_list_option_2_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-list-option", 12);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var item_r9 = ctx.$implicit;
    ɵɵproperty("value", item_r9);
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", item_r9.name, " ");
} }
function FormFieldInputFileComponent_section_0_mat_selection_list_4_button_3_Template(rf, ctx) { if (rf & 1) {
    var _r11 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "button", 13);
    ɵɵlistener("click", function FormFieldInputFileComponent_section_0_mat_selection_list_4_button_3_Template_button_click_0_listener() { ɵɵrestoreView(_r11); ɵɵnextContext(); var _r6 = ɵɵreference(1); var ctx_r10 = ɵɵnextContext(2); return ctx_r10.onRemoveFiles(_r6.selectedOptions.selected); });
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r8 = ɵɵnextContext(3);
    ɵɵproperty("color", ctx_r8.formFieldInputFile.getButtonDeleteColor());
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", ctx_r8.formFieldInputFile.getButtonDeleteText(), " ");
} }
function FormFieldInputFileComponent_section_0_mat_selection_list_4_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-selection-list", 8, 9);
    ɵɵtemplate(2, FormFieldInputFileComponent_section_0_mat_selection_list_4_mat_list_option_2_Template, 2, 2, "mat-list-option", 10);
    ɵɵtemplate(3, FormFieldInputFileComponent_section_0_mat_selection_list_4_button_3_Template, 2, 2, "button", 11);
    ɵɵelementEnd();
} if (rf & 2) {
    var _r6 = ɵɵreference(1);
    var ctx_r4 = ɵɵnextContext(2);
    ɵɵadvance(2);
    ɵɵproperty("ngForOf", ctx_r4.formFieldInputFile.getDefaultValue());
    ɵɵadvance(1);
    ɵɵproperty("ngIf", _r6.selectedOptions.selected.length > 0);
} }
function FormFieldInputFileComponent_section_0_Template(rf, ctx) { if (rf & 1) {
    var _r13 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "section", 2, 3);
    ɵɵlistener("onFileDropped", function FormFieldInputFileComponent_section_0_Template_section_onFileDropped_0_listener($event) { ɵɵrestoreView(_r13); var ctx_r12 = ɵɵnextContext(); return !ctx_r12.formFieldInputFile.getDisabled() && ctx_r12.onFileDropped($event); });
    ɵɵelementStart(2, "h3");
    ɵɵtext(3);
    ɵɵelementEnd();
    ɵɵtemplate(4, FormFieldInputFileComponent_section_0_mat_selection_list_4_Template, 4, 2, "mat-selection-list", 4);
    ɵɵelementStart(5, "input", 5, 6);
    ɵɵlistener("change", function FormFieldInputFileComponent_section_0_Template_input_change_5_listener() { ɵɵrestoreView(_r13); var ctx_r14 = ɵɵnextContext(); return ctx_r14.onFilesAdded(); });
    ɵɵelementEnd();
    ɵɵelementStart(7, "button", 7);
    ɵɵlistener("click", function FormFieldInputFileComponent_section_0_Template_button_click_7_listener() { ɵɵrestoreView(_r13); var ctx_r15 = ɵɵnextContext(); return ctx_r15.addFiles(); });
    ɵɵtext(8);
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r0 = ɵɵnextContext();
    var tmp_1_0 = null;
    var currVal_1 = ((tmp_1_0 = ctx_r0.formFieldInputFile.getDefaultValue()) == null ? null : tmp_1_0.length) > 0;
    ɵɵadvance(3);
    ɵɵtextInterpolate(ctx_r0.formFieldInputFile.getLabelName());
    ɵɵadvance(1);
    ɵɵproperty("ngIf", currVal_1);
    ɵɵadvance(3);
    ɵɵproperty("color", ctx_r0.formFieldInputFile.getButtonAddColor())("disabled", ctx_r0.formFieldInputFile.getDisabled());
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", ctx_r0.formFieldInputFile.getButtonAddText(), " ");
} }
function FormFieldInputFileComponent_section_1_mat_selection_list_3_mat_list_option_2_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-list-option", 12);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var item_r21 = ctx.$implicit;
    ɵɵproperty("value", item_r21);
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", item_r21.name, " ");
} }
function FormFieldInputFileComponent_section_1_mat_selection_list_3_button_3_Template(rf, ctx) { if (rf & 1) {
    var _r23 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "button", 13);
    ɵɵlistener("click", function FormFieldInputFileComponent_section_1_mat_selection_list_3_button_3_Template_button_click_0_listener() { ɵɵrestoreView(_r23); ɵɵnextContext(); var _r18 = ɵɵreference(1); var ctx_r22 = ɵɵnextContext(2); return ctx_r22.onRemoveFiles(_r18.selectedOptions.selected); });
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r20 = ɵɵnextContext(3);
    ɵɵproperty("color", ctx_r20.formFieldInputFile.getButtonDeleteColor());
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", ctx_r20.formFieldInputFile.getButtonDeleteText(), " ");
} }
function FormFieldInputFileComponent_section_1_mat_selection_list_3_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-selection-list", 8, 9);
    ɵɵtemplate(2, FormFieldInputFileComponent_section_1_mat_selection_list_3_mat_list_option_2_Template, 2, 2, "mat-list-option", 10);
    ɵɵtemplate(3, FormFieldInputFileComponent_section_1_mat_selection_list_3_button_3_Template, 2, 2, "button", 11);
    ɵɵelementEnd();
} if (rf & 2) {
    var _r18 = ɵɵreference(1);
    var ctx_r16 = ɵɵnextContext(2);
    ɵɵadvance(2);
    ɵɵproperty("ngForOf", ctx_r16.formFieldInputFile.getDefaultValue());
    ɵɵadvance(1);
    ɵɵproperty("ngIf", _r18.selectedOptions.selected.length > 0);
} }
function FormFieldInputFileComponent_section_1_Template(rf, ctx) { if (rf & 1) {
    var _r25 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "section", 14);
    ɵɵelementStart(1, "h3");
    ɵɵtext(2);
    ɵɵelementEnd();
    ɵɵtemplate(3, FormFieldInputFileComponent_section_1_mat_selection_list_3_Template, 4, 2, "mat-selection-list", 4);
    ɵɵelementStart(4, "input", 5, 6);
    ɵɵlistener("change", function FormFieldInputFileComponent_section_1_Template_input_change_4_listener() { ɵɵrestoreView(_r25); var ctx_r24 = ɵɵnextContext(); return ctx_r24.onFilesAdded(); });
    ɵɵelementEnd();
    ɵɵelementStart(6, "button", 7);
    ɵɵlistener("click", function FormFieldInputFileComponent_section_1_Template_button_click_6_listener() { ɵɵrestoreView(_r25); var ctx_r26 = ɵɵnextContext(); return ctx_r26.addFiles(); });
    ɵɵtext(7);
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r1 = ɵɵnextContext();
    var tmp_1_0 = null;
    var currVal_1 = ((tmp_1_0 = ctx_r1.formFieldInputFile.getDefaultValue()) == null ? null : tmp_1_0.length) > 0;
    ɵɵadvance(2);
    ɵɵtextInterpolate(ctx_r1.formFieldInputFile.getLabelName());
    ɵɵadvance(1);
    ɵɵproperty("ngIf", currVal_1);
    ɵɵadvance(3);
    ɵɵproperty("color", ctx_r1.formFieldInputFile.getButtonAddColor())("disabled", ctx_r1.formFieldInputFile.getDisabled());
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", ctx_r1.formFieldInputFile.getButtonAddText(), " ");
} }
function FormFieldInputFileComponent_section_2_mat_selection_list_3_mat_list_option_2_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-list-option", 12);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var item_r32 = ctx.$implicit;
    ɵɵproperty("value", item_r32);
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", item_r32.name, " ");
} }
function FormFieldInputFileComponent_section_2_mat_selection_list_3_button_3_Template(rf, ctx) { if (rf & 1) {
    var _r34 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "button", 13);
    ɵɵlistener("click", function FormFieldInputFileComponent_section_2_mat_selection_list_3_button_3_Template_button_click_0_listener() { ɵɵrestoreView(_r34); ɵɵnextContext(); var _r29 = ɵɵreference(1); var ctx_r33 = ɵɵnextContext(2); return ctx_r33.onRemoveFiles(_r29.selectedOptions.selected); });
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r31 = ɵɵnextContext(3);
    ɵɵproperty("color", ctx_r31.formFieldInputFile.getButtonDeleteColor());
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", ctx_r31.formFieldInputFile.getButtonDeleteText(), " ");
} }
function FormFieldInputFileComponent_section_2_mat_selection_list_3_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-selection-list", 8, 9);
    ɵɵtemplate(2, FormFieldInputFileComponent_section_2_mat_selection_list_3_mat_list_option_2_Template, 2, 2, "mat-list-option", 10);
    ɵɵtemplate(3, FormFieldInputFileComponent_section_2_mat_selection_list_3_button_3_Template, 2, 2, "button", 11);
    ɵɵelementEnd();
} if (rf & 2) {
    var _r29 = ɵɵreference(1);
    var ctx_r27 = ɵɵnextContext(2);
    ɵɵadvance(2);
    ɵɵproperty("ngForOf", ctx_r27.formFieldInputFile.getDefaultValue());
    ɵɵadvance(1);
    ɵɵproperty("ngIf", _r29.selectedOptions.selected.length > 0);
} }
function FormFieldInputFileComponent_section_2_Template(rf, ctx) { if (rf & 1) {
    var _r36 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "section", 14);
    ɵɵelementStart(1, "h3");
    ɵɵtext(2);
    ɵɵelementEnd();
    ɵɵtemplate(3, FormFieldInputFileComponent_section_2_mat_selection_list_3_Template, 4, 2, "mat-selection-list", 4);
    ɵɵelementStart(4, "input", 5, 6);
    ɵɵlistener("change", function FormFieldInputFileComponent_section_2_Template_input_change_4_listener() { ɵɵrestoreView(_r36); var ctx_r35 = ɵɵnextContext(); return ctx_r35.onFilesAdded(); });
    ɵɵelementEnd();
    ɵɵelementStart(6, "button", 7);
    ɵɵlistener("click", function FormFieldInputFileComponent_section_2_Template_button_click_6_listener() { ɵɵrestoreView(_r36); var ctx_r37 = ɵɵnextContext(); return ctx_r37.addFiles(); });
    ɵɵtext(7);
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r2 = ɵɵnextContext();
    var tmp_1_0 = null;
    var currVal_1 = ((tmp_1_0 = ctx_r2.formFieldInputFile.getDefaultValue()) == null ? null : tmp_1_0.length) > 0;
    ɵɵadvance(2);
    ɵɵtextInterpolate(ctx_r2.formFieldInputFile.getLabelName());
    ɵɵadvance(1);
    ɵɵproperty("ngIf", currVal_1);
    ɵɵadvance(3);
    ɵɵproperty("color", ctx_r2.formFieldInputFile.getButtonAddColor())("disabled", ctx_r2.formFieldInputFile.getDisabled());
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", ctx_r2.formFieldInputFile.getButtonAddText(), " ");
} }
var FormFieldInputFileComponent = /** @class */ (function () {
    function FormFieldInputFileComponent(renderer, deviceService, uiService) {
        var _this = this;
        this.renderer = renderer;
        this.deviceService = deviceService;
        this.uiService = uiService;
        this.onChange = function () { };
        this.onTouched = function () { };
        this.onFilesAdded = function () {
            var files = _this.file.nativeElement.files;
            for (var key in _this.file.nativeElement.files) {
                if (!isNaN(parseInt(key))) {
                    // console.log(files[key]);
                    if (_this.formFieldInputFile.isFileTypeAllowed(files[key].type))
                        _this.formFieldInputFile.addFile(files[key]);
                    else
                        _this.uiService.onShowSnackBar(files[key].type + ' is not allowed', null, 1500);
                }
            }
            if (_this.formFieldInputFile.getDefaultValue().length > 0) {
                _this.onChange(_this.formFieldInputFile.getDefaultValue());
                _this.onTouched();
            }
        };
        this.onFileDropped = function (fileList) {
            Array.from(fileList).forEach(function (file) {
                if (_this.formFieldInputFile.isFileTypeAllowed(file.type))
                    _this.formFieldInputFile.addFile(file);
                else
                    _this.uiService.onShowSnackBar(file.type + ' is not allowed', null, 1500);
            });
        };
        this.addFiles = function () {
            _this.file.nativeElement.click();
        };
        this.onRemoveFiles = function (matListOptions) {
            var _loop_1 = function (mLOindex) {
                var matListOption = matListOptions[mLOindex];
                _this.formFieldInputFile.setDefaultValue(_this.formFieldInputFile
                    .getDefaultValue()
                    .filter(function (f) { return f !== matListOption.value; }));
            };
            for (var mLOindex in matListOptions) {
                _loop_1(mLOindex);
            }
        };
    }
    FormFieldInputFileComponent.prototype.registerOnChange = function (fn) {
        this.onChange = fn;
    };
    FormFieldInputFileComponent.prototype.registerOnTouched = function (fn) {
        this.onTouched = fn;
    };
    FormFieldInputFileComponent.prototype.writeValue = function (value) {
        if (value) {
            this.formFieldInputFile.setDefaultValue(value);
        }
    };
    FormFieldInputFileComponent.prototype.ngOnInit = function () {
        // console.log('FormFieldInputFileComponent init');
    };
    FormFieldInputFileComponent.prototype.ngOnDestroy = function () {
        // console.log('FormFieldInputFileComponent destroyed');
    };
    FormFieldInputFileComponent.prototype.ngAfterViewInit = function () {
        if (this.appDragDrop)
            this.renderer.setStyle(this.appDragDrop.nativeElement, 'border', '2px dashed ' + this.formFieldInputFile.getBorderColor());
    };
    FormFieldInputFileComponent.ɵfac = function FormFieldInputFileComponent_Factory(t) { return new (t || FormFieldInputFileComponent)(ɵɵdirectiveInject(Renderer2), ɵɵdirectiveInject(DeviceService), ɵɵdirectiveInject(UiService)); };
    FormFieldInputFileComponent.ɵcmp = ɵɵdefineComponent({ type: FormFieldInputFileComponent, selectors: [["app-form-field-input-file"]], viewQuery: function FormFieldInputFileComponent_Query(rf, ctx) { if (rf & 1) {
            ɵɵviewQuery(_c0, true);
            ɵɵviewQuery(_c1, true);
        } if (rf & 2) {
            var _t;
            ɵɵqueryRefresh(_t = ɵɵloadQuery()) && (ctx.appDragDrop = _t.first);
            ɵɵqueryRefresh(_t = ɵɵloadQuery()) && (ctx.file = _t.first);
        } }, inputs: { formFieldInputFile: "formFieldInputFile" }, features: [ɵɵProvidersFeature([
                {
                    provide: NG_VALUE_ACCESSOR,
                    multi: true,
                    useExisting: forwardRef(function () { return FormFieldInputFileComponent; }),
                },
            ])], decls: 3, vars: 3, consts: [["fxLayout", "column", "fxLayoutAlign", "center center", "fxLayoutGap", "10px", "class", "upload-file-container add-padding", "appDragDrop", "", 3, "onFileDropped", 4, "ngIf"], ["fxLayout", "column", "fxLayoutAlign", "center center", "fxLayoutGap", "10px", "class", "add-padding", 4, "ngIf"], ["fxLayout", "column", "fxLayoutAlign", "center center", "fxLayoutGap", "10px", "appDragDrop", "", 1, "upload-file-container", "add-padding", 3, "onFileDropped"], ["appDragDrop", ""], ["fxLayout", "column", "fxLayoutAlign", "center center", "fxLayoutGap", "8px", 4, "ngIf"], ["type", "file", "multiple", "", 3, "change"], ["file", ""], ["type", "button", "mat-raised-button", "", 3, "color", "disabled", "click"], ["fxLayout", "column", "fxLayoutAlign", "center center", "fxLayoutGap", "8px"], ["files", ""], [3, "value", 4, "ngFor", "ngForOf"], ["type", "button", "mat-button", "", 3, "color", "click", 4, "ngIf"], [3, "value"], ["type", "button", "mat-button", "", 3, "color", "click"], ["fxLayout", "column", "fxLayoutAlign", "center center", "fxLayoutGap", "10px", 1, "add-padding"]], template: function FormFieldInputFileComponent_Template(rf, ctx) { if (rf & 1) {
            ɵɵtemplate(0, FormFieldInputFileComponent_section_0_Template, 9, 5, "section", 0);
            ɵɵtemplate(1, FormFieldInputFileComponent_section_1_Template, 8, 5, "section", 1);
            ɵɵtemplate(2, FormFieldInputFileComponent_section_2_Template, 8, 5, "section", 1);
        } if (rf & 2) {
            ɵɵproperty("ngIf", ctx.formFieldInputFile.getDragAndDrop() && ctx.deviceService.isDeviceDesktop());
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.formFieldInputFile.getDragAndDrop() && !ctx.deviceService.isDeviceDesktop());
            ɵɵadvance(1);
            ɵɵproperty("ngIf", !ctx.formFieldInputFile.getDragAndDrop());
        } }, directives: [NgIf, DefaultLayoutDirective, DefaultLayoutAlignDirective, DefaultLayoutGapDirective, DragDropDirective, MatButton, MatSelectionList, NgForOf, MatListOption], styles: ["", ".add-padding[_ngcontent-%COMP%] {\n        padding: 20px;\n    }\n\n    .cursor[_ngcontent-%COMP%] {\n        cursor: pointer;\n    }\n\n    input[type=\"file\"][_ngcontent-%COMP%] {\n        display: none;\n    }\n\n    h3[_ngcontent-%COMP%] {\n        font-size: inherit;\n        margin: 0;\n        font-weight: inherit;\n    }\n\n    .upload-file-container[_ngcontent-%COMP%] {\n        min-height: 200px;\n        \n        \n        margin: 5px auto;        \n        border-radius: 10px;\n    }\n\n    mat-form-field[_ngcontent-%COMP%] {\n        min-width: 250px;\n    }"] });
    return FormFieldInputFileComponent;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(FormFieldInputFileComponent, [{
        type: Component,
        args: [{
                selector: 'app-form-field-input-file',
                templateUrl: './form-field-input-file.component.html',
                styleUrls: ['./form-field-input-file.component.css'],
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        multi: true,
                        useExisting: forwardRef(function () { return FormFieldInputFileComponent; }),
                    },
                ],
            }]
    }], function () { return [{ type: Renderer2 }, { type: DeviceService }, { type: UiService }]; }, { appDragDrop: [{
            type: ViewChild,
            args: ['appDragDrop', { static: false }]
        }], file: [{
            type: ViewChild,
            args: ['file', { static: false }]
        }], formFieldInputFile: [{
            type: Input
        }] }); })();

function DatepickerComponent_mat_datepicker_toggle_6_mat_icon_1_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-icon", 11);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r5 = ɵɵnextContext(2);
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r5.datepicker.getMatIcon());
} }
function DatepickerComponent_mat_datepicker_toggle_6_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-datepicker-toggle", 9);
    ɵɵtemplate(1, DatepickerComponent_mat_datepicker_toggle_6_mat_icon_1_Template, 2, 1, "mat-icon", 10);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r1 = ɵɵnextContext();
    var _r3 = ɵɵreference(9);
    ɵɵproperty("for", _r3);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r1.datepicker.getMatIcon());
} }
function DatepickerComponent_mat_datepicker_toggle_7_mat_icon_1_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-icon", 11);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r6 = ɵɵnextContext(2);
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r6.datepicker.getMatIcon());
} }
function DatepickerComponent_mat_datepicker_toggle_7_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-datepicker-toggle", 12);
    ɵɵtemplate(1, DatepickerComponent_mat_datepicker_toggle_7_mat_icon_1_Template, 2, 1, "mat-icon", 10);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r2 = ɵɵnextContext();
    var _r3 = ɵɵreference(9);
    ɵɵproperty("for", _r3);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r2.datepicker.getMatIcon());
} }
function DatepickerComponent_mat_form_field_10_Template(rf, ctx) { if (rf & 1) {
    var _r8 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "mat-form-field", 13);
    ɵɵelementStart(1, "mat-label");
    ɵɵtext(2);
    ɵɵelementEnd();
    ɵɵelement(3, "br");
    ɵɵelementStart(4, "ngx-timepicker-field", 14);
    ɵɵlistener("timeChanged", function DatepickerComponent_mat_form_field_10_Template_ngx_timepicker_field_timeChanged_4_listener($event) { ɵɵrestoreView(_r8); var ctx_r7 = ɵɵnextContext(); return ctx_r7.onTimeChanged($event); });
    ɵɵelementEnd();
    ɵɵelement(5, "input", 15);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r4 = ɵɵnextContext();
    ɵɵproperty("appearance", ctx_r4.datepicker.getAppearance());
    ɵɵadvance(2);
    ɵɵtextInterpolate(ctx_r4.datepicker.getTimepickerInsideDatepickerInterface().labelName ? ctx_r4.datepicker.getTimepickerInsideDatepickerInterface().labelName : "pick time");
    ɵɵadvance(2);
    ɵɵproperty("buttonAlign", ctx_r4.datepicker.getTimepickerInsideDatepickerInterface().buttonAlign ? ctx_r4.datepicker.getTimepickerInsideDatepickerInterface().buttonAlign : "right")("disabled", ctx_r4.datepicker.getTimepickerInsideDatepickerInterface().disabled)("format", 24)("min", ctx_r4.datepicker.getTimepickerInsideDatepickerInterface().min ? ctx_r4.datepicker.getTimepickerInsideDatepickerInterface().min : undefined)("max", ctx_r4.datepicker.getTimepickerInsideDatepickerInterface().max ? ctx_r4.datepicker.getTimepickerInsideDatepickerInterface().max : undefined)("defaultTime", ctx_r4.datepicker.getTimepickerInsideDatepickerInterface().defaultValue ? ctx_r4.datepicker.getTimepickerInsideDatepickerInterface().defaultValue : undefined);
    ɵɵadvance(1);
    ɵɵproperty("hidden", true);
} }
var MY_FORMATS = {
    parse: {
        dateInput: 'LL',
    },
    display: {
        dateInput: 'YYYY-MM-DD',
        monthYearLabel: 'YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'YYYY',
    },
};
var DatepickerComponent = /** @class */ (function () {
    function DatepickerComponent(deviceService) {
        var _this = this;
        this.deviceService = deviceService;
        this.onChange = function () { };
        this.onTouched = function () { };
        this.onBlur = function () {
            _this.onChange(_this.datepicker.getDefaultValue());
            _this.onTouched();
        };
        this.disableDays = function (d) {
            var day = d.day();
            var allow = true;
            _this.datepicker.getDisabledDays().forEach(function (d) {
                if (day === d)
                    allow = false;
            });
            return allow;
        };
        this.onTimeChanged = function (time) {
            var sati = +time.split(':')[0];
            var minuti = +time.split(':')[1];
            // console.log('treba dodati time: sati: ', sati, ' minuti: ', minuti);
            if (_this.datepicker.getDefaultValue() instanceof Date) {
                // console.log('treba dodati time: sati: ', sati, ' minuti: ', minuti);
                var date = _this.datepicker.getDefaultValue();
                date.setHours(date.getHours() - _this._prethodniSati + sati);
                date.setMinutes(date.getMinutes() - _this._prethodniMinuti + minuti);
                // console.log(date.toUTCString());
            }
            _this._prethodniSati = sati;
            _this._prethodniMinuti = minuti;
        };
    }
    DatepickerComponent.prototype.addEvent = function (type, event) {
        // console.log(`${type}: ${event.value}`);
        // console.log(new Date(event.value));
        var date = new Date(event.value);
        date.setHours(date.getHours() + this._prethodniSati);
        date.setMinutes(date.getMinutes() + this._prethodniMinuti);
        this.datepicker.setDefaultValue(date);
        this.onChange(this.datepicker.getDefaultValue());
        this.onTouched();
        // console.log(this.datepicker.getDefaultValue());
        // console.log(this.datepicker.getDefaultValue().toUTCString());
    };
    DatepickerComponent.prototype.registerOnChange = function (fn) {
        this.onChange = fn;
    };
    DatepickerComponent.prototype.registerOnTouched = function (fn) {
        this.onTouched = fn;
    };
    DatepickerComponent.prototype.writeValue = function (value) {
        if (value) {
            this.datepicker.setDefaultValue(value);
        }
    };
    DatepickerComponent.prototype.ngOnInit = function () {
        var _a;
        if ((_a = this.datepicker.getTimepickerInsideDatepickerInterface()) === null || _a === void 0 ? void 0 : _a.defaultValue) {
            this._prethodniSati = +this.datepicker
                .getTimepickerInsideDatepickerInterface()
                .defaultValue.split(':')[0];
            this._prethodniMinuti = +this.datepicker
                .getTimepickerInsideDatepickerInterface()
                .defaultValue.split(':')[1];
            // console.log(this._prethodniSati, this._prethodniMinuti);
        }
        else {
            this._prethodniSati = 0;
            this._prethodniMinuti = 0;
        }
        // console.log('DatepickerComponent init');
    };
    DatepickerComponent.prototype.ngOnDestroy = function () {
        // console.log('DatepickerComponent destroyed');
    };
    DatepickerComponent.ɵfac = function DatepickerComponent_Factory(t) { return new (t || DatepickerComponent)(ɵɵdirectiveInject(DeviceService)); };
    DatepickerComponent.ɵcmp = ɵɵdefineComponent({ type: DatepickerComponent, selectors: [["app-datepicker"]], inputs: { datepicker: "datepicker" }, features: [ɵɵProvidersFeature([
                {
                    provide: NG_VALUE_ACCESSOR,
                    multi: true,
                    useExisting: forwardRef(function () { return DatepickerComponent; }),
                },
                {
                    provide: DateAdapter,
                    useClass: MomentDateAdapter,
                    deps: [MAT_DATE_LOCALE],
                },
                { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
            ])], decls: 11, vars: 15, consts: [["fxLayout", "column", "fxLayoutAlign", "center center"], [3, "color", "appearance"], ["matInput", "", 3, "min", "max", "value", "matDatepickerFilter", "matDatepicker", "disabled", "blur", "dateInput", "dateChange"], ["input", ""], ["matSuffix", "", 3, "for", 4, "ngIf"], ["matPrefix", "", 3, "for", 4, "ngIf"], [3, "startView", "startAt", "touchUi"], ["picker", ""], [3, "appearance", 4, "ngIf"], ["matSuffix", "", 3, "for"], ["matDatepickerToggleIcon", "", 4, "ngIf"], ["matDatepickerToggleIcon", ""], ["matPrefix", "", 3, "for"], [3, "appearance"], [3, "buttonAlign", "disabled", "format", "min", "max", "defaultTime", "timeChanged"], ["matInput", "", "type", "timepicker", 3, "hidden"]], template: function DatepickerComponent_Template(rf, ctx) { if (rf & 1) {
            ɵɵelementStart(0, "section", 0);
            ɵɵelementStart(1, "mat-form-field", 1);
            ɵɵelementStart(2, "mat-label");
            ɵɵtext(3);
            ɵɵelementEnd();
            ɵɵelementStart(4, "input", 2, 3);
            ɵɵlistener("blur", function DatepickerComponent_Template_input_blur_4_listener() { return ctx.onBlur(); })("dateInput", function DatepickerComponent_Template_input_dateInput_4_listener($event) { return ctx.addEvent("input", $event); })("dateChange", function DatepickerComponent_Template_input_dateChange_4_listener($event) { return ctx.addEvent("change", $event); });
            ɵɵelementEnd();
            ɵɵtemplate(6, DatepickerComponent_mat_datepicker_toggle_6_Template, 2, 2, "mat-datepicker-toggle", 4);
            ɵɵtemplate(7, DatepickerComponent_mat_datepicker_toggle_7_Template, 2, 2, "mat-datepicker-toggle", 5);
            ɵɵelement(8, "mat-datepicker", 6, 7);
            ɵɵelementEnd();
            ɵɵtemplate(10, DatepickerComponent_mat_form_field_10_Template, 6, 9, "mat-form-field", 8);
            ɵɵelementEnd();
        } if (rf & 2) {
            var _r3 = ɵɵreference(9);
            ɵɵadvance(1);
            ɵɵproperty("color", ctx.datepicker.getColor())("appearance", ctx.datepicker.getAppearance());
            ɵɵadvance(2);
            ɵɵtextInterpolate(ctx.datepicker.getLabelName());
            ɵɵadvance(1);
            ɵɵproperty("min", ctx.datepicker.getMin() ? ctx.datepicker.getMin() : "")("max", ctx.datepicker.getMax() ? ctx.datepicker.getMax() : "")("value", ctx.datepicker.getDefaultValue())("matDatepickerFilter", ctx.disableDays)("matDatepicker", _r3)("disabled", ctx.datepicker.getDisabled());
            ɵɵadvance(2);
            ɵɵproperty("ngIf", ctx.datepicker.getToggleSideSuffix());
            ɵɵadvance(1);
            ɵɵproperty("ngIf", !ctx.datepicker.getToggleSideSuffix());
            ɵɵadvance(1);
            ɵɵproperty("startView", ctx.datepicker.getStartView())("startAt", ctx.datepicker.getStartAt() ? ctx.datepicker.getStartAt() : "")("touchUi", !ctx.deviceService.isDeviceDesktop());
            ɵɵadvance(2);
            ɵɵproperty("ngIf", ctx.datepicker.getTimepickerInsideDatepickerInterface());
        } }, directives: [DefaultLayoutDirective, DefaultLayoutAlignDirective, MatFormField, MatLabel, MatInput, MatDatepickerInput, NgIf, MatDatepicker, MatDatepickerToggle, MatSuffix, MatIcon, MatDatepickerToggleIcon, MatPrefix, NgxTimepickerFieldComponent], styles: ["", "mat-form-field[_ngcontent-%COMP%] {\n        min-width: 250px;\n    }"] });
    return DatepickerComponent;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(DatepickerComponent, [{
        type: Component,
        args: [{
                selector: 'app-datepicker',
                templateUrl: './datepicker.component.html',
                styleUrls: ['./datepicker.component.css'],
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        multi: true,
                        useExisting: forwardRef(function () { return DatepickerComponent; }),
                    },
                    {
                        provide: DateAdapter,
                        useClass: MomentDateAdapter,
                        deps: [MAT_DATE_LOCALE],
                    },
                    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
                ],
            }]
    }], function () { return [{ type: DeviceService }]; }, { datepicker: [{
            type: Input
        }] }); })();

var TimepickerComponent = /** @class */ (function () {
    function TimepickerComponent() {
        var _this = this;
        this.onChange = function () { };
        this.onTouched = function () { };
        this.onTimeChanged = function (time) {
            // console.log(time);
            _this.timepicker.setDefaultValue(time + ':00');
            _this.onChange(_this.timepicker.getDefaultValue());
            _this.onTouched();
        };
        this.onTimepickerClosed = function () {
            _this.onChange(_this.timepicker.getDefaultValue());
            _this.onTouched();
        };
    }
    TimepickerComponent.prototype.registerOnChange = function (fn) {
        this.onChange = fn;
    };
    TimepickerComponent.prototype.registerOnTouched = function (fn) {
        this.onTouched = fn;
    };
    TimepickerComponent.prototype.writeValue = function (value) {
        if (value) {
            this.timepicker.setDefaultValue(value);
        }
    };
    TimepickerComponent.prototype.ngOnInit = function () {
        // console.log('TimepickerComponent init');
    };
    TimepickerComponent.prototype.ngOnDestroy = function () {
        // console.log('TimepickerComponent destroyed');
    };
    TimepickerComponent.ɵfac = function TimepickerComponent_Factory(t) { return new (t || TimepickerComponent)(); };
    TimepickerComponent.ɵcmp = ɵɵdefineComponent({ type: TimepickerComponent, selectors: [["app-timepicker"]], inputs: { timepicker: "timepicker" }, features: [ɵɵProvidersFeature([
                {
                    provide: NG_VALUE_ACCESSOR,
                    multi: true,
                    useExisting: forwardRef(function () { return TimepickerComponent; }),
                },
            ])], decls: 7, vars: 9, consts: [["fxLayoutAlign", "center center"], [3, "appearance"], [3, "buttonAlign", "disabled", "format", "min", "max", "defaultTime", "timeChanged", "closed"], ["matInput", "", "type", "timepicker", 3, "hidden"]], template: function TimepickerComponent_Template(rf, ctx) { if (rf & 1) {
            ɵɵelementStart(0, "section", 0);
            ɵɵelementStart(1, "mat-form-field", 1);
            ɵɵelementStart(2, "mat-label");
            ɵɵtext(3);
            ɵɵelementEnd();
            ɵɵelement(4, "br");
            ɵɵelementStart(5, "ngx-timepicker-field", 2);
            ɵɵlistener("timeChanged", function TimepickerComponent_Template_ngx_timepicker_field_timeChanged_5_listener($event) { return ctx.onTimeChanged($event); })("closed", function TimepickerComponent_Template_ngx_timepicker_field_closed_5_listener() { return ctx.onTimepickerClosed(); });
            ɵɵelementEnd();
            ɵɵelement(6, "input", 3);
            ɵɵelementEnd();
            ɵɵelementEnd();
        } if (rf & 2) {
            ɵɵadvance(1);
            ɵɵproperty("appearance", ctx.timepicker.getAppearance());
            ɵɵadvance(2);
            ɵɵtextInterpolate(ctx.timepicker.getLabelName());
            ɵɵadvance(2);
            ɵɵproperty("buttonAlign", ctx.timepicker.getButtonAlign())("disabled", ctx.timepicker.getDisabled())("format", 24)("min", ctx.timepicker.getMin() ? ctx.timepicker.getMin() : undefined)("max", ctx.timepicker.getMax() ? ctx.timepicker.getMax() : undefined)("defaultTime", ctx.timepicker.getDefaultValue());
            ɵɵadvance(1);
            ɵɵproperty("hidden", true);
        } }, directives: [DefaultLayoutAlignDirective, MatFormField, MatLabel, NgxTimepickerFieldComponent, MatInput], styles: ["", "mat-form-field[_ngcontent-%COMP%] {\n        min-width: 250px;\n    }"] });
    return TimepickerComponent;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(TimepickerComponent, [{
        type: Component,
        args: [{
                selector: 'app-timepicker',
                templateUrl: './timepicker.component.html',
                styleUrls: ['./timepicker.component.css'],
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        multi: true,
                        useExisting: forwardRef(function () { return TimepickerComponent; }),
                    },
                ],
            }]
    }], function () { return []; }, { timepicker: [{
            type: Input
        }] }); })();

function TextareaComponent_mat_label_2_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-label");
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r0 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r0.textarea.getLabelName());
} }
function TextareaComponent_mat_icon_5_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-icon", 9);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r2 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r2.textarea.getMatPrefixImgText());
} }
function TextareaComponent_mat_icon_6_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-icon", 10);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r3 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r3.textarea.getMatSuffixImgText());
} }
function TextareaComponent_span_7_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "span", 9);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r4 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate1("", ctx_r4.textarea.getTextPrefix(), "\u00A0");
} }
function TextareaComponent_span_8_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "span", 10);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r5 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r5.textarea.getTextSuffix());
} }
function TextareaComponent_mat_hint_9_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-hint", 11);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r6 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r6.textarea.getLeftHintLabel());
} }
function TextareaComponent_mat_hint_10_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-hint", 12);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r7 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r7.textarea.getRightHintLabel());
} }
function TextareaComponent_mat_hint_11_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-hint", 12);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r8 = ɵɵnextContext();
    var tmp_0_0 = null;
    ɵɵadvance(1);
    ɵɵtextInterpolate3("", ((tmp_0_0 = ctx_r8.getMinLengthValidator()) == null ? null : tmp_0_0.length) ? ctx_r8.getMinLengthValidator().length + " / " : "", "", (tmp_0_0 = ctx_r8.textarea.getDefaultValue()) == null ? null : tmp_0_0.length, "", ((tmp_0_0 = ctx_r8.getMaxLengthValidator()) == null ? null : tmp_0_0.length) ? " / " + ctx_r8.getMaxLengthValidator().length : "", "");
} }
function TextareaComponent_mat_hint_12_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-hint", 12);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r9 = ɵɵnextContext();
    var tmp_0_0 = null;
    ɵɵadvance(1);
    ɵɵtextInterpolate2("", (tmp_0_0 = ctx_r9.textarea.getDefaultValue()) == null ? null : tmp_0_0.length, "", ((tmp_0_0 = ctx_r9.getMinLengthValidator()) == null ? null : tmp_0_0.length) ? " / " + ctx_r9.getMinLengthValidator().length : "", "");
} }
function TextareaComponent_mat_hint_13_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-hint", 12);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r10 = ɵɵnextContext();
    var tmp_0_0 = null;
    ɵɵadvance(1);
    ɵɵtextInterpolate2("", (tmp_0_0 = ctx_r10.textarea.getDefaultValue()) == null ? null : tmp_0_0.length, "", ((tmp_0_0 = ctx_r10.getMaxLengthValidator()) == null ? null : tmp_0_0.length) ? " / " + ctx_r10.getMaxLengthValidator().length : "", "");
} }
var TextareaComponent = /** @class */ (function () {
    function TextareaComponent() {
        var _this = this;
        this.onChange = function () { };
        this.onTouched = function () { };
        this.onKeyUp = function (textarea) {
            // console.log(textarea.value);
            _this.textarea.setDefaultValue(textarea.value);
            _this.onChange(_this.textarea.getDefaultValue());
            _this.onTouched();
        };
        this.getMinLengthValidator = function () {
            var theValidator;
            _this.textarea
                .getValidators()
                .forEach(function (validator) {
                if (validator.name === VALIDATOR_NAMES.MIN_LENGTH)
                    theValidator = validator;
            });
            return theValidator;
        };
        this.getMaxLengthValidator = function () {
            var theValidator;
            _this.textarea
                .getValidators()
                .forEach(function (validator) {
                if (validator.name === VALIDATOR_NAMES.MAX_LENGTH)
                    theValidator = validator;
            });
            return theValidator;
        };
    }
    TextareaComponent.prototype.registerOnChange = function (fn) {
        this.onChange = fn;
    };
    TextareaComponent.prototype.registerOnTouched = function (fn) {
        this.onTouched = fn;
    };
    TextareaComponent.prototype.writeValue = function (value) {
        if (value) {
            this.textarea.setDefaultValue(value);
        }
    };
    TextareaComponent.prototype.ngOnInit = function () {
        // console.log('TextareaComponent init');
    };
    TextareaComponent.prototype.ngOnDestroy = function () {
        // console.log('TextareaComponent destroyed');
    };
    TextareaComponent.ɵfac = function TextareaComponent_Factory(t) { return new (t || TextareaComponent)(); };
    TextareaComponent.ɵcmp = ɵɵdefineComponent({ type: TextareaComponent, selectors: [["app-textarea"]], inputs: { textarea: "textarea" }, features: [ɵɵProvidersFeature([
                {
                    provide: NG_VALUE_ACCESSOR,
                    multi: true,
                    useExisting: forwardRef(function () { return TextareaComponent; }),
                },
            ])], decls: 14, vars: 18, consts: [["fxLayoutAlign", "center center"], [3, "appearance", "hideRequiredMarker"], [4, "ngIf"], ["matInput", "", "cdkTextareaAutosize", "", 3, "type", "placeholder", "value", "disabled", "readonly", "required", "keyup", "blur"], ["textareaField", ""], ["matPrefix", "", 4, "ngIf"], ["matSuffix", "", 4, "ngIf"], ["align", "start", 4, "ngIf"], ["align", "end", 4, "ngIf"], ["matPrefix", ""], ["matSuffix", ""], ["align", "start"], ["align", "end"]], template: function TextareaComponent_Template(rf, ctx) { if (rf & 1) {
            var _r11 = ɵɵgetCurrentView();
            ɵɵelementStart(0, "section", 0);
            ɵɵelementStart(1, "mat-form-field", 1);
            ɵɵtemplate(2, TextareaComponent_mat_label_2_Template, 2, 1, "mat-label", 2);
            ɵɵelementStart(3, "textarea", 3, 4);
            ɵɵlistener("keyup", function TextareaComponent_Template_textarea_keyup_3_listener() { ɵɵrestoreView(_r11); var _r1 = ɵɵreference(4); return ctx.onKeyUp(_r1); })("blur", function TextareaComponent_Template_textarea_blur_3_listener() { ɵɵrestoreView(_r11); var _r1 = ɵɵreference(4); return ctx.onKeyUp(_r1); });
            ɵɵelementEnd();
            ɵɵtemplate(5, TextareaComponent_mat_icon_5_Template, 2, 1, "mat-icon", 5);
            ɵɵtemplate(6, TextareaComponent_mat_icon_6_Template, 2, 1, "mat-icon", 6);
            ɵɵtemplate(7, TextareaComponent_span_7_Template, 2, 1, "span", 5);
            ɵɵtemplate(8, TextareaComponent_span_8_Template, 2, 1, "span", 6);
            ɵɵtemplate(9, TextareaComponent_mat_hint_9_Template, 2, 1, "mat-hint", 7);
            ɵɵtemplate(10, TextareaComponent_mat_hint_10_Template, 2, 1, "mat-hint", 8);
            ɵɵtemplate(11, TextareaComponent_mat_hint_11_Template, 2, 3, "mat-hint", 8);
            ɵɵtemplate(12, TextareaComponent_mat_hint_12_Template, 2, 2, "mat-hint", 8);
            ɵɵtemplate(13, TextareaComponent_mat_hint_13_Template, 2, 2, "mat-hint", 8);
            ɵɵelementEnd();
            ɵɵelementEnd();
        } if (rf & 2) {
            ɵɵadvance(1);
            ɵɵproperty("appearance", ctx.textarea.getAppearance())("hideRequiredMarker", ctx.textarea.getRequired().hideRequiredMarker);
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.textarea.getLabelName());
            ɵɵadvance(1);
            ɵɵproperty("type", ctx.textarea.getFormFieldInputType())("placeholder", ctx.textarea.getPlaceholder() ? ctx.textarea.getPlaceholder() : "")("value", ctx.textarea.getDefaultValue())("disabled", ctx.textarea.getDisabled())("readonly", ctx.textarea.getReadonly())("required", ctx.textarea.getRequired().required);
            ɵɵadvance(2);
            ɵɵproperty("ngIf", ctx.textarea.getMatPrefixImgText());
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.textarea.getMatSuffixImgText());
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.textarea.getTextPrefix());
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.textarea.getTextSuffix());
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.textarea.getLeftHintLabel());
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.textarea.getRightHintLabel());
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.textarea.getShowHintAboutMinMaxLength() && ctx.getMinLengthValidator() && ctx.getMaxLengthValidator());
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.textarea.getShowHintAboutMinMaxLength() && ctx.getMinLengthValidator() && !ctx.getMaxLengthValidator());
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.textarea.getShowHintAboutMinMaxLength() && !ctx.getMinLengthValidator() && ctx.getMaxLengthValidator());
        } }, directives: [DefaultLayoutAlignDirective, MatFormField, NgIf, MatInput, CdkTextareaAutosize, MatLabel, MatIcon, MatPrefix, MatSuffix, MatHint], styles: ["", ".cursor[_ngcontent-%COMP%] {\n        cursor: pointer;\n    }\n\n    input[type=\"file\"][_ngcontent-%COMP%] {\n        display: none;\n    }\n\n    h3[_ngcontent-%COMP%] {\n        font-size: inherit;\n        margin: 0;\n        font-weight: inherit;\n    }\n\n    mat-form-field[_ngcontent-%COMP%] {\n        min-width: 250px;\n    }"] });
    return TextareaComponent;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(TextareaComponent, [{
        type: Component,
        args: [{
                selector: "app-textarea",
                templateUrl: "./textarea.component.html",
                styleUrls: ["./textarea.component.css"],
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        multi: true,
                        useExisting: forwardRef(function () { return TextareaComponent; }),
                    },
                ],
            }]
    }], function () { return []; }, { textarea: [{
            type: Input
        }] }); })();

function SelectComponent_mat_label_2_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-label");
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r0 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r0.select.getLabelName());
} }
function SelectComponent_mat_select_trigger_5_span_2_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "span");
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r10 = ɵɵnextContext(2);
    var tmp_0_0 = null;
    ɵɵadvance(1);
    ɵɵtextInterpolate2(" (+", ctx_r10.select.getDefaultValue().length - 1, " ", ((tmp_0_0 = ctx_r10.select.getDefaultValue()) == null ? null : tmp_0_0.length) === 2 ? "other" : "others", ") ");
} }
function SelectComponent_mat_select_trigger_5_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-select-trigger");
    ɵɵtext(1);
    ɵɵtemplate(2, SelectComponent_mat_select_trigger_5_span_2_Template, 2, 2, "span", 2);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r2 = ɵɵnextContext();
    var tmp_1_0 = null;
    var currVal_1 = ((tmp_1_0 = ctx_r2.select.getDefaultValue()) == null ? null : tmp_1_0.length) > 1;
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", ctx_r2.select.getDefaultValue() ? ctx_r2.select.getDefaultValue()[0] : "", " ");
    ɵɵadvance(1);
    ɵɵproperty("ngIf", currVal_1);
} }
function SelectComponent_section_6_mat_option_1_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-option", 12);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var optionValue_r11 = ɵɵnextContext().$implicit;
    ɵɵproperty("value", optionValue_r11.value)("disabled", optionValue_r11.disabled);
    ɵɵadvance(1);
    ɵɵtextInterpolate1("", optionValue_r11.textToShow, " ");
} }
function SelectComponent_section_6_mat_optgroup_2_mat_option_1_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-option", 12);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var oV_r16 = ctx.$implicit;
    ɵɵproperty("value", oV_r16.value)("disabled", oV_r16.disabled);
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", oV_r16.textToShow, " ");
} }
function SelectComponent_section_6_mat_optgroup_2_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-optgroup", 13);
    ɵɵtemplate(1, SelectComponent_section_6_mat_optgroup_2_mat_option_1_Template, 2, 3, "mat-option", 14);
    ɵɵelementEnd();
} if (rf & 2) {
    var optionValue_r11 = ɵɵnextContext().$implicit;
    ɵɵproperty("label", optionValue_r11.labelName)("disabled", optionValue_r11.disabled);
    ɵɵadvance(1);
    ɵɵproperty("ngForOf", optionValue_r11.optionValues);
} }
function SelectComponent_section_6_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "section");
    ɵɵtemplate(1, SelectComponent_section_6_mat_option_1_Template, 2, 3, "mat-option", 10);
    ɵɵtemplate(2, SelectComponent_section_6_mat_optgroup_2_Template, 2, 3, "mat-optgroup", 11);
    ɵɵelementEnd();
} if (rf & 2) {
    var optionValue_r11 = ctx.$implicit;
    ɵɵadvance(1);
    ɵɵproperty("ngIf", !optionValue_r11.optionValues);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", optionValue_r11.optionValues && optionValue_r11.labelName);
} }
function SelectComponent_mat_icon_7_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-icon", 15);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r4 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r4.select.getMatPrefixImgText());
} }
function SelectComponent_mat_icon_8_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-icon", 16);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r5 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r5.select.getMatSuffixImgText());
} }
function SelectComponent_span_9_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "span", 15);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r6 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate1("", ctx_r6.select.getTextPrefix(), "\u00A0");
} }
function SelectComponent_span_10_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "span", 16);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r7 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r7.select.getTextSuffix());
} }
function SelectComponent_mat_hint_11_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-hint", 17);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r8 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r8.select.getLeftHintLabel());
} }
function SelectComponent_mat_hint_12_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-hint", 18);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r9 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r9.select.getRightHintLabel());
} }
var SelectComponent = /** @class */ (function () {
    function SelectComponent() {
        var _this = this;
        this.onChange = function () { };
        this.onTouched = function () { };
        this.onSelectionChange = function (select) {
            // console.log(select.value);
            _this.select.setDefaultValue(select.value);
            _this.onChange(_this.select.getDefaultValue());
            _this.onTouched();
        };
        this.onBlur = function () {
            _this.onChange(_this.select.getDefaultValue());
            _this.onTouched();
        };
    }
    SelectComponent.prototype.registerOnChange = function (fn) {
        this.onChange = fn;
    };
    SelectComponent.prototype.registerOnTouched = function (fn) {
        this.onTouched = fn;
    };
    SelectComponent.prototype.writeValue = function (value) {
        if (value) {
            this.select.setDefaultValue(value);
        }
    };
    SelectComponent.prototype.ngOnInit = function () {
        // console.log('SelectComponent init');
    };
    SelectComponent.prototype.ngOnDestroy = function () {
        // console.log('SelectComponent destroyed');
    };
    SelectComponent.ɵfac = function SelectComponent_Factory(t) { return new (t || SelectComponent)(); };
    SelectComponent.ɵcmp = ɵɵdefineComponent({ type: SelectComponent, selectors: [["app-select"]], inputs: { select: "select" }, features: [ɵɵProvidersFeature([
                {
                    provide: NG_VALUE_ACCESSOR,
                    multi: true,
                    useExisting: forwardRef(function () { return SelectComponent; }),
                },
            ])], decls: 13, vars: 15, consts: [["fxLayoutAlign", "center center"], [3, "appearance", "hideRequiredMarker"], [4, "ngIf"], [3, "value", "multiple", "required", "disabled", "selectionChange", "blur"], ["matSelect", ""], [4, "ngFor", "ngForOf"], ["matPrefix", "", 4, "ngIf"], ["matSuffix", "", 4, "ngIf"], ["align", "start", 4, "ngIf"], ["align", "end", 4, "ngIf"], [3, "value", "disabled", 4, "ngIf"], [3, "label", "disabled", 4, "ngIf"], [3, "value", "disabled"], [3, "label", "disabled"], [3, "value", "disabled", 4, "ngFor", "ngForOf"], ["matPrefix", ""], ["matSuffix", ""], ["align", "start"], ["align", "end"]], template: function SelectComponent_Template(rf, ctx) { if (rf & 1) {
            var _r18 = ɵɵgetCurrentView();
            ɵɵelementStart(0, "section", 0);
            ɵɵelementStart(1, "mat-form-field", 1);
            ɵɵtemplate(2, SelectComponent_mat_label_2_Template, 2, 1, "mat-label", 2);
            ɵɵelementStart(3, "mat-select", 3, 4);
            ɵɵlistener("selectionChange", function SelectComponent_Template_mat_select_selectionChange_3_listener() { ɵɵrestoreView(_r18); var _r1 = ɵɵreference(4); return ctx.onSelectionChange(_r1); })("blur", function SelectComponent_Template_mat_select_blur_3_listener() { return ctx.onBlur(); });
            ɵɵtemplate(5, SelectComponent_mat_select_trigger_5_Template, 3, 2, "mat-select-trigger", 2);
            ɵɵtemplate(6, SelectComponent_section_6_Template, 3, 2, "section", 5);
            ɵɵelementEnd();
            ɵɵtemplate(7, SelectComponent_mat_icon_7_Template, 2, 1, "mat-icon", 6);
            ɵɵtemplate(8, SelectComponent_mat_icon_8_Template, 2, 1, "mat-icon", 7);
            ɵɵtemplate(9, SelectComponent_span_9_Template, 2, 1, "span", 6);
            ɵɵtemplate(10, SelectComponent_span_10_Template, 2, 1, "span", 7);
            ɵɵtemplate(11, SelectComponent_mat_hint_11_Template, 2, 1, "mat-hint", 8);
            ɵɵtemplate(12, SelectComponent_mat_hint_12_Template, 2, 1, "mat-hint", 9);
            ɵɵelementEnd();
            ɵɵelementEnd();
        } if (rf & 2) {
            ɵɵadvance(1);
            ɵɵproperty("appearance", ctx.select.getAppearance())("hideRequiredMarker", ctx.select.getRequired().hideRequiredMarker);
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.select.getLabelName());
            ɵɵadvance(1);
            ɵɵproperty("value", ctx.select.getDefaultValue())("multiple", ctx.select.getMultiple())("required", ctx.select.getRequired().required)("disabled", ctx.select.getDisabled());
            ɵɵadvance(2);
            ɵɵproperty("ngIf", ctx.select.getMatSelectTriggerOn());
            ɵɵadvance(1);
            ɵɵproperty("ngForOf", ctx.select.getOptionValues());
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.select.getMatPrefixImgText());
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.select.getMatSuffixImgText());
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.select.getTextPrefix());
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.select.getTextSuffix());
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.select.getLeftHintLabel());
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.select.getRightHintLabel());
        } }, directives: [DefaultLayoutAlignDirective, MatFormField, NgIf, MatSelect, NgForOf, MatLabel, MatSelectTrigger, MatOption, MatOptgroup, MatIcon, MatPrefix, MatSuffix, MatHint], styles: ["", ".cursor[_ngcontent-%COMP%] {\n        cursor: pointer;\n    }\n\n    input[type=\"file\"][_ngcontent-%COMP%] {\n        display: none;\n    }\n\n    h3[_ngcontent-%COMP%] {\n        font-size: inherit;\n        margin: 0;\n        font-weight: inherit;\n    }\n\n    mat-form-field[_ngcontent-%COMP%] {\n        min-width: 250px;\n    }"] });
    return SelectComponent;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(SelectComponent, [{
        type: Component,
        args: [{
                selector: 'app-select',
                templateUrl: './select.component.html',
                styleUrls: ['./select.component.css'],
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        multi: true,
                        useExisting: forwardRef(function () { return SelectComponent; }),
                    },
                ],
            }]
    }], function () { return []; }, { select: [{
            type: Input
        }] }); })();

var SliderComponent = /** @class */ (function () {
    function SliderComponent() {
        var _this = this;
        this.onChange = function () { };
        this.onTouched = function () { };
        this.onValueChange = function (slider) {
            // console.log(slider);
            _this.slider.setDefaultValue(slider.value);
            _this.onChange(_this.slider.getDefaultValue());
            _this.onTouched();
        };
        this.onBlur = function () {
            _this.onChange(_this.slider.getDefaultValue());
            _this.onTouched();
        };
    }
    SliderComponent.prototype.registerOnChange = function (fn) {
        this.onChange = fn;
    };
    SliderComponent.prototype.registerOnTouched = function (fn) {
        this.onTouched = fn;
    };
    SliderComponent.prototype.writeValue = function (value) {
        if (value) {
            this.slider.setDefaultValue(value);
        }
    };
    SliderComponent.prototype.ngOnInit = function () {
        // console.log('SliderComponent init');
    };
    SliderComponent.prototype.ngOnDestroy = function () {
        // console.log('SliderComponent destroyed');
    };
    SliderComponent.ɵfac = function SliderComponent_Factory(t) { return new (t || SliderComponent)(); };
    SliderComponent.ɵcmp = ɵɵdefineComponent({ type: SliderComponent, selectors: [["app-slider"]], inputs: { slider: "slider" }, features: [ɵɵProvidersFeature([
                {
                    provide: NG_VALUE_ACCESSOR,
                    multi: true,
                    useExisting: forwardRef(function () { return SliderComponent; }),
                },
            ])], decls: 3, vars: 9, consts: [["fxLayoutAlign", "center center"], [3, "value", "min", "max", "disabled", "invert", "step", "tickInterval", "thumbLabel", "vertical", "change", "blur"], ["matSlider", ""]], template: function SliderComponent_Template(rf, ctx) { if (rf & 1) {
            var _r1 = ɵɵgetCurrentView();
            ɵɵelementStart(0, "section", 0);
            ɵɵelementStart(1, "mat-slider", 1, 2);
            ɵɵlistener("change", function SliderComponent_Template_mat_slider_change_1_listener() { ɵɵrestoreView(_r1); var _r0 = ɵɵreference(2); return ctx.onValueChange(_r0); })("blur", function SliderComponent_Template_mat_slider_blur_1_listener() { return ctx.onBlur(); });
            ɵɵelementEnd();
            ɵɵelementEnd();
        } if (rf & 2) {
            ɵɵadvance(1);
            ɵɵproperty("value", ctx.slider.getDefaultValue())("min", ctx.slider.getMinValue() ? ctx.slider.getMinValue() : undefined)("max", ctx.slider.getMaxValue() ? ctx.slider.getMaxValue() : undefined)("disabled", ctx.slider.getDisabled())("invert", ctx.slider.getInvert())("step", ctx.slider.getStep())("tickInterval", ctx.slider.getTickInterval())("thumbLabel", ctx.slider.getThumbLabel())("vertical", ctx.slider.getVertical());
        } }, directives: [DefaultLayoutAlignDirective, MatSlider], styles: ["", "mat-slider[_ngcontent-%COMP%] {\n        min-width: 250px;\n    }\n\n    section[_ngcontent-%COMP%] {\n        min-height: 70px;\n    }"] });
    return SliderComponent;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(SliderComponent, [{
        type: Component,
        args: [{
                selector: 'app-slider',
                templateUrl: './slider.component.html',
                styleUrls: ['./slider.component.css'],
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        multi: true,
                        useExisting: forwardRef(function () { return SliderComponent; }),
                    },
                ],
            }]
    }], function () { return []; }, { slider: [{
            type: Input
        }] }); })();

var SlideToggleComponent = /** @class */ (function () {
    function SlideToggleComponent() {
        var _this = this;
        this.onChange = function () { };
        this.onTouched = function () { };
        this.onValueChange = function (slideToggle) {
            console.log(slideToggle);
            _this.slideToggle.setDefaultValue(slideToggle.checked);
            _this.onChange(_this.slideToggle.getDefaultValue());
            _this.onTouched();
        };
    }
    SlideToggleComponent.prototype.registerOnChange = function (fn) {
        this.onChange = fn;
    };
    SlideToggleComponent.prototype.registerOnTouched = function (fn) {
        this.onTouched = fn;
    };
    SlideToggleComponent.prototype.writeValue = function (value) {
        if (value) {
            this.slideToggle.setDefaultValue(value);
        }
    };
    SlideToggleComponent.prototype.ngOnInit = function () {
        // console.log('SlideToggleComponent init');
    };
    SlideToggleComponent.prototype.ngOnDestroy = function () {
        // console.log('SlideToggleComponent destroyed');
    };
    SlideToggleComponent.ɵfac = function SlideToggleComponent_Factory(t) { return new (t || SlideToggleComponent)(); };
    SlideToggleComponent.ɵcmp = ɵɵdefineComponent({ type: SlideToggleComponent, selectors: [["app-slide-toggle"]], inputs: { slideToggle: "slideToggle" }, features: [ɵɵProvidersFeature([
                {
                    provide: NG_VALUE_ACCESSOR,
                    multi: true,
                    useExisting: forwardRef(function () { return SlideToggleComponent; }),
                },
            ])], decls: 4, vars: 4, consts: [["fxLayoutAlign", "center center"], [3, "color", "checked", "disabled", "change"], ["matSlideToggle", ""]], template: function SlideToggleComponent_Template(rf, ctx) { if (rf & 1) {
            var _r1 = ɵɵgetCurrentView();
            ɵɵelementStart(0, "section", 0);
            ɵɵelementStart(1, "mat-slide-toggle", 1, 2);
            ɵɵlistener("change", function SlideToggleComponent_Template_mat_slide_toggle_change_1_listener() { ɵɵrestoreView(_r1); var _r0 = ɵɵreference(2); return ctx.onValueChange(_r0); });
            ɵɵtext(3);
            ɵɵelementEnd();
            ɵɵelementEnd();
        } if (rf & 2) {
            ɵɵadvance(1);
            ɵɵproperty("color", ctx.slideToggle.getColor())("checked", ctx.slideToggle.getDefaultValue())("disabled", ctx.slideToggle.getDisabled());
            ɵɵadvance(2);
            ɵɵtextInterpolate(ctx.slideToggle.getContext());
        } }, directives: [DefaultLayoutAlignDirective, MatSlideToggle], styles: ["", ""] });
    return SlideToggleComponent;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(SlideToggleComponent, [{
        type: Component,
        args: [{
                selector: 'app-slide-toggle',
                templateUrl: './slide-toggle.component.html',
                styleUrls: ['./slide-toggle.component.css'],
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        multi: true,
                        useExisting: forwardRef(function () { return SlideToggleComponent; }),
                    },
                ],
            }]
    }], function () { return []; }, { slideToggle: [{
            type: Input
        }] }); })();

function ButtonComponent_button_1_Template(rf, ctx) { if (rf & 1) {
    var _r8 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "button", 8);
    ɵɵlistener("click", function ButtonComponent_button_1_Template_button_click_0_listener() { ɵɵrestoreView(_r8); var ctx_r7 = ɵɵnextContext(); return ctx_r7.button.getFunctionToExecute() ? ctx_r7.button.getFunctionToExecute()() : undefined; });
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r0 = ɵɵnextContext();
    ɵɵproperty("type", ctx_r0.button.getButtonType())("disabled", ctx_r0.button.getDisabled())("color", ctx_r0.button.getColor());
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r0.button.getContext());
} }
function ButtonComponent_button_2_Template(rf, ctx) { if (rf & 1) {
    var _r10 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "button", 9);
    ɵɵlistener("click", function ButtonComponent_button_2_Template_button_click_0_listener() { ɵɵrestoreView(_r10); var ctx_r9 = ɵɵnextContext(); return ctx_r9.button.getFunctionToExecute() ? ctx_r9.button.getFunctionToExecute()() : undefined; });
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r1 = ɵɵnextContext();
    ɵɵproperty("type", ctx_r1.button.getButtonType())("disabled", ctx_r1.button.getDisabled())("color", ctx_r1.button.getColor());
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r1.button.getContext());
} }
function ButtonComponent_button_3_Template(rf, ctx) { if (rf & 1) {
    var _r12 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "button", 10);
    ɵɵlistener("click", function ButtonComponent_button_3_Template_button_click_0_listener() { ɵɵrestoreView(_r12); var ctx_r11 = ɵɵnextContext(); return ctx_r11.button.getFunctionToExecute() ? ctx_r11.button.getFunctionToExecute()() : undefined; });
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r2 = ɵɵnextContext();
    ɵɵproperty("type", ctx_r2.button.getButtonType())("disabled", ctx_r2.button.getDisabled())("color", ctx_r2.button.getColor());
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r2.button.getContext());
} }
function ButtonComponent_button_4_Template(rf, ctx) { if (rf & 1) {
    var _r14 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "button", 11);
    ɵɵlistener("click", function ButtonComponent_button_4_Template_button_click_0_listener() { ɵɵrestoreView(_r14); var ctx_r13 = ɵɵnextContext(); return ctx_r13.button.getFunctionToExecute() ? ctx_r13.button.getFunctionToExecute()() : undefined; });
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r3 = ɵɵnextContext();
    ɵɵproperty("type", ctx_r3.button.getButtonType())("disabled", ctx_r3.button.getDisabled())("color", ctx_r3.button.getColor());
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r3.button.getContext());
} }
function ButtonComponent_button_5_Template(rf, ctx) { if (rf & 1) {
    var _r16 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "button", 12);
    ɵɵlistener("click", function ButtonComponent_button_5_Template_button_click_0_listener() { ɵɵrestoreView(_r16); var ctx_r15 = ɵɵnextContext(); return ctx_r15.button.getFunctionToExecute() ? ctx_r15.button.getFunctionToExecute()() : undefined; });
    ɵɵelementStart(1, "mat-icon");
    ɵɵtext(2);
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r4 = ɵɵnextContext();
    ɵɵproperty("type", ctx_r4.button.getButtonType())("disabled", ctx_r4.button.getDisabled())("color", ctx_r4.button.getColor());
    ɵɵadvance(2);
    ɵɵtextInterpolate(ctx_r4.button.getMatIconString());
} }
function ButtonComponent_button_6_Template(rf, ctx) { if (rf & 1) {
    var _r18 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "button", 13);
    ɵɵlistener("click", function ButtonComponent_button_6_Template_button_click_0_listener() { ɵɵrestoreView(_r18); var ctx_r17 = ɵɵnextContext(); return ctx_r17.button.getFunctionToExecute() ? ctx_r17.button.getFunctionToExecute()() : undefined; });
    ɵɵelementStart(1, "mat-icon");
    ɵɵtext(2);
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r5 = ɵɵnextContext();
    ɵɵproperty("type", ctx_r5.button.getButtonType())("disabled", ctx_r5.button.getDisabled())("color", ctx_r5.button.getColor());
    ɵɵadvance(2);
    ɵɵtextInterpolate(ctx_r5.button.getMatIconString());
} }
function ButtonComponent_button_7_Template(rf, ctx) { if (rf & 1) {
    var _r20 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "button", 14);
    ɵɵlistener("click", function ButtonComponent_button_7_Template_button_click_0_listener() { ɵɵrestoreView(_r20); var ctx_r19 = ɵɵnextContext(); return ctx_r19.button.getFunctionToExecute() ? ctx_r19.button.getFunctionToExecute()() : undefined; });
    ɵɵelementStart(1, "mat-icon");
    ɵɵtext(2);
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r6 = ɵɵnextContext();
    ɵɵproperty("type", ctx_r6.button.getButtonType())("disabled", ctx_r6.button.getDisabled())("color", ctx_r6.button.getColor());
    ɵɵadvance(2);
    ɵɵtextInterpolate(ctx_r6.button.getMatIconString());
} }
var ButtonComponent = /** @class */ (function () {
    function ButtonComponent() {
        this.onChange = function () { };
        this.onTouched = function () { };
    }
    ButtonComponent.prototype.registerOnChange = function (fn) {
        this.onChange = fn;
    };
    ButtonComponent.prototype.registerOnTouched = function (fn) {
        this.onTouched = fn;
    };
    ButtonComponent.prototype.writeValue = function (value) {
        if (value) {
            this.button.setDefaultValue(value);
        }
    };
    ButtonComponent.prototype.ngOnInit = function () {
        // console.log('ButtonComponent init');
    };
    ButtonComponent.prototype.ngOnDestroy = function () {
        // console.log('ButtonComponent destroyed');
    };
    ButtonComponent.ɵfac = function ButtonComponent_Factory(t) { return new (t || ButtonComponent)(); };
    ButtonComponent.ɵcmp = ɵɵdefineComponent({ type: ButtonComponent, selectors: [["app-button"]], inputs: { button: "button" }, features: [ɵɵProvidersFeature([
                {
                    provide: NG_VALUE_ACCESSOR,
                    multi: true,
                    useExisting: forwardRef(function () { return ButtonComponent; }),
                },
            ])], decls: 8, vars: 7, consts: [["fxLayoutAlign", "center center"], ["mat-button", "", 3, "type", "disabled", "color", "click", 4, "ngIf"], ["mat-raised-button", "", 3, "type", "disabled", "color", "click", 4, "ngIf"], ["mat-stroked-button", "", 3, "type", "disabled", "color", "click", 4, "ngIf"], ["mat-flat-button", "", 3, "type", "disabled", "color", "click", 4, "ngIf"], ["mat-icon-button", "", 3, "type", "disabled", "color", "click", 4, "ngIf"], ["mat-fab", "", 3, "type", "disabled", "color", "click", 4, "ngIf"], ["mat-mini-fab", "", 3, "type", "disabled", "color", "click", 4, "ngIf"], ["mat-button", "", 3, "type", "disabled", "color", "click"], ["mat-raised-button", "", 3, "type", "disabled", "color", "click"], ["mat-stroked-button", "", 3, "type", "disabled", "color", "click"], ["mat-flat-button", "", 3, "type", "disabled", "color", "click"], ["mat-icon-button", "", 3, "type", "disabled", "color", "click"], ["mat-fab", "", 3, "type", "disabled", "color", "click"], ["mat-mini-fab", "", 3, "type", "disabled", "color", "click"]], template: function ButtonComponent_Template(rf, ctx) { if (rf & 1) {
            ɵɵelementStart(0, "section", 0);
            ɵɵtemplate(1, ButtonComponent_button_1_Template, 2, 4, "button", 1);
            ɵɵtemplate(2, ButtonComponent_button_2_Template, 2, 4, "button", 2);
            ɵɵtemplate(3, ButtonComponent_button_3_Template, 2, 4, "button", 3);
            ɵɵtemplate(4, ButtonComponent_button_4_Template, 2, 4, "button", 4);
            ɵɵtemplate(5, ButtonComponent_button_5_Template, 3, 4, "button", 5);
            ɵɵtemplate(6, ButtonComponent_button_6_Template, 3, 4, "button", 6);
            ɵɵtemplate(7, ButtonComponent_button_7_Template, 3, 4, "button", 7);
            ɵɵelementEnd();
        } if (rf & 2) {
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.button.getMatButtonType() == "BASIC");
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.button.getMatButtonType() == "RAISED");
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.button.getMatButtonType() == "STROKED");
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.button.getMatButtonType() == "FLAT");
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.button.getMatButtonType() == "ICON");
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.button.getMatButtonType() == "FAB");
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.button.getMatButtonType() == "MINI_FAB");
        } }, directives: [DefaultLayoutAlignDirective, NgIf, MatButton, MatIcon], styles: ["", ""] });
    return ButtonComponent;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(ButtonComponent, [{
        type: Component,
        args: [{
                selector: 'app-button',
                templateUrl: './button.component.html',
                styleUrls: ['./button.component.css'],
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        multi: true,
                        useExisting: forwardRef(function () { return ButtonComponent; }),
                    },
                ],
            }]
    }], function () { return []; }, { button: [{
            type: Input
        }] }); })();

var AutocompleteComponent = /** @class */ (function () {
    function AutocompleteComponent() {
        this.onChange = function () { };
        this.onTouched = function () { };
    }
    AutocompleteComponent.prototype.registerOnChange = function (fn) {
        this.onChange = fn;
    };
    AutocompleteComponent.prototype.registerOnTouched = function (fn) {
        this.onTouched = fn;
    };
    AutocompleteComponent.prototype.writeValue = function (value) {
        if (value) {
            this.autocomplete.setDefaultValue(value);
        }
    };
    AutocompleteComponent.prototype.ngOnInit = function () {
        // console.log(this.autocomplete);
        var _this = this;
        this.autocomplete.getValueSubject().subscribe(function (optionItem) {
            _this.onChange(optionItem);
            _this.onTouched();
        });
        this.autocompleteInterface = {
            pageDataSource: this.autocomplete.getPageDataSource(),
            criteria: this.autocomplete.getCriteria(),
            inputPlaceHolder: this.autocomplete.getInputPlaceholder(),
            sortingOptions: this.autocomplete.getSortingOptions(),
            disabled: this.autocomplete.getDisabled(),
            appearance: this.autocomplete.getAppearance(),
            valueSubject: this.autocomplete.getValueSubject(),
            value: this.autocomplete.getValue(),
            hiddenSubject: this.autocomplete.getHiddenSubject(),
        };
        // console.log('AutocompleteComponent init');
    };
    AutocompleteComponent.prototype.ngOnDestroy = function () {
        // console.log('AutocompleteComponent destroyed');
    };
    AutocompleteComponent.ɵfac = function AutocompleteComponent_Factory(t) { return new (t || AutocompleteComponent)(); };
    AutocompleteComponent.ɵcmp = ɵɵdefineComponent({ type: AutocompleteComponent, selectors: [["app-autocomplete"]], inputs: { autocomplete: "autocomplete" }, features: [ɵɵProvidersFeature([
                {
                    provide: NG_VALUE_ACCESSOR,
                    multi: true,
                    useExisting: forwardRef(function () { return AutocompleteComponent; }),
                },
            ])], decls: 2, vars: 1, consts: [["fxLayout", "column", "fxLayoutAlign", "center center", "fxLayoutGap", "2vw"], [3, "autocomplete"]], template: function AutocompleteComponent_Template(rf, ctx) { if (rf & 1) {
            ɵɵelementStart(0, "section", 0);
            ɵɵelement(1, "lib-autocomplete", 1);
            ɵɵelementEnd();
        } if (rf & 2) {
            ɵɵadvance(1);
            ɵɵproperty("autocomplete", ctx.autocompleteInterface);
        } }, directives: [DefaultLayoutDirective, DefaultLayoutAlignDirective, DefaultLayoutGapDirective, AutocompleteComponent$1], styles: [""] });
    return AutocompleteComponent;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(AutocompleteComponent, [{
        type: Component,
        args: [{
                selector: 'app-autocomplete',
                templateUrl: './autocomplete.component.html',
                styleUrls: ['./autocomplete.component.css'],
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        multi: true,
                        useExisting: forwardRef(function () { return AutocompleteComponent; }),
                    },
                ],
            }]
    }], function () { return []; }, { autocomplete: [{
            type: Input
        }] }); })();

function MyAngularFormComponent_section_2_section_1_app_radio_button_1_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "app-radio-button", 27);
} if (rf & 2) {
    var rowItem_r7 = ɵɵnextContext().$implicit;
    ɵɵproperty("formControlName", rowItem_r7.getControlName())("radioButtonGroup", rowItem_r7);
} }
function MyAngularFormComponent_section_2_section_1_app_checkbox_2_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "app-checkbox", 28);
} if (rf & 2) {
    var rowItem_r7 = ɵɵnextContext().$implicit;
    ɵɵproperty("formControlName", rowItem_r7.getControlName())("checkbox", rowItem_r7);
} }
function MyAngularFormComponent_section_2_section_1_app_form_field_input_text_3_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "app-form-field-input-text", 29);
} if (rf & 2) {
    var rowItem_r7 = ɵɵnextContext().$implicit;
    ɵɵproperty("formControlName", rowItem_r7.getControlName())("formFieldInputText", rowItem_r7);
} }
function MyAngularFormComponent_section_2_section_1_app_form_field_input_password_4_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "app-form-field-input-password", 30);
} if (rf & 2) {
    var rowItem_r7 = ɵɵnextContext().$implicit;
    ɵɵproperty("formControlName", rowItem_r7.getControlName())("formFieldInputPassword", rowItem_r7);
} }
function MyAngularFormComponent_section_2_section_1_app_form_field_input_number_5_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "app-form-field-input-number", 31);
} if (rf & 2) {
    var rowItem_r7 = ɵɵnextContext().$implicit;
    ɵɵproperty("formControlName", rowItem_r7.getControlName())("formFieldInputNumber", rowItem_r7);
} }
function MyAngularFormComponent_section_2_section_1_app_form_field_input_email_6_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "app-form-field-input-email", 32);
} if (rf & 2) {
    var rowItem_r7 = ɵɵnextContext().$implicit;
    ɵɵproperty("formControlName", rowItem_r7.getControlName())("formFieldInputEmail", rowItem_r7);
} }
function MyAngularFormComponent_section_2_section_1_app_form_field_input_file_7_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "app-form-field-input-file", 33);
} if (rf & 2) {
    var rowItem_r7 = ɵɵnextContext().$implicit;
    ɵɵproperty("formControlName", rowItem_r7.getControlName())("formFieldInputFile", rowItem_r7);
} }
function MyAngularFormComponent_section_2_section_1_app_datepicker_8_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "app-datepicker", 34);
} if (rf & 2) {
    var rowItem_r7 = ɵɵnextContext().$implicit;
    ɵɵproperty("formControlName", rowItem_r7.getControlName())("datepicker", rowItem_r7);
} }
function MyAngularFormComponent_section_2_section_1_app_timepicker_9_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "app-timepicker", 35);
} if (rf & 2) {
    var rowItem_r7 = ɵɵnextContext().$implicit;
    ɵɵproperty("formControlName", rowItem_r7.getControlName())("timepicker", rowItem_r7);
} }
function MyAngularFormComponent_section_2_section_1_app_textarea_10_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "app-textarea", 36);
} if (rf & 2) {
    var rowItem_r7 = ɵɵnextContext().$implicit;
    ɵɵproperty("formControlName", rowItem_r7.getControlName())("textarea", rowItem_r7);
} }
function MyAngularFormComponent_section_2_section_1_app_select_11_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "app-select", 37);
} if (rf & 2) {
    var rowItem_r7 = ɵɵnextContext().$implicit;
    ɵɵproperty("formControlName", rowItem_r7.getControlName())("select", rowItem_r7);
} }
function MyAngularFormComponent_section_2_section_1_app_slider_12_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "app-slider", 38);
} if (rf & 2) {
    var rowItem_r7 = ɵɵnextContext().$implicit;
    ɵɵproperty("formControlName", rowItem_r7.getControlName())("slider", rowItem_r7);
} }
function MyAngularFormComponent_section_2_section_1_app_slide_toggle_13_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "app-slide-toggle", 39);
} if (rf & 2) {
    var rowItem_r7 = ɵɵnextContext().$implicit;
    ɵɵproperty("formControlName", rowItem_r7.getControlName())("slideToggle", rowItem_r7);
} }
function MyAngularFormComponent_section_2_section_1_app_button_14_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "app-button", 40);
} if (rf & 2) {
    var rowItem_r7 = ɵɵnextContext().$implicit;
    ɵɵproperty("formControlName", rowItem_r7.getControlName())("button", rowItem_r7);
} }
function MyAngularFormComponent_section_2_section_1_ng_container_15_app_autocomplete_1_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "app-autocomplete", 42);
} if (rf & 2) {
    var rowItem_r7 = ɵɵnextContext(2).$implicit;
    ɵɵproperty("formControlName", rowItem_r7.getControlName())("autocomplete", rowItem_r7);
} }
function MyAngularFormComponent_section_2_section_1_ng_container_15_section_2_mat_error_1_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-error");
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var rowItem_r7 = ɵɵnextContext(3).$implicit;
    var ctx_r43 = ɵɵnextContext(2);
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r43.getFirstExistingControlError(rowItem_r7).message);
} }
function MyAngularFormComponent_section_2_section_1_ng_container_15_section_2_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "section", 43);
    ɵɵtemplate(1, MyAngularFormComponent_section_2_section_1_ng_container_15_section_2_mat_error_1_Template, 2, 1, "mat-error", 24);
    ɵɵelementEnd();
} if (rf & 2) {
    var rowItem_r7 = ɵɵnextContext(2).$implicit;
    var ctx_r41 = ɵɵnextContext(2);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r41.getFirstExistingControlError(rowItem_r7));
} }
function MyAngularFormComponent_section_2_section_1_ng_container_15_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementContainerStart(0);
    ɵɵtemplate(1, MyAngularFormComponent_section_2_section_1_ng_container_15_app_autocomplete_1_Template, 1, 2, "app-autocomplete", 41);
    ɵɵtemplate(2, MyAngularFormComponent_section_2_section_1_ng_container_15_section_2_Template, 2, 1, "section", 26);
    ɵɵelementContainerEnd();
} if (rf & 2) {
    var autocompleteSubjects_r39 = ctx.ngIf;
    var rowItem_r7 = ɵɵnextContext().$implicit;
    var ctx_r22 = ɵɵnextContext(2);
    var _r0 = ɵɵreference(1);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", !autocompleteSubjects_r39.hide);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", !autocompleteSubjects_r39.hide && (!ctx_r22.form.getFormGroup().get(rowItem_r7.getControlName()).pristine && ctx_r22.hasFormControlError(rowItem_r7.getControlName()) || _r0.submitted && ctx_r22.hasFormControlError(rowItem_r7.getControlName())));
} }
function MyAngularFormComponent_section_2_section_1_p_17_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "p", 44);
    ɵɵtext(1, " ssssssss ");
    ɵɵelementEnd();
} }
function MyAngularFormComponent_section_2_section_1_section_18_mat_error_1_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-error");
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var rowItem_r7 = ɵɵnextContext(2).$implicit;
    var ctx_r47 = ɵɵnextContext(2);
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r47.getFirstExistingControlError(rowItem_r7).message);
} }
function MyAngularFormComponent_section_2_section_1_section_18_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "section", 43);
    ɵɵtemplate(1, MyAngularFormComponent_section_2_section_1_section_18_mat_error_1_Template, 2, 1, "mat-error", 24);
    ɵɵelementEnd();
} if (rf & 2) {
    var rowItem_r7 = ɵɵnextContext().$implicit;
    var ctx_r24 = ɵɵnextContext(2);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r24.getFirstExistingControlError(rowItem_r7));
} }
var _c0$1 = function (a0) { return { hide: a0 }; };
function MyAngularFormComponent_section_2_section_1_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "section", 9);
    ɵɵtemplate(1, MyAngularFormComponent_section_2_section_1_app_radio_button_1_Template, 1, 2, "app-radio-button", 10);
    ɵɵtemplate(2, MyAngularFormComponent_section_2_section_1_app_checkbox_2_Template, 1, 2, "app-checkbox", 11);
    ɵɵtemplate(3, MyAngularFormComponent_section_2_section_1_app_form_field_input_text_3_Template, 1, 2, "app-form-field-input-text", 12);
    ɵɵtemplate(4, MyAngularFormComponent_section_2_section_1_app_form_field_input_password_4_Template, 1, 2, "app-form-field-input-password", 13);
    ɵɵtemplate(5, MyAngularFormComponent_section_2_section_1_app_form_field_input_number_5_Template, 1, 2, "app-form-field-input-number", 14);
    ɵɵtemplate(6, MyAngularFormComponent_section_2_section_1_app_form_field_input_email_6_Template, 1, 2, "app-form-field-input-email", 15);
    ɵɵtemplate(7, MyAngularFormComponent_section_2_section_1_app_form_field_input_file_7_Template, 1, 2, "app-form-field-input-file", 16);
    ɵɵtemplate(8, MyAngularFormComponent_section_2_section_1_app_datepicker_8_Template, 1, 2, "app-datepicker", 17);
    ɵɵtemplate(9, MyAngularFormComponent_section_2_section_1_app_timepicker_9_Template, 1, 2, "app-timepicker", 18);
    ɵɵtemplate(10, MyAngularFormComponent_section_2_section_1_app_textarea_10_Template, 1, 2, "app-textarea", 19);
    ɵɵtemplate(11, MyAngularFormComponent_section_2_section_1_app_select_11_Template, 1, 2, "app-select", 20);
    ɵɵtemplate(12, MyAngularFormComponent_section_2_section_1_app_slider_12_Template, 1, 2, "app-slider", 21);
    ɵɵtemplate(13, MyAngularFormComponent_section_2_section_1_app_slide_toggle_13_Template, 1, 2, "app-slide-toggle", 22);
    ɵɵtemplate(14, MyAngularFormComponent_section_2_section_1_app_button_14_Template, 1, 2, "app-button", 23);
    ɵɵtemplate(15, MyAngularFormComponent_section_2_section_1_ng_container_15_Template, 3, 2, "ng-container", 24);
    ɵɵpipe(16, "async");
    ɵɵtemplate(17, MyAngularFormComponent_section_2_section_1_p_17_Template, 2, 0, "p", 25);
    ɵɵtemplate(18, MyAngularFormComponent_section_2_section_1_section_18_Template, 2, 1, "section", 26);
    ɵɵelementEnd();
} if (rf & 2) {
    var rowItem_r7 = ctx.$implicit;
    var ctx_r6 = ɵɵnextContext(2);
    var _r0 = ɵɵreference(1);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", rowItem_r7.getType() == "RADIO_BUTTON_GROUP");
    ɵɵadvance(1);
    ɵɵproperty("ngIf", rowItem_r7.getType() == "CHECKBOX");
    ɵɵadvance(1);
    ɵɵproperty("ngIf", rowItem_r7.getType() == "FORM_FIELD_INPUT" && ctx_r6.getFormFieldInputType(rowItem_r7) === "text");
    ɵɵadvance(1);
    ɵɵproperty("ngIf", rowItem_r7.getType() == "FORM_FIELD_INPUT" && ctx_r6.getFormFieldInputType(rowItem_r7) === "password");
    ɵɵadvance(1);
    ɵɵproperty("ngIf", rowItem_r7.getType() == "FORM_FIELD_INPUT" && ctx_r6.getFormFieldInputType(rowItem_r7) === "number");
    ɵɵadvance(1);
    ɵɵproperty("ngIf", rowItem_r7.getType() == "FORM_FIELD_INPUT" && ctx_r6.getFormFieldInputType(rowItem_r7) === "email");
    ɵɵadvance(1);
    ɵɵproperty("ngIf", rowItem_r7.getType() == "FORM_FIELD_INPUT" && ctx_r6.getFormFieldInputType(rowItem_r7) === "file");
    ɵɵadvance(1);
    ɵɵproperty("ngIf", rowItem_r7.getType() == "DATEPICKER");
    ɵɵadvance(1);
    ɵɵproperty("ngIf", rowItem_r7.getType() == "TIMEPICKER");
    ɵɵadvance(1);
    ɵɵproperty("ngIf", rowItem_r7.getType() == "FORM_FIELD_TEXTAREA");
    ɵɵadvance(1);
    ɵɵproperty("ngIf", rowItem_r7.getType() == "FORM_FIELD_SELECT");
    ɵɵadvance(1);
    ɵɵproperty("ngIf", rowItem_r7.getType() == "SLIDER");
    ɵɵadvance(1);
    ɵɵproperty("ngIf", rowItem_r7.getType() == "SLIDE_TOGGLE");
    ɵɵadvance(1);
    ɵɵproperty("ngIf", rowItem_r7.getType() == "BUTTON");
    ɵɵadvance(1);
    ɵɵproperty("ngIf", rowItem_r7.getType() == "AUTOCOMPLETE" && ɵɵpureFunction1(19, _c0$1, ɵɵpipeBind1(16, 17, rowItem_r7.getHiddenSubject())));
    ɵɵadvance(2);
    ɵɵproperty("ngIf", !(rowItem_r7.getType() == "RADIO_BUTTON_GROUP" || rowItem_r7.getType() == "CHECKBOX" || rowItem_r7.getType() == "FORM_FIELD_INPUT" || rowItem_r7.getType() == "DATEPICKER" || rowItem_r7.getType() == "FORM_FIELD_TEXTAREA" || rowItem_r7.getType() == "FORM_FIELD_SELECT" || rowItem_r7.getType() == "SLIDER" || rowItem_r7.getType() == "SLIDE_TOGGLE" || rowItem_r7.getType() == "BUTTON" || rowItem_r7.getType() == "AUTOCOMPLETE" || rowItem_r7.getType() == "TIMEPICKER"));
    ɵɵadvance(1);
    ɵɵproperty("ngIf", rowItem_r7.getType() != "AUTOCOMPLETE" && (ctx_r6.form.getFormGroup().get(rowItem_r7.getControlName()) && !ctx_r6.form.getFormGroup().get(rowItem_r7.getControlName()).pristine && ctx_r6.hasFormControlError(rowItem_r7.getControlName()) || _r0.submitted && ctx_r6.hasFormControlError(rowItem_r7.getControlName())));
} }
function MyAngularFormComponent_section_2_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "section", 7);
    ɵɵtemplate(1, MyAngularFormComponent_section_2_section_1_Template, 19, 21, "section", 8);
    ɵɵelementEnd();
} if (rf & 2) {
    var row_r5 = ctx.$implicit;
    var ctx_r1 = ɵɵnextContext();
    ɵɵproperty("fxLayout", ctx_r1.deviceService.isDeviceDesktop() ? "row" : "column")("fxLayoutAlign", ctx_r1.deviceService.isDeviceDesktop() ? "center start" : "center center")("fxLayoutGap", ctx_r1.deviceService.isDeviceDesktop() ? "20px" : "10px");
    ɵɵadvance(1);
    ɵɵproperty("ngForOf", row_r5.getChildren());
} }
function MyAngularFormComponent_button_4_Template(rf, ctx) { if (rf & 1) {
    var _r51 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "button", 45);
    ɵɵlistener("click", function MyAngularFormComponent_button_4_Template_button_click_0_listener() { ɵɵrestoreView(_r51); var ctx_r50 = ɵɵnextContext(); return ctx_r50.form.submitOnlyIfFormValid ? ctx_r50.form.getFormGroup().valid && ctx_r50.onSubmitHappened() : ctx_r50.onSubmitHappened(); });
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r2 = ɵɵnextContext();
    ɵɵproperty("disabled", ctx_r2.form.submitOnlyIfFormValid ? !ctx_r2.form.getFormGroup().valid : false);
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r2.form.submitButtonText);
} }
function MyAngularFormComponent_button_5_Template(rf, ctx) { if (rf & 1) {
    var _r53 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "button", 46);
    ɵɵlistener("click", function MyAngularFormComponent_button_5_Template_button_click_0_listener() { ɵɵrestoreView(_r53); var ctx_r52 = ɵɵnextContext(); return ctx_r52.onCancelHappened(); });
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r3 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r3.form.cancelButtonText);
} }
function MyAngularFormComponent_button_6_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "button", 47);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r4 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r4.form.resetButtonText);
} }
var MyAngularFormComponent = /** @class */ (function () {
    function MyAngularFormComponent(deviceService) {
        var _this = this;
        this.deviceService = deviceService;
        this.onSubmit = new EventEmitter();
        this.onCancel = new EventEmitter();
        this.form = undefined;
        this.onSubmitHappened = function () {
            _this.onSubmit.emit(_this.form.getFormGroup());
        };
        this.onCancelHappened = function () {
            _this.onCancel.emit();
        };
        this.hasFormControlError = function (formControlName) {
            return _this.form.getFormGroup().get(formControlName)
                ? _this.form.getFormGroup().get(formControlName).errors
                    ? true
                    : false
                : false;
        };
        this.getFirstExistingControlError = function (rowItem) {
            var _a;
            var errors = (_a = _this.form.getFormGroup().get(rowItem.getControlName())) === null || _a === void 0 ? void 0 : _a.errors;
            if (errors)
                for (var key in errors) {
                    for (var index in rowItem.getValidators()) {
                        if (rowItem.getValidators()[index].name === key)
                            return rowItem.getValidators()[index];
                    }
                }
            return undefined;
        };
        // for form-field-input
        this.getFormFieldInputType = function (rowItem) {
            return rowItem.getFormFieldInputType();
        };
    }
    MyAngularFormComponent.prototype.ngOnInit = function () {
        // console.log("MyFormComponent init");
    };
    MyAngularFormComponent.prototype.ngOnChanges = function (changes) {
        if (changes.form && changes.form.currentValue) {
            // console.log(changes.form);
            // this.initForm();
        }
    };
    MyAngularFormComponent.prototype.ngDoCheck = function () {
        // if (this.form.getChildren().length !== this._numOfFormRows) {
        //   console.log(this._numOfFormRows);
        //   console.log(this.form.getChildren().length);
        //   for (
        //     let index = this._numOfFormRows;
        //     index < this.form.getChildren().length;
        //     index++
        //   ) {
        //     this._updateForm(<FormRow>this.form.getChildren()[index]);
        //   }
        // }
    };
    MyAngularFormComponent.prototype.ngOnDestroy = function () {
        // console.log("MyFormComponent destroyed");
    };
    MyAngularFormComponent.ɵfac = function MyAngularFormComponent_Factory(t) { return new (t || MyAngularFormComponent)(ɵɵdirectiveInject(DeviceService)); };
    MyAngularFormComponent.ɵcmp = ɵɵdefineComponent({ type: MyAngularFormComponent, selectors: [["lib-my-angular-form"]], inputs: { form: "form" }, outputs: { onSubmit: "onSubmit", onCancel: "onCancel" }, features: [ɵɵNgOnChangesFeature], decls: 7, vars: 5, consts: [["fxLayout", "column", "fxLayoutAlign", "center center", "fxLayoutGap", "10px", 3, "formGroup"], ["ngForm", "ngForm"], ["class", "row overflow-hidden", 3, "fxLayout", "fxLayoutAlign", "fxLayoutGap", 4, "ngFor", "ngForOf"], ["fxLayoutAlign", "center center", "fxLayoutGap", "2vw"], ["type", "button", "mat-raised-button", "", 3, "disabled", "click", 4, "ngIf"], ["type", "button", "mat-raised-button", "", 3, "click", 4, "ngIf"], ["type", "reset", "mat-raised-button", "", 4, "ngIf"], [1, "row", "overflow-hidden", 3, "fxLayout", "fxLayoutAlign", "fxLayoutGap"], ["class", "row-item", "fxFlex", "", 4, "ngFor", "ngForOf"], ["fxFlex", "", 1, "row-item"], [3, "formControlName", "radioButtonGroup", 4, "ngIf"], [3, "formControlName", "checkbox", 4, "ngIf"], [3, "formControlName", "formFieldInputText", 4, "ngIf"], [3, "formControlName", "formFieldInputPassword", 4, "ngIf"], [3, "formControlName", "formFieldInputNumber", 4, "ngIf"], [3, "formControlName", "formFieldInputEmail", 4, "ngIf"], [3, "formControlName", "formFieldInputFile", 4, "ngIf"], [3, "formControlName", "datepicker", 4, "ngIf"], [3, "formControlName", "timepicker", 4, "ngIf"], [3, "formControlName", "textarea", 4, "ngIf"], [3, "formControlName", "select", 4, "ngIf"], [3, "formControlName", "slider", 4, "ngIf"], [3, "formControlName", "slideToggle", 4, "ngIf"], [3, "formControlName", "button", 4, "ngIf"], [4, "ngIf"], ["class", "no-margin", "fxLayout", "column", "fxLayoutAlign", "center center", 4, "ngIf"], ["fxLayout", "column", "fxLayoutAlign", "center center", "class", "top-margin", 4, "ngIf"], [3, "formControlName", "radioButtonGroup"], [3, "formControlName", "checkbox"], [3, "formControlName", "formFieldInputText"], [3, "formControlName", "formFieldInputPassword"], [3, "formControlName", "formFieldInputNumber"], [3, "formControlName", "formFieldInputEmail"], [3, "formControlName", "formFieldInputFile"], [3, "formControlName", "datepicker"], [3, "formControlName", "timepicker"], [3, "formControlName", "textarea"], [3, "formControlName", "select"], [3, "formControlName", "slider"], [3, "formControlName", "slideToggle"], [3, "formControlName", "button"], [3, "formControlName", "autocomplete", 4, "ngIf"], [3, "formControlName", "autocomplete"], ["fxLayout", "column", "fxLayoutAlign", "center center", 1, "top-margin"], ["fxLayout", "column", "fxLayoutAlign", "center center", 1, "no-margin"], ["type", "button", "mat-raised-button", "", 3, "disabled", "click"], ["type", "button", "mat-raised-button", "", 3, "click"], ["type", "reset", "mat-raised-button", ""]], template: function MyAngularFormComponent_Template(rf, ctx) { if (rf & 1) {
            ɵɵelementStart(0, "form", 0, 1);
            ɵɵtemplate(2, MyAngularFormComponent_section_2_Template, 2, 4, "section", 2);
            ɵɵelementStart(3, "section", 3);
            ɵɵtemplate(4, MyAngularFormComponent_button_4_Template, 2, 2, "button", 4);
            ɵɵtemplate(5, MyAngularFormComponent_button_5_Template, 2, 1, "button", 5);
            ɵɵtemplate(6, MyAngularFormComponent_button_6_Template, 2, 1, "button", 6);
            ɵɵelementEnd();
            ɵɵelementEnd();
        } if (rf & 2) {
            ɵɵproperty("formGroup", ctx.form.getFormGroup());
            ɵɵadvance(2);
            ɵɵproperty("ngForOf", ctx.form.getChildren());
            ɵɵadvance(2);
            ɵɵproperty("ngIf", ctx.form.showSubmitButton);
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.form.showCancelButton);
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.form.showResetButton);
        } }, directives: [ɵangular_packages_forms_forms_y, NgControlStatusGroup, DefaultLayoutDirective, DefaultLayoutAlignDirective, DefaultLayoutGapDirective, FormGroupDirective, NgForOf, NgIf, DefaultFlexDirective, RadioButtonComponent, NgControlStatus, FormControlName, CheckboxComponent, FormFieldInputTextComponent, FormFieldInputPasswordComponent, FormFieldInputNumberComponent, FormFieldInputEmailComponent, FormFieldInputFileComponent, DatepickerComponent, TimepickerComponent, TextareaComponent, SelectComponent, SliderComponent, SlideToggleComponent, ButtonComponent, AutocompleteComponent, MatError, MatButton], pipes: [AsyncPipe], styles: ["", "section.row[_ngcontent-%COMP%] {\n        width: 100%;\n        \n        padding: 10px 0;\n        \n    }\n\n    section.row-item[_ngcontent-%COMP%] {\n        \n        \n    }\n\n    .overflow-hidden[_ngcontent-%COMP%] {\n        overflow: hidden;\n    }\n\n    .text-overflow-ellipsis[_ngcontent-%COMP%] {\n        text-overflow: ellipsis;\n    }\n\n    .white-space-nowrap[_ngcontent-%COMP%] {\n        white-space: nowrap;\n    }\n\n    form[_ngcontent-%COMP%] {\n        padding: 15px;\n        \n        \n    }\n\n    .top-margin[_ngcontent-%COMP%] {\n        margin-top: 10px;\n    }\n\n    .no-margin[_ngcontent-%COMP%] {\n        margin: 0;\n    }\n\n    .no-padding[_ngcontent-%COMP%] {\n        padding: 0;\n    }"] });
    return MyAngularFormComponent;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(MyAngularFormComponent, [{
        type: Component,
        args: [{
                selector: 'lib-my-angular-form',
                templateUrl: './my-angular-form.component.html',
                styleUrls: ['./my-angular-form.component.css'],
            }]
    }], function () { return [{ type: DeviceService }]; }, { onSubmit: [{
            type: Output
        }], onCancel: [{
            type: Output
        }], form: [{
            type: Input
        }] }); })();

var MAT_COLOR;
(function (MAT_COLOR) {
    MAT_COLOR["PRIMARY"] = "primary";
    MAT_COLOR["WARN"] = "warn";
    MAT_COLOR["ACCENT"] = "accent";
    MAT_COLOR["EMPTY"] = "";
})(MAT_COLOR || (MAT_COLOR = {}));
var material = [
    MatButtonModule,
    MatToolbarModule,
    MatRadioModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatDialogModule,
    MatListModule,
    MatProgressBarModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSnackBarModule,
    MatSelectModule,
    MatSliderModule,
    MatSlideToggleModule,
];
var MaterialModule = /** @class */ (function () {
    function MaterialModule() {
    }
    MaterialModule.ɵmod = ɵɵdefineNgModule({ type: MaterialModule });
    MaterialModule.ɵinj = ɵɵdefineInjector({ factory: function MaterialModule_Factory(t) { return new (t || MaterialModule)(); }, imports: [__spread(material), MatButtonModule,
            MatToolbarModule,
            MatRadioModule,
            MatCheckboxModule,
            MatFormFieldModule,
            MatInputModule,
            MatIconModule,
            MatDialogModule,
            MatListModule,
            MatProgressBarModule,
            MatDatepickerModule,
            MatNativeDateModule,
            MatSnackBarModule,
            MatSelectModule,
            MatSliderModule,
            MatSlideToggleModule] });
    return MaterialModule;
}());
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && ɵɵsetNgModuleScope(MaterialModule, { imports: [MatButtonModule,
        MatToolbarModule,
        MatRadioModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatInputModule,
        MatIconModule,
        MatDialogModule,
        MatListModule,
        MatProgressBarModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatSnackBarModule,
        MatSelectModule,
        MatSliderModule,
        MatSlideToggleModule], exports: [MatButtonModule,
        MatToolbarModule,
        MatRadioModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatInputModule,
        MatIconModule,
        MatDialogModule,
        MatListModule,
        MatProgressBarModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatSnackBarModule,
        MatSelectModule,
        MatSliderModule,
        MatSlideToggleModule] }); })();
/*@__PURE__*/ (function () { ɵsetClassMetadata(MaterialModule, [{
        type: NgModule,
        args: [{
                imports: __spread(material),
                exports: __spread(material),
            }]
    }], null, null); })();

var url = 'http://localhost:3000/upload';
var UploadService = /** @class */ (function () {
    function UploadService(http) {
        this.http = http;
    }
    UploadService.prototype.upload = function (files) {
        var _this = this;
        // this will be the our resulting map
        var status = {};
        files.forEach(function (file) {
            // create a new multipart-form for every file
            var formData = new FormData();
            formData.append('file', file, file.name);
            // create a http-post request and pass the form
            // tell it to report the upload progress
            var req = new HttpRequest('POST', url, formData, {
                reportProgress: true,
            });
            // create a new progress-subject for every file
            var progress = new Subject();
            // send the http-request and subscribe for progress-updates
            _this.http.request(req).subscribe(function (event) {
                if (event.type === HttpEventType.UploadProgress) {
                    // calculate the progress percentage
                    var percentDone = Math.round((100 * event.loaded) / event.total);
                    // pass the percentage into the progress-stream
                    console.log(percentDone);
                    progress.next(percentDone);
                }
                else if (event instanceof HttpResponse) {
                    // Close the progress-stream if we get an answer form the API
                    // The upload is complete
                    progress.complete();
                }
            });
            // Save every progress-observable in a map of all observables
            status[file.name] = {
                progress: progress.asObservable(),
            };
        });
        // return the map of progress.observables
        return status;
    };
    UploadService.ɵfac = function UploadService_Factory(t) { return new (t || UploadService)(ɵɵinject(HttpClient)); };
    UploadService.ɵprov = ɵɵdefineInjectable({ token: UploadService, factory: UploadService.ɵfac, providedIn: 'root' });
    return UploadService;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(UploadService, [{
        type: Injectable,
        args: [{ providedIn: 'root' }]
    }], function () { return [{ type: HttpClient }]; }, null); })();

var _c0$2 = ["file"];
function UploadDialogComponent_mat_list_item_10_mat_progress_bar_3_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "mat-progress-bar", 12);
    ɵɵpipe(1, "async");
} if (rf & 2) {
    var file_r3 = ɵɵnextContext().$implicit;
    var ctx_r4 = ɵɵnextContext();
    ɵɵproperty("value", ɵɵpipeBind1(1, 1, ctx_r4.progress[file_r3.name].progress));
} }
function UploadDialogComponent_mat_list_item_10_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-list-item");
    ɵɵelementStart(1, "h4", 10);
    ɵɵtext(2);
    ɵɵelementEnd();
    ɵɵtemplate(3, UploadDialogComponent_mat_list_item_10_mat_progress_bar_3_Template, 2, 3, "mat-progress-bar", 11);
    ɵɵelementEnd();
} if (rf & 2) {
    var file_r3 = ctx.$implicit;
    var ctx_r1 = ɵɵnextContext();
    ɵɵadvance(2);
    ɵɵtextInterpolate(file_r3.name);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r1.progress);
} }
function UploadDialogComponent_button_12_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "button", 13);
    ɵɵtext(1, "Cancel");
    ɵɵelementEnd();
} }
var UploadDialogComponent = /** @class */ (function () {
    function UploadDialogComponent(dialogRef, uploadService) {
        this.dialogRef = dialogRef;
        this.uploadService = uploadService;
        this.files = new Set();
        this.canBeClosed = true;
        this.primaryButtonText = 'Upload';
        this.showCancelButton = true;
        this.uploading = false;
        this.uploadSuccessful = false;
    }
    UploadDialogComponent.prototype.onFilesAdded = function () {
        var files = this.file.nativeElement.files;
        for (var key in files) {
            if (!isNaN(parseInt(key))) {
                this.files.add(files[key]);
            }
        }
    };
    UploadDialogComponent.prototype.addFiles = function () {
        this.file.nativeElement.click();
    };
    UploadDialogComponent.prototype.closeDialog = function () {
        var _this = this;
        // if everything was uploaded already, just close the dialog
        if (this.uploadSuccessful) {
            return this.dialogRef.close();
        }
        // set the component state to "uploading"
        this.uploading = true;
        // start the upload and save the progress map
        this.progress = this.uploadService.upload(this.files);
        console.log(this.progress);
        for (var key in this.progress) {
            this.progress[key].progress.subscribe(function (val) { return console.log(val); });
        }
        // convert the progress map into an array
        var allProgressObservables = [];
        for (var key in this.progress) {
            allProgressObservables.push(this.progress[key].progress);
        }
        // Adjust the state variables
        // The OK-button should have the text "Finish" now
        this.primaryButtonText = 'Finish';
        // The dialog should not be closed while uploading
        this.canBeClosed = false;
        this.dialogRef.disableClose = true;
        // Hide the cancel-button
        this.showCancelButton = false;
        // When all progress-observables are completed...
        forkJoin(allProgressObservables).subscribe(function (end) {
            // ... the dialog can be closed again...
            _this.canBeClosed = true;
            _this.dialogRef.disableClose = false;
            // ... the upload was successful...
            _this.uploadSuccessful = true;
            // ... and the component is no longer uploading
            _this.uploading = false;
        });
    };
    UploadDialogComponent.prototype.ngOnInit = function () {
        console.log('UploadDialogComponent init');
    };
    UploadDialogComponent.prototype.ngOnDestroy = function () {
        console.log('UploadDialogComponent destroyed');
    };
    UploadDialogComponent.ɵfac = function UploadDialogComponent_Factory(t) { return new (t || UploadDialogComponent)(ɵɵdirectiveInject(MatDialogRef), ɵɵdirectiveInject(UploadService)); };
    UploadDialogComponent.ɵcmp = ɵɵdefineComponent({ type: UploadDialogComponent, selectors: [["app-upload-dialog"]], viewQuery: function UploadDialogComponent_Query(rf, ctx) { if (rf & 1) {
            ɵɵviewQuery(_c0$2, true);
        } if (rf & 2) {
            var _t;
            ɵɵqueryRefresh(_t = ɵɵloadQuery()) && (ctx.file = _t.first);
        } }, decls: 15, vars: 5, consts: [["type", "file", "multiple", "", 2, "display", "none", 3, "change"], ["file", ""], ["fxLayout", "column", "fxLayoutAlign", "space-evenly stretch", 1, "container"], ["mat-dialog-title", ""], ["mat-raised-button", "", "color", "primary", 1, "add-files-btn", 3, "disabled", "click"], ["fxFlex", ""], [4, "ngFor", "ngForOf"], [1, "actions"], ["mat-button", "", "mat-dialog-close", "", 4, "ngIf"], ["mat-raised-button", "", "color", "primary", 3, "disabled", "click"], ["mat-line", ""], ["mode", "determinate", 3, "value", 4, "ngIf"], ["mode", "determinate", 3, "value"], ["mat-button", "", "mat-dialog-close", ""]], template: function UploadDialogComponent_Template(rf, ctx) { if (rf & 1) {
            ɵɵelementStart(0, "input", 0, 1);
            ɵɵlistener("change", function UploadDialogComponent_Template_input_change_0_listener() { return ctx.onFilesAdded(); });
            ɵɵelementEnd();
            ɵɵelementStart(2, "div", 2);
            ɵɵelementStart(3, "h1", 3);
            ɵɵtext(4, "Upload Files");
            ɵɵelementEnd();
            ɵɵelementStart(5, "div");
            ɵɵelementStart(6, "button", 4);
            ɵɵlistener("click", function UploadDialogComponent_Template_button_click_6_listener() { return ctx.addFiles(); });
            ɵɵtext(7, " Add Files ");
            ɵɵelementEnd();
            ɵɵelementEnd();
            ɵɵelementStart(8, "mat-dialog-content", 5);
            ɵɵelementStart(9, "mat-list");
            ɵɵtemplate(10, UploadDialogComponent_mat_list_item_10_Template, 4, 2, "mat-list-item", 6);
            ɵɵelementEnd();
            ɵɵelementEnd();
            ɵɵelementStart(11, "mat-dialog-actions", 7);
            ɵɵtemplate(12, UploadDialogComponent_button_12_Template, 2, 0, "button", 8);
            ɵɵelementStart(13, "button", 9);
            ɵɵlistener("click", function UploadDialogComponent_Template_button_click_13_listener() { return ctx.closeDialog(); });
            ɵɵtext(14);
            ɵɵelementEnd();
            ɵɵelementEnd();
            ɵɵelementEnd();
        } if (rf & 2) {
            ɵɵadvance(6);
            ɵɵproperty("disabled", ctx.uploading || ctx.uploadSuccessful);
            ɵɵadvance(4);
            ɵɵproperty("ngForOf", ctx.files);
            ɵɵadvance(2);
            ɵɵproperty("ngIf", ctx.showCancelButton);
            ɵɵadvance(1);
            ɵɵproperty("disabled", !ctx.canBeClosed);
            ɵɵadvance(1);
            ɵɵtextInterpolate(ctx.primaryButtonText);
        } }, directives: [DefaultLayoutDirective, DefaultLayoutAlignDirective, MatDialogTitle, MatButton, MatDialogContent, DefaultFlexDirective, MatList, NgForOf, MatDialogActions, NgIf, MatListItem, MatLine, MatProgressBar, MatDialogClose], pipes: [AsyncPipe], styles: ["", ".add-files-btn[_ngcontent-%COMP%] {\n  float: right;\n}\n\n[_nghost-%COMP%] {\n  height: 100%;\n  display: flex;\n  flex: 1;\n  flex-direction: column;\n}\n\n.actions[_ngcontent-%COMP%] {\n  justify-content: flex-end;\n}\n\n.container[_ngcontent-%COMP%] {\n  height: 100%;\n}"] });
    return UploadDialogComponent;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(UploadDialogComponent, [{
        type: Component,
        args: [{
                selector: 'app-upload-dialog',
                templateUrl: './upload-dialog.component.html',
                styleUrls: ['./upload-dialog.component.css'],
            }]
    }], function () { return [{ type: MatDialogRef }, { type: UploadService }]; }, { file: [{
            type: ViewChild,
            args: ['file', { static: false }]
        }] }); })();

var SharedModule = /** @class */ (function () {
    function SharedModule() {
    }
    SharedModule.ɵmod = ɵɵdefineNgModule({ type: SharedModule });
    SharedModule.ɵinj = ɵɵdefineInjector({ factory: function SharedModule_Factory(t) { return new (t || SharedModule)(); }, imports: [[
                CommonModule,
                FormsModule,
                ReactiveFormsModule,
                FlexLayoutModule,
                MaterialModule,
                HttpClientModule,
                DeviceDetectorModule.forRoot(),
                AutocompleteModule,
                NgxMaterialTimepickerModule,
            ],
            CommonModule,
            FormsModule,
            ReactiveFormsModule,
            FlexLayoutModule,
            MaterialModule,
            HttpClientModule,
            DeviceDetectorModule,
            AutocompleteModule,
            NgxMaterialTimepickerModule] });
    return SharedModule;
}());
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && ɵɵsetNgModuleScope(SharedModule, { declarations: [RadioButtonComponent,
        CheckboxComponent,
        UploadDialogComponent,
        DragDropDirective,
        FormFieldInputTextComponent,
        FormFieldInputPasswordComponent,
        FormFieldInputNumberComponent,
        FormFieldInputFileComponent,
        FormFieldInputEmailComponent,
        DatepickerComponent,
        TextareaComponent,
        SelectComponent,
        SliderComponent,
        SlideToggleComponent,
        ButtonComponent,
        AutocompleteComponent,
        TimepickerComponent], imports: [CommonModule,
        FormsModule,
        ReactiveFormsModule,
        FlexLayoutModule,
        MaterialModule,
        HttpClientModule, DeviceDetectorModule, AutocompleteModule,
        NgxMaterialTimepickerModule], exports: [CommonModule,
        FormsModule,
        ReactiveFormsModule,
        FlexLayoutModule,
        MaterialModule,
        HttpClientModule,
        DeviceDetectorModule,
        AutocompleteModule,
        NgxMaterialTimepickerModule,
        RadioButtonComponent,
        CheckboxComponent,
        DragDropDirective,
        FormFieldInputTextComponent,
        FormFieldInputPasswordComponent,
        FormFieldInputNumberComponent,
        FormFieldInputFileComponent,
        FormFieldInputEmailComponent,
        DatepickerComponent,
        TextareaComponent,
        SelectComponent,
        SliderComponent,
        SlideToggleComponent,
        ButtonComponent,
        AutocompleteComponent,
        TimepickerComponent] }); })();
/*@__PURE__*/ (function () { ɵsetClassMetadata(SharedModule, [{
        type: NgModule,
        args: [{
                declarations: [
                    RadioButtonComponent,
                    CheckboxComponent,
                    UploadDialogComponent,
                    DragDropDirective,
                    FormFieldInputTextComponent,
                    FormFieldInputPasswordComponent,
                    FormFieldInputNumberComponent,
                    FormFieldInputFileComponent,
                    FormFieldInputEmailComponent,
                    DatepickerComponent,
                    TextareaComponent,
                    SelectComponent,
                    SliderComponent,
                    SlideToggleComponent,
                    ButtonComponent,
                    AutocompleteComponent,
                    TimepickerComponent,
                ],
                imports: [
                    CommonModule,
                    FormsModule,
                    ReactiveFormsModule,
                    FlexLayoutModule,
                    MaterialModule,
                    HttpClientModule,
                    DeviceDetectorModule.forRoot(),
                    AutocompleteModule,
                    NgxMaterialTimepickerModule,
                ],
                exports: [
                    CommonModule,
                    FormsModule,
                    ReactiveFormsModule,
                    FlexLayoutModule,
                    MaterialModule,
                    HttpClientModule,
                    DeviceDetectorModule,
                    AutocompleteModule,
                    NgxMaterialTimepickerModule,
                    RadioButtonComponent,
                    CheckboxComponent,
                    DragDropDirective,
                    FormFieldInputTextComponent,
                    FormFieldInputPasswordComponent,
                    FormFieldInputNumberComponent,
                    FormFieldInputFileComponent,
                    FormFieldInputEmailComponent,
                    DatepickerComponent,
                    TextareaComponent,
                    SelectComponent,
                    SliderComponent,
                    SlideToggleComponent,
                    ButtonComponent,
                    AutocompleteComponent,
                    TimepickerComponent,
                ],
                entryComponents: [UploadDialogComponent],
            }]
    }], null, null); })();

var MyAngularFormModule = /** @class */ (function () {
    function MyAngularFormModule() {
    }
    MyAngularFormModule.ɵmod = ɵɵdefineNgModule({ type: MyAngularFormModule });
    MyAngularFormModule.ɵinj = ɵɵdefineInjector({ factory: function MyAngularFormModule_Factory(t) { return new (t || MyAngularFormModule)(); }, imports: [[SharedModule]] });
    return MyAngularFormModule;
}());
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && ɵɵsetNgModuleScope(MyAngularFormModule, { declarations: [MyAngularFormComponent], imports: [SharedModule], exports: [MyAngularFormComponent] }); })();
/*@__PURE__*/ (function () { ɵsetClassMetadata(MyAngularFormModule, [{
        type: NgModule,
        args: [{
                declarations: [MyAngularFormComponent],
                imports: [SharedModule],
                exports: [MyAngularFormComponent],
            }]
    }], null, null); })();

var MAT_BUTTON_TYPE;
(function (MAT_BUTTON_TYPE) {
    MAT_BUTTON_TYPE["BASIC"] = "BASIC";
    MAT_BUTTON_TYPE["RAISED"] = "RAISED";
    MAT_BUTTON_TYPE["STROKED"] = "STROKED";
    MAT_BUTTON_TYPE["FLAT"] = "FLAT";
    MAT_BUTTON_TYPE["ICON"] = "ICON";
    MAT_BUTTON_TYPE["FAB"] = "FAB";
    MAT_BUTTON_TYPE["MINI_FAB"] = "MINI_FAB";
})(MAT_BUTTON_TYPE || (MAT_BUTTON_TYPE = {}));
var BUTTON_TYPE;
(function (BUTTON_TYPE) {
    BUTTON_TYPE["SUBMIT"] = "submit";
    BUTTON_TYPE["BUTTON"] = "button";
    BUTTON_TYPE["RESET"] = "reset";
    BUTTON_TYPE["MENU"] = "menu";
})(BUTTON_TYPE || (BUTTON_TYPE = {}));
var Button = /** @class */ (function (_super) {
    __extends(Button, _super);
    function Button() {
        var _this = _super.call(this) || this;
        _this.color = MAT_COLOR.EMPTY;
        _this.context = 'Click me';
        _this.matButtonType = MAT_BUTTON_TYPE.RAISED;
        _this.buttonType = BUTTON_TYPE.BUTTON;
        _this.matIconString = '';
        _this.getColor = function () {
            return _this.color;
        };
        _this.setColor = function (color) {
            _this.color = color;
        };
        _this.getContext = function () {
            return _this.context;
        };
        _this.setContext = function (context) {
            _this.context = context;
        };
        _this.getMatButtonType = function () {
            return _this.matButtonType;
        };
        _this.setMatButtonType = function (matButtonType) {
            _this.matButtonType = matButtonType;
        };
        _this.getButtonType = function () {
            return _this.buttonType;
        };
        _this.setButtonType = function (buttonType) {
            _this.buttonType = buttonType;
        };
        _this.getFunctionToExecute = function () {
            return _this.functionToExecute;
        };
        _this.setFunctionToExecute = function (functionToExecute) {
            _this.functionToExecute = functionToExecute;
        };
        _this.getMatIconString = function () {
            return _this.matIconString;
        };
        _this.setMatIconString = function (matIconString) {
            _this.matIconString = matIconString;
        };
        return _this;
    }
    return Button;
}(FormRowItem));

var ButtonConstructionStrategy = /** @class */ (function () {
    function ButtonConstructionStrategy() {
    }
    ButtonConstructionStrategy.prototype.construct = function (buttonInterface) {
        var button = new Button();
        button.setType(buttonInterface.type);
        if (buttonInterface.defaultValue != '')
            throw new Error('Default value is not allowed on button');
        button.setControlName(buttonInterface.controlName);
        if (buttonInterface.validators)
            throw new Error('Validators are not allowed on button');
        if (buttonInterface.color)
            button.setColor(buttonInterface.color);
        if (buttonInterface.disabled)
            button.setDisabled(buttonInterface.disabled);
        if (buttonInterface.context)
            button.setContext(buttonInterface.context);
        if (buttonInterface.matButtonType)
            button.setMatButtonType(buttonInterface.matButtonType);
        if (buttonInterface.buttonType)
            button.setButtonType(buttonInterface.buttonType);
        if (buttonInterface.functionToExecute) {
            if (button.getButtonType() !== BUTTON_TYPE.BUTTON)
                throw new Error('Function is only allowed on button of type button');
            else {
                if (isFunction(buttonInterface.functionToExecute))
                    button.setFunctionToExecute(buttonInterface.functionToExecute);
            }
        }
        if (buttonInterface.matIconString) {
            if (button.getMatButtonType() === MAT_BUTTON_TYPE.ICON ||
                button.getMatButtonType() === MAT_BUTTON_TYPE.FAB ||
                button.getMatButtonType() === MAT_BUTTON_TYPE.MINI_FAB) {
                button.setMatIconString(buttonInterface.matIconString);
            }
            else {
                throw new Error('Mat icon string is only allowed on button of matButtonYype ICON|FAB|MINI_FAB');
            }
        }
        return button;
    };
    return ButtonConstructionStrategy;
}());

var Checkbox = /** @class */ (function (_super) {
    __extends(Checkbox, _super);
    function Checkbox() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.color = MAT_COLOR.PRIMARY;
        _this.labelPosition = 'after';
        _this.getName = function () {
            return _this.name;
        };
        _this.setName = function (name) {
            _this.name = name;
        };
        _this.getColor = function () {
            return _this.color;
        };
        _this.setColor = function (color) {
            _this.color = color;
        };
        _this.getLabelPosition = function () {
            return _this.labelPosition;
        };
        _this.setLabelPosition = function (labelPosition) {
            _this.labelPosition = labelPosition;
        };
        return _this;
    }
    return Checkbox;
}(FormRowItem));

var CheckboxConstructionStrategy = /** @class */ (function () {
    function CheckboxConstructionStrategy() {
    }
    CheckboxConstructionStrategy.prototype.construct = function (checkboxInterface) {
        var checkbox = new Checkbox();
        checkbox.setType(checkboxInterface.type);
        checkbox.setControlName(checkboxInterface.controlName);
        if (!checkboxInterface.defaultValue)
            checkboxInterface.defaultValue = false;
        checkbox.setDefaultValue(checkboxInterface.defaultValue);
        if (checkboxInterface.validators)
            checkbox.setValidators(checkboxInterface.validators);
        checkbox.setName(checkboxInterface.name);
        if (checkboxInterface.color)
            checkbox.setColor(checkboxInterface.color);
        if (checkboxInterface.labelPosition)
            checkbox.setLabelPosition(checkboxInterface.labelPosition);
        if (checkboxInterface.disabled)
            checkbox.setDisabled(checkboxInterface.disabled);
        return checkbox;
    };
    return CheckboxConstructionStrategy;
}());

var DAY;
(function (DAY) {
    DAY[DAY["MONDAY"] = 1] = "MONDAY";
    DAY[DAY["TUESDAY"] = 2] = "TUESDAY";
    DAY[DAY["WEDNESDAY"] = 3] = "WEDNESDAY";
    DAY[DAY["THURSDAY"] = 4] = "THURSDAY";
    DAY[DAY["FRIDAY"] = 5] = "FRIDAY";
    DAY[DAY["SATURDAY"] = 6] = "SATURDAY";
    DAY[DAY["SUNDAY"] = 0] = "SUNDAY";
})(DAY || (DAY = {}));
var Datepicker = /** @class */ (function (_super) {
    __extends(Datepicker, _super);
    function Datepicker() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.startView = 'month';
        _this.color = MAT_COLOR.PRIMARY;
        _this.toggleSideSuffix = true;
        _this.appearance = 'legacy';
        _this.disabledDays = [];
        _this.getAppearance = function () {
            return _this.appearance;
        };
        _this.setAppearance = function (appearance) {
            _this.appearance = appearance;
        };
        _this.getDisabledDays = function () {
            return _this.disabledDays;
        };
        _this.setDisabledDays = function (disabledDays) {
            _this.disabledDays = disabledDays;
        };
        _this.getMatIcon = function () {
            return _this.matIcon;
        };
        _this.setMatIcon = function (matIcon) {
            _this.matIcon = matIcon;
        };
        _this.getLabelName = function () {
            return _this.labelName;
        };
        _this.setLabelName = function (labelName) {
            _this.labelName = labelName;
        };
        _this.getToggleSideSuffix = function () {
            return _this.toggleSideSuffix;
        };
        _this.setToggleSideSuffix = function (toggleSideSuffix) {
            _this.toggleSideSuffix = toggleSideSuffix;
        };
        _this.getColor = function () {
            return _this.color;
        };
        _this.setColor = function (color) {
            _this.color = color;
        };
        _this.getStartView = function () {
            return _this.startView;
        };
        _this.setStartView = function (startView) {
            _this.startView = startView;
        };
        _this.getStartAt = function () {
            return _this.startAt;
        };
        _this.setStartAt = function (startAt) {
            _this.startAt = startAt;
        };
        _this.getMin = function () {
            return _this.min;
        };
        _this.setMin = function (min) {
            _this.min = min;
        };
        _this.getMax = function () {
            return _this.max;
        };
        _this.setMax = function (max) {
            _this.max = max;
        };
        _this.getTimepickerInsideDatepickerInterface = function () {
            return _this.timepickerInsideDatepickerInterface;
        };
        _this.setTimepickerInsideDatepickerInterface = function (timepickerInsideDatepickerInterface) {
            _this.timepickerInsideDatepickerInterface = timepickerInsideDatepickerInterface;
        };
        return _this;
    }
    return Datepicker;
}(FormRowItem));

var DatepickerConstructionStrategy = /** @class */ (function () {
    function DatepickerConstructionStrategy() {
    }
    DatepickerConstructionStrategy.prototype.construct = function (datepickerInterface) {
        var datepicker = new Datepicker();
        datepicker.setType(datepickerInterface.type);
        datepicker.setControlName(datepickerInterface.controlName);
        if (datepickerInterface.appearance)
            datepicker.setAppearance(datepickerInterface.appearance);
        if (datepickerInterface.disabledDays)
            datepicker.setDisabledDays(datepickerInterface.disabledDays);
        if (datepickerInterface.defaultValue) {
            if (datepickerInterface.min) {
                if (datepickerInterface.defaultValue < datepickerInterface.min) {
                    throw Error('Default value Date must be at or after min Date');
                }
            }
            if (datepickerInterface.max) {
                if (datepickerInterface.defaultValue > datepickerInterface.max) {
                    throw Error('Default value Date must be at or before max Date');
                }
            }
        }
        datepicker.setDefaultValue(datepickerInterface.defaultValue);
        if (datepickerInterface.validators) {
            datepickerInterface.validators.forEach(function (validator) {
                if (validator.validatorFn.name === VALIDATOR_NAMES.REQUIRED) {
                    datepicker.addValidator(validator);
                }
            });
        }
        if (datepickerInterface.min) {
            if (datepickerInterface.max) {
                if (datepickerInterface.min > datepickerInterface.max) {
                    throw Error('min Date must be at or before max Date');
                }
            }
        }
        datepicker.setMin(datepickerInterface.min);
        if (datepickerInterface.max) {
            if (datepickerInterface.min) {
                if (datepickerInterface.max < datepickerInterface.min) {
                    throw Error('max Date must be at or after min Date');
                }
            }
        }
        datepicker.setMax(datepickerInterface.max);
        if (datepickerInterface.startAt) {
            if (datepickerInterface.min) {
                if (datepickerInterface.startAt < datepickerInterface.min) {
                    throw Error('StartAt value Date must be at or after min Date');
                }
            }
            if (datepickerInterface.max) {
                if (datepickerInterface.startAt > datepickerInterface.max) {
                    throw Error('StartAt value Date must be at or before max Date');
                }
            }
        }
        datepicker.setStartAt(datepickerInterface.startAt);
        if (datepickerInterface.disabled)
            datepicker.setDisabled(datepickerInterface.disabled);
        if (datepickerInterface.startView)
            datepicker.setStartView(datepickerInterface.startView);
        if (datepickerInterface.color)
            datepicker.setColor(datepickerInterface.color);
        if (datepickerInterface.toggleSideSuffix !== undefined)
            datepicker.setToggleSideSuffix(datepickerInterface.toggleSideSuffix);
        if (datepickerInterface.matIcon)
            datepicker.setMatIcon(datepickerInterface.matIcon);
        if (datepickerInterface.labelName)
            datepicker.setLabelName(datepickerInterface.labelName);
        else
            throw Error('Label name is required');
        if (datepickerInterface.timepicker)
            datepicker.setTimepickerInsideDatepickerInterface(datepickerInterface.timepicker);
        return datepicker;
    };
    return DatepickerConstructionStrategy;
}());

var FORM_FIELD_INPUT_TYPE;
(function (FORM_FIELD_INPUT_TYPE) {
    // FORM_FIELD_INPUT, // text, number(step, min, max), file, email, password // dodati opciju za validacije
    FORM_FIELD_INPUT_TYPE["TEXT"] = "text";
    FORM_FIELD_INPUT_TYPE["NUMBER"] = "number";
    FORM_FIELD_INPUT_TYPE["FILE"] = "file";
    FORM_FIELD_INPUT_TYPE["EMAIL"] = "email";
    FORM_FIELD_INPUT_TYPE["PASSWORD"] = "password";
})(FORM_FIELD_INPUT_TYPE || (FORM_FIELD_INPUT_TYPE = {}));
var FormFieldInput = /** @class */ (function (_super) {
    __extends(FormFieldInput, _super);
    function FormFieldInput() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.appearance = 'legacy';
        _this.readonly = false;
        _this.required = {
            required: false,
            hideRequiredMarker: false,
        };
        _this.getFormFieldInputType = function () {
            return _this.formFieldInputType;
        };
        _this.setFormFieldInputType = function (formFieldInputType) {
            _this.formFieldInputType = formFieldInputType;
        };
        _this.getAppearance = function () {
            return _this.appearance;
        };
        _this.setAppearance = function (appearance) {
            _this.appearance = appearance;
        };
        _this.getPlaceholder = function () {
            return _this.placeholder;
        };
        _this.setPlaceholder = function (placeholder) {
            _this.placeholder = placeholder;
        };
        _this.getReadonly = function () {
            return _this.readonly;
        };
        _this.setReadonly = function (readonly) {
            _this.readonly = readonly;
        };
        _this.getRequired = function () {
            return _this.required;
        };
        _this.setRequired = function (required) {
            _this.required = required;
        };
        _this.getLabelName = function () {
            return _this.labelName;
        };
        _this.setLabelName = function (labelName) {
            _this.labelName = labelName;
        };
        _this.getLeftHintLabel = function () {
            return _this.leftHintLabel;
        };
        _this.setLeftHintLabel = function (leftHintLabel) {
            _this.leftHintLabel = leftHintLabel;
        };
        _this.getRightHintLabel = function () {
            return _this.rightHintLabel;
        };
        _this.setRightHintLabel = function (rightHintLabel) {
            _this.rightHintLabel = rightHintLabel;
        };
        _this.getMatPrefixImgText = function () {
            return _this.matPrefixImgText;
        };
        _this.setMatPrefixImgText = function (matPrefixImgText) {
            _this.matPrefixImgText = matPrefixImgText;
        };
        _this.getMatSuffixImgText = function () {
            return _this.matSuffixImgText;
        };
        _this.setMatSuffixImgText = function (matSuffixImgText) {
            _this.matSuffixImgText = matSuffixImgText;
        };
        _this.getTextSuffix = function () {
            return _this.textSuffix;
        };
        _this.setTextSuffix = function (textSuffix) {
            _this.textSuffix = textSuffix;
        };
        _this.getTextPrefix = function () {
            return _this.textPrefix;
        };
        _this.setTextPrefix = function (textPrefix) {
            _this.textPrefix = textPrefix;
        };
        return _this;
    }
    return FormFieldInput;
}(FormRowItem));

var FormFieldInputPassword = /** @class */ (function (_super) {
    __extends(FormFieldInputPassword, _super);
    function FormFieldInputPassword() {
        var _this = _super.call(this) || this;
        _this.showPassword = false;
        _this.showHintAboutMinMaxLength = false;
        _this.onShowPassword = function () {
            _this.showPassword = true;
            setTimeout(function () {
                _this.showPassword = false;
            }, _this.showPasswordInMs);
        };
        _this.getShowPassword = function () {
            return _this.showPassword;
        };
        _this.getShowPasswordInMs = function () {
            return _this.showPasswordInMs;
        };
        _this.setShowPasswordInMs = function (showPasswordInMs) {
            _this.showPasswordInMs = showPasswordInMs;
        };
        _this.getShowHintAboutMinMaxLength = function () {
            return _this.showHintAboutMinMaxLength;
        };
        _this.setShowHintAboutMinMaxLength = function (showHintAboutMinMaxLength) {
            _this.showHintAboutMinMaxLength = showHintAboutMinMaxLength;
        };
        return _this;
    }
    return FormFieldInputPassword;
}(FormFieldInput));

var FormFieldInputPasswordConstructionStrategy = /** @class */ (function () {
    function FormFieldInputPasswordConstructionStrategy() {
    }
    FormFieldInputPasswordConstructionStrategy.prototype.construct = function (formFieldInputPasswordInterface) {
        var formFieldInputPassword = new FormFieldInputPassword();
        formFieldInputPassword.setControlName(formFieldInputPasswordInterface.controlName);
        if (formFieldInputPasswordInterface.validators)
            formFieldInputPassword.setValidators(formFieldInputPasswordInterface.validators);
        if (formFieldInputPasswordInterface.showPasswordInMs)
            formFieldInputPassword.setShowPasswordInMs(formFieldInputPasswordInterface.showPasswordInMs);
        if (formFieldInputPasswordInterface.showHintAboutMinMaxLength) {
            // check if maxLength or minLength validator exists
            var minOrMaxLengthValidatorExists_1 = false;
            formFieldInputPassword
                .getValidators()
                .forEach(function (validator) {
                if (validator.name == VALIDATOR_NAMES.MIN_LENGTH ||
                    validator.name == VALIDATOR_NAMES.MAX_LENGTH) {
                    minOrMaxLengthValidatorExists_1 = true;
                }
            });
            if (minOrMaxLengthValidatorExists_1) {
                (formFieldInputPassword).setShowHintAboutMinMaxLength(true);
            }
            else {
                throw Error("For showHintAboutMinMaxLength to work field needs to have minLength or maxLength validator");
            }
        }
        if (formFieldInputPasswordInterface.hintLabels) {
            if (formFieldInputPasswordInterface.hintLabels.leftHintLabelContext)
                formFieldInputPassword.setLeftHintLabel(formFieldInputPasswordInterface.hintLabels.leftHintLabelContext);
            if (formFieldInputPasswordInterface.hintLabels.rightHintLabelContext) {
                if ((formFieldInputPassword).getShowHintAboutMinMaxLength()) {
                    console.log(formFieldInputPassword.getControlName() +
                        " - Right Hint Label Context is not allowed cause you are using Show Hint About Min or Max Length");
                }
                else {
                    formFieldInputPassword.setRightHintLabel(formFieldInputPasswordInterface.hintLabels.rightHintLabelContext);
                }
            }
        }
        return formFieldInputPassword;
    };
    return FormFieldInputPasswordConstructionStrategy;
}());

var FormFieldInputNumber = /** @class */ (function (_super) {
    __extends(FormFieldInputNumber, _super);
    function FormFieldInputNumber() {
        var _this = _super.call(this) || this;
        _this.step = 1;
        _this.getStep = function () {
            return _this.step;
        };
        _this.setStep = function (step) {
            _this.step = step;
        };
        return _this;
    }
    return FormFieldInputNumber;
}(FormFieldInput));

var FormFieldInputNumberConstructionStrategy = /** @class */ (function () {
    function FormFieldInputNumberConstructionStrategy() {
    }
    FormFieldInputNumberConstructionStrategy.prototype.construct = function (formFieldInputNumberInterface) {
        var formFieldInputNumber = new FormFieldInputNumber();
        formFieldInputNumber.setControlName(formFieldInputNumberInterface.controlName);
        if (formFieldInputNumberInterface.validators)
            formFieldInputNumber.setValidators(formFieldInputNumberInterface.validators);
        if (formFieldInputNumberInterface.step)
            formFieldInputNumber.setStep(formFieldInputNumberInterface.step);
        if (formFieldInputNumberInterface.hintLabels) {
            if (formFieldInputNumberInterface.hintLabels.leftHintLabelContext)
                formFieldInputNumber.setLeftHintLabel(formFieldInputNumberInterface.hintLabels.leftHintLabelContext);
            if (formFieldInputNumberInterface.hintLabels.rightHintLabelContext)
                formFieldInputNumber.setRightHintLabel(formFieldInputNumberInterface.hintLabels.rightHintLabelContext);
        }
        return formFieldInputNumber;
    };
    return FormFieldInputNumberConstructionStrategy;
}());

var FormFieldInputText = /** @class */ (function (_super) {
    __extends(FormFieldInputText, _super);
    function FormFieldInputText() {
        var _this = _super.call(this) || this;
        _this.showHintAboutMinMaxLength = false;
        _this.getShowHintAboutMinMaxLength = function () {
            return _this.showHintAboutMinMaxLength;
        };
        _this.setShowHintAboutMinMaxLength = function (showHintAboutMinMaxLength) {
            _this.showHintAboutMinMaxLength = showHintAboutMinMaxLength;
        };
        return _this;
    }
    return FormFieldInputText;
}(FormFieldInput));

var FormFieldInputTextConstructionStrategy = /** @class */ (function () {
    function FormFieldInputTextConstructionStrategy() {
    }
    FormFieldInputTextConstructionStrategy.prototype.construct = function (formFieldInputTextInterface) {
        var formFieldInputText = new FormFieldInputText();
        formFieldInputText.setControlName(formFieldInputTextInterface.controlName);
        if (formFieldInputTextInterface.validators)
            formFieldInputText.setValidators(formFieldInputTextInterface.validators);
        if (formFieldInputTextInterface.showHintAboutMinMaxLength) {
            // check if maxLength or minLength validator exists
            var minOrMaxLengthValidatorExists_1 = false;
            formFieldInputText
                .getValidators()
                .forEach(function (validator) {
                if (validator.name == VALIDATOR_NAMES.MIN_LENGTH ||
                    validator.name == VALIDATOR_NAMES.MAX_LENGTH) {
                    minOrMaxLengthValidatorExists_1 = true;
                }
            });
            if (minOrMaxLengthValidatorExists_1) {
                (formFieldInputText).setShowHintAboutMinMaxLength(true);
            }
            else {
                throw Error("For showHintAboutMinMaxLength to work field needs to have minLength or maxLength validator");
            }
        }
        if (formFieldInputTextInterface.hintLabels) {
            if (formFieldInputTextInterface.hintLabels.leftHintLabelContext)
                formFieldInputText.setLeftHintLabel(formFieldInputTextInterface.hintLabels.leftHintLabelContext);
            if (formFieldInputTextInterface.hintLabels.rightHintLabelContext) {
                if ((formFieldInputText).getShowHintAboutMinMaxLength()) {
                    console.log(formFieldInputText.getControlName() +
                        " - Right Hint Label Context is not allowed cause you are using Show Hint About Min or Max Length");
                }
                else {
                    formFieldInputText.setRightHintLabel(formFieldInputTextInterface.hintLabels.rightHintLabelContext);
                }
            }
        }
        return formFieldInputText;
    };
    return FormFieldInputTextConstructionStrategy;
}());

var FormFieldInputFile = /** @class */ (function (_super) {
    __extends(FormFieldInputFile, _super);
    function FormFieldInputFile() {
        var _this = _super.call(this) || this;
        _this.dragAndDrop = false;
        _this.buttonAddText = "Add";
        _this.buttonDeleteText = "Remove";
        _this.buttonAddColor = MAT_COLOR.EMPTY;
        _this.buttonDeleteColor = MAT_COLOR.EMPTY;
        _this.borderColor = "#415fb4";
        _this.allowedFileTypes = [];
        _this.getAllowedFileTypes = function () {
            return _this.allowedFileTypes;
        };
        _this.setAllowedFileTypes = function (allowedFileTypes) {
            _this.allowedFileTypes = allowedFileTypes;
        };
        _this.isFileTypeAllowed = function (type) {
            for (var index in _this.allowedFileTypes) {
                var allowedType = _this.allowedFileTypes[index];
                if (type.includes(allowedType))
                    return true;
            }
            return false;
        };
        _this.getDragAndDrop = function () {
            return _this.dragAndDrop;
        };
        _this.setDragAndDrop = function (dragAndDrop) {
            _this.dragAndDrop = dragAndDrop;
        };
        _this.getButtonAddText = function () {
            return _this.buttonAddText;
        };
        _this.setButtonAddText = function (buttonAddText) {
            _this.buttonAddText = buttonAddText;
        };
        _this.getButtonDeleteText = function () {
            return _this.buttonDeleteText;
        };
        _this.setButtonDeleteText = function (buttonDeleteText) {
            _this.buttonDeleteText = buttonDeleteText;
        };
        _this.getButtonAddColor = function () {
            return _this.buttonAddColor;
        };
        _this.setButtonAddColor = function (buttonAddColor) {
            _this.buttonAddColor = buttonAddColor;
        };
        _this.getButtonDeleteColor = function () {
            return _this.buttonDeleteColor;
        };
        _this.setButtonDeleteColor = function (buttonDeleteColor) {
            _this.buttonDeleteColor = buttonDeleteColor;
        };
        _this.getBorderColor = function () {
            return _this.borderColor;
        };
        _this.setBorderColor = function (borderColor) {
            _this.borderColor = borderColor;
        };
        _this.addFile = function (file) {
            var data = _this.getDefaultValue();
            data.push(file);
            _this.setDefaultValue(data);
        };
        //   override
        _this.setValidators = function (validators) {
            for (var index in validators) {
                if (validators[index].name === VALIDATOR_NAMES.REQUIRED) {
                    _this.validators = [validators[index]];
                    break;
                }
            }
        };
        return _this;
    }
    return FormFieldInputFile;
}(FormFieldInput));

var FormFieldInputFileConstructionStrategy = /** @class */ (function () {
    function FormFieldInputFileConstructionStrategy() {
    }
    FormFieldInputFileConstructionStrategy.prototype.construct = function (formFieldInputFileInterface) {
        var formFieldInputFile = new FormFieldInputFile();
        formFieldInputFile.setControlName(formFieldInputFileInterface.controlName);
        if (formFieldInputFileInterface.allowedFileTypes)
            formFieldInputFile.setAllowedFileTypes(formFieldInputFileInterface.allowedFileTypes);
        if (formFieldInputFileInterface.validators)
            formFieldInputFile.setValidators(formFieldInputFileInterface.validators);
        formFieldInputFileInterface.defaultValue = [];
        if (formFieldInputFileInterface.dragAndDrop)
            formFieldInputFile.setDragAndDrop(formFieldInputFileInterface.dragAndDrop);
        if (formFieldInputFileInterface.buttonAddText)
            formFieldInputFile.setButtonAddText(formFieldInputFileInterface.buttonAddText);
        if (formFieldInputFileInterface.buttonDeleteText)
            formFieldInputFile.setButtonDeleteText(formFieldInputFileInterface.buttonDeleteText);
        if (formFieldInputFileInterface.buttonAddColor)
            formFieldInputFile.setButtonAddColor(formFieldInputFileInterface.buttonAddColor);
        if (formFieldInputFileInterface.buttonDeleteColor)
            formFieldInputFile.setButtonDeleteColor(formFieldInputFileInterface.buttonDeleteColor);
        if (formFieldInputFileInterface.borderColor)
            formFieldInputFile.setBorderColor(formFieldInputFileInterface.borderColor);
        if (formFieldInputFileInterface.hintLabels) {
            if (formFieldInputFileInterface.hintLabels.leftHintLabelContext)
                formFieldInputFile.setLeftHintLabel(formFieldInputFileInterface.hintLabels.leftHintLabelContext);
            if (formFieldInputFileInterface.hintLabels.rightHintLabelContext)
                formFieldInputFile.setRightHintLabel(formFieldInputFileInterface.hintLabels.rightHintLabelContext);
        }
        return formFieldInputFile;
    };
    return FormFieldInputFileConstructionStrategy;
}());

var FormFieldInputEmailConstructionStrategy = /** @class */ (function () {
    function FormFieldInputEmailConstructionStrategy() {
    }
    FormFieldInputEmailConstructionStrategy.prototype.construct = function (formFieldInputInterface) {
        var formFieldInputEmail = new FormFieldInput();
        formFieldInputEmail.setControlName(formFieldInputInterface.controlName);
        if (formFieldInputInterface.validators)
            formFieldInputEmail.setValidators(formFieldInputInterface.validators);
        if (formFieldInputInterface.hintLabels) {
            if (formFieldInputInterface.hintLabels.leftHintLabelContext)
                formFieldInputEmail.setLeftHintLabel(formFieldInputInterface.hintLabels.leftHintLabelContext);
            if (formFieldInputInterface.hintLabels.rightHintLabelContext)
                formFieldInputEmail.setRightHintLabel(formFieldInputInterface.hintLabels.rightHintLabelContext);
        }
        return formFieldInputEmail;
    };
    return FormFieldInputEmailConstructionStrategy;
}());

var FormFieldInputConstructionStrategy = /** @class */ (function () {
    function FormFieldInputConstructionStrategy() {
    }
    FormFieldInputConstructionStrategy.prototype.construct = function (formFieldInputInterface) {
        var formFieldInput;
        if (!formFieldInputInterface.formFieldInputType)
            throw Error('Form Field Input Type is required');
        if (formFieldInputInterface.formFieldInputType ===
            FORM_FIELD_INPUT_TYPE.PASSWORD) {
            formFieldInput = new FormFieldInputPasswordConstructionStrategy().construct(formFieldInputInterface);
        }
        else if (formFieldInputInterface.formFieldInputType ===
            FORM_FIELD_INPUT_TYPE.NUMBER) {
            formFieldInput = new FormFieldInputNumberConstructionStrategy().construct(formFieldInputInterface);
        }
        else if (formFieldInputInterface.formFieldInputType === FORM_FIELD_INPUT_TYPE.TEXT) {
            formFieldInput = new FormFieldInputTextConstructionStrategy().construct(formFieldInputInterface);
        }
        else if (formFieldInputInterface.formFieldInputType === FORM_FIELD_INPUT_TYPE.FILE) {
            formFieldInput = new FormFieldInputFileConstructionStrategy().construct(formFieldInputInterface);
        }
        else if (formFieldInputInterface.formFieldInputType === FORM_FIELD_INPUT_TYPE.EMAIL) {
            formFieldInput = new FormFieldInputEmailConstructionStrategy().construct(formFieldInputInterface);
        }
        formFieldInput.setType(formFieldInputInterface.type);
        formFieldInput.setDefaultValue(formFieldInputInterface.defaultValue);
        formFieldInput.setFormFieldInputType(formFieldInputInterface.formFieldInputType);
        if (formFieldInputInterface.appearance)
            formFieldInput.setAppearance(formFieldInputInterface.appearance);
        if (formFieldInputInterface.placeholder)
            formFieldInput.setPlaceholder(formFieldInputInterface.placeholder);
        if (formFieldInputInterface.disabled)
            formFieldInput.setDisabled(formFieldInputInterface.disabled);
        if (formFieldInputInterface.readonly)
            formFieldInput.setReadonly(formFieldInputInterface.readonly);
        if (formFieldInputInterface.requiredOption) {
            var requiredOption = {
                required: false,
                hideRequiredMarker: false,
            };
            if (formFieldInputInterface.requiredOption.required)
                requiredOption.required =
                    formFieldInputInterface.requiredOption.required;
            if (formFieldInputInterface.requiredOption.hideRequiredMarker)
                requiredOption.hideRequiredMarker =
                    formFieldInputInterface.requiredOption.hideRequiredMarker;
            formFieldInput.setRequired(requiredOption);
            // add required validatorFn if it doesn't exists
            var validatorRequiredFnExists_1 = false;
            formFieldInput
                .getValidators()
                .forEach(function (validator) {
                if (validator.name === VALIDATOR_NAMES.REQUIRED) {
                    validatorRequiredFnExists_1 = true;
                }
            });
            if (!validatorRequiredFnExists_1) {
                formFieldInput.addValidator({
                    message: 'the field is required',
                    name: VALIDATOR_NAMES.REQUIRED,
                    validatorFn: Validators.required,
                });
            }
        }
        else {
            // check if it has required validator if it has add requiredOption.required = true
            var validatorRequiredFnExists_2 = false;
            formFieldInput
                .getValidators()
                .forEach(function (validator) {
                if (validator.name === VALIDATOR_NAMES.REQUIRED) {
                    validatorRequiredFnExists_2 = true;
                }
            });
            if (validatorRequiredFnExists_2) {
                var requiredOption = {
                    required: true,
                    hideRequiredMarker: false,
                };
                formFieldInput.setRequired(requiredOption);
            }
        }
        if (formFieldInputInterface.labelName)
            formFieldInput.setLabelName(formFieldInputInterface.labelName);
        else
            throw Error('Label name is required');
        if (formFieldInputInterface.matImages) {
            if (formFieldInputInterface.matImages.matPrefixImgText)
                formFieldInput.setMatPrefixImgText(formFieldInputInterface.matImages.matPrefixImgText);
            if (formFieldInputInterface.matImages.matSuffixImgText)
                formFieldInput.setMatSuffixImgText(formFieldInputInterface.matImages.matSuffixImgText);
        }
        if (formFieldInputInterface.textPrefix)
            formFieldInput.setTextPrefix(formFieldInputInterface.textPrefix);
        if (formFieldInputInterface.textSuffix)
            formFieldInput.setTextSuffix(formFieldInputInterface.textSuffix);
        return formFieldInput;
    };
    return FormFieldInputConstructionStrategy;
}());

var RadioButton = /** @class */ (function (_super) {
    __extends(RadioButton, _super);
    function RadioButton() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.getName = function () {
            return _this.name;
        };
        _this.getValue = function () {
            return _this.value;
        };
        _this.setName = function (name) {
            _this.name = name;
        };
        _this.setValue = function (value) {
            _this.value = value;
        };
        return _this;
    }
    return RadioButton;
}(FormRowItem));

var RadioButtonGroup = /** @class */ (function (_super) {
    __extends(RadioButtonGroup, _super);
    function RadioButtonGroup() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.direction = 'column';
        _this.labelPosition = 'after';
        _this.getDirection = function () {
            return _this.direction;
        };
        _this.getLabel = function () {
            return _this.label;
        };
        _this.setDirection = function (direction) {
            _this.direction = direction;
        };
        _this.setLabel = function (label) {
            _this.label = label;
        };
        _this.getLabelPosition = function () {
            return _this.labelPosition;
        };
        _this.setLabelPosition = function (labelPosition) {
            _this.labelPosition = labelPosition;
        };
        return _this;
    }
    RadioButtonGroup.prototype.addChild = function (rowButton) {
        this.children.push(rowButton);
        rowButton.setParent(this);
        return true;
    };
    RadioButtonGroup.prototype.addChildInterface = function (radioButtonInterface) {
        var radioButton = new RadioButton();
        radioButton.setName(radioButtonInterface.name);
        radioButton.setValue(radioButtonInterface.value);
        return this.addChild(radioButton);
    };
    RadioButtonGroup.prototype.removeChild = function (rowButton) {
        this.children = this.children.filter(function (child) { return child !== rowButton; });
        rowButton.setParent(undefined);
        return true;
    };
    RadioButtonGroup.prototype.shouldHaveChildren = function () {
        return true;
    };
    return RadioButtonGroup;
}(FormRowItem));

var RadioButtonGroupConstructionStrategy = /** @class */ (function () {
    function RadioButtonGroupConstructionStrategy() {
    }
    RadioButtonGroupConstructionStrategy.prototype.construct = function (radioButtonGroupInterface) {
        var radioButtonGroup = new RadioButtonGroup();
        radioButtonGroup.setType(radioButtonGroupInterface.type);
        if (radioButtonGroupInterface.defaultValue != '') {
            var exists_1 = false;
            radioButtonGroupInterface.options.forEach(function (radioButtonInterface) {
                if (radioButtonInterface.value == radioButtonGroupInterface.defaultValue)
                    exists_1 = true;
            });
            if (!exists_1)
                throw Error('Default value for radio button group should be some of options value');
        }
        radioButtonGroup.setDefaultValue(radioButtonGroupInterface.defaultValue);
        if (radioButtonGroupInterface.validators)
            radioButtonGroup.setValidators(radioButtonGroupInterface.validators);
        if (radioButtonGroupInterface.controlName)
            radioButtonGroup.setControlName(radioButtonGroupInterface.controlName);
        else
            throw Error('Control name is required');
        radioButtonGroup.setDirection(radioButtonGroupInterface.direction);
        if (radioButtonGroupInterface.label)
            radioButtonGroup.setLabel(radioButtonGroupInterface.label);
        if (radioButtonGroupInterface.disabled)
            radioButtonGroup.setDisabled(radioButtonGroupInterface.disabled);
        if (radioButtonGroupInterface.labelPosition)
            radioButtonGroup.setLabelPosition(radioButtonGroupInterface.labelPosition);
        radioButtonGroupInterface.options.forEach(function (radioButtonInterface) {
            radioButtonGroup.addChildInterface(radioButtonInterface);
        });
        return radioButtonGroup;
    };
    return RadioButtonGroupConstructionStrategy;
}());

var Select = /** @class */ (function (_super) {
    __extends(Select, _super);
    function Select() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.multiple = false;
        _this.matSelectTriggerOn = false;
        _this.optionValues = [];
        _this.getOptionValues = function () {
            return _this.optionValues;
        };
        _this.setOptionValues = function (optionValues) {
            _this.optionValues = optionValues;
        };
        _this.getMultiple = function () {
            return _this.multiple;
        };
        _this.setMultiple = function (multiple) {
            _this.multiple = multiple;
        };
        _this.getMatSelectTriggerOn = function () {
            return _this.matSelectTriggerOn;
        };
        _this.setMatSelectTriggerOn = function (matSelectTriggerOn) {
            _this.matSelectTriggerOn = matSelectTriggerOn;
        };
        return _this;
    }
    return Select;
}(FormFieldInput));

var SelectConstructionStrategy = /** @class */ (function () {
    function SelectConstructionStrategy() {
    }
    SelectConstructionStrategy.prototype.construct = function (selectInterface) {
        var select = new Select();
        select.setType(selectInterface.type);
        if (selectInterface.multiple) {
            select.setMultiple(selectInterface.multiple);
            if (selectInterface.defaultValue) {
                if (selectInterface.defaultValue instanceof Array) {
                    select.setDefaultValue(selectInterface.defaultValue);
                }
                else {
                    throw Error('If you want multiple choice default value must be an Array');
                }
            }
        }
        else {
            if (selectInterface.defaultValue) {
                if (selectInterface.defaultValue instanceof Array) {
                    throw Error("If you don't want multiple choice default value must not be an Array");
                }
                else {
                    select.setDefaultValue(selectInterface.defaultValue);
                }
            }
        }
        if (selectInterface.matSelectTriggerOn) {
            if (!select.getMultiple())
                throw Error('MatSelectTriggerOn make sense only if multiple is set to true');
            else
                select.setMatSelectTriggerOn(selectInterface.matSelectTriggerOn);
        }
        select.setControlName(selectInterface.controlName);
        if (selectInterface.validators)
            select.setValidators(selectInterface.validators);
        if (selectInterface.hintLabels) {
            throw Error('Hint Labels are not allowed on select');
        }
        if (selectInterface.appearance)
            select.setAppearance(selectInterface.appearance);
        if (selectInterface.placeholder)
            throw Error('Placeholder is not allowed on select');
        if (selectInterface.disabled)
            select.setDisabled(selectInterface.disabled);
        if (selectInterface.readonly)
            throw Error('Readonly is not allowed on select');
        if (selectInterface.requiredOption) {
            var requiredOption = {
                required: false,
                hideRequiredMarker: false,
            };
            if (selectInterface.requiredOption.required)
                requiredOption.required = selectInterface.requiredOption.required;
            if (selectInterface.requiredOption.hideRequiredMarker)
                requiredOption.hideRequiredMarker =
                    selectInterface.requiredOption.hideRequiredMarker;
            select.setRequired(requiredOption);
            // add required validatorFn if it doesn't exists
            var validatorRequiredFnExists_1 = false;
            select
                .getValidators()
                .forEach(function (validator) {
                if (validator.name === VALIDATOR_NAMES.REQUIRED) {
                    validatorRequiredFnExists_1 = true;
                }
            });
            if (!validatorRequiredFnExists_1) {
                select.addValidator({
                    message: 'the field is required',
                    name: VALIDATOR_NAMES.REQUIRED,
                    validatorFn: Validators.required,
                });
            }
        }
        else {
            // check if it has required validator if it has add requiredOption.required = true
            var validatorRequiredFnExists_2 = false;
            select
                .getValidators()
                .forEach(function (validator) {
                if (validator.name === VALIDATOR_NAMES.REQUIRED) {
                    validatorRequiredFnExists_2 = true;
                }
            });
            if (validatorRequiredFnExists_2) {
                var requiredOption = {
                    required: true,
                    hideRequiredMarker: false,
                };
                select.setRequired(requiredOption);
            }
        }
        if (selectInterface.labelName)
            select.setLabelName(selectInterface.labelName);
        else
            throw Error('Label name is required');
        if (selectInterface.matImages) {
            if (selectInterface.matImages.matPrefixImgText)
                select.setMatPrefixImgText(selectInterface.matImages.matPrefixImgText);
            if (selectInterface.matImages.matSuffixImgText)
                select.setMatSuffixImgText(selectInterface.matImages.matSuffixImgText);
        }
        if (selectInterface.textPrefix)
            select.setTextPrefix(selectInterface.textPrefix);
        if (selectInterface.textSuffix)
            select.setTextSuffix(selectInterface.textSuffix);
        if (selectInterface.optionValues && selectInterface.optionValues.length > 0)
            select.setOptionValues(selectInterface.optionValues);
        else
            throw Error('Option Values are required for select');
        return select;
    };
    return SelectConstructionStrategy;
}());

var SlideToggle = /** @class */ (function (_super) {
    __extends(SlideToggle, _super);
    function SlideToggle() {
        var _this = _super.call(this) || this;
        _this.color = MAT_COLOR.PRIMARY;
        _this.context = '';
        _this.getColor = function () {
            return _this.color;
        };
        _this.setColor = function (color) {
            _this.color = color;
        };
        _this.getContext = function () {
            return _this.context;
        };
        _this.setContext = function (context) {
            _this.context = context;
        };
        return _this;
    }
    return SlideToggle;
}(FormRowItem));

var SlideToggleConstructionStrategy = /** @class */ (function () {
    function SlideToggleConstructionStrategy() {
    }
    SlideToggleConstructionStrategy.prototype.construct = function (slideToggleInterface) {
        var slideToggle = new SlideToggle();
        slideToggle.setType(slideToggleInterface.type);
        if (slideToggleInterface.defaultValue != '' &&
            slideToggleInterface.defaultValue != true &&
            slideToggleInterface.defaultValue != false)
            throw new Error('Default value for slide-toggle must be boolean');
        else if (slideToggleInterface.defaultValue == '')
            slideToggle.setDefaultValue(false);
        else
            slideToggle.setDefaultValue(slideToggleInterface.defaultValue);
        slideToggle.setControlName(slideToggleInterface.controlName);
        if (slideToggleInterface.validators)
            slideToggle.setValidators(slideToggleInterface.validators);
        if (slideToggleInterface.color)
            slideToggle.setColor(slideToggleInterface.color);
        if (slideToggleInterface.disabled)
            slideToggle.setDisabled(slideToggleInterface.disabled);
        if (slideToggleInterface.context)
            slideToggle.setContext(slideToggleInterface.context);
        return slideToggle;
    };
    return SlideToggleConstructionStrategy;
}());

var Slider = /** @class */ (function (_super) {
    __extends(Slider, _super);
    function Slider() {
        var _this = _super.call(this) || this;
        _this.vertical = false;
        _this.thumbLabel = false;
        _this.invert = false;
        _this.step = 1;
        _this.tickInterval = 0;
        _this.getMinValue = function () {
            return _this.minValue;
        };
        _this.setMinValue = function (minValue) {
            _this.minValue = minValue;
        };
        _this.getMaxValue = function () {
            return _this.maxValue;
        };
        _this.setMaxValue = function (maxValue) {
            _this.maxValue = maxValue;
        };
        _this.getInvert = function () {
            return _this.invert;
        };
        _this.setInvert = function (invert) {
            _this.invert = invert;
        };
        _this.getVertical = function () {
            return _this.vertical;
        };
        _this.setVertical = function (vertical) {
            _this.vertical = vertical;
        };
        _this.getThumbLabel = function () {
            return _this.thumbLabel;
        };
        _this.setThumbLabel = function (thumbLabel) {
            _this.thumbLabel = thumbLabel;
        };
        _this.getStep = function () {
            return _this.step;
        };
        _this.setStep = function (step) {
            _this.step = step;
        };
        _this.getTickInterval = function () {
            return _this.tickInterval;
        };
        _this.setTickInterval = function (tickInterval) {
            _this.tickInterval = tickInterval;
        };
        return _this;
    }
    return Slider;
}(FormRowItem));

// ------------- PREDEFINED REGEX ----------------------
// https://stackoverflow.com/questions/19605150/regex-for-password-must-contain-at-least-eight-characters-at-least-one-number-a
var TEXT_ONLY_REGEX = /^[a-zA-Z]+$/;
var NUMBERS_ONLY_REGEX = /^[0-9]*$/;
var AT_LEAST_ONE_DIGIT_REGEX = /^(?=.*?[0-9])/;
var AT_LEAST_ONE_UPPER_CASE_REGEX = /^(?=.*?[A-Z])/;
var AT_LEAST_ONE_LOWER_CASE_REGEX = /^(?=.*?[a-z])/;
var AT_LEAST_ONE_SPECIAL_CHARACTER_REGEX = /^(?=.*?[#?!@$%^&*-])/;
var AT_LEAST_ONE_LOWER_CASE_CHARACTER_ONE_UPPER_CASE_CHARACTER_ONE_DIGIT_ONE_SPECIAL_CHARACTER_REGEX = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*?[#?!@$%^&*-])(?!.*\s).*$/;
var EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
// ----------------------------------------------------------

var SliderConstructionStrategy = /** @class */ (function () {
    function SliderConstructionStrategy() {
    }
    SliderConstructionStrategy.prototype.construct = function (sliderInterface) {
        var slider = new Slider();
        slider.setType(sliderInterface.type);
        slider.setDefaultValue(sliderInterface.defaultValue);
        slider.setControlName(sliderInterface.controlName);
        if (sliderInterface.validators)
            slider.setValidators(sliderInterface.validators);
        slider.addValidator({
            message: "slider's value must be numeric",
            name: VALIDATOR_NAMES.PATTERN,
            validatorFn: Validators.pattern(NUMBERS_ONLY_REGEX),
        });
        if (sliderInterface.minValue) {
            slider.setMinValue(sliderInterface.minValue);
            if (sliderInterface.validators) {
                var minValidatorExists_1 = false;
                sliderInterface.validators.forEach(function (validator) {
                    if (validator.name === VALIDATOR_NAMES.MIN) {
                        minValidatorExists_1 = true;
                    }
                });
                if (!minValidatorExists_1) {
                    slider.addValidator({
                        message: "slider's value must be higher than " +
                            slider.getMinValue().toString(),
                        name: VALIDATOR_NAMES.MIN,
                        validatorFn: Validators.min(+slider.getMinValue()),
                    });
                }
            }
            else {
                slider.addValidator({
                    message: "slider's value must be higher than " +
                        slider.getMinValue().toString(),
                    name: VALIDATOR_NAMES.MIN,
                    validatorFn: Validators.min(+slider.getMinValue()),
                });
            }
        }
        if (sliderInterface.maxValue) {
            slider.setMaxValue(sliderInterface.maxValue);
            if (sliderInterface.validators) {
                var maxValidatorExists_1 = false;
                sliderInterface.validators.forEach(function (validator) {
                    if (validator.name === VALIDATOR_NAMES.MAX) {
                        maxValidatorExists_1 = true;
                    }
                });
                if (!maxValidatorExists_1) {
                    slider.addValidator({
                        message: "slider's value must be lower than " +
                            slider.getMaxValue().toString(),
                        name: VALIDATOR_NAMES.MAX,
                        validatorFn: Validators.max(+slider.getMaxValue()),
                    });
                }
            }
            else {
                slider.addValidator({
                    message: "slider's value must be lower than " +
                        slider.getMaxValue().toString(),
                    name: VALIDATOR_NAMES.MAX,
                    validatorFn: Validators.max(+slider.getMaxValue()),
                });
            }
        }
        if (sliderInterface.disabled)
            slider.setDisabled(sliderInterface.disabled);
        if (sliderInterface.invert)
            slider.setInvert(sliderInterface.invert);
        if (sliderInterface.step)
            slider.setStep(sliderInterface.step);
        if (sliderInterface.tickInterval)
            slider.setTickInterval(sliderInterface.tickInterval);
        if (sliderInterface.thumbLabel)
            slider.setThumbLabel(sliderInterface.thumbLabel);
        if (sliderInterface.vertical)
            slider.setVertical(sliderInterface.vertical);
        return slider;
    };
    return SliderConstructionStrategy;
}());

var Textarea = /** @class */ (function (_super) {
    __extends(Textarea, _super);
    function Textarea() {
        var _this = _super.call(this) || this;
        _this.showHintAboutMinMaxLength = false;
        _this.getShowHintAboutMinMaxLength = function () {
            return _this.showHintAboutMinMaxLength;
        };
        _this.setShowHintAboutMinMaxLength = function (showHintAboutMinMaxLength) {
            _this.showHintAboutMinMaxLength = showHintAboutMinMaxLength;
        };
        return _this;
    }
    return Textarea;
}(FormFieldInput));

var TextareaConstructionStrategy = /** @class */ (function () {
    function TextareaConstructionStrategy() {
    }
    TextareaConstructionStrategy.prototype.construct = function (textareaInterface) {
        var textarea = new Textarea();
        textarea.setType(textareaInterface.type);
        textarea.setDefaultValue(textareaInterface.defaultValue);
        textarea.setControlName(textareaInterface.controlName);
        if (textareaInterface.validators)
            textarea.setValidators(textareaInterface.validators);
        if (textareaInterface.showHintAboutMinMaxLength) {
            // check if maxLength or minLength validator exists
            var minOrMaxLengthValidatorExists_1 = false;
            textarea
                .getValidators()
                .forEach(function (validator) {
                if (validator.name == VALIDATOR_NAMES.MIN_LENGTH ||
                    validator.name == VALIDATOR_NAMES.MAX_LENGTH) {
                    minOrMaxLengthValidatorExists_1 = true;
                }
            });
            if (minOrMaxLengthValidatorExists_1) {
                (textarea).setShowHintAboutMinMaxLength(true);
            }
            else {
                throw Error("For showHintAboutMinMaxLength to work field needs to have minLength or maxLength validator");
            }
        }
        if (textareaInterface.hintLabels) {
            if (textareaInterface.hintLabels.leftHintLabelContext)
                textarea.setLeftHintLabel(textareaInterface.hintLabels.leftHintLabelContext);
            if (textareaInterface.hintLabels.rightHintLabelContext) {
                if ((textarea).getShowHintAboutMinMaxLength()) {
                    console.log(textarea.getControlName() +
                        " - Right Hint Label Context is not allowed cause you are using Show Hint About Min or Max Length");
                }
                else {
                    textarea.setRightHintLabel(textareaInterface.hintLabels.rightHintLabelContext);
                }
            }
        }
        if (textareaInterface.appearance)
            textarea.setAppearance(textareaInterface.appearance);
        if (textareaInterface.placeholder)
            textarea.setPlaceholder(textareaInterface.placeholder);
        if (textareaInterface.disabled)
            textarea.setDisabled(textareaInterface.disabled);
        if (textareaInterface.readonly)
            textarea.setReadonly(textareaInterface.readonly);
        if (textareaInterface.requiredOption) {
            var requiredOption = {
                required: false,
                hideRequiredMarker: false,
            };
            if (textareaInterface.requiredOption.required)
                requiredOption.required = textareaInterface.requiredOption.required;
            if (textareaInterface.requiredOption.hideRequiredMarker)
                requiredOption.hideRequiredMarker =
                    textareaInterface.requiredOption.hideRequiredMarker;
            textarea.setRequired(requiredOption);
            // add required validatorFn if it doesn't exists
            var validatorRequiredFnExists_1 = false;
            textarea
                .getValidators()
                .forEach(function (validator) {
                if (validator.name === VALIDATOR_NAMES.REQUIRED) {
                    validatorRequiredFnExists_1 = true;
                }
            });
            if (!validatorRequiredFnExists_1) {
                textarea.addValidator({
                    message: "the field is required",
                    name: VALIDATOR_NAMES.REQUIRED,
                    validatorFn: Validators.required,
                });
            }
        }
        else {
            // check if it has required validator if it has add requiredOption.required = true
            var validatorRequiredFnExists_2 = false;
            textarea
                .getValidators()
                .forEach(function (validator) {
                if (validator.name === VALIDATOR_NAMES.REQUIRED) {
                    validatorRequiredFnExists_2 = true;
                }
            });
            if (validatorRequiredFnExists_2) {
                var requiredOption = {
                    required: true,
                    hideRequiredMarker: false,
                };
                textarea.setRequired(requiredOption);
            }
        }
        if (textareaInterface.labelName)
            textarea.setLabelName(textareaInterface.labelName);
        else
            throw Error("Label name is required");
        if (textareaInterface.matImages) {
            if (textareaInterface.matImages.matPrefixImgText)
                textarea.setMatPrefixImgText(textareaInterface.matImages.matPrefixImgText);
            if (textareaInterface.matImages.matSuffixImgText)
                textarea.setMatSuffixImgText(textareaInterface.matImages.matSuffixImgText);
        }
        if (textareaInterface.textPrefix)
            textarea.setTextPrefix(textareaInterface.textPrefix);
        if (textareaInterface.textSuffix)
            textarea.setTextSuffix(textareaInterface.textSuffix);
        return textarea;
    };
    return TextareaConstructionStrategy;
}());

var Autocomplete = /** @class */ (function (_super) {
    __extends(Autocomplete, _super);
    function Autocomplete() {
        var _this = _super.call(this) || this;
        _this._sortingOptions = [];
        _this._inputPlaceholder = '';
        _this._criteria = '';
        _this._appearance = 'fill';
        _this._valueSubject = new Subject();
        _this._hiddenSubject = new BehaviorSubject(false);
        _this.getSortingOptions = function () {
            return _this._sortingOptions;
        };
        _this.setSortingOptions = function (sortingOptions) {
            _this._sortingOptions = sortingOptions;
            return true;
        };
        _this.getPageDataSource = function () {
            return _this._pageDataSource;
        };
        _this.setPageDataSource = function (pageDataSource) {
            _this._pageDataSource = pageDataSource;
            return true;
        };
        _this.getInputPlaceholder = function () {
            return _this._inputPlaceholder;
        };
        _this.setInputPlaceholder = function (inputPlaceholder) {
            _this._inputPlaceholder = inputPlaceholder;
            return true;
        };
        _this.getCriteria = function () {
            return _this._criteria;
        };
        _this.setCriteria = function (criteria) {
            _this._criteria = criteria;
            return true;
        };
        _this.getAppearance = function () {
            return _this._appearance;
        };
        _this.setAppearance = function (appearance) {
            _this._appearance = appearance;
            return true;
        };
        _this.getValueSubject = function () {
            return _this._valueSubject;
        };
        _this.getValue = function () {
            return _this._value;
        };
        _this.setValue = function (value) {
            _this._value = value;
            return true;
        };
        _this.getHiddenSubject = function () {
            return _this._hiddenSubject;
        };
        _this.setHiddenSubject = function (hiddenSubject) {
            _this._hiddenSubject = hiddenSubject;
        };
        return _this;
    }
    return Autocomplete;
}(FormRowItem));

var AutocompleteConstructionStrategy = /** @class */ (function () {
    function AutocompleteConstructionStrategy() {
    }
    AutocompleteConstructionStrategy.prototype.construct = function (autocompleteInterface) {
        var autocomplete = new Autocomplete();
        autocomplete.setType(autocompleteInterface.type);
        autocomplete.setDefaultValue(autocompleteInterface.defaultValue);
        autocomplete.setControlName(autocompleteInterface.controlName);
        if (autocompleteInterface.validators)
            autocomplete.setValidators(autocompleteInterface.validators);
        if (autocompleteInterface.disabled)
            autocomplete.setDisabled(autocompleteInterface.disabled);
        if (autocompleteInterface.sortingOptions)
            autocomplete.setSortingOptions(autocompleteInterface.sortingOptions);
        if (autocompleteInterface.inputPlaceHolder)
            autocomplete.setInputPlaceholder(autocompleteInterface.inputPlaceHolder);
        if (autocompleteInterface.pageDataSource)
            autocomplete.setPageDataSource(autocompleteInterface.pageDataSource);
        else
            throw Error('PageDataSource must be provided');
        if (autocompleteInterface.criteria)
            autocomplete.setCriteria(autocompleteInterface.criteria);
        if (autocompleteInterface.appearance)
            autocomplete.setAppearance(autocompleteInterface.appearance);
        if (autocompleteInterface.value)
            autocomplete.setValue(autocompleteInterface.value);
        if (autocompleteInterface.hiddenSubject)
            autocomplete.setHiddenSubject(autocompleteInterface.hiddenSubject);
        return autocomplete;
    };
    return AutocompleteConstructionStrategy;
}());

var Timepicker = /** @class */ (function (_super) {
    __extends(Timepicker, _super);
    function Timepicker() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.appearance = 'legacy';
        _this.labelName = 'Pick time';
        _this.buttonAlign = 'right';
        _this.getAppearance = function () {
            return _this.appearance;
        };
        _this.setAppearance = function (appearance) {
            _this.appearance = appearance;
        };
        _this.getButtonAlign = function () {
            return _this.buttonAlign;
        };
        _this.setButtonAlign = function (buttonAlign) {
            _this.buttonAlign = buttonAlign;
        };
        _this.getMax = function () {
            return _this.max;
        };
        _this.setMax = function (max) {
            _this.max = max;
        };
        _this.getMin = function () {
            return _this.min;
        };
        _this.setMin = function (min) {
            _this.min = min;
        };
        _this.getLabelName = function () {
            return _this.labelName;
        };
        _this.setLabelName = function (labelName) {
            _this.labelName = labelName;
        };
        return _this;
    }
    return Timepicker;
}(FormRowItem));

var TimepickerConstructionStrategy = /** @class */ (function () {
    function TimepickerConstructionStrategy() {
    }
    TimepickerConstructionStrategy.prototype.construct = function (timepickerInterface) {
        var timepicker = new Timepicker();
        timepicker.setType(timepickerInterface.type);
        timepicker.setControlName(timepickerInterface.controlName);
        if (timepickerInterface.validators)
            timepicker.setValidators(timepickerInterface.validators);
        if (timepickerInterface.defaultValue)
            timepicker.setDefaultValue(timepickerInterface.defaultValue);
        if (timepickerInterface.disabled)
            timepicker.setDisabled(timepickerInterface.disabled);
        if (timepickerInterface.appearance)
            timepicker.setAppearance(timepickerInterface.appearance);
        if (timepickerInterface.min)
            timepicker.setMin(timepickerInterface.min);
        if (timepickerInterface.max)
            timepicker.setMax(timepickerInterface.max);
        if (timepickerInterface.labelName)
            timepicker.setLabelName(timepickerInterface.labelName);
        if (timepickerInterface.buttonAlign)
            timepicker.setButtonAlign(timepickerInterface.buttonAlign);
        return timepicker;
    };
    return TimepickerConstructionStrategy;
}());

var FormRow = /** @class */ (function (_super) {
    __extends(FormRow, _super);
    function FormRow() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.disableAllRowItems = function () {
            _this.children.forEach(function (rowItem) {
                rowItem.setDisabled(true);
            });
        };
        return _this;
    }
    FormRow.prototype.addChild = function (formRowItem) {
        this.children.push(formRowItem);
        formRowItem.setParent(this);
        try {
            this.getParent()
                .getFormGroup()
                .addControl(formRowItem.getControlName(), new FormControl(formRowItem.getDefaultValue(), formRowItem.getValidatorFns()));
        }
        catch (error) {
            throw Error('Add form row into form first');
        }
        return true;
    };
    FormRow.prototype.addChildInterface = function (formRowItemInterface) {
        if (formRowItemInterface.defaultValue === undefined ||
            formRowItemInterface.defaultValue === null)
            formRowItemInterface.defaultValue = '';
        if (!formRowItemInterface.controlName)
            throw Error('Control Name is required');
        // strategy pattern
        if (formRowItemInterface.type === ROW_ITEM_TYPE.RADIO_BUTTON_GROUP) {
            return this.addChild(new RadioButtonGroupConstructionStrategy().construct(formRowItemInterface));
        }
        else if (formRowItemInterface.type === ROW_ITEM_TYPE.CHECKBOX) {
            return this.addChild(new CheckboxConstructionStrategy().construct(formRowItemInterface));
        }
        else if (formRowItemInterface.type === ROW_ITEM_TYPE.FORM_FIELD_INPUT) {
            return this.addChild(new FormFieldInputConstructionStrategy().construct(formRowItemInterface));
        }
        else if (formRowItemInterface.type === ROW_ITEM_TYPE.DATEPICKER) {
            return this.addChild(new DatepickerConstructionStrategy().construct(formRowItemInterface));
        }
        else if (formRowItemInterface.type === ROW_ITEM_TYPE.FORM_FIELD_TEXTAREA) {
            return this.addChild(new TextareaConstructionStrategy().construct(formRowItemInterface));
        }
        else if (formRowItemInterface.type === ROW_ITEM_TYPE.FORM_FIELD_SELECT) {
            return this.addChild(new SelectConstructionStrategy().construct(formRowItemInterface));
        }
        else if (formRowItemInterface.type === ROW_ITEM_TYPE.SLIDER) {
            return this.addChild(new SliderConstructionStrategy().construct(formRowItemInterface));
        }
        else if (formRowItemInterface.type === ROW_ITEM_TYPE.SLIDE_TOGGLE) {
            return this.addChild(new SlideToggleConstructionStrategy().construct(formRowItemInterface));
        }
        else if (formRowItemInterface.type === ROW_ITEM_TYPE.BUTTON) {
            return this.addChild(new ButtonConstructionStrategy().construct(formRowItemInterface));
        }
        else if (formRowItemInterface.type === ROW_ITEM_TYPE.AUTOCOMPLETE) {
            return this.addChild(new AutocompleteConstructionStrategy().construct(formRowItemInterface));
        }
        else if (formRowItemInterface.type === ROW_ITEM_TYPE.TIMEPICKER) {
            return this.addChild(new TimepickerConstructionStrategy().construct(formRowItemInterface));
        }
        throw Error('Row item type unknown');
    };
    FormRow.prototype.removeChild = function (formRowItem) {
        this.children = this.children.filter(function (child) { return child !== formRowItem; });
        formRowItem.setParent(undefined);
        try {
            this.getParent()
                .getFormGroup()
                .removeControl(formRowItem.getControlName());
        }
        catch (error) {
            throw Error('Add form row into form first');
        }
        return true;
    };
    FormRow.prototype.shouldHaveChildren = function () {
        return true;
    };
    return FormRow;
}(Collection));

var Form = /** @class */ (function (_super) {
    __extends(Form, _super);
    function Form(formInterface) {
        var _this = _super.call(this) || this;
        // opcija za progress-spinner ili progress-bar
        _this.showSubmitButton = false;
        _this.submitOnlyIfFormValid = false;
        _this.showCancelButton = false;
        _this.showResetButton = false;
        _this.submitButtonText = 'Submit';
        _this.cancelButtonText = 'Cancel';
        _this.resetButtonText = 'Reset';
        _this._formGroup = new FormGroup({});
        _this.controlNames = [];
        _this.disableAllRowItems = function () {
            console.log('disable');
            _this.children.forEach(function (row) {
                row.disableAllRowItems();
            });
        };
        _this.getFormGroup = function () {
            return _this._formGroup;
        };
        _this.setFormGroup = function (formGroup) {
            _this._formGroup = formGroup;
        };
        if (formInterface.showSubmitButton)
            _this.showSubmitButton = formInterface.showSubmitButton;
        if (formInterface.submitOnlyIfFormValid)
            _this.submitOnlyIfFormValid = formInterface.submitOnlyIfFormValid;
        if (formInterface.showCancelButton)
            _this.showCancelButton = formInterface.showCancelButton;
        if (formInterface.showResetButton)
            _this.showResetButton = formInterface.showResetButton;
        if (formInterface.submitButtonText)
            _this.submitButtonText = formInterface.submitButtonText;
        if (formInterface.cancelButtonText)
            _this.cancelButtonText = formInterface.cancelButtonText;
        if (formInterface.resetButtonText)
            _this.resetButtonText = formInterface.resetButtonText;
        return _this;
    }
    Form.prototype.addChild = function (formRow) {
        formRow.getChildren().forEach(function (rowItem) {
            formRow.addChild(rowItem);
        });
        this.children.push(formRow);
        formRow.setParent(this);
        // console.log(this._formGroup.value);
        return true;
    };
    Form.prototype.removeChild = function (formRow) {
        formRow.getChildren().forEach(function (rowItem) {
            formRow.removeChild(rowItem);
        });
        this.children = this.children.filter(function (child) { return child !== formRow; });
        formRow.setParent(undefined);
        // console.log(this._formGroup.value);
        return true;
    };
    Form.prototype.shouldHaveChildren = function () {
        return true;
    };
    return Form;
}(Collection));

var DISCRETE_TYPES;
(function (DISCRETE_TYPES) {
    DISCRETE_TYPES["APPLICATION"] = "application";
    DISCRETE_TYPES["AUDIO"] = "audio";
    DISCRETE_TYPES["FONT"] = "font";
    DISCRETE_TYPES["IMAGE"] = "image";
    DISCRETE_TYPES["TEXT"] = "text";
    DISCRETE_TYPES["VIDEO"] = "video";
})(DISCRETE_TYPES || (DISCRETE_TYPES = {}));
var MULTIPART_TYPES;
(function (MULTIPART_TYPES) {
    MULTIPART_TYPES["MESSAGE"] = "message";
    MULTIPART_TYPES["MULTIPART"] = "multipart";
})(MULTIPART_TYPES || (MULTIPART_TYPES = {}));
var APPLICATION_TYPES;
(function (APPLICATION_TYPES) {
    APPLICATION_TYPES["APPLICATION_PDF"] = "application/pdf";
})(APPLICATION_TYPES || (APPLICATION_TYPES = {}));
var TEXT_TYPES;
(function (TEXT_TYPES) {
    TEXT_TYPES["TEXT_PLAIN"] = "text/plain";
    TEXT_TYPES["TEXT_CSS"] = "text/css";
    TEXT_TYPES["TEXT_HTML"] = "text/html";
    TEXT_TYPES["TEXT_JAVASCRIPT"] = "text/javascript";
    TEXT_TYPES["TEXT_XML"] = "text/xml";
})(TEXT_TYPES || (TEXT_TYPES = {}));
var AUDIO_TYPES;
(function (AUDIO_TYPES) {
    AUDIO_TYPES["AUDIO_WAV"] = "audio/wav";
    AUDIO_TYPES["AUDIO_MP3"] = "audio/mp3";
    AUDIO_TYPES["AUDIO_FLAC"] = "audio/flac";
})(AUDIO_TYPES || (AUDIO_TYPES = {}));
var IMAGE_TYPES;
(function (IMAGE_TYPES) {
    IMAGE_TYPES["JPEG"] = "image/jpeg";
    IMAGE_TYPES["PNG"] = "image/png";
    IMAGE_TYPES["SVG"] = "image/svg+xml";
})(IMAGE_TYPES || (IMAGE_TYPES = {}));

/*
 * Public API Surface of my-angular-form
 */

/**
 * Generated bundle index. Do not edit.
 */

export { APPLICATION_TYPES, AT_LEAST_ONE_DIGIT_REGEX, AT_LEAST_ONE_LOWER_CASE_CHARACTER_ONE_UPPER_CASE_CHARACTER_ONE_DIGIT_ONE_SPECIAL_CHARACTER_REGEX, AT_LEAST_ONE_LOWER_CASE_REGEX, AT_LEAST_ONE_SPECIAL_CHARACTER_REGEX, AT_LEAST_ONE_UPPER_CASE_REGEX, AUDIO_TYPES, Autocomplete, AutocompleteComponent, AutocompleteConstructionStrategy, BUTTON_TYPE, Button, ButtonComponent, ButtonConstructionStrategy, Checkbox, CheckboxComponent, CheckboxConstructionStrategy, Collection, DAY, DISCRETE_TYPES, Datepicker, DatepickerComponent, DatepickerConstructionStrategy, DeviceService, DragDropDirective, EMAIL_REGEX, FORM_FIELD_INPUT_TYPE, Form, FormFieldInput, FormFieldInputConstructionStrategy, FormFieldInputEmailComponent, FormFieldInputEmailConstructionStrategy, FormFieldInputFile, FormFieldInputFileComponent, FormFieldInputFileConstructionStrategy, FormFieldInputNumber, FormFieldInputNumberComponent, FormFieldInputNumberConstructionStrategy, FormFieldInputPassword, FormFieldInputPasswordComponent, FormFieldInputPasswordConstructionStrategy, FormFieldInputText, FormFieldInputTextComponent, FormFieldInputTextConstructionStrategy, FormRow, FormRowItem, IMAGE_TYPES, MAT_BUTTON_TYPE, MAT_COLOR, MULTIPART_TYPES, MY_FORMATS, MaterialModule, MyAngularFormComponent, MyAngularFormModule, NUMBERS_ONLY_REGEX, ROW_ITEM_TYPE, RadioButton, RadioButtonComponent, RadioButtonGroup, RadioButtonGroupConstructionStrategy, Select, SelectComponent, SelectConstructionStrategy, SharedModule, SlideToggle, SlideToggleComponent, SlideToggleConstructionStrategy, Slider, SliderComponent, SliderConstructionStrategy, TEXT_ONLY_REGEX, TEXT_TYPES, Textarea, TextareaComponent, TextareaConstructionStrategy, Timepicker, TimepickerComponent, TimepickerConstructionStrategy, UiService, UploadDialogComponent, UploadService, VALIDATOR_NAMES };
//# sourceMappingURL=my-angular-form.js.map
