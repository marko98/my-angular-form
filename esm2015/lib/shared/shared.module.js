import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from './material.module';
import { HttpClientModule } from '@angular/common/http';
import { DeviceDetectorModule } from 'ngx-device-detector';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
// component
import { RadioButtonComponent } from './ui/radio-button/radio-button.component';
import { CheckboxComponent } from './ui/checkbox/checkbox.component';
import { UploadDialogComponent } from './ui/upload-dialog/upload-dialog.component';
import { DragDropDirective } from './directive/drag-drop.directive';
import { FormFieldInputTextComponent } from './ui/form-field-input/form-field-input-text/form-field-input-text.component';
import { FormFieldInputPasswordComponent } from './ui/form-field-input/form-field-input-password/form-field-input-password.component';
import { FormFieldInputNumberComponent } from './ui/form-field-input/form-field-input-number/form-field-input-number.component';
import { FormFieldInputFileComponent } from './ui/form-field-input/form-field-input-file/form-field-input-file.component';
import { FormFieldInputEmailComponent } from './ui/form-field-input/form-field-input-email/form-field-input-email.component';
import { DatepickerComponent } from './ui/datepicker/datepicker.component';
import { TextareaComponent } from './ui/textarea/textarea.component';
import { SelectComponent } from './ui/select/select.component';
import { SliderComponent } from './ui/slider/slider.component';
import { SlideToggleComponent } from './ui/slide-toggle/slide-toggle.component';
import { ButtonComponent } from './ui/button/button.component';
import { AutocompleteComponent } from './ui/autocomplete/autocomplete.component';
import { AutocompleteModule } from 'autocomplete';
import { TimepickerComponent } from './ui/timepicker/timepicker.component';
import * as i0 from "@angular/core";
import * as i1 from "ngx-device-detector";
export class SharedModule {
}
SharedModule.ɵmod = i0.ɵɵdefineNgModule({ type: SharedModule });
SharedModule.ɵinj = i0.ɵɵdefineInjector({ factory: function SharedModule_Factory(t) { return new (t || SharedModule)(); }, imports: [[
            CommonModule,
            FormsModule,
            ReactiveFormsModule,
            FlexLayoutModule,
            MaterialModule,
            HttpClientModule,
            DeviceDetectorModule.forRoot(),
            AutocompleteModule,
            NgxMaterialTimepickerModule,
        ],
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        FlexLayoutModule,
        MaterialModule,
        HttpClientModule,
        DeviceDetectorModule,
        AutocompleteModule,
        NgxMaterialTimepickerModule] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(SharedModule, { declarations: [RadioButtonComponent,
        CheckboxComponent,
        UploadDialogComponent,
        DragDropDirective,
        FormFieldInputTextComponent,
        FormFieldInputPasswordComponent,
        FormFieldInputNumberComponent,
        FormFieldInputFileComponent,
        FormFieldInputEmailComponent,
        DatepickerComponent,
        TextareaComponent,
        SelectComponent,
        SliderComponent,
        SlideToggleComponent,
        ButtonComponent,
        AutocompleteComponent,
        TimepickerComponent], imports: [CommonModule,
        FormsModule,
        ReactiveFormsModule,
        FlexLayoutModule,
        MaterialModule,
        HttpClientModule, i1.DeviceDetectorModule, AutocompleteModule,
        NgxMaterialTimepickerModule], exports: [CommonModule,
        FormsModule,
        ReactiveFormsModule,
        FlexLayoutModule,
        MaterialModule,
        HttpClientModule,
        DeviceDetectorModule,
        AutocompleteModule,
        NgxMaterialTimepickerModule,
        RadioButtonComponent,
        CheckboxComponent,
        DragDropDirective,
        FormFieldInputTextComponent,
        FormFieldInputPasswordComponent,
        FormFieldInputNumberComponent,
        FormFieldInputFileComponent,
        FormFieldInputEmailComponent,
        DatepickerComponent,
        TextareaComponent,
        SelectComponent,
        SliderComponent,
        SlideToggleComponent,
        ButtonComponent,
        AutocompleteComponent,
        TimepickerComponent] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(SharedModule, [{
        type: NgModule,
        args: [{
                declarations: [
                    RadioButtonComponent,
                    CheckboxComponent,
                    UploadDialogComponent,
                    DragDropDirective,
                    FormFieldInputTextComponent,
                    FormFieldInputPasswordComponent,
                    FormFieldInputNumberComponent,
                    FormFieldInputFileComponent,
                    FormFieldInputEmailComponent,
                    DatepickerComponent,
                    TextareaComponent,
                    SelectComponent,
                    SliderComponent,
                    SlideToggleComponent,
                    ButtonComponent,
                    AutocompleteComponent,
                    TimepickerComponent,
                ],
                imports: [
                    CommonModule,
                    FormsModule,
                    ReactiveFormsModule,
                    FlexLayoutModule,
                    MaterialModule,
                    HttpClientModule,
                    DeviceDetectorModule.forRoot(),
                    AutocompleteModule,
                    NgxMaterialTimepickerModule,
                ],
                exports: [
                    CommonModule,
                    FormsModule,
                    ReactiveFormsModule,
                    FlexLayoutModule,
                    MaterialModule,
                    HttpClientModule,
                    DeviceDetectorModule,
                    AutocompleteModule,
                    NgxMaterialTimepickerModule,
                    RadioButtonComponent,
                    CheckboxComponent,
                    DragDropDirective,
                    FormFieldInputTextComponent,
                    FormFieldInputPasswordComponent,
                    FormFieldInputNumberComponent,
                    FormFieldInputFileComponent,
                    FormFieldInputEmailComponent,
                    DatepickerComponent,
                    TextareaComponent,
                    SelectComponent,
                    SliderComponent,
                    SlideToggleComponent,
                    ButtonComponent,
                    AutocompleteComponent,
                    TimepickerComponent,
                ],
                entryComponents: [UploadDialogComponent],
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hhcmVkLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215LWFuZ3VsYXItZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvc2hhcmVkLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxXQUFXLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNsRSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDL0MsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDeEQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQ25ELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ3hELE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQzNELE9BQU8sRUFBRSwyQkFBMkIsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBRXRFLFlBQVk7QUFDWixPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUNoRixPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUNyRSxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSw0Q0FBNEMsQ0FBQztBQUNuRixPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQztBQUNwRSxPQUFPLEVBQUUsMkJBQTJCLEVBQUUsTUFBTSw2RUFBNkUsQ0FBQztBQUMxSCxPQUFPLEVBQUUsK0JBQStCLEVBQUUsTUFBTSxxRkFBcUYsQ0FBQztBQUN0SSxPQUFPLEVBQUUsNkJBQTZCLEVBQUUsTUFBTSxpRkFBaUYsQ0FBQztBQUNoSSxPQUFPLEVBQUUsMkJBQTJCLEVBQUUsTUFBTSw2RUFBNkUsQ0FBQztBQUMxSCxPQUFPLEVBQUUsNEJBQTRCLEVBQUUsTUFBTSwrRUFBK0UsQ0FBQztBQUM3SCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxzQ0FBc0MsQ0FBQztBQUMzRSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUNyRSxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFDL0QsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLDhCQUE4QixDQUFDO0FBQy9ELE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBQ2hGLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSw4QkFBOEIsQ0FBQztBQUMvRCxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUNqRixPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFDbEQsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sc0NBQXNDLENBQUM7OztBQW1FM0UsTUFBTSxPQUFPLFlBQVk7O2dEQUFaLFlBQVk7dUdBQVosWUFBWSxrQkEzQ2Q7WUFDUCxZQUFZO1lBQ1osV0FBVztZQUNYLG1CQUFtQjtZQUNuQixnQkFBZ0I7WUFDaEIsY0FBYztZQUNkLGdCQUFnQjtZQUNoQixvQkFBb0IsQ0FBQyxPQUFPLEVBQUU7WUFDOUIsa0JBQWtCO1lBQ2xCLDJCQUEyQjtTQUM1QjtRQUVDLFlBQVk7UUFDWixXQUFXO1FBQ1gsbUJBQW1CO1FBQ25CLGdCQUFnQjtRQUNoQixjQUFjO1FBQ2QsZ0JBQWdCO1FBQ2hCLG9CQUFvQjtRQUNwQixrQkFBa0I7UUFDbEIsMkJBQTJCO3dGQXVCbEIsWUFBWSxtQkEvRHJCLG9CQUFvQjtRQUNwQixpQkFBaUI7UUFDakIscUJBQXFCO1FBRXJCLGlCQUFpQjtRQUVqQiwyQkFBMkI7UUFDM0IsK0JBQStCO1FBQy9CLDZCQUE2QjtRQUM3QiwyQkFBMkI7UUFDM0IsNEJBQTRCO1FBQzVCLG1CQUFtQjtRQUNuQixpQkFBaUI7UUFDakIsZUFBZTtRQUNmLGVBQWU7UUFDZixvQkFBb0I7UUFDcEIsZUFBZTtRQUNmLHFCQUFxQjtRQUNyQixtQkFBbUIsYUFHbkIsWUFBWTtRQUNaLFdBQVc7UUFDWCxtQkFBbUI7UUFDbkIsZ0JBQWdCO1FBQ2hCLGNBQWM7UUFDZCxnQkFBZ0IsMkJBRWhCLGtCQUFrQjtRQUNsQiwyQkFBMkIsYUFHM0IsWUFBWTtRQUNaLFdBQVc7UUFDWCxtQkFBbUI7UUFDbkIsZ0JBQWdCO1FBQ2hCLGNBQWM7UUFDZCxnQkFBZ0I7UUFDaEIsb0JBQW9CO1FBQ3BCLGtCQUFrQjtRQUNsQiwyQkFBMkI7UUFFM0Isb0JBQW9CO1FBQ3BCLGlCQUFpQjtRQUVqQixpQkFBaUI7UUFFakIsMkJBQTJCO1FBQzNCLCtCQUErQjtRQUMvQiw2QkFBNkI7UUFDN0IsMkJBQTJCO1FBQzNCLDRCQUE0QjtRQUM1QixtQkFBbUI7UUFDbkIsaUJBQWlCO1FBQ2pCLGVBQWU7UUFDZixlQUFlO1FBQ2Ysb0JBQW9CO1FBQ3BCLGVBQWU7UUFDZixxQkFBcUI7UUFDckIsbUJBQW1CO2tEQUlWLFlBQVk7Y0FqRXhCLFFBQVE7ZUFBQztnQkFDUixZQUFZLEVBQUU7b0JBQ1osb0JBQW9CO29CQUNwQixpQkFBaUI7b0JBQ2pCLHFCQUFxQjtvQkFFckIsaUJBQWlCO29CQUVqQiwyQkFBMkI7b0JBQzNCLCtCQUErQjtvQkFDL0IsNkJBQTZCO29CQUM3QiwyQkFBMkI7b0JBQzNCLDRCQUE0QjtvQkFDNUIsbUJBQW1CO29CQUNuQixpQkFBaUI7b0JBQ2pCLGVBQWU7b0JBQ2YsZUFBZTtvQkFDZixvQkFBb0I7b0JBQ3BCLGVBQWU7b0JBQ2YscUJBQXFCO29CQUNyQixtQkFBbUI7aUJBQ3BCO2dCQUNELE9BQU8sRUFBRTtvQkFDUCxZQUFZO29CQUNaLFdBQVc7b0JBQ1gsbUJBQW1CO29CQUNuQixnQkFBZ0I7b0JBQ2hCLGNBQWM7b0JBQ2QsZ0JBQWdCO29CQUNoQixvQkFBb0IsQ0FBQyxPQUFPLEVBQUU7b0JBQzlCLGtCQUFrQjtvQkFDbEIsMkJBQTJCO2lCQUM1QjtnQkFDRCxPQUFPLEVBQUU7b0JBQ1AsWUFBWTtvQkFDWixXQUFXO29CQUNYLG1CQUFtQjtvQkFDbkIsZ0JBQWdCO29CQUNoQixjQUFjO29CQUNkLGdCQUFnQjtvQkFDaEIsb0JBQW9CO29CQUNwQixrQkFBa0I7b0JBQ2xCLDJCQUEyQjtvQkFFM0Isb0JBQW9CO29CQUNwQixpQkFBaUI7b0JBRWpCLGlCQUFpQjtvQkFFakIsMkJBQTJCO29CQUMzQiwrQkFBK0I7b0JBQy9CLDZCQUE2QjtvQkFDN0IsMkJBQTJCO29CQUMzQiw0QkFBNEI7b0JBQzVCLG1CQUFtQjtvQkFDbkIsaUJBQWlCO29CQUNqQixlQUFlO29CQUNmLGVBQWU7b0JBQ2Ysb0JBQW9CO29CQUNwQixlQUFlO29CQUNmLHFCQUFxQjtvQkFDckIsbUJBQW1CO2lCQUNwQjtnQkFDRCxlQUFlLEVBQUUsQ0FBQyxxQkFBcUIsQ0FBQzthQUN6QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZvcm1zTW9kdWxlLCBSZWFjdGl2ZUZvcm1zTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQgeyBGbGV4TGF5b3V0TW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvZmxleC1sYXlvdXQnO1xyXG5pbXBvcnQgeyBNYXRlcmlhbE1vZHVsZSB9IGZyb20gJy4vbWF0ZXJpYWwubW9kdWxlJztcclxuaW1wb3J0IHsgSHR0cENsaWVudE1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcclxuaW1wb3J0IHsgRGV2aWNlRGV0ZWN0b3JNb2R1bGUgfSBmcm9tICduZ3gtZGV2aWNlLWRldGVjdG9yJztcclxuaW1wb3J0IHsgTmd4TWF0ZXJpYWxUaW1lcGlja2VyTW9kdWxlIH0gZnJvbSAnbmd4LW1hdGVyaWFsLXRpbWVwaWNrZXInO1xyXG5cclxuLy8gY29tcG9uZW50XHJcbmltcG9ydCB7IFJhZGlvQnV0dG9uQ29tcG9uZW50IH0gZnJvbSAnLi91aS9yYWRpby1idXR0b24vcmFkaW8tYnV0dG9uLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IENoZWNrYm94Q29tcG9uZW50IH0gZnJvbSAnLi91aS9jaGVja2JveC9jaGVja2JveC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBVcGxvYWREaWFsb2dDb21wb25lbnQgfSBmcm9tICcuL3VpL3VwbG9hZC1kaWFsb2cvdXBsb2FkLWRpYWxvZy5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBEcmFnRHJvcERpcmVjdGl2ZSB9IGZyb20gJy4vZGlyZWN0aXZlL2RyYWctZHJvcC5kaXJlY3RpdmUnO1xyXG5pbXBvcnQgeyBGb3JtRmllbGRJbnB1dFRleHRDb21wb25lbnQgfSBmcm9tICcuL3VpL2Zvcm0tZmllbGQtaW5wdXQvZm9ybS1maWVsZC1pbnB1dC10ZXh0L2Zvcm0tZmllbGQtaW5wdXQtdGV4dC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBGb3JtRmllbGRJbnB1dFBhc3N3b3JkQ29tcG9uZW50IH0gZnJvbSAnLi91aS9mb3JtLWZpZWxkLWlucHV0L2Zvcm0tZmllbGQtaW5wdXQtcGFzc3dvcmQvZm9ybS1maWVsZC1pbnB1dC1wYXNzd29yZC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBGb3JtRmllbGRJbnB1dE51bWJlckNvbXBvbmVudCB9IGZyb20gJy4vdWkvZm9ybS1maWVsZC1pbnB1dC9mb3JtLWZpZWxkLWlucHV0LW51bWJlci9mb3JtLWZpZWxkLWlucHV0LW51bWJlci5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBGb3JtRmllbGRJbnB1dEZpbGVDb21wb25lbnQgfSBmcm9tICcuL3VpL2Zvcm0tZmllbGQtaW5wdXQvZm9ybS1maWVsZC1pbnB1dC1maWxlL2Zvcm0tZmllbGQtaW5wdXQtZmlsZS5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBGb3JtRmllbGRJbnB1dEVtYWlsQ29tcG9uZW50IH0gZnJvbSAnLi91aS9mb3JtLWZpZWxkLWlucHV0L2Zvcm0tZmllbGQtaW5wdXQtZW1haWwvZm9ybS1maWVsZC1pbnB1dC1lbWFpbC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBEYXRlcGlja2VyQ29tcG9uZW50IH0gZnJvbSAnLi91aS9kYXRlcGlja2VyL2RhdGVwaWNrZXIuY29tcG9uZW50JztcclxuaW1wb3J0IHsgVGV4dGFyZWFDb21wb25lbnQgfSBmcm9tICcuL3VpL3RleHRhcmVhL3RleHRhcmVhLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFNlbGVjdENvbXBvbmVudCB9IGZyb20gJy4vdWkvc2VsZWN0L3NlbGVjdC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBTbGlkZXJDb21wb25lbnQgfSBmcm9tICcuL3VpL3NsaWRlci9zbGlkZXIuY29tcG9uZW50JztcclxuaW1wb3J0IHsgU2xpZGVUb2dnbGVDb21wb25lbnQgfSBmcm9tICcuL3VpL3NsaWRlLXRvZ2dsZS9zbGlkZS10b2dnbGUuY29tcG9uZW50JztcclxuaW1wb3J0IHsgQnV0dG9uQ29tcG9uZW50IH0gZnJvbSAnLi91aS9idXR0b24vYnV0dG9uLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IEF1dG9jb21wbGV0ZUNvbXBvbmVudCB9IGZyb20gJy4vdWkvYXV0b2NvbXBsZXRlL2F1dG9jb21wbGV0ZS5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBBdXRvY29tcGxldGVNb2R1bGUgfSBmcm9tICdhdXRvY29tcGxldGUnO1xyXG5pbXBvcnQgeyBUaW1lcGlja2VyQ29tcG9uZW50IH0gZnJvbSAnLi91aS90aW1lcGlja2VyL3RpbWVwaWNrZXIuY29tcG9uZW50JztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgZGVjbGFyYXRpb25zOiBbXHJcbiAgICBSYWRpb0J1dHRvbkNvbXBvbmVudCxcclxuICAgIENoZWNrYm94Q29tcG9uZW50LFxyXG4gICAgVXBsb2FkRGlhbG9nQ29tcG9uZW50LFxyXG5cclxuICAgIERyYWdEcm9wRGlyZWN0aXZlLFxyXG5cclxuICAgIEZvcm1GaWVsZElucHV0VGV4dENvbXBvbmVudCxcclxuICAgIEZvcm1GaWVsZElucHV0UGFzc3dvcmRDb21wb25lbnQsXHJcbiAgICBGb3JtRmllbGRJbnB1dE51bWJlckNvbXBvbmVudCxcclxuICAgIEZvcm1GaWVsZElucHV0RmlsZUNvbXBvbmVudCxcclxuICAgIEZvcm1GaWVsZElucHV0RW1haWxDb21wb25lbnQsXHJcbiAgICBEYXRlcGlja2VyQ29tcG9uZW50LFxyXG4gICAgVGV4dGFyZWFDb21wb25lbnQsXHJcbiAgICBTZWxlY3RDb21wb25lbnQsXHJcbiAgICBTbGlkZXJDb21wb25lbnQsXHJcbiAgICBTbGlkZVRvZ2dsZUNvbXBvbmVudCxcclxuICAgIEJ1dHRvbkNvbXBvbmVudCxcclxuICAgIEF1dG9jb21wbGV0ZUNvbXBvbmVudCxcclxuICAgIFRpbWVwaWNrZXJDb21wb25lbnQsXHJcbiAgXSxcclxuICBpbXBvcnRzOiBbXHJcbiAgICBDb21tb25Nb2R1bGUsXHJcbiAgICBGb3Jtc01vZHVsZSxcclxuICAgIFJlYWN0aXZlRm9ybXNNb2R1bGUsXHJcbiAgICBGbGV4TGF5b3V0TW9kdWxlLFxyXG4gICAgTWF0ZXJpYWxNb2R1bGUsXHJcbiAgICBIdHRwQ2xpZW50TW9kdWxlLFxyXG4gICAgRGV2aWNlRGV0ZWN0b3JNb2R1bGUuZm9yUm9vdCgpLFxyXG4gICAgQXV0b2NvbXBsZXRlTW9kdWxlLFxyXG4gICAgTmd4TWF0ZXJpYWxUaW1lcGlja2VyTW9kdWxlLFxyXG4gIF0sXHJcbiAgZXhwb3J0czogW1xyXG4gICAgQ29tbW9uTW9kdWxlLFxyXG4gICAgRm9ybXNNb2R1bGUsXHJcbiAgICBSZWFjdGl2ZUZvcm1zTW9kdWxlLFxyXG4gICAgRmxleExheW91dE1vZHVsZSxcclxuICAgIE1hdGVyaWFsTW9kdWxlLFxyXG4gICAgSHR0cENsaWVudE1vZHVsZSxcclxuICAgIERldmljZURldGVjdG9yTW9kdWxlLFxyXG4gICAgQXV0b2NvbXBsZXRlTW9kdWxlLFxyXG4gICAgTmd4TWF0ZXJpYWxUaW1lcGlja2VyTW9kdWxlLFxyXG5cclxuICAgIFJhZGlvQnV0dG9uQ29tcG9uZW50LFxyXG4gICAgQ2hlY2tib3hDb21wb25lbnQsXHJcblxyXG4gICAgRHJhZ0Ryb3BEaXJlY3RpdmUsXHJcblxyXG4gICAgRm9ybUZpZWxkSW5wdXRUZXh0Q29tcG9uZW50LFxyXG4gICAgRm9ybUZpZWxkSW5wdXRQYXNzd29yZENvbXBvbmVudCxcclxuICAgIEZvcm1GaWVsZElucHV0TnVtYmVyQ29tcG9uZW50LFxyXG4gICAgRm9ybUZpZWxkSW5wdXRGaWxlQ29tcG9uZW50LFxyXG4gICAgRm9ybUZpZWxkSW5wdXRFbWFpbENvbXBvbmVudCxcclxuICAgIERhdGVwaWNrZXJDb21wb25lbnQsXHJcbiAgICBUZXh0YXJlYUNvbXBvbmVudCxcclxuICAgIFNlbGVjdENvbXBvbmVudCxcclxuICAgIFNsaWRlckNvbXBvbmVudCxcclxuICAgIFNsaWRlVG9nZ2xlQ29tcG9uZW50LFxyXG4gICAgQnV0dG9uQ29tcG9uZW50LFxyXG4gICAgQXV0b2NvbXBsZXRlQ29tcG9uZW50LFxyXG4gICAgVGltZXBpY2tlckNvbXBvbmVudCxcclxuICBdLFxyXG4gIGVudHJ5Q29tcG9uZW50czogW1VwbG9hZERpYWxvZ0NvbXBvbmVudF0sXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBTaGFyZWRNb2R1bGUge31cclxuIl19