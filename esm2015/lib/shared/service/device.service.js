import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "ngx-device-detector";
export class DeviceService {
    constructor(deviceService) {
        this.deviceService = deviceService;
        this.isDeviceMobile = () => {
            return this.deviceService.isMobile();
        };
        this.isDeviceTablet = () => {
            return this.deviceService.isTablet();
        };
        this.isDeviceDesktop = () => {
            return this.deviceService.isDesktop();
        };
        this.getDeviceInfo = () => {
            return this.deviceService.getDeviceInfo();
        };
    }
}
DeviceService.ɵfac = function DeviceService_Factory(t) { return new (t || DeviceService)(i0.ɵɵinject(i1.DeviceDetectorService)); };
DeviceService.ɵprov = i0.ɵɵdefineInjectable({ token: DeviceService, factory: DeviceService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(DeviceService, [{
        type: Injectable,
        args: [{ providedIn: 'root' }]
    }], function () { return [{ type: i1.DeviceDetectorService }]; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGV2aWNlLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9teS1hbmd1bGFyLWZvcm0vIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL3NlcnZpY2UvZGV2aWNlLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQzs7O0FBTTNDLE1BQU0sT0FBTyxhQUFhO0lBQ3hCLFlBQW9CLGFBQW9DO1FBQXBDLGtCQUFhLEdBQWIsYUFBYSxDQUF1QjtRQUV4RCxtQkFBYyxHQUFHLEdBQVksRUFBRTtZQUM3QixPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDdkMsQ0FBQyxDQUFDO1FBRUYsbUJBQWMsR0FBRyxHQUFZLEVBQUU7WUFDN0IsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQ3ZDLENBQUMsQ0FBQztRQUVGLG9CQUFlLEdBQUcsR0FBWSxFQUFFO1lBQzlCLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUN4QyxDQUFDLENBQUM7UUFFRixrQkFBYSxHQUFHLEdBQWUsRUFBRTtZQUMvQixPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDNUMsQ0FBQyxDQUFDO0lBaEJ5RCxDQUFDOzswRUFEakQsYUFBYTtxREFBYixhQUFhLFdBQWIsYUFBYSxtQkFEQSxNQUFNO2tEQUNuQixhQUFhO2NBRHpCLFVBQVU7ZUFBQyxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG4vLyBzZXJ2aWNlXHJcbmltcG9ydCB7IERldmljZURldGVjdG9yU2VydmljZSwgRGV2aWNlSW5mbyB9IGZyb20gJ25neC1kZXZpY2UtZGV0ZWN0b3InO1xyXG5cclxuQEluamVjdGFibGUoeyBwcm92aWRlZEluOiAncm9vdCcgfSlcclxuZXhwb3J0IGNsYXNzIERldmljZVNlcnZpY2Uge1xyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgZGV2aWNlU2VydmljZTogRGV2aWNlRGV0ZWN0b3JTZXJ2aWNlKSB7fVxyXG5cclxuICBpc0RldmljZU1vYmlsZSA9ICgpOiBib29sZWFuID0+IHtcclxuICAgIHJldHVybiB0aGlzLmRldmljZVNlcnZpY2UuaXNNb2JpbGUoKTtcclxuICB9O1xyXG5cclxuICBpc0RldmljZVRhYmxldCA9ICgpOiBib29sZWFuID0+IHtcclxuICAgIHJldHVybiB0aGlzLmRldmljZVNlcnZpY2UuaXNUYWJsZXQoKTtcclxuICB9O1xyXG5cclxuICBpc0RldmljZURlc2t0b3AgPSAoKTogYm9vbGVhbiA9PiB7XHJcbiAgICByZXR1cm4gdGhpcy5kZXZpY2VTZXJ2aWNlLmlzRGVza3RvcCgpO1xyXG4gIH07XHJcblxyXG4gIGdldERldmljZUluZm8gPSAoKTogRGV2aWNlSW5mbyA9PiB7XHJcbiAgICByZXR1cm4gdGhpcy5kZXZpY2VTZXJ2aWNlLmdldERldmljZUluZm8oKTtcclxuICB9O1xyXG59XHJcbiJdfQ==