import { Injectable } from "@angular/core";
import * as i0 from "@angular/core";
import * as i1 from "@angular/material/snack-bar";
export class UiService {
    constructor(snackBar) {
        this.snackBar = snackBar;
        this.onShowSnackBar = (message, action, duration) => {
            this.snackBar.open(message, action, {
                duration: duration,
            });
        };
    }
}
UiService.ɵfac = function UiService_Factory(t) { return new (t || UiService)(i0.ɵɵinject(i1.MatSnackBar)); };
UiService.ɵprov = i0.ɵɵdefineInjectable({ token: UiService, factory: UiService.ɵfac, providedIn: "root" });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(UiService, [{
        type: Injectable,
        args: [{ providedIn: "root" }]
    }], function () { return [{ type: i1.MatSnackBar }]; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWkuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215LWFuZ3VsYXItZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvc2VydmljZS91aS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7OztBQUkzQyxNQUFNLE9BQU8sU0FBUztJQUNwQixZQUFvQixRQUFxQjtRQUFyQixhQUFRLEdBQVIsUUFBUSxDQUFhO1FBRXpDLG1CQUFjLEdBQUcsQ0FBQyxPQUFlLEVBQUUsTUFBVyxFQUFFLFFBQWdCLEVBQVEsRUFBRTtZQUN4RSxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFO2dCQUNsQyxRQUFRLEVBQUUsUUFBUTthQUNuQixDQUFDLENBQUM7UUFDTCxDQUFDLENBQUM7SUFOMEMsQ0FBQzs7a0VBRGxDLFNBQVM7aURBQVQsU0FBUyxXQUFULFNBQVMsbUJBREksTUFBTTtrREFDbkIsU0FBUztjQURyQixVQUFVO2VBQUMsRUFBRSxVQUFVLEVBQUUsTUFBTSxFQUFFIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IE1hdFNuYWNrQmFyIH0gZnJvbSBcIkBhbmd1bGFyL21hdGVyaWFsL3NuYWNrLWJhclwiO1xyXG5cclxuQEluamVjdGFibGUoeyBwcm92aWRlZEluOiBcInJvb3RcIiB9KVxyXG5leHBvcnQgY2xhc3MgVWlTZXJ2aWNlIHtcclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHNuYWNrQmFyOiBNYXRTbmFja0Jhcikge31cclxuXHJcbiAgb25TaG93U25hY2tCYXIgPSAobWVzc2FnZTogc3RyaW5nLCBhY3Rpb246IGFueSwgZHVyYXRpb246IG51bWJlcik6IHZvaWQgPT4ge1xyXG4gICAgdGhpcy5zbmFja0Jhci5vcGVuKG1lc3NhZ2UsIGFjdGlvbiwge1xyXG4gICAgICBkdXJhdGlvbjogZHVyYXRpb24sXHJcbiAgICB9KTtcclxuICB9O1xyXG59XHJcbiJdfQ==