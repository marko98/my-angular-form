import { Datepicker, } from '../../../structural/composite/form/datepicker/datepicker.model';
import { VALIDATOR_NAMES, } from '../../../structural/composite/form/form-row-item.model';
export class DatepickerConstructionStrategy {
    construct(datepickerInterface) {
        let datepicker = new Datepicker();
        datepicker.setType(datepickerInterface.type);
        datepicker.setControlName(datepickerInterface.controlName);
        if (datepickerInterface.appearance)
            datepicker.setAppearance(datepickerInterface.appearance);
        if (datepickerInterface.disabledDays)
            datepicker.setDisabledDays(datepickerInterface.disabledDays);
        if (datepickerInterface.defaultValue) {
            if (datepickerInterface.min) {
                if (datepickerInterface.defaultValue < datepickerInterface.min) {
                    throw Error('Default value Date must be at or after min Date');
                }
            }
            if (datepickerInterface.max) {
                if (datepickerInterface.defaultValue > datepickerInterface.max) {
                    throw Error('Default value Date must be at or before max Date');
                }
            }
        }
        datepicker.setDefaultValue(datepickerInterface.defaultValue);
        if (datepickerInterface.validators) {
            datepickerInterface.validators.forEach((validator) => {
                if (validator.validatorFn.name === VALIDATOR_NAMES.REQUIRED) {
                    datepicker.addValidator(validator);
                }
            });
        }
        if (datepickerInterface.min) {
            if (datepickerInterface.max) {
                if (datepickerInterface.min > datepickerInterface.max) {
                    throw Error('min Date must be at or before max Date');
                }
            }
        }
        datepicker.setMin(datepickerInterface.min);
        if (datepickerInterface.max) {
            if (datepickerInterface.min) {
                if (datepickerInterface.max < datepickerInterface.min) {
                    throw Error('max Date must be at or after min Date');
                }
            }
        }
        datepicker.setMax(datepickerInterface.max);
        if (datepickerInterface.startAt) {
            if (datepickerInterface.min) {
                if (datepickerInterface.startAt < datepickerInterface.min) {
                    throw Error('StartAt value Date must be at or after min Date');
                }
            }
            if (datepickerInterface.max) {
                if (datepickerInterface.startAt > datepickerInterface.max) {
                    throw Error('StartAt value Date must be at or before max Date');
                }
            }
        }
        datepicker.setStartAt(datepickerInterface.startAt);
        if (datepickerInterface.disabled)
            datepicker.setDisabled(datepickerInterface.disabled);
        if (datepickerInterface.startView)
            datepicker.setStartView(datepickerInterface.startView);
        if (datepickerInterface.color)
            datepicker.setColor(datepickerInterface.color);
        if (datepickerInterface.toggleSideSuffix !== undefined)
            datepicker.setToggleSideSuffix(datepickerInterface.toggleSideSuffix);
        if (datepickerInterface.matIcon)
            datepicker.setMatIcon(datepickerInterface.matIcon);
        if (datepickerInterface.labelName)
            datepicker.setLabelName(datepickerInterface.labelName);
        else
            throw Error('Label name is required');
        if (datepickerInterface.timepicker)
            datepicker.setTimepickerInsideDatepickerInterface(datepickerInterface.timepicker);
        return datepicker;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0ZXBpY2tlci1jb25zdHJ1Y3Rpb24tc3RyYXRlZ3kubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9teS1hbmd1bGFyLWZvcm0vIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL21vZGVsL2JlaGF2aW91cmFsL3N0cmF0ZWd5L2Zvcm0tcm93LWl0ZW0vZGF0ZXBpY2tlci1jb25zdHJ1Y3Rpb24tc3RyYXRlZ3kubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxFQUNMLFVBQVUsR0FFWCxNQUFNLGdFQUFnRSxDQUFDO0FBQ3hFLE9BQU8sRUFFTCxlQUFlLEdBQ2hCLE1BQU0sd0RBQXdELENBQUM7QUFFaEUsTUFBTSxPQUFPLDhCQUE4QjtJQUV6QyxTQUFTLENBQUMsbUJBQXdDO1FBQ2hELElBQUksVUFBVSxHQUFHLElBQUksVUFBVSxFQUFFLENBQUM7UUFFbEMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUU3QyxVQUFVLENBQUMsY0FBYyxDQUFDLG1CQUFtQixDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBRTNELElBQUksbUJBQW1CLENBQUMsVUFBVTtZQUNoQyxVQUFVLENBQUMsYUFBYSxDQUFDLG1CQUFtQixDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBRTNELElBQUksbUJBQW1CLENBQUMsWUFBWTtZQUNsQyxVQUFVLENBQUMsZUFBZSxDQUFDLG1CQUFtQixDQUFDLFlBQVksQ0FBQyxDQUFDO1FBRS9ELElBQUksbUJBQW1CLENBQUMsWUFBWSxFQUFFO1lBQ3BDLElBQUksbUJBQW1CLENBQUMsR0FBRyxFQUFFO2dCQUMzQixJQUFJLG1CQUFtQixDQUFDLFlBQVksR0FBRyxtQkFBbUIsQ0FBQyxHQUFHLEVBQUU7b0JBQzlELE1BQU0sS0FBSyxDQUFDLGlEQUFpRCxDQUFDLENBQUM7aUJBQ2hFO2FBQ0Y7WUFFRCxJQUFJLG1CQUFtQixDQUFDLEdBQUcsRUFBRTtnQkFDM0IsSUFBSSxtQkFBbUIsQ0FBQyxZQUFZLEdBQUcsbUJBQW1CLENBQUMsR0FBRyxFQUFFO29CQUM5RCxNQUFNLEtBQUssQ0FBQyxrREFBa0QsQ0FBQyxDQUFDO2lCQUNqRTthQUNGO1NBQ0Y7UUFDRCxVQUFVLENBQUMsZUFBZSxDQUFDLG1CQUFtQixDQUFDLFlBQVksQ0FBQyxDQUFDO1FBRTdELElBQUksbUJBQW1CLENBQUMsVUFBVSxFQUFFO1lBQ2xDLG1CQUFtQixDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQ3BDLENBQUMsU0FBd0MsRUFBRSxFQUFFO2dCQUMzQyxJQUFJLFNBQVMsQ0FBQyxXQUFXLENBQUMsSUFBSSxLQUFLLGVBQWUsQ0FBQyxRQUFRLEVBQUU7b0JBQzNELFVBQVUsQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLENBQUM7aUJBQ3BDO1lBQ0gsQ0FBQyxDQUNGLENBQUM7U0FDSDtRQUVELElBQUksbUJBQW1CLENBQUMsR0FBRyxFQUFFO1lBQzNCLElBQUksbUJBQW1CLENBQUMsR0FBRyxFQUFFO2dCQUMzQixJQUFJLG1CQUFtQixDQUFDLEdBQUcsR0FBRyxtQkFBbUIsQ0FBQyxHQUFHLEVBQUU7b0JBQ3JELE1BQU0sS0FBSyxDQUFDLHdDQUF3QyxDQUFDLENBQUM7aUJBQ3ZEO2FBQ0Y7U0FDRjtRQUNELFVBQVUsQ0FBQyxNQUFNLENBQUMsbUJBQW1CLENBQUMsR0FBRyxDQUFDLENBQUM7UUFFM0MsSUFBSSxtQkFBbUIsQ0FBQyxHQUFHLEVBQUU7WUFDM0IsSUFBSSxtQkFBbUIsQ0FBQyxHQUFHLEVBQUU7Z0JBQzNCLElBQUksbUJBQW1CLENBQUMsR0FBRyxHQUFHLG1CQUFtQixDQUFDLEdBQUcsRUFBRTtvQkFDckQsTUFBTSxLQUFLLENBQUMsdUNBQXVDLENBQUMsQ0FBQztpQkFDdEQ7YUFDRjtTQUNGO1FBQ0QsVUFBVSxDQUFDLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUUzQyxJQUFJLG1CQUFtQixDQUFDLE9BQU8sRUFBRTtZQUMvQixJQUFJLG1CQUFtQixDQUFDLEdBQUcsRUFBRTtnQkFDM0IsSUFBSSxtQkFBbUIsQ0FBQyxPQUFPLEdBQUcsbUJBQW1CLENBQUMsR0FBRyxFQUFFO29CQUN6RCxNQUFNLEtBQUssQ0FBQyxpREFBaUQsQ0FBQyxDQUFDO2lCQUNoRTthQUNGO1lBRUQsSUFBSSxtQkFBbUIsQ0FBQyxHQUFHLEVBQUU7Z0JBQzNCLElBQUksbUJBQW1CLENBQUMsT0FBTyxHQUFHLG1CQUFtQixDQUFDLEdBQUcsRUFBRTtvQkFDekQsTUFBTSxLQUFLLENBQUMsa0RBQWtELENBQUMsQ0FBQztpQkFDakU7YUFDRjtTQUNGO1FBQ0QsVUFBVSxDQUFDLFVBQVUsQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUVuRCxJQUFJLG1CQUFtQixDQUFDLFFBQVE7WUFDOUIsVUFBVSxDQUFDLFdBQVcsQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUV2RCxJQUFJLG1CQUFtQixDQUFDLFNBQVM7WUFDL0IsVUFBVSxDQUFDLFlBQVksQ0FBQyxtQkFBbUIsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUN6RCxJQUFJLG1CQUFtQixDQUFDLEtBQUs7WUFDM0IsVUFBVSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNqRCxJQUFJLG1CQUFtQixDQUFDLGdCQUFnQixLQUFLLFNBQVM7WUFDcEQsVUFBVSxDQUFDLG1CQUFtQixDQUFDLG1CQUFtQixDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDdkUsSUFBSSxtQkFBbUIsQ0FBQyxPQUFPO1lBQzdCLFVBQVUsQ0FBQyxVQUFVLENBQUMsbUJBQW1CLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDckQsSUFBSSxtQkFBbUIsQ0FBQyxTQUFTO1lBQy9CLFVBQVUsQ0FBQyxZQUFZLENBQUMsbUJBQW1CLENBQUMsU0FBUyxDQUFDLENBQUM7O1lBQ3BELE1BQU0sS0FBSyxDQUFDLHdCQUF3QixDQUFDLENBQUM7UUFFM0MsSUFBSSxtQkFBbUIsQ0FBQyxVQUFVO1lBQ2hDLFVBQVUsQ0FBQyxzQ0FBc0MsQ0FDL0MsbUJBQW1CLENBQUMsVUFBVSxDQUMvQixDQUFDO1FBRUosT0FBTyxVQUFVLENBQUM7SUFDcEIsQ0FBQztDQUNGIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRm9ybVJvd0l0ZW1Db25zdHJ1Y3Rpb25TdHJhdGVneSB9IGZyb20gJy4vZm9ybS1yb3ctaXRlbS1jb25zdHJ1Y3Rpb24tc3RyYXRlZ3kuaW50ZXJmYWNlJztcclxuaW1wb3J0IHtcclxuICBEYXRlcGlja2VyLFxyXG4gIERhdGVwaWNrZXJJbnRlcmZhY2UsXHJcbn0gZnJvbSAnLi4vLi4vLi4vc3RydWN0dXJhbC9jb21wb3NpdGUvZm9ybS9kYXRlcGlja2VyL2RhdGVwaWNrZXIubW9kZWwnO1xyXG5pbXBvcnQge1xyXG4gIEZvcm1Sb3dJdGVtVmFsaWRhdG9ySW50ZXJmYWNlLFxyXG4gIFZBTElEQVRPUl9OQU1FUyxcclxufSBmcm9tICcuLi8uLi8uLi9zdHJ1Y3R1cmFsL2NvbXBvc2l0ZS9mb3JtL2Zvcm0tcm93LWl0ZW0ubW9kZWwnO1xyXG5cclxuZXhwb3J0IGNsYXNzIERhdGVwaWNrZXJDb25zdHJ1Y3Rpb25TdHJhdGVneVxyXG4gIGltcGxlbWVudHMgRm9ybVJvd0l0ZW1Db25zdHJ1Y3Rpb25TdHJhdGVneSB7XHJcbiAgY29uc3RydWN0KGRhdGVwaWNrZXJJbnRlcmZhY2U6IERhdGVwaWNrZXJJbnRlcmZhY2UpOiBEYXRlcGlja2VyIHtcclxuICAgIGxldCBkYXRlcGlja2VyID0gbmV3IERhdGVwaWNrZXIoKTtcclxuXHJcbiAgICBkYXRlcGlja2VyLnNldFR5cGUoZGF0ZXBpY2tlckludGVyZmFjZS50eXBlKTtcclxuXHJcbiAgICBkYXRlcGlja2VyLnNldENvbnRyb2xOYW1lKGRhdGVwaWNrZXJJbnRlcmZhY2UuY29udHJvbE5hbWUpO1xyXG5cclxuICAgIGlmIChkYXRlcGlja2VySW50ZXJmYWNlLmFwcGVhcmFuY2UpXHJcbiAgICAgIGRhdGVwaWNrZXIuc2V0QXBwZWFyYW5jZShkYXRlcGlja2VySW50ZXJmYWNlLmFwcGVhcmFuY2UpO1xyXG5cclxuICAgIGlmIChkYXRlcGlja2VySW50ZXJmYWNlLmRpc2FibGVkRGF5cylcclxuICAgICAgZGF0ZXBpY2tlci5zZXREaXNhYmxlZERheXMoZGF0ZXBpY2tlckludGVyZmFjZS5kaXNhYmxlZERheXMpO1xyXG5cclxuICAgIGlmIChkYXRlcGlja2VySW50ZXJmYWNlLmRlZmF1bHRWYWx1ZSkge1xyXG4gICAgICBpZiAoZGF0ZXBpY2tlckludGVyZmFjZS5taW4pIHtcclxuICAgICAgICBpZiAoZGF0ZXBpY2tlckludGVyZmFjZS5kZWZhdWx0VmFsdWUgPCBkYXRlcGlja2VySW50ZXJmYWNlLm1pbikge1xyXG4gICAgICAgICAgdGhyb3cgRXJyb3IoJ0RlZmF1bHQgdmFsdWUgRGF0ZSBtdXN0IGJlIGF0IG9yIGFmdGVyIG1pbiBEYXRlJyk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAoZGF0ZXBpY2tlckludGVyZmFjZS5tYXgpIHtcclxuICAgICAgICBpZiAoZGF0ZXBpY2tlckludGVyZmFjZS5kZWZhdWx0VmFsdWUgPiBkYXRlcGlja2VySW50ZXJmYWNlLm1heCkge1xyXG4gICAgICAgICAgdGhyb3cgRXJyb3IoJ0RlZmF1bHQgdmFsdWUgRGF0ZSBtdXN0IGJlIGF0IG9yIGJlZm9yZSBtYXggRGF0ZScpO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgZGF0ZXBpY2tlci5zZXREZWZhdWx0VmFsdWUoZGF0ZXBpY2tlckludGVyZmFjZS5kZWZhdWx0VmFsdWUpO1xyXG5cclxuICAgIGlmIChkYXRlcGlja2VySW50ZXJmYWNlLnZhbGlkYXRvcnMpIHtcclxuICAgICAgZGF0ZXBpY2tlckludGVyZmFjZS52YWxpZGF0b3JzLmZvckVhY2goXHJcbiAgICAgICAgKHZhbGlkYXRvcjogRm9ybVJvd0l0ZW1WYWxpZGF0b3JJbnRlcmZhY2UpID0+IHtcclxuICAgICAgICAgIGlmICh2YWxpZGF0b3IudmFsaWRhdG9yRm4ubmFtZSA9PT0gVkFMSURBVE9SX05BTUVTLlJFUVVJUkVEKSB7XHJcbiAgICAgICAgICAgIGRhdGVwaWNrZXIuYWRkVmFsaWRhdG9yKHZhbGlkYXRvcik7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIGlmIChkYXRlcGlja2VySW50ZXJmYWNlLm1pbikge1xyXG4gICAgICBpZiAoZGF0ZXBpY2tlckludGVyZmFjZS5tYXgpIHtcclxuICAgICAgICBpZiAoZGF0ZXBpY2tlckludGVyZmFjZS5taW4gPiBkYXRlcGlja2VySW50ZXJmYWNlLm1heCkge1xyXG4gICAgICAgICAgdGhyb3cgRXJyb3IoJ21pbiBEYXRlIG11c3QgYmUgYXQgb3IgYmVmb3JlIG1heCBEYXRlJyk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICBkYXRlcGlja2VyLnNldE1pbihkYXRlcGlja2VySW50ZXJmYWNlLm1pbik7XHJcblxyXG4gICAgaWYgKGRhdGVwaWNrZXJJbnRlcmZhY2UubWF4KSB7XHJcbiAgICAgIGlmIChkYXRlcGlja2VySW50ZXJmYWNlLm1pbikge1xyXG4gICAgICAgIGlmIChkYXRlcGlja2VySW50ZXJmYWNlLm1heCA8IGRhdGVwaWNrZXJJbnRlcmZhY2UubWluKSB7XHJcbiAgICAgICAgICB0aHJvdyBFcnJvcignbWF4IERhdGUgbXVzdCBiZSBhdCBvciBhZnRlciBtaW4gRGF0ZScpO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgZGF0ZXBpY2tlci5zZXRNYXgoZGF0ZXBpY2tlckludGVyZmFjZS5tYXgpO1xyXG5cclxuICAgIGlmIChkYXRlcGlja2VySW50ZXJmYWNlLnN0YXJ0QXQpIHtcclxuICAgICAgaWYgKGRhdGVwaWNrZXJJbnRlcmZhY2UubWluKSB7XHJcbiAgICAgICAgaWYgKGRhdGVwaWNrZXJJbnRlcmZhY2Uuc3RhcnRBdCA8IGRhdGVwaWNrZXJJbnRlcmZhY2UubWluKSB7XHJcbiAgICAgICAgICB0aHJvdyBFcnJvcignU3RhcnRBdCB2YWx1ZSBEYXRlIG11c3QgYmUgYXQgb3IgYWZ0ZXIgbWluIERhdGUnKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmIChkYXRlcGlja2VySW50ZXJmYWNlLm1heCkge1xyXG4gICAgICAgIGlmIChkYXRlcGlja2VySW50ZXJmYWNlLnN0YXJ0QXQgPiBkYXRlcGlja2VySW50ZXJmYWNlLm1heCkge1xyXG4gICAgICAgICAgdGhyb3cgRXJyb3IoJ1N0YXJ0QXQgdmFsdWUgRGF0ZSBtdXN0IGJlIGF0IG9yIGJlZm9yZSBtYXggRGF0ZScpO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgZGF0ZXBpY2tlci5zZXRTdGFydEF0KGRhdGVwaWNrZXJJbnRlcmZhY2Uuc3RhcnRBdCk7XHJcblxyXG4gICAgaWYgKGRhdGVwaWNrZXJJbnRlcmZhY2UuZGlzYWJsZWQpXHJcbiAgICAgIGRhdGVwaWNrZXIuc2V0RGlzYWJsZWQoZGF0ZXBpY2tlckludGVyZmFjZS5kaXNhYmxlZCk7XHJcblxyXG4gICAgaWYgKGRhdGVwaWNrZXJJbnRlcmZhY2Uuc3RhcnRWaWV3KVxyXG4gICAgICBkYXRlcGlja2VyLnNldFN0YXJ0VmlldyhkYXRlcGlja2VySW50ZXJmYWNlLnN0YXJ0Vmlldyk7XHJcbiAgICBpZiAoZGF0ZXBpY2tlckludGVyZmFjZS5jb2xvcilcclxuICAgICAgZGF0ZXBpY2tlci5zZXRDb2xvcihkYXRlcGlja2VySW50ZXJmYWNlLmNvbG9yKTtcclxuICAgIGlmIChkYXRlcGlja2VySW50ZXJmYWNlLnRvZ2dsZVNpZGVTdWZmaXggIT09IHVuZGVmaW5lZClcclxuICAgICAgZGF0ZXBpY2tlci5zZXRUb2dnbGVTaWRlU3VmZml4KGRhdGVwaWNrZXJJbnRlcmZhY2UudG9nZ2xlU2lkZVN1ZmZpeCk7XHJcbiAgICBpZiAoZGF0ZXBpY2tlckludGVyZmFjZS5tYXRJY29uKVxyXG4gICAgICBkYXRlcGlja2VyLnNldE1hdEljb24oZGF0ZXBpY2tlckludGVyZmFjZS5tYXRJY29uKTtcclxuICAgIGlmIChkYXRlcGlja2VySW50ZXJmYWNlLmxhYmVsTmFtZSlcclxuICAgICAgZGF0ZXBpY2tlci5zZXRMYWJlbE5hbWUoZGF0ZXBpY2tlckludGVyZmFjZS5sYWJlbE5hbWUpO1xyXG4gICAgZWxzZSB0aHJvdyBFcnJvcignTGFiZWwgbmFtZSBpcyByZXF1aXJlZCcpO1xyXG5cclxuICAgIGlmIChkYXRlcGlja2VySW50ZXJmYWNlLnRpbWVwaWNrZXIpXHJcbiAgICAgIGRhdGVwaWNrZXIuc2V0VGltZXBpY2tlckluc2lkZURhdGVwaWNrZXJJbnRlcmZhY2UoXHJcbiAgICAgICAgZGF0ZXBpY2tlckludGVyZmFjZS50aW1lcGlja2VyXHJcbiAgICAgICk7XHJcblxyXG4gICAgcmV0dXJuIGRhdGVwaWNrZXI7XHJcbiAgfVxyXG59XHJcbiJdfQ==