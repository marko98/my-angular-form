import { Select, } from '../../../structural/composite/form/select/select.model';
import { VALIDATOR_NAMES, } from '../../../structural/composite/form/form-row-item.model';
import { Validators } from '@angular/forms';
export class SelectConstructionStrategy {
    construct(selectInterface) {
        let select = new Select();
        select.setType(selectInterface.type);
        if (selectInterface.multiple) {
            select.setMultiple(selectInterface.multiple);
            if (selectInterface.defaultValue) {
                if (selectInterface.defaultValue instanceof Array) {
                    select.setDefaultValue(selectInterface.defaultValue);
                }
                else {
                    throw Error('If you want multiple choice default value must be an Array');
                }
            }
        }
        else {
            if (selectInterface.defaultValue) {
                if (selectInterface.defaultValue instanceof Array) {
                    throw Error("If you don't want multiple choice default value must not be an Array");
                }
                else {
                    select.setDefaultValue(selectInterface.defaultValue);
                }
            }
        }
        if (selectInterface.matSelectTriggerOn) {
            if (!select.getMultiple())
                throw Error('MatSelectTriggerOn make sense only if multiple is set to true');
            else
                select.setMatSelectTriggerOn(selectInterface.matSelectTriggerOn);
        }
        select.setControlName(selectInterface.controlName);
        if (selectInterface.validators)
            select.setValidators(selectInterface.validators);
        if (selectInterface.hintLabels) {
            throw Error('Hint Labels are not allowed on select');
        }
        if (selectInterface.appearance)
            select.setAppearance(selectInterface.appearance);
        if (selectInterface.placeholder)
            throw Error('Placeholder is not allowed on select');
        if (selectInterface.disabled)
            select.setDisabled(selectInterface.disabled);
        if (selectInterface.readonly)
            throw Error('Readonly is not allowed on select');
        if (selectInterface.requiredOption) {
            let requiredOption = {
                required: false,
                hideRequiredMarker: false,
            };
            if (selectInterface.requiredOption.required)
                requiredOption.required = selectInterface.requiredOption.required;
            if (selectInterface.requiredOption.hideRequiredMarker)
                requiredOption.hideRequiredMarker =
                    selectInterface.requiredOption.hideRequiredMarker;
            select.setRequired(requiredOption);
            // add required validatorFn if it doesn't exists
            let validatorRequiredFnExists = false;
            select
                .getValidators()
                .forEach((validator) => {
                if (validator.name === VALIDATOR_NAMES.REQUIRED) {
                    validatorRequiredFnExists = true;
                }
            });
            if (!validatorRequiredFnExists) {
                select.addValidator({
                    message: 'the field is required',
                    name: VALIDATOR_NAMES.REQUIRED,
                    validatorFn: Validators.required,
                });
            }
        }
        else {
            // check if it has required validator if it has add requiredOption.required = true
            let validatorRequiredFnExists = false;
            select
                .getValidators()
                .forEach((validator) => {
                if (validator.name === VALIDATOR_NAMES.REQUIRED) {
                    validatorRequiredFnExists = true;
                }
            });
            if (validatorRequiredFnExists) {
                let requiredOption = {
                    required: true,
                    hideRequiredMarker: false,
                };
                select.setRequired(requiredOption);
            }
        }
        if (selectInterface.labelName)
            select.setLabelName(selectInterface.labelName);
        else
            throw Error('Label name is required');
        if (selectInterface.matImages) {
            if (selectInterface.matImages.matPrefixImgText)
                select.setMatPrefixImgText(selectInterface.matImages.matPrefixImgText);
            if (selectInterface.matImages.matSuffixImgText)
                select.setMatSuffixImgText(selectInterface.matImages.matSuffixImgText);
        }
        if (selectInterface.textPrefix)
            select.setTextPrefix(selectInterface.textPrefix);
        if (selectInterface.textSuffix)
            select.setTextSuffix(selectInterface.textSuffix);
        if (selectInterface.optionValues && selectInterface.optionValues.length > 0)
            select.setOptionValues(selectInterface.optionValues);
        else
            throw Error('Option Values are required for select');
        return select;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VsZWN0LWNvbnN0cnVjdGlvbi1zdHJhdGVneS5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215LWFuZ3VsYXItZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvbW9kZWwvYmVoYXZpb3VyYWwvc3RyYXRlZ3kvZm9ybS1yb3ctaXRlbS9zZWxlY3QtY29uc3RydWN0aW9uLXN0cmF0ZWd5Lm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBLE9BQU8sRUFFTCxNQUFNLEdBQ1AsTUFBTSx3REFBd0QsQ0FBQztBQUNoRSxPQUFPLEVBRUwsZUFBZSxHQUVoQixNQUFNLHdEQUF3RCxDQUFDO0FBQ2hFLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUU1QyxNQUFNLE9BQU8sMEJBQTBCO0lBRXJDLFNBQVMsQ0FBQyxlQUFnQztRQUN4QyxJQUFJLE1BQU0sR0FBVyxJQUFJLE1BQU0sRUFBRSxDQUFDO1FBRWxDLE1BQU0sQ0FBQyxPQUFPLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBRXJDLElBQUksZUFBZSxDQUFDLFFBQVEsRUFBRTtZQUM1QixNQUFNLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUU3QyxJQUFJLGVBQWUsQ0FBQyxZQUFZLEVBQUU7Z0JBQ2hDLElBQUksZUFBZSxDQUFDLFlBQVksWUFBWSxLQUFLLEVBQUU7b0JBQ2pELE1BQU0sQ0FBQyxlQUFlLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxDQUFDO2lCQUN0RDtxQkFBTTtvQkFDTCxNQUFNLEtBQUssQ0FDVCw0REFBNEQsQ0FDN0QsQ0FBQztpQkFDSDthQUNGO1NBQ0Y7YUFBTTtZQUNMLElBQUksZUFBZSxDQUFDLFlBQVksRUFBRTtnQkFDaEMsSUFBSSxlQUFlLENBQUMsWUFBWSxZQUFZLEtBQUssRUFBRTtvQkFDakQsTUFBTSxLQUFLLENBQ1Qsc0VBQXNFLENBQ3ZFLENBQUM7aUJBQ0g7cUJBQU07b0JBQ0wsTUFBTSxDQUFDLGVBQWUsQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDLENBQUM7aUJBQ3REO2FBQ0Y7U0FDRjtRQUVELElBQUksZUFBZSxDQUFDLGtCQUFrQixFQUFFO1lBQ3RDLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFO2dCQUN2QixNQUFNLEtBQUssQ0FDVCwrREFBK0QsQ0FDaEUsQ0FBQzs7Z0JBQ0MsTUFBTSxDQUFDLHFCQUFxQixDQUFDLGVBQWUsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1NBQ3ZFO1FBRUQsTUFBTSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLENBQUM7UUFFbkQsSUFBSSxlQUFlLENBQUMsVUFBVTtZQUM1QixNQUFNLENBQUMsYUFBYSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUVuRCxJQUFJLGVBQWUsQ0FBQyxVQUFVLEVBQUU7WUFDOUIsTUFBTSxLQUFLLENBQUMsdUNBQXVDLENBQUMsQ0FBQztTQUN0RDtRQUVELElBQUksZUFBZSxDQUFDLFVBQVU7WUFDNUIsTUFBTSxDQUFDLGFBQWEsQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLENBQUM7UUFFbkQsSUFBSSxlQUFlLENBQUMsV0FBVztZQUM3QixNQUFNLEtBQUssQ0FBQyxzQ0FBc0MsQ0FBQyxDQUFDO1FBRXRELElBQUksZUFBZSxDQUFDLFFBQVE7WUFBRSxNQUFNLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUUzRSxJQUFJLGVBQWUsQ0FBQyxRQUFRO1lBQzFCLE1BQU0sS0FBSyxDQUFDLG1DQUFtQyxDQUFDLENBQUM7UUFFbkQsSUFBSSxlQUFlLENBQUMsY0FBYyxFQUFFO1lBQ2xDLElBQUksY0FBYyxHQUFHO2dCQUNuQixRQUFRLEVBQUUsS0FBSztnQkFDZixrQkFBa0IsRUFBRSxLQUFLO2FBQzFCLENBQUM7WUFFRixJQUFJLGVBQWUsQ0FBQyxjQUFjLENBQUMsUUFBUTtnQkFDekMsY0FBYyxDQUFDLFFBQVEsR0FBRyxlQUFlLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQztZQUVwRSxJQUFJLGVBQWUsQ0FBQyxjQUFjLENBQUMsa0JBQWtCO2dCQUNuRCxjQUFjLENBQUMsa0JBQWtCO29CQUMvQixlQUFlLENBQUMsY0FBYyxDQUFDLGtCQUFrQixDQUFDO1lBRXRELE1BQU0sQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLENBQUM7WUFFbkMsZ0RBQWdEO1lBQ2hELElBQUkseUJBQXlCLEdBQUcsS0FBSyxDQUFDO1lBQ3hCLE1BQU87aUJBQ2xCLGFBQWEsRUFBRTtpQkFDZixPQUFPLENBQUMsQ0FBQyxTQUF3QyxFQUFFLEVBQUU7Z0JBQ3BELElBQUksU0FBUyxDQUFDLElBQUksS0FBSyxlQUFlLENBQUMsUUFBUSxFQUFFO29CQUMvQyx5QkFBeUIsR0FBRyxJQUFJLENBQUM7aUJBQ2xDO1lBQ0gsQ0FBQyxDQUFDLENBQUM7WUFFTCxJQUFJLENBQUMseUJBQXlCLEVBQUU7Z0JBQ2hCLE1BQU8sQ0FBQyxZQUFZLENBQUM7b0JBQ2pDLE9BQU8sRUFBRSx1QkFBdUI7b0JBQ2hDLElBQUksRUFBRSxlQUFlLENBQUMsUUFBUTtvQkFDOUIsV0FBVyxFQUFFLFVBQVUsQ0FBQyxRQUFRO2lCQUNqQyxDQUFDLENBQUM7YUFDSjtTQUNGO2FBQU07WUFDTCxrRkFBa0Y7WUFDbEYsSUFBSSx5QkFBeUIsR0FBRyxLQUFLLENBQUM7WUFDeEIsTUFBTztpQkFDbEIsYUFBYSxFQUFFO2lCQUNmLE9BQU8sQ0FBQyxDQUFDLFNBQXdDLEVBQUUsRUFBRTtnQkFDcEQsSUFBSSxTQUFTLENBQUMsSUFBSSxLQUFLLGVBQWUsQ0FBQyxRQUFRLEVBQUU7b0JBQy9DLHlCQUF5QixHQUFHLElBQUksQ0FBQztpQkFDbEM7WUFDSCxDQUFDLENBQUMsQ0FBQztZQUVMLElBQUkseUJBQXlCLEVBQUU7Z0JBQzdCLElBQUksY0FBYyxHQUFHO29CQUNuQixRQUFRLEVBQUUsSUFBSTtvQkFDZCxrQkFBa0IsRUFBRSxLQUFLO2lCQUMxQixDQUFDO2dCQUNGLE1BQU0sQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLENBQUM7YUFDcEM7U0FDRjtRQUVELElBQUksZUFBZSxDQUFDLFNBQVM7WUFDM0IsTUFBTSxDQUFDLFlBQVksQ0FBQyxlQUFlLENBQUMsU0FBUyxDQUFDLENBQUM7O1lBQzVDLE1BQU0sS0FBSyxDQUFDLHdCQUF3QixDQUFDLENBQUM7UUFFM0MsSUFBSSxlQUFlLENBQUMsU0FBUyxFQUFFO1lBQzdCLElBQUksZUFBZSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0I7Z0JBQzVDLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxlQUFlLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLENBQUM7WUFFekUsSUFBSSxlQUFlLENBQUMsU0FBUyxDQUFDLGdCQUFnQjtnQkFDNUMsTUFBTSxDQUFDLG1CQUFtQixDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztTQUMxRTtRQUVELElBQUksZUFBZSxDQUFDLFVBQVU7WUFDNUIsTUFBTSxDQUFDLGFBQWEsQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLENBQUM7UUFFbkQsSUFBSSxlQUFlLENBQUMsVUFBVTtZQUM1QixNQUFNLENBQUMsYUFBYSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUVuRCxJQUFJLGVBQWUsQ0FBQyxZQUFZLElBQUksZUFBZSxDQUFDLFlBQVksQ0FBQyxNQUFNLEdBQUcsQ0FBQztZQUN6RSxNQUFNLENBQUMsZUFBZSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsQ0FBQzs7WUFDbEQsTUFBTSxLQUFLLENBQUMsdUNBQXVDLENBQUMsQ0FBQztRQUUxRCxPQUFPLE1BQU0sQ0FBQztJQUNoQixDQUFDO0NBQ0YiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBGb3JtRmllbGRJbnB1dENvbnN0cnVjdGlvblN0cmF0ZWd5IH0gZnJvbSAnLi9mb3JtLWZpZWxkLWlucHV0LWNvbnN0cnVjdGlvbi1zdHJhdGVneS5tb2RlbCc7XHJcbmltcG9ydCB7XHJcbiAgU2VsZWN0SW50ZXJmYWNlLFxyXG4gIFNlbGVjdCxcclxufSBmcm9tICcuLi8uLi8uLi9zdHJ1Y3R1cmFsL2NvbXBvc2l0ZS9mb3JtL3NlbGVjdC9zZWxlY3QubW9kZWwnO1xyXG5pbXBvcnQge1xyXG4gIEZvcm1Sb3dJdGVtVmFsaWRhdG9ySW50ZXJmYWNlLFxyXG4gIFZBTElEQVRPUl9OQU1FUyxcclxuICBGb3JtUm93SXRlbSxcclxufSBmcm9tICcuLi8uLi8uLi9zdHJ1Y3R1cmFsL2NvbXBvc2l0ZS9mb3JtL2Zvcm0tcm93LWl0ZW0ubW9kZWwnO1xyXG5pbXBvcnQgeyBWYWxpZGF0b3JzIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5cclxuZXhwb3J0IGNsYXNzIFNlbGVjdENvbnN0cnVjdGlvblN0cmF0ZWd5XHJcbiAgaW1wbGVtZW50cyBGb3JtRmllbGRJbnB1dENvbnN0cnVjdGlvblN0cmF0ZWd5IHtcclxuICBjb25zdHJ1Y3Qoc2VsZWN0SW50ZXJmYWNlOiBTZWxlY3RJbnRlcmZhY2UpOiBTZWxlY3Qge1xyXG4gICAgbGV0IHNlbGVjdDogU2VsZWN0ID0gbmV3IFNlbGVjdCgpO1xyXG5cclxuICAgIHNlbGVjdC5zZXRUeXBlKHNlbGVjdEludGVyZmFjZS50eXBlKTtcclxuXHJcbiAgICBpZiAoc2VsZWN0SW50ZXJmYWNlLm11bHRpcGxlKSB7XHJcbiAgICAgIHNlbGVjdC5zZXRNdWx0aXBsZShzZWxlY3RJbnRlcmZhY2UubXVsdGlwbGUpO1xyXG5cclxuICAgICAgaWYgKHNlbGVjdEludGVyZmFjZS5kZWZhdWx0VmFsdWUpIHtcclxuICAgICAgICBpZiAoc2VsZWN0SW50ZXJmYWNlLmRlZmF1bHRWYWx1ZSBpbnN0YW5jZW9mIEFycmF5KSB7XHJcbiAgICAgICAgICBzZWxlY3Quc2V0RGVmYXVsdFZhbHVlKHNlbGVjdEludGVyZmFjZS5kZWZhdWx0VmFsdWUpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICB0aHJvdyBFcnJvcihcclxuICAgICAgICAgICAgJ0lmIHlvdSB3YW50IG11bHRpcGxlIGNob2ljZSBkZWZhdWx0IHZhbHVlIG11c3QgYmUgYW4gQXJyYXknXHJcbiAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgaWYgKHNlbGVjdEludGVyZmFjZS5kZWZhdWx0VmFsdWUpIHtcclxuICAgICAgICBpZiAoc2VsZWN0SW50ZXJmYWNlLmRlZmF1bHRWYWx1ZSBpbnN0YW5jZW9mIEFycmF5KSB7XHJcbiAgICAgICAgICB0aHJvdyBFcnJvcihcclxuICAgICAgICAgICAgXCJJZiB5b3UgZG9uJ3Qgd2FudCBtdWx0aXBsZSBjaG9pY2UgZGVmYXVsdCB2YWx1ZSBtdXN0IG5vdCBiZSBhbiBBcnJheVwiXHJcbiAgICAgICAgICApO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBzZWxlY3Quc2V0RGVmYXVsdFZhbHVlKHNlbGVjdEludGVyZmFjZS5kZWZhdWx0VmFsdWUpO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGlmIChzZWxlY3RJbnRlcmZhY2UubWF0U2VsZWN0VHJpZ2dlck9uKSB7XHJcbiAgICAgIGlmICghc2VsZWN0LmdldE11bHRpcGxlKCkpXHJcbiAgICAgICAgdGhyb3cgRXJyb3IoXHJcbiAgICAgICAgICAnTWF0U2VsZWN0VHJpZ2dlck9uIG1ha2Ugc2Vuc2Ugb25seSBpZiBtdWx0aXBsZSBpcyBzZXQgdG8gdHJ1ZSdcclxuICAgICAgICApO1xyXG4gICAgICBlbHNlIHNlbGVjdC5zZXRNYXRTZWxlY3RUcmlnZ2VyT24oc2VsZWN0SW50ZXJmYWNlLm1hdFNlbGVjdFRyaWdnZXJPbik7XHJcbiAgICB9XHJcblxyXG4gICAgc2VsZWN0LnNldENvbnRyb2xOYW1lKHNlbGVjdEludGVyZmFjZS5jb250cm9sTmFtZSk7XHJcblxyXG4gICAgaWYgKHNlbGVjdEludGVyZmFjZS52YWxpZGF0b3JzKVxyXG4gICAgICBzZWxlY3Quc2V0VmFsaWRhdG9ycyhzZWxlY3RJbnRlcmZhY2UudmFsaWRhdG9ycyk7XHJcblxyXG4gICAgaWYgKHNlbGVjdEludGVyZmFjZS5oaW50TGFiZWxzKSB7XHJcbiAgICAgIHRocm93IEVycm9yKCdIaW50IExhYmVscyBhcmUgbm90IGFsbG93ZWQgb24gc2VsZWN0Jyk7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKHNlbGVjdEludGVyZmFjZS5hcHBlYXJhbmNlKVxyXG4gICAgICBzZWxlY3Quc2V0QXBwZWFyYW5jZShzZWxlY3RJbnRlcmZhY2UuYXBwZWFyYW5jZSk7XHJcblxyXG4gICAgaWYgKHNlbGVjdEludGVyZmFjZS5wbGFjZWhvbGRlcilcclxuICAgICAgdGhyb3cgRXJyb3IoJ1BsYWNlaG9sZGVyIGlzIG5vdCBhbGxvd2VkIG9uIHNlbGVjdCcpO1xyXG5cclxuICAgIGlmIChzZWxlY3RJbnRlcmZhY2UuZGlzYWJsZWQpIHNlbGVjdC5zZXREaXNhYmxlZChzZWxlY3RJbnRlcmZhY2UuZGlzYWJsZWQpO1xyXG5cclxuICAgIGlmIChzZWxlY3RJbnRlcmZhY2UucmVhZG9ubHkpXHJcbiAgICAgIHRocm93IEVycm9yKCdSZWFkb25seSBpcyBub3QgYWxsb3dlZCBvbiBzZWxlY3QnKTtcclxuXHJcbiAgICBpZiAoc2VsZWN0SW50ZXJmYWNlLnJlcXVpcmVkT3B0aW9uKSB7XHJcbiAgICAgIGxldCByZXF1aXJlZE9wdGlvbiA9IHtcclxuICAgICAgICByZXF1aXJlZDogZmFsc2UsXHJcbiAgICAgICAgaGlkZVJlcXVpcmVkTWFya2VyOiBmYWxzZSxcclxuICAgICAgfTtcclxuXHJcbiAgICAgIGlmIChzZWxlY3RJbnRlcmZhY2UucmVxdWlyZWRPcHRpb24ucmVxdWlyZWQpXHJcbiAgICAgICAgcmVxdWlyZWRPcHRpb24ucmVxdWlyZWQgPSBzZWxlY3RJbnRlcmZhY2UucmVxdWlyZWRPcHRpb24ucmVxdWlyZWQ7XHJcblxyXG4gICAgICBpZiAoc2VsZWN0SW50ZXJmYWNlLnJlcXVpcmVkT3B0aW9uLmhpZGVSZXF1aXJlZE1hcmtlcilcclxuICAgICAgICByZXF1aXJlZE9wdGlvbi5oaWRlUmVxdWlyZWRNYXJrZXIgPVxyXG4gICAgICAgICAgc2VsZWN0SW50ZXJmYWNlLnJlcXVpcmVkT3B0aW9uLmhpZGVSZXF1aXJlZE1hcmtlcjtcclxuXHJcbiAgICAgIHNlbGVjdC5zZXRSZXF1aXJlZChyZXF1aXJlZE9wdGlvbik7XHJcblxyXG4gICAgICAvLyBhZGQgcmVxdWlyZWQgdmFsaWRhdG9yRm4gaWYgaXQgZG9lc24ndCBleGlzdHNcclxuICAgICAgbGV0IHZhbGlkYXRvclJlcXVpcmVkRm5FeGlzdHMgPSBmYWxzZTtcclxuICAgICAgKDxGb3JtUm93SXRlbT5zZWxlY3QpXHJcbiAgICAgICAgLmdldFZhbGlkYXRvcnMoKVxyXG4gICAgICAgIC5mb3JFYWNoKCh2YWxpZGF0b3I6IEZvcm1Sb3dJdGVtVmFsaWRhdG9ySW50ZXJmYWNlKSA9PiB7XHJcbiAgICAgICAgICBpZiAodmFsaWRhdG9yLm5hbWUgPT09IFZBTElEQVRPUl9OQU1FUy5SRVFVSVJFRCkge1xyXG4gICAgICAgICAgICB2YWxpZGF0b3JSZXF1aXJlZEZuRXhpc3RzID0gdHJ1ZTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgIGlmICghdmFsaWRhdG9yUmVxdWlyZWRGbkV4aXN0cykge1xyXG4gICAgICAgICg8Rm9ybVJvd0l0ZW0+c2VsZWN0KS5hZGRWYWxpZGF0b3Ioe1xyXG4gICAgICAgICAgbWVzc2FnZTogJ3RoZSBmaWVsZCBpcyByZXF1aXJlZCcsXHJcbiAgICAgICAgICBuYW1lOiBWQUxJREFUT1JfTkFNRVMuUkVRVUlSRUQsXHJcbiAgICAgICAgICB2YWxpZGF0b3JGbjogVmFsaWRhdG9ycy5yZXF1aXJlZCxcclxuICAgICAgICB9KTtcclxuICAgICAgfVxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgLy8gY2hlY2sgaWYgaXQgaGFzIHJlcXVpcmVkIHZhbGlkYXRvciBpZiBpdCBoYXMgYWRkIHJlcXVpcmVkT3B0aW9uLnJlcXVpcmVkID0gdHJ1ZVxyXG4gICAgICBsZXQgdmFsaWRhdG9yUmVxdWlyZWRGbkV4aXN0cyA9IGZhbHNlO1xyXG4gICAgICAoPEZvcm1Sb3dJdGVtPnNlbGVjdClcclxuICAgICAgICAuZ2V0VmFsaWRhdG9ycygpXHJcbiAgICAgICAgLmZvckVhY2goKHZhbGlkYXRvcjogRm9ybVJvd0l0ZW1WYWxpZGF0b3JJbnRlcmZhY2UpID0+IHtcclxuICAgICAgICAgIGlmICh2YWxpZGF0b3IubmFtZSA9PT0gVkFMSURBVE9SX05BTUVTLlJFUVVJUkVEKSB7XHJcbiAgICAgICAgICAgIHZhbGlkYXRvclJlcXVpcmVkRm5FeGlzdHMgPSB0cnVlO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgaWYgKHZhbGlkYXRvclJlcXVpcmVkRm5FeGlzdHMpIHtcclxuICAgICAgICBsZXQgcmVxdWlyZWRPcHRpb24gPSB7XHJcbiAgICAgICAgICByZXF1aXJlZDogdHJ1ZSxcclxuICAgICAgICAgIGhpZGVSZXF1aXJlZE1hcmtlcjogZmFsc2UsXHJcbiAgICAgICAgfTtcclxuICAgICAgICBzZWxlY3Quc2V0UmVxdWlyZWQocmVxdWlyZWRPcHRpb24pO1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKHNlbGVjdEludGVyZmFjZS5sYWJlbE5hbWUpXHJcbiAgICAgIHNlbGVjdC5zZXRMYWJlbE5hbWUoc2VsZWN0SW50ZXJmYWNlLmxhYmVsTmFtZSk7XHJcbiAgICBlbHNlIHRocm93IEVycm9yKCdMYWJlbCBuYW1lIGlzIHJlcXVpcmVkJyk7XHJcblxyXG4gICAgaWYgKHNlbGVjdEludGVyZmFjZS5tYXRJbWFnZXMpIHtcclxuICAgICAgaWYgKHNlbGVjdEludGVyZmFjZS5tYXRJbWFnZXMubWF0UHJlZml4SW1nVGV4dClcclxuICAgICAgICBzZWxlY3Quc2V0TWF0UHJlZml4SW1nVGV4dChzZWxlY3RJbnRlcmZhY2UubWF0SW1hZ2VzLm1hdFByZWZpeEltZ1RleHQpO1xyXG5cclxuICAgICAgaWYgKHNlbGVjdEludGVyZmFjZS5tYXRJbWFnZXMubWF0U3VmZml4SW1nVGV4dClcclxuICAgICAgICBzZWxlY3Quc2V0TWF0U3VmZml4SW1nVGV4dChzZWxlY3RJbnRlcmZhY2UubWF0SW1hZ2VzLm1hdFN1ZmZpeEltZ1RleHQpO1xyXG4gICAgfVxyXG5cclxuICAgIGlmIChzZWxlY3RJbnRlcmZhY2UudGV4dFByZWZpeClcclxuICAgICAgc2VsZWN0LnNldFRleHRQcmVmaXgoc2VsZWN0SW50ZXJmYWNlLnRleHRQcmVmaXgpO1xyXG5cclxuICAgIGlmIChzZWxlY3RJbnRlcmZhY2UudGV4dFN1ZmZpeClcclxuICAgICAgc2VsZWN0LnNldFRleHRTdWZmaXgoc2VsZWN0SW50ZXJmYWNlLnRleHRTdWZmaXgpO1xyXG5cclxuICAgIGlmIChzZWxlY3RJbnRlcmZhY2Uub3B0aW9uVmFsdWVzICYmIHNlbGVjdEludGVyZmFjZS5vcHRpb25WYWx1ZXMubGVuZ3RoID4gMClcclxuICAgICAgc2VsZWN0LnNldE9wdGlvblZhbHVlcyhzZWxlY3RJbnRlcmZhY2Uub3B0aW9uVmFsdWVzKTtcclxuICAgIGVsc2UgdGhyb3cgRXJyb3IoJ09wdGlvbiBWYWx1ZXMgYXJlIHJlcXVpcmVkIGZvciBzZWxlY3QnKTtcclxuXHJcbiAgICByZXR1cm4gc2VsZWN0O1xyXG4gIH1cclxufVxyXG4iXX0=