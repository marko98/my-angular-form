import { VALIDATOR_NAMES, } from "../../../../model/structural/composite/form/form-row-item.model";
import { Textarea, } from "../../../structural/composite/form/textarea/textarea.model";
import { Validators } from "@angular/forms";
export class TextareaConstructionStrategy {
    construct(textareaInterface) {
        let textarea = new Textarea();
        textarea.setType(textareaInterface.type);
        textarea.setDefaultValue(textareaInterface.defaultValue);
        textarea.setControlName(textareaInterface.controlName);
        if (textareaInterface.validators)
            textarea.setValidators(textareaInterface.validators);
        if (textareaInterface.showHintAboutMinMaxLength) {
            // check if maxLength or minLength validator exists
            let minOrMaxLengthValidatorExists = false;
            textarea
                .getValidators()
                .forEach((validator) => {
                if (validator.name == VALIDATOR_NAMES.MIN_LENGTH ||
                    validator.name == VALIDATOR_NAMES.MAX_LENGTH) {
                    minOrMaxLengthValidatorExists = true;
                }
            });
            if (minOrMaxLengthValidatorExists) {
                (textarea).setShowHintAboutMinMaxLength(true);
            }
            else {
                throw Error("For showHintAboutMinMaxLength to work field needs to have minLength or maxLength validator");
            }
        }
        if (textareaInterface.hintLabels) {
            if (textareaInterface.hintLabels.leftHintLabelContext)
                textarea.setLeftHintLabel(textareaInterface.hintLabels.leftHintLabelContext);
            if (textareaInterface.hintLabels.rightHintLabelContext) {
                if ((textarea).getShowHintAboutMinMaxLength()) {
                    console.log(textarea.getControlName() +
                        " - Right Hint Label Context is not allowed cause you are using Show Hint About Min or Max Length");
                }
                else {
                    textarea.setRightHintLabel(textareaInterface.hintLabels.rightHintLabelContext);
                }
            }
        }
        if (textareaInterface.appearance)
            textarea.setAppearance(textareaInterface.appearance);
        if (textareaInterface.placeholder)
            textarea.setPlaceholder(textareaInterface.placeholder);
        if (textareaInterface.disabled)
            textarea.setDisabled(textareaInterface.disabled);
        if (textareaInterface.readonly)
            textarea.setReadonly(textareaInterface.readonly);
        if (textareaInterface.requiredOption) {
            let requiredOption = {
                required: false,
                hideRequiredMarker: false,
            };
            if (textareaInterface.requiredOption.required)
                requiredOption.required = textareaInterface.requiredOption.required;
            if (textareaInterface.requiredOption.hideRequiredMarker)
                requiredOption.hideRequiredMarker =
                    textareaInterface.requiredOption.hideRequiredMarker;
            textarea.setRequired(requiredOption);
            // add required validatorFn if it doesn't exists
            let validatorRequiredFnExists = false;
            textarea
                .getValidators()
                .forEach((validator) => {
                if (validator.name === VALIDATOR_NAMES.REQUIRED) {
                    validatorRequiredFnExists = true;
                }
            });
            if (!validatorRequiredFnExists) {
                textarea.addValidator({
                    message: "the field is required",
                    name: VALIDATOR_NAMES.REQUIRED,
                    validatorFn: Validators.required,
                });
            }
        }
        else {
            // check if it has required validator if it has add requiredOption.required = true
            let validatorRequiredFnExists = false;
            textarea
                .getValidators()
                .forEach((validator) => {
                if (validator.name === VALIDATOR_NAMES.REQUIRED) {
                    validatorRequiredFnExists = true;
                }
            });
            if (validatorRequiredFnExists) {
                let requiredOption = {
                    required: true,
                    hideRequiredMarker: false,
                };
                textarea.setRequired(requiredOption);
            }
        }
        if (textareaInterface.labelName)
            textarea.setLabelName(textareaInterface.labelName);
        else
            throw Error("Label name is required");
        if (textareaInterface.matImages) {
            if (textareaInterface.matImages.matPrefixImgText)
                textarea.setMatPrefixImgText(textareaInterface.matImages.matPrefixImgText);
            if (textareaInterface.matImages.matSuffixImgText)
                textarea.setMatSuffixImgText(textareaInterface.matImages.matSuffixImgText);
        }
        if (textareaInterface.textPrefix)
            textarea.setTextPrefix(textareaInterface.textPrefix);
        if (textareaInterface.textSuffix)
            textarea.setTextSuffix(textareaInterface.textSuffix);
        return textarea;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGV4dGFyZWEtY29uc3RydWN0aW9uLXN0cmF0ZWd5Lm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXktYW5ndWxhci1mb3JtLyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9tb2RlbC9iZWhhdmlvdXJhbC9zdHJhdGVneS9mb3JtLXJvdy1pdGVtL3RleHRhcmVhLWNvbnN0cnVjdGlvbi1zdHJhdGVneS5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQ0wsZUFBZSxHQUdoQixNQUFNLGlFQUFpRSxDQUFDO0FBRXpFLE9BQU8sRUFFTCxRQUFRLEdBQ1QsTUFBTSw0REFBNEQsQ0FBQztBQUVwRSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFFNUMsTUFBTSxPQUFPLDRCQUE0QjtJQUV2QyxTQUFTLENBQUMsaUJBQW9DO1FBQzVDLElBQUksUUFBUSxHQUFhLElBQUksUUFBUSxFQUFFLENBQUM7UUFFeEMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUV6QyxRQUFRLENBQUMsZUFBZSxDQUFDLGlCQUFpQixDQUFDLFlBQVksQ0FBQyxDQUFDO1FBRXpELFFBQVEsQ0FBQyxjQUFjLENBQUMsaUJBQWlCLENBQUMsV0FBVyxDQUFDLENBQUM7UUFFdkQsSUFBSSxpQkFBaUIsQ0FBQyxVQUFVO1lBQzlCLFFBQVEsQ0FBQyxhQUFhLENBQUMsaUJBQWlCLENBQUMsVUFBVSxDQUFDLENBQUM7UUFFdkQsSUFBSSxpQkFBaUIsQ0FBQyx5QkFBeUIsRUFBRTtZQUMvQyxtREFBbUQ7WUFDbkQsSUFBSSw2QkFBNkIsR0FBRyxLQUFLLENBQUM7WUFDNUIsUUFBUztpQkFDcEIsYUFBYSxFQUFFO2lCQUNmLE9BQU8sQ0FBQyxDQUFDLFNBQXdDLEVBQUUsRUFBRTtnQkFDcEQsSUFDRSxTQUFTLENBQUMsSUFBSSxJQUFJLGVBQWUsQ0FBQyxVQUFVO29CQUM1QyxTQUFTLENBQUMsSUFBSSxJQUFJLGVBQWUsQ0FBQyxVQUFVLEVBQzVDO29CQUNBLDZCQUE2QixHQUFHLElBQUksQ0FBQztpQkFDdEM7WUFDSCxDQUFDLENBQUMsQ0FBQztZQUVMLElBQUksNkJBQTZCLEVBQUU7Z0JBQ0ssQ0FDcEMsUUFBUSxDQUNSLENBQUMsNEJBQTRCLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDdkM7aUJBQU07Z0JBQ0wsTUFBTSxLQUFLLENBQ1QsNEZBQTRGLENBQzdGLENBQUM7YUFDSDtTQUNGO1FBRUQsSUFBSSxpQkFBaUIsQ0FBQyxVQUFVLEVBQUU7WUFDaEMsSUFBSSxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsb0JBQW9CO2dCQUNuRCxRQUFRLENBQUMsZ0JBQWdCLENBQ3ZCLGlCQUFpQixDQUFDLFVBQVUsQ0FBQyxvQkFBb0IsQ0FDbEQsQ0FBQztZQUVKLElBQUksaUJBQWlCLENBQUMsVUFBVSxDQUFDLHFCQUFxQixFQUFFO2dCQUN0RCxJQUN3QyxDQUNwQyxRQUFRLENBQ1IsQ0FBQyw0QkFBNEIsRUFBRSxFQUNqQztvQkFDQSxPQUFPLENBQUMsR0FBRyxDQUNLLFFBQVMsQ0FBQyxjQUFjLEVBQUU7d0JBQ3RDLGtHQUFrRyxDQUNyRyxDQUFDO2lCQUNIO3FCQUFNO29CQUNMLFFBQVEsQ0FBQyxpQkFBaUIsQ0FDeEIsaUJBQWlCLENBQUMsVUFBVSxDQUFDLHFCQUFxQixDQUNuRCxDQUFDO2lCQUNIO2FBQ0Y7U0FDRjtRQUVELElBQUksaUJBQWlCLENBQUMsVUFBVTtZQUM5QixRQUFRLENBQUMsYUFBYSxDQUFDLGlCQUFpQixDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBRXZELElBQUksaUJBQWlCLENBQUMsV0FBVztZQUMvQixRQUFRLENBQUMsY0FBYyxDQUFDLGlCQUFpQixDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBRXpELElBQUksaUJBQWlCLENBQUMsUUFBUTtZQUM1QixRQUFRLENBQUMsV0FBVyxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBRW5ELElBQUksaUJBQWlCLENBQUMsUUFBUTtZQUM1QixRQUFRLENBQUMsV0FBVyxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBRW5ELElBQUksaUJBQWlCLENBQUMsY0FBYyxFQUFFO1lBQ3BDLElBQUksY0FBYyxHQUFHO2dCQUNuQixRQUFRLEVBQUUsS0FBSztnQkFDZixrQkFBa0IsRUFBRSxLQUFLO2FBQzFCLENBQUM7WUFFRixJQUFJLGlCQUFpQixDQUFDLGNBQWMsQ0FBQyxRQUFRO2dCQUMzQyxjQUFjLENBQUMsUUFBUSxHQUFHLGlCQUFpQixDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUM7WUFFdEUsSUFBSSxpQkFBaUIsQ0FBQyxjQUFjLENBQUMsa0JBQWtCO2dCQUNyRCxjQUFjLENBQUMsa0JBQWtCO29CQUMvQixpQkFBaUIsQ0FBQyxjQUFjLENBQUMsa0JBQWtCLENBQUM7WUFFeEQsUUFBUSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUVyQyxnREFBZ0Q7WUFDaEQsSUFBSSx5QkFBeUIsR0FBRyxLQUFLLENBQUM7WUFDeEIsUUFBUztpQkFDcEIsYUFBYSxFQUFFO2lCQUNmLE9BQU8sQ0FBQyxDQUFDLFNBQXdDLEVBQUUsRUFBRTtnQkFDcEQsSUFBSSxTQUFTLENBQUMsSUFBSSxLQUFLLGVBQWUsQ0FBQyxRQUFRLEVBQUU7b0JBQy9DLHlCQUF5QixHQUFHLElBQUksQ0FBQztpQkFDbEM7WUFDSCxDQUFDLENBQUMsQ0FBQztZQUVMLElBQUksQ0FBQyx5QkFBeUIsRUFBRTtnQkFDaEIsUUFBUyxDQUFDLFlBQVksQ0FBQztvQkFDbkMsT0FBTyxFQUFFLHVCQUF1QjtvQkFDaEMsSUFBSSxFQUFFLGVBQWUsQ0FBQyxRQUFRO29CQUM5QixXQUFXLEVBQUUsVUFBVSxDQUFDLFFBQVE7aUJBQ2pDLENBQUMsQ0FBQzthQUNKO1NBQ0Y7YUFBTTtZQUNMLGtGQUFrRjtZQUNsRixJQUFJLHlCQUF5QixHQUFHLEtBQUssQ0FBQztZQUN4QixRQUFTO2lCQUNwQixhQUFhLEVBQUU7aUJBQ2YsT0FBTyxDQUFDLENBQUMsU0FBd0MsRUFBRSxFQUFFO2dCQUNwRCxJQUFJLFNBQVMsQ0FBQyxJQUFJLEtBQUssZUFBZSxDQUFDLFFBQVEsRUFBRTtvQkFDL0MseUJBQXlCLEdBQUcsSUFBSSxDQUFDO2lCQUNsQztZQUNILENBQUMsQ0FBQyxDQUFDO1lBRUwsSUFBSSx5QkFBeUIsRUFBRTtnQkFDN0IsSUFBSSxjQUFjLEdBQUc7b0JBQ25CLFFBQVEsRUFBRSxJQUFJO29CQUNkLGtCQUFrQixFQUFFLEtBQUs7aUJBQzFCLENBQUM7Z0JBQ0YsUUFBUSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsQ0FBQzthQUN0QztTQUNGO1FBRUQsSUFBSSxpQkFBaUIsQ0FBQyxTQUFTO1lBQzdCLFFBQVEsQ0FBQyxZQUFZLENBQUMsaUJBQWlCLENBQUMsU0FBUyxDQUFDLENBQUM7O1lBQ2hELE1BQU0sS0FBSyxDQUFDLHdCQUF3QixDQUFDLENBQUM7UUFFM0MsSUFBSSxpQkFBaUIsQ0FBQyxTQUFTLEVBQUU7WUFDL0IsSUFBSSxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsZ0JBQWdCO2dCQUM5QyxRQUFRLENBQUMsbUJBQW1CLENBQzFCLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FDN0MsQ0FBQztZQUVKLElBQUksaUJBQWlCLENBQUMsU0FBUyxDQUFDLGdCQUFnQjtnQkFDOUMsUUFBUSxDQUFDLG1CQUFtQixDQUMxQixpQkFBaUIsQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQzdDLENBQUM7U0FDTDtRQUVELElBQUksaUJBQWlCLENBQUMsVUFBVTtZQUM5QixRQUFRLENBQUMsYUFBYSxDQUFDLGlCQUFpQixDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBRXZELElBQUksaUJBQWlCLENBQUMsVUFBVTtZQUM5QixRQUFRLENBQUMsYUFBYSxDQUFDLGlCQUFpQixDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBRXZELE9BQU8sUUFBUSxDQUFDO0lBQ2xCLENBQUM7Q0FDRiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XHJcbiAgVkFMSURBVE9SX05BTUVTLFxyXG4gIEZvcm1Sb3dJdGVtVmFsaWRhdG9ySW50ZXJmYWNlLFxyXG4gIEZvcm1Sb3dJdGVtLFxyXG59IGZyb20gXCIuLi8uLi8uLi8uLi9tb2RlbC9zdHJ1Y3R1cmFsL2NvbXBvc2l0ZS9mb3JtL2Zvcm0tcm93LWl0ZW0ubW9kZWxcIjtcclxuaW1wb3J0IHsgRm9ybUZpZWxkSW5wdXRUZXh0UGFzc3dvcmRJbnRlcmZhY2UgfSBmcm9tIFwiLi4vLi4vLi4vLi4vbW9kZWwvc3RydWN0dXJhbC9jb21wb3NpdGUvZm9ybS9mb3JtLWZpZWxkLWlucHV0L2Zvcm0tZmllbGQtaW5wdXQtdGV4dC1wYXNzd29yZC5pbnRlcmZhY2VcIjtcclxuaW1wb3J0IHtcclxuICBUZXh0YXJlYUludGVyZmFjZSxcclxuICBUZXh0YXJlYSxcclxufSBmcm9tIFwiLi4vLi4vLi4vc3RydWN0dXJhbC9jb21wb3NpdGUvZm9ybS90ZXh0YXJlYS90ZXh0YXJlYS5tb2RlbFwiO1xyXG5pbXBvcnQgeyBGb3JtUm93SXRlbUNvbnN0cnVjdGlvblN0cmF0ZWd5IH0gZnJvbSBcIi4vZm9ybS1yb3ctaXRlbS1jb25zdHJ1Y3Rpb24tc3RyYXRlZ3kuaW50ZXJmYWNlXCI7XHJcbmltcG9ydCB7IFZhbGlkYXRvcnMgfSBmcm9tIFwiQGFuZ3VsYXIvZm9ybXNcIjtcclxuXHJcbmV4cG9ydCBjbGFzcyBUZXh0YXJlYUNvbnN0cnVjdGlvblN0cmF0ZWd5XHJcbiAgaW1wbGVtZW50cyBGb3JtUm93SXRlbUNvbnN0cnVjdGlvblN0cmF0ZWd5IHtcclxuICBjb25zdHJ1Y3QodGV4dGFyZWFJbnRlcmZhY2U6IFRleHRhcmVhSW50ZXJmYWNlKTogVGV4dGFyZWEge1xyXG4gICAgbGV0IHRleHRhcmVhOiBUZXh0YXJlYSA9IG5ldyBUZXh0YXJlYSgpO1xyXG5cclxuICAgIHRleHRhcmVhLnNldFR5cGUodGV4dGFyZWFJbnRlcmZhY2UudHlwZSk7XHJcblxyXG4gICAgdGV4dGFyZWEuc2V0RGVmYXVsdFZhbHVlKHRleHRhcmVhSW50ZXJmYWNlLmRlZmF1bHRWYWx1ZSk7XHJcblxyXG4gICAgdGV4dGFyZWEuc2V0Q29udHJvbE5hbWUodGV4dGFyZWFJbnRlcmZhY2UuY29udHJvbE5hbWUpO1xyXG5cclxuICAgIGlmICh0ZXh0YXJlYUludGVyZmFjZS52YWxpZGF0b3JzKVxyXG4gICAgICB0ZXh0YXJlYS5zZXRWYWxpZGF0b3JzKHRleHRhcmVhSW50ZXJmYWNlLnZhbGlkYXRvcnMpO1xyXG5cclxuICAgIGlmICh0ZXh0YXJlYUludGVyZmFjZS5zaG93SGludEFib3V0TWluTWF4TGVuZ3RoKSB7XHJcbiAgICAgIC8vIGNoZWNrIGlmIG1heExlbmd0aCBvciBtaW5MZW5ndGggdmFsaWRhdG9yIGV4aXN0c1xyXG4gICAgICBsZXQgbWluT3JNYXhMZW5ndGhWYWxpZGF0b3JFeGlzdHMgPSBmYWxzZTtcclxuICAgICAgKDxGb3JtUm93SXRlbT50ZXh0YXJlYSlcclxuICAgICAgICAuZ2V0VmFsaWRhdG9ycygpXHJcbiAgICAgICAgLmZvckVhY2goKHZhbGlkYXRvcjogRm9ybVJvd0l0ZW1WYWxpZGF0b3JJbnRlcmZhY2UpID0+IHtcclxuICAgICAgICAgIGlmIChcclxuICAgICAgICAgICAgdmFsaWRhdG9yLm5hbWUgPT0gVkFMSURBVE9SX05BTUVTLk1JTl9MRU5HVEggfHxcclxuICAgICAgICAgICAgdmFsaWRhdG9yLm5hbWUgPT0gVkFMSURBVE9SX05BTUVTLk1BWF9MRU5HVEhcclxuICAgICAgICAgICkge1xyXG4gICAgICAgICAgICBtaW5Pck1heExlbmd0aFZhbGlkYXRvckV4aXN0cyA9IHRydWU7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICBpZiAobWluT3JNYXhMZW5ndGhWYWxpZGF0b3JFeGlzdHMpIHtcclxuICAgICAgICAoPEZvcm1GaWVsZElucHV0VGV4dFBhc3N3b3JkSW50ZXJmYWNlPihcclxuICAgICAgICAgIHRleHRhcmVhXHJcbiAgICAgICAgKSkuc2V0U2hvd0hpbnRBYm91dE1pbk1heExlbmd0aCh0cnVlKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aHJvdyBFcnJvcihcclxuICAgICAgICAgIFwiRm9yIHNob3dIaW50QWJvdXRNaW5NYXhMZW5ndGggdG8gd29yayBmaWVsZCBuZWVkcyB0byBoYXZlIG1pbkxlbmd0aCBvciBtYXhMZW5ndGggdmFsaWRhdG9yXCJcclxuICAgICAgICApO1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKHRleHRhcmVhSW50ZXJmYWNlLmhpbnRMYWJlbHMpIHtcclxuICAgICAgaWYgKHRleHRhcmVhSW50ZXJmYWNlLmhpbnRMYWJlbHMubGVmdEhpbnRMYWJlbENvbnRleHQpXHJcbiAgICAgICAgdGV4dGFyZWEuc2V0TGVmdEhpbnRMYWJlbChcclxuICAgICAgICAgIHRleHRhcmVhSW50ZXJmYWNlLmhpbnRMYWJlbHMubGVmdEhpbnRMYWJlbENvbnRleHRcclxuICAgICAgICApO1xyXG5cclxuICAgICAgaWYgKHRleHRhcmVhSW50ZXJmYWNlLmhpbnRMYWJlbHMucmlnaHRIaW50TGFiZWxDb250ZXh0KSB7XHJcbiAgICAgICAgaWYgKFxyXG4gICAgICAgICAgKDxGb3JtRmllbGRJbnB1dFRleHRQYXNzd29yZEludGVyZmFjZT4oXHJcbiAgICAgICAgICAgIHRleHRhcmVhXHJcbiAgICAgICAgICApKS5nZXRTaG93SGludEFib3V0TWluTWF4TGVuZ3RoKClcclxuICAgICAgICApIHtcclxuICAgICAgICAgIGNvbnNvbGUubG9nKFxyXG4gICAgICAgICAgICAoPEZvcm1Sb3dJdGVtPnRleHRhcmVhKS5nZXRDb250cm9sTmFtZSgpICtcclxuICAgICAgICAgICAgICBcIiAtIFJpZ2h0IEhpbnQgTGFiZWwgQ29udGV4dCBpcyBub3QgYWxsb3dlZCBjYXVzZSB5b3UgYXJlIHVzaW5nIFNob3cgSGludCBBYm91dCBNaW4gb3IgTWF4IExlbmd0aFwiXHJcbiAgICAgICAgICApO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICB0ZXh0YXJlYS5zZXRSaWdodEhpbnRMYWJlbChcclxuICAgICAgICAgICAgdGV4dGFyZWFJbnRlcmZhY2UuaGludExhYmVscy5yaWdodEhpbnRMYWJlbENvbnRleHRcclxuICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKHRleHRhcmVhSW50ZXJmYWNlLmFwcGVhcmFuY2UpXHJcbiAgICAgIHRleHRhcmVhLnNldEFwcGVhcmFuY2UodGV4dGFyZWFJbnRlcmZhY2UuYXBwZWFyYW5jZSk7XHJcblxyXG4gICAgaWYgKHRleHRhcmVhSW50ZXJmYWNlLnBsYWNlaG9sZGVyKVxyXG4gICAgICB0ZXh0YXJlYS5zZXRQbGFjZWhvbGRlcih0ZXh0YXJlYUludGVyZmFjZS5wbGFjZWhvbGRlcik7XHJcblxyXG4gICAgaWYgKHRleHRhcmVhSW50ZXJmYWNlLmRpc2FibGVkKVxyXG4gICAgICB0ZXh0YXJlYS5zZXREaXNhYmxlZCh0ZXh0YXJlYUludGVyZmFjZS5kaXNhYmxlZCk7XHJcblxyXG4gICAgaWYgKHRleHRhcmVhSW50ZXJmYWNlLnJlYWRvbmx5KVxyXG4gICAgICB0ZXh0YXJlYS5zZXRSZWFkb25seSh0ZXh0YXJlYUludGVyZmFjZS5yZWFkb25seSk7XHJcblxyXG4gICAgaWYgKHRleHRhcmVhSW50ZXJmYWNlLnJlcXVpcmVkT3B0aW9uKSB7XHJcbiAgICAgIGxldCByZXF1aXJlZE9wdGlvbiA9IHtcclxuICAgICAgICByZXF1aXJlZDogZmFsc2UsXHJcbiAgICAgICAgaGlkZVJlcXVpcmVkTWFya2VyOiBmYWxzZSxcclxuICAgICAgfTtcclxuXHJcbiAgICAgIGlmICh0ZXh0YXJlYUludGVyZmFjZS5yZXF1aXJlZE9wdGlvbi5yZXF1aXJlZClcclxuICAgICAgICByZXF1aXJlZE9wdGlvbi5yZXF1aXJlZCA9IHRleHRhcmVhSW50ZXJmYWNlLnJlcXVpcmVkT3B0aW9uLnJlcXVpcmVkO1xyXG5cclxuICAgICAgaWYgKHRleHRhcmVhSW50ZXJmYWNlLnJlcXVpcmVkT3B0aW9uLmhpZGVSZXF1aXJlZE1hcmtlcilcclxuICAgICAgICByZXF1aXJlZE9wdGlvbi5oaWRlUmVxdWlyZWRNYXJrZXIgPVxyXG4gICAgICAgICAgdGV4dGFyZWFJbnRlcmZhY2UucmVxdWlyZWRPcHRpb24uaGlkZVJlcXVpcmVkTWFya2VyO1xyXG5cclxuICAgICAgdGV4dGFyZWEuc2V0UmVxdWlyZWQocmVxdWlyZWRPcHRpb24pO1xyXG5cclxuICAgICAgLy8gYWRkIHJlcXVpcmVkIHZhbGlkYXRvckZuIGlmIGl0IGRvZXNuJ3QgZXhpc3RzXHJcbiAgICAgIGxldCB2YWxpZGF0b3JSZXF1aXJlZEZuRXhpc3RzID0gZmFsc2U7XHJcbiAgICAgICg8Rm9ybVJvd0l0ZW0+dGV4dGFyZWEpXHJcbiAgICAgICAgLmdldFZhbGlkYXRvcnMoKVxyXG4gICAgICAgIC5mb3JFYWNoKCh2YWxpZGF0b3I6IEZvcm1Sb3dJdGVtVmFsaWRhdG9ySW50ZXJmYWNlKSA9PiB7XHJcbiAgICAgICAgICBpZiAodmFsaWRhdG9yLm5hbWUgPT09IFZBTElEQVRPUl9OQU1FUy5SRVFVSVJFRCkge1xyXG4gICAgICAgICAgICB2YWxpZGF0b3JSZXF1aXJlZEZuRXhpc3RzID0gdHJ1ZTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgIGlmICghdmFsaWRhdG9yUmVxdWlyZWRGbkV4aXN0cykge1xyXG4gICAgICAgICg8Rm9ybVJvd0l0ZW0+dGV4dGFyZWEpLmFkZFZhbGlkYXRvcih7XHJcbiAgICAgICAgICBtZXNzYWdlOiBcInRoZSBmaWVsZCBpcyByZXF1aXJlZFwiLFxyXG4gICAgICAgICAgbmFtZTogVkFMSURBVE9SX05BTUVTLlJFUVVJUkVELFxyXG4gICAgICAgICAgdmFsaWRhdG9yRm46IFZhbGlkYXRvcnMucmVxdWlyZWQsXHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIC8vIGNoZWNrIGlmIGl0IGhhcyByZXF1aXJlZCB2YWxpZGF0b3IgaWYgaXQgaGFzIGFkZCByZXF1aXJlZE9wdGlvbi5yZXF1aXJlZCA9IHRydWVcclxuICAgICAgbGV0IHZhbGlkYXRvclJlcXVpcmVkRm5FeGlzdHMgPSBmYWxzZTtcclxuICAgICAgKDxGb3JtUm93SXRlbT50ZXh0YXJlYSlcclxuICAgICAgICAuZ2V0VmFsaWRhdG9ycygpXHJcbiAgICAgICAgLmZvckVhY2goKHZhbGlkYXRvcjogRm9ybVJvd0l0ZW1WYWxpZGF0b3JJbnRlcmZhY2UpID0+IHtcclxuICAgICAgICAgIGlmICh2YWxpZGF0b3IubmFtZSA9PT0gVkFMSURBVE9SX05BTUVTLlJFUVVJUkVEKSB7XHJcbiAgICAgICAgICAgIHZhbGlkYXRvclJlcXVpcmVkRm5FeGlzdHMgPSB0cnVlO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgaWYgKHZhbGlkYXRvclJlcXVpcmVkRm5FeGlzdHMpIHtcclxuICAgICAgICBsZXQgcmVxdWlyZWRPcHRpb24gPSB7XHJcbiAgICAgICAgICByZXF1aXJlZDogdHJ1ZSxcclxuICAgICAgICAgIGhpZGVSZXF1aXJlZE1hcmtlcjogZmFsc2UsXHJcbiAgICAgICAgfTtcclxuICAgICAgICB0ZXh0YXJlYS5zZXRSZXF1aXJlZChyZXF1aXJlZE9wdGlvbik7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBpZiAodGV4dGFyZWFJbnRlcmZhY2UubGFiZWxOYW1lKVxyXG4gICAgICB0ZXh0YXJlYS5zZXRMYWJlbE5hbWUodGV4dGFyZWFJbnRlcmZhY2UubGFiZWxOYW1lKTtcclxuICAgIGVsc2UgdGhyb3cgRXJyb3IoXCJMYWJlbCBuYW1lIGlzIHJlcXVpcmVkXCIpO1xyXG5cclxuICAgIGlmICh0ZXh0YXJlYUludGVyZmFjZS5tYXRJbWFnZXMpIHtcclxuICAgICAgaWYgKHRleHRhcmVhSW50ZXJmYWNlLm1hdEltYWdlcy5tYXRQcmVmaXhJbWdUZXh0KVxyXG4gICAgICAgIHRleHRhcmVhLnNldE1hdFByZWZpeEltZ1RleHQoXHJcbiAgICAgICAgICB0ZXh0YXJlYUludGVyZmFjZS5tYXRJbWFnZXMubWF0UHJlZml4SW1nVGV4dFxyXG4gICAgICAgICk7XHJcblxyXG4gICAgICBpZiAodGV4dGFyZWFJbnRlcmZhY2UubWF0SW1hZ2VzLm1hdFN1ZmZpeEltZ1RleHQpXHJcbiAgICAgICAgdGV4dGFyZWEuc2V0TWF0U3VmZml4SW1nVGV4dChcclxuICAgICAgICAgIHRleHRhcmVhSW50ZXJmYWNlLm1hdEltYWdlcy5tYXRTdWZmaXhJbWdUZXh0XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICBpZiAodGV4dGFyZWFJbnRlcmZhY2UudGV4dFByZWZpeClcclxuICAgICAgdGV4dGFyZWEuc2V0VGV4dFByZWZpeCh0ZXh0YXJlYUludGVyZmFjZS50ZXh0UHJlZml4KTtcclxuXHJcbiAgICBpZiAodGV4dGFyZWFJbnRlcmZhY2UudGV4dFN1ZmZpeClcclxuICAgICAgdGV4dGFyZWEuc2V0VGV4dFN1ZmZpeCh0ZXh0YXJlYUludGVyZmFjZS50ZXh0U3VmZml4KTtcclxuXHJcbiAgICByZXR1cm4gdGV4dGFyZWE7XHJcbiAgfVxyXG59XHJcbiJdfQ==