export class Collection {
    constructor() {
        this.children = [];
        this.parent = undefined;
        this.getChildren = () => {
            // vracamo kopiju
            // return this.children.slice();
            return this.children;
        };
        this.getParent = () => {
            return this.parent;
        };
        this.setParent = (parent) => {
            this.parent = parent;
            return true;
        };
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sbGVjdGlvbi5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215LWFuZ3VsYXItZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvbW9kZWwvc3RydWN0dXJhbC9jb21wb3NpdGUvY29sbGVjdGlvbi5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxNQUFNLE9BQWdCLFVBQVU7SUFBaEM7UUFDWSxhQUFRLEdBQWlCLEVBQUUsQ0FBQztRQUM1QixXQUFNLEdBQWUsU0FBUyxDQUFDO1FBUXpDLGdCQUFXLEdBQUcsR0FBaUIsRUFBRTtZQUMvQixpQkFBaUI7WUFDakIsZ0NBQWdDO1lBQ2hDLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUN2QixDQUFDLENBQUM7UUFFRixjQUFTLEdBQUcsR0FBZSxFQUFFO1lBQzNCLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUNyQixDQUFDLENBQUM7UUFFRixjQUFTLEdBQUcsQ0FBQyxNQUFrQixFQUFXLEVBQUU7WUFDMUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7WUFDckIsT0FBTyxJQUFJLENBQUM7UUFDZCxDQUFDLENBQUM7SUFDSixDQUFDO0NBQUEiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgYWJzdHJhY3QgY2xhc3MgQ29sbGVjdGlvbiB7XHJcbiAgcHJvdGVjdGVkIGNoaWxkcmVuOiBDb2xsZWN0aW9uW10gPSBbXTtcclxuICBwcm90ZWN0ZWQgcGFyZW50OiBDb2xsZWN0aW9uID0gdW5kZWZpbmVkO1xyXG5cclxuICBhYnN0cmFjdCBhZGRDaGlsZChjaGlsZDogQ29sbGVjdGlvbik6IGJvb2xlYW47XHJcblxyXG4gIGFic3RyYWN0IHJlbW92ZUNoaWxkKGNoaWxkOiBDb2xsZWN0aW9uKTogYm9vbGVhbjtcclxuXHJcbiAgYWJzdHJhY3Qgc2hvdWxkSGF2ZUNoaWxkcmVuKCk6IGJvb2xlYW47XHJcblxyXG4gIGdldENoaWxkcmVuID0gKCk6IENvbGxlY3Rpb25bXSA9PiB7XHJcbiAgICAvLyB2cmFjYW1vIGtvcGlqdVxyXG4gICAgLy8gcmV0dXJuIHRoaXMuY2hpbGRyZW4uc2xpY2UoKTtcclxuICAgIHJldHVybiB0aGlzLmNoaWxkcmVuO1xyXG4gIH07XHJcblxyXG4gIGdldFBhcmVudCA9ICgpOiBDb2xsZWN0aW9uID0+IHtcclxuICAgIHJldHVybiB0aGlzLnBhcmVudDtcclxuICB9O1xyXG5cclxuICBzZXRQYXJlbnQgPSAocGFyZW50OiBDb2xsZWN0aW9uKTogYm9vbGVhbiA9PiB7XHJcbiAgICB0aGlzLnBhcmVudCA9IHBhcmVudDtcclxuICAgIHJldHVybiB0cnVlO1xyXG4gIH07XHJcbn1cclxuIl19