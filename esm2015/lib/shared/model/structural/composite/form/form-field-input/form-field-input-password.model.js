// model
import { FormFieldInput, } from './form-field-input.model';
export class FormFieldInputPassword extends FormFieldInput {
    constructor() {
        super();
        this.showPassword = false;
        this.showHintAboutMinMaxLength = false;
        this.onShowPassword = () => {
            this.showPassword = true;
            setTimeout(() => {
                this.showPassword = false;
            }, this.showPasswordInMs);
        };
        this.getShowPassword = () => {
            return this.showPassword;
        };
        this.getShowPasswordInMs = () => {
            return this.showPasswordInMs;
        };
        this.setShowPasswordInMs = (showPasswordInMs) => {
            this.showPasswordInMs = showPasswordInMs;
        };
        this.getShowHintAboutMinMaxLength = () => {
            return this.showHintAboutMinMaxLength;
        };
        this.setShowHintAboutMinMaxLength = (showHintAboutMinMaxLength) => {
            this.showHintAboutMinMaxLength = showHintAboutMinMaxLength;
        };
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1maWVsZC1pbnB1dC1wYXNzd29yZC5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215LWFuZ3VsYXItZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvbW9kZWwvc3RydWN0dXJhbC9jb21wb3NpdGUvZm9ybS9mb3JtLWZpZWxkLWlucHV0L2Zvcm0tZmllbGQtaW5wdXQtcGFzc3dvcmQubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsUUFBUTtBQUNSLE9BQU8sRUFDTCxjQUFjLEdBRWYsTUFBTSwwQkFBMEIsQ0FBQztBQVNsQyxNQUFNLE9BQU8sc0JBQXVCLFNBQVEsY0FBYztJQU14RDtRQUNFLEtBQUssRUFBRSxDQUFDO1FBSkYsaUJBQVksR0FBWSxLQUFLLENBQUM7UUFDOUIsOEJBQXlCLEdBQVksS0FBSyxDQUFDO1FBTW5ELG1CQUFjLEdBQUcsR0FBUyxFQUFFO1lBQzFCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO1lBRXpCLFVBQVUsQ0FBQyxHQUFHLEVBQUU7Z0JBQ2QsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7WUFDNUIsQ0FBQyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1FBQzVCLENBQUMsQ0FBQztRQUVGLG9CQUFlLEdBQUcsR0FBWSxFQUFFO1lBQzlCLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQztRQUMzQixDQUFDLENBQUM7UUFFRix3QkFBbUIsR0FBRyxHQUFXLEVBQUU7WUFDakMsT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUM7UUFDL0IsQ0FBQyxDQUFDO1FBRUYsd0JBQW1CLEdBQUcsQ0FBQyxnQkFBd0IsRUFBUSxFQUFFO1lBQ3ZELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxnQkFBZ0IsQ0FBQztRQUMzQyxDQUFDLENBQUM7UUFFRixpQ0FBNEIsR0FBRyxHQUFZLEVBQUU7WUFDM0MsT0FBTyxJQUFJLENBQUMseUJBQXlCLENBQUM7UUFDeEMsQ0FBQyxDQUFDO1FBRUYsaUNBQTRCLEdBQUcsQ0FBQyx5QkFBa0MsRUFBUSxFQUFFO1lBQzFFLElBQUksQ0FBQyx5QkFBeUIsR0FBRyx5QkFBeUIsQ0FBQztRQUM3RCxDQUFDLENBQUM7SUE1QkYsQ0FBQztDQTZCRiIsInNvdXJjZXNDb250ZW50IjpbIi8vIG1vZGVsXHJcbmltcG9ydCB7XHJcbiAgRm9ybUZpZWxkSW5wdXQsXHJcbiAgRm9ybUZpZWxkSW5wdXRJbnRlcmZhY2UsXHJcbn0gZnJvbSAnLi9mb3JtLWZpZWxkLWlucHV0Lm1vZGVsJztcclxuaW1wb3J0IHsgRm9ybUZpZWxkSW5wdXRUZXh0UGFzc3dvcmRJbnRlcmZhY2UgfSBmcm9tICcuL2Zvcm0tZmllbGQtaW5wdXQtdGV4dC1wYXNzd29yZC5pbnRlcmZhY2UnO1xyXG5cclxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIEZvcm1GaWVsZElucHV0UGFzc3dvcmRJbnRlcmZhY2VcclxuICBleHRlbmRzIEZvcm1GaWVsZElucHV0SW50ZXJmYWNlIHtcclxuICBzaG93UGFzc3dvcmRJbk1zPzogbnVtYmVyO1xyXG4gIHNob3dIaW50QWJvdXRNaW5NYXhMZW5ndGg/OiBib29sZWFuO1xyXG59XHJcblxyXG5leHBvcnQgY2xhc3MgRm9ybUZpZWxkSW5wdXRQYXNzd29yZCBleHRlbmRzIEZvcm1GaWVsZElucHV0XHJcbiAgaW1wbGVtZW50cyBGb3JtRmllbGRJbnB1dFRleHRQYXNzd29yZEludGVyZmFjZSB7XHJcbiAgcHJpdmF0ZSBzaG93UGFzc3dvcmRJbk1zPzogbnVtYmVyO1xyXG4gIHByaXZhdGUgc2hvd1Bhc3N3b3JkOiBib29sZWFuID0gZmFsc2U7XHJcbiAgcHJpdmF0ZSBzaG93SGludEFib3V0TWluTWF4TGVuZ3RoOiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgc3VwZXIoKTtcclxuICB9XHJcblxyXG4gIG9uU2hvd1Bhc3N3b3JkID0gKCk6IHZvaWQgPT4ge1xyXG4gICAgdGhpcy5zaG93UGFzc3dvcmQgPSB0cnVlO1xyXG5cclxuICAgIHNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgICB0aGlzLnNob3dQYXNzd29yZCA9IGZhbHNlO1xyXG4gICAgfSwgdGhpcy5zaG93UGFzc3dvcmRJbk1zKTtcclxuICB9O1xyXG5cclxuICBnZXRTaG93UGFzc3dvcmQgPSAoKTogYm9vbGVhbiA9PiB7XHJcbiAgICByZXR1cm4gdGhpcy5zaG93UGFzc3dvcmQ7XHJcbiAgfTtcclxuXHJcbiAgZ2V0U2hvd1Bhc3N3b3JkSW5NcyA9ICgpOiBudW1iZXIgPT4ge1xyXG4gICAgcmV0dXJuIHRoaXMuc2hvd1Bhc3N3b3JkSW5NcztcclxuICB9O1xyXG5cclxuICBzZXRTaG93UGFzc3dvcmRJbk1zID0gKHNob3dQYXNzd29yZEluTXM6IG51bWJlcik6IHZvaWQgPT4ge1xyXG4gICAgdGhpcy5zaG93UGFzc3dvcmRJbk1zID0gc2hvd1Bhc3N3b3JkSW5NcztcclxuICB9O1xyXG5cclxuICBnZXRTaG93SGludEFib3V0TWluTWF4TGVuZ3RoID0gKCk6IGJvb2xlYW4gPT4ge1xyXG4gICAgcmV0dXJuIHRoaXMuc2hvd0hpbnRBYm91dE1pbk1heExlbmd0aDtcclxuICB9O1xyXG5cclxuICBzZXRTaG93SGludEFib3V0TWluTWF4TGVuZ3RoID0gKHNob3dIaW50QWJvdXRNaW5NYXhMZW5ndGg6IGJvb2xlYW4pOiB2b2lkID0+IHtcclxuICAgIHRoaXMuc2hvd0hpbnRBYm91dE1pbk1heExlbmd0aCA9IHNob3dIaW50QWJvdXRNaW5NYXhMZW5ndGg7XHJcbiAgfTtcclxufVxyXG4iXX0=