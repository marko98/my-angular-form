import { FormRowItem } from '../form-row-item.model';
import { MAT_COLOR } from '../../../../../material.module';
export var MAT_BUTTON_TYPE;
(function (MAT_BUTTON_TYPE) {
    MAT_BUTTON_TYPE["BASIC"] = "BASIC";
    MAT_BUTTON_TYPE["RAISED"] = "RAISED";
    MAT_BUTTON_TYPE["STROKED"] = "STROKED";
    MAT_BUTTON_TYPE["FLAT"] = "FLAT";
    MAT_BUTTON_TYPE["ICON"] = "ICON";
    MAT_BUTTON_TYPE["FAB"] = "FAB";
    MAT_BUTTON_TYPE["MINI_FAB"] = "MINI_FAB";
})(MAT_BUTTON_TYPE || (MAT_BUTTON_TYPE = {}));
export var BUTTON_TYPE;
(function (BUTTON_TYPE) {
    BUTTON_TYPE["SUBMIT"] = "submit";
    BUTTON_TYPE["BUTTON"] = "button";
    BUTTON_TYPE["RESET"] = "reset";
    BUTTON_TYPE["MENU"] = "menu";
})(BUTTON_TYPE || (BUTTON_TYPE = {}));
export class Button extends FormRowItem {
    constructor() {
        super();
        this.color = MAT_COLOR.EMPTY;
        this.context = 'Click me';
        this.matButtonType = MAT_BUTTON_TYPE.RAISED;
        this.buttonType = BUTTON_TYPE.BUTTON;
        this.matIconString = '';
        this.getColor = () => {
            return this.color;
        };
        this.setColor = (color) => {
            this.color = color;
        };
        this.getContext = () => {
            return this.context;
        };
        this.setContext = (context) => {
            this.context = context;
        };
        this.getMatButtonType = () => {
            return this.matButtonType;
        };
        this.setMatButtonType = (matButtonType) => {
            this.matButtonType = matButtonType;
        };
        this.getButtonType = () => {
            return this.buttonType;
        };
        this.setButtonType = (buttonType) => {
            this.buttonType = buttonType;
        };
        this.getFunctionToExecute = () => {
            return this.functionToExecute;
        };
        this.setFunctionToExecute = (functionToExecute) => {
            this.functionToExecute = functionToExecute;
        };
        this.getMatIconString = () => {
            return this.matIconString;
        };
        this.setMatIconString = (matIconString) => {
            this.matIconString = matIconString;
        };
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnV0dG9uLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXktYW5ndWxhci1mb3JtLyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9tb2RlbC9zdHJ1Y3R1cmFsL2NvbXBvc2l0ZS9mb3JtL2J1dHRvbi9idXR0b24ubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUF3QixXQUFXLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUMzRSxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sZ0NBQWdDLENBQUM7QUFZM0QsTUFBTSxDQUFOLElBQVksZUFRWDtBQVJELFdBQVksZUFBZTtJQUN6QixrQ0FBZSxDQUFBO0lBQ2Ysb0NBQWlCLENBQUE7SUFDakIsc0NBQW1CLENBQUE7SUFDbkIsZ0NBQWEsQ0FBQTtJQUNiLGdDQUFhLENBQUE7SUFDYiw4QkFBVyxDQUFBO0lBQ1gsd0NBQXFCLENBQUE7QUFDdkIsQ0FBQyxFQVJXLGVBQWUsS0FBZixlQUFlLFFBUTFCO0FBRUQsTUFBTSxDQUFOLElBQVksV0FLWDtBQUxELFdBQVksV0FBVztJQUNyQixnQ0FBaUIsQ0FBQTtJQUNqQixnQ0FBaUIsQ0FBQTtJQUNqQiw4QkFBZSxDQUFBO0lBQ2YsNEJBQWEsQ0FBQTtBQUNmLENBQUMsRUFMVyxXQUFXLEtBQVgsV0FBVyxRQUt0QjtBQUVELE1BQU0sT0FBTyxNQUFPLFNBQVEsV0FBVztJQVFyQztRQUNFLEtBQUssRUFBRSxDQUFDO1FBUkYsVUFBSyxHQUFjLFNBQVMsQ0FBQyxLQUFLLENBQUM7UUFDbkMsWUFBTyxHQUFXLFVBQVUsQ0FBQztRQUM3QixrQkFBYSxHQUFvQixlQUFlLENBQUMsTUFBTSxDQUFDO1FBQ3hELGVBQVUsR0FBZ0IsV0FBVyxDQUFDLE1BQU0sQ0FBQztRQUU3QyxrQkFBYSxHQUFXLEVBQUUsQ0FBQztRQU01QixhQUFRLEdBQUcsR0FBYyxFQUFFO1lBQ2hDLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztRQUNwQixDQUFDLENBQUM7UUFFSyxhQUFRLEdBQUcsQ0FBQyxLQUFnQixFQUFRLEVBQUU7WUFDM0MsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7UUFDckIsQ0FBQyxDQUFDO1FBRUssZUFBVSxHQUFHLEdBQVcsRUFBRTtZQUMvQixPQUFPLElBQUksQ0FBQyxPQUFPLENBQUM7UUFDdEIsQ0FBQyxDQUFDO1FBRUssZUFBVSxHQUFHLENBQUMsT0FBZSxFQUFRLEVBQUU7WUFDNUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7UUFDekIsQ0FBQyxDQUFDO1FBRUsscUJBQWdCLEdBQUcsR0FBb0IsRUFBRTtZQUM5QyxPQUFPLElBQUksQ0FBQyxhQUFhLENBQUM7UUFDNUIsQ0FBQyxDQUFDO1FBRUsscUJBQWdCLEdBQUcsQ0FBQyxhQUE4QixFQUFRLEVBQUU7WUFDakUsSUFBSSxDQUFDLGFBQWEsR0FBRyxhQUFhLENBQUM7UUFDckMsQ0FBQyxDQUFDO1FBRUssa0JBQWEsR0FBRyxHQUFnQixFQUFFO1lBQ3ZDLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQztRQUN6QixDQUFDLENBQUM7UUFFSyxrQkFBYSxHQUFHLENBQUMsVUFBdUIsRUFBUSxFQUFFO1lBQ3ZELElBQUksQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFDO1FBQy9CLENBQUMsQ0FBQztRQUVLLHlCQUFvQixHQUFHLEdBQWEsRUFBRTtZQUMzQyxPQUFPLElBQUksQ0FBQyxpQkFBaUIsQ0FBQztRQUNoQyxDQUFDLENBQUM7UUFFSyx5QkFBb0IsR0FBRyxDQUFDLGlCQUEyQixFQUFRLEVBQUU7WUFDbEUsSUFBSSxDQUFDLGlCQUFpQixHQUFHLGlCQUFpQixDQUFDO1FBQzdDLENBQUMsQ0FBQztRQUVLLHFCQUFnQixHQUFHLEdBQVcsRUFBRTtZQUNyQyxPQUFPLElBQUksQ0FBQyxhQUFhLENBQUM7UUFDNUIsQ0FBQyxDQUFDO1FBRUsscUJBQWdCLEdBQUcsQ0FBQyxhQUFxQixFQUFRLEVBQUU7WUFDeEQsSUFBSSxDQUFDLGFBQWEsR0FBRyxhQUFhLENBQUM7UUFDckMsQ0FBQyxDQUFDO0lBaERGLENBQUM7Q0FpREYiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBGb3JtUm93SXRlbUludGVyZmFjZSwgRm9ybVJvd0l0ZW0gfSBmcm9tICcuLi9mb3JtLXJvdy1pdGVtLm1vZGVsJztcclxuaW1wb3J0IHsgTUFUX0NPTE9SIH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vbWF0ZXJpYWwubW9kdWxlJztcclxuXHJcbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBCdXR0b25JbnRlcmZhY2UgZXh0ZW5kcyBGb3JtUm93SXRlbUludGVyZmFjZSB7XHJcbiAgY29sb3I6IE1BVF9DT0xPUjtcclxuICBkaXNhYmxlZDogYm9vbGVhbjtcclxuICBjb250ZXh0OiBzdHJpbmc7XHJcbiAgbWF0QnV0dG9uVHlwZTogTUFUX0JVVFRPTl9UWVBFO1xyXG4gIGJ1dHRvblR5cGU6IEJVVFRPTl9UWVBFO1xyXG4gIGZ1bmN0aW9uVG9FeGVjdXRlOiBhbnk7XHJcbiAgbWF0SWNvblN0cmluZzogc3RyaW5nO1xyXG59XHJcblxyXG5leHBvcnQgZW51bSBNQVRfQlVUVE9OX1RZUEUge1xyXG4gIEJBU0lDID0gJ0JBU0lDJyxcclxuICBSQUlTRUQgPSAnUkFJU0VEJyxcclxuICBTVFJPS0VEID0gJ1NUUk9LRUQnLFxyXG4gIEZMQVQgPSAnRkxBVCcsXHJcbiAgSUNPTiA9ICdJQ09OJyxcclxuICBGQUIgPSAnRkFCJyxcclxuICBNSU5JX0ZBQiA9ICdNSU5JX0ZBQicsXHJcbn1cclxuXHJcbmV4cG9ydCBlbnVtIEJVVFRPTl9UWVBFIHtcclxuICBTVUJNSVQgPSAnc3VibWl0JyxcclxuICBCVVRUT04gPSAnYnV0dG9uJyxcclxuICBSRVNFVCA9ICdyZXNldCcsXHJcbiAgTUVOVSA9ICdtZW51JyxcclxufVxyXG5cclxuZXhwb3J0IGNsYXNzIEJ1dHRvbiBleHRlbmRzIEZvcm1Sb3dJdGVtIHtcclxuICBwcml2YXRlIGNvbG9yOiBNQVRfQ09MT1IgPSBNQVRfQ09MT1IuRU1QVFk7XHJcbiAgcHJpdmF0ZSBjb250ZXh0OiBzdHJpbmcgPSAnQ2xpY2sgbWUnO1xyXG4gIHByaXZhdGUgbWF0QnV0dG9uVHlwZTogTUFUX0JVVFRPTl9UWVBFID0gTUFUX0JVVFRPTl9UWVBFLlJBSVNFRDtcclxuICBwcml2YXRlIGJ1dHRvblR5cGU6IEJVVFRPTl9UWVBFID0gQlVUVE9OX1RZUEUuQlVUVE9OO1xyXG4gIHByaXZhdGUgZnVuY3Rpb25Ub0V4ZWN1dGU6IEZ1bmN0aW9uO1xyXG4gIHByaXZhdGUgbWF0SWNvblN0cmluZzogc3RyaW5nID0gJyc7XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgc3VwZXIoKTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBnZXRDb2xvciA9ICgpOiBNQVRfQ09MT1IgPT4ge1xyXG4gICAgcmV0dXJuIHRoaXMuY29sb3I7XHJcbiAgfTtcclxuXHJcbiAgcHVibGljIHNldENvbG9yID0gKGNvbG9yOiBNQVRfQ09MT1IpOiB2b2lkID0+IHtcclxuICAgIHRoaXMuY29sb3IgPSBjb2xvcjtcclxuICB9O1xyXG5cclxuICBwdWJsaWMgZ2V0Q29udGV4dCA9ICgpOiBzdHJpbmcgPT4ge1xyXG4gICAgcmV0dXJuIHRoaXMuY29udGV4dDtcclxuICB9O1xyXG5cclxuICBwdWJsaWMgc2V0Q29udGV4dCA9IChjb250ZXh0OiBzdHJpbmcpOiB2b2lkID0+IHtcclxuICAgIHRoaXMuY29udGV4dCA9IGNvbnRleHQ7XHJcbiAgfTtcclxuXHJcbiAgcHVibGljIGdldE1hdEJ1dHRvblR5cGUgPSAoKTogTUFUX0JVVFRPTl9UWVBFID0+IHtcclxuICAgIHJldHVybiB0aGlzLm1hdEJ1dHRvblR5cGU7XHJcbiAgfTtcclxuXHJcbiAgcHVibGljIHNldE1hdEJ1dHRvblR5cGUgPSAobWF0QnV0dG9uVHlwZTogTUFUX0JVVFRPTl9UWVBFKTogdm9pZCA9PiB7XHJcbiAgICB0aGlzLm1hdEJ1dHRvblR5cGUgPSBtYXRCdXR0b25UeXBlO1xyXG4gIH07XHJcblxyXG4gIHB1YmxpYyBnZXRCdXR0b25UeXBlID0gKCk6IEJVVFRPTl9UWVBFID0+IHtcclxuICAgIHJldHVybiB0aGlzLmJ1dHRvblR5cGU7XHJcbiAgfTtcclxuXHJcbiAgcHVibGljIHNldEJ1dHRvblR5cGUgPSAoYnV0dG9uVHlwZTogQlVUVE9OX1RZUEUpOiB2b2lkID0+IHtcclxuICAgIHRoaXMuYnV0dG9uVHlwZSA9IGJ1dHRvblR5cGU7XHJcbiAgfTtcclxuXHJcbiAgcHVibGljIGdldEZ1bmN0aW9uVG9FeGVjdXRlID0gKCk6IEZ1bmN0aW9uID0+IHtcclxuICAgIHJldHVybiB0aGlzLmZ1bmN0aW9uVG9FeGVjdXRlO1xyXG4gIH07XHJcblxyXG4gIHB1YmxpYyBzZXRGdW5jdGlvblRvRXhlY3V0ZSA9IChmdW5jdGlvblRvRXhlY3V0ZTogRnVuY3Rpb24pOiB2b2lkID0+IHtcclxuICAgIHRoaXMuZnVuY3Rpb25Ub0V4ZWN1dGUgPSBmdW5jdGlvblRvRXhlY3V0ZTtcclxuICB9O1xyXG5cclxuICBwdWJsaWMgZ2V0TWF0SWNvblN0cmluZyA9ICgpOiBzdHJpbmcgPT4ge1xyXG4gICAgcmV0dXJuIHRoaXMubWF0SWNvblN0cmluZztcclxuICB9O1xyXG5cclxuICBwdWJsaWMgc2V0TWF0SWNvblN0cmluZyA9IChtYXRJY29uU3RyaW5nOiBzdHJpbmcpOiB2b2lkID0+IHtcclxuICAgIHRoaXMubWF0SWNvblN0cmluZyA9IG1hdEljb25TdHJpbmc7XHJcbiAgfTtcclxufVxyXG4iXX0=