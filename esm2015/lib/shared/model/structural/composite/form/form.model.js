// model
import { Collection } from '../collection.model';
import { FormGroup } from '@angular/forms';
export class Form extends Collection {
    constructor(formInterface) {
        super();
        // opcija za progress-spinner ili progress-bar
        this.showSubmitButton = false;
        this.submitOnlyIfFormValid = false;
        this.showCancelButton = false;
        this.showResetButton = false;
        this.submitButtonText = 'Submit';
        this.cancelButtonText = 'Cancel';
        this.resetButtonText = 'Reset';
        this._formGroup = new FormGroup({});
        this.controlNames = [];
        this.disableAllRowItems = () => {
            console.log('disable');
            this.children.forEach((row) => {
                row.disableAllRowItems();
            });
        };
        this.getFormGroup = () => {
            return this._formGroup;
        };
        this.setFormGroup = (formGroup) => {
            this._formGroup = formGroup;
        };
        if (formInterface.showSubmitButton)
            this.showSubmitButton = formInterface.showSubmitButton;
        if (formInterface.submitOnlyIfFormValid)
            this.submitOnlyIfFormValid = formInterface.submitOnlyIfFormValid;
        if (formInterface.showCancelButton)
            this.showCancelButton = formInterface.showCancelButton;
        if (formInterface.showResetButton)
            this.showResetButton = formInterface.showResetButton;
        if (formInterface.submitButtonText)
            this.submitButtonText = formInterface.submitButtonText;
        if (formInterface.cancelButtonText)
            this.cancelButtonText = formInterface.cancelButtonText;
        if (formInterface.resetButtonText)
            this.resetButtonText = formInterface.resetButtonText;
    }
    addChild(formRow) {
        formRow.getChildren().forEach((rowItem) => {
            formRow.addChild(rowItem);
        });
        this.children.push(formRow);
        formRow.setParent(this);
        // console.log(this._formGroup.value);
        return true;
    }
    removeChild(formRow) {
        formRow.getChildren().forEach((rowItem) => {
            formRow.removeChild(rowItem);
        });
        this.children = this.children.filter((child) => child !== formRow);
        formRow.setParent(undefined);
        // console.log(this._formGroup.value);
        return true;
    }
    shouldHaveChildren() {
        return true;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215LWFuZ3VsYXItZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvbW9kZWwvc3RydWN0dXJhbC9jb21wb3NpdGUvZm9ybS9mb3JtLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLFFBQVE7QUFDUixPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFHakQsT0FBTyxFQUFFLFNBQVMsRUFBZSxNQUFNLGdCQUFnQixDQUFDO0FBYXhELE1BQU0sT0FBTyxJQUFLLFNBQVEsVUFBVTtJQWFsQyxZQUFZLGFBQTZCO1FBQ3ZDLEtBQUssRUFBRSxDQUFDO1FBYlYsOENBQThDO1FBQ3ZDLHFCQUFnQixHQUFZLEtBQUssQ0FBQztRQUNsQywwQkFBcUIsR0FBWSxLQUFLLENBQUM7UUFDdkMscUJBQWdCLEdBQVksS0FBSyxDQUFDO1FBQ2xDLG9CQUFlLEdBQVksS0FBSyxDQUFDO1FBQ2pDLHFCQUFnQixHQUFXLFFBQVEsQ0FBQztRQUNwQyxxQkFBZ0IsR0FBVyxRQUFRLENBQUM7UUFDcEMsb0JBQWUsR0FBVyxPQUFPLENBQUM7UUFDakMsZUFBVSxHQUFjLElBQUksU0FBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBRTNDLGlCQUFZLEdBQWEsRUFBRSxDQUFDO1FBb0I1Qix1QkFBa0IsR0FBRyxHQUFTLEVBQUU7WUFDckMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUN2QixJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDLEdBQVksRUFBRSxFQUFFO2dCQUNyQyxHQUFHLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztZQUMzQixDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQztRQWdDSyxpQkFBWSxHQUFHLEdBQWMsRUFBRTtZQUNwQyxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUM7UUFDekIsQ0FBQyxDQUFDO1FBRUssaUJBQVksR0FBRyxDQUFDLFNBQW9CLEVBQVEsRUFBRTtZQUNuRCxJQUFJLENBQUMsVUFBVSxHQUFHLFNBQVMsQ0FBQztRQUM5QixDQUFDLENBQUM7UUEzREEsSUFBSSxhQUFhLENBQUMsZ0JBQWdCO1lBQ2hDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxhQUFhLENBQUMsZ0JBQWdCLENBQUM7UUFDekQsSUFBSSxhQUFhLENBQUMscUJBQXFCO1lBQ3JDLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxhQUFhLENBQUMscUJBQXFCLENBQUM7UUFDbkUsSUFBSSxhQUFhLENBQUMsZ0JBQWdCO1lBQ2hDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxhQUFhLENBQUMsZ0JBQWdCLENBQUM7UUFDekQsSUFBSSxhQUFhLENBQUMsZUFBZTtZQUMvQixJQUFJLENBQUMsZUFBZSxHQUFHLGFBQWEsQ0FBQyxlQUFlLENBQUM7UUFDdkQsSUFBSSxhQUFhLENBQUMsZ0JBQWdCO1lBQ2hDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxhQUFhLENBQUMsZ0JBQWdCLENBQUM7UUFDekQsSUFBSSxhQUFhLENBQUMsZ0JBQWdCO1lBQ2hDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxhQUFhLENBQUMsZ0JBQWdCLENBQUM7UUFDekQsSUFBSSxhQUFhLENBQUMsZUFBZTtZQUMvQixJQUFJLENBQUMsZUFBZSxHQUFHLGFBQWEsQ0FBQyxlQUFlLENBQUM7SUFDekQsQ0FBQztJQVNELFFBQVEsQ0FBQyxPQUFnQjtRQUN2QixPQUFPLENBQUMsV0FBVyxFQUFFLENBQUMsT0FBTyxDQUFDLENBQUMsT0FBb0IsRUFBRSxFQUFFO1lBQ3JELE9BQU8sQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDNUIsQ0FBQyxDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUM1QixPQUFPLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBRXhCLHNDQUFzQztRQUV0QyxPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7SUFFRCxXQUFXLENBQUMsT0FBZ0I7UUFDMUIsT0FBTyxDQUFDLFdBQVcsRUFBRSxDQUFDLE9BQU8sQ0FBQyxDQUFDLE9BQW9CLEVBQUUsRUFBRTtZQUNyRCxPQUFPLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQy9CLENBQUMsQ0FBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsS0FBSyxLQUFLLE9BQU8sQ0FBQyxDQUFDO1FBQ25FLE9BQU8sQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLENBQUM7UUFFN0Isc0NBQXNDO1FBRXRDLE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUVELGtCQUFrQjtRQUNoQixPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7Q0FTRiIsInNvdXJjZXNDb250ZW50IjpbIi8vIG1vZGVsXHJcbmltcG9ydCB7IENvbGxlY3Rpb24gfSBmcm9tICcuLi9jb2xsZWN0aW9uLm1vZGVsJztcclxuaW1wb3J0IHsgRm9ybVJvdyB9IGZyb20gJy4vZm9ybS1yb3cubW9kZWwnO1xyXG5pbXBvcnQgeyBGb3JtUm93SXRlbSB9IGZyb20gJy4vZm9ybS1yb3ctaXRlbS5tb2RlbCc7XHJcbmltcG9ydCB7IEZvcm1Hcm91cCwgRm9ybUNvbnRyb2wgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcblxyXG4vLyBpbnRlcmZhY2VcclxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIEZvcm1JbnRlcmZhY2Uge1xyXG4gIHNob3dTdWJtaXRCdXR0b24/OiBib29sZWFuO1xyXG4gIHN1Ym1pdEJ1dHRvblRleHQ/OiBzdHJpbmc7XHJcbiAgc2hvd0NhbmNlbEJ1dHRvbj86IGJvb2xlYW47XHJcbiAgY2FuY2VsQnV0dG9uVGV4dD86IHN0cmluZztcclxuICBzaG93UmVzZXRCdXR0b24/OiBib29sZWFuO1xyXG4gIHJlc2V0QnV0dG9uVGV4dD86IHN0cmluZztcclxuICBzdWJtaXRPbmx5SWZGb3JtVmFsaWQ/OiBib29sZWFuO1xyXG59XHJcblxyXG5leHBvcnQgY2xhc3MgRm9ybSBleHRlbmRzIENvbGxlY3Rpb24ge1xyXG4gIC8vIG9wY2lqYSB6YSBwcm9ncmVzcy1zcGlubmVyIGlsaSBwcm9ncmVzcy1iYXJcclxuICBwdWJsaWMgc2hvd1N1Ym1pdEJ1dHRvbjogYm9vbGVhbiA9IGZhbHNlO1xyXG4gIHB1YmxpYyBzdWJtaXRPbmx5SWZGb3JtVmFsaWQ6IGJvb2xlYW4gPSBmYWxzZTtcclxuICBwdWJsaWMgc2hvd0NhbmNlbEJ1dHRvbjogYm9vbGVhbiA9IGZhbHNlO1xyXG4gIHB1YmxpYyBzaG93UmVzZXRCdXR0b246IGJvb2xlYW4gPSBmYWxzZTtcclxuICBwdWJsaWMgc3VibWl0QnV0dG9uVGV4dDogc3RyaW5nID0gJ1N1Ym1pdCc7XHJcbiAgcHVibGljIGNhbmNlbEJ1dHRvblRleHQ6IHN0cmluZyA9ICdDYW5jZWwnO1xyXG4gIHB1YmxpYyByZXNldEJ1dHRvblRleHQ6IHN0cmluZyA9ICdSZXNldCc7XHJcbiAgcHJpdmF0ZSBfZm9ybUdyb3VwOiBGb3JtR3JvdXAgPSBuZXcgRm9ybUdyb3VwKHt9KTtcclxuXHJcbiAgcHVibGljIGNvbnRyb2xOYW1lczogc3RyaW5nW10gPSBbXTtcclxuXHJcbiAgY29uc3RydWN0b3IoZm9ybUludGVyZmFjZT86IEZvcm1JbnRlcmZhY2UpIHtcclxuICAgIHN1cGVyKCk7XHJcbiAgICBpZiAoZm9ybUludGVyZmFjZS5zaG93U3VibWl0QnV0dG9uKVxyXG4gICAgICB0aGlzLnNob3dTdWJtaXRCdXR0b24gPSBmb3JtSW50ZXJmYWNlLnNob3dTdWJtaXRCdXR0b247XHJcbiAgICBpZiAoZm9ybUludGVyZmFjZS5zdWJtaXRPbmx5SWZGb3JtVmFsaWQpXHJcbiAgICAgIHRoaXMuc3VibWl0T25seUlmRm9ybVZhbGlkID0gZm9ybUludGVyZmFjZS5zdWJtaXRPbmx5SWZGb3JtVmFsaWQ7XHJcbiAgICBpZiAoZm9ybUludGVyZmFjZS5zaG93Q2FuY2VsQnV0dG9uKVxyXG4gICAgICB0aGlzLnNob3dDYW5jZWxCdXR0b24gPSBmb3JtSW50ZXJmYWNlLnNob3dDYW5jZWxCdXR0b247XHJcbiAgICBpZiAoZm9ybUludGVyZmFjZS5zaG93UmVzZXRCdXR0b24pXHJcbiAgICAgIHRoaXMuc2hvd1Jlc2V0QnV0dG9uID0gZm9ybUludGVyZmFjZS5zaG93UmVzZXRCdXR0b247XHJcbiAgICBpZiAoZm9ybUludGVyZmFjZS5zdWJtaXRCdXR0b25UZXh0KVxyXG4gICAgICB0aGlzLnN1Ym1pdEJ1dHRvblRleHQgPSBmb3JtSW50ZXJmYWNlLnN1Ym1pdEJ1dHRvblRleHQ7XHJcbiAgICBpZiAoZm9ybUludGVyZmFjZS5jYW5jZWxCdXR0b25UZXh0KVxyXG4gICAgICB0aGlzLmNhbmNlbEJ1dHRvblRleHQgPSBmb3JtSW50ZXJmYWNlLmNhbmNlbEJ1dHRvblRleHQ7XHJcbiAgICBpZiAoZm9ybUludGVyZmFjZS5yZXNldEJ1dHRvblRleHQpXHJcbiAgICAgIHRoaXMucmVzZXRCdXR0b25UZXh0ID0gZm9ybUludGVyZmFjZS5yZXNldEJ1dHRvblRleHQ7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgZGlzYWJsZUFsbFJvd0l0ZW1zID0gKCk6IHZvaWQgPT4ge1xyXG4gICAgY29uc29sZS5sb2coJ2Rpc2FibGUnKTtcclxuICAgIHRoaXMuY2hpbGRyZW4uZm9yRWFjaCgocm93OiBGb3JtUm93KSA9PiB7XHJcbiAgICAgIHJvdy5kaXNhYmxlQWxsUm93SXRlbXMoKTtcclxuICAgIH0pO1xyXG4gIH07XHJcblxyXG4gIGFkZENoaWxkKGZvcm1Sb3c6IEZvcm1Sb3cpOiBib29sZWFuIHtcclxuICAgIGZvcm1Sb3cuZ2V0Q2hpbGRyZW4oKS5mb3JFYWNoKChyb3dJdGVtOiBGb3JtUm93SXRlbSkgPT4ge1xyXG4gICAgICBmb3JtUm93LmFkZENoaWxkKHJvd0l0ZW0pO1xyXG4gICAgfSk7XHJcblxyXG4gICAgdGhpcy5jaGlsZHJlbi5wdXNoKGZvcm1Sb3cpO1xyXG4gICAgZm9ybVJvdy5zZXRQYXJlbnQodGhpcyk7XHJcblxyXG4gICAgLy8gY29uc29sZS5sb2codGhpcy5fZm9ybUdyb3VwLnZhbHVlKTtcclxuXHJcbiAgICByZXR1cm4gdHJ1ZTtcclxuICB9XHJcblxyXG4gIHJlbW92ZUNoaWxkKGZvcm1Sb3c6IEZvcm1Sb3cpOiBib29sZWFuIHtcclxuICAgIGZvcm1Sb3cuZ2V0Q2hpbGRyZW4oKS5mb3JFYWNoKChyb3dJdGVtOiBGb3JtUm93SXRlbSkgPT4ge1xyXG4gICAgICBmb3JtUm93LnJlbW92ZUNoaWxkKHJvd0l0ZW0pO1xyXG4gICAgfSk7XHJcblxyXG4gICAgdGhpcy5jaGlsZHJlbiA9IHRoaXMuY2hpbGRyZW4uZmlsdGVyKChjaGlsZCkgPT4gY2hpbGQgIT09IGZvcm1Sb3cpO1xyXG4gICAgZm9ybVJvdy5zZXRQYXJlbnQodW5kZWZpbmVkKTtcclxuXHJcbiAgICAvLyBjb25zb2xlLmxvZyh0aGlzLl9mb3JtR3JvdXAudmFsdWUpO1xyXG5cclxuICAgIHJldHVybiB0cnVlO1xyXG4gIH1cclxuXHJcbiAgc2hvdWxkSGF2ZUNoaWxkcmVuKCk6IGJvb2xlYW4ge1xyXG4gICAgcmV0dXJuIHRydWU7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgZ2V0Rm9ybUdyb3VwID0gKCk6IEZvcm1Hcm91cCA9PiB7XHJcbiAgICByZXR1cm4gdGhpcy5fZm9ybUdyb3VwO1xyXG4gIH07XHJcblxyXG4gIHB1YmxpYyBzZXRGb3JtR3JvdXAgPSAoZm9ybUdyb3VwOiBGb3JtR3JvdXApOiB2b2lkID0+IHtcclxuICAgIHRoaXMuX2Zvcm1Hcm91cCA9IGZvcm1Hcm91cDtcclxuICB9O1xyXG59XHJcbiJdfQ==