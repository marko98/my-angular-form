import { FormRowItem } from '../form-row-item.model';
export class Timepicker extends FormRowItem {
    constructor() {
        super(...arguments);
        this.appearance = 'legacy';
        this.labelName = 'Pick time';
        this.buttonAlign = 'right';
        this.getAppearance = () => {
            return this.appearance;
        };
        this.setAppearance = (appearance) => {
            this.appearance = appearance;
        };
        this.getButtonAlign = () => {
            return this.buttonAlign;
        };
        this.setButtonAlign = (buttonAlign) => {
            this.buttonAlign = buttonAlign;
        };
        this.getMax = () => {
            return this.max;
        };
        this.setMax = (max) => {
            this.max = max;
        };
        this.getMin = () => {
            return this.min;
        };
        this.setMin = (min) => {
            this.min = min;
        };
        this.getLabelName = () => {
            return this.labelName;
        };
        this.setLabelName = (labelName) => {
            this.labelName = labelName;
        };
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGltZXBpY2tlci5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215LWFuZ3VsYXItZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvbW9kZWwvc3RydWN0dXJhbC9jb21wb3NpdGUvZm9ybS90aW1lcGlja2VyL3RpbWVwaWNrZXIubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFdBQVcsRUFBd0IsTUFBTSx3QkFBd0IsQ0FBQztBQVczRSxNQUFNLE9BQU8sVUFBVyxTQUFRLFdBQVc7SUFBM0M7O1FBQ1UsZUFBVSxHQUErQyxRQUFRLENBQUM7UUFHbEUsY0FBUyxHQUFXLFdBQVcsQ0FBQztRQUNoQyxnQkFBVyxHQUFxQixPQUFPLENBQUM7UUFFaEQsa0JBQWEsR0FBRyxHQUErQyxFQUFFO1lBQy9ELE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQztRQUN6QixDQUFDLENBQUM7UUFFRixrQkFBYSxHQUFHLENBQ2QsVUFBc0QsRUFDaEQsRUFBRTtZQUNSLElBQUksQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFDO1FBQy9CLENBQUMsQ0FBQztRQUVGLG1CQUFjLEdBQUcsR0FBcUIsRUFBRTtZQUN0QyxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUM7UUFDMUIsQ0FBQyxDQUFDO1FBRUYsbUJBQWMsR0FBRyxDQUFDLFdBQTZCLEVBQVEsRUFBRTtZQUN2RCxJQUFJLENBQUMsV0FBVyxHQUFHLFdBQVcsQ0FBQztRQUNqQyxDQUFDLENBQUM7UUFFRixXQUFNLEdBQUcsR0FBVyxFQUFFO1lBQ3BCLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQztRQUNsQixDQUFDLENBQUM7UUFFRixXQUFNLEdBQUcsQ0FBQyxHQUFXLEVBQVEsRUFBRTtZQUM3QixJQUFJLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQztRQUNqQixDQUFDLENBQUM7UUFFRixXQUFNLEdBQUcsR0FBVyxFQUFFO1lBQ3BCLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQztRQUNsQixDQUFDLENBQUM7UUFFRixXQUFNLEdBQUcsQ0FBQyxHQUFXLEVBQVEsRUFBRTtZQUM3QixJQUFJLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQztRQUNqQixDQUFDLENBQUM7UUFFRixpQkFBWSxHQUFHLEdBQVcsRUFBRTtZQUMxQixPQUFPLElBQUksQ0FBQyxTQUFTLENBQUM7UUFDeEIsQ0FBQyxDQUFDO1FBRUYsaUJBQVksR0FBRyxDQUFDLFNBQWlCLEVBQVEsRUFBRTtZQUN6QyxJQUFJLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQztRQUM3QixDQUFDLENBQUM7SUFDSixDQUFDO0NBQUEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBGb3JtUm93SXRlbSwgRm9ybVJvd0l0ZW1JbnRlcmZhY2UgfSBmcm9tICcuLi9mb3JtLXJvdy1pdGVtLm1vZGVsJztcclxuaW1wb3J0IHsgTUFUX0NPTE9SIH0gZnJvbSAncHJvamVjdHMvbXktYW5ndWxhci1mb3JtL3NyYy9saWIvc2hhcmVkL21hdGVyaWFsLm1vZHVsZSc7XHJcblxyXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgVGltZXBpY2tlckludGVyZmFjZSBleHRlbmRzIEZvcm1Sb3dJdGVtSW50ZXJmYWNlIHtcclxuICBtaW4/OiBzdHJpbmc7XHJcbiAgbWF4Pzogc3RyaW5nO1xyXG4gIGFwcGVhcmFuY2U/OiAnbGVnYWN5JyB8ICdzdGFuZGFyZCcgfCAnZmlsbCcgfCAnb3V0bGluZSc7XHJcbiAgbGFiZWxOYW1lPzogc3RyaW5nO1xyXG4gIGJ1dHRvbkFsaWduPzogJ2xlZnQnIHwgJ3JpZ2h0JztcclxufVxyXG5cclxuZXhwb3J0IGNsYXNzIFRpbWVwaWNrZXIgZXh0ZW5kcyBGb3JtUm93SXRlbSB7XHJcbiAgcHJpdmF0ZSBhcHBlYXJhbmNlOiAnbGVnYWN5JyB8ICdzdGFuZGFyZCcgfCAnZmlsbCcgfCAnb3V0bGluZScgPSAnbGVnYWN5JztcclxuICBwcml2YXRlIG1pbjogc3RyaW5nO1xyXG4gIHByaXZhdGUgbWF4OiBzdHJpbmc7XHJcbiAgcHJpdmF0ZSBsYWJlbE5hbWU6IHN0cmluZyA9ICdQaWNrIHRpbWUnO1xyXG4gIHByaXZhdGUgYnV0dG9uQWxpZ246ICdsZWZ0JyB8ICdyaWdodCcgPSAncmlnaHQnO1xyXG5cclxuICBnZXRBcHBlYXJhbmNlID0gKCk6ICdsZWdhY3knIHwgJ3N0YW5kYXJkJyB8ICdmaWxsJyB8ICdvdXRsaW5lJyA9PiB7XHJcbiAgICByZXR1cm4gdGhpcy5hcHBlYXJhbmNlO1xyXG4gIH07XHJcblxyXG4gIHNldEFwcGVhcmFuY2UgPSAoXHJcbiAgICBhcHBlYXJhbmNlOiAnbGVnYWN5JyB8ICdzdGFuZGFyZCcgfCAnZmlsbCcgfCAnb3V0bGluZSdcclxuICApOiB2b2lkID0+IHtcclxuICAgIHRoaXMuYXBwZWFyYW5jZSA9IGFwcGVhcmFuY2U7XHJcbiAgfTtcclxuXHJcbiAgZ2V0QnV0dG9uQWxpZ24gPSAoKTogJ2xlZnQnIHwgJ3JpZ2h0JyA9PiB7XHJcbiAgICByZXR1cm4gdGhpcy5idXR0b25BbGlnbjtcclxuICB9O1xyXG5cclxuICBzZXRCdXR0b25BbGlnbiA9IChidXR0b25BbGlnbjogJ2xlZnQnIHwgJ3JpZ2h0Jyk6IHZvaWQgPT4ge1xyXG4gICAgdGhpcy5idXR0b25BbGlnbiA9IGJ1dHRvbkFsaWduO1xyXG4gIH07XHJcblxyXG4gIGdldE1heCA9ICgpOiBzdHJpbmcgPT4ge1xyXG4gICAgcmV0dXJuIHRoaXMubWF4O1xyXG4gIH07XHJcblxyXG4gIHNldE1heCA9IChtYXg6IHN0cmluZyk6IHZvaWQgPT4ge1xyXG4gICAgdGhpcy5tYXggPSBtYXg7XHJcbiAgfTtcclxuXHJcbiAgZ2V0TWluID0gKCk6IHN0cmluZyA9PiB7XHJcbiAgICByZXR1cm4gdGhpcy5taW47XHJcbiAgfTtcclxuXHJcbiAgc2V0TWluID0gKG1pbjogc3RyaW5nKTogdm9pZCA9PiB7XHJcbiAgICB0aGlzLm1pbiA9IG1pbjtcclxuICB9O1xyXG5cclxuICBnZXRMYWJlbE5hbWUgPSAoKTogc3RyaW5nID0+IHtcclxuICAgIHJldHVybiB0aGlzLmxhYmVsTmFtZTtcclxuICB9O1xyXG5cclxuICBzZXRMYWJlbE5hbWUgPSAobGFiZWxOYW1lOiBzdHJpbmcpOiB2b2lkID0+IHtcclxuICAgIHRoaXMubGFiZWxOYW1lID0gbGFiZWxOYW1lO1xyXG4gIH07XHJcbn1cclxuIl19