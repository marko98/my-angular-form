// model
import { FormRowItem } from '../form-row-item.model';
export class RadioButton extends FormRowItem {
    constructor() {
        super(...arguments);
        this.getName = () => {
            return this.name;
        };
        this.getValue = () => {
            return this.value;
        };
        this.setName = (name) => {
            this.name = name;
        };
        this.setValue = (value) => {
            this.value = value;
        };
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmFkaW8tYnV0dG9uLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXktYW5ndWxhci1mb3JtLyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9tb2RlbC9zdHJ1Y3R1cmFsL2NvbXBvc2l0ZS9mb3JtL3JhZGlvLWJ1dHRvbi1ncm91cC9yYWRpby1idXR0b24ubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsUUFBUTtBQUNSLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQU9yRCxNQUFNLE9BQU8sV0FBWSxTQUFRLFdBQVc7SUFBNUM7O1FBSUksWUFBTyxHQUFHLEdBQVcsRUFBRTtZQUNuQixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUM7UUFDckIsQ0FBQyxDQUFBO1FBRUQsYUFBUSxHQUFHLEdBQVEsRUFBRTtZQUNqQixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDdEIsQ0FBQyxDQUFBO1FBRUQsWUFBTyxHQUFHLENBQUMsSUFBWSxFQUFRLEVBQUU7WUFDN0IsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7UUFDckIsQ0FBQyxDQUFBO1FBRUQsYUFBUSxHQUFHLENBQUMsS0FBVSxFQUFRLEVBQUU7WUFDNUIsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7UUFDdkIsQ0FBQyxDQUFBO0lBRUwsQ0FBQztDQUFBIiwic291cmNlc0NvbnRlbnQiOlsiLy8gbW9kZWxcclxuaW1wb3J0IHsgRm9ybVJvd0l0ZW0gfSBmcm9tICcuLi9mb3JtLXJvdy1pdGVtLm1vZGVsJztcclxuXHJcbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBSYWRpb0J1dHRvbkludGVyZmFjZSB7XHJcbiAgICBuYW1lOiBzdHJpbmcsXHJcbiAgICB2YWx1ZTogYW55XHJcbn1cclxuXHJcbmV4cG9ydCBjbGFzcyBSYWRpb0J1dHRvbiBleHRlbmRzIEZvcm1Sb3dJdGVtIHtcclxuICAgIHByaXZhdGUgbmFtZTogc3RyaW5nO1xyXG4gICAgcHJpdmF0ZSB2YWx1ZTogYW55O1xyXG5cclxuICAgIGdldE5hbWUgPSAoKTogc3RyaW5nID0+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5uYW1lO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFZhbHVlID0gKCk6IGFueSA9PiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMudmFsdWU7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0TmFtZSA9IChuYW1lOiBzdHJpbmcpOiB2b2lkID0+IHtcclxuICAgICAgICB0aGlzLm5hbWUgPSBuYW1lO1xyXG4gICAgfVxyXG5cclxuICAgIHNldFZhbHVlID0gKHZhbHVlOiBhbnkpOiB2b2lkID0+IHtcclxuICAgICAgICB0aGlzLnZhbHVlID0gdmFsdWU7XHJcbiAgICB9XHJcblxyXG59Il19