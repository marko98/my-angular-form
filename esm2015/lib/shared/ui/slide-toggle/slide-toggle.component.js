import { Component, forwardRef, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import * as i0 from "@angular/core";
import * as i1 from "@angular/flex-layout/flex";
import * as i2 from "@angular/material/slide-toggle";
export class SlideToggleComponent {
    constructor() {
        this.onChange = () => { };
        this.onTouched = () => { };
        this.onValueChange = (slideToggle) => {
            console.log(slideToggle);
            this.slideToggle.setDefaultValue(slideToggle.checked);
            this.onChange(this.slideToggle.getDefaultValue());
            this.onTouched();
        };
    }
    registerOnChange(fn) {
        this.onChange = fn;
    }
    registerOnTouched(fn) {
        this.onTouched = fn;
    }
    writeValue(value) {
        if (value) {
            this.slideToggle.setDefaultValue(value);
        }
    }
    ngOnInit() {
        // console.log('SlideToggleComponent init');
    }
    ngOnDestroy() {
        // console.log('SlideToggleComponent destroyed');
    }
}
SlideToggleComponent.ɵfac = function SlideToggleComponent_Factory(t) { return new (t || SlideToggleComponent)(); };
SlideToggleComponent.ɵcmp = i0.ɵɵdefineComponent({ type: SlideToggleComponent, selectors: [["app-slide-toggle"]], inputs: { slideToggle: "slideToggle" }, features: [i0.ɵɵProvidersFeature([
            {
                provide: NG_VALUE_ACCESSOR,
                multi: true,
                useExisting: forwardRef(() => SlideToggleComponent),
            },
        ])], decls: 4, vars: 4, consts: [["fxLayoutAlign", "center center"], [3, "color", "checked", "disabled", "change"], ["matSlideToggle", ""]], template: function SlideToggleComponent_Template(rf, ctx) { if (rf & 1) {
        const _r1 = i0.ɵɵgetCurrentView();
        i0.ɵɵelementStart(0, "section", 0);
        i0.ɵɵelementStart(1, "mat-slide-toggle", 1, 2);
        i0.ɵɵlistener("change", function SlideToggleComponent_Template_mat_slide_toggle_change_1_listener() { i0.ɵɵrestoreView(_r1); const _r0 = i0.ɵɵreference(2); return ctx.onValueChange(_r0); });
        i0.ɵɵtext(3);
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("color", ctx.slideToggle.getColor())("checked", ctx.slideToggle.getDefaultValue())("disabled", ctx.slideToggle.getDisabled());
        i0.ɵɵadvance(2);
        i0.ɵɵtextInterpolate(ctx.slideToggle.getContext());
    } }, directives: [i1.DefaultLayoutAlignDirective, i2.MatSlideToggle], styles: ["", ""] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(SlideToggleComponent, [{
        type: Component,
        args: [{
                selector: 'app-slide-toggle',
                templateUrl: './slide-toggle.component.html',
                styleUrls: ['./slide-toggle.component.css'],
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        multi: true,
                        useExisting: forwardRef(() => SlideToggleComponent),
                    },
                ],
            }]
    }], function () { return []; }, { slideToggle: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2xpZGUtdG9nZ2xlLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215LWFuZ3VsYXItZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvdWkvc2xpZGUtdG9nZ2xlL3NsaWRlLXRvZ2dsZS5jb21wb25lbnQudHMiLCJsaWIvc2hhcmVkL3VpL3NsaWRlLXRvZ2dsZS9zbGlkZS10b2dnbGUuY29tcG9uZW50Lmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxVQUFVLEVBQWEsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ2hGLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGdCQUFnQixDQUFDOzs7O0FBZ0JuRCxNQUFNLE9BQU8sb0JBQW9CO0lBTS9CO1FBSE8sYUFBUSxHQUFRLEdBQUcsRUFBRSxHQUFFLENBQUMsQ0FBQztRQUN6QixjQUFTLEdBQVEsR0FBRyxFQUFFLEdBQUUsQ0FBQyxDQUFDO1FBSWpDLGtCQUFhLEdBQUcsQ0FBQyxXQUEyQixFQUFRLEVBQUU7WUFDcEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUN6QixJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDdEQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsRUFBRSxDQUFDLENBQUM7WUFDbEQsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ25CLENBQUMsQ0FBQztJQVBhLENBQUM7SUFTaEIsZ0JBQWdCLENBQUMsRUFBRTtRQUNqQixJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztJQUNyQixDQUFDO0lBRUQsaUJBQWlCLENBQUMsRUFBRTtRQUNsQixJQUFJLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztJQUN0QixDQUFDO0lBRUQsVUFBVSxDQUFDLEtBQUs7UUFDZCxJQUFJLEtBQUssRUFBRTtZQUNULElBQUksQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3pDO0lBQ0gsQ0FBQztJQUVELFFBQVE7UUFDTiw0Q0FBNEM7SUFDOUMsQ0FBQztJQUVELFdBQVc7UUFDVCxpREFBaUQ7SUFDbkQsQ0FBQzs7d0ZBbkNVLG9CQUFvQjt5REFBcEIsb0JBQW9CLDhHQVJwQjtZQUNUO2dCQUNFLE9BQU8sRUFBRSxpQkFBaUI7Z0JBQzFCLEtBQUssRUFBRSxJQUFJO2dCQUNYLFdBQVcsRUFBRSxVQUFVLENBQUMsR0FBRyxFQUFFLENBQUMsb0JBQW9CLENBQUM7YUFDcEQ7U0FDRjs7UUNWSCxrQ0FHUTtRQUFBLDhDQUtvQjtRQURoQixtS0FBVSxzQkFBa0MsSUFBQztRQUM3QixZQUFpQztRQUFBLGlCQUFtQjtRQUVoRixpQkFBVTs7UUFORSxlQUFxQztRQUFyQyxrREFBcUMsOENBQUEsMkNBQUE7UUFJckIsZUFBaUM7UUFBakMsa0RBQWlDOztrRERJaEQsb0JBQW9CO2NBWmhDLFNBQVM7ZUFBQztnQkFDVCxRQUFRLEVBQUUsa0JBQWtCO2dCQUM1QixXQUFXLEVBQUUsK0JBQStCO2dCQUM1QyxTQUFTLEVBQUUsQ0FBQyw4QkFBOEIsQ0FBQztnQkFDM0MsU0FBUyxFQUFFO29CQUNUO3dCQUNFLE9BQU8sRUFBRSxpQkFBaUI7d0JBQzFCLEtBQUssRUFBRSxJQUFJO3dCQUNYLFdBQVcsRUFBRSxVQUFVLENBQUMsR0FBRyxFQUFFLENBQUMsb0JBQW9CLENBQUM7cUJBQ3BEO2lCQUNGO2FBQ0Y7O2tCQUVFLEtBQUsiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgZm9yd2FyZFJlZiwgT25EZXN0cm95LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgTkdfVkFMVUVfQUNDRVNTT1IgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQgeyBNYXRTbGlkZVRvZ2dsZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL3NsaWRlLXRvZ2dsZSc7XG5pbXBvcnQgeyBTbGlkZVRvZ2dsZSB9IGZyb20gJy4uLy4uL21vZGVsL3N0cnVjdHVyYWwvY29tcG9zaXRlL2Zvcm0vc2xpZGUtdG9nZ2xlL3NsaWRlLXRvZ2dsZS5tb2RlbCc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2FwcC1zbGlkZS10b2dnbGUnLFxuICB0ZW1wbGF0ZVVybDogJy4vc2xpZGUtdG9nZ2xlLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vc2xpZGUtdG9nZ2xlLmNvbXBvbmVudC5jc3MnXSxcbiAgcHJvdmlkZXJzOiBbXG4gICAge1xuICAgICAgcHJvdmlkZTogTkdfVkFMVUVfQUNDRVNTT1IsXG4gICAgICBtdWx0aTogdHJ1ZSxcbiAgICAgIHVzZUV4aXN0aW5nOiBmb3J3YXJkUmVmKCgpID0+IFNsaWRlVG9nZ2xlQ29tcG9uZW50KSxcbiAgICB9LFxuICBdLFxufSlcbmV4cG9ydCBjbGFzcyBTbGlkZVRvZ2dsZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcbiAgQElucHV0KCkgc2xpZGVUb2dnbGU6IFNsaWRlVG9nZ2xlO1xuXG4gIHB1YmxpYyBvbkNoYW5nZTogYW55ID0gKCkgPT4ge307XG4gIHB1YmxpYyBvblRvdWNoZWQ6IGFueSA9ICgpID0+IHt9O1xuXG4gIGNvbnN0cnVjdG9yKCkge31cblxuICBvblZhbHVlQ2hhbmdlID0gKHNsaWRlVG9nZ2xlOiBNYXRTbGlkZVRvZ2dsZSk6IHZvaWQgPT4ge1xuICAgIGNvbnNvbGUubG9nKHNsaWRlVG9nZ2xlKTtcbiAgICB0aGlzLnNsaWRlVG9nZ2xlLnNldERlZmF1bHRWYWx1ZShzbGlkZVRvZ2dsZS5jaGVja2VkKTtcbiAgICB0aGlzLm9uQ2hhbmdlKHRoaXMuc2xpZGVUb2dnbGUuZ2V0RGVmYXVsdFZhbHVlKCkpO1xuICAgIHRoaXMub25Ub3VjaGVkKCk7XG4gIH07XG5cbiAgcmVnaXN0ZXJPbkNoYW5nZShmbikge1xuICAgIHRoaXMub25DaGFuZ2UgPSBmbjtcbiAgfVxuXG4gIHJlZ2lzdGVyT25Ub3VjaGVkKGZuKSB7XG4gICAgdGhpcy5vblRvdWNoZWQgPSBmbjtcbiAgfVxuXG4gIHdyaXRlVmFsdWUodmFsdWUpIHtcbiAgICBpZiAodmFsdWUpIHtcbiAgICAgIHRoaXMuc2xpZGVUb2dnbGUuc2V0RGVmYXVsdFZhbHVlKHZhbHVlKTtcbiAgICB9XG4gIH1cblxuICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgICAvLyBjb25zb2xlLmxvZygnU2xpZGVUb2dnbGVDb21wb25lbnQgaW5pdCcpO1xuICB9XG5cbiAgbmdPbkRlc3Ryb3koKTogdm9pZCB7XG4gICAgLy8gY29uc29sZS5sb2coJ1NsaWRlVG9nZ2xlQ29tcG9uZW50IGRlc3Ryb3llZCcpO1xuICB9XG59XG4iLCI8c3R5bGU+XG5cblxuPC9zdHlsZT5cblxuPHNlY3Rpb25cbiAgICBmeExheW91dEFsaWduPVwiY2VudGVyIGNlbnRlclwiPlxuXG4gICAgICAgIDxtYXQtc2xpZGUtdG9nZ2xlXG4gICAgICAgICAgICBbY29sb3JdPVwidGhpcy5zbGlkZVRvZ2dsZS5nZXRDb2xvcigpXCJcbiAgICAgICAgICAgIFtjaGVja2VkXT1cInRoaXMuc2xpZGVUb2dnbGUuZ2V0RGVmYXVsdFZhbHVlKClcIlxuICAgICAgICAgICAgW2Rpc2FibGVkXT1cInRoaXMuc2xpZGVUb2dnbGUuZ2V0RGlzYWJsZWQoKVwiXG4gICAgICAgICAgICAoY2hhbmdlKT1cInRoaXMub25WYWx1ZUNoYW5nZShtYXRTbGlkZVRvZ2dsZSlcIlxuICAgICAgICAgICAgI21hdFNsaWRlVG9nZ2xlPnt7dGhpcy5zbGlkZVRvZ2dsZS5nZXRDb250ZXh0KCl9fTwvbWF0LXNsaWRlLXRvZ2dsZT5cblxuPC9zZWN0aW9uPiJdfQ==