import { Component, forwardRef, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import * as i0 from "@angular/core";
import * as i1 from "@angular/flex-layout/flex";
import * as i2 from "autocomplete";
export class AutocompleteComponent {
    constructor() {
        this.onChange = () => { };
        this.onTouched = () => { };
    }
    registerOnChange(fn) {
        this.onChange = fn;
    }
    registerOnTouched(fn) {
        this.onTouched = fn;
    }
    writeValue(value) {
        if (value) {
            this.autocomplete.setDefaultValue(value);
        }
    }
    ngOnInit() {
        // console.log(this.autocomplete);
        this.autocomplete.getValueSubject().subscribe((optionItem) => {
            this.onChange(optionItem);
            this.onTouched();
        });
        this.autocompleteInterface = {
            pageDataSource: this.autocomplete.getPageDataSource(),
            criteria: this.autocomplete.getCriteria(),
            inputPlaceHolder: this.autocomplete.getInputPlaceholder(),
            sortingOptions: this.autocomplete.getSortingOptions(),
            disabled: this.autocomplete.getDisabled(),
            appearance: this.autocomplete.getAppearance(),
            valueSubject: this.autocomplete.getValueSubject(),
            value: this.autocomplete.getValue(),
            hiddenSubject: this.autocomplete.getHiddenSubject(),
        };
        // console.log('AutocompleteComponent init');
    }
    ngOnDestroy() {
        // console.log('AutocompleteComponent destroyed');
    }
}
AutocompleteComponent.ɵfac = function AutocompleteComponent_Factory(t) { return new (t || AutocompleteComponent)(); };
AutocompleteComponent.ɵcmp = i0.ɵɵdefineComponent({ type: AutocompleteComponent, selectors: [["app-autocomplete"]], inputs: { autocomplete: "autocomplete" }, features: [i0.ɵɵProvidersFeature([
            {
                provide: NG_VALUE_ACCESSOR,
                multi: true,
                useExisting: forwardRef(() => AutocompleteComponent),
            },
        ])], decls: 2, vars: 1, consts: [["fxLayout", "column", "fxLayoutAlign", "center center", "fxLayoutGap", "2vw"], [3, "autocomplete"]], template: function AutocompleteComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "section", 0);
        i0.ɵɵelement(1, "lib-autocomplete", 1);
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("autocomplete", ctx.autocompleteInterface);
    } }, directives: [i1.DefaultLayoutDirective, i1.DefaultLayoutAlignDirective, i1.DefaultLayoutGapDirective, i2.AutocompleteComponent], styles: [""] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(AutocompleteComponent, [{
        type: Component,
        args: [{
                selector: 'app-autocomplete',
                templateUrl: './autocomplete.component.html',
                styleUrls: ['./autocomplete.component.css'],
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        multi: true,
                        useExisting: forwardRef(() => AutocompleteComponent),
                    },
                ],
            }]
    }], function () { return []; }, { autocomplete: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0b2NvbXBsZXRlLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215LWFuZ3VsYXItZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvdWkvYXV0b2NvbXBsZXRlL2F1dG9jb21wbGV0ZS5jb21wb25lbnQudHMiLCJsaWIvc2hhcmVkL3VpL2F1dG9jb21wbGV0ZS9hdXRvY29tcGxldGUuY29tcG9uZW50Lmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBcUIsVUFBVSxFQUFFLEtBQUssRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNoRixPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQzs7OztBQWdCbkQsTUFBTSxPQUFPLHFCQUFxQjtJQU9oQztRQUxPLGFBQVEsR0FBUSxHQUFHLEVBQUUsR0FBRSxDQUFDLENBQUM7UUFDekIsY0FBUyxHQUFRLEdBQUcsRUFBRSxHQUFFLENBQUMsQ0FBQztJQUlsQixDQUFDO0lBRWhCLGdCQUFnQixDQUFDLEVBQUU7UUFDakIsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7SUFDckIsQ0FBQztJQUVELGlCQUFpQixDQUFDLEVBQUU7UUFDbEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7SUFDdEIsQ0FBQztJQUVELFVBQVUsQ0FBQyxLQUFLO1FBQ2QsSUFBSSxLQUFLLEVBQUU7WUFDVCxJQUFJLENBQUMsWUFBWSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUMxQztJQUNILENBQUM7SUFFRCxRQUFRO1FBQ04sa0NBQWtDO1FBRWxDLElBQUksQ0FBQyxZQUFZLENBQUMsZUFBZSxFQUFFLENBQUMsU0FBUyxDQUFDLENBQUMsVUFBc0IsRUFBRSxFQUFFO1lBQ3ZFLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDMUIsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ25CLENBQUMsQ0FBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLHFCQUFxQixHQUFHO1lBQzNCLGNBQWMsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLGlCQUFpQixFQUFFO1lBQ3JELFFBQVEsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRTtZQUN6QyxnQkFBZ0IsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLG1CQUFtQixFQUFFO1lBQ3pELGNBQWMsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLGlCQUFpQixFQUFFO1lBQ3JELFFBQVEsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRTtZQUN6QyxVQUFVLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxhQUFhLEVBQUU7WUFDN0MsWUFBWSxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsZUFBZSxFQUFFO1lBQ2pELEtBQUssRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsRUFBRTtZQUNuQyxhQUFhLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsRUFBRTtTQUNwRCxDQUFDO1FBRUYsNkNBQTZDO0lBQy9DLENBQUM7SUFFRCxXQUFXO1FBQ1Qsa0RBQWtEO0lBQ3BELENBQUM7OzBGQWhEVSxxQkFBcUI7MERBQXJCLHFCQUFxQixnSEFSckI7WUFDVDtnQkFDRSxPQUFPLEVBQUUsaUJBQWlCO2dCQUMxQixLQUFLLEVBQUUsSUFBSTtnQkFDWCxXQUFXLEVBQUUsVUFBVSxDQUFDLEdBQUcsRUFBRSxDQUFDLHFCQUFxQixDQUFDO2FBQ3JEO1NBQ0Y7UUNmSCxrQ0FLUTtRQUFBLHNDQUNtRTtRQUUzRSxpQkFBVTs7UUFGRSxlQUEyQztRQUEzQyx3REFBMkM7O2tERFcxQyxxQkFBcUI7Y0FaakMsU0FBUztlQUFDO2dCQUNULFFBQVEsRUFBRSxrQkFBa0I7Z0JBQzVCLFdBQVcsRUFBRSwrQkFBK0I7Z0JBQzVDLFNBQVMsRUFBRSxDQUFDLDhCQUE4QixDQUFDO2dCQUMzQyxTQUFTLEVBQUU7b0JBQ1Q7d0JBQ0UsT0FBTyxFQUFFLGlCQUFpQjt3QkFDMUIsS0FBSyxFQUFFLElBQUk7d0JBQ1gsV0FBVyxFQUFFLFVBQVUsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxxQkFBcUIsQ0FBQztxQkFDckQ7aUJBQ0Y7YUFDRjs7a0JBRUUsS0FBSyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBPbkRlc3Ryb3ksIGZvcndhcmRSZWYsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBOR19WQUxVRV9BQ0NFU1NPUiB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7IEF1dG9jb21wbGV0ZSB9IGZyb20gJy4uLy4uL21vZGVsL3N0cnVjdHVyYWwvY29tcG9zaXRlL2Zvcm0vYXV0b2NvbXBsZXRlL2F1dG9jb21wbGV0ZS5tb2RlbCc7XG5pbXBvcnQgeyBBdXRvY29tcGxldGVJbnRlcmZhY2UsIE9wdGlvbkl0ZW0gfSBmcm9tICdhdXRvY29tcGxldGUnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdhcHAtYXV0b2NvbXBsZXRlJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2F1dG9jb21wbGV0ZS5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL2F1dG9jb21wbGV0ZS5jb21wb25lbnQuY3NzJ10sXG4gIHByb3ZpZGVyczogW1xuICAgIHtcbiAgICAgIHByb3ZpZGU6IE5HX1ZBTFVFX0FDQ0VTU09SLFxuICAgICAgbXVsdGk6IHRydWUsXG4gICAgICB1c2VFeGlzdGluZzogZm9yd2FyZFJlZigoKSA9PiBBdXRvY29tcGxldGVDb21wb25lbnQpLFxuICAgIH0sXG4gIF0sXG59KVxuZXhwb3J0IGNsYXNzIEF1dG9jb21wbGV0ZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcbiAgQElucHV0KCkgYXV0b2NvbXBsZXRlOiBBdXRvY29tcGxldGU7XG4gIHB1YmxpYyBvbkNoYW5nZTogYW55ID0gKCkgPT4ge307XG4gIHB1YmxpYyBvblRvdWNoZWQ6IGFueSA9ICgpID0+IHt9O1xuXG4gIHB1YmxpYyBhdXRvY29tcGxldGVJbnRlcmZhY2U6IEF1dG9jb21wbGV0ZUludGVyZmFjZTtcblxuICBjb25zdHJ1Y3RvcigpIHt9XG5cbiAgcmVnaXN0ZXJPbkNoYW5nZShmbikge1xuICAgIHRoaXMub25DaGFuZ2UgPSBmbjtcbiAgfVxuXG4gIHJlZ2lzdGVyT25Ub3VjaGVkKGZuKSB7XG4gICAgdGhpcy5vblRvdWNoZWQgPSBmbjtcbiAgfVxuXG4gIHdyaXRlVmFsdWUodmFsdWUpIHtcbiAgICBpZiAodmFsdWUpIHtcbiAgICAgIHRoaXMuYXV0b2NvbXBsZXRlLnNldERlZmF1bHRWYWx1ZSh2YWx1ZSk7XG4gICAgfVxuICB9XG5cbiAgbmdPbkluaXQoKTogdm9pZCB7XG4gICAgLy8gY29uc29sZS5sb2codGhpcy5hdXRvY29tcGxldGUpO1xuXG4gICAgdGhpcy5hdXRvY29tcGxldGUuZ2V0VmFsdWVTdWJqZWN0KCkuc3Vic2NyaWJlKChvcHRpb25JdGVtOiBPcHRpb25JdGVtKSA9PiB7XG4gICAgICB0aGlzLm9uQ2hhbmdlKG9wdGlvbkl0ZW0pO1xuICAgICAgdGhpcy5vblRvdWNoZWQoKTtcbiAgICB9KTtcblxuICAgIHRoaXMuYXV0b2NvbXBsZXRlSW50ZXJmYWNlID0ge1xuICAgICAgcGFnZURhdGFTb3VyY2U6IHRoaXMuYXV0b2NvbXBsZXRlLmdldFBhZ2VEYXRhU291cmNlKCksXG4gICAgICBjcml0ZXJpYTogdGhpcy5hdXRvY29tcGxldGUuZ2V0Q3JpdGVyaWEoKSxcbiAgICAgIGlucHV0UGxhY2VIb2xkZXI6IHRoaXMuYXV0b2NvbXBsZXRlLmdldElucHV0UGxhY2Vob2xkZXIoKSxcbiAgICAgIHNvcnRpbmdPcHRpb25zOiB0aGlzLmF1dG9jb21wbGV0ZS5nZXRTb3J0aW5nT3B0aW9ucygpLFxuICAgICAgZGlzYWJsZWQ6IHRoaXMuYXV0b2NvbXBsZXRlLmdldERpc2FibGVkKCksXG4gICAgICBhcHBlYXJhbmNlOiB0aGlzLmF1dG9jb21wbGV0ZS5nZXRBcHBlYXJhbmNlKCksXG4gICAgICB2YWx1ZVN1YmplY3Q6IHRoaXMuYXV0b2NvbXBsZXRlLmdldFZhbHVlU3ViamVjdCgpLFxuICAgICAgdmFsdWU6IHRoaXMuYXV0b2NvbXBsZXRlLmdldFZhbHVlKCksXG4gICAgICBoaWRkZW5TdWJqZWN0OiB0aGlzLmF1dG9jb21wbGV0ZS5nZXRIaWRkZW5TdWJqZWN0KCksXG4gICAgfTtcblxuICAgIC8vIGNvbnNvbGUubG9nKCdBdXRvY29tcGxldGVDb21wb25lbnQgaW5pdCcpO1xuICB9XG5cbiAgbmdPbkRlc3Ryb3koKTogdm9pZCB7XG4gICAgLy8gY29uc29sZS5sb2coJ0F1dG9jb21wbGV0ZUNvbXBvbmVudCBkZXN0cm95ZWQnKTtcbiAgfVxufVxuIiwiPHNlY3Rpb25cbiAgICBmeExheW91dD1cImNvbHVtblwiXG4gICAgZnhMYXlvdXRBbGlnbj1cImNlbnRlciBjZW50ZXJcIlxuICAgIGZ4TGF5b3V0R2FwPVwiMnZ3XCI+XG5cbiAgICAgICAgPGxpYi1hdXRvY29tcGxldGVcbiAgICAgICAgICAgIFthdXRvY29tcGxldGVdPVwidGhpcy5hdXRvY29tcGxldGVJbnRlcmZhY2VcIj48L2xpYi1hdXRvY29tcGxldGU+XG5cbjwvc2VjdGlvbj5cbiJdfQ==