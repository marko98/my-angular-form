import { Component, Input, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import * as i0 from "@angular/core";
import * as i1 from "@angular/flex-layout/flex";
import * as i2 from "@angular/common";
import * as i3 from "@angular/material/button";
import * as i4 from "@angular/material/icon";
function ButtonComponent_button_1_Template(rf, ctx) { if (rf & 1) {
    const _r8 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 8);
    i0.ɵɵlistener("click", function ButtonComponent_button_1_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r8); const ctx_r7 = i0.ɵɵnextContext(); return ctx_r7.button.getFunctionToExecute() ? ctx_r7.button.getFunctionToExecute()() : undefined; });
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r0 = i0.ɵɵnextContext();
    i0.ɵɵproperty("type", ctx_r0.button.getButtonType())("disabled", ctx_r0.button.getDisabled())("color", ctx_r0.button.getColor());
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r0.button.getContext());
} }
function ButtonComponent_button_2_Template(rf, ctx) { if (rf & 1) {
    const _r10 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 9);
    i0.ɵɵlistener("click", function ButtonComponent_button_2_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r10); const ctx_r9 = i0.ɵɵnextContext(); return ctx_r9.button.getFunctionToExecute() ? ctx_r9.button.getFunctionToExecute()() : undefined; });
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r1 = i0.ɵɵnextContext();
    i0.ɵɵproperty("type", ctx_r1.button.getButtonType())("disabled", ctx_r1.button.getDisabled())("color", ctx_r1.button.getColor());
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r1.button.getContext());
} }
function ButtonComponent_button_3_Template(rf, ctx) { if (rf & 1) {
    const _r12 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 10);
    i0.ɵɵlistener("click", function ButtonComponent_button_3_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r12); const ctx_r11 = i0.ɵɵnextContext(); return ctx_r11.button.getFunctionToExecute() ? ctx_r11.button.getFunctionToExecute()() : undefined; });
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r2 = i0.ɵɵnextContext();
    i0.ɵɵproperty("type", ctx_r2.button.getButtonType())("disabled", ctx_r2.button.getDisabled())("color", ctx_r2.button.getColor());
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r2.button.getContext());
} }
function ButtonComponent_button_4_Template(rf, ctx) { if (rf & 1) {
    const _r14 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 11);
    i0.ɵɵlistener("click", function ButtonComponent_button_4_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r14); const ctx_r13 = i0.ɵɵnextContext(); return ctx_r13.button.getFunctionToExecute() ? ctx_r13.button.getFunctionToExecute()() : undefined; });
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r3 = i0.ɵɵnextContext();
    i0.ɵɵproperty("type", ctx_r3.button.getButtonType())("disabled", ctx_r3.button.getDisabled())("color", ctx_r3.button.getColor());
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r3.button.getContext());
} }
function ButtonComponent_button_5_Template(rf, ctx) { if (rf & 1) {
    const _r16 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 12);
    i0.ɵɵlistener("click", function ButtonComponent_button_5_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r16); const ctx_r15 = i0.ɵɵnextContext(); return ctx_r15.button.getFunctionToExecute() ? ctx_r15.button.getFunctionToExecute()() : undefined; });
    i0.ɵɵelementStart(1, "mat-icon");
    i0.ɵɵtext(2);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r4 = i0.ɵɵnextContext();
    i0.ɵɵproperty("type", ctx_r4.button.getButtonType())("disabled", ctx_r4.button.getDisabled())("color", ctx_r4.button.getColor());
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(ctx_r4.button.getMatIconString());
} }
function ButtonComponent_button_6_Template(rf, ctx) { if (rf & 1) {
    const _r18 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 13);
    i0.ɵɵlistener("click", function ButtonComponent_button_6_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r18); const ctx_r17 = i0.ɵɵnextContext(); return ctx_r17.button.getFunctionToExecute() ? ctx_r17.button.getFunctionToExecute()() : undefined; });
    i0.ɵɵelementStart(1, "mat-icon");
    i0.ɵɵtext(2);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r5 = i0.ɵɵnextContext();
    i0.ɵɵproperty("type", ctx_r5.button.getButtonType())("disabled", ctx_r5.button.getDisabled())("color", ctx_r5.button.getColor());
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(ctx_r5.button.getMatIconString());
} }
function ButtonComponent_button_7_Template(rf, ctx) { if (rf & 1) {
    const _r20 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 14);
    i0.ɵɵlistener("click", function ButtonComponent_button_7_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r20); const ctx_r19 = i0.ɵɵnextContext(); return ctx_r19.button.getFunctionToExecute() ? ctx_r19.button.getFunctionToExecute()() : undefined; });
    i0.ɵɵelementStart(1, "mat-icon");
    i0.ɵɵtext(2);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r6 = i0.ɵɵnextContext();
    i0.ɵɵproperty("type", ctx_r6.button.getButtonType())("disabled", ctx_r6.button.getDisabled())("color", ctx_r6.button.getColor());
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(ctx_r6.button.getMatIconString());
} }
export class ButtonComponent {
    constructor() {
        this.onChange = () => { };
        this.onTouched = () => { };
    }
    registerOnChange(fn) {
        this.onChange = fn;
    }
    registerOnTouched(fn) {
        this.onTouched = fn;
    }
    writeValue(value) {
        if (value) {
            this.button.setDefaultValue(value);
        }
    }
    ngOnInit() {
        // console.log('ButtonComponent init');
    }
    ngOnDestroy() {
        // console.log('ButtonComponent destroyed');
    }
}
ButtonComponent.ɵfac = function ButtonComponent_Factory(t) { return new (t || ButtonComponent)(); };
ButtonComponent.ɵcmp = i0.ɵɵdefineComponent({ type: ButtonComponent, selectors: [["app-button"]], inputs: { button: "button" }, features: [i0.ɵɵProvidersFeature([
            {
                provide: NG_VALUE_ACCESSOR,
                multi: true,
                useExisting: forwardRef(() => ButtonComponent),
            },
        ])], decls: 8, vars: 7, consts: [["fxLayoutAlign", "center center"], ["mat-button", "", 3, "type", "disabled", "color", "click", 4, "ngIf"], ["mat-raised-button", "", 3, "type", "disabled", "color", "click", 4, "ngIf"], ["mat-stroked-button", "", 3, "type", "disabled", "color", "click", 4, "ngIf"], ["mat-flat-button", "", 3, "type", "disabled", "color", "click", 4, "ngIf"], ["mat-icon-button", "", 3, "type", "disabled", "color", "click", 4, "ngIf"], ["mat-fab", "", 3, "type", "disabled", "color", "click", 4, "ngIf"], ["mat-mini-fab", "", 3, "type", "disabled", "color", "click", 4, "ngIf"], ["mat-button", "", 3, "type", "disabled", "color", "click"], ["mat-raised-button", "", 3, "type", "disabled", "color", "click"], ["mat-stroked-button", "", 3, "type", "disabled", "color", "click"], ["mat-flat-button", "", 3, "type", "disabled", "color", "click"], ["mat-icon-button", "", 3, "type", "disabled", "color", "click"], ["mat-fab", "", 3, "type", "disabled", "color", "click"], ["mat-mini-fab", "", 3, "type", "disabled", "color", "click"]], template: function ButtonComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "section", 0);
        i0.ɵɵtemplate(1, ButtonComponent_button_1_Template, 2, 4, "button", 1);
        i0.ɵɵtemplate(2, ButtonComponent_button_2_Template, 2, 4, "button", 2);
        i0.ɵɵtemplate(3, ButtonComponent_button_3_Template, 2, 4, "button", 3);
        i0.ɵɵtemplate(4, ButtonComponent_button_4_Template, 2, 4, "button", 4);
        i0.ɵɵtemplate(5, ButtonComponent_button_5_Template, 3, 4, "button", 5);
        i0.ɵɵtemplate(6, ButtonComponent_button_6_Template, 3, 4, "button", 6);
        i0.ɵɵtemplate(7, ButtonComponent_button_7_Template, 3, 4, "button", 7);
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.button.getMatButtonType() == "BASIC");
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.button.getMatButtonType() == "RAISED");
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.button.getMatButtonType() == "STROKED");
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.button.getMatButtonType() == "FLAT");
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.button.getMatButtonType() == "ICON");
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.button.getMatButtonType() == "FAB");
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.button.getMatButtonType() == "MINI_FAB");
    } }, directives: [i1.DefaultLayoutAlignDirective, i2.NgIf, i3.MatButton, i4.MatIcon], styles: ["", ""] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(ButtonComponent, [{
        type: Component,
        args: [{
                selector: 'app-button',
                templateUrl: './button.component.html',
                styleUrls: ['./button.component.css'],
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        multi: true,
                        useExisting: forwardRef(() => ButtonComponent),
                    },
                ],
            }]
    }], function () { return []; }, { button: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnV0dG9uLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215LWFuZ3VsYXItZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvdWkvYnV0dG9uL2J1dHRvbi5jb21wb25lbnQudHMiLCJsaWIvc2hhcmVkL3VpL2J1dHRvbi9idXR0b24uY29tcG9uZW50Lmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxLQUFLLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRXJFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGdCQUFnQixDQUFDOzs7Ozs7OztJQ0szQyxpQ0FNcUM7SUFGakMsK0pBQVMsb0NBQWtDLEdBQUcsb0NBQWtDLEVBQUUsR0FBRyxTQUFTLElBQUM7SUFFOUQsWUFBOEI7SUFBQSxpQkFBUzs7O0lBSHhFLG9EQUFvQyx5Q0FBQSxtQ0FBQTtJQUdILGVBQThCO0lBQTlCLGdEQUE4Qjs7OztJQUVuRSxpQ0FNcUM7SUFGakMsZ0tBQVMsb0NBQWtDLEdBQUcsb0NBQWtDLEVBQUUsR0FBRyxTQUFTLElBQUM7SUFFOUQsWUFBOEI7SUFBQSxpQkFBUzs7O0lBSHhFLG9EQUFvQyx5Q0FBQSxtQ0FBQTtJQUdILGVBQThCO0lBQTlCLGdEQUE4Qjs7OztJQUVuRSxrQ0FNcUM7SUFGakMsaUtBQVMscUNBQWtDLEdBQUcscUNBQWtDLEVBQUUsR0FBRyxTQUFTLElBQUM7SUFFOUQsWUFBOEI7SUFBQSxpQkFBUzs7O0lBSHhFLG9EQUFvQyx5Q0FBQSxtQ0FBQTtJQUdILGVBQThCO0lBQTlCLGdEQUE4Qjs7OztJQUVuRSxrQ0FNcUM7SUFGakMsaUtBQVMscUNBQWtDLEdBQUcscUNBQWtDLEVBQUUsR0FBRyxTQUFTLElBQUM7SUFFOUQsWUFBOEI7SUFBQSxpQkFBUzs7O0lBSHhFLG9EQUFvQyx5Q0FBQSxtQ0FBQTtJQUdILGVBQThCO0lBQTlCLGdEQUE4Qjs7OztJQUVuRSxrQ0FPUTtJQUhKLGlLQUFTLHFDQUFrQyxHQUFHLHFDQUFrQyxFQUFFLEdBQUcsU0FBUyxJQUFDO0lBRzNGLGdDQUFVO0lBQUEsWUFBa0M7SUFBQSxpQkFBVztJQUMvRCxpQkFBUzs7O0lBTEwsb0RBQW9DLHlDQUFBLG1DQUFBO0lBSXRCLGVBQWtDO0lBQWxDLHNEQUFrQzs7OztJQUdwRCxrQ0FPSTtJQUhBLGlLQUFTLHFDQUFrQyxHQUFHLHFDQUFrQyxFQUFFLEdBQUcsU0FBUyxJQUFDO0lBRy9GLGdDQUFVO0lBQUEsWUFBa0M7SUFBQSxpQkFBVztJQUMzRCxpQkFBUzs7O0lBTEwsb0RBQW9DLHlDQUFBLG1DQUFBO0lBSTFCLGVBQWtDO0lBQWxDLHNEQUFrQzs7OztJQUdoRCxrQ0FPSTtJQUhBLGlLQUFTLHFDQUFrQyxHQUFHLHFDQUFrQyxFQUFFLEdBQUcsU0FBUyxJQUFDO0lBRy9GLGdDQUFVO0lBQUEsWUFBa0M7SUFBQSxpQkFBVztJQUMzRCxpQkFBUzs7O0lBTEwsb0RBQW9DLHlDQUFBLG1DQUFBO0lBSTFCLGVBQWtDO0lBQWxDLHNEQUFrQzs7QURsRHhELE1BQU0sT0FBTyxlQUFlO0lBTTFCO1FBSE8sYUFBUSxHQUFRLEdBQUcsRUFBRSxHQUFFLENBQUMsQ0FBQztRQUN6QixjQUFTLEdBQVEsR0FBRyxFQUFFLEdBQUUsQ0FBQyxDQUFDO0lBRWxCLENBQUM7SUFFaEIsZ0JBQWdCLENBQUMsRUFBRTtRQUNqQixJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztJQUNyQixDQUFDO0lBRUQsaUJBQWlCLENBQUMsRUFBRTtRQUNsQixJQUFJLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztJQUN0QixDQUFDO0lBRUQsVUFBVSxDQUFDLEtBQUs7UUFDZCxJQUFJLEtBQUssRUFBRTtZQUNULElBQUksQ0FBQyxNQUFNLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3BDO0lBQ0gsQ0FBQztJQUVELFFBQVE7UUFDTix1Q0FBdUM7SUFDekMsQ0FBQztJQUVELFdBQVc7UUFDVCw0Q0FBNEM7SUFDOUMsQ0FBQzs7OEVBNUJVLGVBQWU7b0RBQWYsZUFBZSw4RkFSZjtZQUNUO2dCQUNFLE9BQU8sRUFBRSxpQkFBaUI7Z0JBQzFCLEtBQUssRUFBRSxJQUFJO2dCQUNYLFdBQVcsRUFBRSxVQUFVLENBQUMsR0FBRyxFQUFFLENBQUMsZUFBZSxDQUFDO2FBQy9DO1NBQ0Y7UUNWSCxrQ0FHUTtRQUFBLHNFQU1xQztRQUVyQyxzRUFNcUM7UUFFckMsc0VBTXFDO1FBRXJDLHNFQU1xQztRQUVyQyxzRUFPUTtRQUdSLHNFQU9JO1FBR0osc0VBT0k7UUFFWixpQkFBVTs7UUE1REUsZUFBaUQ7UUFBakQsK0RBQWlEO1FBUWpELGVBQWtEO1FBQWxELGdFQUFrRDtRQVFsRCxlQUFtRDtRQUFuRCxpRUFBbUQ7UUFRbkQsZUFBZ0Q7UUFBaEQsOERBQWdEO1FBUWhELGVBQWdEO1FBQWhELDhEQUFnRDtRQVVoRCxlQUErQztRQUEvQyw2REFBK0M7UUFVL0MsZUFBb0Q7UUFBcEQsa0VBQW9EOztrREQ1Q25ELGVBQWU7Y0FaM0IsU0FBUztlQUFDO2dCQUNULFFBQVEsRUFBRSxZQUFZO2dCQUN0QixXQUFXLEVBQUUseUJBQXlCO2dCQUN0QyxTQUFTLEVBQUUsQ0FBQyx3QkFBd0IsQ0FBQztnQkFDckMsU0FBUyxFQUFFO29CQUNUO3dCQUNFLE9BQU8sRUFBRSxpQkFBaUI7d0JBQzFCLEtBQUssRUFBRSxJQUFJO3dCQUNYLFdBQVcsRUFBRSxVQUFVLENBQUMsR0FBRyxFQUFFLENBQUMsZUFBZSxDQUFDO3FCQUMvQztpQkFDRjthQUNGOztrQkFFRSxLQUFLIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0LCBmb3J3YXJkUmVmIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBCdXR0b24gfSBmcm9tICcuLi8uLi9tb2RlbC9zdHJ1Y3R1cmFsL2NvbXBvc2l0ZS9mb3JtL2J1dHRvbi9idXR0b24ubW9kZWwnO1xuaW1wb3J0IHsgTkdfVkFMVUVfQUNDRVNTT1IgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2FwcC1idXR0b24nLFxuICB0ZW1wbGF0ZVVybDogJy4vYnV0dG9uLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vYnV0dG9uLmNvbXBvbmVudC5jc3MnXSxcbiAgcHJvdmlkZXJzOiBbXG4gICAge1xuICAgICAgcHJvdmlkZTogTkdfVkFMVUVfQUNDRVNTT1IsXG4gICAgICBtdWx0aTogdHJ1ZSxcbiAgICAgIHVzZUV4aXN0aW5nOiBmb3J3YXJkUmVmKCgpID0+IEJ1dHRvbkNvbXBvbmVudCksXG4gICAgfSxcbiAgXSxcbn0pXG5leHBvcnQgY2xhc3MgQnV0dG9uQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgQElucHV0KCkgYnV0dG9uOiBCdXR0b247XG5cbiAgcHVibGljIG9uQ2hhbmdlOiBhbnkgPSAoKSA9PiB7fTtcbiAgcHVibGljIG9uVG91Y2hlZDogYW55ID0gKCkgPT4ge307XG5cbiAgY29uc3RydWN0b3IoKSB7fVxuXG4gIHJlZ2lzdGVyT25DaGFuZ2UoZm4pIHtcbiAgICB0aGlzLm9uQ2hhbmdlID0gZm47XG4gIH1cblxuICByZWdpc3Rlck9uVG91Y2hlZChmbikge1xuICAgIHRoaXMub25Ub3VjaGVkID0gZm47XG4gIH1cblxuICB3cml0ZVZhbHVlKHZhbHVlKSB7XG4gICAgaWYgKHZhbHVlKSB7XG4gICAgICB0aGlzLmJ1dHRvbi5zZXREZWZhdWx0VmFsdWUodmFsdWUpO1xuICAgIH1cbiAgfVxuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICAgIC8vIGNvbnNvbGUubG9nKCdCdXR0b25Db21wb25lbnQgaW5pdCcpO1xuICB9XG5cbiAgbmdPbkRlc3Ryb3koKTogdm9pZCB7XG4gICAgLy8gY29uc29sZS5sb2coJ0J1dHRvbkNvbXBvbmVudCBkZXN0cm95ZWQnKTtcbiAgfVxufVxuIiwiPHN0eWxlPlxuXG48L3N0eWxlPlxuXG48c2VjdGlvblxuICAgIGZ4TGF5b3V0QWxpZ249XCJjZW50ZXIgY2VudGVyXCI+XG5cbiAgICAgICAgPGJ1dHRvbiBcbiAgICAgICAgICAgICpuZ0lmPVwidGhpcy5idXR0b24uZ2V0TWF0QnV0dG9uVHlwZSgpID09ICdCQVNJQydcIlxuICAgICAgICAgICAgbWF0LWJ1dHRvblxuICAgICAgICAgICAgW3R5cGVdPVwidGhpcy5idXR0b24uZ2V0QnV0dG9uVHlwZSgpXCJcbiAgICAgICAgICAgIChjbGljayk9XCJ0aGlzLmJ1dHRvbi5nZXRGdW5jdGlvblRvRXhlY3V0ZSgpID8gdGhpcy5idXR0b24uZ2V0RnVuY3Rpb25Ub0V4ZWN1dGUoKSgpIDogdW5kZWZpbmVkXCJcbiAgICAgICAgICAgIFtkaXNhYmxlZF09XCJ0aGlzLmJ1dHRvbi5nZXREaXNhYmxlZCgpXCJcbiAgICAgICAgICAgIFtjb2xvcl09XCJ0aGlzLmJ1dHRvbi5nZXRDb2xvcigpXCI+e3sgdGhpcy5idXR0b24uZ2V0Q29udGV4dCgpIH19PC9idXR0b24+XG5cbiAgICAgICAgPGJ1dHRvbiBcbiAgICAgICAgICAgICpuZ0lmPVwidGhpcy5idXR0b24uZ2V0TWF0QnV0dG9uVHlwZSgpID09ICdSQUlTRUQnXCJcbiAgICAgICAgICAgIG1hdC1yYWlzZWQtYnV0dG9uXG4gICAgICAgICAgICBbdHlwZV09XCJ0aGlzLmJ1dHRvbi5nZXRCdXR0b25UeXBlKClcIlxuICAgICAgICAgICAgKGNsaWNrKT1cInRoaXMuYnV0dG9uLmdldEZ1bmN0aW9uVG9FeGVjdXRlKCkgPyB0aGlzLmJ1dHRvbi5nZXRGdW5jdGlvblRvRXhlY3V0ZSgpKCkgOiB1bmRlZmluZWRcIlxuICAgICAgICAgICAgW2Rpc2FibGVkXT1cInRoaXMuYnV0dG9uLmdldERpc2FibGVkKClcIlxuICAgICAgICAgICAgW2NvbG9yXT1cInRoaXMuYnV0dG9uLmdldENvbG9yKClcIj57eyB0aGlzLmJ1dHRvbi5nZXRDb250ZXh0KCkgfX08L2J1dHRvbj5cblxuICAgICAgICA8YnV0dG9uIFxuICAgICAgICAgICAgKm5nSWY9XCJ0aGlzLmJ1dHRvbi5nZXRNYXRCdXR0b25UeXBlKCkgPT0gJ1NUUk9LRUQnXCJcbiAgICAgICAgICAgIG1hdC1zdHJva2VkLWJ1dHRvblxuICAgICAgICAgICAgW3R5cGVdPVwidGhpcy5idXR0b24uZ2V0QnV0dG9uVHlwZSgpXCJcbiAgICAgICAgICAgIChjbGljayk9XCJ0aGlzLmJ1dHRvbi5nZXRGdW5jdGlvblRvRXhlY3V0ZSgpID8gdGhpcy5idXR0b24uZ2V0RnVuY3Rpb25Ub0V4ZWN1dGUoKSgpIDogdW5kZWZpbmVkXCJcbiAgICAgICAgICAgIFtkaXNhYmxlZF09XCJ0aGlzLmJ1dHRvbi5nZXREaXNhYmxlZCgpXCJcbiAgICAgICAgICAgIFtjb2xvcl09XCJ0aGlzLmJ1dHRvbi5nZXRDb2xvcigpXCI+e3sgdGhpcy5idXR0b24uZ2V0Q29udGV4dCgpIH19PC9idXR0b24+XG5cbiAgICAgICAgPGJ1dHRvbiBcbiAgICAgICAgICAgICpuZ0lmPVwidGhpcy5idXR0b24uZ2V0TWF0QnV0dG9uVHlwZSgpID09ICdGTEFUJ1wiXG4gICAgICAgICAgICBtYXQtZmxhdC1idXR0b25cbiAgICAgICAgICAgIFt0eXBlXT1cInRoaXMuYnV0dG9uLmdldEJ1dHRvblR5cGUoKVwiXG4gICAgICAgICAgICAoY2xpY2spPVwidGhpcy5idXR0b24uZ2V0RnVuY3Rpb25Ub0V4ZWN1dGUoKSA/IHRoaXMuYnV0dG9uLmdldEZ1bmN0aW9uVG9FeGVjdXRlKCkoKSA6IHVuZGVmaW5lZFwiXG4gICAgICAgICAgICBbZGlzYWJsZWRdPVwidGhpcy5idXR0b24uZ2V0RGlzYWJsZWQoKVwiXG4gICAgICAgICAgICBbY29sb3JdPVwidGhpcy5idXR0b24uZ2V0Q29sb3IoKVwiPnt7IHRoaXMuYnV0dG9uLmdldENvbnRleHQoKSB9fTwvYnV0dG9uPlxuXG4gICAgICAgIDxidXR0b24gXG4gICAgICAgICAgICAqbmdJZj1cInRoaXMuYnV0dG9uLmdldE1hdEJ1dHRvblR5cGUoKSA9PSAnSUNPTidcIlxuICAgICAgICAgICAgbWF0LWljb24tYnV0dG9uXG4gICAgICAgICAgICBbdHlwZV09XCJ0aGlzLmJ1dHRvbi5nZXRCdXR0b25UeXBlKClcIlxuICAgICAgICAgICAgKGNsaWNrKT1cInRoaXMuYnV0dG9uLmdldEZ1bmN0aW9uVG9FeGVjdXRlKCkgPyB0aGlzLmJ1dHRvbi5nZXRGdW5jdGlvblRvRXhlY3V0ZSgpKCkgOiB1bmRlZmluZWRcIlxuICAgICAgICAgICAgW2Rpc2FibGVkXT1cInRoaXMuYnV0dG9uLmdldERpc2FibGVkKClcIlxuICAgICAgICAgICAgW2NvbG9yXT1cInRoaXMuYnV0dG9uLmdldENvbG9yKClcIj5cbiAgICAgICAgICAgICAgICA8bWF0LWljb24+e3t0aGlzLmJ1dHRvbi5nZXRNYXRJY29uU3RyaW5nKCl9fTwvbWF0LWljb24+XG4gICAgICAgIDwvYnV0dG9uPlxuXG4gICAgICAgIDxidXR0b24gXG4gICAgICAgICAgICAqbmdJZj1cInRoaXMuYnV0dG9uLmdldE1hdEJ1dHRvblR5cGUoKSA9PSAnRkFCJ1wiXG4gICAgICAgICAgICBtYXQtZmFiXG4gICAgICAgICAgICBbdHlwZV09XCJ0aGlzLmJ1dHRvbi5nZXRCdXR0b25UeXBlKClcIlxuICAgICAgICAgICAgKGNsaWNrKT1cInRoaXMuYnV0dG9uLmdldEZ1bmN0aW9uVG9FeGVjdXRlKCkgPyB0aGlzLmJ1dHRvbi5nZXRGdW5jdGlvblRvRXhlY3V0ZSgpKCkgOiB1bmRlZmluZWRcIlxuICAgICAgICAgICAgW2Rpc2FibGVkXT1cInRoaXMuYnV0dG9uLmdldERpc2FibGVkKClcIlxuICAgICAgICAgICAgW2NvbG9yXT1cInRoaXMuYnV0dG9uLmdldENvbG9yKClcIj5cbiAgICAgICAgICAgIDxtYXQtaWNvbj57e3RoaXMuYnV0dG9uLmdldE1hdEljb25TdHJpbmcoKX19PC9tYXQtaWNvbj5cbiAgICAgICAgPC9idXR0b24+XG5cbiAgICAgICAgPGJ1dHRvbiBcbiAgICAgICAgICAgICpuZ0lmPVwidGhpcy5idXR0b24uZ2V0TWF0QnV0dG9uVHlwZSgpID09ICdNSU5JX0ZBQidcIlxuICAgICAgICAgICAgbWF0LW1pbmktZmFiXG4gICAgICAgICAgICBbdHlwZV09XCJ0aGlzLmJ1dHRvbi5nZXRCdXR0b25UeXBlKClcIlxuICAgICAgICAgICAgKGNsaWNrKT1cInRoaXMuYnV0dG9uLmdldEZ1bmN0aW9uVG9FeGVjdXRlKCkgPyB0aGlzLmJ1dHRvbi5nZXRGdW5jdGlvblRvRXhlY3V0ZSgpKCkgOiB1bmRlZmluZWRcIlxuICAgICAgICAgICAgW2Rpc2FibGVkXT1cInRoaXMuYnV0dG9uLmdldERpc2FibGVkKClcIlxuICAgICAgICAgICAgW2NvbG9yXT1cInRoaXMuYnV0dG9uLmdldENvbG9yKClcIj5cbiAgICAgICAgICAgIDxtYXQtaWNvbj57e3RoaXMuYnV0dG9uLmdldE1hdEljb25TdHJpbmcoKX19PC9tYXQtaWNvbj5cbiAgICAgICAgPC9idXR0b24+XG48L3NlY3Rpb24+Il19