import { Component, forwardRef, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import * as i0 from "@angular/core";
import * as i1 from "@angular/flex-layout/flex";
import * as i2 from "@angular/material/slider";
export class SliderComponent {
    constructor() {
        this.onChange = () => { };
        this.onTouched = () => { };
        this.onValueChange = (slider) => {
            // console.log(slider);
            this.slider.setDefaultValue(slider.value);
            this.onChange(this.slider.getDefaultValue());
            this.onTouched();
        };
        this.onBlur = () => {
            this.onChange(this.slider.getDefaultValue());
            this.onTouched();
        };
    }
    registerOnChange(fn) {
        this.onChange = fn;
    }
    registerOnTouched(fn) {
        this.onTouched = fn;
    }
    writeValue(value) {
        if (value) {
            this.slider.setDefaultValue(value);
        }
    }
    ngOnInit() {
        // console.log('SliderComponent init');
    }
    ngOnDestroy() {
        // console.log('SliderComponent destroyed');
    }
}
SliderComponent.ɵfac = function SliderComponent_Factory(t) { return new (t || SliderComponent)(); };
SliderComponent.ɵcmp = i0.ɵɵdefineComponent({ type: SliderComponent, selectors: [["app-slider"]], inputs: { slider: "slider" }, features: [i0.ɵɵProvidersFeature([
            {
                provide: NG_VALUE_ACCESSOR,
                multi: true,
                useExisting: forwardRef(() => SliderComponent),
            },
        ])], decls: 3, vars: 9, consts: [["fxLayoutAlign", "center center"], [3, "value", "min", "max", "disabled", "invert", "step", "tickInterval", "thumbLabel", "vertical", "change", "blur"], ["matSlider", ""]], template: function SliderComponent_Template(rf, ctx) { if (rf & 1) {
        const _r1 = i0.ɵɵgetCurrentView();
        i0.ɵɵelementStart(0, "section", 0);
        i0.ɵɵelementStart(1, "mat-slider", 1, 2);
        i0.ɵɵlistener("change", function SliderComponent_Template_mat_slider_change_1_listener() { i0.ɵɵrestoreView(_r1); const _r0 = i0.ɵɵreference(2); return ctx.onValueChange(_r0); })("blur", function SliderComponent_Template_mat_slider_blur_1_listener() { return ctx.onBlur(); });
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("value", ctx.slider.getDefaultValue())("min", ctx.slider.getMinValue() ? ctx.slider.getMinValue() : undefined)("max", ctx.slider.getMaxValue() ? ctx.slider.getMaxValue() : undefined)("disabled", ctx.slider.getDisabled())("invert", ctx.slider.getInvert())("step", ctx.slider.getStep())("tickInterval", ctx.slider.getTickInterval())("thumbLabel", ctx.slider.getThumbLabel())("vertical", ctx.slider.getVertical());
    } }, directives: [i1.DefaultLayoutAlignDirective, i2.MatSlider], styles: ["", "mat-slider[_ngcontent-%COMP%] {\n        min-width: 250px;\n    }\n\n    section[_ngcontent-%COMP%] {\n        min-height: 70px;\n    }"] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(SliderComponent, [{
        type: Component,
        args: [{
                selector: 'app-slider',
                templateUrl: './slider.component.html',
                styleUrls: ['./slider.component.css'],
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        multi: true,
                        useExisting: forwardRef(() => SliderComponent),
                    },
                ],
            }]
    }], function () { return []; }, { slider: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2xpZGVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215LWFuZ3VsYXItZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvdWkvc2xpZGVyL3NsaWRlci5jb21wb25lbnQudHMiLCJsaWIvc2hhcmVkL3VpL3NsaWRlci9zbGlkZXIuY29tcG9uZW50Lmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBcUIsVUFBVSxFQUFFLEtBQUssRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNoRixPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQzs7OztBQWdCbkQsTUFBTSxPQUFPLGVBQWU7SUFNMUI7UUFITyxhQUFRLEdBQVEsR0FBRyxFQUFFLEdBQUUsQ0FBQyxDQUFDO1FBQ3pCLGNBQVMsR0FBUSxHQUFHLEVBQUUsR0FBRSxDQUFDLENBQUM7UUFJakMsa0JBQWEsR0FBRyxDQUFDLE1BQWlCLEVBQVEsRUFBRTtZQUMxQyx1QkFBdUI7WUFDdkIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxlQUFlLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxlQUFlLEVBQUUsQ0FBQyxDQUFDO1lBQzdDLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUNuQixDQUFDLENBQUM7UUFFRixXQUFNLEdBQUcsR0FBUyxFQUFFO1lBQ2xCLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxlQUFlLEVBQUUsQ0FBQyxDQUFDO1lBQzdDLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUNuQixDQUFDLENBQUM7SUFaYSxDQUFDO0lBY2hCLGdCQUFnQixDQUFDLEVBQUU7UUFDakIsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7SUFDckIsQ0FBQztJQUVELGlCQUFpQixDQUFDLEVBQUU7UUFDbEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7SUFDdEIsQ0FBQztJQUVELFVBQVUsQ0FBQyxLQUFLO1FBQ2QsSUFBSSxLQUFLLEVBQUU7WUFDVCxJQUFJLENBQUMsTUFBTSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUNwQztJQUNILENBQUM7SUFFRCxRQUFRO1FBQ04sdUNBQXVDO0lBQ3pDLENBQUM7SUFFRCxXQUFXO1FBQ1QsNENBQTRDO0lBQzlDLENBQUM7OzhFQXhDVSxlQUFlO29EQUFmLGVBQWUsOEZBUmY7WUFDVDtnQkFDRSxPQUFPLEVBQUUsaUJBQWlCO2dCQUMxQixLQUFLLEVBQUUsSUFBSTtnQkFDWCxXQUFXLEVBQUUsVUFBVSxDQUFDLEdBQUcsRUFBRSxDQUFDLGVBQWUsQ0FBQzthQUMvQztTQUNGOztRQ0hILGtDQUdRO1FBQUEsd0NBWTRCO1FBVnhCLHdKQUFVLHNCQUE2QixJQUFDLGlGQUNoQyxZQUFhLElBRG1CO1FBVTdCLGlCQUFhO1FBRXBDLGlCQUFVOztRQWJFLGVBQXVDO1FBQXZDLG9EQUF1Qyx3RUFBQSx3RUFBQSxzQ0FBQSxrQ0FBQSw4QkFBQSw4Q0FBQSwwQ0FBQSxzQ0FBQTs7a0REQ3RDLGVBQWU7Y0FaM0IsU0FBUztlQUFDO2dCQUNULFFBQVEsRUFBRSxZQUFZO2dCQUN0QixXQUFXLEVBQUUseUJBQXlCO2dCQUN0QyxTQUFTLEVBQUUsQ0FBQyx3QkFBd0IsQ0FBQztnQkFDckMsU0FBUyxFQUFFO29CQUNUO3dCQUNFLE9BQU8sRUFBRSxpQkFBaUI7d0JBQzFCLEtBQUssRUFBRSxJQUFJO3dCQUNYLFdBQVcsRUFBRSxVQUFVLENBQUMsR0FBRyxFQUFFLENBQUMsZUFBZSxDQUFDO3FCQUMvQztpQkFDRjthQUNGOztrQkFFRSxLQUFLIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIE9uRGVzdHJveSwgZm9yd2FyZFJlZiwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE5HX1ZBTFVFX0FDQ0VTU09SIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuaW1wb3J0IHsgU2xpZGVyIH0gZnJvbSAnLi4vLi4vbW9kZWwvc3RydWN0dXJhbC9jb21wb3NpdGUvZm9ybS9zbGlkZXIvc2xpZGVyLm1vZGVsJztcbmltcG9ydCB7IE1hdFNsaWRlciB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL3NsaWRlcic7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2FwcC1zbGlkZXInLFxuICB0ZW1wbGF0ZVVybDogJy4vc2xpZGVyLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vc2xpZGVyLmNvbXBvbmVudC5jc3MnXSxcbiAgcHJvdmlkZXJzOiBbXG4gICAge1xuICAgICAgcHJvdmlkZTogTkdfVkFMVUVfQUNDRVNTT1IsXG4gICAgICBtdWx0aTogdHJ1ZSxcbiAgICAgIHVzZUV4aXN0aW5nOiBmb3J3YXJkUmVmKCgpID0+IFNsaWRlckNvbXBvbmVudCksXG4gICAgfSxcbiAgXSxcbn0pXG5leHBvcnQgY2xhc3MgU2xpZGVyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xuICBASW5wdXQoKSBzbGlkZXI6IFNsaWRlcjtcblxuICBwdWJsaWMgb25DaGFuZ2U6IGFueSA9ICgpID0+IHt9O1xuICBwdWJsaWMgb25Ub3VjaGVkOiBhbnkgPSAoKSA9PiB7fTtcblxuICBjb25zdHJ1Y3RvcigpIHt9XG5cbiAgb25WYWx1ZUNoYW5nZSA9IChzbGlkZXI6IE1hdFNsaWRlcik6IHZvaWQgPT4ge1xuICAgIC8vIGNvbnNvbGUubG9nKHNsaWRlcik7XG4gICAgdGhpcy5zbGlkZXIuc2V0RGVmYXVsdFZhbHVlKHNsaWRlci52YWx1ZSk7XG4gICAgdGhpcy5vbkNoYW5nZSh0aGlzLnNsaWRlci5nZXREZWZhdWx0VmFsdWUoKSk7XG4gICAgdGhpcy5vblRvdWNoZWQoKTtcbiAgfTtcblxuICBvbkJsdXIgPSAoKTogdm9pZCA9PiB7XG4gICAgdGhpcy5vbkNoYW5nZSh0aGlzLnNsaWRlci5nZXREZWZhdWx0VmFsdWUoKSk7XG4gICAgdGhpcy5vblRvdWNoZWQoKTtcbiAgfTtcblxuICByZWdpc3Rlck9uQ2hhbmdlKGZuKSB7XG4gICAgdGhpcy5vbkNoYW5nZSA9IGZuO1xuICB9XG5cbiAgcmVnaXN0ZXJPblRvdWNoZWQoZm4pIHtcbiAgICB0aGlzLm9uVG91Y2hlZCA9IGZuO1xuICB9XG5cbiAgd3JpdGVWYWx1ZSh2YWx1ZSkge1xuICAgIGlmICh2YWx1ZSkge1xuICAgICAgdGhpcy5zbGlkZXIuc2V0RGVmYXVsdFZhbHVlKHZhbHVlKTtcbiAgICB9XG4gIH1cblxuICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgICAvLyBjb25zb2xlLmxvZygnU2xpZGVyQ29tcG9uZW50IGluaXQnKTtcbiAgfVxuXG4gIG5nT25EZXN0cm95KCk6IHZvaWQge1xuICAgIC8vIGNvbnNvbGUubG9nKCdTbGlkZXJDb21wb25lbnQgZGVzdHJveWVkJyk7XG4gIH1cbn1cbiIsIjxzdHlsZT5cblxuICAgIG1hdC1zbGlkZXIge1xuICAgICAgICBtaW4td2lkdGg6IDI1MHB4O1xuICAgIH1cblxuICAgIHNlY3Rpb24ge1xuICAgICAgICBtaW4taGVpZ2h0OiA3MHB4O1xuICAgIH1cblxuPC9zdHlsZT5cblxuPHNlY3Rpb25cbiAgICBmeExheW91dEFsaWduPVwiY2VudGVyIGNlbnRlclwiPlxuXG4gICAgICAgIDxtYXQtc2xpZGVyXG4gICAgICAgICAgICBbdmFsdWVdPVwidGhpcy5zbGlkZXIuZ2V0RGVmYXVsdFZhbHVlKClcIlxuICAgICAgICAgICAgKGNoYW5nZSk9XCJ0aGlzLm9uVmFsdWVDaGFuZ2UobWF0U2xpZGVyKVwiXG4gICAgICAgICAgICAoYmx1cik9XCJ0aGlzLm9uQmx1cigpXCJcbiAgICAgICAgICAgIFttaW5dPVwidGhpcy5zbGlkZXIuZ2V0TWluVmFsdWUoKSA/IHRoaXMuc2xpZGVyLmdldE1pblZhbHVlKCkgOiB1bmRlZmluZWRcIlxuICAgICAgICAgICAgW21heF09XCJ0aGlzLnNsaWRlci5nZXRNYXhWYWx1ZSgpID8gdGhpcy5zbGlkZXIuZ2V0TWF4VmFsdWUoKSA6IHVuZGVmaW5lZFwiXG4gICAgICAgICAgICBbZGlzYWJsZWRdPVwidGhpcy5zbGlkZXIuZ2V0RGlzYWJsZWQoKVwiXG4gICAgICAgICAgICBbaW52ZXJ0XT1cInRoaXMuc2xpZGVyLmdldEludmVydCgpXCJcbiAgICAgICAgICAgIFtzdGVwXT1cInRoaXMuc2xpZGVyLmdldFN0ZXAoKVwiXG4gICAgICAgICAgICBbdGlja0ludGVydmFsXT1cInRoaXMuc2xpZGVyLmdldFRpY2tJbnRlcnZhbCgpXCJcbiAgICAgICAgICAgIFt0aHVtYkxhYmVsXT1cInRoaXMuc2xpZGVyLmdldFRodW1iTGFiZWwoKVwiXG4gICAgICAgICAgICBbdmVydGljYWxdPVwidGhpcy5zbGlkZXIuZ2V0VmVydGljYWwoKVwiXG4gICAgICAgICAgICAjbWF0U2xpZGVyPjwvbWF0LXNsaWRlcj5cblxuPC9zZWN0aW9uPiJdfQ==