import { Component, forwardRef, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { MAT_DATE_FORMATS, DateAdapter, MAT_DATE_LOCALE, } from '@angular/material/core';
import * as i0 from "@angular/core";
import * as i1 from "../../service/device.service";
import * as i2 from "@angular/flex-layout/flex";
import * as i3 from "@angular/material/form-field";
import * as i4 from "@angular/material/input";
import * as i5 from "@angular/material/datepicker";
import * as i6 from "@angular/common";
import * as i7 from "@angular/material/icon";
import * as i8 from "ngx-material-timepicker";
function DatepickerComponent_mat_datepicker_toggle_6_mat_icon_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-icon", 11);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r5 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r5.datepicker.getMatIcon());
} }
function DatepickerComponent_mat_datepicker_toggle_6_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-datepicker-toggle", 9);
    i0.ɵɵtemplate(1, DatepickerComponent_mat_datepicker_toggle_6_mat_icon_1_Template, 2, 1, "mat-icon", 10);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r1 = i0.ɵɵnextContext();
    const _r3 = i0.ɵɵreference(9);
    i0.ɵɵproperty("for", _r3);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r1.datepicker.getMatIcon());
} }
function DatepickerComponent_mat_datepicker_toggle_7_mat_icon_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-icon", 11);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r6 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r6.datepicker.getMatIcon());
} }
function DatepickerComponent_mat_datepicker_toggle_7_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-datepicker-toggle", 12);
    i0.ɵɵtemplate(1, DatepickerComponent_mat_datepicker_toggle_7_mat_icon_1_Template, 2, 1, "mat-icon", 10);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r2 = i0.ɵɵnextContext();
    const _r3 = i0.ɵɵreference(9);
    i0.ɵɵproperty("for", _r3);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r2.datepicker.getMatIcon());
} }
function DatepickerComponent_mat_form_field_10_Template(rf, ctx) { if (rf & 1) {
    const _r8 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "mat-form-field", 13);
    i0.ɵɵelementStart(1, "mat-label");
    i0.ɵɵtext(2);
    i0.ɵɵelementEnd();
    i0.ɵɵelement(3, "br");
    i0.ɵɵelementStart(4, "ngx-timepicker-field", 14);
    i0.ɵɵlistener("timeChanged", function DatepickerComponent_mat_form_field_10_Template_ngx_timepicker_field_timeChanged_4_listener($event) { i0.ɵɵrestoreView(_r8); const ctx_r7 = i0.ɵɵnextContext(); return ctx_r7.onTimeChanged($event); });
    i0.ɵɵelementEnd();
    i0.ɵɵelement(5, "input", 15);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r4 = i0.ɵɵnextContext();
    i0.ɵɵproperty("appearance", ctx_r4.datepicker.getAppearance());
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(ctx_r4.datepicker.getTimepickerInsideDatepickerInterface().labelName ? ctx_r4.datepicker.getTimepickerInsideDatepickerInterface().labelName : "pick time");
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("buttonAlign", ctx_r4.datepicker.getTimepickerInsideDatepickerInterface().buttonAlign ? ctx_r4.datepicker.getTimepickerInsideDatepickerInterface().buttonAlign : "right")("disabled", ctx_r4.datepicker.getTimepickerInsideDatepickerInterface().disabled)("format", 24)("min", ctx_r4.datepicker.getTimepickerInsideDatepickerInterface().min ? ctx_r4.datepicker.getTimepickerInsideDatepickerInterface().min : undefined)("max", ctx_r4.datepicker.getTimepickerInsideDatepickerInterface().max ? ctx_r4.datepicker.getTimepickerInsideDatepickerInterface().max : undefined)("defaultTime", ctx_r4.datepicker.getTimepickerInsideDatepickerInterface().defaultValue ? ctx_r4.datepicker.getTimepickerInsideDatepickerInterface().defaultValue : undefined);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("hidden", true);
} }
export const MY_FORMATS = {
    parse: {
        dateInput: 'LL',
    },
    display: {
        dateInput: 'YYYY-MM-DD',
        monthYearLabel: 'YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'YYYY',
    },
};
export class DatepickerComponent {
    constructor(deviceService) {
        this.deviceService = deviceService;
        this.onChange = () => { };
        this.onTouched = () => { };
        this.onBlur = () => {
            this.onChange(this.datepicker.getDefaultValue());
            this.onTouched();
        };
        this.disableDays = (d) => {
            const day = d.day();
            let allow = true;
            this.datepicker.getDisabledDays().forEach((d) => {
                if (day === d)
                    allow = false;
            });
            return allow;
        };
        this.onTimeChanged = (time) => {
            let sati = +time.split(':')[0];
            let minuti = +time.split(':')[1];
            // console.log('treba dodati time: sati: ', sati, ' minuti: ', minuti);
            if (this.datepicker.getDefaultValue() instanceof Date) {
                // console.log('treba dodati time: sati: ', sati, ' minuti: ', minuti);
                let date = this.datepicker.getDefaultValue();
                date.setHours(date.getHours() - this._prethodniSati + sati);
                date.setMinutes(date.getMinutes() - this._prethodniMinuti + minuti);
                // console.log(date.toUTCString());
            }
            this._prethodniSati = sati;
            this._prethodniMinuti = minuti;
        };
    }
    addEvent(type, event) {
        // console.log(`${type}: ${event.value}`);
        // console.log(new Date(event.value));
        let date = new Date(event.value);
        date.setHours(date.getHours() + this._prethodniSati);
        date.setMinutes(date.getMinutes() + this._prethodniMinuti);
        this.datepicker.setDefaultValue(date);
        this.onChange(this.datepicker.getDefaultValue());
        this.onTouched();
        // console.log(this.datepicker.getDefaultValue());
        // console.log(this.datepicker.getDefaultValue().toUTCString());
    }
    registerOnChange(fn) {
        this.onChange = fn;
    }
    registerOnTouched(fn) {
        this.onTouched = fn;
    }
    writeValue(value) {
        if (value) {
            this.datepicker.setDefaultValue(value);
        }
    }
    ngOnInit() {
        var _a;
        if ((_a = this.datepicker.getTimepickerInsideDatepickerInterface()) === null || _a === void 0 ? void 0 : _a.defaultValue) {
            this._prethodniSati = +this.datepicker
                .getTimepickerInsideDatepickerInterface()
                .defaultValue.split(':')[0];
            this._prethodniMinuti = +this.datepicker
                .getTimepickerInsideDatepickerInterface()
                .defaultValue.split(':')[1];
            // console.log(this._prethodniSati, this._prethodniMinuti);
        }
        else {
            this._prethodniSati = 0;
            this._prethodniMinuti = 0;
        }
        // console.log('DatepickerComponent init');
    }
    ngOnDestroy() {
        // console.log('DatepickerComponent destroyed');
    }
}
DatepickerComponent.ɵfac = function DatepickerComponent_Factory(t) { return new (t || DatepickerComponent)(i0.ɵɵdirectiveInject(i1.DeviceService)); };
DatepickerComponent.ɵcmp = i0.ɵɵdefineComponent({ type: DatepickerComponent, selectors: [["app-datepicker"]], inputs: { datepicker: "datepicker" }, features: [i0.ɵɵProvidersFeature([
            {
                provide: NG_VALUE_ACCESSOR,
                multi: true,
                useExisting: forwardRef(() => DatepickerComponent),
            },
            {
                provide: DateAdapter,
                useClass: MomentDateAdapter,
                deps: [MAT_DATE_LOCALE],
            },
            { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
        ])], decls: 11, vars: 15, consts: [["fxLayout", "column", "fxLayoutAlign", "center center"], [3, "color", "appearance"], ["matInput", "", 3, "min", "max", "value", "matDatepickerFilter", "matDatepicker", "disabled", "blur", "dateInput", "dateChange"], ["input", ""], ["matSuffix", "", 3, "for", 4, "ngIf"], ["matPrefix", "", 3, "for", 4, "ngIf"], [3, "startView", "startAt", "touchUi"], ["picker", ""], [3, "appearance", 4, "ngIf"], ["matSuffix", "", 3, "for"], ["matDatepickerToggleIcon", "", 4, "ngIf"], ["matDatepickerToggleIcon", ""], ["matPrefix", "", 3, "for"], [3, "appearance"], [3, "buttonAlign", "disabled", "format", "min", "max", "defaultTime", "timeChanged"], ["matInput", "", "type", "timepicker", 3, "hidden"]], template: function DatepickerComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "section", 0);
        i0.ɵɵelementStart(1, "mat-form-field", 1);
        i0.ɵɵelementStart(2, "mat-label");
        i0.ɵɵtext(3);
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(4, "input", 2, 3);
        i0.ɵɵlistener("blur", function DatepickerComponent_Template_input_blur_4_listener() { return ctx.onBlur(); })("dateInput", function DatepickerComponent_Template_input_dateInput_4_listener($event) { return ctx.addEvent("input", $event); })("dateChange", function DatepickerComponent_Template_input_dateChange_4_listener($event) { return ctx.addEvent("change", $event); });
        i0.ɵɵelementEnd();
        i0.ɵɵtemplate(6, DatepickerComponent_mat_datepicker_toggle_6_Template, 2, 2, "mat-datepicker-toggle", 4);
        i0.ɵɵtemplate(7, DatepickerComponent_mat_datepicker_toggle_7_Template, 2, 2, "mat-datepicker-toggle", 5);
        i0.ɵɵelement(8, "mat-datepicker", 6, 7);
        i0.ɵɵelementEnd();
        i0.ɵɵtemplate(10, DatepickerComponent_mat_form_field_10_Template, 6, 9, "mat-form-field", 8);
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        const _r3 = i0.ɵɵreference(9);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("color", ctx.datepicker.getColor())("appearance", ctx.datepicker.getAppearance());
        i0.ɵɵadvance(2);
        i0.ɵɵtextInterpolate(ctx.datepicker.getLabelName());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("min", ctx.datepicker.getMin() ? ctx.datepicker.getMin() : "")("max", ctx.datepicker.getMax() ? ctx.datepicker.getMax() : "")("value", ctx.datepicker.getDefaultValue())("matDatepickerFilter", ctx.disableDays)("matDatepicker", _r3)("disabled", ctx.datepicker.getDisabled());
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("ngIf", ctx.datepicker.getToggleSideSuffix());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", !ctx.datepicker.getToggleSideSuffix());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("startView", ctx.datepicker.getStartView())("startAt", ctx.datepicker.getStartAt() ? ctx.datepicker.getStartAt() : "")("touchUi", !ctx.deviceService.isDeviceDesktop());
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("ngIf", ctx.datepicker.getTimepickerInsideDatepickerInterface());
    } }, directives: [i2.DefaultLayoutDirective, i2.DefaultLayoutAlignDirective, i3.MatFormField, i3.MatLabel, i4.MatInput, i5.MatDatepickerInput, i6.NgIf, i5.MatDatepicker, i5.MatDatepickerToggle, i3.MatSuffix, i7.MatIcon, i5.MatDatepickerToggleIcon, i3.MatPrefix, i8.NgxTimepickerFieldComponent], styles: ["", "mat-form-field[_ngcontent-%COMP%] {\n        min-width: 250px;\n    }"] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(DatepickerComponent, [{
        type: Component,
        args: [{
                selector: 'app-datepicker',
                templateUrl: './datepicker.component.html',
                styleUrls: ['./datepicker.component.css'],
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        multi: true,
                        useExisting: forwardRef(() => DatepickerComponent),
                    },
                    {
                        provide: DateAdapter,
                        useClass: MomentDateAdapter,
                        deps: [MAT_DATE_LOCALE],
                    },
                    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
                ],
            }]
    }], function () { return [{ type: i1.DeviceService }]; }, { datepicker: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0ZXBpY2tlci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9teS1hbmd1bGFyLWZvcm0vIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL3VpL2RhdGVwaWNrZXIvZGF0ZXBpY2tlci5jb21wb25lbnQudHMiLCJsaWIvc2hhcmVkL3VpL2RhdGVwaWNrZXIvZGF0ZXBpY2tlci5jb21wb25lbnQuaHRtbCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFxQixVQUFVLEVBQUUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ2hGLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBTW5ELE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQ3JFLE9BQU8sRUFDTCxnQkFBZ0IsRUFDaEIsV0FBVyxFQUNYLGVBQWUsR0FDaEIsTUFBTSx3QkFBd0IsQ0FBQzs7Ozs7Ozs7Ozs7SUNvQnBCLG9DQUU0QjtJQUFBLFlBQWtDO0lBQUEsaUJBQVc7OztJQUE3QyxlQUFrQztJQUFsQyxvREFBa0M7OztJQVBsRSxnREFLSTtJQUFBLHVHQUU0QjtJQUVoQyxpQkFBd0I7Ozs7SUFOcEIseUJBQWM7SUFHVixlQUFvQztJQUFwQyxxREFBb0M7OztJQVd4QyxvQ0FFNEI7SUFBQSxZQUFrQztJQUFBLGlCQUFXOzs7SUFBN0MsZUFBa0M7SUFBbEMsb0RBQWtDOzs7SUFQbEUsaURBS0k7SUFBQSx1R0FFNEI7SUFFaEMsaUJBQXdCOzs7O0lBTnBCLHlCQUFjO0lBR1YsZUFBb0M7SUFBcEMscURBQW9DOzs7O0lBYWhELDBDQUlJO0lBQUEsaUNBQVc7SUFBQSxZQUF5SjtJQUFBLGlCQUFZO0lBQ2hMLHFCQUVBO0lBSUEsZ0RBT3NFO0lBQWxFLDRPQUEwQztJQUFDLGlCQUF1QjtJQUV0RSw0QkFDSjtJQUFBLGlCQUFpQjs7O0lBbkJiLDhEQUE4QztJQUVuQyxlQUF5SjtJQUF6SiwrS0FBeUo7SUFRaEssZUFBcUs7SUFBckssdUxBQXFLLGlGQUFBLGNBQUEsb0pBQUEsb0pBQUEsOEtBQUE7SUFRdkksZUFBZTtJQUFmLDZCQUFlOztBRDlEekQsTUFBTSxDQUFDLE1BQU0sVUFBVSxHQUFHO0lBQ3hCLEtBQUssRUFBRTtRQUNMLFNBQVMsRUFBRSxJQUFJO0tBQ2hCO0lBQ0QsT0FBTyxFQUFFO1FBQ1AsU0FBUyxFQUFFLFlBQVk7UUFDdkIsY0FBYyxFQUFFLE1BQU07UUFDdEIsYUFBYSxFQUFFLElBQUk7UUFDbkIsa0JBQWtCLEVBQUUsTUFBTTtLQUMzQjtDQUNGLENBQUM7QUFvQkYsTUFBTSxPQUFPLG1CQUFtQjtJQVE5QixZQUFtQixhQUE0QjtRQUE1QixrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQU54QyxhQUFRLEdBQVEsR0FBRyxFQUFFLEdBQUUsQ0FBQyxDQUFDO1FBQ3pCLGNBQVMsR0FBUSxHQUFHLEVBQUUsR0FBRSxDQUFDLENBQUM7UUFPakMsV0FBTSxHQUFHLEdBQVMsRUFBRTtZQUNsQixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxFQUFFLENBQUMsQ0FBQztZQUNqRCxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDbkIsQ0FBQyxDQUFDO1FBa0JGLGdCQUFXLEdBQUcsQ0FBQyxDQUFTLEVBQVcsRUFBRTtZQUNuQyxNQUFNLEdBQUcsR0FBRyxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUM7WUFFcEIsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDO1lBQ2pCLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxFQUFFLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBTSxFQUFFLEVBQUU7Z0JBQ25ELElBQUksR0FBRyxLQUFLLENBQUM7b0JBQUUsS0FBSyxHQUFHLEtBQUssQ0FBQztZQUMvQixDQUFDLENBQUMsQ0FBQztZQUVILE9BQU8sS0FBSyxDQUFDO1FBQ2YsQ0FBQyxDQUFDO1FBRUYsa0JBQWEsR0FBRyxDQUFDLElBQVksRUFBUSxFQUFFO1lBQ3JDLElBQUksSUFBSSxHQUFXLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN2QyxJQUFJLE1BQU0sR0FBVyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDekMsdUVBQXVFO1lBRXZFLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLEVBQUUsWUFBWSxJQUFJLEVBQUU7Z0JBQ3JELHVFQUF1RTtnQkFFdkUsSUFBSSxJQUFJLEdBQWUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLEVBQUUsQ0FBQztnQkFFekQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLEdBQUcsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsQ0FBQztnQkFDNUQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixHQUFHLE1BQU0sQ0FBQyxDQUFDO2dCQUVwRSxtQ0FBbUM7YUFDcEM7WUFFRCxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQztZQUMzQixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsTUFBTSxDQUFDO1FBQ2pDLENBQUMsQ0FBQztJQXBEZ0QsQ0FBQztJQU9uRCxRQUFRLENBQUMsSUFBWSxFQUFFLEtBQW9DO1FBQ3pELDBDQUEwQztRQUMxQyxzQ0FBc0M7UUFFdEMsSUFBSSxJQUFJLEdBQVMsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUNyRCxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUUzRCxJQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN0QyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxFQUFFLENBQUMsQ0FBQztRQUNqRCxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7UUFFakIsa0RBQWtEO1FBQ2xELGdFQUFnRTtJQUNsRSxDQUFDO0lBaUNELGdCQUFnQixDQUFDLEVBQUU7UUFDakIsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7SUFDckIsQ0FBQztJQUVELGlCQUFpQixDQUFDLEVBQUU7UUFDbEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7SUFDdEIsQ0FBQztJQUVELFVBQVUsQ0FBQyxLQUFLO1FBQ2QsSUFBSSxLQUFLLEVBQUU7WUFDVCxJQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUN4QztJQUNILENBQUM7SUFFRCxRQUFROztRQUNOLFVBQ0UsSUFBSSxDQUFDLFVBQVUsQ0FBQyxzQ0FBc0MsRUFBRSwwQ0FBRSxZQUFZLEVBQ3RFO1lBQ0EsSUFBSSxDQUFDLGNBQWMsR0FBRyxDQUFDLElBQUksQ0FBQyxVQUFVO2lCQUNuQyxzQ0FBc0MsRUFBRTtpQkFDeEMsWUFBWSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUM5QixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVTtpQkFDckMsc0NBQXNDLEVBQUU7aUJBQ3hDLFlBQVksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFFOUIsMkRBQTJEO1NBQzVEO2FBQU07WUFDTCxJQUFJLENBQUMsY0FBYyxHQUFHLENBQUMsQ0FBQztZQUN4QixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsQ0FBQyxDQUFDO1NBQzNCO1FBRUQsMkNBQTJDO0lBQzdDLENBQUM7SUFFRCxXQUFXO1FBQ1QsZ0RBQWdEO0lBQ2xELENBQUM7O3NGQWxHVSxtQkFBbUI7d0RBQW5CLG1CQUFtQiwwR0FkbkI7WUFDVDtnQkFDRSxPQUFPLEVBQUUsaUJBQWlCO2dCQUMxQixLQUFLLEVBQUUsSUFBSTtnQkFDWCxXQUFXLEVBQUUsVUFBVSxDQUFDLEdBQUcsRUFBRSxDQUFDLG1CQUFtQixDQUFDO2FBQ25EO1lBQ0Q7Z0JBQ0UsT0FBTyxFQUFFLFdBQVc7Z0JBQ3BCLFFBQVEsRUFBRSxpQkFBaUI7Z0JBQzNCLElBQUksRUFBRSxDQUFDLGVBQWUsQ0FBQzthQUN4QjtZQUNELEVBQUUsT0FBTyxFQUFFLGdCQUFnQixFQUFFLFFBQVEsRUFBRSxVQUFVLEVBQUU7U0FDcEQ7UUNwQ0gsa0NBSUk7UUFBQSx5Q0FDSTtRQUFBLGlDQUFXO1FBQUEsWUFBb0M7UUFBQSxpQkFBWTtRQUMzRCxtQ0FZQTtRQVBJLDZGQUFRLFlBQWEsSUFBQyxnR0FDVCxhQUFjLE9BQU8sU0FBUyxJQURyQixrR0FDcUMsYUFBYyxRQUFRLFNBQVMsSUFEcEU7UUFMMUIsaUJBWUE7UUFDQSx3R0FLSTtRQU9KLHdHQUtJO1FBTUosdUNBSXVFO1FBRTNFLGlCQUFpQjtRQUVqQiw0RkFJSTtRQW1CUixpQkFBVTs7O1FBckVVLGVBQW9DO1FBQXBDLGlEQUFvQyw4Q0FBQTtRQUNyQyxlQUFvQztRQUFwQyxtREFBb0M7UUFHM0MsZUFBZ0U7UUFBaEUsNEVBQWdFLCtEQUFBLDJDQUFBLHdDQUFBLHNCQUFBLDBDQUFBO1FBWWhFLGVBQTZDO1FBQTdDLDJEQUE2QztRQVk3QyxlQUE4QztRQUE5Qyw0REFBOEM7UUFXOUMsZUFBNEM7UUFBNUMseURBQTRDLDJFQUFBLGlEQUFBO1FBUWhELGVBQWdFO1FBQWhFLDhFQUFnRTs7a0REYjNELG1CQUFtQjtjQWxCL0IsU0FBUztlQUFDO2dCQUNULFFBQVEsRUFBRSxnQkFBZ0I7Z0JBQzFCLFdBQVcsRUFBRSw2QkFBNkI7Z0JBQzFDLFNBQVMsRUFBRSxDQUFDLDRCQUE0QixDQUFDO2dCQUN6QyxTQUFTLEVBQUU7b0JBQ1Q7d0JBQ0UsT0FBTyxFQUFFLGlCQUFpQjt3QkFDMUIsS0FBSyxFQUFFLElBQUk7d0JBQ1gsV0FBVyxFQUFFLFVBQVUsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxtQkFBbUIsQ0FBQztxQkFDbkQ7b0JBQ0Q7d0JBQ0UsT0FBTyxFQUFFLFdBQVc7d0JBQ3BCLFFBQVEsRUFBRSxpQkFBaUI7d0JBQzNCLElBQUksRUFBRSxDQUFDLGVBQWUsQ0FBQztxQkFDeEI7b0JBQ0QsRUFBRSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsUUFBUSxFQUFFLFVBQVUsRUFBRTtpQkFDcEQ7YUFDRjs7a0JBRUUsS0FBSyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBPbkRlc3Ryb3ksIGZvcndhcmRSZWYsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBOR19WQUxVRV9BQ0NFU1NPUiB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7XG4gIERhdGVwaWNrZXIsXG4gIERBWSxcbn0gZnJvbSAnLi4vLi4vbW9kZWwvc3RydWN0dXJhbC9jb21wb3NpdGUvZm9ybS9kYXRlcGlja2VyL2RhdGVwaWNrZXIubW9kZWwnO1xuaW1wb3J0IHsgRGV2aWNlU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2UvZGV2aWNlLnNlcnZpY2UnO1xuaW1wb3J0IHsgTW9tZW50RGF0ZUFkYXB0ZXIgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC1tb21lbnQtYWRhcHRlcic7XG5pbXBvcnQge1xuICBNQVRfREFURV9GT1JNQVRTLFxuICBEYXRlQWRhcHRlcixcbiAgTUFUX0RBVEVfTE9DQUxFLFxufSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9jb3JlJztcbmltcG9ydCB7IE1hdERhdGVwaWNrZXJJbnB1dEV2ZW50IH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvZGF0ZXBpY2tlcic7XG5pbXBvcnQgeyBNb21lbnQgfSBmcm9tICdtb21lbnQnO1xuXG5leHBvcnQgY29uc3QgTVlfRk9STUFUUyA9IHtcbiAgcGFyc2U6IHtcbiAgICBkYXRlSW5wdXQ6ICdMTCcsXG4gIH0sXG4gIGRpc3BsYXk6IHtcbiAgICBkYXRlSW5wdXQ6ICdZWVlZLU1NLUREJyxcbiAgICBtb250aFllYXJMYWJlbDogJ1lZWVknLFxuICAgIGRhdGVBMTF5TGFiZWw6ICdMTCcsXG4gICAgbW9udGhZZWFyQTExeUxhYmVsOiAnWVlZWScsXG4gIH0sXG59O1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdhcHAtZGF0ZXBpY2tlcicsXG4gIHRlbXBsYXRlVXJsOiAnLi9kYXRlcGlja2VyLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vZGF0ZXBpY2tlci5jb21wb25lbnQuY3NzJ10sXG4gIHByb3ZpZGVyczogW1xuICAgIHtcbiAgICAgIHByb3ZpZGU6IE5HX1ZBTFVFX0FDQ0VTU09SLFxuICAgICAgbXVsdGk6IHRydWUsXG4gICAgICB1c2VFeGlzdGluZzogZm9yd2FyZFJlZigoKSA9PiBEYXRlcGlja2VyQ29tcG9uZW50KSxcbiAgICB9LFxuICAgIHtcbiAgICAgIHByb3ZpZGU6IERhdGVBZGFwdGVyLFxuICAgICAgdXNlQ2xhc3M6IE1vbWVudERhdGVBZGFwdGVyLFxuICAgICAgZGVwczogW01BVF9EQVRFX0xPQ0FMRV0sXG4gICAgfSxcbiAgICB7IHByb3ZpZGU6IE1BVF9EQVRFX0ZPUk1BVFMsIHVzZVZhbHVlOiBNWV9GT1JNQVRTIH0sXG4gIF0sXG59KVxuZXhwb3J0IGNsYXNzIERhdGVwaWNrZXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XG4gIEBJbnB1dCgpIGRhdGVwaWNrZXI6IERhdGVwaWNrZXI7XG4gIHB1YmxpYyBvbkNoYW5nZTogYW55ID0gKCkgPT4ge307XG4gIHB1YmxpYyBvblRvdWNoZWQ6IGFueSA9ICgpID0+IHt9O1xuXG4gIHByaXZhdGUgX3ByZXRob2RuaVNhdGk6IG51bWJlcjtcbiAgcHJpdmF0ZSBfcHJldGhvZG5pTWludXRpOiBudW1iZXI7XG5cbiAgY29uc3RydWN0b3IocHVibGljIGRldmljZVNlcnZpY2U6IERldmljZVNlcnZpY2UpIHt9XG5cbiAgb25CbHVyID0gKCk6IHZvaWQgPT4ge1xuICAgIHRoaXMub25DaGFuZ2UodGhpcy5kYXRlcGlja2VyLmdldERlZmF1bHRWYWx1ZSgpKTtcbiAgICB0aGlzLm9uVG91Y2hlZCgpO1xuICB9O1xuXG4gIGFkZEV2ZW50KHR5cGU6IHN0cmluZywgZXZlbnQ6IE1hdERhdGVwaWNrZXJJbnB1dEV2ZW50PERhdGU+KSB7XG4gICAgLy8gY29uc29sZS5sb2coYCR7dHlwZX06ICR7ZXZlbnQudmFsdWV9YCk7XG4gICAgLy8gY29uc29sZS5sb2cobmV3IERhdGUoZXZlbnQudmFsdWUpKTtcblxuICAgIGxldCBkYXRlOiBEYXRlID0gbmV3IERhdGUoZXZlbnQudmFsdWUpO1xuICAgIGRhdGUuc2V0SG91cnMoZGF0ZS5nZXRIb3VycygpICsgdGhpcy5fcHJldGhvZG5pU2F0aSk7XG4gICAgZGF0ZS5zZXRNaW51dGVzKGRhdGUuZ2V0TWludXRlcygpICsgdGhpcy5fcHJldGhvZG5pTWludXRpKTtcblxuICAgIHRoaXMuZGF0ZXBpY2tlci5zZXREZWZhdWx0VmFsdWUoZGF0ZSk7XG4gICAgdGhpcy5vbkNoYW5nZSh0aGlzLmRhdGVwaWNrZXIuZ2V0RGVmYXVsdFZhbHVlKCkpO1xuICAgIHRoaXMub25Ub3VjaGVkKCk7XG5cbiAgICAvLyBjb25zb2xlLmxvZyh0aGlzLmRhdGVwaWNrZXIuZ2V0RGVmYXVsdFZhbHVlKCkpO1xuICAgIC8vIGNvbnNvbGUubG9nKHRoaXMuZGF0ZXBpY2tlci5nZXREZWZhdWx0VmFsdWUoKS50b1VUQ1N0cmluZygpKTtcbiAgfVxuXG4gIGRpc2FibGVEYXlzID0gKGQ6IE1vbWVudCk6IGJvb2xlYW4gPT4ge1xuICAgIGNvbnN0IGRheSA9IGQuZGF5KCk7XG5cbiAgICBsZXQgYWxsb3cgPSB0cnVlO1xuICAgIHRoaXMuZGF0ZXBpY2tlci5nZXREaXNhYmxlZERheXMoKS5mb3JFYWNoKChkOiBEQVkpID0+IHtcbiAgICAgIGlmIChkYXkgPT09IGQpIGFsbG93ID0gZmFsc2U7XG4gICAgfSk7XG5cbiAgICByZXR1cm4gYWxsb3c7XG4gIH07XG5cbiAgb25UaW1lQ2hhbmdlZCA9ICh0aW1lOiBzdHJpbmcpOiB2b2lkID0+IHtcbiAgICBsZXQgc2F0aTogbnVtYmVyID0gK3RpbWUuc3BsaXQoJzonKVswXTtcbiAgICBsZXQgbWludXRpOiBudW1iZXIgPSArdGltZS5zcGxpdCgnOicpWzFdO1xuICAgIC8vIGNvbnNvbGUubG9nKCd0cmViYSBkb2RhdGkgdGltZTogc2F0aTogJywgc2F0aSwgJyBtaW51dGk6ICcsIG1pbnV0aSk7XG5cbiAgICBpZiAodGhpcy5kYXRlcGlja2VyLmdldERlZmF1bHRWYWx1ZSgpIGluc3RhbmNlb2YgRGF0ZSkge1xuICAgICAgLy8gY29uc29sZS5sb2coJ3RyZWJhIGRvZGF0aSB0aW1lOiBzYXRpOiAnLCBzYXRpLCAnIG1pbnV0aTogJywgbWludXRpKTtcblxuICAgICAgbGV0IGRhdGU6IERhdGUgPSA8RGF0ZT50aGlzLmRhdGVwaWNrZXIuZ2V0RGVmYXVsdFZhbHVlKCk7XG5cbiAgICAgIGRhdGUuc2V0SG91cnMoZGF0ZS5nZXRIb3VycygpIC0gdGhpcy5fcHJldGhvZG5pU2F0aSArIHNhdGkpO1xuICAgICAgZGF0ZS5zZXRNaW51dGVzKGRhdGUuZ2V0TWludXRlcygpIC0gdGhpcy5fcHJldGhvZG5pTWludXRpICsgbWludXRpKTtcblxuICAgICAgLy8gY29uc29sZS5sb2coZGF0ZS50b1VUQ1N0cmluZygpKTtcbiAgICB9XG5cbiAgICB0aGlzLl9wcmV0aG9kbmlTYXRpID0gc2F0aTtcbiAgICB0aGlzLl9wcmV0aG9kbmlNaW51dGkgPSBtaW51dGk7XG4gIH07XG5cbiAgcmVnaXN0ZXJPbkNoYW5nZShmbikge1xuICAgIHRoaXMub25DaGFuZ2UgPSBmbjtcbiAgfVxuXG4gIHJlZ2lzdGVyT25Ub3VjaGVkKGZuKSB7XG4gICAgdGhpcy5vblRvdWNoZWQgPSBmbjtcbiAgfVxuXG4gIHdyaXRlVmFsdWUodmFsdWUpIHtcbiAgICBpZiAodmFsdWUpIHtcbiAgICAgIHRoaXMuZGF0ZXBpY2tlci5zZXREZWZhdWx0VmFsdWUodmFsdWUpO1xuICAgIH1cbiAgfVxuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICAgIGlmIChcbiAgICAgIHRoaXMuZGF0ZXBpY2tlci5nZXRUaW1lcGlja2VySW5zaWRlRGF0ZXBpY2tlckludGVyZmFjZSgpPy5kZWZhdWx0VmFsdWVcbiAgICApIHtcbiAgICAgIHRoaXMuX3ByZXRob2RuaVNhdGkgPSArdGhpcy5kYXRlcGlja2VyXG4gICAgICAgIC5nZXRUaW1lcGlja2VySW5zaWRlRGF0ZXBpY2tlckludGVyZmFjZSgpXG4gICAgICAgIC5kZWZhdWx0VmFsdWUuc3BsaXQoJzonKVswXTtcbiAgICAgIHRoaXMuX3ByZXRob2RuaU1pbnV0aSA9ICt0aGlzLmRhdGVwaWNrZXJcbiAgICAgICAgLmdldFRpbWVwaWNrZXJJbnNpZGVEYXRlcGlja2VySW50ZXJmYWNlKClcbiAgICAgICAgLmRlZmF1bHRWYWx1ZS5zcGxpdCgnOicpWzFdO1xuXG4gICAgICAvLyBjb25zb2xlLmxvZyh0aGlzLl9wcmV0aG9kbmlTYXRpLCB0aGlzLl9wcmV0aG9kbmlNaW51dGkpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLl9wcmV0aG9kbmlTYXRpID0gMDtcbiAgICAgIHRoaXMuX3ByZXRob2RuaU1pbnV0aSA9IDA7XG4gICAgfVxuXG4gICAgLy8gY29uc29sZS5sb2coJ0RhdGVwaWNrZXJDb21wb25lbnQgaW5pdCcpO1xuICB9XG5cbiAgbmdPbkRlc3Ryb3koKTogdm9pZCB7XG4gICAgLy8gY29uc29sZS5sb2coJ0RhdGVwaWNrZXJDb21wb25lbnQgZGVzdHJveWVkJyk7XG4gIH1cbn1cbiIsIjxzdHlsZT5cblxuICAgIG1hdC1mb3JtLWZpZWxkIHtcbiAgICAgICAgbWluLXdpZHRoOiAyNTBweDtcbiAgICB9XG5cbjwvc3R5bGU+XG5cbjxzZWN0aW9uXG4gICAgZnhMYXlvdXQ9XCJjb2x1bW5cIlxuICAgIGZ4TGF5b3V0QWxpZ249XCJjZW50ZXIgY2VudGVyXCI+XG5cbiAgICA8bWF0LWZvcm0tZmllbGQgW2NvbG9yXT1cInRoaXMuZGF0ZXBpY2tlci5nZXRDb2xvcigpXCIgW2FwcGVhcmFuY2VdPVwidGhpcy5kYXRlcGlja2VyLmdldEFwcGVhcmFuY2UoKVwiPlxuICAgICAgICA8bWF0LWxhYmVsPnt7IHRoaXMuZGF0ZXBpY2tlci5nZXRMYWJlbE5hbWUoKSB9fTwvbWF0LWxhYmVsPlxuICAgICAgICA8aW5wdXQgXG4gICAgICAgICAgICBtYXRJbnB1dCBcbiAgICAgICAgICAgIFttaW5dPVwidGhpcy5kYXRlcGlja2VyLmdldE1pbigpID8gdGhpcy5kYXRlcGlja2VyLmdldE1pbigpIDogJydcIiBcbiAgICAgICAgICAgIFttYXhdPVwidGhpcy5kYXRlcGlja2VyLmdldE1heCgpID8gdGhpcy5kYXRlcGlja2VyLmdldE1heCgpIDogJydcIiBcbiAgICAgICAgICAgIFt2YWx1ZV09XCJ0aGlzLmRhdGVwaWNrZXIuZ2V0RGVmYXVsdFZhbHVlKClcIlxuICAgICAgICAgICAgKGJsdXIpPVwidGhpcy5vbkJsdXIoKVwiXG4gICAgICAgICAgICAoZGF0ZUlucHV0KT1cInRoaXMuYWRkRXZlbnQoJ2lucHV0JywgJGV2ZW50KVwiIChkYXRlQ2hhbmdlKT1cInRoaXMuYWRkRXZlbnQoJ2NoYW5nZScsICRldmVudClcIlxuICAgICAgICAgICAgI2lucHV0XG4gICAgICAgICAgICBbbWF0RGF0ZXBpY2tlckZpbHRlcl09XCJ0aGlzLmRpc2FibGVEYXlzXCJcbiAgICAgICAgICAgIFttYXREYXRlcGlja2VyXT1cInBpY2tlclwiXG4gICAgICAgICAgICBbZGlzYWJsZWRdPVwidGhpcy5kYXRlcGlja2VyLmdldERpc2FibGVkKClcIj5cblxuICAgICAgICA8IS0tIGlmIGdldFRvZ2dsZVNpZGVTdWZmaXgoKSAtPiB0cnVlIC0tPlxuICAgICAgICA8bWF0LWRhdGVwaWNrZXItdG9nZ2xlXG4gICAgICAgICAgICAqbmdJZj1cInRoaXMuZGF0ZXBpY2tlci5nZXRUb2dnbGVTaWRlU3VmZml4KClcIlxuICAgICAgICAgICAgbWF0U3VmZml4IFxuICAgICAgICAgICAgW2Zvcl09XCJwaWNrZXJcIj5cblxuICAgICAgICAgICAgPG1hdC1pY29uXG4gICAgICAgICAgICAgICAgKm5nSWY9XCJ0aGlzLmRhdGVwaWNrZXIuZ2V0TWF0SWNvbigpXCJcbiAgICAgICAgICAgICAgICBtYXREYXRlcGlja2VyVG9nZ2xlSWNvbj57eyB0aGlzLmRhdGVwaWNrZXIuZ2V0TWF0SWNvbigpIH19PC9tYXQtaWNvbj5cblxuICAgICAgICA8L21hdC1kYXRlcGlja2VyLXRvZ2dsZT5cblxuICAgICAgICA8IS0tIGlmIGdldFRvZ2dsZVNpZGVTdWZmaXgoKSAtPiBmYWxzZSAtLT5cbiAgICAgICAgPG1hdC1kYXRlcGlja2VyLXRvZ2dsZVxuICAgICAgICAgICAgKm5nSWY9XCIhdGhpcy5kYXRlcGlja2VyLmdldFRvZ2dsZVNpZGVTdWZmaXgoKVwiXG4gICAgICAgICAgICBtYXRQcmVmaXggXG4gICAgICAgICAgICBbZm9yXT1cInBpY2tlclwiPlxuXG4gICAgICAgICAgICA8bWF0LWljb25cbiAgICAgICAgICAgICAgICAqbmdJZj1cInRoaXMuZGF0ZXBpY2tlci5nZXRNYXRJY29uKClcIlxuICAgICAgICAgICAgICAgIG1hdERhdGVwaWNrZXJUb2dnbGVJY29uPnt7IHRoaXMuZGF0ZXBpY2tlci5nZXRNYXRJY29uKCkgfX08L21hdC1pY29uPlxuXG4gICAgICAgIDwvbWF0LWRhdGVwaWNrZXItdG9nZ2xlPlxuXG4gICAgICAgIDxtYXQtZGF0ZXBpY2tlciBcbiAgICAgICAgICAgIFtzdGFydFZpZXddPVwidGhpcy5kYXRlcGlja2VyLmdldFN0YXJ0VmlldygpXCIgXG4gICAgICAgICAgICBbc3RhcnRBdF09XCJ0aGlzLmRhdGVwaWNrZXIuZ2V0U3RhcnRBdCgpID8gdGhpcy5kYXRlcGlja2VyLmdldFN0YXJ0QXQoKSA6ICcnXCJcbiAgICAgICAgICAgICNwaWNrZXJcbiAgICAgICAgICAgIFt0b3VjaFVpXT1cIiF0aGlzLmRldmljZVNlcnZpY2UuaXNEZXZpY2VEZXNrdG9wKClcIj48L21hdC1kYXRlcGlja2VyPlxuXG4gICAgPC9tYXQtZm9ybS1maWVsZD5cblxuICAgIDxtYXQtZm9ybS1maWVsZCBcbiAgICAgICAgKm5nSWY9XCJ0aGlzLmRhdGVwaWNrZXIuZ2V0VGltZXBpY2tlckluc2lkZURhdGVwaWNrZXJJbnRlcmZhY2UoKVwiXG4gICAgICAgIFthcHBlYXJhbmNlXT1cInRoaXMuZGF0ZXBpY2tlci5nZXRBcHBlYXJhbmNlKClcIj5cblxuICAgICAgICA8bWF0LWxhYmVsPnt7dGhpcy5kYXRlcGlja2VyLmdldFRpbWVwaWNrZXJJbnNpZGVEYXRlcGlja2VySW50ZXJmYWNlKCkubGFiZWxOYW1lID8gdGhpcy5kYXRlcGlja2VyLmdldFRpbWVwaWNrZXJJbnNpZGVEYXRlcGlja2VySW50ZXJmYWNlKCkubGFiZWxOYW1lIDogJ3BpY2sgdGltZSd9fTwvbWF0LWxhYmVsPlxuICAgICAgICA8YnI+XG5cbiAgICAgICAgPCEtLSBcbiAgICAgICAgICAgIGh0dHBzOi8vYWdyYW5vbS5naXRodWIuaW8vbmd4LW1hdGVyaWFsLXRpbWVwaWNrZXIvXG4gICAgICAgICAgICBodHRwczovL3d3dy5ucG1qcy5jb20vcGFja2FnZS9uZ3gtbWF0ZXJpYWwtdGltZXBpY2tlclxuICAgICAgICAgICAgLS0+XG4gICAgICAgIDxuZ3gtdGltZXBpY2tlci1maWVsZCBcbiAgICAgICAgICAgIFtidXR0b25BbGlnbl09XCJ0aGlzLmRhdGVwaWNrZXIuZ2V0VGltZXBpY2tlckluc2lkZURhdGVwaWNrZXJJbnRlcmZhY2UoKS5idXR0b25BbGlnbiA/IHRoaXMuZGF0ZXBpY2tlci5nZXRUaW1lcGlja2VySW5zaWRlRGF0ZXBpY2tlckludGVyZmFjZSgpLmJ1dHRvbkFsaWduIDogJ3JpZ2h0J1wiXG4gICAgICAgICAgICBbZGlzYWJsZWRdPVwidGhpcy5kYXRlcGlja2VyLmdldFRpbWVwaWNrZXJJbnNpZGVEYXRlcGlja2VySW50ZXJmYWNlKCkuZGlzYWJsZWRcIlxuICAgICAgICAgICAgW2Zvcm1hdF09XCIyNFwiXG4gICAgICAgICAgICBbbWluXT1cInRoaXMuZGF0ZXBpY2tlci5nZXRUaW1lcGlja2VySW5zaWRlRGF0ZXBpY2tlckludGVyZmFjZSgpLm1pbiA/IHRoaXMuZGF0ZXBpY2tlci5nZXRUaW1lcGlja2VySW5zaWRlRGF0ZXBpY2tlckludGVyZmFjZSgpLm1pbiA6IHVuZGVmaW5lZFwiXG4gICAgICAgICAgICBbbWF4XT1cInRoaXMuZGF0ZXBpY2tlci5nZXRUaW1lcGlja2VySW5zaWRlRGF0ZXBpY2tlckludGVyZmFjZSgpLm1heCA/IHRoaXMuZGF0ZXBpY2tlci5nZXRUaW1lcGlja2VySW5zaWRlRGF0ZXBpY2tlckludGVyZmFjZSgpLm1heCA6IHVuZGVmaW5lZFwiXG4gICAgICAgICAgICBbZGVmYXVsdFRpbWVdPVwidGhpcy5kYXRlcGlja2VyLmdldFRpbWVwaWNrZXJJbnNpZGVEYXRlcGlja2VySW50ZXJmYWNlKCkuZGVmYXVsdFZhbHVlID8gdGhpcy5kYXRlcGlja2VyLmdldFRpbWVwaWNrZXJJbnNpZGVEYXRlcGlja2VySW50ZXJmYWNlKCkuZGVmYXVsdFZhbHVlIDogdW5kZWZpbmVkXCJcbiAgICAgICAgICAgICh0aW1lQ2hhbmdlZCk9XCJ0aGlzLm9uVGltZUNoYW5nZWQoJGV2ZW50KVwiPjwvbmd4LXRpbWVwaWNrZXItZmllbGQ+XG4gICAgICAgICAgICBcbiAgICAgICAgPGlucHV0IG1hdElucHV0IHR5cGU9XCJ0aW1lcGlja2VyXCIgW2hpZGRlbl09XCJ0cnVlXCI+XG4gICAgPC9tYXQtZm9ybS1maWVsZD5cblxuPC9zZWN0aW9uPiJdfQ==