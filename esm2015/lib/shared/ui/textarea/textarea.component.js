import { Component, Input, forwardRef } from "@angular/core";
import { NG_VALUE_ACCESSOR } from "@angular/forms";
// model
import { VALIDATOR_NAMES, } from "../../model/structural/composite/form/form-row-item.model";
import * as i0 from "@angular/core";
import * as i1 from "@angular/flex-layout/flex";
import * as i2 from "@angular/material/form-field";
import * as i3 from "@angular/common";
import * as i4 from "@angular/material/input";
import * as i5 from "@angular/cdk/text-field";
import * as i6 from "@angular/material/icon";
function TextareaComponent_mat_label_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-label");
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r0 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r0.textarea.getLabelName());
} }
function TextareaComponent_mat_icon_5_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-icon", 9);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r2 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r2.textarea.getMatPrefixImgText());
} }
function TextareaComponent_mat_icon_6_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-icon", 10);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r3 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r3.textarea.getMatSuffixImgText());
} }
function TextareaComponent_span_7_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 9);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r4 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1("", ctx_r4.textarea.getTextPrefix(), "\u00A0");
} }
function TextareaComponent_span_8_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 10);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r5 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r5.textarea.getTextSuffix());
} }
function TextareaComponent_mat_hint_9_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-hint", 11);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r6 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r6.textarea.getLeftHintLabel());
} }
function TextareaComponent_mat_hint_10_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-hint", 12);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r7 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r7.textarea.getRightHintLabel());
} }
function TextareaComponent_mat_hint_11_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-hint", 12);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r8 = i0.ɵɵnextContext();
    var tmp_0_0 = null;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate3("", ((tmp_0_0 = ctx_r8.getMinLengthValidator()) == null ? null : tmp_0_0.length) ? ctx_r8.getMinLengthValidator().length + " / " : "", "", (tmp_0_0 = ctx_r8.textarea.getDefaultValue()) == null ? null : tmp_0_0.length, "", ((tmp_0_0 = ctx_r8.getMaxLengthValidator()) == null ? null : tmp_0_0.length) ? " / " + ctx_r8.getMaxLengthValidator().length : "", "");
} }
function TextareaComponent_mat_hint_12_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-hint", 12);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r9 = i0.ɵɵnextContext();
    var tmp_0_0 = null;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate2("", (tmp_0_0 = ctx_r9.textarea.getDefaultValue()) == null ? null : tmp_0_0.length, "", ((tmp_0_0 = ctx_r9.getMinLengthValidator()) == null ? null : tmp_0_0.length) ? " / " + ctx_r9.getMinLengthValidator().length : "", "");
} }
function TextareaComponent_mat_hint_13_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-hint", 12);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r10 = i0.ɵɵnextContext();
    var tmp_0_0 = null;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate2("", (tmp_0_0 = ctx_r10.textarea.getDefaultValue()) == null ? null : tmp_0_0.length, "", ((tmp_0_0 = ctx_r10.getMaxLengthValidator()) == null ? null : tmp_0_0.length) ? " / " + ctx_r10.getMaxLengthValidator().length : "", "");
} }
export class TextareaComponent {
    constructor() {
        this.onChange = () => { };
        this.onTouched = () => { };
        this.onKeyUp = (textarea) => {
            // console.log(textarea.value);
            this.textarea.setDefaultValue(textarea.value);
            this.onChange(this.textarea.getDefaultValue());
            this.onTouched();
        };
        this.getMinLengthValidator = () => {
            let theValidator;
            this.textarea
                .getValidators()
                .forEach((validator) => {
                if (validator.name === VALIDATOR_NAMES.MIN_LENGTH)
                    theValidator = validator;
            });
            return theValidator;
        };
        this.getMaxLengthValidator = () => {
            let theValidator;
            this.textarea
                .getValidators()
                .forEach((validator) => {
                if (validator.name === VALIDATOR_NAMES.MAX_LENGTH)
                    theValidator = validator;
            });
            return theValidator;
        };
    }
    registerOnChange(fn) {
        this.onChange = fn;
    }
    registerOnTouched(fn) {
        this.onTouched = fn;
    }
    writeValue(value) {
        if (value) {
            this.textarea.setDefaultValue(value);
        }
    }
    ngOnInit() {
        // console.log('TextareaComponent init');
    }
    ngOnDestroy() {
        // console.log('TextareaComponent destroyed');
    }
}
TextareaComponent.ɵfac = function TextareaComponent_Factory(t) { return new (t || TextareaComponent)(); };
TextareaComponent.ɵcmp = i0.ɵɵdefineComponent({ type: TextareaComponent, selectors: [["app-textarea"]], inputs: { textarea: "textarea" }, features: [i0.ɵɵProvidersFeature([
            {
                provide: NG_VALUE_ACCESSOR,
                multi: true,
                useExisting: forwardRef(() => TextareaComponent),
            },
        ])], decls: 14, vars: 18, consts: [["fxLayoutAlign", "center center"], [3, "appearance", "hideRequiredMarker"], [4, "ngIf"], ["matInput", "", "cdkTextareaAutosize", "", 3, "type", "placeholder", "value", "disabled", "readonly", "required", "keyup", "blur"], ["textareaField", ""], ["matPrefix", "", 4, "ngIf"], ["matSuffix", "", 4, "ngIf"], ["align", "start", 4, "ngIf"], ["align", "end", 4, "ngIf"], ["matPrefix", ""], ["matSuffix", ""], ["align", "start"], ["align", "end"]], template: function TextareaComponent_Template(rf, ctx) { if (rf & 1) {
        const _r11 = i0.ɵɵgetCurrentView();
        i0.ɵɵelementStart(0, "section", 0);
        i0.ɵɵelementStart(1, "mat-form-field", 1);
        i0.ɵɵtemplate(2, TextareaComponent_mat_label_2_Template, 2, 1, "mat-label", 2);
        i0.ɵɵelementStart(3, "textarea", 3, 4);
        i0.ɵɵlistener("keyup", function TextareaComponent_Template_textarea_keyup_3_listener() { i0.ɵɵrestoreView(_r11); const _r1 = i0.ɵɵreference(4); return ctx.onKeyUp(_r1); })("blur", function TextareaComponent_Template_textarea_blur_3_listener() { i0.ɵɵrestoreView(_r11); const _r1 = i0.ɵɵreference(4); return ctx.onKeyUp(_r1); });
        i0.ɵɵelementEnd();
        i0.ɵɵtemplate(5, TextareaComponent_mat_icon_5_Template, 2, 1, "mat-icon", 5);
        i0.ɵɵtemplate(6, TextareaComponent_mat_icon_6_Template, 2, 1, "mat-icon", 6);
        i0.ɵɵtemplate(7, TextareaComponent_span_7_Template, 2, 1, "span", 5);
        i0.ɵɵtemplate(8, TextareaComponent_span_8_Template, 2, 1, "span", 6);
        i0.ɵɵtemplate(9, TextareaComponent_mat_hint_9_Template, 2, 1, "mat-hint", 7);
        i0.ɵɵtemplate(10, TextareaComponent_mat_hint_10_Template, 2, 1, "mat-hint", 8);
        i0.ɵɵtemplate(11, TextareaComponent_mat_hint_11_Template, 2, 3, "mat-hint", 8);
        i0.ɵɵtemplate(12, TextareaComponent_mat_hint_12_Template, 2, 2, "mat-hint", 8);
        i0.ɵɵtemplate(13, TextareaComponent_mat_hint_13_Template, 2, 2, "mat-hint", 8);
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("appearance", ctx.textarea.getAppearance())("hideRequiredMarker", ctx.textarea.getRequired().hideRequiredMarker);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.textarea.getLabelName());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("type", ctx.textarea.getFormFieldInputType())("placeholder", ctx.textarea.getPlaceholder() ? ctx.textarea.getPlaceholder() : "")("value", ctx.textarea.getDefaultValue())("disabled", ctx.textarea.getDisabled())("readonly", ctx.textarea.getReadonly())("required", ctx.textarea.getRequired().required);
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("ngIf", ctx.textarea.getMatPrefixImgText());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.textarea.getMatSuffixImgText());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.textarea.getTextPrefix());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.textarea.getTextSuffix());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.textarea.getLeftHintLabel());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.textarea.getRightHintLabel());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.textarea.getShowHintAboutMinMaxLength() && ctx.getMinLengthValidator() && ctx.getMaxLengthValidator());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.textarea.getShowHintAboutMinMaxLength() && ctx.getMinLengthValidator() && !ctx.getMaxLengthValidator());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.textarea.getShowHintAboutMinMaxLength() && !ctx.getMinLengthValidator() && ctx.getMaxLengthValidator());
    } }, directives: [i1.DefaultLayoutAlignDirective, i2.MatFormField, i3.NgIf, i4.MatInput, i5.CdkTextareaAutosize, i2.MatLabel, i6.MatIcon, i2.MatPrefix, i2.MatSuffix, i2.MatHint], styles: ["", ".cursor[_ngcontent-%COMP%] {\n        cursor: pointer;\n    }\n\n    input[type=\"file\"][_ngcontent-%COMP%] {\n        display: none;\n    }\n\n    h3[_ngcontent-%COMP%] {\n        font-size: inherit;\n        margin: 0;\n        font-weight: inherit;\n    }\n\n    mat-form-field[_ngcontent-%COMP%] {\n        min-width: 250px;\n    }"] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(TextareaComponent, [{
        type: Component,
        args: [{
                selector: "app-textarea",
                templateUrl: "./textarea.component.html",
                styleUrls: ["./textarea.component.css"],
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        multi: true,
                        useExisting: forwardRef(() => TextareaComponent),
                    },
                ],
            }]
    }], function () { return []; }, { textarea: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGV4dGFyZWEuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXktYW5ndWxhci1mb3JtLyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC91aS90ZXh0YXJlYS90ZXh0YXJlYS5jb21wb25lbnQudHMiLCJsaWIvc2hhcmVkL3VpL3RleHRhcmVhL3RleHRhcmVhLmNvbXBvbmVudC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQXFCLEtBQUssRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDaEYsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFFbkQsUUFBUTtBQUNSLE9BQU8sRUFFTCxlQUFlLEdBQ2hCLE1BQU0sMkRBQTJELENBQUM7Ozs7Ozs7OztJQ3NCbkQsaUNBQ3lDO0lBQUEsWUFBa0M7SUFBQSxpQkFBWTs7O0lBQTlDLGVBQWtDO0lBQWxDLG9EQUFrQzs7O0lBZTNFLG1DQUVjO0lBQUEsWUFBeUM7SUFBQSxpQkFBVzs7O0lBQXBELGVBQXlDO0lBQXpDLDJEQUF5Qzs7O0lBRXZELG9DQUVjO0lBQUEsWUFBeUM7SUFBQSxpQkFBVzs7O0lBQXBELGVBQXlDO0lBQXpDLDJEQUF5Qzs7O0lBRXZELCtCQUVjO0lBQUEsWUFBeUM7SUFBQSxpQkFBTzs7O0lBQWhELGVBQXlDO0lBQXpDLG9FQUF5Qzs7O0lBRXZELGdDQUVjO0lBQUEsWUFBbUM7SUFBQSxpQkFBTzs7O0lBQTFDLGVBQW1DO0lBQW5DLHFEQUFtQzs7O0lBRWpELG9DQUVrQjtJQUFBLFlBQXNDO0lBQUEsaUJBQVc7OztJQUFqRCxlQUFzQztJQUF0Qyx3REFBc0M7OztJQUV4RCxvQ0FFZ0I7SUFBQSxZQUF1QztJQUFBLGlCQUFXOzs7SUFBbEQsZUFBdUM7SUFBdkMseURBQXVDOzs7SUFHdkQsb0NBRWdCO0lBQUEsWUFBb087SUFBQSxpQkFBVzs7OztJQUEvTyxlQUFvTztJQUFwTywwWEFBb087OztJQUVwUCxvQ0FFZ0I7SUFBQSxZQUF5STtJQUFBLGlCQUFXOzs7O0lBQXBKLGVBQXlJO0lBQXpJLG1QQUF5STs7O0lBRXpKLG9DQUVnQjtJQUFBLFlBQXdJO0lBQUEsaUJBQVc7Ozs7SUFBbkosZUFBd0k7SUFBeEksc1BBQXdJOztBRDFEeEssTUFBTSxPQUFPLGlCQUFpQjtJQU01QjtRQUhPLGFBQVEsR0FBUSxHQUFHLEVBQUUsR0FBRSxDQUFDLENBQUM7UUFDekIsY0FBUyxHQUFRLEdBQUcsRUFBRSxHQUFFLENBQUMsQ0FBQztRQUlqQyxZQUFPLEdBQUcsQ0FBQyxRQUEwQixFQUFFLEVBQUU7WUFDdkMsK0JBQStCO1lBQy9CLElBQUksQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUM5QyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsZUFBZSxFQUFFLENBQUMsQ0FBQztZQUMvQyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDbkIsQ0FBQyxDQUFDO1FBZ0JGLDBCQUFxQixHQUFHLEdBQWtDLEVBQUU7WUFDMUQsSUFBSSxZQUFZLENBQUM7WUFDakIsSUFBSSxDQUFDLFFBQVE7aUJBQ1YsYUFBYSxFQUFFO2lCQUNmLE9BQU8sQ0FBQyxDQUFDLFNBQXdDLEVBQUUsRUFBRTtnQkFDcEQsSUFBSSxTQUFTLENBQUMsSUFBSSxLQUFLLGVBQWUsQ0FBQyxVQUFVO29CQUMvQyxZQUFZLEdBQUcsU0FBUyxDQUFDO1lBQzdCLENBQUMsQ0FBQyxDQUFDO1lBQ0wsT0FBTyxZQUFZLENBQUM7UUFDdEIsQ0FBQyxDQUFDO1FBRUYsMEJBQXFCLEdBQUcsR0FBa0MsRUFBRTtZQUMxRCxJQUFJLFlBQVksQ0FBQztZQUNqQixJQUFJLENBQUMsUUFBUTtpQkFDVixhQUFhLEVBQUU7aUJBQ2YsT0FBTyxDQUFDLENBQUMsU0FBd0MsRUFBRSxFQUFFO2dCQUNwRCxJQUFJLFNBQVMsQ0FBQyxJQUFJLEtBQUssZUFBZSxDQUFDLFVBQVU7b0JBQy9DLFlBQVksR0FBRyxTQUFTLENBQUM7WUFDN0IsQ0FBQyxDQUFDLENBQUM7WUFDTCxPQUFPLFlBQVksQ0FBQztRQUN0QixDQUFDLENBQUM7SUEzQ2EsQ0FBQztJQVNoQixnQkFBZ0IsQ0FBQyxFQUFFO1FBQ2pCLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO0lBQ3JCLENBQUM7SUFFRCxpQkFBaUIsQ0FBQyxFQUFFO1FBQ2xCLElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO0lBQ3RCLENBQUM7SUFFRCxVQUFVLENBQUMsS0FBSztRQUNkLElBQUksS0FBSyxFQUFFO1lBQ1QsSUFBSSxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDdEM7SUFDSCxDQUFDO0lBd0JELFFBQVE7UUFDTix5Q0FBeUM7SUFDM0MsQ0FBQztJQUVELFdBQVc7UUFDVCw4Q0FBOEM7SUFDaEQsQ0FBQzs7a0ZBekRVLGlCQUFpQjtzREFBakIsaUJBQWlCLG9HQVJqQjtZQUNUO2dCQUNFLE9BQU8sRUFBRSxpQkFBaUI7Z0JBQzFCLEtBQUssRUFBRSxJQUFJO2dCQUNYLFdBQVcsRUFBRSxVQUFVLENBQUMsR0FBRyxFQUFFLENBQUMsaUJBQWlCLENBQUM7YUFDakQ7U0FDRjs7UUNFSCxrQ0FHUTtRQUFBLHlDQUlRO1FBQUEsOEVBQ3lDO1FBRXpDLHNDQVdtQztRQU4vQix1SkFBUyxnQkFBMkIsSUFBQyx3SUFDN0IsZ0JBQTJCLElBREU7UUFNakIsaUJBQVc7UUFFbkMsNEVBRWM7UUFFZCw0RUFFYztRQUVkLG9FQUVjO1FBRWQsb0VBRWM7UUFFZCw0RUFFa0I7UUFFbEIsOEVBRWdCO1FBR2hCLDhFQUVnQjtRQUVoQiw4RUFFZ0I7UUFFaEIsOEVBRWdCO1FBRXhCLGlCQUFpQjtRQUV6QixpQkFBVTs7UUExREUsZUFBNEM7UUFBNUMseURBQTRDLHFFQUFBO1FBSXBDLGVBQW9DO1FBQXBDLGtEQUFvQztRQUlwQyxlQUE4QztRQUE5QywyREFBOEMsbUZBQUEseUNBQUEsd0NBQUEsd0NBQUEsaURBQUE7UUFZOUMsZUFBMkM7UUFBM0MseURBQTJDO1FBSTNDLGVBQTJDO1FBQTNDLHlEQUEyQztRQUkzQyxlQUFxQztRQUFyQyxtREFBcUM7UUFJckMsZUFBcUM7UUFBckMsbURBQXFDO1FBSXJDLGVBQXdDO1FBQXhDLHNEQUF3QztRQUl4QyxlQUF5QztRQUF6Qyx1REFBeUM7UUFLekMsZUFBb0g7UUFBcEgsZ0lBQW9IO1FBSXBILGVBQXFIO1FBQXJILGlJQUFxSDtRQUlySCxlQUFxSDtRQUFySCxpSUFBcUg7O2tERHpENUgsaUJBQWlCO2NBWjdCLFNBQVM7ZUFBQztnQkFDVCxRQUFRLEVBQUUsY0FBYztnQkFDeEIsV0FBVyxFQUFFLDJCQUEyQjtnQkFDeEMsU0FBUyxFQUFFLENBQUMsMEJBQTBCLENBQUM7Z0JBQ3ZDLFNBQVMsRUFBRTtvQkFDVDt3QkFDRSxPQUFPLEVBQUUsaUJBQWlCO3dCQUMxQixLQUFLLEVBQUUsSUFBSTt3QkFDWCxXQUFXLEVBQUUsVUFBVSxDQUFDLEdBQUcsRUFBRSxDQUFDLGlCQUFpQixDQUFDO3FCQUNqRDtpQkFDRjthQUNGOztrQkFFRSxLQUFLIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIE9uRGVzdHJveSwgSW5wdXQsIGZvcndhcmRSZWYgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgTkdfVkFMVUVfQUNDRVNTT1IgfSBmcm9tIFwiQGFuZ3VsYXIvZm9ybXNcIjtcblxuLy8gbW9kZWxcbmltcG9ydCB7XG4gIEZvcm1Sb3dJdGVtVmFsaWRhdG9ySW50ZXJmYWNlLFxuICBWQUxJREFUT1JfTkFNRVMsXG59IGZyb20gXCIuLi8uLi9tb2RlbC9zdHJ1Y3R1cmFsL2NvbXBvc2l0ZS9mb3JtL2Zvcm0tcm93LWl0ZW0ubW9kZWxcIjtcbmltcG9ydCB7IFRleHRhcmVhIH0gZnJvbSBcIi4uLy4uL21vZGVsL3N0cnVjdHVyYWwvY29tcG9zaXRlL2Zvcm0vdGV4dGFyZWEvdGV4dGFyZWEubW9kZWxcIjtcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiBcImFwcC10ZXh0YXJlYVwiLFxuICB0ZW1wbGF0ZVVybDogXCIuL3RleHRhcmVhLmNvbXBvbmVudC5odG1sXCIsXG4gIHN0eWxlVXJsczogW1wiLi90ZXh0YXJlYS5jb21wb25lbnQuY3NzXCJdLFxuICBwcm92aWRlcnM6IFtcbiAgICB7XG4gICAgICBwcm92aWRlOiBOR19WQUxVRV9BQ0NFU1NPUixcbiAgICAgIG11bHRpOiB0cnVlLFxuICAgICAgdXNlRXhpc3Rpbmc6IGZvcndhcmRSZWYoKCkgPT4gVGV4dGFyZWFDb21wb25lbnQpLFxuICAgIH0sXG4gIF0sXG59KVxuZXhwb3J0IGNsYXNzIFRleHRhcmVhQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xuICBASW5wdXQoKSB0ZXh0YXJlYTogVGV4dGFyZWE7XG5cbiAgcHVibGljIG9uQ2hhbmdlOiBhbnkgPSAoKSA9PiB7fTtcbiAgcHVibGljIG9uVG91Y2hlZDogYW55ID0gKCkgPT4ge307XG5cbiAgY29uc3RydWN0b3IoKSB7fVxuXG4gIG9uS2V5VXAgPSAodGV4dGFyZWE6IEhUTUxJbnB1dEVsZW1lbnQpID0+IHtcbiAgICAvLyBjb25zb2xlLmxvZyh0ZXh0YXJlYS52YWx1ZSk7XG4gICAgdGhpcy50ZXh0YXJlYS5zZXREZWZhdWx0VmFsdWUodGV4dGFyZWEudmFsdWUpO1xuICAgIHRoaXMub25DaGFuZ2UodGhpcy50ZXh0YXJlYS5nZXREZWZhdWx0VmFsdWUoKSk7XG4gICAgdGhpcy5vblRvdWNoZWQoKTtcbiAgfTtcblxuICByZWdpc3Rlck9uQ2hhbmdlKGZuKSB7XG4gICAgdGhpcy5vbkNoYW5nZSA9IGZuO1xuICB9XG5cbiAgcmVnaXN0ZXJPblRvdWNoZWQoZm4pIHtcbiAgICB0aGlzLm9uVG91Y2hlZCA9IGZuO1xuICB9XG5cbiAgd3JpdGVWYWx1ZSh2YWx1ZSkge1xuICAgIGlmICh2YWx1ZSkge1xuICAgICAgdGhpcy50ZXh0YXJlYS5zZXREZWZhdWx0VmFsdWUodmFsdWUpO1xuICAgIH1cbiAgfVxuXG4gIGdldE1pbkxlbmd0aFZhbGlkYXRvciA9ICgpOiBGb3JtUm93SXRlbVZhbGlkYXRvckludGVyZmFjZSA9PiB7XG4gICAgbGV0IHRoZVZhbGlkYXRvcjtcbiAgICB0aGlzLnRleHRhcmVhXG4gICAgICAuZ2V0VmFsaWRhdG9ycygpXG4gICAgICAuZm9yRWFjaCgodmFsaWRhdG9yOiBGb3JtUm93SXRlbVZhbGlkYXRvckludGVyZmFjZSkgPT4ge1xuICAgICAgICBpZiAodmFsaWRhdG9yLm5hbWUgPT09IFZBTElEQVRPUl9OQU1FUy5NSU5fTEVOR1RIKVxuICAgICAgICAgIHRoZVZhbGlkYXRvciA9IHZhbGlkYXRvcjtcbiAgICAgIH0pO1xuICAgIHJldHVybiB0aGVWYWxpZGF0b3I7XG4gIH07XG5cbiAgZ2V0TWF4TGVuZ3RoVmFsaWRhdG9yID0gKCk6IEZvcm1Sb3dJdGVtVmFsaWRhdG9ySW50ZXJmYWNlID0+IHtcbiAgICBsZXQgdGhlVmFsaWRhdG9yO1xuICAgIHRoaXMudGV4dGFyZWFcbiAgICAgIC5nZXRWYWxpZGF0b3JzKClcbiAgICAgIC5mb3JFYWNoKCh2YWxpZGF0b3I6IEZvcm1Sb3dJdGVtVmFsaWRhdG9ySW50ZXJmYWNlKSA9PiB7XG4gICAgICAgIGlmICh2YWxpZGF0b3IubmFtZSA9PT0gVkFMSURBVE9SX05BTUVTLk1BWF9MRU5HVEgpXG4gICAgICAgICAgdGhlVmFsaWRhdG9yID0gdmFsaWRhdG9yO1xuICAgICAgfSk7XG4gICAgcmV0dXJuIHRoZVZhbGlkYXRvcjtcbiAgfTtcblxuICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgICAvLyBjb25zb2xlLmxvZygnVGV4dGFyZWFDb21wb25lbnQgaW5pdCcpO1xuICB9XG5cbiAgbmdPbkRlc3Ryb3koKTogdm9pZCB7XG4gICAgLy8gY29uc29sZS5sb2coJ1RleHRhcmVhQ29tcG9uZW50IGRlc3Ryb3llZCcpO1xuICB9XG59XG4iLCI8c3R5bGU+XG5cbiAgICAuY3Vyc29yIHtcbiAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIH1cblxuICAgIGlucHV0W3R5cGU9XCJmaWxlXCJdIHtcbiAgICAgICAgZGlzcGxheTogbm9uZTtcbiAgICB9XG5cbiAgICBoMyB7XG4gICAgICAgIGZvbnQtc2l6ZTogaW5oZXJpdDtcbiAgICAgICAgbWFyZ2luOiAwO1xuICAgICAgICBmb250LXdlaWdodDogaW5oZXJpdDtcbiAgICB9XG5cbiAgICBtYXQtZm9ybS1maWVsZCB7XG4gICAgICAgIG1pbi13aWR0aDogMjUwcHg7XG4gICAgfVxuXG48L3N0eWxlPlxuICAgIFxuPHNlY3Rpb25cbiAgICBmeExheW91dEFsaWduPVwiY2VudGVyIGNlbnRlclwiPlxuXG4gICAgICAgIDxtYXQtZm9ybS1maWVsZCBcbiAgICAgICAgICAgIFthcHBlYXJhbmNlXT1cInRoaXMudGV4dGFyZWEuZ2V0QXBwZWFyYW5jZSgpXCJcbiAgICAgICAgICAgIFtoaWRlUmVxdWlyZWRNYXJrZXJdPVwidGhpcy50ZXh0YXJlYS5nZXRSZXF1aXJlZCgpLmhpZGVSZXF1aXJlZE1hcmtlclwiPlxuXG4gICAgICAgICAgICAgICAgPG1hdC1sYWJlbFxuICAgICAgICAgICAgICAgICAgICAqbmdJZj1cInRoaXMudGV4dGFyZWEuZ2V0TGFiZWxOYW1lKClcIj57eyB0aGlzLnRleHRhcmVhLmdldExhYmVsTmFtZSgpIH19PC9tYXQtbGFiZWw+XG5cbiAgICAgICAgICAgICAgICA8dGV4dGFyZWEgXG4gICAgICAgICAgICAgICAgICAgIG1hdElucHV0XG4gICAgICAgICAgICAgICAgICAgIFt0eXBlXT1cInRoaXMudGV4dGFyZWEuZ2V0Rm9ybUZpZWxkSW5wdXRUeXBlKClcIlxuICAgICAgICAgICAgICAgICAgICBbcGxhY2Vob2xkZXJdPVwidGhpcy50ZXh0YXJlYS5nZXRQbGFjZWhvbGRlcigpID8gdGhpcy50ZXh0YXJlYS5nZXRQbGFjZWhvbGRlcigpIDogJydcIlxuICAgICAgICAgICAgICAgICAgICBbdmFsdWVdPVwidGhpcy50ZXh0YXJlYS5nZXREZWZhdWx0VmFsdWUoKVwiXG4gICAgICAgICAgICAgICAgICAgIChrZXl1cCk9XCJ0aGlzLm9uS2V5VXAodGV4dGFyZWFGaWVsZClcIlxuICAgICAgICAgICAgICAgICAgICAoYmx1cik9XCJ0aGlzLm9uS2V5VXAodGV4dGFyZWFGaWVsZClcIlxuICAgICAgICAgICAgICAgICAgICBbZGlzYWJsZWRdPVwidGhpcy50ZXh0YXJlYS5nZXREaXNhYmxlZCgpXCJcbiAgICAgICAgICAgICAgICAgICAgW3JlYWRvbmx5XT1cInRoaXMudGV4dGFyZWEuZ2V0UmVhZG9ubHkoKVwiXG4gICAgICAgICAgICAgICAgICAgIFtyZXF1aXJlZF09XCJ0aGlzLnRleHRhcmVhLmdldFJlcXVpcmVkKCkucmVxdWlyZWRcIlxuICAgICAgICAgICAgICAgICAgICAjdGV4dGFyZWFGaWVsZFxuICAgICAgICAgICAgICAgICAgICBjZGtUZXh0YXJlYUF1dG9zaXplPjwvdGV4dGFyZWE+XG5cbiAgICAgICAgICAgICAgICA8bWF0LWljb24gXG4gICAgICAgICAgICAgICAgICAgICpuZ0lmPVwidGhpcy50ZXh0YXJlYS5nZXRNYXRQcmVmaXhJbWdUZXh0KClcIlxuICAgICAgICAgICAgICAgICAgICBtYXRQcmVmaXg+e3sgdGhpcy50ZXh0YXJlYS5nZXRNYXRQcmVmaXhJbWdUZXh0KCkgfX08L21hdC1pY29uPlxuXG4gICAgICAgICAgICAgICAgPG1hdC1pY29uIFxuICAgICAgICAgICAgICAgICAgICAqbmdJZj1cInRoaXMudGV4dGFyZWEuZ2V0TWF0U3VmZml4SW1nVGV4dCgpXCJcbiAgICAgICAgICAgICAgICAgICAgbWF0U3VmZml4Pnt7IHRoaXMudGV4dGFyZWEuZ2V0TWF0U3VmZml4SW1nVGV4dCgpIH19PC9tYXQtaWNvbj5cblxuICAgICAgICAgICAgICAgIDxzcGFuIFxuICAgICAgICAgICAgICAgICAgICAqbmdJZj1cInRoaXMudGV4dGFyZWEuZ2V0VGV4dFByZWZpeCgpXCJcbiAgICAgICAgICAgICAgICAgICAgbWF0UHJlZml4Pnt7IHRoaXMudGV4dGFyZWEuZ2V0VGV4dFByZWZpeCgpIH19Jm5ic3A7PC9zcGFuPlxuXG4gICAgICAgICAgICAgICAgPHNwYW4gXG4gICAgICAgICAgICAgICAgICAgICpuZ0lmPVwidGhpcy50ZXh0YXJlYS5nZXRUZXh0U3VmZml4KClcIlxuICAgICAgICAgICAgICAgICAgICBtYXRTdWZmaXg+e3sgdGhpcy50ZXh0YXJlYS5nZXRUZXh0U3VmZml4KCkgfX08L3NwYW4+XG5cbiAgICAgICAgICAgICAgICA8bWF0LWhpbnQgXG4gICAgICAgICAgICAgICAgICAgICpuZ0lmPVwidGhpcy50ZXh0YXJlYS5nZXRMZWZ0SGludExhYmVsKClcIlxuICAgICAgICAgICAgICAgICAgICBhbGlnbj1cInN0YXJ0XCI+e3sgdGhpcy50ZXh0YXJlYS5nZXRMZWZ0SGludExhYmVsKCkgfX08L21hdC1oaW50PlxuXG4gICAgICAgICAgICAgICAgPG1hdC1oaW50XG4gICAgICAgICAgICAgICAgICAgICpuZ0lmPVwidGhpcy50ZXh0YXJlYS5nZXRSaWdodEhpbnRMYWJlbCgpXCJcbiAgICAgICAgICAgICAgICAgICAgYWxpZ249XCJlbmRcIj57eyB0aGlzLnRleHRhcmVhLmdldFJpZ2h0SGludExhYmVsKCkgfX08L21hdC1oaW50PlxuXG4gICAgICAgICAgICAgICAgPCEtLSBpZiBmaWVsZCBoYXMgU2hvd0hpbnRBYm91dE1pbk1heExlbmd0aCBzZXQgdG8gdHJ1ZSAtLT5cbiAgICAgICAgICAgICAgICA8bWF0LWhpbnRcbiAgICAgICAgICAgICAgICAgICAgKm5nSWY9XCJ0aGlzLnRleHRhcmVhLmdldFNob3dIaW50QWJvdXRNaW5NYXhMZW5ndGgoKSAmJiB0aGlzLmdldE1pbkxlbmd0aFZhbGlkYXRvcigpICYmIHRoaXMuZ2V0TWF4TGVuZ3RoVmFsaWRhdG9yKClcIlxuICAgICAgICAgICAgICAgICAgICBhbGlnbj1cImVuZFwiPnt7IHRoaXMuZ2V0TWluTGVuZ3RoVmFsaWRhdG9yKCk/Lmxlbmd0aCA/IHRoaXMuZ2V0TWluTGVuZ3RoVmFsaWRhdG9yKCkubGVuZ3RoICsgXCIgLyBcIiA6ICcnfX17eyB0aGlzLnRleHRhcmVhLmdldERlZmF1bHRWYWx1ZSgpPy5sZW5ndGggfX17eyB0aGlzLmdldE1heExlbmd0aFZhbGlkYXRvcigpPy5sZW5ndGggPyBcIiAvIFwiICsgdGhpcy5nZXRNYXhMZW5ndGhWYWxpZGF0b3IoKS5sZW5ndGg6ICcnfX08L21hdC1oaW50PlxuXG4gICAgICAgICAgICAgICAgPG1hdC1oaW50XG4gICAgICAgICAgICAgICAgICAgICpuZ0lmPVwidGhpcy50ZXh0YXJlYS5nZXRTaG93SGludEFib3V0TWluTWF4TGVuZ3RoKCkgJiYgdGhpcy5nZXRNaW5MZW5ndGhWYWxpZGF0b3IoKSAmJiAhdGhpcy5nZXRNYXhMZW5ndGhWYWxpZGF0b3IoKVwiXG4gICAgICAgICAgICAgICAgICAgIGFsaWduPVwiZW5kXCI+e3sgdGhpcy50ZXh0YXJlYS5nZXREZWZhdWx0VmFsdWUoKT8ubGVuZ3RoIH19e3sgdGhpcy5nZXRNaW5MZW5ndGhWYWxpZGF0b3IoKT8ubGVuZ3RoID8gXCIgLyBcIiArIHRoaXMuZ2V0TWluTGVuZ3RoVmFsaWRhdG9yKCkubGVuZ3RoIDogJyd9fTwvbWF0LWhpbnQ+XG5cbiAgICAgICAgICAgICAgICA8bWF0LWhpbnRcbiAgICAgICAgICAgICAgICAgICAgKm5nSWY9XCJ0aGlzLnRleHRhcmVhLmdldFNob3dIaW50QWJvdXRNaW5NYXhMZW5ndGgoKSAmJiAhdGhpcy5nZXRNaW5MZW5ndGhWYWxpZGF0b3IoKSAmJiB0aGlzLmdldE1heExlbmd0aFZhbGlkYXRvcigpXCJcbiAgICAgICAgICAgICAgICAgICAgYWxpZ249XCJlbmRcIj57eyB0aGlzLnRleHRhcmVhLmdldERlZmF1bHRWYWx1ZSgpPy5sZW5ndGggfX17eyB0aGlzLmdldE1heExlbmd0aFZhbGlkYXRvcigpPy5sZW5ndGggPyBcIiAvIFwiICsgdGhpcy5nZXRNYXhMZW5ndGhWYWxpZGF0b3IoKS5sZW5ndGg6ICcnfX08L21hdC1oaW50PlxuXG4gICAgICAgIDwvbWF0LWZvcm0tZmllbGQ+XG5cbjwvc2VjdGlvbj4iXX0=