import { Component, Input, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import * as i0 from "@angular/core";
import * as i1 from "@angular/flex-layout/flex";
import * as i2 from "@angular/common";
import * as i3 from "@angular/material/radio";
import * as i4 from "@angular/forms";
function RadioButtonComponent_label_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "label", 4);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r0 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r0.radioButtonGroup.getLabel().context);
} }
function RadioButtonComponent_mat_radio_button_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-radio-button", 5);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const option_r3 = ctx.$implicit;
    i0.ɵɵproperty("value", option_r3.value);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(option_r3.name);
} }
function RadioButtonComponent_label_4_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "label", 4);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r2 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r2.radioButtonGroup.getLabel().context);
} }
// za vise o ControlValueAccessor, pogledaj:
// https://coryrylan.com/blog/angular-custom-form-controls-with-reactive-forms-and-ngmodel
export class RadioButtonComponent {
    constructor() {
        this.value = false;
        this.onChange = () => { };
        this.onTouched = () => { };
        this.onNgModelChange = () => {
            this.radioButtonGroup.setDefaultValue(this.value);
            this.onChange(this.value);
            this.onTouched();
            // console.log(this.radioButtonGroup);
        };
    }
    registerOnChange(fn) {
        this.onChange = fn;
    }
    registerOnTouched(fn) {
        this.onTouched = fn;
    }
    writeValue(value) {
        if (value) {
            this.value = value;
        }
    }
    ngOnInit() {
        // console.log(this.radioButtonGroup);
        this.value = this.radioButtonGroup.getDefaultValue();
        // console.log('RadioButtonComponent init');
    }
    ngOnDestroy() {
        // console.log('RadioButtonComponent destroyed');
    }
}
RadioButtonComponent.ɵfac = function RadioButtonComponent_Factory(t) { return new (t || RadioButtonComponent)(); };
RadioButtonComponent.ɵcmp = i0.ɵɵdefineComponent({ type: RadioButtonComponent, selectors: [["app-radio-button"]], inputs: { radioButtonGroup: "radioButtonGroup" }, features: [i0.ɵɵProvidersFeature([
            {
                provide: NG_VALUE_ACCESSOR,
                multi: true,
                useExisting: forwardRef(() => RadioButtonComponent),
            },
        ])], decls: 5, vars: 7, consts: [["fxLayout", "column", "fxLayoutAlign", "center center", "fxLayoutGap", "15px"], ["id", "radio-group-label", 4, "ngIf"], ["aria-labelledby", "radio-group-label", "fxLayoutAlign", "start start", "fxLayoutGap", "20px", 3, "ngModel", "disabled", "labelPosition", "fxLayout", "ngModelChange"], [3, "value", 4, "ngFor", "ngForOf"], ["id", "radio-group-label"], [3, "value"]], template: function RadioButtonComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "section", 0);
        i0.ɵɵtemplate(1, RadioButtonComponent_label_1_Template, 2, 1, "label", 1);
        i0.ɵɵelementStart(2, "mat-radio-group", 2);
        i0.ɵɵlistener("ngModelChange", function RadioButtonComponent_Template_mat_radio_group_ngModelChange_2_listener($event) { return ctx.value = $event; })("ngModelChange", function RadioButtonComponent_Template_mat_radio_group_ngModelChange_2_listener() { return ctx.onNgModelChange(); });
        i0.ɵɵtemplate(3, RadioButtonComponent_mat_radio_button_3_Template, 2, 2, "mat-radio-button", 3);
        i0.ɵɵelementEnd();
        i0.ɵɵtemplate(4, RadioButtonComponent_label_4_Template, 2, 1, "label", 1);
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.radioButtonGroup.getLabel().position != "bellow");
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngModel", ctx.value)("disabled", ctx.radioButtonGroup.getDisabled())("labelPosition", ctx.radioButtonGroup.getLabelPosition())("fxLayout", ctx.radioButtonGroup.getDirection());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngForOf", ctx.radioButtonGroup.getChildren());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.radioButtonGroup.getLabel().position == "bellow");
    } }, directives: [i1.DefaultLayoutDirective, i1.DefaultLayoutAlignDirective, i1.DefaultLayoutGapDirective, i2.NgIf, i3.MatRadioGroup, i4.NgControlStatus, i4.NgModel, i2.NgForOf, i3.MatRadioButton], styles: ["", ".no-margin[_ngcontent-%COMP%] {\n        margin: 0;\n    }"] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(RadioButtonComponent, [{
        type: Component,
        args: [{
                selector: 'app-radio-button',
                templateUrl: './radio-button.component.html',
                styleUrls: ['./radio-button.component.css'],
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        multi: true,
                        useExisting: forwardRef(() => RadioButtonComponent),
                    },
                ],
            }]
    }], function () { return []; }, { radioButtonGroup: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmFkaW8tYnV0dG9uLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215LWFuZ3VsYXItZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvdWkvcmFkaW8tYnV0dG9uL3JhZGlvLWJ1dHRvbi5jb21wb25lbnQudHMiLCJsaWIvc2hhcmVkL3VpL3JhZGlvLWJ1dHRvbi9yYWRpby1idXR0b24uY29tcG9uZW50Lmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxLQUFLLEVBQWEsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ2hGLE9BQU8sRUFBd0IsaUJBQWlCLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQzs7Ozs7OztJQ1lqRSxnQ0FFMkI7SUFBQSxZQUE4QztJQUFBLGlCQUFROzs7SUFBdEQsZUFBOEM7SUFBOUMsZ0VBQThDOzs7SUFZakUsMkNBRTJCO0lBQUEsWUFBaUI7SUFBQSxpQkFBbUI7OztJQUEzRCx1Q0FBc0I7SUFBQyxlQUFpQjtJQUFqQixvQ0FBaUI7OztJQUlwRCxnQ0FFMkI7SUFBQSxZQUE4QztJQUFBLGlCQUFROzs7SUFBdEQsZUFBOEM7SUFBOUMsZ0VBQThDOztBRDdCakYsNENBQTRDO0FBQzVDLDBGQUEwRjtBQWExRixNQUFNLE9BQU8sb0JBQW9CO0lBUS9CO1FBTE8sVUFBSyxHQUE4QixLQUFLLENBQUM7UUFFekMsYUFBUSxHQUFRLEdBQUcsRUFBRSxHQUFFLENBQUMsQ0FBQztRQUN6QixjQUFTLEdBQVEsR0FBRyxFQUFFLEdBQUUsQ0FBQyxDQUFDO1FBSWpDLG9CQUFlLEdBQUcsR0FBUyxFQUFFO1lBQzNCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ2xELElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFCLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztZQUVqQixzQ0FBc0M7UUFDeEMsQ0FBQyxDQUFDO0lBUmEsQ0FBQztJQVVoQixnQkFBZ0IsQ0FBQyxFQUFFO1FBQ2pCLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO0lBQ3JCLENBQUM7SUFFRCxpQkFBaUIsQ0FBQyxFQUFFO1FBQ2xCLElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO0lBQ3RCLENBQUM7SUFFRCxVQUFVLENBQUMsS0FBSztRQUNkLElBQUksS0FBSyxFQUFFO1lBQ1QsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7U0FDcEI7SUFDSCxDQUFDO0lBRUQsUUFBUTtRQUNOLHNDQUFzQztRQUV0QyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUVyRCw0Q0FBNEM7SUFDOUMsQ0FBQztJQUVELFdBQVc7UUFDVCxpREFBaUQ7SUFDbkQsQ0FBQzs7d0ZBMUNVLG9CQUFvQjt5REFBcEIsb0JBQW9CLHdIQVJwQjtZQUNUO2dCQUNFLE9BQU8sRUFBRSxpQkFBaUI7Z0JBQzFCLEtBQUssRUFBRSxJQUFJO2dCQUNYLFdBQVcsRUFBRSxVQUFVLENBQUMsR0FBRyxFQUFFLENBQUMsb0JBQW9CLENBQUM7YUFDcEQ7U0FDRjtRQ1ZILGtDQUtRO1FBQUEseUVBRTJCO1FBRTNCLDBDQVVRO1FBUkosc0pBQXdCLDZHQUNQLHFCQUFzQixJQURmO1FBUXBCLCtGQUUyQjtRQUVuQyxpQkFBa0I7UUFFbEIseUVBRTJCO1FBRW5DLGlCQUFVOztRQXZCRSxlQUE2RDtRQUE3RCwyRUFBNkQ7UUFLN0QsZUFBd0I7UUFBeEIsbUNBQXdCLGdEQUFBLDBEQUFBLGlEQUFBO1FBU2hCLGVBQTBEO1FBQTFELDREQUEwRDtRQU1sRSxlQUE2RDtRQUE3RCwyRUFBNkQ7O2tERGQ1RCxvQkFBb0I7Y0FaaEMsU0FBUztlQUFDO2dCQUNULFFBQVEsRUFBRSxrQkFBa0I7Z0JBQzVCLFdBQVcsRUFBRSwrQkFBK0I7Z0JBQzVDLFNBQVMsRUFBRSxDQUFDLDhCQUE4QixDQUFDO2dCQUMzQyxTQUFTLEVBQUU7b0JBQ1Q7d0JBQ0UsT0FBTyxFQUFFLGlCQUFpQjt3QkFDMUIsS0FBSyxFQUFFLElBQUk7d0JBQ1gsV0FBVyxFQUFFLFVBQVUsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxvQkFBb0IsQ0FBQztxQkFDcEQ7aUJBQ0Y7YUFDRjs7a0JBR0UsS0FBSyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCwgT25EZXN0cm95LCBmb3J3YXJkUmVmIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDb250cm9sVmFsdWVBY2Nlc3NvciwgTkdfVkFMVUVfQUNDRVNTT1IgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5cbi8vIG1vZGVsXG5pbXBvcnQgeyBSYWRpb0J1dHRvbkdyb3VwIH0gZnJvbSAnLi4vLi4vbW9kZWwvc3RydWN0dXJhbC9jb21wb3NpdGUvZm9ybS9yYWRpby1idXR0b24tZ3JvdXAvcmFkaW8tYnV0dG9uLWdyb3VwLm1vZGVsJztcblxuLy8gemEgdmlzZSBvIENvbnRyb2xWYWx1ZUFjY2Vzc29yLCBwb2dsZWRhajpcbi8vIGh0dHBzOi8vY29yeXJ5bGFuLmNvbS9ibG9nL2FuZ3VsYXItY3VzdG9tLWZvcm0tY29udHJvbHMtd2l0aC1yZWFjdGl2ZS1mb3Jtcy1hbmQtbmdtb2RlbFxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnYXBwLXJhZGlvLWJ1dHRvbicsXG4gIHRlbXBsYXRlVXJsOiAnLi9yYWRpby1idXR0b24uY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9yYWRpby1idXR0b24uY29tcG9uZW50LmNzcyddLFxuICBwcm92aWRlcnM6IFtcbiAgICB7XG4gICAgICBwcm92aWRlOiBOR19WQUxVRV9BQ0NFU1NPUixcbiAgICAgIG11bHRpOiB0cnVlLFxuICAgICAgdXNlRXhpc3Rpbmc6IGZvcndhcmRSZWYoKCkgPT4gUmFkaW9CdXR0b25Db21wb25lbnQpLFxuICAgIH0sXG4gIF0sXG59KVxuZXhwb3J0IGNsYXNzIFJhZGlvQnV0dG9uQ29tcG9uZW50XG4gIGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3ksIENvbnRyb2xWYWx1ZUFjY2Vzc29yIHtcbiAgQElucHV0KCkgcmFkaW9CdXR0b25Hcm91cDogUmFkaW9CdXR0b25Hcm91cDtcbiAgcHVibGljIHZhbHVlOiBzdHJpbmcgfCBudW1iZXIgfCBib29sZWFuID0gZmFsc2U7XG5cbiAgcHVibGljIG9uQ2hhbmdlOiBhbnkgPSAoKSA9PiB7fTtcbiAgcHVibGljIG9uVG91Y2hlZDogYW55ID0gKCkgPT4ge307XG5cbiAgY29uc3RydWN0b3IoKSB7fVxuXG4gIG9uTmdNb2RlbENoYW5nZSA9ICgpOiB2b2lkID0+IHtcbiAgICB0aGlzLnJhZGlvQnV0dG9uR3JvdXAuc2V0RGVmYXVsdFZhbHVlKHRoaXMudmFsdWUpO1xuICAgIHRoaXMub25DaGFuZ2UodGhpcy52YWx1ZSk7XG4gICAgdGhpcy5vblRvdWNoZWQoKTtcblxuICAgIC8vIGNvbnNvbGUubG9nKHRoaXMucmFkaW9CdXR0b25Hcm91cCk7XG4gIH07XG5cbiAgcmVnaXN0ZXJPbkNoYW5nZShmbikge1xuICAgIHRoaXMub25DaGFuZ2UgPSBmbjtcbiAgfVxuXG4gIHJlZ2lzdGVyT25Ub3VjaGVkKGZuKSB7XG4gICAgdGhpcy5vblRvdWNoZWQgPSBmbjtcbiAgfVxuXG4gIHdyaXRlVmFsdWUodmFsdWUpIHtcbiAgICBpZiAodmFsdWUpIHtcbiAgICAgIHRoaXMudmFsdWUgPSB2YWx1ZTtcbiAgICB9XG4gIH1cblxuICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgICAvLyBjb25zb2xlLmxvZyh0aGlzLnJhZGlvQnV0dG9uR3JvdXApO1xuXG4gICAgdGhpcy52YWx1ZSA9IHRoaXMucmFkaW9CdXR0b25Hcm91cC5nZXREZWZhdWx0VmFsdWUoKTtcblxuICAgIC8vIGNvbnNvbGUubG9nKCdSYWRpb0J1dHRvbkNvbXBvbmVudCBpbml0Jyk7XG4gIH1cblxuICBuZ09uRGVzdHJveSgpOiB2b2lkIHtcbiAgICAvLyBjb25zb2xlLmxvZygnUmFkaW9CdXR0b25Db21wb25lbnQgZGVzdHJveWVkJyk7XG4gIH1cbn1cbiIsIjxzdHlsZT5cblxuICAgIC5uby1tYXJnaW4ge1xuICAgICAgICBtYXJnaW46IDA7XG4gICAgfVxuXG48L3N0eWxlPlxuXG48c2VjdGlvblxuICAgIGZ4TGF5b3V0PVwiY29sdW1uXCJcbiAgICBmeExheW91dEFsaWduPVwiY2VudGVyIGNlbnRlclwiXG4gICAgZnhMYXlvdXRHYXA9XCIxNXB4XCI+XG5cbiAgICAgICAgPGxhYmVsIFxuICAgICAgICAgICAgKm5nSWY9XCJ0aGlzLnJhZGlvQnV0dG9uR3JvdXAuZ2V0TGFiZWwoKS5wb3NpdGlvbiAhPSAnYmVsbG93J1wiXG4gICAgICAgICAgICBpZD1cInJhZGlvLWdyb3VwLWxhYmVsXCI+e3sgdGhpcy5yYWRpb0J1dHRvbkdyb3VwLmdldExhYmVsKCkuY29udGV4dCB9fTwvbGFiZWw+XG5cbiAgICAgICAgPG1hdC1yYWRpby1ncm91cFxuICAgICAgICAgICAgYXJpYS1sYWJlbGxlZGJ5PVwicmFkaW8tZ3JvdXAtbGFiZWxcIlxuICAgICAgICAgICAgWyhuZ01vZGVsKV09XCJ0aGlzLnZhbHVlXCJcbiAgICAgICAgICAgIChuZ01vZGVsQ2hhbmdlKT1cInRoaXMub25OZ01vZGVsQ2hhbmdlKClcIlxuICAgICAgICAgICAgW2Rpc2FibGVkXT1cInRoaXMucmFkaW9CdXR0b25Hcm91cC5nZXREaXNhYmxlZCgpXCJcbiAgICAgICAgICAgIFtsYWJlbFBvc2l0aW9uXT1cInRoaXMucmFkaW9CdXR0b25Hcm91cC5nZXRMYWJlbFBvc2l0aW9uKClcIlxuICAgICAgICAgICAgW2Z4TGF5b3V0XT1cInRoaXMucmFkaW9CdXR0b25Hcm91cC5nZXREaXJlY3Rpb24oKVwiXG4gICAgICAgICAgICBmeExheW91dEFsaWduPVwic3RhcnQgc3RhcnRcIlxuICAgICAgICAgICAgZnhMYXlvdXRHYXA9XCIyMHB4XCI+XG5cbiAgICAgICAgICAgICAgICA8bWF0LXJhZGlvLWJ1dHRvblxuICAgICAgICAgICAgICAgICAgICAqbmdGb3I9XCJsZXQgb3B0aW9uIG9mIHRoaXMucmFkaW9CdXR0b25Hcm91cC5nZXRDaGlsZHJlbigpXCJcbiAgICAgICAgICAgICAgICAgICAgW3ZhbHVlXT1cIm9wdGlvbi52YWx1ZVwiPnt7IG9wdGlvbi5uYW1lIH19PC9tYXQtcmFkaW8tYnV0dG9uPlxuICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgPC9tYXQtcmFkaW8tZ3JvdXA+XG5cbiAgICAgICAgPGxhYmVsIFxuICAgICAgICAgICAgKm5nSWY9XCJ0aGlzLnJhZGlvQnV0dG9uR3JvdXAuZ2V0TGFiZWwoKS5wb3NpdGlvbiA9PSAnYmVsbG93J1wiXG4gICAgICAgICAgICBpZD1cInJhZGlvLWdyb3VwLWxhYmVsXCI+e3sgdGhpcy5yYWRpb0J1dHRvbkdyb3VwLmdldExhYmVsKCkuY29udGV4dCB9fTwvbGFiZWw+XG5cbjwvc2VjdGlvbj4iXX0=