import { Component, Input, forwardRef } from "@angular/core";
import { NG_VALUE_ACCESSOR } from "@angular/forms";
import { VALIDATOR_NAMES, } from "../../../model/structural/composite/form/form-row-item.model";
import * as i0 from "@angular/core";
import * as i1 from "@angular/flex-layout/flex";
import * as i2 from "@angular/material/form-field";
import * as i3 from "@angular/common";
import * as i4 from "@angular/material/input";
import * as i5 from "@angular/material/icon";
function FormFieldInputTextComponent_mat_label_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-label");
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r0 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r0.formFieldInputText.getLabelName());
} }
function FormFieldInputTextComponent_mat_icon_5_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-icon", 9);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r2 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r2.formFieldInputText.getMatPrefixImgText());
} }
function FormFieldInputTextComponent_mat_icon_6_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-icon", 10);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r3 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r3.formFieldInputText.getMatSuffixImgText());
} }
function FormFieldInputTextComponent_span_7_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 9);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r4 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1("", ctx_r4.formFieldInputText.getTextPrefix(), "\u00A0");
} }
function FormFieldInputTextComponent_span_8_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 10);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r5 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r5.formFieldInputText.getTextSuffix());
} }
function FormFieldInputTextComponent_mat_hint_9_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-hint", 11);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r6 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r6.formFieldInputText.getLeftHintLabel());
} }
function FormFieldInputTextComponent_mat_hint_10_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-hint", 12);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r7 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r7.formFieldInputText.getRightHintLabel());
} }
function FormFieldInputTextComponent_mat_hint_11_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-hint", 12);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r8 = i0.ɵɵnextContext();
    var tmp_0_0 = null;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate3("", ((tmp_0_0 = ctx_r8.getMinLengthValidator()) == null ? null : tmp_0_0.length) ? ctx_r8.getMinLengthValidator().length + " / " : "", "", (tmp_0_0 = ctx_r8.formFieldInputText.getDefaultValue()) == null ? null : tmp_0_0.length, "", ((tmp_0_0 = ctx_r8.getMaxLengthValidator()) == null ? null : tmp_0_0.length) ? " / " + ctx_r8.getMaxLengthValidator().length : "", "");
} }
function FormFieldInputTextComponent_mat_hint_12_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-hint", 12);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r9 = i0.ɵɵnextContext();
    var tmp_0_0 = null;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate2("", (tmp_0_0 = ctx_r9.formFieldInputText.getDefaultValue()) == null ? null : tmp_0_0.length, "", ((tmp_0_0 = ctx_r9.getMinLengthValidator()) == null ? null : tmp_0_0.length) ? " / " + ctx_r9.getMinLengthValidator().length : "", "");
} }
function FormFieldInputTextComponent_mat_hint_13_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-hint", 12);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r10 = i0.ɵɵnextContext();
    var tmp_0_0 = null;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate2("", (tmp_0_0 = ctx_r10.formFieldInputText.getDefaultValue()) == null ? null : tmp_0_0.length, "", ((tmp_0_0 = ctx_r10.getMaxLengthValidator()) == null ? null : tmp_0_0.length) ? " / " + ctx_r10.getMaxLengthValidator().length : "", "");
} }
export class FormFieldInputTextComponent {
    constructor() {
        this.onChange = () => { };
        this.onTouched = () => { };
        this.onKeyUp = (input) => {
            // console.log(input.value);
            this.formFieldInputText.setDefaultValue(input.value);
            this.onChange(this.formFieldInputText.getDefaultValue());
            this.onTouched();
        };
        this.getMinLengthValidator = () => {
            let theValidator;
            this.formFieldInputText
                .getValidators()
                .forEach((validator) => {
                if (validator.name === VALIDATOR_NAMES.MIN_LENGTH)
                    theValidator = validator;
            });
            return theValidator;
        };
        this.getMaxLengthValidator = () => {
            let theValidator;
            this.formFieldInputText
                .getValidators()
                .forEach((validator) => {
                if (validator.name === VALIDATOR_NAMES.MAX_LENGTH)
                    theValidator = validator;
            });
            return theValidator;
        };
    }
    registerOnChange(fn) {
        this.onChange = fn;
    }
    registerOnTouched(fn) {
        this.onTouched = fn;
    }
    writeValue(value) {
        if (value) {
            this.formFieldInputText.setDefaultValue(value);
        }
    }
    ngOnInit() {
        // console.log('FormFieldInputTextComponent init');
    }
    ngOnDestroy() {
        // console.log('FormFieldInputTextComponent destroyed');
    }
}
FormFieldInputTextComponent.ɵfac = function FormFieldInputTextComponent_Factory(t) { return new (t || FormFieldInputTextComponent)(); };
FormFieldInputTextComponent.ɵcmp = i0.ɵɵdefineComponent({ type: FormFieldInputTextComponent, selectors: [["app-form-field-input-text"]], inputs: { formFieldInputText: "formFieldInputText" }, features: [i0.ɵɵProvidersFeature([
            {
                provide: NG_VALUE_ACCESSOR,
                multi: true,
                useExisting: forwardRef(() => FormFieldInputTextComponent),
            },
        ])], decls: 14, vars: 18, consts: [["fxLayoutAlign", "center center"], [3, "appearance", "hideRequiredMarker"], [4, "ngIf"], ["matInput", "", 3, "type", "placeholder", "value", "disabled", "readonly", "required", "keyup", "blur"], ["input", ""], ["matPrefix", "", 4, "ngIf"], ["matSuffix", "", 4, "ngIf"], ["align", "start", 4, "ngIf"], ["align", "end", 4, "ngIf"], ["matPrefix", ""], ["matSuffix", ""], ["align", "start"], ["align", "end"]], template: function FormFieldInputTextComponent_Template(rf, ctx) { if (rf & 1) {
        const _r11 = i0.ɵɵgetCurrentView();
        i0.ɵɵelementStart(0, "section", 0);
        i0.ɵɵelementStart(1, "mat-form-field", 1);
        i0.ɵɵtemplate(2, FormFieldInputTextComponent_mat_label_2_Template, 2, 1, "mat-label", 2);
        i0.ɵɵelementStart(3, "input", 3, 4);
        i0.ɵɵlistener("keyup", function FormFieldInputTextComponent_Template_input_keyup_3_listener() { i0.ɵɵrestoreView(_r11); const _r1 = i0.ɵɵreference(4); return ctx.onKeyUp(_r1); })("blur", function FormFieldInputTextComponent_Template_input_blur_3_listener() { i0.ɵɵrestoreView(_r11); const _r1 = i0.ɵɵreference(4); return ctx.onKeyUp(_r1); });
        i0.ɵɵelementEnd();
        i0.ɵɵtemplate(5, FormFieldInputTextComponent_mat_icon_5_Template, 2, 1, "mat-icon", 5);
        i0.ɵɵtemplate(6, FormFieldInputTextComponent_mat_icon_6_Template, 2, 1, "mat-icon", 6);
        i0.ɵɵtemplate(7, FormFieldInputTextComponent_span_7_Template, 2, 1, "span", 5);
        i0.ɵɵtemplate(8, FormFieldInputTextComponent_span_8_Template, 2, 1, "span", 6);
        i0.ɵɵtemplate(9, FormFieldInputTextComponent_mat_hint_9_Template, 2, 1, "mat-hint", 7);
        i0.ɵɵtemplate(10, FormFieldInputTextComponent_mat_hint_10_Template, 2, 1, "mat-hint", 8);
        i0.ɵɵtemplate(11, FormFieldInputTextComponent_mat_hint_11_Template, 2, 3, "mat-hint", 8);
        i0.ɵɵtemplate(12, FormFieldInputTextComponent_mat_hint_12_Template, 2, 2, "mat-hint", 8);
        i0.ɵɵtemplate(13, FormFieldInputTextComponent_mat_hint_13_Template, 2, 2, "mat-hint", 8);
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("appearance", ctx.formFieldInputText.getAppearance())("hideRequiredMarker", ctx.formFieldInputText.getRequired().hideRequiredMarker);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.formFieldInputText.getLabelName());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("type", ctx.formFieldInputText.getFormFieldInputType())("placeholder", ctx.formFieldInputText.getPlaceholder() ? ctx.formFieldInputText.getPlaceholder() : "")("value", ctx.formFieldInputText.getDefaultValue())("disabled", ctx.formFieldInputText.getDisabled())("readonly", ctx.formFieldInputText.getReadonly())("required", ctx.formFieldInputText.getRequired().required);
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("ngIf", ctx.formFieldInputText.getMatPrefixImgText());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.formFieldInputText.getMatSuffixImgText());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.formFieldInputText.getTextPrefix());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.formFieldInputText.getTextSuffix());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.formFieldInputText.getLeftHintLabel());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.formFieldInputText.getRightHintLabel());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.formFieldInputText.getShowHintAboutMinMaxLength() && ctx.getMinLengthValidator() && ctx.getMaxLengthValidator());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.formFieldInputText.getShowHintAboutMinMaxLength() && ctx.getMinLengthValidator() && !ctx.getMaxLengthValidator());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.formFieldInputText.getShowHintAboutMinMaxLength() && !ctx.getMinLengthValidator() && ctx.getMaxLengthValidator());
    } }, directives: [i1.DefaultLayoutAlignDirective, i2.MatFormField, i3.NgIf, i4.MatInput, i2.MatLabel, i5.MatIcon, i2.MatPrefix, i2.MatSuffix, i2.MatHint], styles: ["", ".cursor[_ngcontent-%COMP%] {\n        cursor: pointer;\n    }\n\n    input[type=\"file\"][_ngcontent-%COMP%] {\n        display: none;\n    }\n\n    h3[_ngcontent-%COMP%] {\n        font-size: inherit;\n        margin: 0;\n        font-weight: inherit;\n    }\n\n    mat-form-field[_ngcontent-%COMP%] {\n        min-width: 250px;\n    }"] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FormFieldInputTextComponent, [{
        type: Component,
        args: [{
                selector: "app-form-field-input-text",
                templateUrl: "./form-field-input-text.component.html",
                styleUrls: ["./form-field-input-text.component.css"],
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        multi: true,
                        useExisting: forwardRef(() => FormFieldInputTextComponent),
                    },
                ],
            }]
    }], function () { return []; }, { formFieldInputText: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1maWVsZC1pbnB1dC10ZXh0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215LWFuZ3VsYXItZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvdWkvZm9ybS1maWVsZC1pbnB1dC9mb3JtLWZpZWxkLWlucHV0LXRleHQvZm9ybS1maWVsZC1pbnB1dC10ZXh0LmNvbXBvbmVudC50cyIsImxpYi9zaGFyZWQvdWkvZm9ybS1maWVsZC1pbnB1dC9mb3JtLWZpZWxkLWlucHV0LXRleHQvZm9ybS1maWVsZC1pbnB1dC10ZXh0LmNvbXBvbmVudC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQXFCLEtBQUssRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDaEYsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFJbkQsT0FBTyxFQUVMLGVBQWUsR0FDaEIsTUFBTSw4REFBOEQsQ0FBQzs7Ozs7Ozs7SUNzQnRELGlDQUNtRDtJQUFBLFlBQTRDO0lBQUEsaUJBQVk7OztJQUF4RCxlQUE0QztJQUE1Qyw4REFBNEM7OztJQWMvRixtQ0FFYztJQUFBLFlBQW1EO0lBQUEsaUJBQVc7OztJQUE5RCxlQUFtRDtJQUFuRCxxRUFBbUQ7OztJQUVqRSxvQ0FFYztJQUFBLFlBQW1EO0lBQUEsaUJBQVc7OztJQUE5RCxlQUFtRDtJQUFuRCxxRUFBbUQ7OztJQUVqRSwrQkFFYztJQUFBLFlBQW1EO0lBQUEsaUJBQU87OztJQUExRCxlQUFtRDtJQUFuRCw4RUFBbUQ7OztJQUVqRSxnQ0FFYztJQUFBLFlBQTZDO0lBQUEsaUJBQU87OztJQUFwRCxlQUE2QztJQUE3QywrREFBNkM7OztJQUUzRCxvQ0FFa0I7SUFBQSxZQUFnRDtJQUFBLGlCQUFXOzs7SUFBM0QsZUFBZ0Q7SUFBaEQsa0VBQWdEOzs7SUFFbEUsb0NBRWdCO0lBQUEsWUFBaUQ7SUFBQSxpQkFBVzs7O0lBQTVELGVBQWlEO0lBQWpELG1FQUFpRDs7O0lBR2pFLG9DQUVnQjtJQUFBLFlBQThPO0lBQUEsaUJBQVc7Ozs7SUFBelAsZUFBOE87SUFBOU8sb1lBQThPOzs7SUFFOVAsb0NBRWdCO0lBQUEsWUFBbUo7SUFBQSxpQkFBVzs7OztJQUE5SixlQUFtSjtJQUFuSiw2UEFBbUo7OztJQUVuSyxvQ0FFZ0I7SUFBQSxZQUFrSjtJQUFBLGlCQUFXOzs7O0lBQTdKLGVBQWtKO0lBQWxKLGdRQUFrSjs7QUQxRGxMLE1BQU0sT0FBTywyQkFBMkI7SUFNdEM7UUFITyxhQUFRLEdBQVEsR0FBRyxFQUFFLEdBQUUsQ0FBQyxDQUFDO1FBQ3pCLGNBQVMsR0FBUSxHQUFHLEVBQUUsR0FBRSxDQUFDLENBQUM7UUFJakMsWUFBTyxHQUFHLENBQUMsS0FBdUIsRUFBRSxFQUFFO1lBQ3BDLDRCQUE0QjtZQUM1QixJQUFJLENBQUMsa0JBQWtCLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNyRCxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxlQUFlLEVBQUUsQ0FBQyxDQUFDO1lBQ3pELElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUNuQixDQUFDLENBQUM7UUFnQkYsMEJBQXFCLEdBQUcsR0FBa0MsRUFBRTtZQUMxRCxJQUFJLFlBQVksQ0FBQztZQUNqQixJQUFJLENBQUMsa0JBQWtCO2lCQUNwQixhQUFhLEVBQUU7aUJBQ2YsT0FBTyxDQUFDLENBQUMsU0FBd0MsRUFBRSxFQUFFO2dCQUNwRCxJQUFJLFNBQVMsQ0FBQyxJQUFJLEtBQUssZUFBZSxDQUFDLFVBQVU7b0JBQy9DLFlBQVksR0FBRyxTQUFTLENBQUM7WUFDN0IsQ0FBQyxDQUFDLENBQUM7WUFDTCxPQUFPLFlBQVksQ0FBQztRQUN0QixDQUFDLENBQUM7UUFFRiwwQkFBcUIsR0FBRyxHQUFrQyxFQUFFO1lBQzFELElBQUksWUFBWSxDQUFDO1lBQ2pCLElBQUksQ0FBQyxrQkFBa0I7aUJBQ3BCLGFBQWEsRUFBRTtpQkFDZixPQUFPLENBQUMsQ0FBQyxTQUF3QyxFQUFFLEVBQUU7Z0JBQ3BELElBQUksU0FBUyxDQUFDLElBQUksS0FBSyxlQUFlLENBQUMsVUFBVTtvQkFDL0MsWUFBWSxHQUFHLFNBQVMsQ0FBQztZQUM3QixDQUFDLENBQUMsQ0FBQztZQUNMLE9BQU8sWUFBWSxDQUFDO1FBQ3RCLENBQUMsQ0FBQztJQTNDYSxDQUFDO0lBU2hCLGdCQUFnQixDQUFDLEVBQUU7UUFDakIsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7SUFDckIsQ0FBQztJQUVELGlCQUFpQixDQUFDLEVBQUU7UUFDbEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7SUFDdEIsQ0FBQztJQUVELFVBQVUsQ0FBQyxLQUFLO1FBQ2QsSUFBSSxLQUFLLEVBQUU7WUFDVCxJQUFJLENBQUMsa0JBQWtCLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ2hEO0lBQ0gsQ0FBQztJQXdCRCxRQUFRO1FBQ04sbURBQW1EO0lBQ3JELENBQUM7SUFFRCxXQUFXO1FBQ1Qsd0RBQXdEO0lBQzFELENBQUM7O3NHQXpEVSwyQkFBMkI7Z0VBQTNCLDJCQUEyQixxSUFSM0I7WUFDVDtnQkFDRSxPQUFPLEVBQUUsaUJBQWlCO2dCQUMxQixLQUFLLEVBQUUsSUFBSTtnQkFDWCxXQUFXLEVBQUUsVUFBVSxDQUFDLEdBQUcsRUFBRSxDQUFDLDJCQUEyQixDQUFDO2FBQzNEO1NBQ0Y7O1FDR0gsa0NBR1E7UUFBQSx5Q0FJUTtRQUFBLHdGQUNtRDtRQUVuRCxtQ0FZQTtRQVBJLDhKQUFTLGdCQUFtQixJQUFDLCtJQUNyQixnQkFBbUIsSUFERTtRQUxqQyxpQkFZQTtRQUFBLHNGQUVjO1FBRWQsc0ZBRWM7UUFFZCw4RUFFYztRQUVkLDhFQUVjO1FBRWQsc0ZBRWtCO1FBRWxCLHdGQUVnQjtRQUdoQix3RkFFZ0I7UUFFaEIsd0ZBRWdCO1FBRWhCLHdGQUVnQjtRQUV4QixpQkFBaUI7UUFFekIsaUJBQVU7O1FBekRFLGVBQXNEO1FBQXRELG1FQUFzRCwrRUFBQTtRQUk5QyxlQUE4QztRQUE5Qyw0REFBOEM7UUFJOUMsZUFBd0Q7UUFBeEQscUVBQXdELHVHQUFBLG1EQUFBLGtEQUFBLGtEQUFBLDJEQUFBO1FBV3hELGVBQXFEO1FBQXJELG1FQUFxRDtRQUlyRCxlQUFxRDtRQUFyRCxtRUFBcUQ7UUFJckQsZUFBK0M7UUFBL0MsNkRBQStDO1FBSS9DLGVBQStDO1FBQS9DLDZEQUErQztRQUkvQyxlQUFrRDtRQUFsRCxnRUFBa0Q7UUFJbEQsZUFBbUQ7UUFBbkQsaUVBQW1EO1FBS25ELGVBQThIO1FBQTlILDBJQUE4SDtRQUk5SCxlQUErSDtRQUEvSCwySUFBK0g7UUFJL0gsZUFBK0g7UUFBL0gsMklBQStIOztrRER6RHRJLDJCQUEyQjtjQVp2QyxTQUFTO2VBQUM7Z0JBQ1QsUUFBUSxFQUFFLDJCQUEyQjtnQkFDckMsV0FBVyxFQUFFLHdDQUF3QztnQkFDckQsU0FBUyxFQUFFLENBQUMsdUNBQXVDLENBQUM7Z0JBQ3BELFNBQVMsRUFBRTtvQkFDVDt3QkFDRSxPQUFPLEVBQUUsaUJBQWlCO3dCQUMxQixLQUFLLEVBQUUsSUFBSTt3QkFDWCxXQUFXLEVBQUUsVUFBVSxDQUFDLEdBQUcsRUFBRSxDQUFDLDJCQUEyQixDQUFDO3FCQUMzRDtpQkFDRjthQUNGOztrQkFFRSxLQUFLIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIE9uRGVzdHJveSwgSW5wdXQsIGZvcndhcmRSZWYgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgTkdfVkFMVUVfQUNDRVNTT1IgfSBmcm9tIFwiQGFuZ3VsYXIvZm9ybXNcIjtcblxuLy8gbW9kZWxcbmltcG9ydCB7IEZvcm1GaWVsZElucHV0VGV4dCB9IGZyb20gXCIuLi8uLi8uLi9tb2RlbC9zdHJ1Y3R1cmFsL2NvbXBvc2l0ZS9mb3JtL2Zvcm0tZmllbGQtaW5wdXQvZm9ybS1maWVsZC1pbnB1dC10ZXh0Lm1vZGVsXCI7XG5pbXBvcnQge1xuICBGb3JtUm93SXRlbVZhbGlkYXRvckludGVyZmFjZSxcbiAgVkFMSURBVE9SX05BTUVTLFxufSBmcm9tIFwiLi4vLi4vLi4vbW9kZWwvc3RydWN0dXJhbC9jb21wb3NpdGUvZm9ybS9mb3JtLXJvdy1pdGVtLm1vZGVsXCI7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogXCJhcHAtZm9ybS1maWVsZC1pbnB1dC10ZXh0XCIsXG4gIHRlbXBsYXRlVXJsOiBcIi4vZm9ybS1maWVsZC1pbnB1dC10ZXh0LmNvbXBvbmVudC5odG1sXCIsXG4gIHN0eWxlVXJsczogW1wiLi9mb3JtLWZpZWxkLWlucHV0LXRleHQuY29tcG9uZW50LmNzc1wiXSxcbiAgcHJvdmlkZXJzOiBbXG4gICAge1xuICAgICAgcHJvdmlkZTogTkdfVkFMVUVfQUNDRVNTT1IsXG4gICAgICBtdWx0aTogdHJ1ZSxcbiAgICAgIHVzZUV4aXN0aW5nOiBmb3J3YXJkUmVmKCgpID0+IEZvcm1GaWVsZElucHV0VGV4dENvbXBvbmVudCksXG4gICAgfSxcbiAgXSxcbn0pXG5leHBvcnQgY2xhc3MgRm9ybUZpZWxkSW5wdXRUZXh0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xuICBASW5wdXQoKSBmb3JtRmllbGRJbnB1dFRleHQ6IEZvcm1GaWVsZElucHV0VGV4dDtcblxuICBwdWJsaWMgb25DaGFuZ2U6IGFueSA9ICgpID0+IHt9O1xuICBwdWJsaWMgb25Ub3VjaGVkOiBhbnkgPSAoKSA9PiB7fTtcblxuICBjb25zdHJ1Y3RvcigpIHt9XG5cbiAgb25LZXlVcCA9IChpbnB1dDogSFRNTElucHV0RWxlbWVudCkgPT4ge1xuICAgIC8vIGNvbnNvbGUubG9nKGlucHV0LnZhbHVlKTtcbiAgICB0aGlzLmZvcm1GaWVsZElucHV0VGV4dC5zZXREZWZhdWx0VmFsdWUoaW5wdXQudmFsdWUpO1xuICAgIHRoaXMub25DaGFuZ2UodGhpcy5mb3JtRmllbGRJbnB1dFRleHQuZ2V0RGVmYXVsdFZhbHVlKCkpO1xuICAgIHRoaXMub25Ub3VjaGVkKCk7XG4gIH07XG5cbiAgcmVnaXN0ZXJPbkNoYW5nZShmbikge1xuICAgIHRoaXMub25DaGFuZ2UgPSBmbjtcbiAgfVxuXG4gIHJlZ2lzdGVyT25Ub3VjaGVkKGZuKSB7XG4gICAgdGhpcy5vblRvdWNoZWQgPSBmbjtcbiAgfVxuXG4gIHdyaXRlVmFsdWUodmFsdWUpIHtcbiAgICBpZiAodmFsdWUpIHtcbiAgICAgIHRoaXMuZm9ybUZpZWxkSW5wdXRUZXh0LnNldERlZmF1bHRWYWx1ZSh2YWx1ZSk7XG4gICAgfVxuICB9XG5cbiAgZ2V0TWluTGVuZ3RoVmFsaWRhdG9yID0gKCk6IEZvcm1Sb3dJdGVtVmFsaWRhdG9ySW50ZXJmYWNlID0+IHtcbiAgICBsZXQgdGhlVmFsaWRhdG9yO1xuICAgIHRoaXMuZm9ybUZpZWxkSW5wdXRUZXh0XG4gICAgICAuZ2V0VmFsaWRhdG9ycygpXG4gICAgICAuZm9yRWFjaCgodmFsaWRhdG9yOiBGb3JtUm93SXRlbVZhbGlkYXRvckludGVyZmFjZSkgPT4ge1xuICAgICAgICBpZiAodmFsaWRhdG9yLm5hbWUgPT09IFZBTElEQVRPUl9OQU1FUy5NSU5fTEVOR1RIKVxuICAgICAgICAgIHRoZVZhbGlkYXRvciA9IHZhbGlkYXRvcjtcbiAgICAgIH0pO1xuICAgIHJldHVybiB0aGVWYWxpZGF0b3I7XG4gIH07XG5cbiAgZ2V0TWF4TGVuZ3RoVmFsaWRhdG9yID0gKCk6IEZvcm1Sb3dJdGVtVmFsaWRhdG9ySW50ZXJmYWNlID0+IHtcbiAgICBsZXQgdGhlVmFsaWRhdG9yO1xuICAgIHRoaXMuZm9ybUZpZWxkSW5wdXRUZXh0XG4gICAgICAuZ2V0VmFsaWRhdG9ycygpXG4gICAgICAuZm9yRWFjaCgodmFsaWRhdG9yOiBGb3JtUm93SXRlbVZhbGlkYXRvckludGVyZmFjZSkgPT4ge1xuICAgICAgICBpZiAodmFsaWRhdG9yLm5hbWUgPT09IFZBTElEQVRPUl9OQU1FUy5NQVhfTEVOR1RIKVxuICAgICAgICAgIHRoZVZhbGlkYXRvciA9IHZhbGlkYXRvcjtcbiAgICAgIH0pO1xuICAgIHJldHVybiB0aGVWYWxpZGF0b3I7XG4gIH07XG5cbiAgbmdPbkluaXQoKTogdm9pZCB7XG4gICAgLy8gY29uc29sZS5sb2coJ0Zvcm1GaWVsZElucHV0VGV4dENvbXBvbmVudCBpbml0Jyk7XG4gIH1cblxuICBuZ09uRGVzdHJveSgpOiB2b2lkIHtcbiAgICAvLyBjb25zb2xlLmxvZygnRm9ybUZpZWxkSW5wdXRUZXh0Q29tcG9uZW50IGRlc3Ryb3llZCcpO1xuICB9XG59XG4iLCI8c3R5bGU+XG5cbiAgICAuY3Vyc29yIHtcbiAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIH1cblxuICAgIGlucHV0W3R5cGU9XCJmaWxlXCJdIHtcbiAgICAgICAgZGlzcGxheTogbm9uZTtcbiAgICB9XG5cbiAgICBoMyB7XG4gICAgICAgIGZvbnQtc2l6ZTogaW5oZXJpdDtcbiAgICAgICAgbWFyZ2luOiAwO1xuICAgICAgICBmb250LXdlaWdodDogaW5oZXJpdDtcbiAgICB9XG5cbiAgICBtYXQtZm9ybS1maWVsZCB7XG4gICAgICAgIG1pbi13aWR0aDogMjUwcHg7XG4gICAgfVxuXG48L3N0eWxlPlxuICAgIFxuPCEtLSAtLS0tLS0tLS0tLS0tLS0tLS0tIGZvcm0gZmllbGQgaW5wdXQgdGV4dCAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSAtLT5cbjxzZWN0aW9uXG4gICAgZnhMYXlvdXRBbGlnbj1cImNlbnRlciBjZW50ZXJcIj5cblxuICAgICAgICA8bWF0LWZvcm0tZmllbGQgXG4gICAgICAgICAgICBbYXBwZWFyYW5jZV09XCJ0aGlzLmZvcm1GaWVsZElucHV0VGV4dC5nZXRBcHBlYXJhbmNlKClcIlxuICAgICAgICAgICAgW2hpZGVSZXF1aXJlZE1hcmtlcl09XCJ0aGlzLmZvcm1GaWVsZElucHV0VGV4dC5nZXRSZXF1aXJlZCgpLmhpZGVSZXF1aXJlZE1hcmtlclwiPlxuXG4gICAgICAgICAgICAgICAgPG1hdC1sYWJlbFxuICAgICAgICAgICAgICAgICAgICAqbmdJZj1cInRoaXMuZm9ybUZpZWxkSW5wdXRUZXh0LmdldExhYmVsTmFtZSgpXCI+e3sgdGhpcy5mb3JtRmllbGRJbnB1dFRleHQuZ2V0TGFiZWxOYW1lKCkgfX08L21hdC1sYWJlbD5cblxuICAgICAgICAgICAgICAgIDxpbnB1dCBcbiAgICAgICAgICAgICAgICAgICAgbWF0SW5wdXRcbiAgICAgICAgICAgICAgICAgICAgW3R5cGVdPVwidGhpcy5mb3JtRmllbGRJbnB1dFRleHQuZ2V0Rm9ybUZpZWxkSW5wdXRUeXBlKClcIlxuICAgICAgICAgICAgICAgICAgICBbcGxhY2Vob2xkZXJdPVwidGhpcy5mb3JtRmllbGRJbnB1dFRleHQuZ2V0UGxhY2Vob2xkZXIoKSA/IHRoaXMuZm9ybUZpZWxkSW5wdXRUZXh0LmdldFBsYWNlaG9sZGVyKCkgOiAnJ1wiXG4gICAgICAgICAgICAgICAgICAgIFt2YWx1ZV09XCJ0aGlzLmZvcm1GaWVsZElucHV0VGV4dC5nZXREZWZhdWx0VmFsdWUoKVwiXG4gICAgICAgICAgICAgICAgICAgIChrZXl1cCk9XCJ0aGlzLm9uS2V5VXAoaW5wdXQpXCJcbiAgICAgICAgICAgICAgICAgICAgKGJsdXIpPVwidGhpcy5vbktleVVwKGlucHV0KVwiXG4gICAgICAgICAgICAgICAgICAgIFtkaXNhYmxlZF09XCJ0aGlzLmZvcm1GaWVsZElucHV0VGV4dC5nZXREaXNhYmxlZCgpXCJcbiAgICAgICAgICAgICAgICAgICAgW3JlYWRvbmx5XT1cInRoaXMuZm9ybUZpZWxkSW5wdXRUZXh0LmdldFJlYWRvbmx5KClcIlxuICAgICAgICAgICAgICAgICAgICBbcmVxdWlyZWRdPVwidGhpcy5mb3JtRmllbGRJbnB1dFRleHQuZ2V0UmVxdWlyZWQoKS5yZXF1aXJlZFwiXG4gICAgICAgICAgICAgICAgICAgICNpbnB1dD5cblxuICAgICAgICAgICAgICAgIDxtYXQtaWNvbiBcbiAgICAgICAgICAgICAgICAgICAgKm5nSWY9XCJ0aGlzLmZvcm1GaWVsZElucHV0VGV4dC5nZXRNYXRQcmVmaXhJbWdUZXh0KClcIlxuICAgICAgICAgICAgICAgICAgICBtYXRQcmVmaXg+e3sgdGhpcy5mb3JtRmllbGRJbnB1dFRleHQuZ2V0TWF0UHJlZml4SW1nVGV4dCgpIH19PC9tYXQtaWNvbj5cblxuICAgICAgICAgICAgICAgIDxtYXQtaWNvbiBcbiAgICAgICAgICAgICAgICAgICAgKm5nSWY9XCJ0aGlzLmZvcm1GaWVsZElucHV0VGV4dC5nZXRNYXRTdWZmaXhJbWdUZXh0KClcIlxuICAgICAgICAgICAgICAgICAgICBtYXRTdWZmaXg+e3sgdGhpcy5mb3JtRmllbGRJbnB1dFRleHQuZ2V0TWF0U3VmZml4SW1nVGV4dCgpIH19PC9tYXQtaWNvbj5cblxuICAgICAgICAgICAgICAgIDxzcGFuIFxuICAgICAgICAgICAgICAgICAgICAqbmdJZj1cInRoaXMuZm9ybUZpZWxkSW5wdXRUZXh0LmdldFRleHRQcmVmaXgoKVwiXG4gICAgICAgICAgICAgICAgICAgIG1hdFByZWZpeD57eyB0aGlzLmZvcm1GaWVsZElucHV0VGV4dC5nZXRUZXh0UHJlZml4KCkgfX0mbmJzcDs8L3NwYW4+XG5cbiAgICAgICAgICAgICAgICA8c3BhbiBcbiAgICAgICAgICAgICAgICAgICAgKm5nSWY9XCJ0aGlzLmZvcm1GaWVsZElucHV0VGV4dC5nZXRUZXh0U3VmZml4KClcIlxuICAgICAgICAgICAgICAgICAgICBtYXRTdWZmaXg+e3sgdGhpcy5mb3JtRmllbGRJbnB1dFRleHQuZ2V0VGV4dFN1ZmZpeCgpIH19PC9zcGFuPlxuXG4gICAgICAgICAgICAgICAgPG1hdC1oaW50IFxuICAgICAgICAgICAgICAgICAgICAqbmdJZj1cInRoaXMuZm9ybUZpZWxkSW5wdXRUZXh0LmdldExlZnRIaW50TGFiZWwoKVwiXG4gICAgICAgICAgICAgICAgICAgIGFsaWduPVwic3RhcnRcIj57eyB0aGlzLmZvcm1GaWVsZElucHV0VGV4dC5nZXRMZWZ0SGludExhYmVsKCkgfX08L21hdC1oaW50PlxuXG4gICAgICAgICAgICAgICAgPG1hdC1oaW50XG4gICAgICAgICAgICAgICAgICAgICpuZ0lmPVwidGhpcy5mb3JtRmllbGRJbnB1dFRleHQuZ2V0UmlnaHRIaW50TGFiZWwoKVwiXG4gICAgICAgICAgICAgICAgICAgIGFsaWduPVwiZW5kXCI+e3sgdGhpcy5mb3JtRmllbGRJbnB1dFRleHQuZ2V0UmlnaHRIaW50TGFiZWwoKSB9fTwvbWF0LWhpbnQ+XG5cbiAgICAgICAgICAgICAgICA8IS0tIGlmIGZpZWxkIGhhcyBTaG93SGludEFib3V0TWluTWF4TGVuZ3RoIHNldCB0byB0cnVlIC0tPlxuICAgICAgICAgICAgICAgIDxtYXQtaGludFxuICAgICAgICAgICAgICAgICAgICAqbmdJZj1cInRoaXMuZm9ybUZpZWxkSW5wdXRUZXh0LmdldFNob3dIaW50QWJvdXRNaW5NYXhMZW5ndGgoKSAmJiB0aGlzLmdldE1pbkxlbmd0aFZhbGlkYXRvcigpICYmIHRoaXMuZ2V0TWF4TGVuZ3RoVmFsaWRhdG9yKClcIlxuICAgICAgICAgICAgICAgICAgICBhbGlnbj1cImVuZFwiPnt7IHRoaXMuZ2V0TWluTGVuZ3RoVmFsaWRhdG9yKCk/Lmxlbmd0aCA/IHRoaXMuZ2V0TWluTGVuZ3RoVmFsaWRhdG9yKCkubGVuZ3RoICsgXCIgLyBcIiA6ICcnfX17eyB0aGlzLmZvcm1GaWVsZElucHV0VGV4dC5nZXREZWZhdWx0VmFsdWUoKT8ubGVuZ3RoIH19e3sgdGhpcy5nZXRNYXhMZW5ndGhWYWxpZGF0b3IoKT8ubGVuZ3RoID8gXCIgLyBcIiArIHRoaXMuZ2V0TWF4TGVuZ3RoVmFsaWRhdG9yKCkubGVuZ3RoOiAnJ319PC9tYXQtaGludD5cblxuICAgICAgICAgICAgICAgIDxtYXQtaGludFxuICAgICAgICAgICAgICAgICAgICAqbmdJZj1cInRoaXMuZm9ybUZpZWxkSW5wdXRUZXh0LmdldFNob3dIaW50QWJvdXRNaW5NYXhMZW5ndGgoKSAmJiB0aGlzLmdldE1pbkxlbmd0aFZhbGlkYXRvcigpICYmICF0aGlzLmdldE1heExlbmd0aFZhbGlkYXRvcigpXCJcbiAgICAgICAgICAgICAgICAgICAgYWxpZ249XCJlbmRcIj57eyB0aGlzLmZvcm1GaWVsZElucHV0VGV4dC5nZXREZWZhdWx0VmFsdWUoKT8ubGVuZ3RoIH19e3sgdGhpcy5nZXRNaW5MZW5ndGhWYWxpZGF0b3IoKT8ubGVuZ3RoID8gXCIgLyBcIiArIHRoaXMuZ2V0TWluTGVuZ3RoVmFsaWRhdG9yKCkubGVuZ3RoIDogJyd9fTwvbWF0LWhpbnQ+XG5cbiAgICAgICAgICAgICAgICA8bWF0LWhpbnRcbiAgICAgICAgICAgICAgICAgICAgKm5nSWY9XCJ0aGlzLmZvcm1GaWVsZElucHV0VGV4dC5nZXRTaG93SGludEFib3V0TWluTWF4TGVuZ3RoKCkgJiYgIXRoaXMuZ2V0TWluTGVuZ3RoVmFsaWRhdG9yKCkgJiYgdGhpcy5nZXRNYXhMZW5ndGhWYWxpZGF0b3IoKVwiXG4gICAgICAgICAgICAgICAgICAgIGFsaWduPVwiZW5kXCI+e3sgdGhpcy5mb3JtRmllbGRJbnB1dFRleHQuZ2V0RGVmYXVsdFZhbHVlKCk/Lmxlbmd0aCB9fXt7IHRoaXMuZ2V0TWF4TGVuZ3RoVmFsaWRhdG9yKCk/Lmxlbmd0aCA/IFwiIC8gXCIgKyB0aGlzLmdldE1heExlbmd0aFZhbGlkYXRvcigpLmxlbmd0aDogJyd9fTwvbWF0LWhpbnQ+XG5cbiAgICAgICAgPC9tYXQtZm9ybS1maWVsZD5cblxuPC9zZWN0aW9uPiJdfQ==