import { Component, Input, ViewChild, forwardRef, } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import * as i0 from "@angular/core";
import * as i1 from "../../../service/device.service";
import * as i2 from "../../../service/ui.service";
import * as i3 from "@angular/common";
import * as i4 from "@angular/flex-layout/flex";
import * as i5 from "../../../directive/drag-drop.directive";
import * as i6 from "@angular/material/button";
import * as i7 from "@angular/material/list";
const _c0 = ["appDragDrop"];
const _c1 = ["file"];
function FormFieldInputFileComponent_section_0_mat_selection_list_4_mat_list_option_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-list-option", 12);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const item_r9 = ctx.$implicit;
    i0.ɵɵproperty("value", item_r9);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", item_r9.name, " ");
} }
function FormFieldInputFileComponent_section_0_mat_selection_list_4_button_3_Template(rf, ctx) { if (rf & 1) {
    const _r11 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 13);
    i0.ɵɵlistener("click", function FormFieldInputFileComponent_section_0_mat_selection_list_4_button_3_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r11); i0.ɵɵnextContext(); const _r6 = i0.ɵɵreference(1); const ctx_r10 = i0.ɵɵnextContext(2); return ctx_r10.onRemoveFiles(_r6.selectedOptions.selected); });
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r8 = i0.ɵɵnextContext(3);
    i0.ɵɵproperty("color", ctx_r8.formFieldInputFile.getButtonDeleteColor());
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r8.formFieldInputFile.getButtonDeleteText(), " ");
} }
function FormFieldInputFileComponent_section_0_mat_selection_list_4_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-selection-list", 8, 9);
    i0.ɵɵtemplate(2, FormFieldInputFileComponent_section_0_mat_selection_list_4_mat_list_option_2_Template, 2, 2, "mat-list-option", 10);
    i0.ɵɵtemplate(3, FormFieldInputFileComponent_section_0_mat_selection_list_4_button_3_Template, 2, 2, "button", 11);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const _r6 = i0.ɵɵreference(1);
    const ctx_r4 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngForOf", ctx_r4.formFieldInputFile.getDefaultValue());
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", _r6.selectedOptions.selected.length > 0);
} }
function FormFieldInputFileComponent_section_0_Template(rf, ctx) { if (rf & 1) {
    const _r13 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "section", 2, 3);
    i0.ɵɵlistener("onFileDropped", function FormFieldInputFileComponent_section_0_Template_section_onFileDropped_0_listener($event) { i0.ɵɵrestoreView(_r13); const ctx_r12 = i0.ɵɵnextContext(); return !ctx_r12.formFieldInputFile.getDisabled() && ctx_r12.onFileDropped($event); });
    i0.ɵɵelementStart(2, "h3");
    i0.ɵɵtext(3);
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(4, FormFieldInputFileComponent_section_0_mat_selection_list_4_Template, 4, 2, "mat-selection-list", 4);
    i0.ɵɵelementStart(5, "input", 5, 6);
    i0.ɵɵlistener("change", function FormFieldInputFileComponent_section_0_Template_input_change_5_listener() { i0.ɵɵrestoreView(_r13); const ctx_r14 = i0.ɵɵnextContext(); return ctx_r14.onFilesAdded(); });
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(7, "button", 7);
    i0.ɵɵlistener("click", function FormFieldInputFileComponent_section_0_Template_button_click_7_listener() { i0.ɵɵrestoreView(_r13); const ctx_r15 = i0.ɵɵnextContext(); return ctx_r15.addFiles(); });
    i0.ɵɵtext(8);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r0 = i0.ɵɵnextContext();
    var tmp_1_0 = null;
    const currVal_1 = ((tmp_1_0 = ctx_r0.formFieldInputFile.getDefaultValue()) == null ? null : tmp_1_0.length) > 0;
    i0.ɵɵadvance(3);
    i0.ɵɵtextInterpolate(ctx_r0.formFieldInputFile.getLabelName());
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", currVal_1);
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("color", ctx_r0.formFieldInputFile.getButtonAddColor())("disabled", ctx_r0.formFieldInputFile.getDisabled());
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r0.formFieldInputFile.getButtonAddText(), " ");
} }
function FormFieldInputFileComponent_section_1_mat_selection_list_3_mat_list_option_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-list-option", 12);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const item_r21 = ctx.$implicit;
    i0.ɵɵproperty("value", item_r21);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", item_r21.name, " ");
} }
function FormFieldInputFileComponent_section_1_mat_selection_list_3_button_3_Template(rf, ctx) { if (rf & 1) {
    const _r23 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 13);
    i0.ɵɵlistener("click", function FormFieldInputFileComponent_section_1_mat_selection_list_3_button_3_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r23); i0.ɵɵnextContext(); const _r18 = i0.ɵɵreference(1); const ctx_r22 = i0.ɵɵnextContext(2); return ctx_r22.onRemoveFiles(_r18.selectedOptions.selected); });
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r20 = i0.ɵɵnextContext(3);
    i0.ɵɵproperty("color", ctx_r20.formFieldInputFile.getButtonDeleteColor());
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r20.formFieldInputFile.getButtonDeleteText(), " ");
} }
function FormFieldInputFileComponent_section_1_mat_selection_list_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-selection-list", 8, 9);
    i0.ɵɵtemplate(2, FormFieldInputFileComponent_section_1_mat_selection_list_3_mat_list_option_2_Template, 2, 2, "mat-list-option", 10);
    i0.ɵɵtemplate(3, FormFieldInputFileComponent_section_1_mat_selection_list_3_button_3_Template, 2, 2, "button", 11);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const _r18 = i0.ɵɵreference(1);
    const ctx_r16 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngForOf", ctx_r16.formFieldInputFile.getDefaultValue());
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", _r18.selectedOptions.selected.length > 0);
} }
function FormFieldInputFileComponent_section_1_Template(rf, ctx) { if (rf & 1) {
    const _r25 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "section", 14);
    i0.ɵɵelementStart(1, "h3");
    i0.ɵɵtext(2);
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(3, FormFieldInputFileComponent_section_1_mat_selection_list_3_Template, 4, 2, "mat-selection-list", 4);
    i0.ɵɵelementStart(4, "input", 5, 6);
    i0.ɵɵlistener("change", function FormFieldInputFileComponent_section_1_Template_input_change_4_listener() { i0.ɵɵrestoreView(_r25); const ctx_r24 = i0.ɵɵnextContext(); return ctx_r24.onFilesAdded(); });
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(6, "button", 7);
    i0.ɵɵlistener("click", function FormFieldInputFileComponent_section_1_Template_button_click_6_listener() { i0.ɵɵrestoreView(_r25); const ctx_r26 = i0.ɵɵnextContext(); return ctx_r26.addFiles(); });
    i0.ɵɵtext(7);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r1 = i0.ɵɵnextContext();
    var tmp_1_0 = null;
    const currVal_1 = ((tmp_1_0 = ctx_r1.formFieldInputFile.getDefaultValue()) == null ? null : tmp_1_0.length) > 0;
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(ctx_r1.formFieldInputFile.getLabelName());
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", currVal_1);
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("color", ctx_r1.formFieldInputFile.getButtonAddColor())("disabled", ctx_r1.formFieldInputFile.getDisabled());
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r1.formFieldInputFile.getButtonAddText(), " ");
} }
function FormFieldInputFileComponent_section_2_mat_selection_list_3_mat_list_option_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-list-option", 12);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const item_r32 = ctx.$implicit;
    i0.ɵɵproperty("value", item_r32);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", item_r32.name, " ");
} }
function FormFieldInputFileComponent_section_2_mat_selection_list_3_button_3_Template(rf, ctx) { if (rf & 1) {
    const _r34 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 13);
    i0.ɵɵlistener("click", function FormFieldInputFileComponent_section_2_mat_selection_list_3_button_3_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r34); i0.ɵɵnextContext(); const _r29 = i0.ɵɵreference(1); const ctx_r33 = i0.ɵɵnextContext(2); return ctx_r33.onRemoveFiles(_r29.selectedOptions.selected); });
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r31 = i0.ɵɵnextContext(3);
    i0.ɵɵproperty("color", ctx_r31.formFieldInputFile.getButtonDeleteColor());
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r31.formFieldInputFile.getButtonDeleteText(), " ");
} }
function FormFieldInputFileComponent_section_2_mat_selection_list_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-selection-list", 8, 9);
    i0.ɵɵtemplate(2, FormFieldInputFileComponent_section_2_mat_selection_list_3_mat_list_option_2_Template, 2, 2, "mat-list-option", 10);
    i0.ɵɵtemplate(3, FormFieldInputFileComponent_section_2_mat_selection_list_3_button_3_Template, 2, 2, "button", 11);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const _r29 = i0.ɵɵreference(1);
    const ctx_r27 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngForOf", ctx_r27.formFieldInputFile.getDefaultValue());
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", _r29.selectedOptions.selected.length > 0);
} }
function FormFieldInputFileComponent_section_2_Template(rf, ctx) { if (rf & 1) {
    const _r36 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "section", 14);
    i0.ɵɵelementStart(1, "h3");
    i0.ɵɵtext(2);
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(3, FormFieldInputFileComponent_section_2_mat_selection_list_3_Template, 4, 2, "mat-selection-list", 4);
    i0.ɵɵelementStart(4, "input", 5, 6);
    i0.ɵɵlistener("change", function FormFieldInputFileComponent_section_2_Template_input_change_4_listener() { i0.ɵɵrestoreView(_r36); const ctx_r35 = i0.ɵɵnextContext(); return ctx_r35.onFilesAdded(); });
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(6, "button", 7);
    i0.ɵɵlistener("click", function FormFieldInputFileComponent_section_2_Template_button_click_6_listener() { i0.ɵɵrestoreView(_r36); const ctx_r37 = i0.ɵɵnextContext(); return ctx_r37.addFiles(); });
    i0.ɵɵtext(7);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r2 = i0.ɵɵnextContext();
    var tmp_1_0 = null;
    const currVal_1 = ((tmp_1_0 = ctx_r2.formFieldInputFile.getDefaultValue()) == null ? null : tmp_1_0.length) > 0;
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(ctx_r2.formFieldInputFile.getLabelName());
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", currVal_1);
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("color", ctx_r2.formFieldInputFile.getButtonAddColor())("disabled", ctx_r2.formFieldInputFile.getDisabled());
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r2.formFieldInputFile.getButtonAddText(), " ");
} }
export class FormFieldInputFileComponent {
    constructor(renderer, deviceService, uiService) {
        this.renderer = renderer;
        this.deviceService = deviceService;
        this.uiService = uiService;
        this.onChange = () => { };
        this.onTouched = () => { };
        this.onFilesAdded = () => {
            let files = this.file.nativeElement.files;
            for (let key in this.file.nativeElement.files) {
                if (!isNaN(parseInt(key))) {
                    // console.log(files[key]);
                    if (this.formFieldInputFile.isFileTypeAllowed(files[key].type))
                        this.formFieldInputFile.addFile(files[key]);
                    else
                        this.uiService.onShowSnackBar(files[key].type + ' is not allowed', null, 1500);
                }
            }
            if (this.formFieldInputFile.getDefaultValue().length > 0) {
                this.onChange(this.formFieldInputFile.getDefaultValue());
                this.onTouched();
            }
        };
        this.onFileDropped = (fileList) => {
            Array.from(fileList).forEach((file) => {
                if (this.formFieldInputFile.isFileTypeAllowed(file.type))
                    this.formFieldInputFile.addFile(file);
                else
                    this.uiService.onShowSnackBar(file.type + ' is not allowed', null, 1500);
            });
        };
        this.addFiles = () => {
            this.file.nativeElement.click();
        };
        this.onRemoveFiles = (matListOptions) => {
            for (let mLOindex in matListOptions) {
                let matListOption = matListOptions[mLOindex];
                this.formFieldInputFile.setDefaultValue(this.formFieldInputFile
                    .getDefaultValue()
                    .filter((f) => f !== matListOption.value));
            }
        };
    }
    registerOnChange(fn) {
        this.onChange = fn;
    }
    registerOnTouched(fn) {
        this.onTouched = fn;
    }
    writeValue(value) {
        if (value) {
            this.formFieldInputFile.setDefaultValue(value);
        }
    }
    ngOnInit() {
        // console.log('FormFieldInputFileComponent init');
    }
    ngOnDestroy() {
        // console.log('FormFieldInputFileComponent destroyed');
    }
    ngAfterViewInit() {
        if (this.appDragDrop)
            this.renderer.setStyle(this.appDragDrop.nativeElement, 'border', '2px dashed ' + this.formFieldInputFile.getBorderColor());
    }
}
FormFieldInputFileComponent.ɵfac = function FormFieldInputFileComponent_Factory(t) { return new (t || FormFieldInputFileComponent)(i0.ɵɵdirectiveInject(i0.Renderer2), i0.ɵɵdirectiveInject(i1.DeviceService), i0.ɵɵdirectiveInject(i2.UiService)); };
FormFieldInputFileComponent.ɵcmp = i0.ɵɵdefineComponent({ type: FormFieldInputFileComponent, selectors: [["app-form-field-input-file"]], viewQuery: function FormFieldInputFileComponent_Query(rf, ctx) { if (rf & 1) {
        i0.ɵɵviewQuery(_c0, true);
        i0.ɵɵviewQuery(_c1, true);
    } if (rf & 2) {
        var _t;
        i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.appDragDrop = _t.first);
        i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.file = _t.first);
    } }, inputs: { formFieldInputFile: "formFieldInputFile" }, features: [i0.ɵɵProvidersFeature([
            {
                provide: NG_VALUE_ACCESSOR,
                multi: true,
                useExisting: forwardRef(() => FormFieldInputFileComponent),
            },
        ])], decls: 3, vars: 3, consts: [["fxLayout", "column", "fxLayoutAlign", "center center", "fxLayoutGap", "10px", "class", "upload-file-container add-padding", "appDragDrop", "", 3, "onFileDropped", 4, "ngIf"], ["fxLayout", "column", "fxLayoutAlign", "center center", "fxLayoutGap", "10px", "class", "add-padding", 4, "ngIf"], ["fxLayout", "column", "fxLayoutAlign", "center center", "fxLayoutGap", "10px", "appDragDrop", "", 1, "upload-file-container", "add-padding", 3, "onFileDropped"], ["appDragDrop", ""], ["fxLayout", "column", "fxLayoutAlign", "center center", "fxLayoutGap", "8px", 4, "ngIf"], ["type", "file", "multiple", "", 3, "change"], ["file", ""], ["type", "button", "mat-raised-button", "", 3, "color", "disabled", "click"], ["fxLayout", "column", "fxLayoutAlign", "center center", "fxLayoutGap", "8px"], ["files", ""], [3, "value", 4, "ngFor", "ngForOf"], ["type", "button", "mat-button", "", 3, "color", "click", 4, "ngIf"], [3, "value"], ["type", "button", "mat-button", "", 3, "color", "click"], ["fxLayout", "column", "fxLayoutAlign", "center center", "fxLayoutGap", "10px", 1, "add-padding"]], template: function FormFieldInputFileComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵtemplate(0, FormFieldInputFileComponent_section_0_Template, 9, 5, "section", 0);
        i0.ɵɵtemplate(1, FormFieldInputFileComponent_section_1_Template, 8, 5, "section", 1);
        i0.ɵɵtemplate(2, FormFieldInputFileComponent_section_2_Template, 8, 5, "section", 1);
    } if (rf & 2) {
        i0.ɵɵproperty("ngIf", ctx.formFieldInputFile.getDragAndDrop() && ctx.deviceService.isDeviceDesktop());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.formFieldInputFile.getDragAndDrop() && !ctx.deviceService.isDeviceDesktop());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", !ctx.formFieldInputFile.getDragAndDrop());
    } }, directives: [i3.NgIf, i4.DefaultLayoutDirective, i4.DefaultLayoutAlignDirective, i4.DefaultLayoutGapDirective, i5.DragDropDirective, i6.MatButton, i7.MatSelectionList, i3.NgForOf, i7.MatListOption], styles: ["", ".add-padding[_ngcontent-%COMP%] {\n        padding: 20px;\n    }\n\n    .cursor[_ngcontent-%COMP%] {\n        cursor: pointer;\n    }\n\n    input[type=\"file\"][_ngcontent-%COMP%] {\n        display: none;\n    }\n\n    h3[_ngcontent-%COMP%] {\n        font-size: inherit;\n        margin: 0;\n        font-weight: inherit;\n    }\n\n    .upload-file-container[_ngcontent-%COMP%] {\n        min-height: 200px;\n        \n        \n        margin: 5px auto;        \n        border-radius: 10px;\n    }\n\n    mat-form-field[_ngcontent-%COMP%] {\n        min-width: 250px;\n    }"] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FormFieldInputFileComponent, [{
        type: Component,
        args: [{
                selector: 'app-form-field-input-file',
                templateUrl: './form-field-input-file.component.html',
                styleUrls: ['./form-field-input-file.component.css'],
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        multi: true,
                        useExisting: forwardRef(() => FormFieldInputFileComponent),
                    },
                ],
            }]
    }], function () { return [{ type: i0.Renderer2 }, { type: i1.DeviceService }, { type: i2.UiService }]; }, { appDragDrop: [{
            type: ViewChild,
            args: ['appDragDrop', { static: false }]
        }], file: [{
            type: ViewChild,
            args: ['file', { static: false }]
        }], formFieldInputFile: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1maWVsZC1pbnB1dC1maWxlLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215LWFuZ3VsYXItZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvdWkvZm9ybS1maWVsZC1pbnB1dC9mb3JtLWZpZWxkLWlucHV0LWZpbGUvZm9ybS1maWVsZC1pbnB1dC1maWxlLmNvbXBvbmVudC50cyIsImxpYi9zaGFyZWQvdWkvZm9ybS1maWVsZC1pbnB1dC9mb3JtLWZpZWxkLWlucHV0LWZpbGUvZm9ybS1maWVsZC1pbnB1dC1maWxlLmNvbXBvbmVudC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFDTCxTQUFTLEVBSVQsS0FBSyxFQUNMLFNBQVMsRUFHVCxVQUFVLEdBQ1gsTUFBTSxlQUFlLENBQUM7QUFFdkIsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sZ0JBQWdCLENBQUM7Ozs7Ozs7Ozs7OztJQzJDbkMsMkNBR1E7SUFBQSxZQUNSO0lBQUEsaUJBQWtCOzs7SUFGZCwrQkFBYztJQUNWLGVBQ1I7SUFEUSw2Q0FDUjs7OztJQUVBLGtDQU1RO0lBRkosdVRBQTREO0lBRXhELFlBQ1I7SUFBQSxpQkFBUzs7O0lBSkwsd0VBQXdEO0lBR3BELGVBQ1I7SUFEUSxnRkFDUjs7O0lBcEJSLGdEQU9RO0lBQUEsb0lBR1E7SUFHUixrSEFNUTtJQUVoQixpQkFBcUI7Ozs7SUFiVCxlQUE4RDtJQUE5RCxxRUFBOEQ7SUFNOUQsZUFBaUQ7SUFBakQsOERBQWlEOzs7O0lBM0JyRSxxQ0FXUTtJQUZKLHNNQUFrQix3Q0FBcUMscUNBQStCO0lBRWxGLDBCQUFJO0lBQUEsWUFBNEM7SUFBQSxpQkFBSztJQUVyRCxvSEFPUTtJQWdCUixtQ0FNQTtJQUhJLHlNQUF5QjtJQUg3QixpQkFNQTtJQUFBLGlDQU1RO0lBSkosb01BQW9CO0lBSWhCLFlBQ1I7SUFBQSxpQkFBUztJQUVqQixpQkFBVTs7Ozs7SUF4Q0UsZUFBNEM7SUFBNUMsOERBQTRDO0lBSTVDLGVBQTZEO0lBQTdELGdDQUE2RDtJQThCN0QsZUFBcUQ7SUFBckQscUVBQXFELHFEQUFBO0lBR2pELGVBQ1I7SUFEUSw2RUFDUjs7O0lBcUJRLDJDQUdRO0lBQUEsWUFDUjtJQUFBLGlCQUFrQjs7O0lBRmQsZ0NBQWM7SUFDVixlQUNSO0lBRFEsOENBQ1I7Ozs7SUFFQSxrQ0FNUTtJQUZKLHlUQUE0RDtJQUV4RCxZQUNSO0lBQUEsaUJBQVM7OztJQUpMLHlFQUF3RDtJQUdwRCxlQUNSO0lBRFEsaUZBQ1I7OztJQXBCUixnREFPUTtJQUFBLG9JQUdRO0lBR1Isa0hBTVE7SUFFaEIsaUJBQXFCOzs7O0lBYlQsZUFBOEQ7SUFBOUQsc0VBQThEO0lBTTlELGVBQWlEO0lBQWpELCtEQUFpRDs7OztJQXZCckUsbUNBT1E7SUFBQSwwQkFBSTtJQUFBLFlBQTRDO0lBQUEsaUJBQUs7SUFFckQsb0hBT1E7SUFnQlIsbUNBTUE7SUFISSx5TUFBeUI7SUFIN0IsaUJBTUE7SUFBQSxpQ0FNUTtJQUpKLG9NQUFvQjtJQUloQixZQUNSO0lBQUEsaUJBQVM7SUFFakIsaUJBQVU7Ozs7O0lBeENFLGVBQTRDO0lBQTVDLDhEQUE0QztJQUk1QyxlQUE2RDtJQUE3RCxnQ0FBNkQ7SUE4QjdELGVBQXFEO0lBQXJELHFFQUFxRCxxREFBQTtJQUdqRCxlQUNSO0lBRFEsNkVBQ1I7OztJQXFCUSwyQ0FHUTtJQUFBLFlBQ1I7SUFBQSxpQkFBa0I7OztJQUZkLGdDQUFjO0lBQ1YsZUFDUjtJQURRLDhDQUNSOzs7O0lBRUEsa0NBTVE7SUFGSix5VEFBNEQ7SUFFeEQsWUFDUjtJQUFBLGlCQUFTOzs7SUFKTCx5RUFBd0Q7SUFHcEQsZUFDUjtJQURRLGlGQUNSOzs7SUFwQlIsZ0RBT1E7SUFBQSxvSUFHUTtJQUdSLGtIQU1RO0lBRWhCLGlCQUFxQjs7OztJQWJULGVBQThEO0lBQTlELHNFQUE4RDtJQU05RCxlQUFpRDtJQUFqRCwrREFBaUQ7Ozs7SUF2QnJFLG1DQU9RO0lBQUEsMEJBQUk7SUFBQSxZQUE0QztJQUFBLGlCQUFLO0lBRXJELG9IQU9RO0lBZ0JSLG1DQU1BO0lBSEkseU1BQXlCO0lBSDdCLGlCQU1BO0lBQUEsaUNBTVE7SUFKSixvTUFBb0I7SUFJaEIsWUFDUjtJQUFBLGlCQUFTO0lBRWpCLGlCQUFVOzs7OztJQXhDRSxlQUE0QztJQUE1Qyw4REFBNEM7SUFJNUMsZUFBNkQ7SUFBN0QsZ0NBQTZEO0lBOEI3RCxlQUFxRDtJQUFyRCxxRUFBcUQscURBQUE7SUFHakQsZUFDUjtJQURRLDZFQUNSOztBRHZKUixNQUFNLE9BQU8sMkJBQTJCO0lBU3RDLFlBQ1UsUUFBbUIsRUFDcEIsYUFBNEIsRUFDM0IsU0FBb0I7UUFGcEIsYUFBUSxHQUFSLFFBQVEsQ0FBVztRQUNwQixrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQUMzQixjQUFTLEdBQVQsU0FBUyxDQUFXO1FBTnZCLGFBQVEsR0FBUSxHQUFHLEVBQUUsR0FBRSxDQUFDLENBQUM7UUFDekIsY0FBUyxHQUFRLEdBQUcsRUFBRSxHQUFFLENBQUMsQ0FBQztRQXNCakMsaUJBQVksR0FBRyxHQUFTLEVBQUU7WUFDeEIsSUFBSSxLQUFLLEdBQTRCLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQztZQUVuRSxLQUFLLElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRTtnQkFDN0MsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRTtvQkFDekIsMkJBQTJCO29CQUMzQixJQUFJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDO3dCQUM1RCxJQUFJLENBQUMsa0JBQWtCLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDOzt3QkFFNUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQzNCLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLEdBQUcsaUJBQWlCLEVBQ25DLElBQUksRUFDSixJQUFJLENBQ0wsQ0FBQztpQkFDTDthQUNGO1lBRUQsSUFBSSxJQUFJLENBQUMsa0JBQWtCLENBQUMsZUFBZSxFQUFFLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDeEQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsZUFBZSxFQUFFLENBQUMsQ0FBQztnQkFDekQsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO2FBQ2xCO1FBQ0gsQ0FBQyxDQUFDO1FBRUYsa0JBQWEsR0FBRyxDQUFDLFFBQWtCLEVBQVEsRUFBRTtZQUMzQyxLQUFLLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksRUFBRSxFQUFFO2dCQUNwQyxJQUFJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO29CQUN0RCxJQUFJLENBQUMsa0JBQWtCLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDOztvQkFFdEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQzNCLElBQUksQ0FBQyxJQUFJLEdBQUcsaUJBQWlCLEVBQzdCLElBQUksRUFDSixJQUFJLENBQ0wsQ0FBQztZQUNOLENBQUMsQ0FBQyxDQUFDO1FBQ0wsQ0FBQyxDQUFDO1FBRUYsYUFBUSxHQUFHLEdBQVMsRUFBRTtZQUNwQixJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNsQyxDQUFDLENBQUM7UUFFRixrQkFBYSxHQUFHLENBQUMsY0FBK0IsRUFBUSxFQUFFO1lBQ3hELEtBQUssSUFBSSxRQUFRLElBQUksY0FBYyxFQUFFO2dCQUNuQyxJQUFJLGFBQWEsR0FBa0IsY0FBYyxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUM1RCxJQUFJLENBQUMsa0JBQWtCLENBQUMsZUFBZSxDQUNyQyxJQUFJLENBQUMsa0JBQWtCO3FCQUNwQixlQUFlLEVBQUU7cUJBQ2pCLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxLQUFLLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FDNUMsQ0FBQzthQUNIO1FBQ0gsQ0FBQyxDQUFDO0lBakVDLENBQUM7SUFFSixnQkFBZ0IsQ0FBQyxFQUFFO1FBQ2pCLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO0lBQ3JCLENBQUM7SUFFRCxpQkFBaUIsQ0FBQyxFQUFFO1FBQ2xCLElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO0lBQ3RCLENBQUM7SUFFRCxVQUFVLENBQUMsS0FBSztRQUNkLElBQUksS0FBSyxFQUFFO1lBQ1QsSUFBSSxDQUFDLGtCQUFrQixDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUNoRDtJQUNILENBQUM7SUFxREQsUUFBUTtRQUNOLG1EQUFtRDtJQUNyRCxDQUFDO0lBRUQsV0FBVztRQUNULHdEQUF3RDtJQUMxRCxDQUFDO0lBRUQsZUFBZTtRQUNiLElBQUksSUFBSSxDQUFDLFdBQVc7WUFDbEIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQ3BCLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUM5QixRQUFRLEVBQ1IsYUFBYSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxjQUFjLEVBQUUsQ0FDekQsQ0FBQztJQUNOLENBQUM7O3NHQS9GVSwyQkFBMkI7Z0VBQTNCLDJCQUEyQjs7Ozs7OztnR0FSM0I7WUFDVDtnQkFDRSxPQUFPLEVBQUUsaUJBQWlCO2dCQUMxQixLQUFLLEVBQUUsSUFBSTtnQkFDWCxXQUFXLEVBQUUsVUFBVSxDQUFDLEdBQUcsRUFBRSxDQUFDLDJCQUEyQixDQUFDO2FBQzNEO1NBQ0Y7UUNJSCxvRkFXUTtRQTJDUixvRkFPUTtRQTJDUixvRkFPUTs7UUE5R0oscUdBQXdGO1FBc0R4RixlQUF5RjtRQUF6RixzR0FBeUY7UUFrRHpGLGVBQWlEO1FBQWpELCtEQUFpRDs7a0REM0d4QywyQkFBMkI7Y0FadkMsU0FBUztlQUFDO2dCQUNULFFBQVEsRUFBRSwyQkFBMkI7Z0JBQ3JDLFdBQVcsRUFBRSx3Q0FBd0M7Z0JBQ3JELFNBQVMsRUFBRSxDQUFDLHVDQUF1QyxDQUFDO2dCQUNwRCxTQUFTLEVBQUU7b0JBQ1Q7d0JBQ0UsT0FBTyxFQUFFLGlCQUFpQjt3QkFDMUIsS0FBSyxFQUFFLElBQUk7d0JBQ1gsV0FBVyxFQUFFLFVBQVUsQ0FBQyxHQUFHLEVBQUUsQ0FBQywyQkFBMkIsQ0FBQztxQkFDM0Q7aUJBQ0Y7YUFDRjs7a0JBR0UsU0FBUzttQkFBQyxhQUFhLEVBQUUsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFOztrQkFDMUMsU0FBUzttQkFBQyxNQUFNLEVBQUUsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFOztrQkFDbkMsS0FBSyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XG4gIENvbXBvbmVudCxcbiAgT25Jbml0LFxuICBPbkRlc3Ryb3ksXG4gIEFmdGVyVmlld0luaXQsXG4gIElucHV0LFxuICBWaWV3Q2hpbGQsXG4gIEVsZW1lbnRSZWYsXG4gIFJlbmRlcmVyMixcbiAgZm9yd2FyZFJlZixcbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBNYXRMaXN0T3B0aW9uIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvbGlzdCc7XG5pbXBvcnQgeyBOR19WQUxVRV9BQ0NFU1NPUiB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcblxuLy8gbW9kZWxcbmltcG9ydCB7IEZvcm1GaWVsZElucHV0RmlsZSB9IGZyb20gJy4uLy4uLy4uL21vZGVsL3N0cnVjdHVyYWwvY29tcG9zaXRlL2Zvcm0vZm9ybS1maWVsZC1pbnB1dC9mb3JtLWZpZWxkLWlucHV0LWZpbGUubW9kZWwnO1xuXG4vLyBzZXJ2aWNlXG5pbXBvcnQgeyBEZXZpY2VTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vc2VydmljZS9kZXZpY2Uuc2VydmljZSc7XG5pbXBvcnQgeyBVaVNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9zZXJ2aWNlL3VpLnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdhcHAtZm9ybS1maWVsZC1pbnB1dC1maWxlJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2Zvcm0tZmllbGQtaW5wdXQtZmlsZS5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL2Zvcm0tZmllbGQtaW5wdXQtZmlsZS5jb21wb25lbnQuY3NzJ10sXG4gIHByb3ZpZGVyczogW1xuICAgIHtcbiAgICAgIHByb3ZpZGU6IE5HX1ZBTFVFX0FDQ0VTU09SLFxuICAgICAgbXVsdGk6IHRydWUsXG4gICAgICB1c2VFeGlzdGluZzogZm9yd2FyZFJlZigoKSA9PiBGb3JtRmllbGRJbnB1dEZpbGVDb21wb25lbnQpLFxuICAgIH0sXG4gIF0sXG59KVxuZXhwb3J0IGNsYXNzIEZvcm1GaWVsZElucHV0RmlsZUNvbXBvbmVudFxuICBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95LCBBZnRlclZpZXdJbml0IHtcbiAgQFZpZXdDaGlsZCgnYXBwRHJhZ0Ryb3AnLCB7IHN0YXRpYzogZmFsc2UgfSkgYXBwRHJhZ0Ryb3A6IEVsZW1lbnRSZWY7XG4gIEBWaWV3Q2hpbGQoJ2ZpbGUnLCB7IHN0YXRpYzogZmFsc2UgfSkgZmlsZTogRWxlbWVudFJlZjtcbiAgQElucHV0KCkgZm9ybUZpZWxkSW5wdXRGaWxlOiBGb3JtRmllbGRJbnB1dEZpbGU7XG5cbiAgcHVibGljIG9uQ2hhbmdlOiBhbnkgPSAoKSA9PiB7fTtcbiAgcHVibGljIG9uVG91Y2hlZDogYW55ID0gKCkgPT4ge307XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSByZW5kZXJlcjogUmVuZGVyZXIyLFxuICAgIHB1YmxpYyBkZXZpY2VTZXJ2aWNlOiBEZXZpY2VTZXJ2aWNlLFxuICAgIHByaXZhdGUgdWlTZXJ2aWNlOiBVaVNlcnZpY2VcbiAgKSB7fVxuXG4gIHJlZ2lzdGVyT25DaGFuZ2UoZm4pIHtcbiAgICB0aGlzLm9uQ2hhbmdlID0gZm47XG4gIH1cblxuICByZWdpc3Rlck9uVG91Y2hlZChmbikge1xuICAgIHRoaXMub25Ub3VjaGVkID0gZm47XG4gIH1cblxuICB3cml0ZVZhbHVlKHZhbHVlKSB7XG4gICAgaWYgKHZhbHVlKSB7XG4gICAgICB0aGlzLmZvcm1GaWVsZElucHV0RmlsZS5zZXREZWZhdWx0VmFsdWUodmFsdWUpO1xuICAgIH1cbiAgfVxuXG4gIG9uRmlsZXNBZGRlZCA9ICgpOiB2b2lkID0+IHtcbiAgICBsZXQgZmlsZXM6IHsgW2tleTogc3RyaW5nXTogRmlsZSB9ID0gdGhpcy5maWxlLm5hdGl2ZUVsZW1lbnQuZmlsZXM7XG5cbiAgICBmb3IgKGxldCBrZXkgaW4gdGhpcy5maWxlLm5hdGl2ZUVsZW1lbnQuZmlsZXMpIHtcbiAgICAgIGlmICghaXNOYU4ocGFyc2VJbnQoa2V5KSkpIHtcbiAgICAgICAgLy8gY29uc29sZS5sb2coZmlsZXNba2V5XSk7XG4gICAgICAgIGlmICh0aGlzLmZvcm1GaWVsZElucHV0RmlsZS5pc0ZpbGVUeXBlQWxsb3dlZChmaWxlc1trZXldLnR5cGUpKVxuICAgICAgICAgIHRoaXMuZm9ybUZpZWxkSW5wdXRGaWxlLmFkZEZpbGUoZmlsZXNba2V5XSk7XG4gICAgICAgIGVsc2VcbiAgICAgICAgICB0aGlzLnVpU2VydmljZS5vblNob3dTbmFja0JhcihcbiAgICAgICAgICAgIGZpbGVzW2tleV0udHlwZSArICcgaXMgbm90IGFsbG93ZWQnLFxuICAgICAgICAgICAgbnVsbCxcbiAgICAgICAgICAgIDE1MDBcbiAgICAgICAgICApO1xuICAgICAgfVxuICAgIH1cblxuICAgIGlmICh0aGlzLmZvcm1GaWVsZElucHV0RmlsZS5nZXREZWZhdWx0VmFsdWUoKS5sZW5ndGggPiAwKSB7XG4gICAgICB0aGlzLm9uQ2hhbmdlKHRoaXMuZm9ybUZpZWxkSW5wdXRGaWxlLmdldERlZmF1bHRWYWx1ZSgpKTtcbiAgICAgIHRoaXMub25Ub3VjaGVkKCk7XG4gICAgfVxuICB9O1xuXG4gIG9uRmlsZURyb3BwZWQgPSAoZmlsZUxpc3Q6IEZpbGVMaXN0KTogdm9pZCA9PiB7XG4gICAgQXJyYXkuZnJvbShmaWxlTGlzdCkuZm9yRWFjaCgoZmlsZSkgPT4ge1xuICAgICAgaWYgKHRoaXMuZm9ybUZpZWxkSW5wdXRGaWxlLmlzRmlsZVR5cGVBbGxvd2VkKGZpbGUudHlwZSkpXG4gICAgICAgIHRoaXMuZm9ybUZpZWxkSW5wdXRGaWxlLmFkZEZpbGUoZmlsZSk7XG4gICAgICBlbHNlXG4gICAgICAgIHRoaXMudWlTZXJ2aWNlLm9uU2hvd1NuYWNrQmFyKFxuICAgICAgICAgIGZpbGUudHlwZSArICcgaXMgbm90IGFsbG93ZWQnLFxuICAgICAgICAgIG51bGwsXG4gICAgICAgICAgMTUwMFxuICAgICAgICApO1xuICAgIH0pO1xuICB9O1xuXG4gIGFkZEZpbGVzID0gKCk6IHZvaWQgPT4ge1xuICAgIHRoaXMuZmlsZS5uYXRpdmVFbGVtZW50LmNsaWNrKCk7XG4gIH07XG5cbiAgb25SZW1vdmVGaWxlcyA9IChtYXRMaXN0T3B0aW9uczogTWF0TGlzdE9wdGlvbltdKTogdm9pZCA9PiB7XG4gICAgZm9yIChsZXQgbUxPaW5kZXggaW4gbWF0TGlzdE9wdGlvbnMpIHtcbiAgICAgIGxldCBtYXRMaXN0T3B0aW9uOiBNYXRMaXN0T3B0aW9uID0gbWF0TGlzdE9wdGlvbnNbbUxPaW5kZXhdO1xuICAgICAgdGhpcy5mb3JtRmllbGRJbnB1dEZpbGUuc2V0RGVmYXVsdFZhbHVlKFxuICAgICAgICB0aGlzLmZvcm1GaWVsZElucHV0RmlsZVxuICAgICAgICAgIC5nZXREZWZhdWx0VmFsdWUoKVxuICAgICAgICAgIC5maWx0ZXIoKGYpID0+IGYgIT09IG1hdExpc3RPcHRpb24udmFsdWUpXG4gICAgICApO1xuICAgIH1cbiAgfTtcblxuICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgICAvLyBjb25zb2xlLmxvZygnRm9ybUZpZWxkSW5wdXRGaWxlQ29tcG9uZW50IGluaXQnKTtcbiAgfVxuXG4gIG5nT25EZXN0cm95KCk6IHZvaWQge1xuICAgIC8vIGNvbnNvbGUubG9nKCdGb3JtRmllbGRJbnB1dEZpbGVDb21wb25lbnQgZGVzdHJveWVkJyk7XG4gIH1cblxuICBuZ0FmdGVyVmlld0luaXQoKTogdm9pZCB7XG4gICAgaWYgKHRoaXMuYXBwRHJhZ0Ryb3ApXG4gICAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKFxuICAgICAgICB0aGlzLmFwcERyYWdEcm9wLm5hdGl2ZUVsZW1lbnQsXG4gICAgICAgICdib3JkZXInLFxuICAgICAgICAnMnB4IGRhc2hlZCAnICsgdGhpcy5mb3JtRmllbGRJbnB1dEZpbGUuZ2V0Qm9yZGVyQ29sb3IoKVxuICAgICAgKTtcbiAgfVxufVxuIiwiPHN0eWxlPlxuICAgIFxuICAgIC5hZGQtcGFkZGluZyB7XG4gICAgICAgIHBhZGRpbmc6IDIwcHg7XG4gICAgfVxuXG4gICAgLmN1cnNvciB7XG4gICAgICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICB9XG5cbiAgICBpbnB1dFt0eXBlPVwiZmlsZVwiXSB7XG4gICAgICAgIGRpc3BsYXk6IG5vbmU7XG4gICAgfVxuXG4gICAgaDMge1xuICAgICAgICBmb250LXNpemU6IGluaGVyaXQ7XG4gICAgICAgIG1hcmdpbjogMDtcbiAgICAgICAgZm9udC13ZWlnaHQ6IGluaGVyaXQ7XG4gICAgfVxuXG4gICAgLnVwbG9hZC1maWxlLWNvbnRhaW5lciB7XG4gICAgICAgIG1pbi1oZWlnaHQ6IDIwMHB4O1xuICAgICAgICAvKiBtYXgtd2lkdGg6IDMwMHB4OyAqL1xuICAgICAgICAvKiB3aWR0aDogODAlOyAqL1xuICAgICAgICBtYXJnaW46IDVweCBhdXRvOyAgICAgICAgXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gICAgfVxuXG4gICAgbWF0LWZvcm0tZmllbGQge1xuICAgICAgICBtaW4td2lkdGg6IDI1MHB4O1xuICAgIH1cblxuPC9zdHlsZT5cblxuPCEtLSAtLS0tLS0tLS0tLS0tLS0tLS0tIGZvcm0gZmllbGQgaW5wdXQgZmlsZSB3aXRoIGRyYWcgYW5kIGRyb3AgYW5kIGRldmljZSBpcyBkZXNrdG9wIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tIC0tPlxuPHNlY3Rpb25cbiAgICAqbmdJZj1cInRoaXMuZm9ybUZpZWxkSW5wdXRGaWxlLmdldERyYWdBbmREcm9wKCkgJiYgdGhpcy5kZXZpY2VTZXJ2aWNlLmlzRGV2aWNlRGVza3RvcCgpXCJcbiAgICBmeExheW91dD1cImNvbHVtblwiXG4gICAgZnhMYXlvdXRBbGlnbj1cImNlbnRlciBjZW50ZXJcIlxuICAgIGZ4TGF5b3V0R2FwPVwiMTBweFwiXG5cbiAgICBjbGFzcz1cInVwbG9hZC1maWxlLWNvbnRhaW5lciBhZGQtcGFkZGluZ1wiXG4gICAgYXBwRHJhZ0Ryb3BcbiAgICAjYXBwRHJhZ0Ryb3BcbiAgICAob25GaWxlRHJvcHBlZCk9XCIhdGhpcy5mb3JtRmllbGRJbnB1dEZpbGUuZ2V0RGlzYWJsZWQoKSAmJiB0aGlzLm9uRmlsZURyb3BwZWQoJGV2ZW50KVwiPlxuXG4gICAgICAgIDxoMz57eyB0aGlzLmZvcm1GaWVsZElucHV0RmlsZS5nZXRMYWJlbE5hbWUoKSB9fTwvaDM+XG5cbiAgICAgICAgPG1hdC1zZWxlY3Rpb24tbGlzdCBcbiAgICAgICAgICAgICNmaWxlc1xuICAgICAgICAgICAgKm5nSWY9XCJ0aGlzLmZvcm1GaWVsZElucHV0RmlsZS5nZXREZWZhdWx0VmFsdWUoKT8ubGVuZ3RoID4gMFwiXG4gICAgICAgICAgICBmeExheW91dD1cImNvbHVtblwiXG4gICAgICAgICAgICBmeExheW91dEFsaWduPVwiY2VudGVyIGNlbnRlclwiXG4gICAgICAgICAgICBmeExheW91dEdhcD1cIjhweFwiPlxuXG4gICAgICAgICAgICAgICAgPG1hdC1saXN0LW9wdGlvbiBcbiAgICAgICAgICAgICAgICAgICAgKm5nRm9yPVwibGV0IGl0ZW0gb2YgdGhpcy5mb3JtRmllbGRJbnB1dEZpbGUuZ2V0RGVmYXVsdFZhbHVlKClcIiBcbiAgICAgICAgICAgICAgICAgICAgW3ZhbHVlXT1cIml0ZW1cIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIHt7IGl0ZW0ubmFtZSB9fVxuICAgICAgICAgICAgICAgIDwvbWF0LWxpc3Qtb3B0aW9uPlxuXG4gICAgICAgICAgICAgICAgPGJ1dHRvblxuICAgICAgICAgICAgICAgICAgICAqbmdJZj1cImZpbGVzLnNlbGVjdGVkT3B0aW9ucy5zZWxlY3RlZC5sZW5ndGggPiAwXCJcbiAgICAgICAgICAgICAgICAgICAgdHlwZT1cImJ1dHRvblwiXG4gICAgICAgICAgICAgICAgICAgIFtjb2xvcl09XCJ0aGlzLmZvcm1GaWVsZElucHV0RmlsZS5nZXRCdXR0b25EZWxldGVDb2xvcigpXCJcbiAgICAgICAgICAgICAgICAgICAgKGNsaWNrKT1cInRoaXMub25SZW1vdmVGaWxlcyhmaWxlcy5zZWxlY3RlZE9wdGlvbnMuc2VsZWN0ZWQpXCJcbiAgICAgICAgICAgICAgICAgICAgbWF0LWJ1dHRvbj5cbiAgICAgICAgICAgICAgICAgICAgICAgIHt7IHRoaXMuZm9ybUZpZWxkSW5wdXRGaWxlLmdldEJ1dHRvbkRlbGV0ZVRleHQoKSB9fVxuICAgICAgICAgICAgICAgIDwvYnV0dG9uPlxuICAgICAgICA8L21hdC1zZWxlY3Rpb24tbGlzdD4gICAgXG5cbiAgICAgICAgPGlucHV0IFxuICAgICAgICAgICAgdHlwZT1cImZpbGVcIiBcbiAgICAgICAgICAgICNmaWxlIFxuICAgICAgICAgICAgKGNoYW5nZSk9XCJvbkZpbGVzQWRkZWQoKVwiICAgICAgICBcbiAgICAgICAgICAgIG11bHRpcGxlLz5cblxuICAgICAgICA8YnV0dG9uIFxuICAgICAgICAgICAgdHlwZT1cImJ1dHRvblwiXG4gICAgICAgICAgICAoY2xpY2spPVwiYWRkRmlsZXMoKVwiXG4gICAgICAgICAgICBbY29sb3JdPVwidGhpcy5mb3JtRmllbGRJbnB1dEZpbGUuZ2V0QnV0dG9uQWRkQ29sb3IoKVwiXG4gICAgICAgICAgICBtYXQtcmFpc2VkLWJ1dHRvblxuICAgICAgICAgICAgW2Rpc2FibGVkXT1cInRoaXMuZm9ybUZpZWxkSW5wdXRGaWxlLmdldERpc2FibGVkKClcIj5cbiAgICAgICAgICAgICAgICB7eyB0aGlzLmZvcm1GaWVsZElucHV0RmlsZS5nZXRCdXR0b25BZGRUZXh0KCkgfX1cbiAgICAgICAgPC9idXR0b24+XG5cbjwvc2VjdGlvbj5cblxuPCEtLSAtLS0tLS0tLS0tLS0tLS0tLS0tIGZvcm0gZmllbGQgaW5wdXQgZmlsZSB3aXRoIGRyYWcgYW5kIGRyb3AgYW5kIGRldmljZSBpcyBub3QgZGVza3RvcCAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSAtLT5cbjxzZWN0aW9uXG4gICAgKm5nSWY9XCJ0aGlzLmZvcm1GaWVsZElucHV0RmlsZS5nZXREcmFnQW5kRHJvcCgpICYmICF0aGlzLmRldmljZVNlcnZpY2UuaXNEZXZpY2VEZXNrdG9wKClcIlxuICAgIGZ4TGF5b3V0PVwiY29sdW1uXCJcbiAgICBmeExheW91dEFsaWduPVwiY2VudGVyIGNlbnRlclwiXG4gICAgZnhMYXlvdXRHYXA9XCIxMHB4XCJcbiAgICBjbGFzcz1cImFkZC1wYWRkaW5nXCI+XG5cbiAgICAgICAgPGgzPnt7IHRoaXMuZm9ybUZpZWxkSW5wdXRGaWxlLmdldExhYmVsTmFtZSgpIH19PC9oMz5cblxuICAgICAgICA8bWF0LXNlbGVjdGlvbi1saXN0IFxuICAgICAgICAgICAgI2ZpbGVzXG4gICAgICAgICAgICAqbmdJZj1cInRoaXMuZm9ybUZpZWxkSW5wdXRGaWxlLmdldERlZmF1bHRWYWx1ZSgpPy5sZW5ndGggPiAwXCJcbiAgICAgICAgICAgIGZ4TGF5b3V0PVwiY29sdW1uXCJcbiAgICAgICAgICAgIGZ4TGF5b3V0QWxpZ249XCJjZW50ZXIgY2VudGVyXCJcbiAgICAgICAgICAgIGZ4TGF5b3V0R2FwPVwiOHB4XCI+XG5cbiAgICAgICAgICAgICAgICA8bWF0LWxpc3Qtb3B0aW9uIFxuICAgICAgICAgICAgICAgICAgICAqbmdGb3I9XCJsZXQgaXRlbSBvZiB0aGlzLmZvcm1GaWVsZElucHV0RmlsZS5nZXREZWZhdWx0VmFsdWUoKVwiIFxuICAgICAgICAgICAgICAgICAgICBbdmFsdWVdPVwiaXRlbVwiPlxuICAgICAgICAgICAgICAgICAgICAgICAge3sgaXRlbS5uYW1lIH19XG4gICAgICAgICAgICAgICAgPC9tYXQtbGlzdC1vcHRpb24+XG5cbiAgICAgICAgICAgICAgICA8YnV0dG9uXG4gICAgICAgICAgICAgICAgICAgICpuZ0lmPVwiZmlsZXMuc2VsZWN0ZWRPcHRpb25zLnNlbGVjdGVkLmxlbmd0aCA+IDBcIlxuICAgICAgICAgICAgICAgICAgICB0eXBlPVwiYnV0dG9uXCJcbiAgICAgICAgICAgICAgICAgICAgW2NvbG9yXT1cInRoaXMuZm9ybUZpZWxkSW5wdXRGaWxlLmdldEJ1dHRvbkRlbGV0ZUNvbG9yKClcIlxuICAgICAgICAgICAgICAgICAgICAoY2xpY2spPVwidGhpcy5vblJlbW92ZUZpbGVzKGZpbGVzLnNlbGVjdGVkT3B0aW9ucy5zZWxlY3RlZClcIlxuICAgICAgICAgICAgICAgICAgICBtYXQtYnV0dG9uPlxuICAgICAgICAgICAgICAgICAgICAgICAge3sgdGhpcy5mb3JtRmllbGRJbnB1dEZpbGUuZ2V0QnV0dG9uRGVsZXRlVGV4dCgpIH19XG4gICAgICAgICAgICAgICAgPC9idXR0b24+XG4gICAgICAgIDwvbWF0LXNlbGVjdGlvbi1saXN0PiAgICBcblxuICAgICAgICA8aW5wdXQgXG4gICAgICAgICAgICB0eXBlPVwiZmlsZVwiIFxuICAgICAgICAgICAgI2ZpbGUgXG4gICAgICAgICAgICAoY2hhbmdlKT1cIm9uRmlsZXNBZGRlZCgpXCIgICAgICAgIFxuICAgICAgICAgICAgbXVsdGlwbGUgLz5cblxuICAgICAgICA8YnV0dG9uIFxuICAgICAgICAgICAgdHlwZT1cImJ1dHRvblwiXG4gICAgICAgICAgICAoY2xpY2spPVwiYWRkRmlsZXMoKVwiXG4gICAgICAgICAgICBbY29sb3JdPVwidGhpcy5mb3JtRmllbGRJbnB1dEZpbGUuZ2V0QnV0dG9uQWRkQ29sb3IoKVwiXG4gICAgICAgICAgICBtYXQtcmFpc2VkLWJ1dHRvblxuICAgICAgICAgICAgW2Rpc2FibGVkXT1cInRoaXMuZm9ybUZpZWxkSW5wdXRGaWxlLmdldERpc2FibGVkKClcIj5cbiAgICAgICAgICAgICAgICB7eyB0aGlzLmZvcm1GaWVsZElucHV0RmlsZS5nZXRCdXR0b25BZGRUZXh0KCkgfX1cbiAgICAgICAgPC9idXR0b24+XG5cbjwvc2VjdGlvbj5cblxuPCEtLSAtLS0tLS0tLS0tLS0tLS0tLS0tIGZvcm0gZmllbGQgaW5wdXQgZmlsZSB3aXRob3V0IGRyYWcgYW5kIGRyb3AgLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0gLS0+XG48c2VjdGlvblxuICAgICpuZ0lmPVwiIXRoaXMuZm9ybUZpZWxkSW5wdXRGaWxlLmdldERyYWdBbmREcm9wKClcIlxuICAgIGZ4TGF5b3V0PVwiY29sdW1uXCJcbiAgICBmeExheW91dEFsaWduPVwiY2VudGVyIGNlbnRlclwiXG4gICAgZnhMYXlvdXRHYXA9XCIxMHB4XCJcbiAgICBjbGFzcz1cImFkZC1wYWRkaW5nXCI+XG5cbiAgICAgICAgPGgzPnt7IHRoaXMuZm9ybUZpZWxkSW5wdXRGaWxlLmdldExhYmVsTmFtZSgpIH19PC9oMz5cblxuICAgICAgICA8bWF0LXNlbGVjdGlvbi1saXN0IFxuICAgICAgICAgICAgI2ZpbGVzXG4gICAgICAgICAgICAqbmdJZj1cInRoaXMuZm9ybUZpZWxkSW5wdXRGaWxlLmdldERlZmF1bHRWYWx1ZSgpPy5sZW5ndGggPiAwXCJcbiAgICAgICAgICAgIGZ4TGF5b3V0PVwiY29sdW1uXCJcbiAgICAgICAgICAgIGZ4TGF5b3V0QWxpZ249XCJjZW50ZXIgY2VudGVyXCJcbiAgICAgICAgICAgIGZ4TGF5b3V0R2FwPVwiOHB4XCI+XG5cbiAgICAgICAgICAgICAgICA8bWF0LWxpc3Qtb3B0aW9uIFxuICAgICAgICAgICAgICAgICAgICAqbmdGb3I9XCJsZXQgaXRlbSBvZiB0aGlzLmZvcm1GaWVsZElucHV0RmlsZS5nZXREZWZhdWx0VmFsdWUoKVwiIFxuICAgICAgICAgICAgICAgICAgICBbdmFsdWVdPVwiaXRlbVwiPlxuICAgICAgICAgICAgICAgICAgICAgICAge3sgaXRlbS5uYW1lIH19XG4gICAgICAgICAgICAgICAgPC9tYXQtbGlzdC1vcHRpb24+XG5cbiAgICAgICAgICAgICAgICA8YnV0dG9uXG4gICAgICAgICAgICAgICAgICAgICpuZ0lmPVwiZmlsZXMuc2VsZWN0ZWRPcHRpb25zLnNlbGVjdGVkLmxlbmd0aCA+IDBcIlxuICAgICAgICAgICAgICAgICAgICB0eXBlPVwiYnV0dG9uXCJcbiAgICAgICAgICAgICAgICAgICAgW2NvbG9yXT1cInRoaXMuZm9ybUZpZWxkSW5wdXRGaWxlLmdldEJ1dHRvbkRlbGV0ZUNvbG9yKClcIlxuICAgICAgICAgICAgICAgICAgICAoY2xpY2spPVwidGhpcy5vblJlbW92ZUZpbGVzKGZpbGVzLnNlbGVjdGVkT3B0aW9ucy5zZWxlY3RlZClcIlxuICAgICAgICAgICAgICAgICAgICBtYXQtYnV0dG9uPlxuICAgICAgICAgICAgICAgICAgICAgICAge3sgdGhpcy5mb3JtRmllbGRJbnB1dEZpbGUuZ2V0QnV0dG9uRGVsZXRlVGV4dCgpIH19XG4gICAgICAgICAgICAgICAgPC9idXR0b24+XG4gICAgICAgIDwvbWF0LXNlbGVjdGlvbi1saXN0PiAgICBcblxuICAgICAgICA8aW5wdXQgXG4gICAgICAgICAgICB0eXBlPVwiZmlsZVwiIFxuICAgICAgICAgICAgI2ZpbGUgXG4gICAgICAgICAgICAoY2hhbmdlKT1cIm9uRmlsZXNBZGRlZCgpXCIgICAgICAgIFxuICAgICAgICAgICAgbXVsdGlwbGUgLz5cblxuICAgICAgICA8YnV0dG9uIFxuICAgICAgICAgICAgdHlwZT1cImJ1dHRvblwiXG4gICAgICAgICAgICAoY2xpY2spPVwiYWRkRmlsZXMoKVwiXG4gICAgICAgICAgICBbY29sb3JdPVwidGhpcy5mb3JtRmllbGRJbnB1dEZpbGUuZ2V0QnV0dG9uQWRkQ29sb3IoKVwiXG4gICAgICAgICAgICBtYXQtcmFpc2VkLWJ1dHRvblxuICAgICAgICAgICAgW2Rpc2FibGVkXT1cInRoaXMuZm9ybUZpZWxkSW5wdXRGaWxlLmdldERpc2FibGVkKClcIj5cbiAgICAgICAgICAgICAgICB7eyB0aGlzLmZvcm1GaWVsZElucHV0RmlsZS5nZXRCdXR0b25BZGRUZXh0KCkgfX1cbiAgICAgICAgPC9idXR0b24+XG5cbjwvc2VjdGlvbj4iXX0=