import { Component, Input, forwardRef } from "@angular/core";
import { NG_VALUE_ACCESSOR } from "@angular/forms";
import * as i0 from "@angular/core";
import * as i1 from "@angular/flex-layout/flex";
import * as i2 from "@angular/material/form-field";
import * as i3 from "@angular/common";
import * as i4 from "@angular/material/input";
import * as i5 from "@angular/material/icon";
function FormFieldInputNumberComponent_mat_label_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-label");
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r0 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r0.formFieldInputNumber.getLabelName());
} }
function FormFieldInputNumberComponent_mat_icon_5_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-icon", 9);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r2 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r2.formFieldInputNumber.getMatPrefixImgText());
} }
function FormFieldInputNumberComponent_mat_icon_6_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-icon", 10);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r3 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r3.formFieldInputNumber.getMatSuffixImgText());
} }
function FormFieldInputNumberComponent_span_7_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 9);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r4 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1("", ctx_r4.formFieldInputNumber.getTextPrefix(), "\u00A0");
} }
function FormFieldInputNumberComponent_span_8_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 10);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r5 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r5.formFieldInputNumber.getTextSuffix());
} }
function FormFieldInputNumberComponent_mat_hint_9_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-hint", 11);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r6 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r6.formFieldInputNumber.getLeftHintLabel());
} }
function FormFieldInputNumberComponent_mat_hint_10_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-hint", 12);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r7 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r7.formFieldInputNumber.getRightHintLabel());
} }
export class FormFieldInputNumberComponent {
    constructor() {
        this.onChange = () => { };
        this.onTouched = () => { };
        this.onKeyUp = (input) => {
            // console.log(input.value);
            this.formFieldInputNumber.setDefaultValue(input.value);
            this.onChange(this.formFieldInputNumber.getDefaultValue());
            this.onTouched();
        };
    }
    registerOnChange(fn) {
        this.onChange = fn;
    }
    registerOnTouched(fn) {
        this.onTouched = fn;
    }
    writeValue(value) {
        if (value) {
            this.formFieldInputNumber.setDefaultValue(value);
        }
    }
    ngOnInit() {
        // console.log('FormFieldInputNumberComponent init');
    }
    ngOnDestroy() {
        // console.log('FormFieldInputNumberComponent destroyed');
    }
}
FormFieldInputNumberComponent.ɵfac = function FormFieldInputNumberComponent_Factory(t) { return new (t || FormFieldInputNumberComponent)(); };
FormFieldInputNumberComponent.ɵcmp = i0.ɵɵdefineComponent({ type: FormFieldInputNumberComponent, selectors: [["app-form-field-input-number"]], inputs: { formFieldInputNumber: "formFieldInputNumber" }, features: [i0.ɵɵProvidersFeature([
            {
                provide: NG_VALUE_ACCESSOR,
                multi: true,
                useExisting: forwardRef(() => FormFieldInputNumberComponent),
            },
        ])], decls: 11, vars: 16, consts: [["fxLayoutAlign", "center center"], [3, "appearance", "hideRequiredMarker"], [4, "ngIf"], ["matInput", "", 3, "type", "placeholder", "value", "step", "disabled", "readonly", "required", "keyup", "blur"], ["input", ""], ["matPrefix", "", 4, "ngIf"], ["matSuffix", "", 4, "ngIf"], ["align", "start", 4, "ngIf"], ["align", "end", 4, "ngIf"], ["matPrefix", ""], ["matSuffix", ""], ["align", "start"], ["align", "end"]], template: function FormFieldInputNumberComponent_Template(rf, ctx) { if (rf & 1) {
        const _r8 = i0.ɵɵgetCurrentView();
        i0.ɵɵelementStart(0, "section", 0);
        i0.ɵɵelementStart(1, "mat-form-field", 1);
        i0.ɵɵtemplate(2, FormFieldInputNumberComponent_mat_label_2_Template, 2, 1, "mat-label", 2);
        i0.ɵɵelementStart(3, "input", 3, 4);
        i0.ɵɵlistener("keyup", function FormFieldInputNumberComponent_Template_input_keyup_3_listener() { i0.ɵɵrestoreView(_r8); const _r1 = i0.ɵɵreference(4); return ctx.onKeyUp(_r1); })("blur", function FormFieldInputNumberComponent_Template_input_blur_3_listener() { i0.ɵɵrestoreView(_r8); const _r1 = i0.ɵɵreference(4); return ctx.onKeyUp(_r1); });
        i0.ɵɵelementEnd();
        i0.ɵɵtemplate(5, FormFieldInputNumberComponent_mat_icon_5_Template, 2, 1, "mat-icon", 5);
        i0.ɵɵtemplate(6, FormFieldInputNumberComponent_mat_icon_6_Template, 2, 1, "mat-icon", 6);
        i0.ɵɵtemplate(7, FormFieldInputNumberComponent_span_7_Template, 2, 1, "span", 5);
        i0.ɵɵtemplate(8, FormFieldInputNumberComponent_span_8_Template, 2, 1, "span", 6);
        i0.ɵɵtemplate(9, FormFieldInputNumberComponent_mat_hint_9_Template, 2, 1, "mat-hint", 7);
        i0.ɵɵtemplate(10, FormFieldInputNumberComponent_mat_hint_10_Template, 2, 1, "mat-hint", 8);
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("appearance", ctx.formFieldInputNumber.getAppearance())("hideRequiredMarker", ctx.formFieldInputNumber.getRequired().hideRequiredMarker);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.formFieldInputNumber.getLabelName());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("type", ctx.formFieldInputNumber.getFormFieldInputType())("placeholder", ctx.formFieldInputNumber.getPlaceholder() ? ctx.formFieldInputNumber.getPlaceholder() : "")("value", ctx.formFieldInputNumber.getDefaultValue())("step", ctx.formFieldInputNumber.getStep())("disabled", ctx.formFieldInputNumber.getDisabled())("readonly", ctx.formFieldInputNumber.getReadonly())("required", ctx.formFieldInputNumber.getRequired().required);
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("ngIf", ctx.formFieldInputNumber.getMatPrefixImgText());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.formFieldInputNumber.getMatSuffixImgText());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.formFieldInputNumber.getTextPrefix());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.formFieldInputNumber.getTextSuffix());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.formFieldInputNumber.getLeftHintLabel());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.formFieldInputNumber.getRightHintLabel());
    } }, directives: [i1.DefaultLayoutAlignDirective, i2.MatFormField, i3.NgIf, i4.MatInput, i2.MatLabel, i5.MatIcon, i2.MatPrefix, i2.MatSuffix, i2.MatHint], styles: ["", ".cursor[_ngcontent-%COMP%] {\n        cursor: pointer;\n    }\n\n    input[type=\"file\"][_ngcontent-%COMP%] {\n        display: none;\n    }\n\n    h3[_ngcontent-%COMP%] {\n        font-size: inherit;\n        margin: 0;\n        font-weight: inherit;\n    }\n\n    mat-form-field[_ngcontent-%COMP%] {\n        min-width: 250px;\n    }"] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FormFieldInputNumberComponent, [{
        type: Component,
        args: [{
                selector: "app-form-field-input-number",
                templateUrl: "./form-field-input-number.component.html",
                styleUrls: ["./form-field-input-number.component.css"],
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        multi: true,
                        useExisting: forwardRef(() => FormFieldInputNumberComponent),
                    },
                ],
            }]
    }], function () { return []; }, { formFieldInputNumber: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1maWVsZC1pbnB1dC1udW1iZXIuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXktYW5ndWxhci1mb3JtLyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC91aS9mb3JtLWZpZWxkLWlucHV0L2Zvcm0tZmllbGQtaW5wdXQtbnVtYmVyL2Zvcm0tZmllbGQtaW5wdXQtbnVtYmVyLmNvbXBvbmVudC50cyIsImxpYi9zaGFyZWQvdWkvZm9ybS1maWVsZC1pbnB1dC9mb3JtLWZpZWxkLWlucHV0LW51bWJlci9mb3JtLWZpZWxkLWlucHV0LW51bWJlci5jb21wb25lbnQuaHRtbCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRSxVQUFVLEVBQWEsTUFBTSxlQUFlLENBQUM7QUFFaEYsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sZ0JBQWdCLENBQUM7Ozs7Ozs7O0lDMkJuQyxpQ0FDcUQ7SUFBQSxZQUE4QztJQUFBLGlCQUFZOzs7SUFBMUQsZUFBOEM7SUFBOUMsZ0VBQThDOzs7SUFlbkcsbUNBRWM7SUFBQSxZQUFxRDtJQUFBLGlCQUFXOzs7SUFBaEUsZUFBcUQ7SUFBckQsdUVBQXFEOzs7SUFFbkUsb0NBRWM7SUFBQSxZQUFxRDtJQUFBLGlCQUFXOzs7SUFBaEUsZUFBcUQ7SUFBckQsdUVBQXFEOzs7SUFFbkUsK0JBRWM7SUFBQSxZQUFxRDtJQUFBLGlCQUFPOzs7SUFBNUQsZUFBcUQ7SUFBckQsZ0ZBQXFEOzs7SUFFbkUsZ0NBRWM7SUFBQSxZQUErQztJQUFBLGlCQUFPOzs7SUFBdEQsZUFBK0M7SUFBL0MsaUVBQStDOzs7SUFFN0Qsb0NBRWtCO0lBQUEsWUFBa0Q7SUFBQSxpQkFBVzs7O0lBQTdELGVBQWtEO0lBQWxELG9FQUFrRDs7O0lBRXBFLG9DQUVnQjtJQUFBLFlBQW1EO0lBQUEsaUJBQVc7OztJQUE5RCxlQUFtRDtJQUFuRCxxRUFBbUQ7O0FEbkRuRixNQUFNLE9BQU8sNkJBQTZCO0lBTXhDO1FBSE8sYUFBUSxHQUFRLEdBQUcsRUFBRSxHQUFFLENBQUMsQ0FBQztRQUN6QixjQUFTLEdBQVEsR0FBRyxFQUFFLEdBQUUsQ0FBQyxDQUFDO1FBSWpDLFlBQU8sR0FBRyxDQUFDLEtBQXVCLEVBQUUsRUFBRTtZQUNwQyw0QkFBNEI7WUFDNUIsSUFBSSxDQUFDLG9CQUFvQixDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDdkQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsZUFBZSxFQUFFLENBQUMsQ0FBQztZQUMzRCxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDbkIsQ0FBQyxDQUFDO0lBUGEsQ0FBQztJQVNoQixnQkFBZ0IsQ0FBQyxFQUFFO1FBQ2pCLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO0lBQ3JCLENBQUM7SUFFRCxpQkFBaUIsQ0FBQyxFQUFFO1FBQ2xCLElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO0lBQ3RCLENBQUM7SUFFRCxVQUFVLENBQUMsS0FBSztRQUNkLElBQUksS0FBSyxFQUFFO1lBQ1QsSUFBSSxDQUFDLG9CQUFvQixDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUNsRDtJQUNILENBQUM7SUFFRCxRQUFRO1FBQ04scURBQXFEO0lBQ3ZELENBQUM7SUFFRCxXQUFXO1FBQ1QsMERBQTBEO0lBQzVELENBQUM7OzBHQW5DVSw2QkFBNkI7a0VBQTdCLDZCQUE2QiwySUFSN0I7WUFDVDtnQkFDRSxPQUFPLEVBQUUsaUJBQWlCO2dCQUMxQixLQUFLLEVBQUUsSUFBSTtnQkFDWCxXQUFXLEVBQUUsVUFBVSxDQUFDLEdBQUcsRUFBRSxDQUFDLDZCQUE2QixDQUFDO2FBQzdEO1NBQ0Y7O1FDUUgsa0NBR1E7UUFBQSx5Q0FJUTtRQUFBLDBGQUNxRDtRQUVyRCxtQ0FhQTtRQVJJLCtKQUFTLGdCQUFtQixJQUFDLGdKQUNyQixnQkFBbUIsSUFERTtRQUxqQyxpQkFhQTtRQUFBLHdGQUVjO1FBRWQsd0ZBRWM7UUFFZCxnRkFFYztRQUVkLGdGQUVjO1FBRWQsd0ZBRWtCO1FBRWxCLDBGQUVnQjtRQUV4QixpQkFBaUI7UUFFekIsaUJBQVU7O1FBN0NFLGVBQXdEO1FBQXhELHFFQUF3RCxpRkFBQTtRQUloRCxlQUFnRDtRQUFoRCw4REFBZ0Q7UUFJaEQsZUFBMEQ7UUFBMUQsdUVBQTBELDJHQUFBLHFEQUFBLDRDQUFBLG9EQUFBLG9EQUFBLDZEQUFBO1FBWTFELGVBQXVEO1FBQXZELHFFQUF1RDtRQUl2RCxlQUF1RDtRQUF2RCxxRUFBdUQ7UUFJdkQsZUFBaUQ7UUFBakQsK0RBQWlEO1FBSWpELGVBQWlEO1FBQWpELCtEQUFpRDtRQUlqRCxlQUFvRDtRQUFwRCxrRUFBb0Q7UUFJcEQsZUFBcUQ7UUFBckQsbUVBQXFEOztrRERsRDVELDZCQUE2QjtjQVp6QyxTQUFTO2VBQUM7Z0JBQ1QsUUFBUSxFQUFFLDZCQUE2QjtnQkFDdkMsV0FBVyxFQUFFLDBDQUEwQztnQkFDdkQsU0FBUyxFQUFFLENBQUMseUNBQXlDLENBQUM7Z0JBQ3RELFNBQVMsRUFBRTtvQkFDVDt3QkFDRSxPQUFPLEVBQUUsaUJBQWlCO3dCQUMxQixLQUFLLEVBQUUsSUFBSTt3QkFDWCxXQUFXLEVBQUUsVUFBVSxDQUFDLEdBQUcsRUFBRSxDQUFDLDZCQUE2QixDQUFDO3FCQUM3RDtpQkFDRjthQUNGOztrQkFFRSxLQUFLIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0LCBmb3J3YXJkUmVmLCBPbkRlc3Ryb3kgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgRm9ybUZpZWxkSW5wdXROdW1iZXIgfSBmcm9tIFwiLi4vLi4vLi4vbW9kZWwvc3RydWN0dXJhbC9jb21wb3NpdGUvZm9ybS9mb3JtLWZpZWxkLWlucHV0L2Zvcm0tZmllbGQtaW5wdXQtbnVtYmVyLm1vZGVsXCI7XG5pbXBvcnQgeyBOR19WQUxVRV9BQ0NFU1NPUiB9IGZyb20gXCJAYW5ndWxhci9mb3Jtc1wiO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6IFwiYXBwLWZvcm0tZmllbGQtaW5wdXQtbnVtYmVyXCIsXG4gIHRlbXBsYXRlVXJsOiBcIi4vZm9ybS1maWVsZC1pbnB1dC1udW1iZXIuY29tcG9uZW50Lmh0bWxcIixcbiAgc3R5bGVVcmxzOiBbXCIuL2Zvcm0tZmllbGQtaW5wdXQtbnVtYmVyLmNvbXBvbmVudC5jc3NcIl0sXG4gIHByb3ZpZGVyczogW1xuICAgIHtcbiAgICAgIHByb3ZpZGU6IE5HX1ZBTFVFX0FDQ0VTU09SLFxuICAgICAgbXVsdGk6IHRydWUsXG4gICAgICB1c2VFeGlzdGluZzogZm9yd2FyZFJlZigoKSA9PiBGb3JtRmllbGRJbnB1dE51bWJlckNvbXBvbmVudCksXG4gICAgfSxcbiAgXSxcbn0pXG5leHBvcnQgY2xhc3MgRm9ybUZpZWxkSW5wdXROdW1iZXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XG4gIEBJbnB1dCgpIGZvcm1GaWVsZElucHV0TnVtYmVyOiBGb3JtRmllbGRJbnB1dE51bWJlcjtcblxuICBwdWJsaWMgb25DaGFuZ2U6IGFueSA9ICgpID0+IHt9O1xuICBwdWJsaWMgb25Ub3VjaGVkOiBhbnkgPSAoKSA9PiB7fTtcblxuICBjb25zdHJ1Y3RvcigpIHt9XG5cbiAgb25LZXlVcCA9IChpbnB1dDogSFRNTElucHV0RWxlbWVudCkgPT4ge1xuICAgIC8vIGNvbnNvbGUubG9nKGlucHV0LnZhbHVlKTtcbiAgICB0aGlzLmZvcm1GaWVsZElucHV0TnVtYmVyLnNldERlZmF1bHRWYWx1ZShpbnB1dC52YWx1ZSk7XG4gICAgdGhpcy5vbkNoYW5nZSh0aGlzLmZvcm1GaWVsZElucHV0TnVtYmVyLmdldERlZmF1bHRWYWx1ZSgpKTtcbiAgICB0aGlzLm9uVG91Y2hlZCgpO1xuICB9O1xuXG4gIHJlZ2lzdGVyT25DaGFuZ2UoZm4pIHtcbiAgICB0aGlzLm9uQ2hhbmdlID0gZm47XG4gIH1cblxuICByZWdpc3Rlck9uVG91Y2hlZChmbikge1xuICAgIHRoaXMub25Ub3VjaGVkID0gZm47XG4gIH1cblxuICB3cml0ZVZhbHVlKHZhbHVlKSB7XG4gICAgaWYgKHZhbHVlKSB7XG4gICAgICB0aGlzLmZvcm1GaWVsZElucHV0TnVtYmVyLnNldERlZmF1bHRWYWx1ZSh2YWx1ZSk7XG4gICAgfVxuICB9XG5cbiAgbmdPbkluaXQoKTogdm9pZCB7XG4gICAgLy8gY29uc29sZS5sb2coJ0Zvcm1GaWVsZElucHV0TnVtYmVyQ29tcG9uZW50IGluaXQnKTtcbiAgfVxuXG4gIG5nT25EZXN0cm95KCk6IHZvaWQge1xuICAgIC8vIGNvbnNvbGUubG9nKCdGb3JtRmllbGRJbnB1dE51bWJlckNvbXBvbmVudCBkZXN0cm95ZWQnKTtcbiAgfVxufVxuIiwiPHN0eWxlPlxuXG4gICAgLmN1cnNvciB7XG4gICAgICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICB9XG5cbiAgICBpbnB1dFt0eXBlPVwiZmlsZVwiXSB7XG4gICAgICAgIGRpc3BsYXk6IG5vbmU7XG4gICAgfVxuXG4gICAgaDMge1xuICAgICAgICBmb250LXNpemU6IGluaGVyaXQ7XG4gICAgICAgIG1hcmdpbjogMDtcbiAgICAgICAgZm9udC13ZWlnaHQ6IGluaGVyaXQ7XG4gICAgfVxuXG4gICAgbWF0LWZvcm0tZmllbGQge1xuICAgICAgICBtaW4td2lkdGg6IDI1MHB4O1xuICAgIH1cblxuPC9zdHlsZT5cblxuPHNlY3Rpb25cbiAgICBmeExheW91dEFsaWduPVwiY2VudGVyIGNlbnRlclwiPlxuXG4gICAgICAgIDxtYXQtZm9ybS1maWVsZCBcbiAgICAgICAgICAgIFthcHBlYXJhbmNlXT1cInRoaXMuZm9ybUZpZWxkSW5wdXROdW1iZXIuZ2V0QXBwZWFyYW5jZSgpXCJcbiAgICAgICAgICAgIFtoaWRlUmVxdWlyZWRNYXJrZXJdPVwidGhpcy5mb3JtRmllbGRJbnB1dE51bWJlci5nZXRSZXF1aXJlZCgpLmhpZGVSZXF1aXJlZE1hcmtlclwiPlxuXG4gICAgICAgICAgICAgICAgPG1hdC1sYWJlbFxuICAgICAgICAgICAgICAgICAgICAqbmdJZj1cInRoaXMuZm9ybUZpZWxkSW5wdXROdW1iZXIuZ2V0TGFiZWxOYW1lKClcIj57eyB0aGlzLmZvcm1GaWVsZElucHV0TnVtYmVyLmdldExhYmVsTmFtZSgpIH19PC9tYXQtbGFiZWw+XG5cbiAgICAgICAgICAgICAgICA8aW5wdXQgXG4gICAgICAgICAgICAgICAgICAgIG1hdElucHV0XG4gICAgICAgICAgICAgICAgICAgIFt0eXBlXT1cInRoaXMuZm9ybUZpZWxkSW5wdXROdW1iZXIuZ2V0Rm9ybUZpZWxkSW5wdXRUeXBlKClcIlxuICAgICAgICAgICAgICAgICAgICBbcGxhY2Vob2xkZXJdPVwidGhpcy5mb3JtRmllbGRJbnB1dE51bWJlci5nZXRQbGFjZWhvbGRlcigpID8gdGhpcy5mb3JtRmllbGRJbnB1dE51bWJlci5nZXRQbGFjZWhvbGRlcigpIDogJydcIlxuICAgICAgICAgICAgICAgICAgICBbdmFsdWVdPVwidGhpcy5mb3JtRmllbGRJbnB1dE51bWJlci5nZXREZWZhdWx0VmFsdWUoKVwiXG4gICAgICAgICAgICAgICAgICAgIChrZXl1cCk9XCJ0aGlzLm9uS2V5VXAoaW5wdXQpXCJcbiAgICAgICAgICAgICAgICAgICAgKGJsdXIpPVwidGhpcy5vbktleVVwKGlucHV0KVwiXG4gICAgICAgICAgICAgICAgICAgIFtzdGVwXT1cInRoaXMuZm9ybUZpZWxkSW5wdXROdW1iZXIuZ2V0U3RlcCgpXCJcbiAgICAgICAgICAgICAgICAgICAgW2Rpc2FibGVkXT1cInRoaXMuZm9ybUZpZWxkSW5wdXROdW1iZXIuZ2V0RGlzYWJsZWQoKVwiXG4gICAgICAgICAgICAgICAgICAgIFtyZWFkb25seV09XCJ0aGlzLmZvcm1GaWVsZElucHV0TnVtYmVyLmdldFJlYWRvbmx5KClcIlxuICAgICAgICAgICAgICAgICAgICBbcmVxdWlyZWRdPVwidGhpcy5mb3JtRmllbGRJbnB1dE51bWJlci5nZXRSZXF1aXJlZCgpLnJlcXVpcmVkXCJcbiAgICAgICAgICAgICAgICAgICAgI2lucHV0PlxuXG4gICAgICAgICAgICAgICAgPG1hdC1pY29uIFxuICAgICAgICAgICAgICAgICAgICAqbmdJZj1cInRoaXMuZm9ybUZpZWxkSW5wdXROdW1iZXIuZ2V0TWF0UHJlZml4SW1nVGV4dCgpXCJcbiAgICAgICAgICAgICAgICAgICAgbWF0UHJlZml4Pnt7IHRoaXMuZm9ybUZpZWxkSW5wdXROdW1iZXIuZ2V0TWF0UHJlZml4SW1nVGV4dCgpIH19PC9tYXQtaWNvbj5cblxuICAgICAgICAgICAgICAgIDxtYXQtaWNvbiBcbiAgICAgICAgICAgICAgICAgICAgKm5nSWY9XCJ0aGlzLmZvcm1GaWVsZElucHV0TnVtYmVyLmdldE1hdFN1ZmZpeEltZ1RleHQoKVwiXG4gICAgICAgICAgICAgICAgICAgIG1hdFN1ZmZpeD57eyB0aGlzLmZvcm1GaWVsZElucHV0TnVtYmVyLmdldE1hdFN1ZmZpeEltZ1RleHQoKSB9fTwvbWF0LWljb24+XG5cbiAgICAgICAgICAgICAgICA8c3BhbiBcbiAgICAgICAgICAgICAgICAgICAgKm5nSWY9XCJ0aGlzLmZvcm1GaWVsZElucHV0TnVtYmVyLmdldFRleHRQcmVmaXgoKVwiXG4gICAgICAgICAgICAgICAgICAgIG1hdFByZWZpeD57eyB0aGlzLmZvcm1GaWVsZElucHV0TnVtYmVyLmdldFRleHRQcmVmaXgoKSB9fSZuYnNwOzwvc3Bhbj5cblxuICAgICAgICAgICAgICAgIDxzcGFuIFxuICAgICAgICAgICAgICAgICAgICAqbmdJZj1cInRoaXMuZm9ybUZpZWxkSW5wdXROdW1iZXIuZ2V0VGV4dFN1ZmZpeCgpXCJcbiAgICAgICAgICAgICAgICAgICAgbWF0U3VmZml4Pnt7IHRoaXMuZm9ybUZpZWxkSW5wdXROdW1iZXIuZ2V0VGV4dFN1ZmZpeCgpIH19PC9zcGFuPlxuXG4gICAgICAgICAgICAgICAgPG1hdC1oaW50IFxuICAgICAgICAgICAgICAgICAgICAqbmdJZj1cInRoaXMuZm9ybUZpZWxkSW5wdXROdW1iZXIuZ2V0TGVmdEhpbnRMYWJlbCgpXCJcbiAgICAgICAgICAgICAgICAgICAgYWxpZ249XCJzdGFydFwiPnt7IHRoaXMuZm9ybUZpZWxkSW5wdXROdW1iZXIuZ2V0TGVmdEhpbnRMYWJlbCgpIH19PC9tYXQtaGludD5cblxuICAgICAgICAgICAgICAgIDxtYXQtaGludFxuICAgICAgICAgICAgICAgICAgICAqbmdJZj1cInRoaXMuZm9ybUZpZWxkSW5wdXROdW1iZXIuZ2V0UmlnaHRIaW50TGFiZWwoKVwiXG4gICAgICAgICAgICAgICAgICAgIGFsaWduPVwiZW5kXCI+e3sgdGhpcy5mb3JtRmllbGRJbnB1dE51bWJlci5nZXRSaWdodEhpbnRMYWJlbCgpIH19PC9tYXQtaGludD5cblxuICAgICAgICA8L21hdC1mb3JtLWZpZWxkPlxuXG48L3NlY3Rpb24+Il19