import { Component, Input, forwardRef } from "@angular/core";
import { NG_VALUE_ACCESSOR } from "@angular/forms";
import * as i0 from "@angular/core";
import * as i1 from "@angular/flex-layout/flex";
import * as i2 from "@angular/material/form-field";
import * as i3 from "@angular/common";
import * as i4 from "@angular/material/input";
import * as i5 from "@angular/material/icon";
function FormFieldInputEmailComponent_mat_label_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-label");
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r0 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r0.formFieldInputEmail.getLabelName());
} }
function FormFieldInputEmailComponent_mat_icon_5_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-icon", 9);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r2 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r2.formFieldInputEmail.getMatPrefixImgText());
} }
function FormFieldInputEmailComponent_mat_icon_6_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-icon", 10);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r3 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r3.formFieldInputEmail.getMatSuffixImgText());
} }
function FormFieldInputEmailComponent_span_7_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 9);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r4 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1("", ctx_r4.formFieldInputEmail.getTextPrefix(), "\u00A0");
} }
function FormFieldInputEmailComponent_span_8_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 10);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r5 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r5.formFieldInputEmail.getTextSuffix());
} }
function FormFieldInputEmailComponent_mat_hint_9_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-hint", 11);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r6 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r6.formFieldInputEmail.getLeftHintLabel());
} }
function FormFieldInputEmailComponent_mat_hint_10_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-hint", 12);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r7 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r7.formFieldInputEmail.getRightHintLabel());
} }
export class FormFieldInputEmailComponent {
    constructor() {
        this.onChange = () => { };
        this.onTouched = () => { };
        this.onKeyUp = (input) => {
            this.formFieldInputEmail.setDefaultValue(input.value);
            this.onChange(this.formFieldInputEmail.getDefaultValue());
            this.onTouched();
        };
    }
    registerOnChange(fn) {
        this.onChange = fn;
    }
    registerOnTouched(fn) {
        this.onTouched = fn;
    }
    writeValue(value) {
        if (value) {
            this.formFieldInputEmail.setDefaultValue(value);
        }
    }
    ngOnInit() {
        // console.log('FormFieldInputEmailComponent init');
    }
    ngOnDestroy() {
        // console.log('FormFieldInputEmailComponent destroyed');
    }
}
FormFieldInputEmailComponent.ɵfac = function FormFieldInputEmailComponent_Factory(t) { return new (t || FormFieldInputEmailComponent)(); };
FormFieldInputEmailComponent.ɵcmp = i0.ɵɵdefineComponent({ type: FormFieldInputEmailComponent, selectors: [["app-form-field-input-email"]], inputs: { formFieldInputEmail: "formFieldInputEmail" }, features: [i0.ɵɵProvidersFeature([
            {
                provide: NG_VALUE_ACCESSOR,
                multi: true,
                useExisting: forwardRef(() => FormFieldInputEmailComponent),
            },
        ])], decls: 11, vars: 15, consts: [["fxLayoutAlign", "center center"], [3, "appearance", "hideRequiredMarker"], [4, "ngIf"], ["matInput", "", 3, "type", "placeholder", "value", "disabled", "readonly", "required", "keyup", "blur"], ["input", ""], ["matPrefix", "", 4, "ngIf"], ["matSuffix", "", 4, "ngIf"], ["align", "start", 4, "ngIf"], ["align", "end", 4, "ngIf"], ["matPrefix", ""], ["matSuffix", ""], ["align", "start"], ["align", "end"]], template: function FormFieldInputEmailComponent_Template(rf, ctx) { if (rf & 1) {
        const _r8 = i0.ɵɵgetCurrentView();
        i0.ɵɵelementStart(0, "section", 0);
        i0.ɵɵelementStart(1, "mat-form-field", 1);
        i0.ɵɵtemplate(2, FormFieldInputEmailComponent_mat_label_2_Template, 2, 1, "mat-label", 2);
        i0.ɵɵelementStart(3, "input", 3, 4);
        i0.ɵɵlistener("keyup", function FormFieldInputEmailComponent_Template_input_keyup_3_listener() { i0.ɵɵrestoreView(_r8); const _r1 = i0.ɵɵreference(4); return ctx.onKeyUp(_r1); })("blur", function FormFieldInputEmailComponent_Template_input_blur_3_listener() { i0.ɵɵrestoreView(_r8); const _r1 = i0.ɵɵreference(4); return ctx.onKeyUp(_r1); });
        i0.ɵɵelementEnd();
        i0.ɵɵtemplate(5, FormFieldInputEmailComponent_mat_icon_5_Template, 2, 1, "mat-icon", 5);
        i0.ɵɵtemplate(6, FormFieldInputEmailComponent_mat_icon_6_Template, 2, 1, "mat-icon", 6);
        i0.ɵɵtemplate(7, FormFieldInputEmailComponent_span_7_Template, 2, 1, "span", 5);
        i0.ɵɵtemplate(8, FormFieldInputEmailComponent_span_8_Template, 2, 1, "span", 6);
        i0.ɵɵtemplate(9, FormFieldInputEmailComponent_mat_hint_9_Template, 2, 1, "mat-hint", 7);
        i0.ɵɵtemplate(10, FormFieldInputEmailComponent_mat_hint_10_Template, 2, 1, "mat-hint", 8);
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("appearance", ctx.formFieldInputEmail.getAppearance())("hideRequiredMarker", ctx.formFieldInputEmail.getRequired().hideRequiredMarker);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.formFieldInputEmail.getLabelName());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("type", ctx.formFieldInputEmail.getFormFieldInputType())("placeholder", ctx.formFieldInputEmail.getPlaceholder() ? ctx.formFieldInputEmail.getPlaceholder() : "")("value", ctx.formFieldInputEmail.getDefaultValue())("disabled", ctx.formFieldInputEmail.getDisabled())("readonly", ctx.formFieldInputEmail.getReadonly())("required", ctx.formFieldInputEmail.getRequired().required);
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("ngIf", ctx.formFieldInputEmail.getMatPrefixImgText());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.formFieldInputEmail.getMatSuffixImgText());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.formFieldInputEmail.getTextPrefix());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.formFieldInputEmail.getTextSuffix());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.formFieldInputEmail.getLeftHintLabel());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.formFieldInputEmail.getRightHintLabel());
    } }, directives: [i1.DefaultLayoutAlignDirective, i2.MatFormField, i3.NgIf, i4.MatInput, i2.MatLabel, i5.MatIcon, i2.MatPrefix, i2.MatSuffix, i2.MatHint], styles: ["", ".cursor[_ngcontent-%COMP%] {\n        cursor: pointer;\n    }\n\n    input[type=\"file\"][_ngcontent-%COMP%] {\n        display: none;\n    }\n\n    h3[_ngcontent-%COMP%] {\n        font-size: inherit;\n        margin: 0;\n        font-weight: inherit;\n    }\n\n    .upload-file-container[_ngcontent-%COMP%] {\n        min-height: 200px;\n        width: 80%;\n        margin: 20px auto;        \n        border-radius: 10px;\n    }\n\n    mat-form-field[_ngcontent-%COMP%] {\n        min-width: 250px;\n    }"] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FormFieldInputEmailComponent, [{
        type: Component,
        args: [{
                selector: "app-form-field-input-email",
                templateUrl: "./form-field-input-email.component.html",
                styleUrls: ["./form-field-input-email.component.css"],
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        multi: true,
                        useExisting: forwardRef(() => FormFieldInputEmailComponent),
                    },
                ],
            }]
    }], function () { return []; }, { formFieldInputEmail: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1maWVsZC1pbnB1dC1lbWFpbC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9teS1hbmd1bGFyLWZvcm0vIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL3VpL2Zvcm0tZmllbGQtaW5wdXQvZm9ybS1maWVsZC1pbnB1dC1lbWFpbC9mb3JtLWZpZWxkLWlucHV0LWVtYWlsLmNvbXBvbmVudC50cyIsImxpYi9zaGFyZWQvdWkvZm9ybS1maWVsZC1pbnB1dC9mb3JtLWZpZWxkLWlucHV0LWVtYWlsL2Zvcm0tZmllbGQtaW5wdXQtZW1haWwuY29tcG9uZW50Lmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxLQUFLLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRXJFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGdCQUFnQixDQUFDOzs7Ozs7OztJQ2tDbkMsaUNBQ29EO0lBQUEsWUFBNkM7SUFBQSxpQkFBWTs7O0lBQXpELGVBQTZDO0lBQTdDLCtEQUE2Qzs7O0lBY2pHLG1DQUVjO0lBQUEsWUFBb0Q7SUFBQSxpQkFBVzs7O0lBQS9ELGVBQW9EO0lBQXBELHNFQUFvRDs7O0lBRWxFLG9DQUVjO0lBQUEsWUFBb0Q7SUFBQSxpQkFBVzs7O0lBQS9ELGVBQW9EO0lBQXBELHNFQUFvRDs7O0lBRWxFLCtCQUVjO0lBQUEsWUFBb0Q7SUFBQSxpQkFBTzs7O0lBQTNELGVBQW9EO0lBQXBELCtFQUFvRDs7O0lBRWxFLGdDQUVjO0lBQUEsWUFBOEM7SUFBQSxpQkFBTzs7O0lBQXJELGVBQThDO0lBQTlDLGdFQUE4Qzs7O0lBRTVELG9DQUVrQjtJQUFBLFlBQWlEO0lBQUEsaUJBQVc7OztJQUE1RCxlQUFpRDtJQUFqRCxtRUFBaUQ7OztJQUVuRSxvQ0FFZ0I7SUFBQSxZQUFrRDtJQUFBLGlCQUFXOzs7SUFBN0QsZUFBa0Q7SUFBbEQsb0VBQWtEOztBRHpEbEYsTUFBTSxPQUFPLDRCQUE0QjtJQU12QztRQUhPLGFBQVEsR0FBUSxHQUFHLEVBQUUsR0FBRSxDQUFDLENBQUM7UUFDekIsY0FBUyxHQUFRLEdBQUcsRUFBRSxHQUFFLENBQUMsQ0FBQztRQUlqQyxZQUFPLEdBQUcsQ0FBQyxLQUF1QixFQUFFLEVBQUU7WUFDcEMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDdEQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsZUFBZSxFQUFFLENBQUMsQ0FBQztZQUMxRCxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDbkIsQ0FBQyxDQUFDO0lBTmEsQ0FBQztJQVFoQixnQkFBZ0IsQ0FBQyxFQUFFO1FBQ2pCLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO0lBQ3JCLENBQUM7SUFFRCxpQkFBaUIsQ0FBQyxFQUFFO1FBQ2xCLElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO0lBQ3RCLENBQUM7SUFFRCxVQUFVLENBQUMsS0FBSztRQUNkLElBQUksS0FBSyxFQUFFO1lBQ1QsSUFBSSxDQUFDLG1CQUFtQixDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUNqRDtJQUNILENBQUM7SUFFRCxRQUFRO1FBQ04sb0RBQW9EO0lBQ3RELENBQUM7SUFFRCxXQUFXO1FBQ1QseURBQXlEO0lBQzNELENBQUM7O3dHQWxDVSw0QkFBNEI7aUVBQTVCLDRCQUE0Qix3SUFSNUI7WUFDVDtnQkFDRSxPQUFPLEVBQUUsaUJBQWlCO2dCQUMxQixLQUFLLEVBQUUsSUFBSTtnQkFDWCxXQUFXLEVBQUUsVUFBVSxDQUFDLEdBQUcsRUFBRSxDQUFDLDRCQUE0QixDQUFDO2FBQzVEO1NBQ0Y7O1FDZUgsa0NBR1E7UUFBQSx5Q0FJUTtRQUFBLHlGQUNvRDtRQUVwRCxtQ0FZQTtRQVBJLDhKQUFTLGdCQUFtQixJQUFDLCtJQUNyQixnQkFBbUIsSUFERTtRQUxqQyxpQkFZQTtRQUFBLHVGQUVjO1FBRWQsdUZBRWM7UUFFZCwrRUFFYztRQUVkLCtFQUVjO1FBRWQsdUZBRWtCO1FBRWxCLHlGQUVnQjtRQUV4QixpQkFBaUI7UUFFekIsaUJBQVU7O1FBNUNFLGVBQXVEO1FBQXZELG9FQUF1RCxnRkFBQTtRQUkvQyxlQUErQztRQUEvQyw2REFBK0M7UUFJL0MsZUFBeUQ7UUFBekQsc0VBQXlELHlHQUFBLG9EQUFBLG1EQUFBLG1EQUFBLDREQUFBO1FBV3pELGVBQXNEO1FBQXRELG9FQUFzRDtRQUl0RCxlQUFzRDtRQUF0RCxvRUFBc0Q7UUFJdEQsZUFBZ0Q7UUFBaEQsOERBQWdEO1FBSWhELGVBQWdEO1FBQWhELDhEQUFnRDtRQUloRCxlQUFtRDtRQUFuRCxpRUFBbUQ7UUFJbkQsZUFBb0Q7UUFBcEQsa0VBQW9EOztrRER4RDNELDRCQUE0QjtjQVp4QyxTQUFTO2VBQUM7Z0JBQ1QsUUFBUSxFQUFFLDRCQUE0QjtnQkFDdEMsV0FBVyxFQUFFLHlDQUF5QztnQkFDdEQsU0FBUyxFQUFFLENBQUMsd0NBQXdDLENBQUM7Z0JBQ3JELFNBQVMsRUFBRTtvQkFDVDt3QkFDRSxPQUFPLEVBQUUsaUJBQWlCO3dCQUMxQixLQUFLLEVBQUUsSUFBSTt3QkFDWCxXQUFXLEVBQUUsVUFBVSxDQUFDLEdBQUcsRUFBRSxDQUFDLDRCQUE0QixDQUFDO3FCQUM1RDtpQkFDRjthQUNGOztrQkFFRSxLQUFLIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0LCBmb3J3YXJkUmVmIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IEZvcm1GaWVsZElucHV0IH0gZnJvbSBcIi4uLy4uLy4uL21vZGVsL3N0cnVjdHVyYWwvY29tcG9zaXRlL2Zvcm0vZm9ybS1maWVsZC1pbnB1dC9mb3JtLWZpZWxkLWlucHV0Lm1vZGVsXCI7XG5pbXBvcnQgeyBOR19WQUxVRV9BQ0NFU1NPUiB9IGZyb20gXCJAYW5ndWxhci9mb3Jtc1wiO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6IFwiYXBwLWZvcm0tZmllbGQtaW5wdXQtZW1haWxcIixcbiAgdGVtcGxhdGVVcmw6IFwiLi9mb3JtLWZpZWxkLWlucHV0LWVtYWlsLmNvbXBvbmVudC5odG1sXCIsXG4gIHN0eWxlVXJsczogW1wiLi9mb3JtLWZpZWxkLWlucHV0LWVtYWlsLmNvbXBvbmVudC5jc3NcIl0sXG4gIHByb3ZpZGVyczogW1xuICAgIHtcbiAgICAgIHByb3ZpZGU6IE5HX1ZBTFVFX0FDQ0VTU09SLFxuICAgICAgbXVsdGk6IHRydWUsXG4gICAgICB1c2VFeGlzdGluZzogZm9yd2FyZFJlZigoKSA9PiBGb3JtRmllbGRJbnB1dEVtYWlsQ29tcG9uZW50KSxcbiAgICB9LFxuICBdLFxufSlcbmV4cG9ydCBjbGFzcyBGb3JtRmllbGRJbnB1dEVtYWlsQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgQElucHV0KCkgZm9ybUZpZWxkSW5wdXRFbWFpbDogRm9ybUZpZWxkSW5wdXQ7XG5cbiAgcHVibGljIG9uQ2hhbmdlOiBhbnkgPSAoKSA9PiB7fTtcbiAgcHVibGljIG9uVG91Y2hlZDogYW55ID0gKCkgPT4ge307XG5cbiAgY29uc3RydWN0b3IoKSB7fVxuXG4gIG9uS2V5VXAgPSAoaW5wdXQ6IEhUTUxJbnB1dEVsZW1lbnQpID0+IHtcbiAgICB0aGlzLmZvcm1GaWVsZElucHV0RW1haWwuc2V0RGVmYXVsdFZhbHVlKGlucHV0LnZhbHVlKTtcbiAgICB0aGlzLm9uQ2hhbmdlKHRoaXMuZm9ybUZpZWxkSW5wdXRFbWFpbC5nZXREZWZhdWx0VmFsdWUoKSk7XG4gICAgdGhpcy5vblRvdWNoZWQoKTtcbiAgfTtcblxuICByZWdpc3Rlck9uQ2hhbmdlKGZuKSB7XG4gICAgdGhpcy5vbkNoYW5nZSA9IGZuO1xuICB9XG5cbiAgcmVnaXN0ZXJPblRvdWNoZWQoZm4pIHtcbiAgICB0aGlzLm9uVG91Y2hlZCA9IGZuO1xuICB9XG5cbiAgd3JpdGVWYWx1ZSh2YWx1ZSkge1xuICAgIGlmICh2YWx1ZSkge1xuICAgICAgdGhpcy5mb3JtRmllbGRJbnB1dEVtYWlsLnNldERlZmF1bHRWYWx1ZSh2YWx1ZSk7XG4gICAgfVxuICB9XG5cbiAgbmdPbkluaXQoKTogdm9pZCB7XG4gICAgLy8gY29uc29sZS5sb2coJ0Zvcm1GaWVsZElucHV0RW1haWxDb21wb25lbnQgaW5pdCcpO1xuICB9XG5cbiAgbmdPbkRlc3Ryb3koKTogdm9pZCB7XG4gICAgLy8gY29uc29sZS5sb2coJ0Zvcm1GaWVsZElucHV0RW1haWxDb21wb25lbnQgZGVzdHJveWVkJyk7XG4gIH1cbn1cbiIsIjxzdHlsZT5cblxuICAgIC5jdXJzb3Ige1xuICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgfVxuXG4gICAgaW5wdXRbdHlwZT1cImZpbGVcIl0ge1xuICAgICAgICBkaXNwbGF5OiBub25lO1xuICAgIH1cblxuICAgIGgzIHtcbiAgICAgICAgZm9udC1zaXplOiBpbmhlcml0O1xuICAgICAgICBtYXJnaW46IDA7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiBpbmhlcml0O1xuICAgIH1cblxuICAgIC51cGxvYWQtZmlsZS1jb250YWluZXIge1xuICAgICAgICBtaW4taGVpZ2h0OiAyMDBweDtcbiAgICAgICAgd2lkdGg6IDgwJTtcbiAgICAgICAgbWFyZ2luOiAyMHB4IGF1dG87ICAgICAgICBcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICB9XG5cbiAgICBtYXQtZm9ybS1maWVsZCB7XG4gICAgICAgIG1pbi13aWR0aDogMjUwcHg7XG4gICAgfVxuXG48L3N0eWxlPlxuXG48c2VjdGlvblxuICAgIGZ4TGF5b3V0QWxpZ249XCJjZW50ZXIgY2VudGVyXCI+XG5cbiAgICAgICAgPG1hdC1mb3JtLWZpZWxkIFxuICAgICAgICAgICAgW2FwcGVhcmFuY2VdPVwidGhpcy5mb3JtRmllbGRJbnB1dEVtYWlsLmdldEFwcGVhcmFuY2UoKVwiXG4gICAgICAgICAgICBbaGlkZVJlcXVpcmVkTWFya2VyXT1cInRoaXMuZm9ybUZpZWxkSW5wdXRFbWFpbC5nZXRSZXF1aXJlZCgpLmhpZGVSZXF1aXJlZE1hcmtlclwiPlxuXG4gICAgICAgICAgICAgICAgPG1hdC1sYWJlbFxuICAgICAgICAgICAgICAgICAgICAqbmdJZj1cInRoaXMuZm9ybUZpZWxkSW5wdXRFbWFpbC5nZXRMYWJlbE5hbWUoKVwiPnt7IHRoaXMuZm9ybUZpZWxkSW5wdXRFbWFpbC5nZXRMYWJlbE5hbWUoKSB9fTwvbWF0LWxhYmVsPlxuXG4gICAgICAgICAgICAgICAgPGlucHV0IFxuICAgICAgICAgICAgICAgICAgICBtYXRJbnB1dFxuICAgICAgICAgICAgICAgICAgICBbdHlwZV09XCJ0aGlzLmZvcm1GaWVsZElucHV0RW1haWwuZ2V0Rm9ybUZpZWxkSW5wdXRUeXBlKClcIlxuICAgICAgICAgICAgICAgICAgICBbcGxhY2Vob2xkZXJdPVwidGhpcy5mb3JtRmllbGRJbnB1dEVtYWlsLmdldFBsYWNlaG9sZGVyKCkgPyB0aGlzLmZvcm1GaWVsZElucHV0RW1haWwuZ2V0UGxhY2Vob2xkZXIoKSA6ICcnXCJcbiAgICAgICAgICAgICAgICAgICAgW3ZhbHVlXT1cInRoaXMuZm9ybUZpZWxkSW5wdXRFbWFpbC5nZXREZWZhdWx0VmFsdWUoKVwiXG4gICAgICAgICAgICAgICAgICAgIChrZXl1cCk9XCJ0aGlzLm9uS2V5VXAoaW5wdXQpXCJcbiAgICAgICAgICAgICAgICAgICAgKGJsdXIpPVwidGhpcy5vbktleVVwKGlucHV0KVwiXG4gICAgICAgICAgICAgICAgICAgIFtkaXNhYmxlZF09XCJ0aGlzLmZvcm1GaWVsZElucHV0RW1haWwuZ2V0RGlzYWJsZWQoKVwiXG4gICAgICAgICAgICAgICAgICAgIFtyZWFkb25seV09XCJ0aGlzLmZvcm1GaWVsZElucHV0RW1haWwuZ2V0UmVhZG9ubHkoKVwiXG4gICAgICAgICAgICAgICAgICAgIFtyZXF1aXJlZF09XCJ0aGlzLmZvcm1GaWVsZElucHV0RW1haWwuZ2V0UmVxdWlyZWQoKS5yZXF1aXJlZFwiXG4gICAgICAgICAgICAgICAgICAgICNpbnB1dD5cblxuICAgICAgICAgICAgICAgIDxtYXQtaWNvbiBcbiAgICAgICAgICAgICAgICAgICAgKm5nSWY9XCJ0aGlzLmZvcm1GaWVsZElucHV0RW1haWwuZ2V0TWF0UHJlZml4SW1nVGV4dCgpXCJcbiAgICAgICAgICAgICAgICAgICAgbWF0UHJlZml4Pnt7IHRoaXMuZm9ybUZpZWxkSW5wdXRFbWFpbC5nZXRNYXRQcmVmaXhJbWdUZXh0KCkgfX08L21hdC1pY29uPlxuXG4gICAgICAgICAgICAgICAgPG1hdC1pY29uIFxuICAgICAgICAgICAgICAgICAgICAqbmdJZj1cInRoaXMuZm9ybUZpZWxkSW5wdXRFbWFpbC5nZXRNYXRTdWZmaXhJbWdUZXh0KClcIlxuICAgICAgICAgICAgICAgICAgICBtYXRTdWZmaXg+e3sgdGhpcy5mb3JtRmllbGRJbnB1dEVtYWlsLmdldE1hdFN1ZmZpeEltZ1RleHQoKSB9fTwvbWF0LWljb24+XG5cbiAgICAgICAgICAgICAgICA8c3BhbiBcbiAgICAgICAgICAgICAgICAgICAgKm5nSWY9XCJ0aGlzLmZvcm1GaWVsZElucHV0RW1haWwuZ2V0VGV4dFByZWZpeCgpXCJcbiAgICAgICAgICAgICAgICAgICAgbWF0UHJlZml4Pnt7IHRoaXMuZm9ybUZpZWxkSW5wdXRFbWFpbC5nZXRUZXh0UHJlZml4KCkgfX0mbmJzcDs8L3NwYW4+XG5cbiAgICAgICAgICAgICAgICA8c3BhbiBcbiAgICAgICAgICAgICAgICAgICAgKm5nSWY9XCJ0aGlzLmZvcm1GaWVsZElucHV0RW1haWwuZ2V0VGV4dFN1ZmZpeCgpXCJcbiAgICAgICAgICAgICAgICAgICAgbWF0U3VmZml4Pnt7IHRoaXMuZm9ybUZpZWxkSW5wdXRFbWFpbC5nZXRUZXh0U3VmZml4KCkgfX08L3NwYW4+XG5cbiAgICAgICAgICAgICAgICA8bWF0LWhpbnQgXG4gICAgICAgICAgICAgICAgICAgICpuZ0lmPVwidGhpcy5mb3JtRmllbGRJbnB1dEVtYWlsLmdldExlZnRIaW50TGFiZWwoKVwiXG4gICAgICAgICAgICAgICAgICAgIGFsaWduPVwic3RhcnRcIj57eyB0aGlzLmZvcm1GaWVsZElucHV0RW1haWwuZ2V0TGVmdEhpbnRMYWJlbCgpIH19PC9tYXQtaGludD5cblxuICAgICAgICAgICAgICAgIDxtYXQtaGludFxuICAgICAgICAgICAgICAgICAgICAqbmdJZj1cInRoaXMuZm9ybUZpZWxkSW5wdXRFbWFpbC5nZXRSaWdodEhpbnRMYWJlbCgpXCJcbiAgICAgICAgICAgICAgICAgICAgYWxpZ249XCJlbmRcIj57eyB0aGlzLmZvcm1GaWVsZElucHV0RW1haWwuZ2V0UmlnaHRIaW50TGFiZWwoKSB9fTwvbWF0LWhpbnQ+XG5cbiAgICAgICAgPC9tYXQtZm9ybS1maWVsZD5cblxuPC9zZWN0aW9uPiJdfQ==