import { Component, Input, forwardRef } from "@angular/core";
import { VALIDATOR_NAMES, } from "../../../model/structural/composite/form/form-row-item.model";
import { NG_VALUE_ACCESSOR } from "@angular/forms";
import * as i0 from "@angular/core";
import * as i1 from "@angular/flex-layout/flex";
import * as i2 from "@angular/material/form-field";
import * as i3 from "@angular/common";
import * as i4 from "@angular/material/input";
import * as i5 from "@angular/material/icon";
function FormFieldInputPasswordComponent_mat_label_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-label");
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r0 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r0.formFieldInputPassword.getLabelName());
} }
function FormFieldInputPasswordComponent_mat_icon_5_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-icon", 10);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r2 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r2.formFieldInputPassword.getMatPrefixImgText());
} }
function FormFieldInputPasswordComponent_mat_icon_6_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-icon", 11);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r3 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r3.formFieldInputPassword.getMatSuffixImgText());
} }
function FormFieldInputPasswordComponent_mat_icon_7_Template(rf, ctx) { if (rf & 1) {
    const _r13 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "mat-icon", 12);
    i0.ɵɵlistener("click", function FormFieldInputPasswordComponent_mat_icon_7_Template_mat_icon_click_0_listener() { i0.ɵɵrestoreView(_r13); const ctx_r12 = i0.ɵɵnextContext(); return ctx_r12.formFieldInputPassword.onShowPassword(); });
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r4 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r4.formFieldInputPassword.getShowPassword() ? "visibility" : "visibility_off", " ");
} }
function FormFieldInputPasswordComponent_span_8_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 10);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r5 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1("", ctx_r5.formFieldInputPassword.getTextPrefix(), "\u00A0");
} }
function FormFieldInputPasswordComponent_span_9_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 11);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r6 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r6.formFieldInputPassword.getTextSuffix());
} }
function FormFieldInputPasswordComponent_mat_hint_10_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-hint", 13);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r7 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r7.formFieldInputPassword.getLeftHintLabel());
} }
function FormFieldInputPasswordComponent_mat_hint_11_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-hint", 14);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r8 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r8.formFieldInputPassword.getRightHintLabel());
} }
function FormFieldInputPasswordComponent_mat_hint_12_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-hint", 14);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r9 = i0.ɵɵnextContext();
    var tmp_0_0 = null;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate3("", ((tmp_0_0 = ctx_r9.getMinLengthValidator()) == null ? null : tmp_0_0.length) ? ctx_r9.getMinLengthValidator().length + " / " : "", "", (tmp_0_0 = ctx_r9.formFieldInputPassword.getDefaultValue()) == null ? null : tmp_0_0.length, "", ((tmp_0_0 = ctx_r9.getMaxLengthValidator()) == null ? null : tmp_0_0.length) ? " / " + ctx_r9.getMaxLengthValidator().length : "", "");
} }
function FormFieldInputPasswordComponent_mat_hint_13_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-hint", 14);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r10 = i0.ɵɵnextContext();
    var tmp_0_0 = null;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate2("", (tmp_0_0 = ctx_r10.formFieldInputPassword.getDefaultValue()) == null ? null : tmp_0_0.length, "", ((tmp_0_0 = ctx_r10.getMinLengthValidator()) == null ? null : tmp_0_0.length) ? " / " + ctx_r10.getMinLengthValidator().length : "", "");
} }
function FormFieldInputPasswordComponent_mat_hint_14_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-hint", 14);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r11 = i0.ɵɵnextContext();
    var tmp_0_0 = null;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate2("", (tmp_0_0 = ctx_r11.formFieldInputPassword.getDefaultValue()) == null ? null : tmp_0_0.length, "", ((tmp_0_0 = ctx_r11.getMaxLengthValidator()) == null ? null : tmp_0_0.length) ? " / " + ctx_r11.getMaxLengthValidator().length : "", "");
} }
export class FormFieldInputPasswordComponent {
    constructor() {
        this.onChange = () => { };
        this.onTouched = () => { };
        this.onKeyUp = (input) => {
            this.formFieldInputPassword.setDefaultValue(input.value);
            this.onChange(this.formFieldInputPassword.getDefaultValue());
            this.onTouched();
        };
        this.getMinLengthValidator = () => {
            let theValidator;
            this.formFieldInputPassword
                .getValidators()
                .forEach((validator) => {
                if (validator.name === VALIDATOR_NAMES.MIN_LENGTH)
                    theValidator = validator;
            });
            return theValidator;
        };
        this.getMaxLengthValidator = () => {
            let theValidator;
            this.formFieldInputPassword
                .getValidators()
                .forEach((validator) => {
                if (validator.name === VALIDATOR_NAMES.MAX_LENGTH)
                    theValidator = validator;
            });
            return theValidator;
        };
    }
    registerOnChange(fn) {
        this.onChange = fn;
    }
    registerOnTouched(fn) {
        this.onTouched = fn;
    }
    writeValue(value) {
        if (value) {
            this.formFieldInputPassword.setDefaultValue(value);
        }
    }
    ngOnInit() {
        // console.log('FormFieldInputPasswordComponent init');
    }
    ngOnDestroy() {
        // console.log('FormFieldInputPasswordComponent destroyed');
    }
}
FormFieldInputPasswordComponent.ɵfac = function FormFieldInputPasswordComponent_Factory(t) { return new (t || FormFieldInputPasswordComponent)(); };
FormFieldInputPasswordComponent.ɵcmp = i0.ɵɵdefineComponent({ type: FormFieldInputPasswordComponent, selectors: [["app-form-field-input-password"]], inputs: { formFieldInputPassword: "formFieldInputPassword" }, features: [i0.ɵɵProvidersFeature([
            {
                provide: NG_VALUE_ACCESSOR,
                multi: true,
                useExisting: forwardRef(() => FormFieldInputPasswordComponent),
            },
        ])], decls: 15, vars: 19, consts: [["fxLayoutAlign", "center center"], [3, "appearance", "hideRequiredMarker"], [4, "ngIf"], ["matInput", "", 3, "type", "placeholder", "value", "disabled", "readonly", "required", "keyup", "blur"], ["input", ""], ["matPrefix", "", 4, "ngIf"], ["matSuffix", "", 4, "ngIf"], ["class", "cursor", "matSuffix", "", 3, "click", 4, "ngIf"], ["align", "start", 4, "ngIf"], ["align", "end", 4, "ngIf"], ["matPrefix", ""], ["matSuffix", ""], ["matSuffix", "", 1, "cursor", 3, "click"], ["align", "start"], ["align", "end"]], template: function FormFieldInputPasswordComponent_Template(rf, ctx) { if (rf & 1) {
        const _r14 = i0.ɵɵgetCurrentView();
        i0.ɵɵelementStart(0, "section", 0);
        i0.ɵɵelementStart(1, "mat-form-field", 1);
        i0.ɵɵtemplate(2, FormFieldInputPasswordComponent_mat_label_2_Template, 2, 1, "mat-label", 2);
        i0.ɵɵelementStart(3, "input", 3, 4);
        i0.ɵɵlistener("keyup", function FormFieldInputPasswordComponent_Template_input_keyup_3_listener() { i0.ɵɵrestoreView(_r14); const _r1 = i0.ɵɵreference(4); return ctx.onKeyUp(_r1); })("blur", function FormFieldInputPasswordComponent_Template_input_blur_3_listener() { i0.ɵɵrestoreView(_r14); const _r1 = i0.ɵɵreference(4); return ctx.onKeyUp(_r1); });
        i0.ɵɵelementEnd();
        i0.ɵɵtemplate(5, FormFieldInputPasswordComponent_mat_icon_5_Template, 2, 1, "mat-icon", 5);
        i0.ɵɵtemplate(6, FormFieldInputPasswordComponent_mat_icon_6_Template, 2, 1, "mat-icon", 6);
        i0.ɵɵtemplate(7, FormFieldInputPasswordComponent_mat_icon_7_Template, 2, 1, "mat-icon", 7);
        i0.ɵɵtemplate(8, FormFieldInputPasswordComponent_span_8_Template, 2, 1, "span", 5);
        i0.ɵɵtemplate(9, FormFieldInputPasswordComponent_span_9_Template, 2, 1, "span", 6);
        i0.ɵɵtemplate(10, FormFieldInputPasswordComponent_mat_hint_10_Template, 2, 1, "mat-hint", 8);
        i0.ɵɵtemplate(11, FormFieldInputPasswordComponent_mat_hint_11_Template, 2, 1, "mat-hint", 9);
        i0.ɵɵtemplate(12, FormFieldInputPasswordComponent_mat_hint_12_Template, 2, 3, "mat-hint", 9);
        i0.ɵɵtemplate(13, FormFieldInputPasswordComponent_mat_hint_13_Template, 2, 2, "mat-hint", 9);
        i0.ɵɵtemplate(14, FormFieldInputPasswordComponent_mat_hint_14_Template, 2, 2, "mat-hint", 9);
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("appearance", ctx.formFieldInputPassword.getAppearance())("hideRequiredMarker", ctx.formFieldInputPassword.getRequired().hideRequiredMarker);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.formFieldInputPassword.getLabelName());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("type", ctx.formFieldInputPassword.getShowPasswordInMs() && ctx.formFieldInputPassword.getShowPassword() ? "text" : ctx.formFieldInputPassword.getFormFieldInputType())("placeholder", ctx.formFieldInputPassword.getPlaceholder() ? ctx.formFieldInputPassword.getPlaceholder() : "")("value", ctx.formFieldInputPassword.getDefaultValue())("disabled", ctx.formFieldInputPassword.getDisabled())("readonly", ctx.formFieldInputPassword.getReadonly())("required", ctx.formFieldInputPassword.getRequired().required);
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("ngIf", ctx.formFieldInputPassword.getMatPrefixImgText());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.formFieldInputPassword.getMatSuffixImgText());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.formFieldInputPassword.getShowPasswordInMs());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.formFieldInputPassword.getTextPrefix());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.formFieldInputPassword.getTextSuffix());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.formFieldInputPassword.getLeftHintLabel());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.formFieldInputPassword.getRightHintLabel());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.formFieldInputPassword.getShowHintAboutMinMaxLength() && ctx.getMinLengthValidator() && ctx.getMaxLengthValidator());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.formFieldInputPassword.getShowHintAboutMinMaxLength() && ctx.getMinLengthValidator() && !ctx.getMaxLengthValidator());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.formFieldInputPassword.getShowHintAboutMinMaxLength() && !ctx.getMinLengthValidator() && ctx.getMaxLengthValidator());
    } }, directives: [i1.DefaultLayoutAlignDirective, i2.MatFormField, i3.NgIf, i4.MatInput, i2.MatLabel, i5.MatIcon, i2.MatPrefix, i2.MatSuffix, i2.MatHint], styles: ["", ".cursor[_ngcontent-%COMP%] {\n        cursor: pointer;\n    }\n\n    input[type=\"file\"][_ngcontent-%COMP%] {\n        display: none;\n    }\n\n    h3[_ngcontent-%COMP%] {\n        font-size: inherit;\n        margin: 0;\n        font-weight: inherit;\n    }\n\n    mat-form-field[_ngcontent-%COMP%] {\n        min-width: 250px;\n    }"] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FormFieldInputPasswordComponent, [{
        type: Component,
        args: [{
                selector: "app-form-field-input-password",
                templateUrl: "./form-field-input-password.component.html",
                styleUrls: ["./form-field-input-password.component.css"],
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        multi: true,
                        useExisting: forwardRef(() => FormFieldInputPasswordComponent),
                    },
                ],
            }]
    }], function () { return []; }, { formFieldInputPassword: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1maWVsZC1pbnB1dC1wYXNzd29yZC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9teS1hbmd1bGFyLWZvcm0vIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL3VpL2Zvcm0tZmllbGQtaW5wdXQvZm9ybS1maWVsZC1pbnB1dC1wYXNzd29yZC9mb3JtLWZpZWxkLWlucHV0LXBhc3N3b3JkLmNvbXBvbmVudC50cyIsImxpYi9zaGFyZWQvdWkvZm9ybS1maWVsZC1pbnB1dC9mb3JtLWZpZWxkLWlucHV0LXBhc3N3b3JkL2Zvcm0tZmllbGQtaW5wdXQtcGFzc3dvcmQuY29tcG9uZW50Lmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBcUIsS0FBSyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUVoRixPQUFPLEVBRUwsZUFBZSxHQUNoQixNQUFNLDhEQUE4RCxDQUFDO0FBQ3RFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGdCQUFnQixDQUFDOzs7Ozs7OztJQ3VCbkMsaUNBQ3VEO0lBQUEsWUFBZ0Q7SUFBQSxpQkFBWTs7O0lBQTVELGVBQWdEO0lBQWhELGtFQUFnRDs7O0lBY3ZHLG9DQUVjO0lBQUEsWUFBdUQ7SUFBQSxpQkFBVzs7O0lBQWxFLGVBQXVEO0lBQXZELHlFQUF1RDs7O0lBRXJFLG9DQUVjO0lBQUEsWUFBdUQ7SUFBQSxpQkFBVzs7O0lBQWxFLGVBQXVEO0lBQXZELHlFQUF1RDs7OztJQUVyRSxvQ0FJZTtJQURYLHFMQUFTLCtDQUE0QyxJQUFDO0lBQzNDLFlBQXFGO0lBQUEsaUJBQVc7OztJQUFoRyxlQUFxRjtJQUFyRixrSEFBcUY7OztJQUVwRyxnQ0FFYztJQUFBLFlBQXVEO0lBQUEsaUJBQU87OztJQUE5RCxlQUF1RDtJQUF2RCxrRkFBdUQ7OztJQUVyRSxnQ0FFYztJQUFBLFlBQWlEO0lBQUEsaUJBQU87OztJQUF4RCxlQUFpRDtJQUFqRCxtRUFBaUQ7OztJQUUvRCxvQ0FFa0I7SUFBQSxZQUFvRDtJQUFBLGlCQUFXOzs7SUFBL0QsZUFBb0Q7SUFBcEQsc0VBQW9EOzs7SUFFdEUsb0NBRWdCO0lBQUEsWUFBcUQ7SUFBQSxpQkFBVzs7O0lBQWhFLGVBQXFEO0lBQXJELHVFQUFxRDs7O0lBR3JFLG9DQUVnQjtJQUFBLFlBQWtQO0lBQUEsaUJBQVc7Ozs7SUFBN1AsZUFBa1A7SUFBbFAsd1lBQWtQOzs7SUFFbFEsb0NBRWdCO0lBQUEsWUFBdUo7SUFBQSxpQkFBVzs7OztJQUFsSyxlQUF1SjtJQUF2SixvUUFBdUo7OztJQUV2SyxvQ0FFZ0I7SUFBQSxZQUFzSjtJQUFBLGlCQUFXOzs7O0lBQWpLLGVBQXNKO0lBQXRKLG9RQUFzSjs7QURqRXRMLE1BQU0sT0FBTywrQkFBK0I7SUFNMUM7UUFITyxhQUFRLEdBQVEsR0FBRyxFQUFFLEdBQUUsQ0FBQyxDQUFDO1FBQ3pCLGNBQVMsR0FBUSxHQUFHLEVBQUUsR0FBRSxDQUFDLENBQUM7UUFJakMsWUFBTyxHQUFHLENBQUMsS0FBdUIsRUFBRSxFQUFFO1lBQ3BDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3pELElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLGVBQWUsRUFBRSxDQUFDLENBQUM7WUFDN0QsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ25CLENBQUMsQ0FBQztRQWdCRiwwQkFBcUIsR0FBRyxHQUFrQyxFQUFFO1lBQzFELElBQUksWUFBWSxDQUFDO1lBQ2pCLElBQUksQ0FBQyxzQkFBc0I7aUJBQ3hCLGFBQWEsRUFBRTtpQkFDZixPQUFPLENBQUMsQ0FBQyxTQUF3QyxFQUFFLEVBQUU7Z0JBQ3BELElBQUksU0FBUyxDQUFDLElBQUksS0FBSyxlQUFlLENBQUMsVUFBVTtvQkFDL0MsWUFBWSxHQUFHLFNBQVMsQ0FBQztZQUM3QixDQUFDLENBQUMsQ0FBQztZQUNMLE9BQU8sWUFBWSxDQUFDO1FBQ3RCLENBQUMsQ0FBQztRQUVGLDBCQUFxQixHQUFHLEdBQWtDLEVBQUU7WUFDMUQsSUFBSSxZQUFZLENBQUM7WUFDakIsSUFBSSxDQUFDLHNCQUFzQjtpQkFDeEIsYUFBYSxFQUFFO2lCQUNmLE9BQU8sQ0FBQyxDQUFDLFNBQXdDLEVBQUUsRUFBRTtnQkFDcEQsSUFBSSxTQUFTLENBQUMsSUFBSSxLQUFLLGVBQWUsQ0FBQyxVQUFVO29CQUMvQyxZQUFZLEdBQUcsU0FBUyxDQUFDO1lBQzdCLENBQUMsQ0FBQyxDQUFDO1lBQ0wsT0FBTyxZQUFZLENBQUM7UUFDdEIsQ0FBQyxDQUFDO0lBMUNhLENBQUM7SUFRaEIsZ0JBQWdCLENBQUMsRUFBRTtRQUNqQixJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztJQUNyQixDQUFDO0lBRUQsaUJBQWlCLENBQUMsRUFBRTtRQUNsQixJQUFJLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztJQUN0QixDQUFDO0lBRUQsVUFBVSxDQUFDLEtBQUs7UUFDZCxJQUFJLEtBQUssRUFBRTtZQUNULElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDcEQ7SUFDSCxDQUFDO0lBd0JELFFBQVE7UUFDTix1REFBdUQ7SUFDekQsQ0FBQztJQUVELFdBQVc7UUFDVCw0REFBNEQ7SUFDOUQsQ0FBQzs7OEdBeERVLCtCQUErQjtvRUFBL0IsK0JBQStCLGlKQVIvQjtZQUNUO2dCQUNFLE9BQU8sRUFBRSxpQkFBaUI7Z0JBQzFCLEtBQUssRUFBRSxJQUFJO2dCQUNYLFdBQVcsRUFBRSxVQUFVLENBQUMsR0FBRyxFQUFFLENBQUMsK0JBQStCLENBQUM7YUFDL0Q7U0FDRjs7UUNJSCxrQ0FHUTtRQUFBLHlDQUlRO1FBQUEsNEZBQ3VEO1FBRXZELG1DQVlBO1FBUEksa0tBQVMsZ0JBQW1CLElBQUMsbUpBQ3JCLGdCQUFtQixJQURFO1FBTGpDLGlCQVlBO1FBQUEsMEZBRWM7UUFFZCwwRkFFYztRQUVkLDBGQUllO1FBRWYsa0ZBRWM7UUFFZCxrRkFFYztRQUVkLDRGQUVrQjtRQUVsQiw0RkFFZ0I7UUFHaEIsNEZBRWdCO1FBRWhCLDRGQUVnQjtRQUVoQiw0RkFFZ0I7UUFFeEIsaUJBQWlCO1FBRXpCLGlCQUFVOztRQS9ERSxlQUEwRDtRQUExRCx1RUFBMEQsbUZBQUE7UUFJbEQsZUFBa0Q7UUFBbEQsZ0VBQWtEO1FBSWxELGVBQTBLO1FBQTFLLHFMQUEwSywrR0FBQSx1REFBQSxzREFBQSxzREFBQSwrREFBQTtRQVcxSyxlQUF5RDtRQUF6RCx1RUFBeUQ7UUFJekQsZUFBeUQ7UUFBekQsdUVBQXlEO1FBS3pELGVBQXlEO1FBQXpELHVFQUF5RDtRQUt6RCxlQUFtRDtRQUFuRCxpRUFBbUQ7UUFJbkQsZUFBbUQ7UUFBbkQsaUVBQW1EO1FBSW5ELGVBQXNEO1FBQXRELG9FQUFzRDtRQUl0RCxlQUF1RDtRQUF2RCxxRUFBdUQ7UUFLdkQsZUFBa0k7UUFBbEksOElBQWtJO1FBSWxJLGVBQW1JO1FBQW5JLCtJQUFtSTtRQUluSSxlQUFtSTtRQUFuSSwrSUFBbUk7O2tERGhFMUksK0JBQStCO2NBWjNDLFNBQVM7ZUFBQztnQkFDVCxRQUFRLEVBQUUsK0JBQStCO2dCQUN6QyxXQUFXLEVBQUUsNENBQTRDO2dCQUN6RCxTQUFTLEVBQUUsQ0FBQywyQ0FBMkMsQ0FBQztnQkFDeEQsU0FBUyxFQUFFO29CQUNUO3dCQUNFLE9BQU8sRUFBRSxpQkFBaUI7d0JBQzFCLEtBQUssRUFBRSxJQUFJO3dCQUNYLFdBQVcsRUFBRSxVQUFVLENBQUMsR0FBRyxFQUFFLENBQUMsK0JBQStCLENBQUM7cUJBQy9EO2lCQUNGO2FBQ0Y7O2tCQUVFLEtBQUsiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgT25EZXN0cm95LCBJbnB1dCwgZm9yd2FyZFJlZiB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBGb3JtRmllbGRJbnB1dFBhc3N3b3JkIH0gZnJvbSBcIi4uLy4uLy4uL21vZGVsL3N0cnVjdHVyYWwvY29tcG9zaXRlL2Zvcm0vZm9ybS1maWVsZC1pbnB1dC9mb3JtLWZpZWxkLWlucHV0LXBhc3N3b3JkLm1vZGVsXCI7XG5pbXBvcnQge1xuICBGb3JtUm93SXRlbVZhbGlkYXRvckludGVyZmFjZSxcbiAgVkFMSURBVE9SX05BTUVTLFxufSBmcm9tIFwiLi4vLi4vLi4vbW9kZWwvc3RydWN0dXJhbC9jb21wb3NpdGUvZm9ybS9mb3JtLXJvdy1pdGVtLm1vZGVsXCI7XG5pbXBvcnQgeyBOR19WQUxVRV9BQ0NFU1NPUiB9IGZyb20gXCJAYW5ndWxhci9mb3Jtc1wiO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6IFwiYXBwLWZvcm0tZmllbGQtaW5wdXQtcGFzc3dvcmRcIixcbiAgdGVtcGxhdGVVcmw6IFwiLi9mb3JtLWZpZWxkLWlucHV0LXBhc3N3b3JkLmNvbXBvbmVudC5odG1sXCIsXG4gIHN0eWxlVXJsczogW1wiLi9mb3JtLWZpZWxkLWlucHV0LXBhc3N3b3JkLmNvbXBvbmVudC5jc3NcIl0sXG4gIHByb3ZpZGVyczogW1xuICAgIHtcbiAgICAgIHByb3ZpZGU6IE5HX1ZBTFVFX0FDQ0VTU09SLFxuICAgICAgbXVsdGk6IHRydWUsXG4gICAgICB1c2VFeGlzdGluZzogZm9yd2FyZFJlZigoKSA9PiBGb3JtRmllbGRJbnB1dFBhc3N3b3JkQ29tcG9uZW50KSxcbiAgICB9LFxuICBdLFxufSlcbmV4cG9ydCBjbGFzcyBGb3JtRmllbGRJbnB1dFBhc3N3b3JkQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xuICBASW5wdXQoKSBmb3JtRmllbGRJbnB1dFBhc3N3b3JkOiBGb3JtRmllbGRJbnB1dFBhc3N3b3JkO1xuXG4gIHB1YmxpYyBvbkNoYW5nZTogYW55ID0gKCkgPT4ge307XG4gIHB1YmxpYyBvblRvdWNoZWQ6IGFueSA9ICgpID0+IHt9O1xuXG4gIGNvbnN0cnVjdG9yKCkge31cblxuICBvbktleVVwID0gKGlucHV0OiBIVE1MSW5wdXRFbGVtZW50KSA9PiB7XG4gICAgdGhpcy5mb3JtRmllbGRJbnB1dFBhc3N3b3JkLnNldERlZmF1bHRWYWx1ZShpbnB1dC52YWx1ZSk7XG4gICAgdGhpcy5vbkNoYW5nZSh0aGlzLmZvcm1GaWVsZElucHV0UGFzc3dvcmQuZ2V0RGVmYXVsdFZhbHVlKCkpO1xuICAgIHRoaXMub25Ub3VjaGVkKCk7XG4gIH07XG5cbiAgcmVnaXN0ZXJPbkNoYW5nZShmbikge1xuICAgIHRoaXMub25DaGFuZ2UgPSBmbjtcbiAgfVxuXG4gIHJlZ2lzdGVyT25Ub3VjaGVkKGZuKSB7XG4gICAgdGhpcy5vblRvdWNoZWQgPSBmbjtcbiAgfVxuXG4gIHdyaXRlVmFsdWUodmFsdWUpIHtcbiAgICBpZiAodmFsdWUpIHtcbiAgICAgIHRoaXMuZm9ybUZpZWxkSW5wdXRQYXNzd29yZC5zZXREZWZhdWx0VmFsdWUodmFsdWUpO1xuICAgIH1cbiAgfVxuXG4gIGdldE1pbkxlbmd0aFZhbGlkYXRvciA9ICgpOiBGb3JtUm93SXRlbVZhbGlkYXRvckludGVyZmFjZSA9PiB7XG4gICAgbGV0IHRoZVZhbGlkYXRvcjtcbiAgICB0aGlzLmZvcm1GaWVsZElucHV0UGFzc3dvcmRcbiAgICAgIC5nZXRWYWxpZGF0b3JzKClcbiAgICAgIC5mb3JFYWNoKCh2YWxpZGF0b3I6IEZvcm1Sb3dJdGVtVmFsaWRhdG9ySW50ZXJmYWNlKSA9PiB7XG4gICAgICAgIGlmICh2YWxpZGF0b3IubmFtZSA9PT0gVkFMSURBVE9SX05BTUVTLk1JTl9MRU5HVEgpXG4gICAgICAgICAgdGhlVmFsaWRhdG9yID0gdmFsaWRhdG9yO1xuICAgICAgfSk7XG4gICAgcmV0dXJuIHRoZVZhbGlkYXRvcjtcbiAgfTtcblxuICBnZXRNYXhMZW5ndGhWYWxpZGF0b3IgPSAoKTogRm9ybVJvd0l0ZW1WYWxpZGF0b3JJbnRlcmZhY2UgPT4ge1xuICAgIGxldCB0aGVWYWxpZGF0b3I7XG4gICAgdGhpcy5mb3JtRmllbGRJbnB1dFBhc3N3b3JkXG4gICAgICAuZ2V0VmFsaWRhdG9ycygpXG4gICAgICAuZm9yRWFjaCgodmFsaWRhdG9yOiBGb3JtUm93SXRlbVZhbGlkYXRvckludGVyZmFjZSkgPT4ge1xuICAgICAgICBpZiAodmFsaWRhdG9yLm5hbWUgPT09IFZBTElEQVRPUl9OQU1FUy5NQVhfTEVOR1RIKVxuICAgICAgICAgIHRoZVZhbGlkYXRvciA9IHZhbGlkYXRvcjtcbiAgICAgIH0pO1xuICAgIHJldHVybiB0aGVWYWxpZGF0b3I7XG4gIH07XG5cbiAgbmdPbkluaXQoKTogdm9pZCB7XG4gICAgLy8gY29uc29sZS5sb2coJ0Zvcm1GaWVsZElucHV0UGFzc3dvcmRDb21wb25lbnQgaW5pdCcpO1xuICB9XG5cbiAgbmdPbkRlc3Ryb3koKTogdm9pZCB7XG4gICAgLy8gY29uc29sZS5sb2coJ0Zvcm1GaWVsZElucHV0UGFzc3dvcmRDb21wb25lbnQgZGVzdHJveWVkJyk7XG4gIH1cbn1cbiIsIjxzdHlsZT5cblxuICAgIC5jdXJzb3Ige1xuICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgfVxuXG4gICAgaW5wdXRbdHlwZT1cImZpbGVcIl0ge1xuICAgICAgICBkaXNwbGF5OiBub25lO1xuICAgIH1cblxuICAgIGgzIHtcbiAgICAgICAgZm9udC1zaXplOiBpbmhlcml0O1xuICAgICAgICBtYXJnaW46IDA7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiBpbmhlcml0O1xuICAgIH1cblxuICAgIG1hdC1mb3JtLWZpZWxkIHtcbiAgICAgICAgbWluLXdpZHRoOiAyNTBweDtcbiAgICB9XG5cbjwvc3R5bGU+XG5cbjxzZWN0aW9uXG4gICAgZnhMYXlvdXRBbGlnbj1cImNlbnRlciBjZW50ZXJcIj5cblxuICAgICAgICA8bWF0LWZvcm0tZmllbGQgXG4gICAgICAgICAgICBbYXBwZWFyYW5jZV09XCJ0aGlzLmZvcm1GaWVsZElucHV0UGFzc3dvcmQuZ2V0QXBwZWFyYW5jZSgpXCJcbiAgICAgICAgICAgIFtoaWRlUmVxdWlyZWRNYXJrZXJdPVwidGhpcy5mb3JtRmllbGRJbnB1dFBhc3N3b3JkLmdldFJlcXVpcmVkKCkuaGlkZVJlcXVpcmVkTWFya2VyXCI+XG5cbiAgICAgICAgICAgICAgICA8bWF0LWxhYmVsXG4gICAgICAgICAgICAgICAgICAgICpuZ0lmPVwidGhpcy5mb3JtRmllbGRJbnB1dFBhc3N3b3JkLmdldExhYmVsTmFtZSgpXCI+e3sgdGhpcy5mb3JtRmllbGRJbnB1dFBhc3N3b3JkLmdldExhYmVsTmFtZSgpIH19PC9tYXQtbGFiZWw+XG5cbiAgICAgICAgICAgICAgICA8aW5wdXQgXG4gICAgICAgICAgICAgICAgICAgIG1hdElucHV0XG4gICAgICAgICAgICAgICAgICAgIFt0eXBlXT1cInRoaXMuZm9ybUZpZWxkSW5wdXRQYXNzd29yZC5nZXRTaG93UGFzc3dvcmRJbk1zKCkgJiYgdGhpcy5mb3JtRmllbGRJbnB1dFBhc3N3b3JkLmdldFNob3dQYXNzd29yZCgpID8gJ3RleHQnIDogdGhpcy5mb3JtRmllbGRJbnB1dFBhc3N3b3JkLmdldEZvcm1GaWVsZElucHV0VHlwZSgpXCJcbiAgICAgICAgICAgICAgICAgICAgW3BsYWNlaG9sZGVyXT1cInRoaXMuZm9ybUZpZWxkSW5wdXRQYXNzd29yZC5nZXRQbGFjZWhvbGRlcigpID8gdGhpcy5mb3JtRmllbGRJbnB1dFBhc3N3b3JkLmdldFBsYWNlaG9sZGVyKCkgOiAnJ1wiXG4gICAgICAgICAgICAgICAgICAgIFt2YWx1ZV09XCJ0aGlzLmZvcm1GaWVsZElucHV0UGFzc3dvcmQuZ2V0RGVmYXVsdFZhbHVlKClcIlxuICAgICAgICAgICAgICAgICAgICAoa2V5dXApPVwidGhpcy5vbktleVVwKGlucHV0KVwiXG4gICAgICAgICAgICAgICAgICAgIChibHVyKT1cInRoaXMub25LZXlVcChpbnB1dClcIlxuICAgICAgICAgICAgICAgICAgICBbZGlzYWJsZWRdPVwidGhpcy5mb3JtRmllbGRJbnB1dFBhc3N3b3JkLmdldERpc2FibGVkKClcIlxuICAgICAgICAgICAgICAgICAgICBbcmVhZG9ubHldPVwidGhpcy5mb3JtRmllbGRJbnB1dFBhc3N3b3JkLmdldFJlYWRvbmx5KClcIlxuICAgICAgICAgICAgICAgICAgICBbcmVxdWlyZWRdPVwidGhpcy5mb3JtRmllbGRJbnB1dFBhc3N3b3JkLmdldFJlcXVpcmVkKCkucmVxdWlyZWRcIlxuICAgICAgICAgICAgICAgICAgICAjaW5wdXQ+XG5cbiAgICAgICAgICAgICAgICA8bWF0LWljb24gXG4gICAgICAgICAgICAgICAgICAgICpuZ0lmPVwidGhpcy5mb3JtRmllbGRJbnB1dFBhc3N3b3JkLmdldE1hdFByZWZpeEltZ1RleHQoKVwiXG4gICAgICAgICAgICAgICAgICAgIG1hdFByZWZpeD57eyB0aGlzLmZvcm1GaWVsZElucHV0UGFzc3dvcmQuZ2V0TWF0UHJlZml4SW1nVGV4dCgpIH19PC9tYXQtaWNvbj5cblxuICAgICAgICAgICAgICAgIDxtYXQtaWNvbiBcbiAgICAgICAgICAgICAgICAgICAgKm5nSWY9XCJ0aGlzLmZvcm1GaWVsZElucHV0UGFzc3dvcmQuZ2V0TWF0U3VmZml4SW1nVGV4dCgpXCJcbiAgICAgICAgICAgICAgICAgICAgbWF0U3VmZml4Pnt7IHRoaXMuZm9ybUZpZWxkSW5wdXRQYXNzd29yZC5nZXRNYXRTdWZmaXhJbWdUZXh0KCkgfX08L21hdC1pY29uPlxuXG4gICAgICAgICAgICAgICAgPG1hdC1pY29uXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzPVwiY3Vyc29yXCJcbiAgICAgICAgICAgICAgICAgICAgKm5nSWY9XCJ0aGlzLmZvcm1GaWVsZElucHV0UGFzc3dvcmQuZ2V0U2hvd1Bhc3N3b3JkSW5NcygpXCJcbiAgICAgICAgICAgICAgICAgICAgKGNsaWNrKT1cInRoaXMuZm9ybUZpZWxkSW5wdXRQYXNzd29yZC5vblNob3dQYXNzd29yZCgpXCJcbiAgICAgICAgICAgICAgICAgICAgbWF0U3VmZml4PiB7eyB0aGlzLmZvcm1GaWVsZElucHV0UGFzc3dvcmQuZ2V0U2hvd1Bhc3N3b3JkKCkgPyAndmlzaWJpbGl0eScgOiAndmlzaWJpbGl0eV9vZmYnfX0gPC9tYXQtaWNvbj5cblxuICAgICAgICAgICAgICAgIDxzcGFuIFxuICAgICAgICAgICAgICAgICAgICAqbmdJZj1cInRoaXMuZm9ybUZpZWxkSW5wdXRQYXNzd29yZC5nZXRUZXh0UHJlZml4KClcIlxuICAgICAgICAgICAgICAgICAgICBtYXRQcmVmaXg+e3sgdGhpcy5mb3JtRmllbGRJbnB1dFBhc3N3b3JkLmdldFRleHRQcmVmaXgoKSB9fSZuYnNwOzwvc3Bhbj5cblxuICAgICAgICAgICAgICAgIDxzcGFuIFxuICAgICAgICAgICAgICAgICAgICAqbmdJZj1cInRoaXMuZm9ybUZpZWxkSW5wdXRQYXNzd29yZC5nZXRUZXh0U3VmZml4KClcIlxuICAgICAgICAgICAgICAgICAgICBtYXRTdWZmaXg+e3sgdGhpcy5mb3JtRmllbGRJbnB1dFBhc3N3b3JkLmdldFRleHRTdWZmaXgoKSB9fTwvc3Bhbj5cblxuICAgICAgICAgICAgICAgIDxtYXQtaGludCBcbiAgICAgICAgICAgICAgICAgICAgKm5nSWY9XCJ0aGlzLmZvcm1GaWVsZElucHV0UGFzc3dvcmQuZ2V0TGVmdEhpbnRMYWJlbCgpXCJcbiAgICAgICAgICAgICAgICAgICAgYWxpZ249XCJzdGFydFwiPnt7IHRoaXMuZm9ybUZpZWxkSW5wdXRQYXNzd29yZC5nZXRMZWZ0SGludExhYmVsKCkgfX08L21hdC1oaW50PlxuXG4gICAgICAgICAgICAgICAgPG1hdC1oaW50XG4gICAgICAgICAgICAgICAgICAgICpuZ0lmPVwidGhpcy5mb3JtRmllbGRJbnB1dFBhc3N3b3JkLmdldFJpZ2h0SGludExhYmVsKClcIlxuICAgICAgICAgICAgICAgICAgICBhbGlnbj1cImVuZFwiPnt7IHRoaXMuZm9ybUZpZWxkSW5wdXRQYXNzd29yZC5nZXRSaWdodEhpbnRMYWJlbCgpIH19PC9tYXQtaGludD5cblxuICAgICAgICAgICAgICAgIDwhLS0gaWYgZmllbGQgaGFzIFNob3dIaW50QWJvdXRNaW5NYXhMZW5ndGggc2V0IHRvIHRydWUgLS0+XG4gICAgICAgICAgICAgICAgPG1hdC1oaW50XG4gICAgICAgICAgICAgICAgICAgICpuZ0lmPVwidGhpcy5mb3JtRmllbGRJbnB1dFBhc3N3b3JkLmdldFNob3dIaW50QWJvdXRNaW5NYXhMZW5ndGgoKSAmJiB0aGlzLmdldE1pbkxlbmd0aFZhbGlkYXRvcigpICYmIHRoaXMuZ2V0TWF4TGVuZ3RoVmFsaWRhdG9yKClcIlxuICAgICAgICAgICAgICAgICAgICBhbGlnbj1cImVuZFwiPnt7IHRoaXMuZ2V0TWluTGVuZ3RoVmFsaWRhdG9yKCk/Lmxlbmd0aCA/IHRoaXMuZ2V0TWluTGVuZ3RoVmFsaWRhdG9yKCkubGVuZ3RoICsgXCIgLyBcIiA6ICcnfX17eyB0aGlzLmZvcm1GaWVsZElucHV0UGFzc3dvcmQuZ2V0RGVmYXVsdFZhbHVlKCk/Lmxlbmd0aCB9fXt7IHRoaXMuZ2V0TWF4TGVuZ3RoVmFsaWRhdG9yKCk/Lmxlbmd0aCA/IFwiIC8gXCIgKyB0aGlzLmdldE1heExlbmd0aFZhbGlkYXRvcigpLmxlbmd0aDogJyd9fTwvbWF0LWhpbnQ+XG5cbiAgICAgICAgICAgICAgICA8bWF0LWhpbnRcbiAgICAgICAgICAgICAgICAgICAgKm5nSWY9XCJ0aGlzLmZvcm1GaWVsZElucHV0UGFzc3dvcmQuZ2V0U2hvd0hpbnRBYm91dE1pbk1heExlbmd0aCgpICYmIHRoaXMuZ2V0TWluTGVuZ3RoVmFsaWRhdG9yKCkgJiYgIXRoaXMuZ2V0TWF4TGVuZ3RoVmFsaWRhdG9yKClcIlxuICAgICAgICAgICAgICAgICAgICBhbGlnbj1cImVuZFwiPnt7IHRoaXMuZm9ybUZpZWxkSW5wdXRQYXNzd29yZC5nZXREZWZhdWx0VmFsdWUoKT8ubGVuZ3RoIH19e3sgdGhpcy5nZXRNaW5MZW5ndGhWYWxpZGF0b3IoKT8ubGVuZ3RoID8gXCIgLyBcIiArIHRoaXMuZ2V0TWluTGVuZ3RoVmFsaWRhdG9yKCkubGVuZ3RoIDogJyd9fTwvbWF0LWhpbnQ+XG5cbiAgICAgICAgICAgICAgICA8bWF0LWhpbnRcbiAgICAgICAgICAgICAgICAgICAgKm5nSWY9XCJ0aGlzLmZvcm1GaWVsZElucHV0UGFzc3dvcmQuZ2V0U2hvd0hpbnRBYm91dE1pbk1heExlbmd0aCgpICYmICF0aGlzLmdldE1pbkxlbmd0aFZhbGlkYXRvcigpICYmIHRoaXMuZ2V0TWF4TGVuZ3RoVmFsaWRhdG9yKClcIlxuICAgICAgICAgICAgICAgICAgICBhbGlnbj1cImVuZFwiPnt7IHRoaXMuZm9ybUZpZWxkSW5wdXRQYXNzd29yZC5nZXREZWZhdWx0VmFsdWUoKT8ubGVuZ3RoIH19e3sgdGhpcy5nZXRNYXhMZW5ndGhWYWxpZGF0b3IoKT8ubGVuZ3RoID8gXCIgLyBcIiArIHRoaXMuZ2V0TWF4TGVuZ3RoVmFsaWRhdG9yKCkubGVuZ3RoOiAnJ319PC9tYXQtaGludD5cblxuICAgICAgICA8L21hdC1mb3JtLWZpZWxkPlxuXG48L3NlY3Rpb24+Il19