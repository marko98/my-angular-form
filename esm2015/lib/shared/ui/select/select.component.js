import { Component, forwardRef, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import * as i0 from "@angular/core";
import * as i1 from "@angular/flex-layout/flex";
import * as i2 from "@angular/material/form-field";
import * as i3 from "@angular/common";
import * as i4 from "@angular/material/select";
import * as i5 from "@angular/material/core";
import * as i6 from "@angular/material/icon";
function SelectComponent_mat_label_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-label");
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r0 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r0.select.getLabelName());
} }
function SelectComponent_mat_select_trigger_5_span_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span");
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r10 = i0.ɵɵnextContext(2);
    var tmp_0_0 = null;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate2(" (+", ctx_r10.select.getDefaultValue().length - 1, " ", ((tmp_0_0 = ctx_r10.select.getDefaultValue()) == null ? null : tmp_0_0.length) === 2 ? "other" : "others", ") ");
} }
function SelectComponent_mat_select_trigger_5_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-select-trigger");
    i0.ɵɵtext(1);
    i0.ɵɵtemplate(2, SelectComponent_mat_select_trigger_5_span_2_Template, 2, 2, "span", 2);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r2 = i0.ɵɵnextContext();
    var tmp_1_0 = null;
    const currVal_1 = ((tmp_1_0 = ctx_r2.select.getDefaultValue()) == null ? null : tmp_1_0.length) > 1;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r2.select.getDefaultValue() ? ctx_r2.select.getDefaultValue()[0] : "", " ");
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", currVal_1);
} }
function SelectComponent_section_6_mat_option_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-option", 12);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const optionValue_r11 = i0.ɵɵnextContext().$implicit;
    i0.ɵɵproperty("value", optionValue_r11.value)("disabled", optionValue_r11.disabled);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1("", optionValue_r11.textToShow, " ");
} }
function SelectComponent_section_6_mat_optgroup_2_mat_option_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-option", 12);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const oV_r16 = ctx.$implicit;
    i0.ɵɵproperty("value", oV_r16.value)("disabled", oV_r16.disabled);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", oV_r16.textToShow, " ");
} }
function SelectComponent_section_6_mat_optgroup_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-optgroup", 13);
    i0.ɵɵtemplate(1, SelectComponent_section_6_mat_optgroup_2_mat_option_1_Template, 2, 3, "mat-option", 14);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const optionValue_r11 = i0.ɵɵnextContext().$implicit;
    i0.ɵɵproperty("label", optionValue_r11.labelName)("disabled", optionValue_r11.disabled);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngForOf", optionValue_r11.optionValues);
} }
function SelectComponent_section_6_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "section");
    i0.ɵɵtemplate(1, SelectComponent_section_6_mat_option_1_Template, 2, 3, "mat-option", 10);
    i0.ɵɵtemplate(2, SelectComponent_section_6_mat_optgroup_2_Template, 2, 3, "mat-optgroup", 11);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const optionValue_r11 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !optionValue_r11.optionValues);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", optionValue_r11.optionValues && optionValue_r11.labelName);
} }
function SelectComponent_mat_icon_7_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-icon", 15);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r4 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r4.select.getMatPrefixImgText());
} }
function SelectComponent_mat_icon_8_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-icon", 16);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r5 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r5.select.getMatSuffixImgText());
} }
function SelectComponent_span_9_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 15);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r6 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1("", ctx_r6.select.getTextPrefix(), "\u00A0");
} }
function SelectComponent_span_10_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 16);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r7 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r7.select.getTextSuffix());
} }
function SelectComponent_mat_hint_11_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-hint", 17);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r8 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r8.select.getLeftHintLabel());
} }
function SelectComponent_mat_hint_12_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-hint", 18);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r9 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r9.select.getRightHintLabel());
} }
export class SelectComponent {
    constructor() {
        this.onChange = () => { };
        this.onTouched = () => { };
        this.onSelectionChange = (select) => {
            // console.log(select.value);
            this.select.setDefaultValue(select.value);
            this.onChange(this.select.getDefaultValue());
            this.onTouched();
        };
        this.onBlur = () => {
            this.onChange(this.select.getDefaultValue());
            this.onTouched();
        };
    }
    registerOnChange(fn) {
        this.onChange = fn;
    }
    registerOnTouched(fn) {
        this.onTouched = fn;
    }
    writeValue(value) {
        if (value) {
            this.select.setDefaultValue(value);
        }
    }
    ngOnInit() {
        // console.log('SelectComponent init');
    }
    ngOnDestroy() {
        // console.log('SelectComponent destroyed');
    }
}
SelectComponent.ɵfac = function SelectComponent_Factory(t) { return new (t || SelectComponent)(); };
SelectComponent.ɵcmp = i0.ɵɵdefineComponent({ type: SelectComponent, selectors: [["app-select"]], inputs: { select: "select" }, features: [i0.ɵɵProvidersFeature([
            {
                provide: NG_VALUE_ACCESSOR,
                multi: true,
                useExisting: forwardRef(() => SelectComponent),
            },
        ])], decls: 13, vars: 15, consts: [["fxLayoutAlign", "center center"], [3, "appearance", "hideRequiredMarker"], [4, "ngIf"], [3, "value", "multiple", "required", "disabled", "selectionChange", "blur"], ["matSelect", ""], [4, "ngFor", "ngForOf"], ["matPrefix", "", 4, "ngIf"], ["matSuffix", "", 4, "ngIf"], ["align", "start", 4, "ngIf"], ["align", "end", 4, "ngIf"], [3, "value", "disabled", 4, "ngIf"], [3, "label", "disabled", 4, "ngIf"], [3, "value", "disabled"], [3, "label", "disabled"], [3, "value", "disabled", 4, "ngFor", "ngForOf"], ["matPrefix", ""], ["matSuffix", ""], ["align", "start"], ["align", "end"]], template: function SelectComponent_Template(rf, ctx) { if (rf & 1) {
        const _r18 = i0.ɵɵgetCurrentView();
        i0.ɵɵelementStart(0, "section", 0);
        i0.ɵɵelementStart(1, "mat-form-field", 1);
        i0.ɵɵtemplate(2, SelectComponent_mat_label_2_Template, 2, 1, "mat-label", 2);
        i0.ɵɵelementStart(3, "mat-select", 3, 4);
        i0.ɵɵlistener("selectionChange", function SelectComponent_Template_mat_select_selectionChange_3_listener() { i0.ɵɵrestoreView(_r18); const _r1 = i0.ɵɵreference(4); return ctx.onSelectionChange(_r1); })("blur", function SelectComponent_Template_mat_select_blur_3_listener() { return ctx.onBlur(); });
        i0.ɵɵtemplate(5, SelectComponent_mat_select_trigger_5_Template, 3, 2, "mat-select-trigger", 2);
        i0.ɵɵtemplate(6, SelectComponent_section_6_Template, 3, 2, "section", 5);
        i0.ɵɵelementEnd();
        i0.ɵɵtemplate(7, SelectComponent_mat_icon_7_Template, 2, 1, "mat-icon", 6);
        i0.ɵɵtemplate(8, SelectComponent_mat_icon_8_Template, 2, 1, "mat-icon", 7);
        i0.ɵɵtemplate(9, SelectComponent_span_9_Template, 2, 1, "span", 6);
        i0.ɵɵtemplate(10, SelectComponent_span_10_Template, 2, 1, "span", 7);
        i0.ɵɵtemplate(11, SelectComponent_mat_hint_11_Template, 2, 1, "mat-hint", 8);
        i0.ɵɵtemplate(12, SelectComponent_mat_hint_12_Template, 2, 1, "mat-hint", 9);
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("appearance", ctx.select.getAppearance())("hideRequiredMarker", ctx.select.getRequired().hideRequiredMarker);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.select.getLabelName());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("value", ctx.select.getDefaultValue())("multiple", ctx.select.getMultiple())("required", ctx.select.getRequired().required)("disabled", ctx.select.getDisabled());
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("ngIf", ctx.select.getMatSelectTriggerOn());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngForOf", ctx.select.getOptionValues());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.select.getMatPrefixImgText());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.select.getMatSuffixImgText());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.select.getTextPrefix());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.select.getTextSuffix());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.select.getLeftHintLabel());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.select.getRightHintLabel());
    } }, directives: [i1.DefaultLayoutAlignDirective, i2.MatFormField, i3.NgIf, i4.MatSelect, i3.NgForOf, i2.MatLabel, i4.MatSelectTrigger, i5.MatOption, i5.MatOptgroup, i6.MatIcon, i2.MatPrefix, i2.MatSuffix, i2.MatHint], styles: ["", ".cursor[_ngcontent-%COMP%] {\n        cursor: pointer;\n    }\n\n    input[type=\"file\"][_ngcontent-%COMP%] {\n        display: none;\n    }\n\n    h3[_ngcontent-%COMP%] {\n        font-size: inherit;\n        margin: 0;\n        font-weight: inherit;\n    }\n\n    mat-form-field[_ngcontent-%COMP%] {\n        min-width: 250px;\n    }"] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(SelectComponent, [{
        type: Component,
        args: [{
                selector: 'app-select',
                templateUrl: './select.component.html',
                styleUrls: ['./select.component.css'],
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        multi: true,
                        useExisting: forwardRef(() => SelectComponent),
                    },
                ],
            }]
    }], function () { return []; }, { select: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VsZWN0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215LWFuZ3VsYXItZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvdWkvc2VsZWN0L3NlbGVjdC5jb21wb25lbnQudHMiLCJsaWIvc2hhcmVkL3VpL3NlbGVjdC9zZWxlY3QuY29tcG9uZW50Lmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxVQUFVLEVBQUUsS0FBSyxFQUFhLE1BQU0sZUFBZSxDQUFDO0FBQ2hGLE9BQU8sRUFBRSxpQkFBaUIsRUFBdUIsTUFBTSxnQkFBZ0IsQ0FBQzs7Ozs7Ozs7O0lDNEJ4RCxpQ0FDdUM7SUFBQSxZQUFnQztJQUFBLGlCQUFZOzs7SUFBNUMsZUFBZ0M7SUFBaEMsa0RBQWdDOzs7SUFlM0QsNEJBQ0E7SUFBQSxZQUNBO0lBQUEsaUJBQU87Ozs7SUFEUCxlQUNBO0lBREEsK0xBQ0E7OztJQUxSLDBDQUVRO0lBQUEsWUFDQTtJQUFBLHVGQUNBO0lBRVIsaUJBQXFCOzs7OztJQUpiLGVBQ0E7SUFEQSwwR0FDQTtJQUFNLGVBQWlEO0lBQWpELGdDQUFpRDs7O0lBUXZELHNDQUdzQztJQUFBLFlBQ3RDO0lBQUEsaUJBQWE7OztJQUZULDZDQUEyQixzQ0FBQTtJQUNPLGVBQ3RDO0lBRHNDLDBEQUN0Qzs7O0lBT1Esc0NBSVE7SUFBQSxZQUNSO0lBQUEsaUJBQWE7OztJQUhULG9DQUFrQiw2QkFBQTtJQUVkLGVBQ1I7SUFEUSxrREFDUjs7O0lBVlIsd0NBS1E7SUFBQSx3R0FJUTtJQUdoQixpQkFBZTs7O0lBVlgsaURBQStCLHNDQUFBO0lBSXZCLGVBQTJDO0lBQTNDLHNEQUEyQzs7O0lBZi9ELCtCQUdRO0lBQUEseUZBR3NDO0lBR3RDLDZGQUtRO0lBU2hCLGlCQUFVOzs7SUFuQkUsZUFBaUM7SUFBakMsb0RBQWlDO0lBTWpDLGVBQXlEO0lBQXpELGdGQUF5RDs7O0lBZ0J6RSxvQ0FFYztJQUFBLFlBQXVDO0lBQUEsaUJBQVc7OztJQUFsRCxlQUF1QztJQUF2Qyx5REFBdUM7OztJQUVyRCxvQ0FFYztJQUFBLFlBQXVDO0lBQUEsaUJBQVc7OztJQUFsRCxlQUF1QztJQUF2Qyx5REFBdUM7OztJQUVyRCxnQ0FFYztJQUFBLFlBQXVDO0lBQUEsaUJBQU87OztJQUE5QyxlQUF1QztJQUF2QyxrRUFBdUM7OztJQUVyRCxnQ0FFYztJQUFBLFlBQWlDO0lBQUEsaUJBQU87OztJQUF4QyxlQUFpQztJQUFqQyxtREFBaUM7OztJQUUvQyxvQ0FFa0I7SUFBQSxZQUFvQztJQUFBLGlCQUFXOzs7SUFBL0MsZUFBb0M7SUFBcEMsc0RBQW9DOzs7SUFFdEQsb0NBRWdCO0lBQUEsWUFBcUM7SUFBQSxpQkFBVzs7O0lBQWhELGVBQXFDO0lBQXJDLHVEQUFxQzs7QURqRnJFLE1BQU0sT0FBTyxlQUFlO0lBTTFCO1FBSE8sYUFBUSxHQUFRLEdBQUcsRUFBRSxHQUFFLENBQUMsQ0FBQztRQUN6QixjQUFTLEdBQVEsR0FBRyxFQUFFLEdBQUUsQ0FBQyxDQUFDO1FBSWpDLHNCQUFpQixHQUFHLENBQUMsTUFBaUIsRUFBRSxFQUFFO1lBQ3hDLDZCQUE2QjtZQUM3QixJQUFJLENBQUMsTUFBTSxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLGVBQWUsRUFBRSxDQUFDLENBQUM7WUFDN0MsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ25CLENBQUMsQ0FBQztRQUVGLFdBQU0sR0FBRyxHQUFHLEVBQUU7WUFDWixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsZUFBZSxFQUFFLENBQUMsQ0FBQztZQUM3QyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDbkIsQ0FBQyxDQUFDO0lBWmEsQ0FBQztJQWNoQixnQkFBZ0IsQ0FBQyxFQUFFO1FBQ2pCLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO0lBQ3JCLENBQUM7SUFFRCxpQkFBaUIsQ0FBQyxFQUFFO1FBQ2xCLElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO0lBQ3RCLENBQUM7SUFFRCxVQUFVLENBQUMsS0FBSztRQUNkLElBQUksS0FBSyxFQUFFO1lBQ1QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDcEM7SUFDSCxDQUFDO0lBRUQsUUFBUTtRQUNOLHVDQUF1QztJQUN6QyxDQUFDO0lBRUQsV0FBVztRQUNULDRDQUE0QztJQUM5QyxDQUFDOzs4RUF4Q1UsZUFBZTtvREFBZixlQUFlLDhGQVJmO1lBQ1Q7Z0JBQ0UsT0FBTyxFQUFFLGlCQUFpQjtnQkFDMUIsS0FBSyxFQUFFLElBQUk7Z0JBQ1gsV0FBVyxFQUFFLFVBQVUsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxlQUFlLENBQUM7YUFDL0M7U0FDRjs7UUNPSCxrQ0FHUTtRQUFBLHlDQUlRO1FBQUEsNEVBQ3VDO1FBR3ZDLHdDQVNJO1FBSkEsMktBQW1CLDBCQUFpQyxJQUFDLGlGQUM3QyxZQUFhLElBRGdDO1FBSXJELDhGQUVRO1FBTVIsd0VBR1E7UUFxQlosaUJBQWE7UUFFYiwwRUFFYztRQUVkLDBFQUVjO1FBRWQsa0VBRWM7UUFFZCxvRUFFYztRQUVkLDRFQUVrQjtRQUVsQiw0RUFFZ0I7UUFFeEIsaUJBQWlCO1FBRXpCLGlCQUFVOztRQTVFRSxlQUEwQztRQUExQyx1REFBMEMsbUVBQUE7UUFJbEMsZUFBa0M7UUFBbEMsZ0RBQWtDO1FBSWxDLGVBQXVDO1FBQXZDLG9EQUF1QyxzQ0FBQSwrQ0FBQSxzQ0FBQTtRQVNuQyxlQUEyQztRQUEzQyx5REFBMkM7UUFRM0MsZUFBeUQ7UUFBekQsc0RBQXlEO1FBMEI3RCxlQUF5QztRQUF6Qyx1REFBeUM7UUFJekMsZUFBeUM7UUFBekMsdURBQXlDO1FBSXpDLGVBQW1DO1FBQW5DLGlEQUFtQztRQUluQyxlQUFtQztRQUFuQyxpREFBbUM7UUFJbkMsZUFBc0M7UUFBdEMsb0RBQXNDO1FBSXRDLGVBQXVDO1FBQXZDLHFEQUF1Qzs7a0REaEY5QyxlQUFlO2NBWjNCLFNBQVM7ZUFBQztnQkFDVCxRQUFRLEVBQUUsWUFBWTtnQkFDdEIsV0FBVyxFQUFFLHlCQUF5QjtnQkFDdEMsU0FBUyxFQUFFLENBQUMsd0JBQXdCLENBQUM7Z0JBQ3JDLFNBQVMsRUFBRTtvQkFDVDt3QkFDRSxPQUFPLEVBQUUsaUJBQWlCO3dCQUMxQixLQUFLLEVBQUUsSUFBSTt3QkFDWCxXQUFXLEVBQUUsVUFBVSxDQUFDLEdBQUcsRUFBRSxDQUFDLGVBQWUsQ0FBQztxQkFDL0M7aUJBQ0Y7YUFDRjs7a0JBRUUsS0FBSyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBmb3J3YXJkUmVmLCBJbnB1dCwgT25EZXN0cm95IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBOR19WQUxVRV9BQ0NFU1NPUiwgTmdGb3JtLCBGb3JtQ29udHJvbCB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7IFNlbGVjdCB9IGZyb20gJy4uLy4uL21vZGVsL3N0cnVjdHVyYWwvY29tcG9zaXRlL2Zvcm0vc2VsZWN0L3NlbGVjdC5tb2RlbCc7XG5pbXBvcnQgeyBNYXRTZWxlY3QgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9zZWxlY3QnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdhcHAtc2VsZWN0JyxcbiAgdGVtcGxhdGVVcmw6ICcuL3NlbGVjdC5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL3NlbGVjdC5jb21wb25lbnQuY3NzJ10sXG4gIHByb3ZpZGVyczogW1xuICAgIHtcbiAgICAgIHByb3ZpZGU6IE5HX1ZBTFVFX0FDQ0VTU09SLFxuICAgICAgbXVsdGk6IHRydWUsXG4gICAgICB1c2VFeGlzdGluZzogZm9yd2FyZFJlZigoKSA9PiBTZWxlY3RDb21wb25lbnQpLFxuICAgIH0sXG4gIF0sXG59KVxuZXhwb3J0IGNsYXNzIFNlbGVjdENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcbiAgQElucHV0KCkgc2VsZWN0OiBTZWxlY3Q7XG5cbiAgcHVibGljIG9uQ2hhbmdlOiBhbnkgPSAoKSA9PiB7fTtcbiAgcHVibGljIG9uVG91Y2hlZDogYW55ID0gKCkgPT4ge307XG5cbiAgY29uc3RydWN0b3IoKSB7fVxuXG4gIG9uU2VsZWN0aW9uQ2hhbmdlID0gKHNlbGVjdDogTWF0U2VsZWN0KSA9PiB7XG4gICAgLy8gY29uc29sZS5sb2coc2VsZWN0LnZhbHVlKTtcbiAgICB0aGlzLnNlbGVjdC5zZXREZWZhdWx0VmFsdWUoc2VsZWN0LnZhbHVlKTtcbiAgICB0aGlzLm9uQ2hhbmdlKHRoaXMuc2VsZWN0LmdldERlZmF1bHRWYWx1ZSgpKTtcbiAgICB0aGlzLm9uVG91Y2hlZCgpO1xuICB9O1xuXG4gIG9uQmx1ciA9ICgpID0+IHtcbiAgICB0aGlzLm9uQ2hhbmdlKHRoaXMuc2VsZWN0LmdldERlZmF1bHRWYWx1ZSgpKTtcbiAgICB0aGlzLm9uVG91Y2hlZCgpO1xuICB9O1xuXG4gIHJlZ2lzdGVyT25DaGFuZ2UoZm4pIHtcbiAgICB0aGlzLm9uQ2hhbmdlID0gZm47XG4gIH1cblxuICByZWdpc3Rlck9uVG91Y2hlZChmbikge1xuICAgIHRoaXMub25Ub3VjaGVkID0gZm47XG4gIH1cblxuICB3cml0ZVZhbHVlKHZhbHVlKSB7XG4gICAgaWYgKHZhbHVlKSB7XG4gICAgICB0aGlzLnNlbGVjdC5zZXREZWZhdWx0VmFsdWUodmFsdWUpO1xuICAgIH1cbiAgfVxuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICAgIC8vIGNvbnNvbGUubG9nKCdTZWxlY3RDb21wb25lbnQgaW5pdCcpO1xuICB9XG5cbiAgbmdPbkRlc3Ryb3koKTogdm9pZCB7XG4gICAgLy8gY29uc29sZS5sb2coJ1NlbGVjdENvbXBvbmVudCBkZXN0cm95ZWQnKTtcbiAgfVxufVxuIiwiPHN0eWxlPlxuXG4gICAgLmN1cnNvciB7XG4gICAgICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICB9XG5cbiAgICBpbnB1dFt0eXBlPVwiZmlsZVwiXSB7XG4gICAgICAgIGRpc3BsYXk6IG5vbmU7XG4gICAgfVxuXG4gICAgaDMge1xuICAgICAgICBmb250LXNpemU6IGluaGVyaXQ7XG4gICAgICAgIG1hcmdpbjogMDtcbiAgICAgICAgZm9udC13ZWlnaHQ6IGluaGVyaXQ7XG4gICAgfVxuXG4gICAgbWF0LWZvcm0tZmllbGQge1xuICAgICAgICBtaW4td2lkdGg6IDI1MHB4O1xuICAgIH1cblxuPC9zdHlsZT5cbiAgICBcbjxzZWN0aW9uXG4gICAgZnhMYXlvdXRBbGlnbj1cImNlbnRlciBjZW50ZXJcIj5cblxuICAgICAgICA8bWF0LWZvcm0tZmllbGQgXG4gICAgICAgICAgICBbYXBwZWFyYW5jZV09XCJ0aGlzLnNlbGVjdC5nZXRBcHBlYXJhbmNlKClcIlxuICAgICAgICAgICAgW2hpZGVSZXF1aXJlZE1hcmtlcl09XCJ0aGlzLnNlbGVjdC5nZXRSZXF1aXJlZCgpLmhpZGVSZXF1aXJlZE1hcmtlclwiPlxuXG4gICAgICAgICAgICAgICAgPG1hdC1sYWJlbFxuICAgICAgICAgICAgICAgICAgICAqbmdJZj1cInRoaXMuc2VsZWN0LmdldExhYmVsTmFtZSgpXCI+e3sgdGhpcy5zZWxlY3QuZ2V0TGFiZWxOYW1lKCkgfX08L21hdC1sYWJlbD5cblxuXG4gICAgICAgICAgICAgICAgPG1hdC1zZWxlY3RcbiAgICAgICAgICAgICAgICAgICAgW3ZhbHVlXT1cInRoaXMuc2VsZWN0LmdldERlZmF1bHRWYWx1ZSgpXCJcbiAgICAgICAgICAgICAgICAgICAgW211bHRpcGxlXT1cInRoaXMuc2VsZWN0LmdldE11bHRpcGxlKClcIlxuICAgICAgICAgICAgICAgICAgICBbcmVxdWlyZWRdPVwidGhpcy5zZWxlY3QuZ2V0UmVxdWlyZWQoKS5yZXF1aXJlZFwiXG4gICAgICAgICAgICAgICAgICAgIFtkaXNhYmxlZF09XCJ0aGlzLnNlbGVjdC5nZXREaXNhYmxlZCgpXCJcbiAgICAgICAgICAgICAgICAgICAgKHNlbGVjdGlvbkNoYW5nZSk9XCJ0aGlzLm9uU2VsZWN0aW9uQ2hhbmdlKG1hdFNlbGVjdClcIlxuICAgICAgICAgICAgICAgICAgICAoYmx1cik9XCJ0aGlzLm9uQmx1cigpXCJcbiAgICAgICAgICAgICAgICAgICAgI21hdFNlbGVjdD5cblxuICAgICAgICAgICAgICAgICAgICA8bWF0LXNlbGVjdC10cmlnZ2VyXG4gICAgICAgICAgICAgICAgICAgICAgICAqbmdJZj1cInRoaXMuc2VsZWN0LmdldE1hdFNlbGVjdFRyaWdnZXJPbigpXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge3t0aGlzLnNlbGVjdC5nZXREZWZhdWx0VmFsdWUoKSA/IHRoaXMuc2VsZWN0LmdldERlZmF1bHRWYWx1ZSgpWzBdIDogJyd9fVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuICpuZ0lmPVwidGhpcy5zZWxlY3QuZ2V0RGVmYXVsdFZhbHVlKCk/Lmxlbmd0aCA+IDFcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAoK3t7dGhpcy5zZWxlY3QuZ2V0RGVmYXVsdFZhbHVlKCkubGVuZ3RoIC0gMX19IHt7dGhpcy5zZWxlY3QuZ2V0RGVmYXVsdFZhbHVlKCk/Lmxlbmd0aCA9PT0gMiA/ICdvdGhlcicgOiAnb3RoZXJzJ319KVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgPC9tYXQtc2VsZWN0LXRyaWdnZXI+XG5cbiAgICAgICAgICAgICAgICAgICAgPHNlY3Rpb25cbiAgICAgICAgICAgICAgICAgICAgICAgICpuZ0Zvcj1cImxldCBvcHRpb25WYWx1ZSBvZiB0aGlzLnNlbGVjdC5nZXRPcHRpb25WYWx1ZXMoKVwiPlxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPG1hdC1vcHRpb25cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKm5nSWY9XCIhb3B0aW9uVmFsdWUub3B0aW9uVmFsdWVzXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgW3ZhbHVlXT1cIm9wdGlvblZhbHVlLnZhbHVlXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgW2Rpc2FibGVkXT1cIm9wdGlvblZhbHVlLmRpc2FibGVkXCI+e3sgb3B0aW9uVmFsdWUudGV4dFRvU2hvdyB9fVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvbWF0LW9wdGlvbj5cblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxtYXQtb3B0Z3JvdXAgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICpuZ0lmPVwib3B0aW9uVmFsdWUub3B0aW9uVmFsdWVzICYmIG9wdGlvblZhbHVlLmxhYmVsTmFtZVwiIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbbGFiZWxdPVwib3B0aW9uVmFsdWUubGFiZWxOYW1lXCIgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtkaXNhYmxlZF09XCJvcHRpb25WYWx1ZS5kaXNhYmxlZFwiPlxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bWF0LW9wdGlvbiBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAqbmdGb3I9XCJsZXQgb1Ygb2Ygb3B0aW9uVmFsdWUub3B0aW9uVmFsdWVzXCIgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgW3ZhbHVlXT1cIm9WLnZhbHVlXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbZGlzYWJsZWRdPVwib1YuZGlzYWJsZWRcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge3sgb1YudGV4dFRvU2hvdyB9fVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9tYXQtb3B0aW9uPlxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9tYXQtb3B0Z3JvdXA+XG5cbiAgICAgICAgICAgICAgICAgICAgPC9zZWN0aW9uPlxuICAgICAgICAgICAgICAgIDwvbWF0LXNlbGVjdD5cblxuICAgICAgICAgICAgICAgIDxtYXQtaWNvbiBcbiAgICAgICAgICAgICAgICAgICAgKm5nSWY9XCJ0aGlzLnNlbGVjdC5nZXRNYXRQcmVmaXhJbWdUZXh0KClcIlxuICAgICAgICAgICAgICAgICAgICBtYXRQcmVmaXg+e3sgdGhpcy5zZWxlY3QuZ2V0TWF0UHJlZml4SW1nVGV4dCgpIH19PC9tYXQtaWNvbj5cblxuICAgICAgICAgICAgICAgIDxtYXQtaWNvbiBcbiAgICAgICAgICAgICAgICAgICAgKm5nSWY9XCJ0aGlzLnNlbGVjdC5nZXRNYXRTdWZmaXhJbWdUZXh0KClcIlxuICAgICAgICAgICAgICAgICAgICBtYXRTdWZmaXg+e3sgdGhpcy5zZWxlY3QuZ2V0TWF0U3VmZml4SW1nVGV4dCgpIH19PC9tYXQtaWNvbj5cblxuICAgICAgICAgICAgICAgIDxzcGFuIFxuICAgICAgICAgICAgICAgICAgICAqbmdJZj1cInRoaXMuc2VsZWN0LmdldFRleHRQcmVmaXgoKVwiXG4gICAgICAgICAgICAgICAgICAgIG1hdFByZWZpeD57eyB0aGlzLnNlbGVjdC5nZXRUZXh0UHJlZml4KCkgfX0mbmJzcDs8L3NwYW4+XG5cbiAgICAgICAgICAgICAgICA8c3BhbiBcbiAgICAgICAgICAgICAgICAgICAgKm5nSWY9XCJ0aGlzLnNlbGVjdC5nZXRUZXh0U3VmZml4KClcIlxuICAgICAgICAgICAgICAgICAgICBtYXRTdWZmaXg+e3sgdGhpcy5zZWxlY3QuZ2V0VGV4dFN1ZmZpeCgpIH19PC9zcGFuPlxuXG4gICAgICAgICAgICAgICAgPG1hdC1oaW50IFxuICAgICAgICAgICAgICAgICAgICAqbmdJZj1cInRoaXMuc2VsZWN0LmdldExlZnRIaW50TGFiZWwoKVwiXG4gICAgICAgICAgICAgICAgICAgIGFsaWduPVwic3RhcnRcIj57eyB0aGlzLnNlbGVjdC5nZXRMZWZ0SGludExhYmVsKCkgfX08L21hdC1oaW50PlxuXG4gICAgICAgICAgICAgICAgPG1hdC1oaW50XG4gICAgICAgICAgICAgICAgICAgICpuZ0lmPVwidGhpcy5zZWxlY3QuZ2V0UmlnaHRIaW50TGFiZWwoKVwiXG4gICAgICAgICAgICAgICAgICAgIGFsaWduPVwiZW5kXCI+e3sgdGhpcy5zZWxlY3QuZ2V0UmlnaHRIaW50TGFiZWwoKSB9fTwvbWF0LWhpbnQ+XG5cbiAgICAgICAgPC9tYXQtZm9ybS1maWVsZD5cblxuPC9zZWN0aW9uPiJdfQ==