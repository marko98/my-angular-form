import { Component, Input, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import * as i0 from "@angular/core";
import * as i1 from "@angular/flex-layout/flex";
import * as i2 from "@angular/material/checkbox";
export class CheckboxComponent {
    constructor() {
        this.onChange = () => { };
        this.onTouched = () => { };
        this.onClick = () => {
            this.checkbox.setDefaultValue(!this.checkbox.getDefaultValue());
            this.onChange(this.checkbox.getDefaultValue());
            this.onTouched();
            // console.log(this.checkbox);
        };
    }
    registerOnChange(fn) {
        this.onChange = fn;
    }
    registerOnTouched(fn) {
        this.onTouched = fn;
    }
    writeValue(value) {
        if (value) {
            this.checkbox.setDefaultValue(value);
        }
    }
    ngOnInit() {
        // console.log(this.checkbox);
        // console.log('CheckboxComponent init');
    }
    ngOnDestroy() {
        // console.log('CheckboxComponent destroyed');
    }
}
CheckboxComponent.ɵfac = function CheckboxComponent_Factory(t) { return new (t || CheckboxComponent)(); };
CheckboxComponent.ɵcmp = i0.ɵɵdefineComponent({ type: CheckboxComponent, selectors: [["app-checkbox"]], inputs: { checkbox: "checkbox" }, features: [i0.ɵɵProvidersFeature([
            {
                provide: NG_VALUE_ACCESSOR,
                multi: true,
                useExisting: forwardRef(() => CheckboxComponent),
            },
        ])], decls: 3, vars: 5, consts: [["fxLayoutAlign", "center center"], [3, "value", "labelPosition", "disabled", "color", "click"]], template: function CheckboxComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "section", 0);
        i0.ɵɵelementStart(1, "mat-checkbox", 1);
        i0.ɵɵlistener("click", function CheckboxComponent_Template_mat_checkbox_click_1_listener() { return ctx.onClick(); });
        i0.ɵɵtext(2);
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("value", ctx.checkbox.getDefaultValue())("labelPosition", ctx.checkbox.getLabelPosition())("disabled", ctx.checkbox.getDisabled())("color", ctx.checkbox.getColor());
        i0.ɵɵadvance(1);
        i0.ɵɵtextInterpolate(ctx.checkbox.getName());
    } }, directives: [i1.DefaultLayoutAlignDirective, i2.MatCheckbox], styles: ["", ""] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(CheckboxComponent, [{
        type: Component,
        args: [{
                selector: 'app-checkbox',
                templateUrl: './checkbox.component.html',
                styleUrls: ['./checkbox.component.css'],
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        multi: true,
                        useExisting: forwardRef(() => CheckboxComponent),
                    },
                ],
            }]
    }], function () { return []; }, { checkbox: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2hlY2tib3guY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXktYW5ndWxhci1mb3JtLyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC91aS9jaGVja2JveC9jaGVja2JveC5jb21wb25lbnQudHMiLCJsaWIvc2hhcmVkL3VpL2NoZWNrYm94L2NoZWNrYm94LmNvbXBvbmVudC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQXFCLEtBQUssRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDaEYsT0FBTyxFQUFFLGlCQUFpQixFQUF3QixNQUFNLGdCQUFnQixDQUFDOzs7O0FBaUJ6RSxNQUFNLE9BQU8saUJBQWlCO0lBTzVCO1FBSE8sYUFBUSxHQUFRLEdBQUcsRUFBRSxHQUFFLENBQUMsQ0FBQztRQUN6QixjQUFTLEdBQVEsR0FBRyxFQUFFLEdBQUUsQ0FBQyxDQUFDO1FBSWpDLFlBQU8sR0FBRyxHQUFTLEVBQUU7WUFDbkIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxDQUFDLENBQUM7WUFDaEUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxDQUFDLENBQUM7WUFDL0MsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1lBRWpCLDhCQUE4QjtRQUNoQyxDQUFDLENBQUM7SUFSYSxDQUFDO0lBVWhCLGdCQUFnQixDQUFDLEVBQUU7UUFDakIsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7SUFDckIsQ0FBQztJQUVELGlCQUFpQixDQUFDLEVBQUU7UUFDbEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7SUFDdEIsQ0FBQztJQUVELFVBQVUsQ0FBQyxLQUFLO1FBQ2QsSUFBSSxLQUFLLEVBQUU7WUFDVCxJQUFJLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUN0QztJQUNILENBQUM7SUFFRCxRQUFRO1FBQ04sOEJBQThCO1FBQzlCLHlDQUF5QztJQUMzQyxDQUFDO0lBRUQsV0FBVztRQUNULDhDQUE4QztJQUNoRCxDQUFDOztrRkF0Q1UsaUJBQWlCO3NEQUFqQixpQkFBaUIsb0dBUmpCO1lBQ1Q7Z0JBQ0UsT0FBTyxFQUFFLGlCQUFpQjtnQkFDMUIsS0FBSyxFQUFFLElBQUk7Z0JBQ1gsV0FBVyxFQUFFLFVBQVUsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQzthQUNqRDtTQUNGO1FDWkgsa0NBRVE7UUFBQSx1Q0FLdUM7UUFKbkMsb0dBQVMsYUFBYyxJQUFDO1FBSVcsWUFBNkI7UUFBQSxpQkFBZTtRQUMzRixpQkFBVTs7UUFKRSxlQUF5QztRQUF6QyxzREFBeUMsa0RBQUEsd0NBQUEsa0NBQUE7UUFHTixlQUE2QjtRQUE3Qiw0Q0FBNkI7O2tERE8vRCxpQkFBaUI7Y0FaN0IsU0FBUztlQUFDO2dCQUNULFFBQVEsRUFBRSxjQUFjO2dCQUN4QixXQUFXLEVBQUUsMkJBQTJCO2dCQUN4QyxTQUFTLEVBQUUsQ0FBQywwQkFBMEIsQ0FBQztnQkFDdkMsU0FBUyxFQUFFO29CQUNUO3dCQUNFLE9BQU8sRUFBRSxpQkFBaUI7d0JBQzFCLEtBQUssRUFBRSxJQUFJO3dCQUNYLFdBQVcsRUFBRSxVQUFVLENBQUMsR0FBRyxFQUFFLENBQUMsaUJBQWlCLENBQUM7cUJBQ2pEO2lCQUNGO2FBQ0Y7O2tCQUdFLEtBQUsiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgT25EZXN0cm95LCBJbnB1dCwgZm9yd2FyZFJlZiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgTkdfVkFMVUVfQUNDRVNTT1IsIENvbnRyb2xWYWx1ZUFjY2Vzc29yIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuXG4vLyBtb2RlbFxuaW1wb3J0IHsgQ2hlY2tib3ggfSBmcm9tICcuLi8uLi9tb2RlbC9zdHJ1Y3R1cmFsL2NvbXBvc2l0ZS9mb3JtL2NoZWNrYm94L2NoZWNrYm94Lm1vZGVsJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnYXBwLWNoZWNrYm94JyxcbiAgdGVtcGxhdGVVcmw6ICcuL2NoZWNrYm94LmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vY2hlY2tib3guY29tcG9uZW50LmNzcyddLFxuICBwcm92aWRlcnM6IFtcbiAgICB7XG4gICAgICBwcm92aWRlOiBOR19WQUxVRV9BQ0NFU1NPUixcbiAgICAgIG11bHRpOiB0cnVlLFxuICAgICAgdXNlRXhpc3Rpbmc6IGZvcndhcmRSZWYoKCkgPT4gQ2hlY2tib3hDb21wb25lbnQpLFxuICAgIH0sXG4gIF0sXG59KVxuZXhwb3J0IGNsYXNzIENoZWNrYm94Q29tcG9uZW50XG4gIGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3ksIENvbnRyb2xWYWx1ZUFjY2Vzc29yIHtcbiAgQElucHV0KCkgY2hlY2tib3g6IENoZWNrYm94O1xuXG4gIHB1YmxpYyBvbkNoYW5nZTogYW55ID0gKCkgPT4ge307XG4gIHB1YmxpYyBvblRvdWNoZWQ6IGFueSA9ICgpID0+IHt9O1xuXG4gIGNvbnN0cnVjdG9yKCkge31cblxuICBvbkNsaWNrID0gKCk6IHZvaWQgPT4ge1xuICAgIHRoaXMuY2hlY2tib3guc2V0RGVmYXVsdFZhbHVlKCF0aGlzLmNoZWNrYm94LmdldERlZmF1bHRWYWx1ZSgpKTtcbiAgICB0aGlzLm9uQ2hhbmdlKHRoaXMuY2hlY2tib3guZ2V0RGVmYXVsdFZhbHVlKCkpO1xuICAgIHRoaXMub25Ub3VjaGVkKCk7XG5cbiAgICAvLyBjb25zb2xlLmxvZyh0aGlzLmNoZWNrYm94KTtcbiAgfTtcblxuICByZWdpc3Rlck9uQ2hhbmdlKGZuKSB7XG4gICAgdGhpcy5vbkNoYW5nZSA9IGZuO1xuICB9XG5cbiAgcmVnaXN0ZXJPblRvdWNoZWQoZm4pIHtcbiAgICB0aGlzLm9uVG91Y2hlZCA9IGZuO1xuICB9XG5cbiAgd3JpdGVWYWx1ZSh2YWx1ZSkge1xuICAgIGlmICh2YWx1ZSkge1xuICAgICAgdGhpcy5jaGVja2JveC5zZXREZWZhdWx0VmFsdWUodmFsdWUpO1xuICAgIH1cbiAgfVxuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICAgIC8vIGNvbnNvbGUubG9nKHRoaXMuY2hlY2tib3gpO1xuICAgIC8vIGNvbnNvbGUubG9nKCdDaGVja2JveENvbXBvbmVudCBpbml0Jyk7XG4gIH1cblxuICBuZ09uRGVzdHJveSgpOiB2b2lkIHtcbiAgICAvLyBjb25zb2xlLmxvZygnQ2hlY2tib3hDb21wb25lbnQgZGVzdHJveWVkJyk7XG4gIH1cbn1cbiIsIjxzdHlsZT5cblxuPC9zdHlsZT5cblxuPHNlY3Rpb25cbiAgICBmeExheW91dEFsaWduPVwiY2VudGVyIGNlbnRlclwiPlxuICAgICAgICA8bWF0LWNoZWNrYm94IFxuICAgICAgICAgICAgKGNsaWNrKT1cInRoaXMub25DbGljaygpXCJcbiAgICAgICAgICAgIFt2YWx1ZV09XCJ0aGlzLmNoZWNrYm94LmdldERlZmF1bHRWYWx1ZSgpXCJcbiAgICAgICAgICAgIFtsYWJlbFBvc2l0aW9uXT1cInRoaXMuY2hlY2tib3guZ2V0TGFiZWxQb3NpdGlvbigpXCJcbiAgICAgICAgICAgIFtkaXNhYmxlZF09XCJ0aGlzLmNoZWNrYm94LmdldERpc2FibGVkKClcIlxuICAgICAgICAgICAgW2NvbG9yXT1cInRoaXMuY2hlY2tib3guZ2V0Q29sb3IoKVwiPnt7IHRoaXMuY2hlY2tib3guZ2V0TmFtZSgpIH19PC9tYXQtY2hlY2tib3g+XG48L3NlY3Rpb24+Il19