import { Component, forwardRef, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import * as i0 from "@angular/core";
import * as i1 from "@angular/flex-layout/flex";
import * as i2 from "@angular/material/form-field";
import * as i3 from "ngx-material-timepicker";
import * as i4 from "@angular/material/input";
export class TimepickerComponent {
    constructor() {
        this.onChange = () => { };
        this.onTouched = () => { };
        this.onTimeChanged = (time) => {
            // console.log(time);
            this.timepicker.setDefaultValue(time + ':00');
            this.onChange(this.timepicker.getDefaultValue());
            this.onTouched();
        };
        this.onTimepickerClosed = () => {
            this.onChange(this.timepicker.getDefaultValue());
            this.onTouched();
        };
    }
    registerOnChange(fn) {
        this.onChange = fn;
    }
    registerOnTouched(fn) {
        this.onTouched = fn;
    }
    writeValue(value) {
        if (value) {
            this.timepicker.setDefaultValue(value);
        }
    }
    ngOnInit() {
        // console.log('TimepickerComponent init');
    }
    ngOnDestroy() {
        // console.log('TimepickerComponent destroyed');
    }
}
TimepickerComponent.ɵfac = function TimepickerComponent_Factory(t) { return new (t || TimepickerComponent)(); };
TimepickerComponent.ɵcmp = i0.ɵɵdefineComponent({ type: TimepickerComponent, selectors: [["app-timepicker"]], inputs: { timepicker: "timepicker" }, features: [i0.ɵɵProvidersFeature([
            {
                provide: NG_VALUE_ACCESSOR,
                multi: true,
                useExisting: forwardRef(() => TimepickerComponent),
            },
        ])], decls: 7, vars: 9, consts: [["fxLayoutAlign", "center center"], [3, "appearance"], [3, "buttonAlign", "disabled", "format", "min", "max", "defaultTime", "timeChanged", "closed"], ["matInput", "", "type", "timepicker", 3, "hidden"]], template: function TimepickerComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "section", 0);
        i0.ɵɵelementStart(1, "mat-form-field", 1);
        i0.ɵɵelementStart(2, "mat-label");
        i0.ɵɵtext(3);
        i0.ɵɵelementEnd();
        i0.ɵɵelement(4, "br");
        i0.ɵɵelementStart(5, "ngx-timepicker-field", 2);
        i0.ɵɵlistener("timeChanged", function TimepickerComponent_Template_ngx_timepicker_field_timeChanged_5_listener($event) { return ctx.onTimeChanged($event); })("closed", function TimepickerComponent_Template_ngx_timepicker_field_closed_5_listener() { return ctx.onTimepickerClosed(); });
        i0.ɵɵelementEnd();
        i0.ɵɵelement(6, "input", 3);
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("appearance", ctx.timepicker.getAppearance());
        i0.ɵɵadvance(2);
        i0.ɵɵtextInterpolate(ctx.timepicker.getLabelName());
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("buttonAlign", ctx.timepicker.getButtonAlign())("disabled", ctx.timepicker.getDisabled())("format", 24)("min", ctx.timepicker.getMin() ? ctx.timepicker.getMin() : undefined)("max", ctx.timepicker.getMax() ? ctx.timepicker.getMax() : undefined)("defaultTime", ctx.timepicker.getDefaultValue());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("hidden", true);
    } }, directives: [i1.DefaultLayoutAlignDirective, i2.MatFormField, i2.MatLabel, i3.NgxTimepickerFieldComponent, i4.MatInput], styles: ["", "mat-form-field[_ngcontent-%COMP%] {\n        min-width: 250px;\n    }"] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(TimepickerComponent, [{
        type: Component,
        args: [{
                selector: 'app-timepicker',
                templateUrl: './timepicker.component.html',
                styleUrls: ['./timepicker.component.css'],
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        multi: true,
                        useExisting: forwardRef(() => TimepickerComponent),
                    },
                ],
            }]
    }], function () { return []; }, { timepicker: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGltZXBpY2tlci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9teS1hbmd1bGFyLWZvcm0vIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL3VpL3RpbWVwaWNrZXIvdGltZXBpY2tlci5jb21wb25lbnQudHMiLCJsaWIvc2hhcmVkL3VpL3RpbWVwaWNrZXIvdGltZXBpY2tlci5jb21wb25lbnQuaHRtbCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFxQixVQUFVLEVBQUUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ2hGLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGdCQUFnQixDQUFDOzs7Ozs7QUFlbkQsTUFBTSxPQUFPLG1CQUFtQjtJQUs5QjtRQUhPLGFBQVEsR0FBUSxHQUFHLEVBQUUsR0FBRSxDQUFDLENBQUM7UUFDekIsY0FBUyxHQUFRLEdBQUcsRUFBRSxHQUFFLENBQUMsQ0FBQztRQUlqQyxrQkFBYSxHQUFHLENBQUMsSUFBWSxFQUFRLEVBQUU7WUFDckMscUJBQXFCO1lBQ3JCLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDLElBQUksR0FBRyxLQUFLLENBQUMsQ0FBQztZQUM5QyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxFQUFFLENBQUMsQ0FBQztZQUNqRCxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDbkIsQ0FBQyxDQUFDO1FBRUYsdUJBQWtCLEdBQUcsR0FBUyxFQUFFO1lBQzlCLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLEVBQUUsQ0FBQyxDQUFDO1lBQ2pELElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUNuQixDQUFDLENBQUM7SUFaYSxDQUFDO0lBY2hCLGdCQUFnQixDQUFDLEVBQUU7UUFDakIsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7SUFDckIsQ0FBQztJQUVELGlCQUFpQixDQUFDLEVBQUU7UUFDbEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7SUFDdEIsQ0FBQztJQUVELFVBQVUsQ0FBQyxLQUFLO1FBQ2QsSUFBSSxLQUFLLEVBQUU7WUFDVCxJQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUN4QztJQUNILENBQUM7SUFFRCxRQUFRO1FBQ04sMkNBQTJDO0lBQzdDLENBQUM7SUFFRCxXQUFXO1FBQ1QsZ0RBQWdEO0lBQ2xELENBQUM7O3NGQXZDVSxtQkFBbUI7d0RBQW5CLG1CQUFtQiwwR0FSbkI7WUFDVDtnQkFDRSxPQUFPLEVBQUUsaUJBQWlCO2dCQUMxQixLQUFLLEVBQUUsSUFBSTtnQkFDWCxXQUFXLEVBQUUsVUFBVSxDQUFDLEdBQUcsRUFBRSxDQUFDLG1CQUFtQixDQUFDO2FBQ25EO1NBQ0Y7UUNOSCxrQ0FHSTtRQUFBLHlDQUNJO1FBQUEsaUNBQVc7UUFBQSxZQUFrQztRQUFBLGlCQUFZO1FBQ3pELHFCQUVBO1FBS0EsK0NBUWdFO1FBRDVELGdJQUFlLHlCQUEwQixJQUFDLG1HQUNoQyx3QkFBeUIsSUFETztRQUNMLGlCQUF1QjtRQUVoRSwyQkFDSjtRQUFBLGlCQUFpQjtRQUVyQixpQkFBVTs7UUF0QlUsZUFBOEM7UUFBOUMsMkRBQThDO1FBQy9DLGVBQWtDO1FBQWxDLG1EQUFrQztRQVN6QyxlQUFnRDtRQUFoRCw2REFBZ0QsMENBQUEsY0FBQSxzRUFBQSxzRUFBQSxpREFBQTtRQVNsQixlQUFlO1FBQWYsNkJBQWU7O2tERGQ1QyxtQkFBbUI7Y0FaL0IsU0FBUztlQUFDO2dCQUNULFFBQVEsRUFBRSxnQkFBZ0I7Z0JBQzFCLFdBQVcsRUFBRSw2QkFBNkI7Z0JBQzFDLFNBQVMsRUFBRSxDQUFDLDRCQUE0QixDQUFDO2dCQUN6QyxTQUFTLEVBQUU7b0JBQ1Q7d0JBQ0UsT0FBTyxFQUFFLGlCQUFpQjt3QkFDMUIsS0FBSyxFQUFFLElBQUk7d0JBQ1gsV0FBVyxFQUFFLFVBQVUsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxtQkFBbUIsQ0FBQztxQkFDbkQ7aUJBQ0Y7YUFDRjs7a0JBRUUsS0FBSyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBPbkRlc3Ryb3ksIGZvcndhcmRSZWYsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBOR19WQUxVRV9BQ0NFU1NPUiB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7IFRpbWVwaWNrZXIgfSBmcm9tICcuLi8uLi9tb2RlbC9zdHJ1Y3R1cmFsL2NvbXBvc2l0ZS9mb3JtL3RpbWVwaWNrZXIvdGltZXBpY2tlci5tb2RlbCc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2FwcC10aW1lcGlja2VyJyxcbiAgdGVtcGxhdGVVcmw6ICcuL3RpbWVwaWNrZXIuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi90aW1lcGlja2VyLmNvbXBvbmVudC5jc3MnXSxcbiAgcHJvdmlkZXJzOiBbXG4gICAge1xuICAgICAgcHJvdmlkZTogTkdfVkFMVUVfQUNDRVNTT1IsXG4gICAgICBtdWx0aTogdHJ1ZSxcbiAgICAgIHVzZUV4aXN0aW5nOiBmb3J3YXJkUmVmKCgpID0+IFRpbWVwaWNrZXJDb21wb25lbnQpLFxuICAgIH0sXG4gIF0sXG59KVxuZXhwb3J0IGNsYXNzIFRpbWVwaWNrZXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XG4gIEBJbnB1dCgpIHRpbWVwaWNrZXI6IFRpbWVwaWNrZXI7XG4gIHB1YmxpYyBvbkNoYW5nZTogYW55ID0gKCkgPT4ge307XG4gIHB1YmxpYyBvblRvdWNoZWQ6IGFueSA9ICgpID0+IHt9O1xuXG4gIGNvbnN0cnVjdG9yKCkge31cblxuICBvblRpbWVDaGFuZ2VkID0gKHRpbWU6IHN0cmluZyk6IHZvaWQgPT4ge1xuICAgIC8vIGNvbnNvbGUubG9nKHRpbWUpO1xuICAgIHRoaXMudGltZXBpY2tlci5zZXREZWZhdWx0VmFsdWUodGltZSArICc6MDAnKTtcbiAgICB0aGlzLm9uQ2hhbmdlKHRoaXMudGltZXBpY2tlci5nZXREZWZhdWx0VmFsdWUoKSk7XG4gICAgdGhpcy5vblRvdWNoZWQoKTtcbiAgfTtcblxuICBvblRpbWVwaWNrZXJDbG9zZWQgPSAoKTogdm9pZCA9PiB7XG4gICAgdGhpcy5vbkNoYW5nZSh0aGlzLnRpbWVwaWNrZXIuZ2V0RGVmYXVsdFZhbHVlKCkpO1xuICAgIHRoaXMub25Ub3VjaGVkKCk7XG4gIH07XG5cbiAgcmVnaXN0ZXJPbkNoYW5nZShmbikge1xuICAgIHRoaXMub25DaGFuZ2UgPSBmbjtcbiAgfVxuXG4gIHJlZ2lzdGVyT25Ub3VjaGVkKGZuKSB7XG4gICAgdGhpcy5vblRvdWNoZWQgPSBmbjtcbiAgfVxuXG4gIHdyaXRlVmFsdWUodmFsdWUpIHtcbiAgICBpZiAodmFsdWUpIHtcbiAgICAgIHRoaXMudGltZXBpY2tlci5zZXREZWZhdWx0VmFsdWUodmFsdWUpO1xuICAgIH1cbiAgfVxuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICAgIC8vIGNvbnNvbGUubG9nKCdUaW1lcGlja2VyQ29tcG9uZW50IGluaXQnKTtcbiAgfVxuXG4gIG5nT25EZXN0cm95KCk6IHZvaWQge1xuICAgIC8vIGNvbnNvbGUubG9nKCdUaW1lcGlja2VyQ29tcG9uZW50IGRlc3Ryb3llZCcpO1xuICB9XG59XG4iLCI8c3R5bGU+XG5cbiAgICBtYXQtZm9ybS1maWVsZCB7XG4gICAgICAgIG1pbi13aWR0aDogMjUwcHg7XG4gICAgfVxuXG48L3N0eWxlPlxuXG48c2VjdGlvblxuICAgIGZ4TGF5b3V0QWxpZ249XCJjZW50ZXIgY2VudGVyXCI+XG5cbiAgICA8bWF0LWZvcm0tZmllbGQgW2FwcGVhcmFuY2VdPVwidGhpcy50aW1lcGlja2VyLmdldEFwcGVhcmFuY2UoKVwiPlxuICAgICAgICA8bWF0LWxhYmVsPnt7dGhpcy50aW1lcGlja2VyLmdldExhYmVsTmFtZSgpfX08L21hdC1sYWJlbD5cbiAgICAgICAgPGJyPlxuXG4gICAgICAgIDwhLS0gXG4gICAgICAgICAgICBodHRwczovL2FncmFub20uZ2l0aHViLmlvL25neC1tYXRlcmlhbC10aW1lcGlja2VyL1xuICAgICAgICAgICAgaHR0cHM6Ly93d3cubnBtanMuY29tL3BhY2thZ2Uvbmd4LW1hdGVyaWFsLXRpbWVwaWNrZXJcbiAgICAgICAgIC0tPlxuXG4gICAgICAgIDxuZ3gtdGltZXBpY2tlci1maWVsZCBcbiAgICAgICAgICAgIFtidXR0b25BbGlnbl09XCJ0aGlzLnRpbWVwaWNrZXIuZ2V0QnV0dG9uQWxpZ24oKVwiXG4gICAgICAgICAgICBbZGlzYWJsZWRdPVwidGhpcy50aW1lcGlja2VyLmdldERpc2FibGVkKClcIlxuICAgICAgICAgICAgW2Zvcm1hdF09XCIyNFwiXG4gICAgICAgICAgICBbbWluXT1cInRoaXMudGltZXBpY2tlci5nZXRNaW4oKSA/IHRoaXMudGltZXBpY2tlci5nZXRNaW4oKSA6IHVuZGVmaW5lZFwiXG4gICAgICAgICAgICBbbWF4XT1cInRoaXMudGltZXBpY2tlci5nZXRNYXgoKSA/IHRoaXMudGltZXBpY2tlci5nZXRNYXgoKSA6IHVuZGVmaW5lZFwiXG4gICAgICAgICAgICBbZGVmYXVsdFRpbWVdPVwidGhpcy50aW1lcGlja2VyLmdldERlZmF1bHRWYWx1ZSgpXCJcbiAgICAgICAgICAgICh0aW1lQ2hhbmdlZCk9XCJ0aGlzLm9uVGltZUNoYW5nZWQoJGV2ZW50KVwiXG4gICAgICAgICAgICAoY2xvc2VkKT1cInRoaXMub25UaW1lcGlja2VyQ2xvc2VkKClcIj48L25neC10aW1lcGlja2VyLWZpZWxkPlxuXG4gICAgICAgIDxpbnB1dCBtYXRJbnB1dCB0eXBlPVwidGltZXBpY2tlclwiIFtoaWRkZW5dPVwidHJ1ZVwiPlxuICAgIDwvbWF0LWZvcm0tZmllbGQ+XG5cbjwvc2VjdGlvbj4iXX0=