import { Component, ViewChild } from '@angular/core';
import { forkJoin } from 'rxjs';
import * as i0 from "@angular/core";
import * as i1 from "@angular/material/dialog";
import * as i2 from "../../service/upload.service";
import * as i3 from "@angular/flex-layout/flex";
import * as i4 from "@angular/material/button";
import * as i5 from "@angular/material/list";
import * as i6 from "@angular/common";
import * as i7 from "@angular/material/core";
import * as i8 from "@angular/material/progress-bar";
const _c0 = ["file"];
function UploadDialogComponent_mat_list_item_10_mat_progress_bar_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "mat-progress-bar", 12);
    i0.ɵɵpipe(1, "async");
} if (rf & 2) {
    const file_r3 = i0.ɵɵnextContext().$implicit;
    const ctx_r4 = i0.ɵɵnextContext();
    i0.ɵɵproperty("value", i0.ɵɵpipeBind1(1, 1, ctx_r4.progress[file_r3.name].progress));
} }
function UploadDialogComponent_mat_list_item_10_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-list-item");
    i0.ɵɵelementStart(1, "h4", 10);
    i0.ɵɵtext(2);
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(3, UploadDialogComponent_mat_list_item_10_mat_progress_bar_3_Template, 2, 3, "mat-progress-bar", 11);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const file_r3 = ctx.$implicit;
    const ctx_r1 = i0.ɵɵnextContext();
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(file_r3.name);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r1.progress);
} }
function UploadDialogComponent_button_12_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "button", 13);
    i0.ɵɵtext(1, "Cancel");
    i0.ɵɵelementEnd();
} }
export class UploadDialogComponent {
    constructor(dialogRef, uploadService) {
        this.dialogRef = dialogRef;
        this.uploadService = uploadService;
        this.files = new Set();
        this.canBeClosed = true;
        this.primaryButtonText = 'Upload';
        this.showCancelButton = true;
        this.uploading = false;
        this.uploadSuccessful = false;
    }
    onFilesAdded() {
        const files = this.file.nativeElement.files;
        for (let key in files) {
            if (!isNaN(parseInt(key))) {
                this.files.add(files[key]);
            }
        }
    }
    addFiles() {
        this.file.nativeElement.click();
    }
    closeDialog() {
        // if everything was uploaded already, just close the dialog
        if (this.uploadSuccessful) {
            return this.dialogRef.close();
        }
        // set the component state to "uploading"
        this.uploading = true;
        // start the upload and save the progress map
        this.progress = this.uploadService.upload(this.files);
        console.log(this.progress);
        for (const key in this.progress) {
            this.progress[key].progress.subscribe((val) => console.log(val));
        }
        // convert the progress map into an array
        let allProgressObservables = [];
        for (let key in this.progress) {
            allProgressObservables.push(this.progress[key].progress);
        }
        // Adjust the state variables
        // The OK-button should have the text "Finish" now
        this.primaryButtonText = 'Finish';
        // The dialog should not be closed while uploading
        this.canBeClosed = false;
        this.dialogRef.disableClose = true;
        // Hide the cancel-button
        this.showCancelButton = false;
        // When all progress-observables are completed...
        forkJoin(allProgressObservables).subscribe((end) => {
            // ... the dialog can be closed again...
            this.canBeClosed = true;
            this.dialogRef.disableClose = false;
            // ... the upload was successful...
            this.uploadSuccessful = true;
            // ... and the component is no longer uploading
            this.uploading = false;
        });
    }
    ngOnInit() {
        console.log('UploadDialogComponent init');
    }
    ngOnDestroy() {
        console.log('UploadDialogComponent destroyed');
    }
}
UploadDialogComponent.ɵfac = function UploadDialogComponent_Factory(t) { return new (t || UploadDialogComponent)(i0.ɵɵdirectiveInject(i1.MatDialogRef), i0.ɵɵdirectiveInject(i2.UploadService)); };
UploadDialogComponent.ɵcmp = i0.ɵɵdefineComponent({ type: UploadDialogComponent, selectors: [["app-upload-dialog"]], viewQuery: function UploadDialogComponent_Query(rf, ctx) { if (rf & 1) {
        i0.ɵɵviewQuery(_c0, true);
    } if (rf & 2) {
        var _t;
        i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.file = _t.first);
    } }, decls: 15, vars: 5, consts: [["type", "file", "multiple", "", 2, "display", "none", 3, "change"], ["file", ""], ["fxLayout", "column", "fxLayoutAlign", "space-evenly stretch", 1, "container"], ["mat-dialog-title", ""], ["mat-raised-button", "", "color", "primary", 1, "add-files-btn", 3, "disabled", "click"], ["fxFlex", ""], [4, "ngFor", "ngForOf"], [1, "actions"], ["mat-button", "", "mat-dialog-close", "", 4, "ngIf"], ["mat-raised-button", "", "color", "primary", 3, "disabled", "click"], ["mat-line", ""], ["mode", "determinate", 3, "value", 4, "ngIf"], ["mode", "determinate", 3, "value"], ["mat-button", "", "mat-dialog-close", ""]], template: function UploadDialogComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "input", 0, 1);
        i0.ɵɵlistener("change", function UploadDialogComponent_Template_input_change_0_listener() { return ctx.onFilesAdded(); });
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(2, "div", 2);
        i0.ɵɵelementStart(3, "h1", 3);
        i0.ɵɵtext(4, "Upload Files");
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(5, "div");
        i0.ɵɵelementStart(6, "button", 4);
        i0.ɵɵlistener("click", function UploadDialogComponent_Template_button_click_6_listener() { return ctx.addFiles(); });
        i0.ɵɵtext(7, " Add Files ");
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(8, "mat-dialog-content", 5);
        i0.ɵɵelementStart(9, "mat-list");
        i0.ɵɵtemplate(10, UploadDialogComponent_mat_list_item_10_Template, 4, 2, "mat-list-item", 6);
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(11, "mat-dialog-actions", 7);
        i0.ɵɵtemplate(12, UploadDialogComponent_button_12_Template, 2, 0, "button", 8);
        i0.ɵɵelementStart(13, "button", 9);
        i0.ɵɵlistener("click", function UploadDialogComponent_Template_button_click_13_listener() { return ctx.closeDialog(); });
        i0.ɵɵtext(14);
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵadvance(6);
        i0.ɵɵproperty("disabled", ctx.uploading || ctx.uploadSuccessful);
        i0.ɵɵadvance(4);
        i0.ɵɵproperty("ngForOf", ctx.files);
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("ngIf", ctx.showCancelButton);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("disabled", !ctx.canBeClosed);
        i0.ɵɵadvance(1);
        i0.ɵɵtextInterpolate(ctx.primaryButtonText);
    } }, directives: [i3.DefaultLayoutDirective, i3.DefaultLayoutAlignDirective, i1.MatDialogTitle, i4.MatButton, i1.MatDialogContent, i3.DefaultFlexDirective, i5.MatList, i6.NgForOf, i1.MatDialogActions, i6.NgIf, i5.MatListItem, i7.MatLine, i8.MatProgressBar, i1.MatDialogClose], pipes: [i6.AsyncPipe], styles: ["", ".add-files-btn[_ngcontent-%COMP%] {\n  float: right;\n}\n\n[_nghost-%COMP%] {\n  height: 100%;\n  display: flex;\n  flex: 1;\n  flex-direction: column;\n}\n\n.actions[_ngcontent-%COMP%] {\n  justify-content: flex-end;\n}\n\n.container[_ngcontent-%COMP%] {\n  height: 100%;\n}"] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(UploadDialogComponent, [{
        type: Component,
        args: [{
                selector: 'app-upload-dialog',
                templateUrl: './upload-dialog.component.html',
                styleUrls: ['./upload-dialog.component.css'],
            }]
    }], function () { return [{ type: i1.MatDialogRef }, { type: i2.UploadService }]; }, { file: [{
            type: ViewChild,
            args: ['file', { static: false }]
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXBsb2FkLWRpYWxvZy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9teS1hbmd1bGFyLWZvcm0vIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL3VpL3VwbG9hZC1kaWFsb2cvdXBsb2FkLWRpYWxvZy5jb21wb25lbnQudHMiLCJsaWIvc2hhcmVkL3VpL3VwbG9hZC1kaWFsb2cvdXBsb2FkLWRpYWxvZy5jb21wb25lbnQuaHRtbCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFxQixTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFHeEUsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLE1BQU0sQ0FBQzs7Ozs7Ozs7Ozs7O0lDb0N4Qix1Q0FBd0g7Ozs7O0lBQWxFLG9GQUE4Qzs7O0lBSnRHLHFDQUVFO0lBQUEsOEJBQWE7SUFBQSxZQUFhO0lBQUEsaUJBQUs7SUFFL0Isa0hBQXFHO0lBRXZHLGlCQUFnQjs7OztJQUpELGVBQWE7SUFBYixrQ0FBYTtJQUVSLGVBQWdCO0lBQWhCLHNDQUFnQjs7O0lBUXRDLGtDQUE2RDtJQUFBLHNCQUFNO0lBQUEsaUJBQVM7O0FEckNoRixNQUFNLE9BQU8scUJBQXFCO0lBS2hDLFlBQ1MsU0FBOEMsRUFDOUMsYUFBNEI7UUFENUIsY0FBUyxHQUFULFNBQVMsQ0FBcUM7UUFDOUMsa0JBQWEsR0FBYixhQUFhLENBQWU7UUFKOUIsVUFBSyxHQUFjLElBQUksR0FBRyxFQUFFLENBQUM7UUFRcEMsZ0JBQVcsR0FBRyxJQUFJLENBQUM7UUFDbkIsc0JBQWlCLEdBQUcsUUFBUSxDQUFDO1FBQzdCLHFCQUFnQixHQUFHLElBQUksQ0FBQztRQUN4QixjQUFTLEdBQUcsS0FBSyxDQUFDO1FBQ2xCLHFCQUFnQixHQUFHLEtBQUssQ0FBQztJQVB0QixDQUFDO0lBU0osWUFBWTtRQUNWLE1BQU0sS0FBSyxHQUE0QixJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUM7UUFDckUsS0FBSyxJQUFJLEdBQUcsSUFBSSxLQUFLLEVBQUU7WUFDckIsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRTtnQkFDekIsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7YUFDNUI7U0FDRjtJQUNILENBQUM7SUFFRCxRQUFRO1FBQ04sSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDbEMsQ0FBQztJQUVELFdBQVc7UUFDVCw0REFBNEQ7UUFDNUQsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLEVBQUU7WUFDekIsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxDQUFDO1NBQy9CO1FBRUQseUNBQXlDO1FBQ3pDLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBRXRCLDZDQUE2QztRQUM3QyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN0RCxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUMzQixLQUFLLE1BQU0sR0FBRyxJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDL0IsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7U0FDbEU7UUFFRCx5Q0FBeUM7UUFDekMsSUFBSSxzQkFBc0IsR0FBRyxFQUFFLENBQUM7UUFDaEMsS0FBSyxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQzdCLHNCQUFzQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1NBQzFEO1FBRUQsNkJBQTZCO1FBRTdCLGtEQUFrRDtRQUNsRCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsUUFBUSxDQUFDO1FBRWxDLGtEQUFrRDtRQUNsRCxJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztRQUN6QixJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7UUFFbkMseUJBQXlCO1FBQ3pCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxLQUFLLENBQUM7UUFFOUIsaURBQWlEO1FBQ2pELFFBQVEsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxFQUFFO1lBQ2pELHdDQUF3QztZQUN4QyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztZQUN4QixJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7WUFFcEMsbUNBQW1DO1lBQ25DLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7WUFFN0IsK0NBQStDO1lBQy9DLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO1FBQ3pCLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELFFBQVE7UUFDTixPQUFPLENBQUMsR0FBRyxDQUFDLDRCQUE0QixDQUFDLENBQUM7SUFDNUMsQ0FBQztJQUVELFdBQVc7UUFDVCxPQUFPLENBQUMsR0FBRyxDQUFDLGlDQUFpQyxDQUFDLENBQUM7SUFDakQsQ0FBQzs7MEZBcEZVLHFCQUFxQjswREFBckIscUJBQXFCOzs7Ozs7UUNhbEMsbUNBQ0E7UUFEK0MsbUdBQVUsa0JBQWMsSUFBQztRQUF4RSxpQkFDQTtRQUFBLDhCQUNFO1FBQUEsNkJBQXFCO1FBQUEsNEJBQVk7UUFBQSxpQkFBSztRQUN0QywyQkFDRTtRQUFBLGlDQUNFO1FBRHlHLGtHQUFTLGNBQVUsSUFBQztRQUM3SCwyQkFDRjtRQUFBLGlCQUFTO1FBQ1gsaUJBQU07UUFHTiw2Q0FDRTtRQUFBLGdDQUNFO1FBQUEsNEZBRUU7UUFLSixpQkFBVztRQUNiLGlCQUFxQjtRQUdyQiw4Q0FDRTtRQUFBLDhFQUE2RDtRQUM3RCxrQ0FBNEY7UUFBeEIsbUdBQVMsaUJBQWEsSUFBQztRQUFDLGFBQXFCO1FBQUEsaUJBQVM7UUFDNUgsaUJBQXFCO1FBQ3ZCLGlCQUFNOztRQXZCTSxlQUEwQztRQUExQyxnRUFBMEM7UUFRakMsZUFBMEI7UUFBMUIsbUNBQTBCO1FBWW5DLGVBQXdCO1FBQXhCLDJDQUF3QjtRQUNVLGVBQXlCO1FBQXpCLDJDQUF5QjtRQUF5QixlQUFxQjtRQUFyQiwyQ0FBcUI7O2tERHRDeEcscUJBQXFCO2NBTGpDLFNBQVM7ZUFBQztnQkFDVCxRQUFRLEVBQUUsbUJBQW1CO2dCQUM3QixXQUFXLEVBQUUsZ0NBQWdDO2dCQUM3QyxTQUFTLEVBQUUsQ0FBQywrQkFBK0IsQ0FBQzthQUM3Qzs7a0JBRUUsU0FBUzttQkFBQyxNQUFNLEVBQUUsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIE9uRGVzdHJveSwgVmlld0NoaWxkIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBNYXREaWFsb2dSZWYgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9kaWFsb2cnO1xuaW1wb3J0IHsgVXBsb2FkU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2UvdXBsb2FkLnNlcnZpY2UnO1xuaW1wb3J0IHsgZm9ya0pvaW4gfSBmcm9tICdyeGpzJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnYXBwLXVwbG9hZC1kaWFsb2cnLFxuICB0ZW1wbGF0ZVVybDogJy4vdXBsb2FkLWRpYWxvZy5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL3VwbG9hZC1kaWFsb2cuY29tcG9uZW50LmNzcyddLFxufSlcbmV4cG9ydCBjbGFzcyBVcGxvYWREaWFsb2dDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XG4gIEBWaWV3Q2hpbGQoJ2ZpbGUnLCB7IHN0YXRpYzogZmFsc2UgfSkgZmlsZTtcblxuICBwdWJsaWMgZmlsZXM6IFNldDxGaWxlPiA9IG5ldyBTZXQoKTtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwdWJsaWMgZGlhbG9nUmVmOiBNYXREaWFsb2dSZWY8VXBsb2FkRGlhbG9nQ29tcG9uZW50PixcbiAgICBwdWJsaWMgdXBsb2FkU2VydmljZTogVXBsb2FkU2VydmljZVxuICApIHt9XG5cbiAgcHJvZ3Jlc3M7XG4gIGNhbkJlQ2xvc2VkID0gdHJ1ZTtcbiAgcHJpbWFyeUJ1dHRvblRleHQgPSAnVXBsb2FkJztcbiAgc2hvd0NhbmNlbEJ1dHRvbiA9IHRydWU7XG4gIHVwbG9hZGluZyA9IGZhbHNlO1xuICB1cGxvYWRTdWNjZXNzZnVsID0gZmFsc2U7XG5cbiAgb25GaWxlc0FkZGVkKCkge1xuICAgIGNvbnN0IGZpbGVzOiB7IFtrZXk6IHN0cmluZ106IEZpbGUgfSA9IHRoaXMuZmlsZS5uYXRpdmVFbGVtZW50LmZpbGVzO1xuICAgIGZvciAobGV0IGtleSBpbiBmaWxlcykge1xuICAgICAgaWYgKCFpc05hTihwYXJzZUludChrZXkpKSkge1xuICAgICAgICB0aGlzLmZpbGVzLmFkZChmaWxlc1trZXldKTtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBhZGRGaWxlcygpIHtcbiAgICB0aGlzLmZpbGUubmF0aXZlRWxlbWVudC5jbGljaygpO1xuICB9XG5cbiAgY2xvc2VEaWFsb2coKSB7XG4gICAgLy8gaWYgZXZlcnl0aGluZyB3YXMgdXBsb2FkZWQgYWxyZWFkeSwganVzdCBjbG9zZSB0aGUgZGlhbG9nXG4gICAgaWYgKHRoaXMudXBsb2FkU3VjY2Vzc2Z1bCkge1xuICAgICAgcmV0dXJuIHRoaXMuZGlhbG9nUmVmLmNsb3NlKCk7XG4gICAgfVxuXG4gICAgLy8gc2V0IHRoZSBjb21wb25lbnQgc3RhdGUgdG8gXCJ1cGxvYWRpbmdcIlxuICAgIHRoaXMudXBsb2FkaW5nID0gdHJ1ZTtcblxuICAgIC8vIHN0YXJ0IHRoZSB1cGxvYWQgYW5kIHNhdmUgdGhlIHByb2dyZXNzIG1hcFxuICAgIHRoaXMucHJvZ3Jlc3MgPSB0aGlzLnVwbG9hZFNlcnZpY2UudXBsb2FkKHRoaXMuZmlsZXMpO1xuICAgIGNvbnNvbGUubG9nKHRoaXMucHJvZ3Jlc3MpO1xuICAgIGZvciAoY29uc3Qga2V5IGluIHRoaXMucHJvZ3Jlc3MpIHtcbiAgICAgIHRoaXMucHJvZ3Jlc3Nba2V5XS5wcm9ncmVzcy5zdWJzY3JpYmUoKHZhbCkgPT4gY29uc29sZS5sb2codmFsKSk7XG4gICAgfVxuXG4gICAgLy8gY29udmVydCB0aGUgcHJvZ3Jlc3MgbWFwIGludG8gYW4gYXJyYXlcbiAgICBsZXQgYWxsUHJvZ3Jlc3NPYnNlcnZhYmxlcyA9IFtdO1xuICAgIGZvciAobGV0IGtleSBpbiB0aGlzLnByb2dyZXNzKSB7XG4gICAgICBhbGxQcm9ncmVzc09ic2VydmFibGVzLnB1c2godGhpcy5wcm9ncmVzc1trZXldLnByb2dyZXNzKTtcbiAgICB9XG5cbiAgICAvLyBBZGp1c3QgdGhlIHN0YXRlIHZhcmlhYmxlc1xuXG4gICAgLy8gVGhlIE9LLWJ1dHRvbiBzaG91bGQgaGF2ZSB0aGUgdGV4dCBcIkZpbmlzaFwiIG5vd1xuICAgIHRoaXMucHJpbWFyeUJ1dHRvblRleHQgPSAnRmluaXNoJztcblxuICAgIC8vIFRoZSBkaWFsb2cgc2hvdWxkIG5vdCBiZSBjbG9zZWQgd2hpbGUgdXBsb2FkaW5nXG4gICAgdGhpcy5jYW5CZUNsb3NlZCA9IGZhbHNlO1xuICAgIHRoaXMuZGlhbG9nUmVmLmRpc2FibGVDbG9zZSA9IHRydWU7XG5cbiAgICAvLyBIaWRlIHRoZSBjYW5jZWwtYnV0dG9uXG4gICAgdGhpcy5zaG93Q2FuY2VsQnV0dG9uID0gZmFsc2U7XG5cbiAgICAvLyBXaGVuIGFsbCBwcm9ncmVzcy1vYnNlcnZhYmxlcyBhcmUgY29tcGxldGVkLi4uXG4gICAgZm9ya0pvaW4oYWxsUHJvZ3Jlc3NPYnNlcnZhYmxlcykuc3Vic2NyaWJlKChlbmQpID0+IHtcbiAgICAgIC8vIC4uLiB0aGUgZGlhbG9nIGNhbiBiZSBjbG9zZWQgYWdhaW4uLi5cbiAgICAgIHRoaXMuY2FuQmVDbG9zZWQgPSB0cnVlO1xuICAgICAgdGhpcy5kaWFsb2dSZWYuZGlzYWJsZUNsb3NlID0gZmFsc2U7XG5cbiAgICAgIC8vIC4uLiB0aGUgdXBsb2FkIHdhcyBzdWNjZXNzZnVsLi4uXG4gICAgICB0aGlzLnVwbG9hZFN1Y2Nlc3NmdWwgPSB0cnVlO1xuXG4gICAgICAvLyAuLi4gYW5kIHRoZSBjb21wb25lbnQgaXMgbm8gbG9uZ2VyIHVwbG9hZGluZ1xuICAgICAgdGhpcy51cGxvYWRpbmcgPSBmYWxzZTtcbiAgICB9KTtcbiAgfVxuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICAgIGNvbnNvbGUubG9nKCdVcGxvYWREaWFsb2dDb21wb25lbnQgaW5pdCcpO1xuICB9XG5cbiAgbmdPbkRlc3Ryb3koKTogdm9pZCB7XG4gICAgY29uc29sZS5sb2coJ1VwbG9hZERpYWxvZ0NvbXBvbmVudCBkZXN0cm95ZWQnKTtcbiAgfVxufVxuIiwiPHN0eWxlPlxuICAgIFxuICAgIC5hZGQtZmlsZXMtYnRuIHtcbiAgZmxvYXQ6IHJpZ2h0O1xufVxuXG46aG9zdCB7XG4gIGhlaWdodDogMTAwJTtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleDogMTtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbn1cblxuLmFjdGlvbnMge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xufVxuXG4uY29udGFpbmVyIHtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuXG48L3N0eWxlPlxuXG48aW5wdXQgdHlwZT1cImZpbGVcIiAjZmlsZSBzdHlsZT1cImRpc3BsYXk6IG5vbmVcIiAoY2hhbmdlKT1cIm9uRmlsZXNBZGRlZCgpXCIgbXVsdGlwbGUgLz5cbjxkaXYgY2xhc3M9XCJjb250YWluZXJcIiBmeExheW91dD1cImNvbHVtblwiIGZ4TGF5b3V0QWxpZ249XCJzcGFjZS1ldmVubHkgc3RyZXRjaFwiPlxuICA8aDEgbWF0LWRpYWxvZy10aXRsZT5VcGxvYWQgRmlsZXM8L2gxPlxuICA8ZGl2PlxuICAgIDxidXR0b24gW2Rpc2FibGVkXT1cInVwbG9hZGluZyB8fCB1cGxvYWRTdWNjZXNzZnVsXCIgbWF0LXJhaXNlZC1idXR0b24gY29sb3I9XCJwcmltYXJ5XCIgY2xhc3M9XCJhZGQtZmlsZXMtYnRuXCIgKGNsaWNrKT1cImFkZEZpbGVzKClcIj5cbiAgICAgIEFkZCBGaWxlc1xuICAgIDwvYnV0dG9uPlxuICA8L2Rpdj5cblxuICA8IS0tIFRoaXMgaXMgdGhlIGNvbnRlbnQgb2YgdGhlIGRpYWxvZywgY29udGFpbmluZyBhIGxpc3Qgb2YgdGhlIGZpbGVzIHRvIHVwbG9hZCAtLT5cbiAgPG1hdC1kaWFsb2ctY29udGVudCBmeEZsZXg+XG4gICAgPG1hdC1saXN0PlxuICAgICAgPG1hdC1saXN0LWl0ZW0gKm5nRm9yPVwibGV0IGZpbGUgb2YgZmlsZXNcIj5cblxuICAgICAgICA8aDQgbWF0LWxpbmU+e3tmaWxlLm5hbWV9fTwvaDQ+XG5cbiAgICAgICAgPG1hdC1wcm9ncmVzcy1iYXIgKm5nSWY9XCJwcm9ncmVzc1wiIG1vZGU9XCJkZXRlcm1pbmF0ZVwiIFt2YWx1ZV09XCJwcm9ncmVzc1tmaWxlLm5hbWVdLnByb2dyZXNzIHwgYXN5bmNcIj48L21hdC1wcm9ncmVzcy1iYXI+XG5cbiAgICAgIDwvbWF0LWxpc3QtaXRlbT5cbiAgICA8L21hdC1saXN0PlxuICA8L21hdC1kaWFsb2ctY29udGVudD5cblxuICA8IS0tIFRoaXMgYXJlIHRoZSBhY3Rpb25zIG9mIHRoZSBkaWFsb2csIGNvbnRhaW5pbmcgdGhlIHByaW1hcnkgYW5kIHRoZSBjYW5jZWwgYnV0dG9uLS0+XG4gIDxtYXQtZGlhbG9nLWFjdGlvbnMgY2xhc3M9XCJhY3Rpb25zXCI+XG4gICAgPGJ1dHRvbiAqbmdJZj1cInNob3dDYW5jZWxCdXR0b25cIiBtYXQtYnV0dG9uIG1hdC1kaWFsb2ctY2xvc2U+Q2FuY2VsPC9idXR0b24+XG4gICAgPGJ1dHRvbiBtYXQtcmFpc2VkLWJ1dHRvbiBjb2xvcj1cInByaW1hcnlcIiBbZGlzYWJsZWRdPVwiIWNhbkJlQ2xvc2VkXCIgKGNsaWNrKT1cImNsb3NlRGlhbG9nKClcIj57e3ByaW1hcnlCdXR0b25UZXh0fX08L2J1dHRvbj5cbiAgPC9tYXQtZGlhbG9nLWFjdGlvbnM+XG48L2Rpdj4iXX0=