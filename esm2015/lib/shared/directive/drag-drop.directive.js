import { Directive, Output, EventEmitter, HostBinding, HostListener, } from '@angular/core';
import * as i0 from "@angular/core";
// https://medium.com/@mariemchabeni/angular-7-drag-and-drop-simple-file-uploadin-in-less-than-5-minutes-d57eb010c0dc
export class DragDropDirective {
    constructor() {
        this.onFileDropped = new EventEmitter();
        this.opacity = '1';
    }
    //Dragover listener
    onDragOver(evt) {
        evt.preventDefault();
        evt.stopPropagation();
        this.opacity = '0.5';
    }
    //Dragleave listener
    onDragLeave(evt) {
        evt.preventDefault();
        evt.stopPropagation();
        this.opacity = '1';
    }
    //Drop listener
    onDrop(evt) {
        evt.preventDefault();
        evt.stopPropagation();
        this.opacity = '1';
        let files = evt.dataTransfer.files;
        if (files.length > 0) {
            this.onFileDropped.emit(files);
        }
    }
}
DragDropDirective.ɵfac = function DragDropDirective_Factory(t) { return new (t || DragDropDirective)(); };
DragDropDirective.ɵdir = i0.ɵɵdefineDirective({ type: DragDropDirective, selectors: [["", "appDragDrop", ""]], hostVars: 2, hostBindings: function DragDropDirective_HostBindings(rf, ctx) { if (rf & 1) {
        i0.ɵɵlistener("dragover", function DragDropDirective_dragover_HostBindingHandler($event) { return ctx.onDragOver($event); })("dragleave", function DragDropDirective_dragleave_HostBindingHandler($event) { return ctx.onDragLeave($event); })("drop", function DragDropDirective_drop_HostBindingHandler($event) { return ctx.onDrop($event); });
    } if (rf & 2) {
        i0.ɵɵstyleProp("opacity", ctx.opacity);
    } }, outputs: { onFileDropped: "onFileDropped" } });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(DragDropDirective, [{
        type: Directive,
        args: [{
                selector: '[appDragDrop]',
            }]
    }], null, { onFileDropped: [{
            type: Output
        }], opacity: [{
            type: HostBinding,
            args: ['style.opacity']
        }], onDragOver: [{
            type: HostListener,
            args: ['dragover', ['$event']]
        }], onDragLeave: [{
            type: HostListener,
            args: ['dragleave', ['$event']]
        }], onDrop: [{
            type: HostListener,
            args: ['drop', ['$event']]
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJhZy1kcm9wLmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215LWFuZ3VsYXItZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvZGlyZWN0aXZlL2RyYWctZHJvcC5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUNMLFNBQVMsRUFDVCxNQUFNLEVBRU4sWUFBWSxFQUNaLFdBQVcsRUFDWCxZQUFZLEdBRWIsTUFBTSxlQUFlLENBQUM7O0FBRXZCLHFIQUFxSDtBQUlySCxNQUFNLE9BQU8saUJBQWlCO0lBSDlCO1FBSVksa0JBQWEsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBRVosWUFBTyxHQUFHLEdBQUcsQ0FBQztLQTBCckQ7SUF4QkMsbUJBQW1CO0lBQ21CLFVBQVUsQ0FBQyxHQUFHO1FBQ2xELEdBQUcsQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUNyQixHQUFHLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDdEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7SUFDdkIsQ0FBQztJQUVELG9CQUFvQjtJQUMwQixXQUFXLENBQUMsR0FBRztRQUMzRCxHQUFHLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDckIsR0FBRyxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBQ3RCLElBQUksQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDO0lBQ3JCLENBQUM7SUFFRCxlQUFlO0lBQzBCLE1BQU0sQ0FBQyxHQUFHO1FBQ2pELEdBQUcsQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUNyQixHQUFHLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDdEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUM7UUFDbkIsSUFBSSxLQUFLLEdBQUcsR0FBRyxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUM7UUFDbkMsSUFBSSxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUNwQixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUNoQztJQUNILENBQUM7O2tGQTVCVSxpQkFBaUI7c0RBQWpCLGlCQUFpQjswR0FBakIsc0JBQWtCLDJGQUFsQix1QkFBbUIsaUZBQW5CLGtCQUFjOzs7O2tEQUFkLGlCQUFpQjtjQUg3QixTQUFTO2VBQUM7Z0JBQ1QsUUFBUSxFQUFFLGVBQWU7YUFDMUI7O2tCQUVFLE1BQU07O2tCQUVOLFdBQVc7bUJBQUMsZUFBZTs7a0JBRzNCLFlBQVk7bUJBQUMsVUFBVSxFQUFFLENBQUMsUUFBUSxDQUFDOztrQkFPbkMsWUFBWTttQkFBQyxXQUFXLEVBQUUsQ0FBQyxRQUFRLENBQUM7O2tCQU9wQyxZQUFZO21CQUFDLE1BQU0sRUFBRSxDQUFDLFFBQVEsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XHJcbiAgRGlyZWN0aXZlLFxyXG4gIE91dHB1dCxcclxuICBJbnB1dCxcclxuICBFdmVudEVtaXR0ZXIsXHJcbiAgSG9zdEJpbmRpbmcsXHJcbiAgSG9zdExpc3RlbmVyLFxyXG4gIEFmdGVyVmlld0luaXQsXHJcbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG4vLyBodHRwczovL21lZGl1bS5jb20vQG1hcmllbWNoYWJlbmkvYW5ndWxhci03LWRyYWctYW5kLWRyb3Atc2ltcGxlLWZpbGUtdXBsb2FkaW4taW4tbGVzcy10aGFuLTUtbWludXRlcy1kNTdlYjAxMGMwZGNcclxuQERpcmVjdGl2ZSh7XHJcbiAgc2VsZWN0b3I6ICdbYXBwRHJhZ0Ryb3BdJyxcclxufSlcclxuZXhwb3J0IGNsYXNzIERyYWdEcm9wRGlyZWN0aXZlIHtcclxuICBAT3V0cHV0KCkgb25GaWxlRHJvcHBlZCA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG5cclxuICBASG9zdEJpbmRpbmcoJ3N0eWxlLm9wYWNpdHknKSBwcml2YXRlIG9wYWNpdHkgPSAnMSc7XHJcblxyXG4gIC8vRHJhZ292ZXIgbGlzdGVuZXJcclxuICBASG9zdExpc3RlbmVyKCdkcmFnb3ZlcicsIFsnJGV2ZW50J10pIG9uRHJhZ092ZXIoZXZ0KSB7XHJcbiAgICBldnQucHJldmVudERlZmF1bHQoKTtcclxuICAgIGV2dC5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgIHRoaXMub3BhY2l0eSA9ICcwLjUnO1xyXG4gIH1cclxuXHJcbiAgLy9EcmFnbGVhdmUgbGlzdGVuZXJcclxuICBASG9zdExpc3RlbmVyKCdkcmFnbGVhdmUnLCBbJyRldmVudCddKSBwdWJsaWMgb25EcmFnTGVhdmUoZXZ0KSB7XHJcbiAgICBldnQucHJldmVudERlZmF1bHQoKTtcclxuICAgIGV2dC5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgIHRoaXMub3BhY2l0eSA9ICcxJztcclxuICB9XHJcblxyXG4gIC8vRHJvcCBsaXN0ZW5lclxyXG4gIEBIb3N0TGlzdGVuZXIoJ2Ryb3AnLCBbJyRldmVudCddKSBwdWJsaWMgb25Ecm9wKGV2dCkge1xyXG4gICAgZXZ0LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICBldnQuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICB0aGlzLm9wYWNpdHkgPSAnMSc7XHJcbiAgICBsZXQgZmlsZXMgPSBldnQuZGF0YVRyYW5zZmVyLmZpbGVzO1xyXG4gICAgaWYgKGZpbGVzLmxlbmd0aCA+IDApIHtcclxuICAgICAgdGhpcy5vbkZpbGVEcm9wcGVkLmVtaXQoZmlsZXMpO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG4iXX0=