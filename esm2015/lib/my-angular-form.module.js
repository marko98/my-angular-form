import { NgModule } from "@angular/core";
import { MyAngularFormComponent } from "./my-angular-form.component";
import { SharedModule } from "./shared/shared.module";
import * as i0 from "@angular/core";
export class MyAngularFormModule {
}
MyAngularFormModule.ɵmod = i0.ɵɵdefineNgModule({ type: MyAngularFormModule });
MyAngularFormModule.ɵinj = i0.ɵɵdefineInjector({ factory: function MyAngularFormModule_Factory(t) { return new (t || MyAngularFormModule)(); }, imports: [[SharedModule]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(MyAngularFormModule, { declarations: [MyAngularFormComponent], imports: [SharedModule], exports: [MyAngularFormComponent] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(MyAngularFormModule, [{
        type: NgModule,
        args: [{
                declarations: [MyAngularFormComponent],
                imports: [SharedModule],
                exports: [MyAngularFormComponent],
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXktYW5ndWxhci1mb3JtLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215LWFuZ3VsYXItZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9teS1hbmd1bGFyLWZvcm0ubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFDckUsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHdCQUF3QixDQUFDOztBQU90RCxNQUFNLE9BQU8sbUJBQW1COzt1REFBbkIsbUJBQW1CO3FIQUFuQixtQkFBbUIsa0JBSHJCLENBQUMsWUFBWSxDQUFDO3dGQUdaLG1CQUFtQixtQkFKZixzQkFBc0IsYUFDM0IsWUFBWSxhQUNaLHNCQUFzQjtrREFFckIsbUJBQW1CO2NBTC9CLFFBQVE7ZUFBQztnQkFDUixZQUFZLEVBQUUsQ0FBQyxzQkFBc0IsQ0FBQztnQkFDdEMsT0FBTyxFQUFFLENBQUMsWUFBWSxDQUFDO2dCQUN2QixPQUFPLEVBQUUsQ0FBQyxzQkFBc0IsQ0FBQzthQUNsQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IE15QW5ndWxhckZvcm1Db21wb25lbnQgfSBmcm9tIFwiLi9teS1hbmd1bGFyLWZvcm0uY29tcG9uZW50XCI7XG5pbXBvcnQgeyBTaGFyZWRNb2R1bGUgfSBmcm9tIFwiLi9zaGFyZWQvc2hhcmVkLm1vZHVsZVwiO1xuXG5ATmdNb2R1bGUoe1xuICBkZWNsYXJhdGlvbnM6IFtNeUFuZ3VsYXJGb3JtQ29tcG9uZW50XSxcbiAgaW1wb3J0czogW1NoYXJlZE1vZHVsZV0sXG4gIGV4cG9ydHM6IFtNeUFuZ3VsYXJGb3JtQ29tcG9uZW50XSxcbn0pXG5leHBvcnQgY2xhc3MgTXlBbmd1bGFyRm9ybU1vZHVsZSB7fVxuIl19