import { Component, Input, Output, EventEmitter, } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "./shared/service/device.service";
import * as i2 from "@angular/forms";
import * as i3 from "@angular/flex-layout/flex";
import * as i4 from "@angular/common";
import * as i5 from "./shared/ui/radio-button/radio-button.component";
import * as i6 from "./shared/ui/checkbox/checkbox.component";
import * as i7 from "./shared/ui/form-field-input/form-field-input-text/form-field-input-text.component";
import * as i8 from "./shared/ui/form-field-input/form-field-input-password/form-field-input-password.component";
import * as i9 from "./shared/ui/form-field-input/form-field-input-number/form-field-input-number.component";
import * as i10 from "./shared/ui/form-field-input/form-field-input-email/form-field-input-email.component";
import * as i11 from "./shared/ui/form-field-input/form-field-input-file/form-field-input-file.component";
import * as i12 from "./shared/ui/datepicker/datepicker.component";
import * as i13 from "./shared/ui/timepicker/timepicker.component";
import * as i14 from "./shared/ui/textarea/textarea.component";
import * as i15 from "./shared/ui/select/select.component";
import * as i16 from "./shared/ui/slider/slider.component";
import * as i17 from "./shared/ui/slide-toggle/slide-toggle.component";
import * as i18 from "./shared/ui/button/button.component";
import * as i19 from "./shared/ui/autocomplete/autocomplete.component";
import * as i20 from "@angular/material/form-field";
import * as i21 from "@angular/material/button";
function MyAngularFormComponent_section_2_section_1_app_radio_button_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "app-radio-button", 27);
} if (rf & 2) {
    const rowItem_r7 = i0.ɵɵnextContext().$implicit;
    i0.ɵɵproperty("formControlName", rowItem_r7.getControlName())("radioButtonGroup", rowItem_r7);
} }
function MyAngularFormComponent_section_2_section_1_app_checkbox_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "app-checkbox", 28);
} if (rf & 2) {
    const rowItem_r7 = i0.ɵɵnextContext().$implicit;
    i0.ɵɵproperty("formControlName", rowItem_r7.getControlName())("checkbox", rowItem_r7);
} }
function MyAngularFormComponent_section_2_section_1_app_form_field_input_text_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "app-form-field-input-text", 29);
} if (rf & 2) {
    const rowItem_r7 = i0.ɵɵnextContext().$implicit;
    i0.ɵɵproperty("formControlName", rowItem_r7.getControlName())("formFieldInputText", rowItem_r7);
} }
function MyAngularFormComponent_section_2_section_1_app_form_field_input_password_4_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "app-form-field-input-password", 30);
} if (rf & 2) {
    const rowItem_r7 = i0.ɵɵnextContext().$implicit;
    i0.ɵɵproperty("formControlName", rowItem_r7.getControlName())("formFieldInputPassword", rowItem_r7);
} }
function MyAngularFormComponent_section_2_section_1_app_form_field_input_number_5_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "app-form-field-input-number", 31);
} if (rf & 2) {
    const rowItem_r7 = i0.ɵɵnextContext().$implicit;
    i0.ɵɵproperty("formControlName", rowItem_r7.getControlName())("formFieldInputNumber", rowItem_r7);
} }
function MyAngularFormComponent_section_2_section_1_app_form_field_input_email_6_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "app-form-field-input-email", 32);
} if (rf & 2) {
    const rowItem_r7 = i0.ɵɵnextContext().$implicit;
    i0.ɵɵproperty("formControlName", rowItem_r7.getControlName())("formFieldInputEmail", rowItem_r7);
} }
function MyAngularFormComponent_section_2_section_1_app_form_field_input_file_7_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "app-form-field-input-file", 33);
} if (rf & 2) {
    const rowItem_r7 = i0.ɵɵnextContext().$implicit;
    i0.ɵɵproperty("formControlName", rowItem_r7.getControlName())("formFieldInputFile", rowItem_r7);
} }
function MyAngularFormComponent_section_2_section_1_app_datepicker_8_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "app-datepicker", 34);
} if (rf & 2) {
    const rowItem_r7 = i0.ɵɵnextContext().$implicit;
    i0.ɵɵproperty("formControlName", rowItem_r7.getControlName())("datepicker", rowItem_r7);
} }
function MyAngularFormComponent_section_2_section_1_app_timepicker_9_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "app-timepicker", 35);
} if (rf & 2) {
    const rowItem_r7 = i0.ɵɵnextContext().$implicit;
    i0.ɵɵproperty("formControlName", rowItem_r7.getControlName())("timepicker", rowItem_r7);
} }
function MyAngularFormComponent_section_2_section_1_app_textarea_10_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "app-textarea", 36);
} if (rf & 2) {
    const rowItem_r7 = i0.ɵɵnextContext().$implicit;
    i0.ɵɵproperty("formControlName", rowItem_r7.getControlName())("textarea", rowItem_r7);
} }
function MyAngularFormComponent_section_2_section_1_app_select_11_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "app-select", 37);
} if (rf & 2) {
    const rowItem_r7 = i0.ɵɵnextContext().$implicit;
    i0.ɵɵproperty("formControlName", rowItem_r7.getControlName())("select", rowItem_r7);
} }
function MyAngularFormComponent_section_2_section_1_app_slider_12_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "app-slider", 38);
} if (rf & 2) {
    const rowItem_r7 = i0.ɵɵnextContext().$implicit;
    i0.ɵɵproperty("formControlName", rowItem_r7.getControlName())("slider", rowItem_r7);
} }
function MyAngularFormComponent_section_2_section_1_app_slide_toggle_13_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "app-slide-toggle", 39);
} if (rf & 2) {
    const rowItem_r7 = i0.ɵɵnextContext().$implicit;
    i0.ɵɵproperty("formControlName", rowItem_r7.getControlName())("slideToggle", rowItem_r7);
} }
function MyAngularFormComponent_section_2_section_1_app_button_14_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "app-button", 40);
} if (rf & 2) {
    const rowItem_r7 = i0.ɵɵnextContext().$implicit;
    i0.ɵɵproperty("formControlName", rowItem_r7.getControlName())("button", rowItem_r7);
} }
function MyAngularFormComponent_section_2_section_1_ng_container_15_app_autocomplete_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "app-autocomplete", 42);
} if (rf & 2) {
    const rowItem_r7 = i0.ɵɵnextContext(2).$implicit;
    i0.ɵɵproperty("formControlName", rowItem_r7.getControlName())("autocomplete", rowItem_r7);
} }
function MyAngularFormComponent_section_2_section_1_ng_container_15_section_2_mat_error_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-error");
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const rowItem_r7 = i0.ɵɵnextContext(3).$implicit;
    const ctx_r43 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r43.getFirstExistingControlError(rowItem_r7).message);
} }
function MyAngularFormComponent_section_2_section_1_ng_container_15_section_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "section", 43);
    i0.ɵɵtemplate(1, MyAngularFormComponent_section_2_section_1_ng_container_15_section_2_mat_error_1_Template, 2, 1, "mat-error", 24);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const rowItem_r7 = i0.ɵɵnextContext(2).$implicit;
    const ctx_r41 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r41.getFirstExistingControlError(rowItem_r7));
} }
function MyAngularFormComponent_section_2_section_1_ng_container_15_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵtemplate(1, MyAngularFormComponent_section_2_section_1_ng_container_15_app_autocomplete_1_Template, 1, 2, "app-autocomplete", 41);
    i0.ɵɵtemplate(2, MyAngularFormComponent_section_2_section_1_ng_container_15_section_2_Template, 2, 1, "section", 26);
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const autocompleteSubjects_r39 = ctx.ngIf;
    const rowItem_r7 = i0.ɵɵnextContext().$implicit;
    const ctx_r22 = i0.ɵɵnextContext(2);
    const _r0 = i0.ɵɵreference(1);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !autocompleteSubjects_r39.hide);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !autocompleteSubjects_r39.hide && (!ctx_r22.form.getFormGroup().get(rowItem_r7.getControlName()).pristine && ctx_r22.hasFormControlError(rowItem_r7.getControlName()) || _r0.submitted && ctx_r22.hasFormControlError(rowItem_r7.getControlName())));
} }
function MyAngularFormComponent_section_2_section_1_p_17_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "p", 44);
    i0.ɵɵtext(1, " ssssssss ");
    i0.ɵɵelementEnd();
} }
function MyAngularFormComponent_section_2_section_1_section_18_mat_error_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-error");
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const rowItem_r7 = i0.ɵɵnextContext(2).$implicit;
    const ctx_r47 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r47.getFirstExistingControlError(rowItem_r7).message);
} }
function MyAngularFormComponent_section_2_section_1_section_18_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "section", 43);
    i0.ɵɵtemplate(1, MyAngularFormComponent_section_2_section_1_section_18_mat_error_1_Template, 2, 1, "mat-error", 24);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const rowItem_r7 = i0.ɵɵnextContext().$implicit;
    const ctx_r24 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r24.getFirstExistingControlError(rowItem_r7));
} }
const _c0 = function (a0) { return { hide: a0 }; };
function MyAngularFormComponent_section_2_section_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "section", 9);
    i0.ɵɵtemplate(1, MyAngularFormComponent_section_2_section_1_app_radio_button_1_Template, 1, 2, "app-radio-button", 10);
    i0.ɵɵtemplate(2, MyAngularFormComponent_section_2_section_1_app_checkbox_2_Template, 1, 2, "app-checkbox", 11);
    i0.ɵɵtemplate(3, MyAngularFormComponent_section_2_section_1_app_form_field_input_text_3_Template, 1, 2, "app-form-field-input-text", 12);
    i0.ɵɵtemplate(4, MyAngularFormComponent_section_2_section_1_app_form_field_input_password_4_Template, 1, 2, "app-form-field-input-password", 13);
    i0.ɵɵtemplate(5, MyAngularFormComponent_section_2_section_1_app_form_field_input_number_5_Template, 1, 2, "app-form-field-input-number", 14);
    i0.ɵɵtemplate(6, MyAngularFormComponent_section_2_section_1_app_form_field_input_email_6_Template, 1, 2, "app-form-field-input-email", 15);
    i0.ɵɵtemplate(7, MyAngularFormComponent_section_2_section_1_app_form_field_input_file_7_Template, 1, 2, "app-form-field-input-file", 16);
    i0.ɵɵtemplate(8, MyAngularFormComponent_section_2_section_1_app_datepicker_8_Template, 1, 2, "app-datepicker", 17);
    i0.ɵɵtemplate(9, MyAngularFormComponent_section_2_section_1_app_timepicker_9_Template, 1, 2, "app-timepicker", 18);
    i0.ɵɵtemplate(10, MyAngularFormComponent_section_2_section_1_app_textarea_10_Template, 1, 2, "app-textarea", 19);
    i0.ɵɵtemplate(11, MyAngularFormComponent_section_2_section_1_app_select_11_Template, 1, 2, "app-select", 20);
    i0.ɵɵtemplate(12, MyAngularFormComponent_section_2_section_1_app_slider_12_Template, 1, 2, "app-slider", 21);
    i0.ɵɵtemplate(13, MyAngularFormComponent_section_2_section_1_app_slide_toggle_13_Template, 1, 2, "app-slide-toggle", 22);
    i0.ɵɵtemplate(14, MyAngularFormComponent_section_2_section_1_app_button_14_Template, 1, 2, "app-button", 23);
    i0.ɵɵtemplate(15, MyAngularFormComponent_section_2_section_1_ng_container_15_Template, 3, 2, "ng-container", 24);
    i0.ɵɵpipe(16, "async");
    i0.ɵɵtemplate(17, MyAngularFormComponent_section_2_section_1_p_17_Template, 2, 0, "p", 25);
    i0.ɵɵtemplate(18, MyAngularFormComponent_section_2_section_1_section_18_Template, 2, 1, "section", 26);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const rowItem_r7 = ctx.$implicit;
    const ctx_r6 = i0.ɵɵnextContext(2);
    const _r0 = i0.ɵɵreference(1);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", rowItem_r7.getType() == "RADIO_BUTTON_GROUP");
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", rowItem_r7.getType() == "CHECKBOX");
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", rowItem_r7.getType() == "FORM_FIELD_INPUT" && ctx_r6.getFormFieldInputType(rowItem_r7) === "text");
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", rowItem_r7.getType() == "FORM_FIELD_INPUT" && ctx_r6.getFormFieldInputType(rowItem_r7) === "password");
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", rowItem_r7.getType() == "FORM_FIELD_INPUT" && ctx_r6.getFormFieldInputType(rowItem_r7) === "number");
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", rowItem_r7.getType() == "FORM_FIELD_INPUT" && ctx_r6.getFormFieldInputType(rowItem_r7) === "email");
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", rowItem_r7.getType() == "FORM_FIELD_INPUT" && ctx_r6.getFormFieldInputType(rowItem_r7) === "file");
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", rowItem_r7.getType() == "DATEPICKER");
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", rowItem_r7.getType() == "TIMEPICKER");
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", rowItem_r7.getType() == "FORM_FIELD_TEXTAREA");
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", rowItem_r7.getType() == "FORM_FIELD_SELECT");
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", rowItem_r7.getType() == "SLIDER");
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", rowItem_r7.getType() == "SLIDE_TOGGLE");
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", rowItem_r7.getType() == "BUTTON");
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", rowItem_r7.getType() == "AUTOCOMPLETE" && i0.ɵɵpureFunction1(19, _c0, i0.ɵɵpipeBind1(16, 17, rowItem_r7.getHiddenSubject())));
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngIf", !(rowItem_r7.getType() == "RADIO_BUTTON_GROUP" || rowItem_r7.getType() == "CHECKBOX" || rowItem_r7.getType() == "FORM_FIELD_INPUT" || rowItem_r7.getType() == "DATEPICKER" || rowItem_r7.getType() == "FORM_FIELD_TEXTAREA" || rowItem_r7.getType() == "FORM_FIELD_SELECT" || rowItem_r7.getType() == "SLIDER" || rowItem_r7.getType() == "SLIDE_TOGGLE" || rowItem_r7.getType() == "BUTTON" || rowItem_r7.getType() == "AUTOCOMPLETE" || rowItem_r7.getType() == "TIMEPICKER"));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", rowItem_r7.getType() != "AUTOCOMPLETE" && (ctx_r6.form.getFormGroup().get(rowItem_r7.getControlName()) && !ctx_r6.form.getFormGroup().get(rowItem_r7.getControlName()).pristine && ctx_r6.hasFormControlError(rowItem_r7.getControlName()) || _r0.submitted && ctx_r6.hasFormControlError(rowItem_r7.getControlName())));
} }
function MyAngularFormComponent_section_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "section", 7);
    i0.ɵɵtemplate(1, MyAngularFormComponent_section_2_section_1_Template, 19, 21, "section", 8);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const row_r5 = ctx.$implicit;
    const ctx_r1 = i0.ɵɵnextContext();
    i0.ɵɵproperty("fxLayout", ctx_r1.deviceService.isDeviceDesktop() ? "row" : "column")("fxLayoutAlign", ctx_r1.deviceService.isDeviceDesktop() ? "center start" : "center center")("fxLayoutGap", ctx_r1.deviceService.isDeviceDesktop() ? "20px" : "10px");
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngForOf", row_r5.getChildren());
} }
function MyAngularFormComponent_button_4_Template(rf, ctx) { if (rf & 1) {
    const _r51 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 45);
    i0.ɵɵlistener("click", function MyAngularFormComponent_button_4_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r51); const ctx_r50 = i0.ɵɵnextContext(); return ctx_r50.form.submitOnlyIfFormValid ? ctx_r50.form.getFormGroup().valid && ctx_r50.onSubmitHappened() : ctx_r50.onSubmitHappened(); });
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r2 = i0.ɵɵnextContext();
    i0.ɵɵproperty("disabled", ctx_r2.form.submitOnlyIfFormValid ? !ctx_r2.form.getFormGroup().valid : false);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r2.form.submitButtonText);
} }
function MyAngularFormComponent_button_5_Template(rf, ctx) { if (rf & 1) {
    const _r53 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 46);
    i0.ɵɵlistener("click", function MyAngularFormComponent_button_5_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r53); const ctx_r52 = i0.ɵɵnextContext(); return ctx_r52.onCancelHappened(); });
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r3 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r3.form.cancelButtonText);
} }
function MyAngularFormComponent_button_6_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "button", 47);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r4 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r4.form.resetButtonText);
} }
export class MyAngularFormComponent {
    constructor(deviceService) {
        this.deviceService = deviceService;
        this.onSubmit = new EventEmitter();
        this.onCancel = new EventEmitter();
        this.form = undefined;
        this.onSubmitHappened = () => {
            this.onSubmit.emit(this.form.getFormGroup());
        };
        this.onCancelHappened = () => {
            this.onCancel.emit();
        };
        this.hasFormControlError = (formControlName) => {
            return this.form.getFormGroup().get(formControlName)
                ? this.form.getFormGroup().get(formControlName).errors
                    ? true
                    : false
                : false;
        };
        this.getFirstExistingControlError = (rowItem) => {
            var _a;
            let errors = (_a = this.form.getFormGroup().get(rowItem.getControlName())) === null || _a === void 0 ? void 0 : _a.errors;
            if (errors)
                for (let key in errors) {
                    for (let index in rowItem.getValidators()) {
                        if (rowItem.getValidators()[index].name === key)
                            return rowItem.getValidators()[index];
                    }
                }
            return undefined;
        };
        // for form-field-input
        this.getFormFieldInputType = (rowItem) => {
            return rowItem.getFormFieldInputType();
        };
    }
    ngOnInit() {
        // console.log("MyFormComponent init");
    }
    ngOnChanges(changes) {
        if (changes.form && changes.form.currentValue) {
            // console.log(changes.form);
            // this.initForm();
        }
    }
    ngDoCheck() {
        // if (this.form.getChildren().length !== this._numOfFormRows) {
        //   console.log(this._numOfFormRows);
        //   console.log(this.form.getChildren().length);
        //   for (
        //     let index = this._numOfFormRows;
        //     index < this.form.getChildren().length;
        //     index++
        //   ) {
        //     this._updateForm(<FormRow>this.form.getChildren()[index]);
        //   }
        // }
    }
    ngOnDestroy() {
        // console.log("MyFormComponent destroyed");
    }
}
MyAngularFormComponent.ɵfac = function MyAngularFormComponent_Factory(t) { return new (t || MyAngularFormComponent)(i0.ɵɵdirectiveInject(i1.DeviceService)); };
MyAngularFormComponent.ɵcmp = i0.ɵɵdefineComponent({ type: MyAngularFormComponent, selectors: [["lib-my-angular-form"]], inputs: { form: "form" }, outputs: { onSubmit: "onSubmit", onCancel: "onCancel" }, features: [i0.ɵɵNgOnChangesFeature], decls: 7, vars: 5, consts: [["fxLayout", "column", "fxLayoutAlign", "center center", "fxLayoutGap", "10px", 3, "formGroup"], ["ngForm", "ngForm"], ["class", "row overflow-hidden", 3, "fxLayout", "fxLayoutAlign", "fxLayoutGap", 4, "ngFor", "ngForOf"], ["fxLayoutAlign", "center center", "fxLayoutGap", "2vw"], ["type", "button", "mat-raised-button", "", 3, "disabled", "click", 4, "ngIf"], ["type", "button", "mat-raised-button", "", 3, "click", 4, "ngIf"], ["type", "reset", "mat-raised-button", "", 4, "ngIf"], [1, "row", "overflow-hidden", 3, "fxLayout", "fxLayoutAlign", "fxLayoutGap"], ["class", "row-item", "fxFlex", "", 4, "ngFor", "ngForOf"], ["fxFlex", "", 1, "row-item"], [3, "formControlName", "radioButtonGroup", 4, "ngIf"], [3, "formControlName", "checkbox", 4, "ngIf"], [3, "formControlName", "formFieldInputText", 4, "ngIf"], [3, "formControlName", "formFieldInputPassword", 4, "ngIf"], [3, "formControlName", "formFieldInputNumber", 4, "ngIf"], [3, "formControlName", "formFieldInputEmail", 4, "ngIf"], [3, "formControlName", "formFieldInputFile", 4, "ngIf"], [3, "formControlName", "datepicker", 4, "ngIf"], [3, "formControlName", "timepicker", 4, "ngIf"], [3, "formControlName", "textarea", 4, "ngIf"], [3, "formControlName", "select", 4, "ngIf"], [3, "formControlName", "slider", 4, "ngIf"], [3, "formControlName", "slideToggle", 4, "ngIf"], [3, "formControlName", "button", 4, "ngIf"], [4, "ngIf"], ["class", "no-margin", "fxLayout", "column", "fxLayoutAlign", "center center", 4, "ngIf"], ["fxLayout", "column", "fxLayoutAlign", "center center", "class", "top-margin", 4, "ngIf"], [3, "formControlName", "radioButtonGroup"], [3, "formControlName", "checkbox"], [3, "formControlName", "formFieldInputText"], [3, "formControlName", "formFieldInputPassword"], [3, "formControlName", "formFieldInputNumber"], [3, "formControlName", "formFieldInputEmail"], [3, "formControlName", "formFieldInputFile"], [3, "formControlName", "datepicker"], [3, "formControlName", "timepicker"], [3, "formControlName", "textarea"], [3, "formControlName", "select"], [3, "formControlName", "slider"], [3, "formControlName", "slideToggle"], [3, "formControlName", "button"], [3, "formControlName", "autocomplete", 4, "ngIf"], [3, "formControlName", "autocomplete"], ["fxLayout", "column", "fxLayoutAlign", "center center", 1, "top-margin"], ["fxLayout", "column", "fxLayoutAlign", "center center", 1, "no-margin"], ["type", "button", "mat-raised-button", "", 3, "disabled", "click"], ["type", "button", "mat-raised-button", "", 3, "click"], ["type", "reset", "mat-raised-button", ""]], template: function MyAngularFormComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "form", 0, 1);
        i0.ɵɵtemplate(2, MyAngularFormComponent_section_2_Template, 2, 4, "section", 2);
        i0.ɵɵelementStart(3, "section", 3);
        i0.ɵɵtemplate(4, MyAngularFormComponent_button_4_Template, 2, 2, "button", 4);
        i0.ɵɵtemplate(5, MyAngularFormComponent_button_5_Template, 2, 1, "button", 5);
        i0.ɵɵtemplate(6, MyAngularFormComponent_button_6_Template, 2, 1, "button", 6);
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵproperty("formGroup", ctx.form.getFormGroup());
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("ngForOf", ctx.form.getChildren());
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("ngIf", ctx.form.showSubmitButton);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.form.showCancelButton);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.form.showResetButton);
    } }, directives: [i2.ɵangular_packages_forms_forms_y, i2.NgControlStatusGroup, i3.DefaultLayoutDirective, i3.DefaultLayoutAlignDirective, i3.DefaultLayoutGapDirective, i2.FormGroupDirective, i4.NgForOf, i4.NgIf, i3.DefaultFlexDirective, i5.RadioButtonComponent, i2.NgControlStatus, i2.FormControlName, i6.CheckboxComponent, i7.FormFieldInputTextComponent, i8.FormFieldInputPasswordComponent, i9.FormFieldInputNumberComponent, i10.FormFieldInputEmailComponent, i11.FormFieldInputFileComponent, i12.DatepickerComponent, i13.TimepickerComponent, i14.TextareaComponent, i15.SelectComponent, i16.SliderComponent, i17.SlideToggleComponent, i18.ButtonComponent, i19.AutocompleteComponent, i20.MatError, i21.MatButton], pipes: [i4.AsyncPipe], styles: ["", "section.row[_ngcontent-%COMP%] {\n        width: 100%;\n        \n        padding: 10px 0;\n        \n    }\n\n    section.row-item[_ngcontent-%COMP%] {\n        \n        \n    }\n\n    .overflow-hidden[_ngcontent-%COMP%] {\n        overflow: hidden;\n    }\n\n    .text-overflow-ellipsis[_ngcontent-%COMP%] {\n        text-overflow: ellipsis;\n    }\n\n    .white-space-nowrap[_ngcontent-%COMP%] {\n        white-space: nowrap;\n    }\n\n    form[_ngcontent-%COMP%] {\n        padding: 15px;\n        \n        \n    }\n\n    .top-margin[_ngcontent-%COMP%] {\n        margin-top: 10px;\n    }\n\n    .no-margin[_ngcontent-%COMP%] {\n        margin: 0;\n    }\n\n    .no-padding[_ngcontent-%COMP%] {\n        padding: 0;\n    }"] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(MyAngularFormComponent, [{
        type: Component,
        args: [{
                selector: 'lib-my-angular-form',
                templateUrl: './my-angular-form.component.html',
                styleUrls: ['./my-angular-form.component.css'],
            }]
    }], function () { return [{ type: i1.DeviceService }]; }, { onSubmit: [{
            type: Output
        }], onCancel: [{
            type: Output
        }], form: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXktYW5ndWxhci1mb3JtLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215LWFuZ3VsYXItZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9teS1hbmd1bGFyLWZvcm0uY29tcG9uZW50LnRzIiwibGliL215LWFuZ3VsYXItZm9ybS5jb21wb25lbnQuaHRtbCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQ0wsU0FBUyxFQUVULEtBQUssRUFFTCxNQUFNLEVBQ04sWUFBWSxHQUliLE1BQU0sZUFBZSxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUN3REMsdUNBR29EOzs7SUFGaEQsNkRBQTRDLGdDQUFBOzs7SUFLaEQsbUNBR3dDOzs7SUFGcEMsNkRBQTRDLHdCQUFBOzs7SUFLaEQsZ0RBRytEOzs7SUFGM0QsNkRBQTRDLGtDQUFBOzs7SUFLaEQsb0RBR3VFOzs7SUFGbkUsNkRBQTRDLHNDQUFBOzs7SUFLaEQsa0RBR21FOzs7SUFGL0QsNkRBQTRDLG9DQUFBOzs7SUFLaEQsaURBR2lFOzs7SUFGN0QsNkRBQTRDLG1DQUFBOzs7SUFLaEQsZ0RBRytEOzs7SUFGM0QsNkRBQTRDLGtDQUFBOzs7SUFLaEQscUNBRzRDOzs7SUFGeEMsNkRBQTRDLDBCQUFBOzs7SUFJaEQscUNBRzRDOzs7SUFGeEMsNkRBQTRDLDBCQUFBOzs7SUFJaEQsbUNBR3dDOzs7SUFGcEMsNkRBQTRDLHdCQUFBOzs7SUFJaEQsaUNBR29DOzs7SUFGaEMsNkRBQTRDLHNCQUFBOzs7SUFJaEQsaUNBR29DOzs7SUFGaEMsNkRBQTRDLHNCQUFBOzs7SUFJaEQsdUNBRytDOzs7SUFGM0MsNkRBQTRDLDJCQUFBOzs7SUFJaEQsaUNBR29DOzs7SUFGaEMsNkRBQTRDLHNCQUFBOzs7SUFPNUMsdUNBR2dEOzs7SUFGNUMsNkRBQTRDLDRCQUFBOzs7SUFXeEMsaUNBQ3VEO0lBQUEsWUFBc0Q7SUFBQSxpQkFBWTs7OztJQUFsRSxlQUFzRDtJQUF0RCw4RUFBc0Q7OztJQVBySCxtQ0FNUTtJQUFBLGtJQUN1RDtJQUUvRCxpQkFBVTs7OztJQUZFLGVBQWtEO0lBQWxELHVFQUFrRDs7O0lBaEJsRSw2QkFHSTtJQUFBLHNJQUc2QjtJQUc3QixvSEFNUTtJQUtaLDBCQUFlOzs7Ozs7SUFmUCxlQUFrQztJQUFsQyxxREFBa0M7SUFRbEMsZUFDa0Y7SUFEbEYsMFFBQ2tGOzs7SUFTMUYsNkJBZVE7SUFBQSwwQkFDUjtJQUFBLGlCQUFJOzs7SUFrQkksaUNBQ3VEO0lBQUEsWUFBc0Q7SUFBQSxpQkFBWTs7OztJQUFsRSxlQUFzRDtJQUF0RCw4RUFBc0Q7OztJQWhCckgsbUNBT1E7SUFRQSxtSEFDdUQ7SUFFL0QsaUJBQVU7Ozs7SUFGRSxlQUFrRDtJQUFsRCx1RUFBa0Q7Ozs7SUE3SXRFLGtDQUtRO0lBQ0Esc0hBR2lDO0lBR2pDLDhHQUd5QjtJQUd6Qix3SUFHbUM7SUFHbkMsZ0pBR3VDO0lBR3ZDLDRJQUdxQztJQUdyQywwSUFHb0M7SUFHcEMsd0lBR21DO0lBR25DLGtIQUcyQjtJQUUzQixrSEFHMkI7SUFFM0IsZ0hBR3lCO0lBRXpCLDRHQUd1QjtJQUV2Qiw0R0FHdUI7SUFFdkIsd0hBRzRCO0lBRTVCLDRHQUd1QjtJQUV2QixnSEFHSTs7SUFvQkosMEZBZVE7SUFJUixzR0FPUTtJQWVoQixpQkFBVTs7Ozs7SUEzSUUsZUFBaUQ7SUFBakQsbUVBQWlEO0lBTWpELGVBQXVDO0lBQXZDLHlEQUF1QztJQU12QyxlQUFpRztJQUFqRyx3SEFBaUc7SUFNakcsZUFBcUc7SUFBckcsNEhBQXFHO0lBTXJHLGVBQW1HO0lBQW5HLDBIQUFtRztJQU1uRyxlQUFrRztJQUFsRyx5SEFBa0c7SUFNbEcsZUFBaUc7SUFBakcsd0hBQWlHO0lBTWpHLGVBQXlDO0lBQXpDLDJEQUF5QztJQUt6QyxlQUF5QztJQUF6QywyREFBeUM7SUFLekMsZUFBa0Q7SUFBbEQsb0VBQWtEO0lBS2xELGVBQWdEO0lBQWhELGtFQUFnRDtJQUtoRCxlQUFxQztJQUFyQyx1REFBcUM7SUFLckMsZUFBMkM7SUFBM0MsNkRBQTJDO0lBSzNDLGVBQXFDO0lBQXJDLHVEQUFxQztJQUlyQyxlQUFpSDtJQUFqSCxtSkFBaUg7SUF1QmpILGVBVTJDO0lBVjNDLHNlQVUyQztJQVkzQyxlQUNrRjtJQURsRiw4VUFDa0Y7OztJQXpJdEcsa0NBT1E7SUFBQSwyRkFLUTtJQWdKaEIsaUJBQVU7Ozs7SUF6Sk4sb0ZBQW9FLDRGQUFBLHlFQUFBO0lBTTVELGVBQXlDO0lBQXpDLDhDQUF5Qzs7OztJQXlKN0Msa0NBSzJGO0lBRnZGLDZNQUEyQywyQkFBd0IscUVBQTREO0lBRXhDLFlBQThCO0lBQUEsaUJBQVM7OztJQUE5SCx3R0FBc0Y7SUFBQyxlQUE4QjtJQUE5QixrREFBOEI7Ozs7SUFFekgsa0NBSXNCO0lBRGxCLHNNQUFpQztJQUNmLFlBQThCO0lBQUEsaUJBQVM7OztJQUF2QyxlQUE4QjtJQUE5QixrREFBOEI7OztJQUVwRCxrQ0FHc0I7SUFBQSxZQUE2QjtJQUFBLGlCQUFTOzs7SUFBdEMsZUFBNkI7SUFBN0IsaURBQTZCOztBRHRNbkUsTUFBTSxPQUFPLHNCQUFzQjtJQU1qQyxZQUFtQixhQUE0QjtRQUE1QixrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQUpyQyxhQUFRLEdBQTRCLElBQUksWUFBWSxFQUFhLENBQUM7UUFDbEUsYUFBUSxHQUFzQixJQUFJLFlBQVksRUFBTyxDQUFDO1FBQ3ZELFNBQUksR0FBUyxTQUFTLENBQUM7UUFJaEMscUJBQWdCLEdBQUcsR0FBUyxFQUFFO1lBQzVCLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUMsQ0FBQztRQUMvQyxDQUFDLENBQUM7UUFFRixxQkFBZ0IsR0FBRyxHQUFTLEVBQUU7WUFDNUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUN2QixDQUFDLENBQUM7UUFFRix3QkFBbUIsR0FBRyxDQUFDLGVBQXVCLEVBQVcsRUFBRTtZQUN6RCxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQztnQkFDbEQsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxDQUFDLE1BQU07b0JBQ3BELENBQUMsQ0FBQyxJQUFJO29CQUNOLENBQUMsQ0FBQyxLQUFLO2dCQUNULENBQUMsQ0FBQyxLQUFLLENBQUM7UUFDWixDQUFDLENBQUM7UUFFRixpQ0FBNEIsR0FBRyxDQUM3QixPQUFvQixFQUNXLEVBQUU7O1lBQ2pDLElBQUksTUFBTSxTQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsQ0FBQywwQ0FBRSxNQUFNLENBQUM7WUFFNUUsSUFBSSxNQUFNO2dCQUNSLEtBQUssSUFBSSxHQUFHLElBQUksTUFBTSxFQUFFO29CQUN0QixLQUFLLElBQUksS0FBSyxJQUFJLE9BQU8sQ0FBQyxhQUFhLEVBQUUsRUFBRTt3QkFDekMsSUFBSSxPQUFPLENBQUMsYUFBYSxFQUFFLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxLQUFLLEdBQUc7NEJBQzdDLE9BQU8sT0FBTyxDQUFDLGFBQWEsRUFBRSxDQUFDLEtBQUssQ0FBQyxDQUFDO3FCQUN6QztpQkFDRjtZQUVILE9BQU8sU0FBUyxDQUFDO1FBQ25CLENBQUMsQ0FBQztRQUVGLHVCQUF1QjtRQUN2QiwwQkFBcUIsR0FBRyxDQUFDLE9BQW9CLEVBQXlCLEVBQUU7WUFDdEUsT0FBd0IsT0FBUSxDQUFDLHFCQUFxQixFQUFFLENBQUM7UUFDM0QsQ0FBQyxDQUFDO0lBckNnRCxDQUFDO0lBdUNuRCxRQUFRO1FBQ04sdUNBQXVDO0lBQ3pDLENBQUM7SUFFRCxXQUFXLENBQUMsT0FBc0I7UUFDaEMsSUFBSSxPQUFPLENBQUMsSUFBSSxJQUFJLE9BQU8sQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFO1lBQzdDLDZCQUE2QjtZQUM3QixtQkFBbUI7U0FDcEI7SUFDSCxDQUFDO0lBRUQsU0FBUztRQUNQLGdFQUFnRTtRQUNoRSxzQ0FBc0M7UUFDdEMsaURBQWlEO1FBQ2pELFVBQVU7UUFDVix1Q0FBdUM7UUFDdkMsOENBQThDO1FBQzlDLGNBQWM7UUFDZCxRQUFRO1FBQ1IsaUVBQWlFO1FBQ2pFLE1BQU07UUFDTixJQUFJO0lBQ04sQ0FBQztJQUVELFdBQVc7UUFDVCw0Q0FBNEM7SUFDOUMsQ0FBQzs7NEZBeEVVLHNCQUFzQjsyREFBdEIsc0JBQXNCO1FDYW5DLGtDQU9RO1FBQUEsK0VBT1E7UUF1SlIsa0NBSVE7UUFBQSw2RUFLMkY7UUFFM0YsNkVBSXNCO1FBRXRCLDZFQUdzQjtRQUU5QixpQkFBVTtRQUVsQixpQkFBTzs7UUE1TEgsbURBQXNDO1FBUTlCLGVBQTJDO1FBQTNDLGdEQUEyQztRQWlLbkMsZUFBa0M7UUFBbEMsZ0RBQWtDO1FBT2xDLGVBQWtDO1FBQWxDLGdEQUFrQztRQU1sQyxlQUFpQztRQUFqQywrQ0FBaUM7O2tERHBNeEMsc0JBQXNCO2NBTGxDLFNBQVM7ZUFBQztnQkFDVCxRQUFRLEVBQUUscUJBQXFCO2dCQUMvQixXQUFXLEVBQUUsa0NBQWtDO2dCQUMvQyxTQUFTLEVBQUUsQ0FBQyxpQ0FBaUMsQ0FBQzthQUMvQzs7a0JBR0UsTUFBTTs7a0JBQ04sTUFBTTs7a0JBQ04sS0FBSyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XG4gIENvbXBvbmVudCxcbiAgT25Jbml0LFxuICBJbnB1dCxcbiAgT25EZXN0cm95LFxuICBPdXRwdXQsXG4gIEV2ZW50RW1pdHRlcixcbiAgU2ltcGxlQ2hhbmdlcyxcbiAgT25DaGFuZ2VzLFxuICBEb0NoZWNrLFxufSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEZvcm1Hcm91cCwgRm9ybUNvbnRyb2wgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5cbi8vIG1vZGVsXG5pbXBvcnQgeyBGb3JtIH0gZnJvbSAnLi9zaGFyZWQvbW9kZWwvc3RydWN0dXJhbC9jb21wb3NpdGUvZm9ybS9mb3JtLm1vZGVsJztcbmltcG9ydCB7IEZvcm1Sb3cgfSBmcm9tICcuL3NoYXJlZC9tb2RlbC9zdHJ1Y3R1cmFsL2NvbXBvc2l0ZS9mb3JtL2Zvcm0tcm93Lm1vZGVsJztcbmltcG9ydCB7XG4gIEZvcm1Sb3dJdGVtLFxuICBGb3JtUm93SXRlbVZhbGlkYXRvckludGVyZmFjZSxcbn0gZnJvbSAnLi9zaGFyZWQvbW9kZWwvc3RydWN0dXJhbC9jb21wb3NpdGUvZm9ybS9mb3JtLXJvdy1pdGVtLm1vZGVsJztcbmltcG9ydCB7XG4gIEZPUk1fRklFTERfSU5QVVRfVFlQRSxcbiAgRm9ybUZpZWxkSW5wdXQsXG59IGZyb20gJy4vc2hhcmVkL21vZGVsL3N0cnVjdHVyYWwvY29tcG9zaXRlL2Zvcm0vZm9ybS1maWVsZC1pbnB1dC9mb3JtLWZpZWxkLWlucHV0Lm1vZGVsJztcblxuLy8gc2VydmljZVxuaW1wb3J0IHsgRGV2aWNlU2VydmljZSB9IGZyb20gJy4vc2hhcmVkL3NlcnZpY2UvZGV2aWNlLnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdsaWItbXktYW5ndWxhci1mb3JtJyxcbiAgdGVtcGxhdGVVcmw6ICcuL215LWFuZ3VsYXItZm9ybS5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL215LWFuZ3VsYXItZm9ybS5jb21wb25lbnQuY3NzJ10sXG59KVxuZXhwb3J0IGNsYXNzIE15QW5ndWxhckZvcm1Db21wb25lbnRcbiAgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSwgT25DaGFuZ2VzLCBEb0NoZWNrIHtcbiAgQE91dHB1dCgpIG9uU3VibWl0OiBFdmVudEVtaXR0ZXI8Rm9ybUdyb3VwPiA9IG5ldyBFdmVudEVtaXR0ZXI8Rm9ybUdyb3VwPigpO1xuICBAT3V0cHV0KCkgb25DYW5jZWw6IEV2ZW50RW1pdHRlcjxhbnk+ID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XG4gIEBJbnB1dCgpIGZvcm06IEZvcm0gPSB1bmRlZmluZWQ7XG5cbiAgY29uc3RydWN0b3IocHVibGljIGRldmljZVNlcnZpY2U6IERldmljZVNlcnZpY2UpIHt9XG5cbiAgb25TdWJtaXRIYXBwZW5lZCA9ICgpOiB2b2lkID0+IHtcbiAgICB0aGlzLm9uU3VibWl0LmVtaXQodGhpcy5mb3JtLmdldEZvcm1Hcm91cCgpKTtcbiAgfTtcblxuICBvbkNhbmNlbEhhcHBlbmVkID0gKCk6IHZvaWQgPT4ge1xuICAgIHRoaXMub25DYW5jZWwuZW1pdCgpO1xuICB9O1xuXG4gIGhhc0Zvcm1Db250cm9sRXJyb3IgPSAoZm9ybUNvbnRyb2xOYW1lOiBzdHJpbmcpOiBib29sZWFuID0+IHtcbiAgICByZXR1cm4gdGhpcy5mb3JtLmdldEZvcm1Hcm91cCgpLmdldChmb3JtQ29udHJvbE5hbWUpXG4gICAgICA/IHRoaXMuZm9ybS5nZXRGb3JtR3JvdXAoKS5nZXQoZm9ybUNvbnRyb2xOYW1lKS5lcnJvcnNcbiAgICAgICAgPyB0cnVlXG4gICAgICAgIDogZmFsc2VcbiAgICAgIDogZmFsc2U7XG4gIH07XG5cbiAgZ2V0Rmlyc3RFeGlzdGluZ0NvbnRyb2xFcnJvciA9IChcbiAgICByb3dJdGVtOiBGb3JtUm93SXRlbVxuICApOiBGb3JtUm93SXRlbVZhbGlkYXRvckludGVyZmFjZSA9PiB7XG4gICAgbGV0IGVycm9ycyA9IHRoaXMuZm9ybS5nZXRGb3JtR3JvdXAoKS5nZXQocm93SXRlbS5nZXRDb250cm9sTmFtZSgpKT8uZXJyb3JzO1xuXG4gICAgaWYgKGVycm9ycylcbiAgICAgIGZvciAobGV0IGtleSBpbiBlcnJvcnMpIHtcbiAgICAgICAgZm9yIChsZXQgaW5kZXggaW4gcm93SXRlbS5nZXRWYWxpZGF0b3JzKCkpIHtcbiAgICAgICAgICBpZiAocm93SXRlbS5nZXRWYWxpZGF0b3JzKClbaW5kZXhdLm5hbWUgPT09IGtleSlcbiAgICAgICAgICAgIHJldHVybiByb3dJdGVtLmdldFZhbGlkYXRvcnMoKVtpbmRleF07XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgIHJldHVybiB1bmRlZmluZWQ7XG4gIH07XG5cbiAgLy8gZm9yIGZvcm0tZmllbGQtaW5wdXRcbiAgZ2V0Rm9ybUZpZWxkSW5wdXRUeXBlID0gKHJvd0l0ZW06IEZvcm1Sb3dJdGVtKTogRk9STV9GSUVMRF9JTlBVVF9UWVBFID0+IHtcbiAgICByZXR1cm4gKDxGb3JtRmllbGRJbnB1dD5yb3dJdGVtKS5nZXRGb3JtRmllbGRJbnB1dFR5cGUoKTtcbiAgfTtcblxuICBuZ09uSW5pdCgpIHtcbiAgICAvLyBjb25zb2xlLmxvZyhcIk15Rm9ybUNvbXBvbmVudCBpbml0XCIpO1xuICB9XG5cbiAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcyk6IHZvaWQge1xuICAgIGlmIChjaGFuZ2VzLmZvcm0gJiYgY2hhbmdlcy5mb3JtLmN1cnJlbnRWYWx1ZSkge1xuICAgICAgLy8gY29uc29sZS5sb2coY2hhbmdlcy5mb3JtKTtcbiAgICAgIC8vIHRoaXMuaW5pdEZvcm0oKTtcbiAgICB9XG4gIH1cblxuICBuZ0RvQ2hlY2soKTogdm9pZCB7XG4gICAgLy8gaWYgKHRoaXMuZm9ybS5nZXRDaGlsZHJlbigpLmxlbmd0aCAhPT0gdGhpcy5fbnVtT2ZGb3JtUm93cykge1xuICAgIC8vICAgY29uc29sZS5sb2codGhpcy5fbnVtT2ZGb3JtUm93cyk7XG4gICAgLy8gICBjb25zb2xlLmxvZyh0aGlzLmZvcm0uZ2V0Q2hpbGRyZW4oKS5sZW5ndGgpO1xuICAgIC8vICAgZm9yIChcbiAgICAvLyAgICAgbGV0IGluZGV4ID0gdGhpcy5fbnVtT2ZGb3JtUm93cztcbiAgICAvLyAgICAgaW5kZXggPCB0aGlzLmZvcm0uZ2V0Q2hpbGRyZW4oKS5sZW5ndGg7XG4gICAgLy8gICAgIGluZGV4KytcbiAgICAvLyAgICkge1xuICAgIC8vICAgICB0aGlzLl91cGRhdGVGb3JtKDxGb3JtUm93PnRoaXMuZm9ybS5nZXRDaGlsZHJlbigpW2luZGV4XSk7XG4gICAgLy8gICB9XG4gICAgLy8gfVxuICB9XG5cbiAgbmdPbkRlc3Ryb3koKSB7XG4gICAgLy8gY29uc29sZS5sb2coXCJNeUZvcm1Db21wb25lbnQgZGVzdHJveWVkXCIpO1xuICB9XG59XG4iLCI8c3R5bGU+XG5cbiAgICBzZWN0aW9uLnJvdyB7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAvKiBtaW4taGVpZ2h0OiA0MHB4OyAqL1xuICAgICAgICBwYWRkaW5nOiAxMHB4IDA7XG4gICAgICAgIC8qIGJvcmRlcjogMXB4IHNvbGlkIGJsYWNrOyAqL1xuICAgIH1cblxuICAgIHNlY3Rpb24ucm93LWl0ZW0ge1xuICAgICAgICAvKiBoZWlnaHQ6IDEwMCU7ICovXG4gICAgICAgIC8qIGJhY2tncm91bmQtY29sb3I6IGxpZ2h0Ymx1ZTsgKi9cbiAgICB9XG5cbiAgICAub3ZlcmZsb3ctaGlkZGVuIHtcbiAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICB9XG5cbiAgICAudGV4dC1vdmVyZmxvdy1lbGxpcHNpcyB7XG4gICAgICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xuICAgIH1cblxuICAgIC53aGl0ZS1zcGFjZS1ub3dyYXAge1xuICAgICAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICAgIH1cblxuICAgIGZvcm0ge1xuICAgICAgICBwYWRkaW5nOiAxNXB4O1xuICAgICAgICAvKiBib3JkZXI6IDFweCBzb2xpZCBibGFjazsgKi9cbiAgICAgICAgLyogYm94LXNoYWRvdzogMCA1cHggNXB4IC0zcHggcmdiYSgwLDAsMCwuMiksIDAgOHB4IDEwcHggMXB4IHJnYmEoMCwwLDAsLjE0KSwgMCAzcHggMTRweCAycHggcmdiYSgwLDAsMCwuMTIpOyAqL1xuICAgIH1cblxuICAgIC50b3AtbWFyZ2luIHtcbiAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcbiAgICB9XG5cbiAgICAubm8tbWFyZ2luIHtcbiAgICAgICAgbWFyZ2luOiAwO1xuICAgIH1cblxuICAgIC5uby1wYWRkaW5nIHtcbiAgICAgICAgcGFkZGluZzogMDtcbiAgICB9XG5cbjwvc3R5bGU+XG5cbjxmb3JtXG4gICAgW2Zvcm1Hcm91cF09XCJ0aGlzLmZvcm0uZ2V0Rm9ybUdyb3VwKClcIlxuICAgICNuZ0Zvcm09XCJuZ0Zvcm1cIlxuICAgIGZ4TGF5b3V0PVwiY29sdW1uXCJcbiAgICBmeExheW91dEFsaWduPVwiY2VudGVyIGNlbnRlclwiXG4gICAgZnhMYXlvdXRHYXA9XCIxMHB4XCI+XG5cbiAgICAgICAgPHNlY3Rpb25cbiAgICAgICAgICAgIGNsYXNzPVwicm93IG92ZXJmbG93LWhpZGRlblwiXG4gICAgICAgICAgICAqbmdGb3I9XCJsZXQgcm93IG9mIHRoaXMuZm9ybS5nZXRDaGlsZHJlbigpXCJcbiAgICAgICAgICAgIFtmeExheW91dF09XCJ0aGlzLmRldmljZVNlcnZpY2UuaXNEZXZpY2VEZXNrdG9wKCkgPyAncm93JyA6ICdjb2x1bW4nXCJcbiAgICAgICAgICAgIFtmeExheW91dEFsaWduXT1cInRoaXMuZGV2aWNlU2VydmljZS5pc0RldmljZURlc2t0b3AoKSA/ICdjZW50ZXIgc3RhcnQnIDogJ2NlbnRlciBjZW50ZXInXCJcbiAgICAgICAgICAgIFtmeExheW91dEdhcF09XCJ0aGlzLmRldmljZVNlcnZpY2UuaXNEZXZpY2VEZXNrdG9wKCkgPyAnMjBweCcgOiAnMTBweCdcIj5cblxuICAgICAgICAgICAgICAgIDxzZWN0aW9uXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzPVwicm93LWl0ZW1cIlxuICAgICAgICAgICAgICAgICAgICAqbmdGb3I9XCJsZXQgcm93SXRlbSBvZiByb3cuZ2V0Q2hpbGRyZW4oKVwiXG4gICAgICAgICAgICAgICAgICAgIGZ4RmxleD5cblxuICAgICAgICAgICAgICAgICAgICAgICAgPCEtLSAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tIFJBRElPX0JVVFRPTl9HUk9VUCAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tIC0tPlxuICAgICAgICAgICAgICAgICAgICAgICAgPGFwcC1yYWRpby1idXR0b25cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBbZm9ybUNvbnRyb2xOYW1lXT1cInJvd0l0ZW0uZ2V0Q29udHJvbE5hbWUoKVwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKm5nSWY9XCJyb3dJdGVtLmdldFR5cGUoKSA9PSAnUkFESU9fQlVUVE9OX0dST1VQJ1wiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgW3JhZGlvQnV0dG9uR3JvdXBdPVwicm93SXRlbVwiPjwvYXBwLXJhZGlvLWJ1dHRvbj5cblxuICAgICAgICAgICAgICAgICAgICAgICAgPCEtLSAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tIENIRUNLQk9YIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0gLS0+XG4gICAgICAgICAgICAgICAgICAgICAgICA8YXBwLWNoZWNrYm94XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgW2Zvcm1Db250cm9sTmFtZV09XCJyb3dJdGVtLmdldENvbnRyb2xOYW1lKClcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICpuZ0lmPVwicm93SXRlbS5nZXRUeXBlKCkgPT0gJ0NIRUNLQk9YJ1wiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgW2NoZWNrYm94XT1cInJvd0l0ZW1cIj48L2FwcC1jaGVja2JveD5cblxuICAgICAgICAgICAgICAgICAgICAgICAgPCEtLSAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tIEZPUk1fRklFTERfSU5QVVQsIFRFWFQgLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSAtLT5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxhcHAtZm9ybS1maWVsZC1pbnB1dC10ZXh0XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgW2Zvcm1Db250cm9sTmFtZV09XCJyb3dJdGVtLmdldENvbnRyb2xOYW1lKClcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICpuZ0lmPVwicm93SXRlbS5nZXRUeXBlKCkgPT0gJ0ZPUk1fRklFTERfSU5QVVQnICYmIHRoaXMuZ2V0Rm9ybUZpZWxkSW5wdXRUeXBlKHJvd0l0ZW0pID09PSAndGV4dCdcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtmb3JtRmllbGRJbnB1dFRleHRdPVwicm93SXRlbVwiPjwvYXBwLWZvcm0tZmllbGQtaW5wdXQtdGV4dD5cblxuICAgICAgICAgICAgICAgICAgICAgICAgPCEtLSAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tIEZPUk1fRklFTERfSU5QVVQsIFBBU1NXT1JEIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0gLS0+XG4gICAgICAgICAgICAgICAgICAgICAgICA8YXBwLWZvcm0tZmllbGQtaW5wdXQtcGFzc3dvcmRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBbZm9ybUNvbnRyb2xOYW1lXT1cInJvd0l0ZW0uZ2V0Q29udHJvbE5hbWUoKVwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKm5nSWY9XCJyb3dJdGVtLmdldFR5cGUoKSA9PSAnRk9STV9GSUVMRF9JTlBVVCcgJiYgdGhpcy5nZXRGb3JtRmllbGRJbnB1dFR5cGUocm93SXRlbSkgPT09ICdwYXNzd29yZCdcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtmb3JtRmllbGRJbnB1dFBhc3N3b3JkXT1cInJvd0l0ZW1cIj48L2FwcC1mb3JtLWZpZWxkLWlucHV0LXBhc3N3b3JkPlxuXG4gICAgICAgICAgICAgICAgICAgICAgICA8IS0tIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0gRk9STV9GSUVMRF9JTlBVVCwgTlVNQkVSIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0gLS0+XG4gICAgICAgICAgICAgICAgICAgICAgICA8YXBwLWZvcm0tZmllbGQtaW5wdXQtbnVtYmVyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgW2Zvcm1Db250cm9sTmFtZV09XCJyb3dJdGVtLmdldENvbnRyb2xOYW1lKClcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICpuZ0lmPVwicm93SXRlbS5nZXRUeXBlKCkgPT0gJ0ZPUk1fRklFTERfSU5QVVQnICYmIHRoaXMuZ2V0Rm9ybUZpZWxkSW5wdXRUeXBlKHJvd0l0ZW0pID09PSAnbnVtYmVyJ1wiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgW2Zvcm1GaWVsZElucHV0TnVtYmVyXT1cInJvd0l0ZW1cIj48L2FwcC1mb3JtLWZpZWxkLWlucHV0LW51bWJlcj5cblxuICAgICAgICAgICAgICAgICAgICAgICAgPCEtLSAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tIEZPUk1fRklFTERfSU5QVVQsIEVNQUlMIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0gLS0+XG4gICAgICAgICAgICAgICAgICAgICAgICA8YXBwLWZvcm0tZmllbGQtaW5wdXQtZW1haWxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBbZm9ybUNvbnRyb2xOYW1lXT1cInJvd0l0ZW0uZ2V0Q29udHJvbE5hbWUoKVwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKm5nSWY9XCJyb3dJdGVtLmdldFR5cGUoKSA9PSAnRk9STV9GSUVMRF9JTlBVVCcgJiYgdGhpcy5nZXRGb3JtRmllbGRJbnB1dFR5cGUocm93SXRlbSkgPT09ICdlbWFpbCdcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtmb3JtRmllbGRJbnB1dEVtYWlsXT1cInJvd0l0ZW1cIj48L2FwcC1mb3JtLWZpZWxkLWlucHV0LWVtYWlsPlxuXG4gICAgICAgICAgICAgICAgICAgICAgICA8IS0tIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0gRk9STV9GSUVMRF9JTlBVVCwgRklMRSAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tIC0tPlxuICAgICAgICAgICAgICAgICAgICAgICAgPGFwcC1mb3JtLWZpZWxkLWlucHV0LWZpbGVcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBbZm9ybUNvbnRyb2xOYW1lXT1cInJvd0l0ZW0uZ2V0Q29udHJvbE5hbWUoKVwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKm5nSWY9XCJyb3dJdGVtLmdldFR5cGUoKSA9PSAnRk9STV9GSUVMRF9JTlBVVCcgJiYgdGhpcy5nZXRGb3JtRmllbGRJbnB1dFR5cGUocm93SXRlbSkgPT09ICdmaWxlJ1wiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgW2Zvcm1GaWVsZElucHV0RmlsZV09XCJyb3dJdGVtXCI+PC9hcHAtZm9ybS1maWVsZC1pbnB1dC1maWxlPlxuXG4gICAgICAgICAgICAgICAgICAgICAgICA8IS0tIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0gREFURVBJQ0tFUiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tIC0tPlxuICAgICAgICAgICAgICAgICAgICAgICAgPGFwcC1kYXRlcGlja2VyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgW2Zvcm1Db250cm9sTmFtZV09XCJyb3dJdGVtLmdldENvbnRyb2xOYW1lKClcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICpuZ0lmPVwicm93SXRlbS5nZXRUeXBlKCkgPT0gJ0RBVEVQSUNLRVInXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBbZGF0ZXBpY2tlcl09XCJyb3dJdGVtXCI+PC9hcHAtZGF0ZXBpY2tlcj5cblxuICAgICAgICAgICAgICAgICAgICAgICAgPGFwcC10aW1lcGlja2VyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgW2Zvcm1Db250cm9sTmFtZV09XCJyb3dJdGVtLmdldENvbnRyb2xOYW1lKClcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICpuZ0lmPVwicm93SXRlbS5nZXRUeXBlKCkgPT0gJ1RJTUVQSUNLRVInXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBbdGltZXBpY2tlcl09XCJyb3dJdGVtXCI+PC9hcHAtdGltZXBpY2tlcj5cblxuICAgICAgICAgICAgICAgICAgICAgICAgPGFwcC10ZXh0YXJlYVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtmb3JtQ29udHJvbE5hbWVdPVwicm93SXRlbS5nZXRDb250cm9sTmFtZSgpXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAqbmdJZj1cInJvd0l0ZW0uZ2V0VHlwZSgpID09ICdGT1JNX0ZJRUxEX1RFWFRBUkVBJ1wiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgW3RleHRhcmVhXT1cInJvd0l0ZW1cIj48L2FwcC10ZXh0YXJlYT5cblxuICAgICAgICAgICAgICAgICAgICAgICAgPGFwcC1zZWxlY3RcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBbZm9ybUNvbnRyb2xOYW1lXT1cInJvd0l0ZW0uZ2V0Q29udHJvbE5hbWUoKVwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKm5nSWY9XCJyb3dJdGVtLmdldFR5cGUoKSA9PSAnRk9STV9GSUVMRF9TRUxFQ1QnXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBbc2VsZWN0XT1cInJvd0l0ZW1cIj48L2FwcC1zZWxlY3Q+XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxhcHAtc2xpZGVyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgW2Zvcm1Db250cm9sTmFtZV09XCJyb3dJdGVtLmdldENvbnRyb2xOYW1lKClcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICpuZ0lmPVwicm93SXRlbS5nZXRUeXBlKCkgPT0gJ1NMSURFUidcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtzbGlkZXJdPVwicm93SXRlbVwiPjwvYXBwLXNsaWRlcj5cblxuICAgICAgICAgICAgICAgICAgICAgICAgPGFwcC1zbGlkZS10b2dnbGVcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBbZm9ybUNvbnRyb2xOYW1lXT1cInJvd0l0ZW0uZ2V0Q29udHJvbE5hbWUoKVwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKm5nSWY9XCJyb3dJdGVtLmdldFR5cGUoKSA9PSAnU0xJREVfVE9HR0xFJ1wiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgW3NsaWRlVG9nZ2xlXT1cInJvd0l0ZW1cIj48L2FwcC1zbGlkZS10b2dnbGU+XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxhcHAtYnV0dG9uXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgW2Zvcm1Db250cm9sTmFtZV09XCJyb3dJdGVtLmdldENvbnRyb2xOYW1lKClcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICpuZ0lmPVwicm93SXRlbS5nZXRUeXBlKCkgPT0gJ0JVVFRPTidcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtidXR0b25dPVwicm93SXRlbVwiPjwvYXBwLWJ1dHRvbj5cblxuICAgICAgICAgICAgICAgICAgICAgICAgPG5nLWNvbnRhaW5lclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICpuZ0lmPVwicm93SXRlbS5nZXRUeXBlKCkgPT0gJ0FVVE9DT01QTEVURScgJiYge2hpZGU6IHJvd0l0ZW0uZ2V0SGlkZGVuU3ViamVjdCgpIHwgYXN5bmN9IGFzIGF1dG9jb21wbGV0ZVN1YmplY3RzXCI+XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YXBwLWF1dG9jb21wbGV0ZVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbZm9ybUNvbnRyb2xOYW1lXT1cInJvd0l0ZW0uZ2V0Q29udHJvbE5hbWUoKVwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICpuZ0lmPVwiIWF1dG9jb21wbGV0ZVN1YmplY3RzLmhpZGVcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbYXV0b2NvbXBsZXRlXT1cInJvd0l0ZW1cIj48L2FwcC1hdXRvY29tcGxldGU+XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8IS0tIC0tLS0tLS0tLS0tLS0tLS0tLSBHcmVza2UgLS0tLS0tLS0tLS0tLS0tLS0tIC0tPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzZWN0aW9uXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZ4TGF5b3V0PVwiY29sdW1uXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZnhMYXlvdXRBbGlnbj1cImNlbnRlciBjZW50ZXJcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzcz1cInRvcC1tYXJnaW5cIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAqbmdJZj1cIiFhdXRvY29tcGxldGVTdWJqZWN0cy5oaWRlICYmICgoIXRoaXMuZm9ybS5nZXRGb3JtR3JvdXAoKS5nZXQocm93SXRlbS5nZXRDb250cm9sTmFtZSgpKS5wcmlzdGluZSAmJiB0aGlzLmhhc0Zvcm1Db250cm9sRXJyb3Iocm93SXRlbS5nZXRDb250cm9sTmFtZSgpKSkgfHwgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKG5nRm9ybS5zdWJtaXR0ZWQgJiYgdGhpcy5oYXNGb3JtQ29udHJvbEVycm9yKHJvd0l0ZW0uZ2V0Q29udHJvbE5hbWUoKSkpKVwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPG1hdC1lcnJvclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICpuZ0lmPVwidGhpcy5nZXRGaXJzdEV4aXN0aW5nQ29udHJvbEVycm9yKHJvd0l0ZW0pXCI+e3t0aGlzLmdldEZpcnN0RXhpc3RpbmdDb250cm9sRXJyb3Iocm93SXRlbSkubWVzc2FnZX19PC9tYXQtZXJyb3I+XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3NlY3Rpb24+XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvbmctY29udGFpbmVyPlxuXG4gICAgICAgICAgICAgICAgICAgICAgICA8IS0tIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0gT1RIRVIgLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSAtLT5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxwXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKm5nSWY9XCIhKHJvd0l0ZW0uZ2V0VHlwZSgpID09ICdSQURJT19CVVRUT05fR1JPVVAnIHx8IFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcm93SXRlbS5nZXRUeXBlKCkgPT0gJ0NIRUNLQk9YJyB8fCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJvd0l0ZW0uZ2V0VHlwZSgpID09ICdGT1JNX0ZJRUxEX0lOUFVUJyB8fFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcm93SXRlbS5nZXRUeXBlKCkgPT0gJ0RBVEVQSUNLRVInIHx8XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByb3dJdGVtLmdldFR5cGUoKSA9PSAnRk9STV9GSUVMRF9URVhUQVJFQScgfHxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJvd0l0ZW0uZ2V0VHlwZSgpID09ICdGT1JNX0ZJRUxEX1NFTEVDVCcgfHxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJvd0l0ZW0uZ2V0VHlwZSgpID09ICdTTElERVInIHx8XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByb3dJdGVtLmdldFR5cGUoKSA9PSAnU0xJREVfVE9HR0xFJyB8fFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcm93SXRlbS5nZXRUeXBlKCkgPT0gJ0JVVFRPTicgfHxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJvd0l0ZW0uZ2V0VHlwZSgpID09ICdBVVRPQ09NUExFVEUnIHx8XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByb3dJdGVtLmdldFR5cGUoKSA9PSAnVElNRVBJQ0tFUicpXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzcz1cIm5vLW1hcmdpblwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZnhMYXlvdXQ9XCJjb2x1bW5cIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZ4TGF5b3V0QWxpZ249XCJjZW50ZXIgY2VudGVyXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNzc3Nzc3NzXG4gICAgICAgICAgICAgICAgICAgICAgICA8L3A+XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIDwhLS0gLS0tLS0tLS0tLS0tLS0tLS0tIEdyZXNrZSAtLS0tLS0tLS0tLS0tLS0tLS0gLS0+XG4gICAgICAgICAgICAgICAgICAgICAgICA8c2VjdGlvblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZ4TGF5b3V0PVwiY29sdW1uXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBmeExheW91dEFsaWduPVwiY2VudGVyIGNlbnRlclwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3M9XCJ0b3AtbWFyZ2luXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAqbmdJZj1cInJvd0l0ZW0uZ2V0VHlwZSgpICE9ICdBVVRPQ09NUExFVEUnICYmICgodGhpcy5mb3JtLmdldEZvcm1Hcm91cCgpLmdldChyb3dJdGVtLmdldENvbnRyb2xOYW1lKCkpICYmICF0aGlzLmZvcm0uZ2V0Rm9ybUdyb3VwKCkuZ2V0KHJvd0l0ZW0uZ2V0Q29udHJvbE5hbWUoKSkucHJpc3RpbmUgJiYgdGhpcy5oYXNGb3JtQ29udHJvbEVycm9yKHJvd0l0ZW0uZ2V0Q29udHJvbE5hbWUoKSkpIHx8IFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKG5nRm9ybS5zdWJtaXR0ZWQgJiYgdGhpcy5oYXNGb3JtQ29udHJvbEVycm9yKHJvd0l0ZW0uZ2V0Q29udHJvbE5hbWUoKSkpKVwiPlxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwhLS0gdG8gc2hvdyBhbGwgZXJyb3JzIC0tPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8IS0tIDxzZWN0aW9uXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAqbmdGb3I9XCJsZXQgdmFsaWRhdG9yIG9mIHJvd0l0ZW0uZ2V0VmFsaWRhdG9ycygpXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPG1hdC1lcnJvclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAqbmdJZj1cInRoaXMuZm9ybS5nZXRGb3JtR3JvdXAoKS5nZXQocm93SXRlbS5nZXRDb250cm9sTmFtZSgpKS5nZXRFcnJvcih2YWxpZGF0b3IubmFtZSlcIj57eyB2YWxpZGF0b3IubWVzc2FnZSB9fTwvbWF0LWVycm9yPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3NlY3Rpb24+IC0tPlxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwhLS0gdG8gc2hvdyBvbmx5IG9uZSBlcnJvciBhdCB0aGUgdGltZSAtLT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPG1hdC1lcnJvclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKm5nSWY9XCJ0aGlzLmdldEZpcnN0RXhpc3RpbmdDb250cm9sRXJyb3Iocm93SXRlbSlcIj57e3RoaXMuZ2V0Rmlyc3RFeGlzdGluZ0NvbnRyb2xFcnJvcihyb3dJdGVtKS5tZXNzYWdlfX08L21hdC1lcnJvcj5cblxuICAgICAgICAgICAgICAgICAgICAgICAgPC9zZWN0aW9uPlxuICAgICAgICAgICAgICAgICAgICAgICAgPCEtLSA8aDE+e3sgdGhpcy5mb3JtLmdldEZvcm1Hcm91cCgpLmdldChyb3dJdGVtLmdldENvbnRyb2xOYW1lKCkpLnByaXN0aW5lIH19PC9oMT4gLS0+XG4gICAgICAgICAgICAgICAgICAgICAgICA8IS0tIDxoMj57e3Jvd0l0ZW0uZ2V0VmFsaWRhdG9yRm5zKClbMF0/Lm5hbWV9fTwvaDI+IC0tPlxuXG4gICAgICAgICAgICAgICAgPC9zZWN0aW9uPlxuXG4gICAgICAgIDwvc2VjdGlvbj5cblxuICAgICAgICA8c2VjdGlvblxuICAgICAgICAgICAgZnhMYXlvdXRBbGlnbj1cImNlbnRlciBjZW50ZXJcIlxuICAgICAgICAgICAgZnhMYXlvdXRHYXA9XCIydndcIj5cblxuICAgICAgICAgICAgICAgIDxidXR0b25cbiAgICAgICAgICAgICAgICAgICAgKm5nSWY9XCJ0aGlzLmZvcm0uc2hvd1N1Ym1pdEJ1dHRvblwiXG4gICAgICAgICAgICAgICAgICAgIHR5cGU9XCJidXR0b25cIlxuICAgICAgICAgICAgICAgICAgICAoY2xpY2spPVwidGhpcy5mb3JtLnN1Ym1pdE9ubHlJZkZvcm1WYWxpZCA/IHRoaXMuZm9ybS5nZXRGb3JtR3JvdXAoKS52YWxpZCAmJiB0aGlzLm9uU3VibWl0SGFwcGVuZWQoKSA6IHRoaXMub25TdWJtaXRIYXBwZW5lZCgpXCJcbiAgICAgICAgICAgICAgICAgICAgbWF0LXJhaXNlZC1idXR0b25cbiAgICAgICAgICAgICAgICAgICAgW2Rpc2FibGVkXT1cInRoaXMuZm9ybS5zdWJtaXRPbmx5SWZGb3JtVmFsaWQgPyAhdGhpcy5mb3JtLmdldEZvcm1Hcm91cCgpLnZhbGlkIDogZmFsc2VcIj57e3RoaXMuZm9ybS5zdWJtaXRCdXR0b25UZXh0fX08L2J1dHRvbj5cblxuICAgICAgICAgICAgICAgIDxidXR0b25cbiAgICAgICAgICAgICAgICAgICAgKm5nSWY9XCJ0aGlzLmZvcm0uc2hvd0NhbmNlbEJ1dHRvblwiXG4gICAgICAgICAgICAgICAgICAgIHR5cGU9XCJidXR0b25cIlxuICAgICAgICAgICAgICAgICAgICAoY2xpY2spPVwidGhpcy5vbkNhbmNlbEhhcHBlbmVkKClcIlxuICAgICAgICAgICAgICAgICAgICBtYXQtcmFpc2VkLWJ1dHRvbj57e3RoaXMuZm9ybS5jYW5jZWxCdXR0b25UZXh0fX08L2J1dHRvbj5cblxuICAgICAgICAgICAgICAgIDxidXR0b25cbiAgICAgICAgICAgICAgICAgICAgKm5nSWY9XCJ0aGlzLmZvcm0uc2hvd1Jlc2V0QnV0dG9uXCJcbiAgICAgICAgICAgICAgICAgICAgdHlwZT1cInJlc2V0XCJcbiAgICAgICAgICAgICAgICAgICAgbWF0LXJhaXNlZC1idXR0b24+e3t0aGlzLmZvcm0ucmVzZXRCdXR0b25UZXh0fX08L2J1dHRvbj5cblxuICAgICAgICA8L3NlY3Rpb24+XG5cbjwvZm9ybT4iXX0=