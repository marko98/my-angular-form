import { OnInit, OnDestroy, EventEmitter, SimpleChanges, OnChanges, DoCheck } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Form } from './shared/model/structural/composite/form/form.model';
import { FormRowItem, FormRowItemValidatorInterface } from './shared/model/structural/composite/form/form-row-item.model';
import { FORM_FIELD_INPUT_TYPE } from './shared/model/structural/composite/form/form-field-input/form-field-input.model';
import { DeviceService } from './shared/service/device.service';
import * as i0 from "@angular/core";
export declare class MyAngularFormComponent implements OnInit, OnDestroy, OnChanges, DoCheck {
    deviceService: DeviceService;
    onSubmit: EventEmitter<FormGroup>;
    onCancel: EventEmitter<any>;
    form: Form;
    constructor(deviceService: DeviceService);
    onSubmitHappened: () => void;
    onCancelHappened: () => void;
    hasFormControlError: (formControlName: string) => boolean;
    getFirstExistingControlError: (rowItem: FormRowItem) => FormRowItemValidatorInterface;
    getFormFieldInputType: (rowItem: FormRowItem) => FORM_FIELD_INPUT_TYPE;
    ngOnInit(): void;
    ngOnChanges(changes: SimpleChanges): void;
    ngDoCheck(): void;
    ngOnDestroy(): void;
    static ɵfac: i0.ɵɵFactoryDef<MyAngularFormComponent, never>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<MyAngularFormComponent, "lib-my-angular-form", never, { "form": "form"; }, { "onSubmit": "onSubmit"; "onCancel": "onCancel"; }, never, never>;
}
