import { OnInit, OnDestroy } from "@angular/core";
import { FormRowItemValidatorInterface } from "../../model/structural/composite/form/form-row-item.model";
import { Textarea } from "../../model/structural/composite/form/textarea/textarea.model";
import * as i0 from "@angular/core";
export declare class TextareaComponent implements OnInit, OnDestroy {
    textarea: Textarea;
    onChange: any;
    onTouched: any;
    constructor();
    onKeyUp: (textarea: HTMLInputElement) => void;
    registerOnChange(fn: any): void;
    registerOnTouched(fn: any): void;
    writeValue(value: any): void;
    getMinLengthValidator: () => FormRowItemValidatorInterface;
    getMaxLengthValidator: () => FormRowItemValidatorInterface;
    ngOnInit(): void;
    ngOnDestroy(): void;
    static ɵfac: i0.ɵɵFactoryDef<TextareaComponent, never>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<TextareaComponent, "app-textarea", never, { "textarea": "textarea"; }, {}, never, never>;
}
