import { OnInit, OnDestroy } from "@angular/core";
import { FormFieldInputPassword } from "../../../model/structural/composite/form/form-field-input/form-field-input-password.model";
import { FormRowItemValidatorInterface } from "../../../model/structural/composite/form/form-row-item.model";
import * as i0 from "@angular/core";
export declare class FormFieldInputPasswordComponent implements OnInit, OnDestroy {
    formFieldInputPassword: FormFieldInputPassword;
    onChange: any;
    onTouched: any;
    constructor();
    onKeyUp: (input: HTMLInputElement) => void;
    registerOnChange(fn: any): void;
    registerOnTouched(fn: any): void;
    writeValue(value: any): void;
    getMinLengthValidator: () => FormRowItemValidatorInterface;
    getMaxLengthValidator: () => FormRowItemValidatorInterface;
    ngOnInit(): void;
    ngOnDestroy(): void;
    static ɵfac: i0.ɵɵFactoryDef<FormFieldInputPasswordComponent, never>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<FormFieldInputPasswordComponent, "app-form-field-input-password", never, { "formFieldInputPassword": "formFieldInputPassword"; }, {}, never, never>;
}
