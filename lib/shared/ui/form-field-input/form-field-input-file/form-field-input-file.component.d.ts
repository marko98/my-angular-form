import { OnInit, OnDestroy, AfterViewInit, ElementRef, Renderer2 } from '@angular/core';
import { MatListOption } from '@angular/material/list';
import { FormFieldInputFile } from '../../../model/structural/composite/form/form-field-input/form-field-input-file.model';
import { DeviceService } from '../../../service/device.service';
import { UiService } from '../../../service/ui.service';
import * as i0 from "@angular/core";
export declare class FormFieldInputFileComponent implements OnInit, OnDestroy, AfterViewInit {
    private renderer;
    deviceService: DeviceService;
    private uiService;
    appDragDrop: ElementRef;
    file: ElementRef;
    formFieldInputFile: FormFieldInputFile;
    onChange: any;
    onTouched: any;
    constructor(renderer: Renderer2, deviceService: DeviceService, uiService: UiService);
    registerOnChange(fn: any): void;
    registerOnTouched(fn: any): void;
    writeValue(value: any): void;
    onFilesAdded: () => void;
    onFileDropped: (fileList: FileList) => void;
    addFiles: () => void;
    onRemoveFiles: (matListOptions: MatListOption[]) => void;
    ngOnInit(): void;
    ngOnDestroy(): void;
    ngAfterViewInit(): void;
    static ɵfac: i0.ɵɵFactoryDef<FormFieldInputFileComponent, never>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<FormFieldInputFileComponent, "app-form-field-input-file", never, { "formFieldInputFile": "formFieldInputFile"; }, {}, never, never>;
}
