import { OnInit } from "@angular/core";
import { FormFieldInput } from "../../../model/structural/composite/form/form-field-input/form-field-input.model";
import * as i0 from "@angular/core";
export declare class FormFieldInputEmailComponent implements OnInit {
    formFieldInputEmail: FormFieldInput;
    onChange: any;
    onTouched: any;
    constructor();
    onKeyUp: (input: HTMLInputElement) => void;
    registerOnChange(fn: any): void;
    registerOnTouched(fn: any): void;
    writeValue(value: any): void;
    ngOnInit(): void;
    ngOnDestroy(): void;
    static ɵfac: i0.ɵɵFactoryDef<FormFieldInputEmailComponent, never>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<FormFieldInputEmailComponent, "app-form-field-input-email", never, { "formFieldInputEmail": "formFieldInputEmail"; }, {}, never, never>;
}
