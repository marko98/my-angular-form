import { OnInit, OnDestroy } from "@angular/core";
import { FormFieldInputNumber } from "../../../model/structural/composite/form/form-field-input/form-field-input-number.model";
import * as i0 from "@angular/core";
export declare class FormFieldInputNumberComponent implements OnInit, OnDestroy {
    formFieldInputNumber: FormFieldInputNumber;
    onChange: any;
    onTouched: any;
    constructor();
    onKeyUp: (input: HTMLInputElement) => void;
    registerOnChange(fn: any): void;
    registerOnTouched(fn: any): void;
    writeValue(value: any): void;
    ngOnInit(): void;
    ngOnDestroy(): void;
    static ɵfac: i0.ɵɵFactoryDef<FormFieldInputNumberComponent, never>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<FormFieldInputNumberComponent, "app-form-field-input-number", never, { "formFieldInputNumber": "formFieldInputNumber"; }, {}, never, never>;
}
