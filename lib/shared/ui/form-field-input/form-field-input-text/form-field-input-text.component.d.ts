import { OnInit, OnDestroy } from "@angular/core";
import { FormFieldInputText } from "../../../model/structural/composite/form/form-field-input/form-field-input-text.model";
import { FormRowItemValidatorInterface } from "../../../model/structural/composite/form/form-row-item.model";
import * as i0 from "@angular/core";
export declare class FormFieldInputTextComponent implements OnInit, OnDestroy {
    formFieldInputText: FormFieldInputText;
    onChange: any;
    onTouched: any;
    constructor();
    onKeyUp: (input: HTMLInputElement) => void;
    registerOnChange(fn: any): void;
    registerOnTouched(fn: any): void;
    writeValue(value: any): void;
    getMinLengthValidator: () => FormRowItemValidatorInterface;
    getMaxLengthValidator: () => FormRowItemValidatorInterface;
    ngOnInit(): void;
    ngOnDestroy(): void;
    static ɵfac: i0.ɵɵFactoryDef<FormFieldInputTextComponent, never>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<FormFieldInputTextComponent, "app-form-field-input-text", never, { "formFieldInputText": "formFieldInputText"; }, {}, never, never>;
}
