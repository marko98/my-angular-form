import { OnInit, OnDestroy } from '@angular/core';
import { ControlValueAccessor } from '@angular/forms';
import { Checkbox } from '../../model/structural/composite/form/checkbox/checkbox.model';
import * as i0 from "@angular/core";
export declare class CheckboxComponent implements OnInit, OnDestroy, ControlValueAccessor {
    checkbox: Checkbox;
    onChange: any;
    onTouched: any;
    constructor();
    onClick: () => void;
    registerOnChange(fn: any): void;
    registerOnTouched(fn: any): void;
    writeValue(value: any): void;
    ngOnInit(): void;
    ngOnDestroy(): void;
    static ɵfac: i0.ɵɵFactoryDef<CheckboxComponent, never>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<CheckboxComponent, "app-checkbox", never, { "checkbox": "checkbox"; }, {}, never, never>;
}
