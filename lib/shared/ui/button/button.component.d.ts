import { OnInit } from '@angular/core';
import { Button } from '../../model/structural/composite/form/button/button.model';
import * as i0 from "@angular/core";
export declare class ButtonComponent implements OnInit {
    button: Button;
    onChange: any;
    onTouched: any;
    constructor();
    registerOnChange(fn: any): void;
    registerOnTouched(fn: any): void;
    writeValue(value: any): void;
    ngOnInit(): void;
    ngOnDestroy(): void;
    static ɵfac: i0.ɵɵFactoryDef<ButtonComponent, never>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<ButtonComponent, "app-button", never, { "button": "button"; }, {}, never, never>;
}
