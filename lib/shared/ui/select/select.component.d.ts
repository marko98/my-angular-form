import { OnInit, OnDestroy } from '@angular/core';
import { Select } from '../../model/structural/composite/form/select/select.model';
import { MatSelect } from '@angular/material/select';
import * as i0 from "@angular/core";
export declare class SelectComponent implements OnInit, OnDestroy {
    select: Select;
    onChange: any;
    onTouched: any;
    constructor();
    onSelectionChange: (select: MatSelect) => void;
    onBlur: () => void;
    registerOnChange(fn: any): void;
    registerOnTouched(fn: any): void;
    writeValue(value: any): void;
    ngOnInit(): void;
    ngOnDestroy(): void;
    static ɵfac: i0.ɵɵFactoryDef<SelectComponent, never>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<SelectComponent, "app-select", never, { "select": "select"; }, {}, never, never>;
}
