import { OnInit, OnDestroy } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { UploadService } from '../../service/upload.service';
import * as i0 from "@angular/core";
export declare class UploadDialogComponent implements OnInit, OnDestroy {
    dialogRef: MatDialogRef<UploadDialogComponent>;
    uploadService: UploadService;
    file: any;
    files: Set<File>;
    constructor(dialogRef: MatDialogRef<UploadDialogComponent>, uploadService: UploadService);
    progress: any;
    canBeClosed: boolean;
    primaryButtonText: string;
    showCancelButton: boolean;
    uploading: boolean;
    uploadSuccessful: boolean;
    onFilesAdded(): void;
    addFiles(): void;
    closeDialog(): void;
    ngOnInit(): void;
    ngOnDestroy(): void;
    static ɵfac: i0.ɵɵFactoryDef<UploadDialogComponent, never>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<UploadDialogComponent, "app-upload-dialog", never, {}, {}, never, never>;
}
