import { OnInit, OnDestroy } from '@angular/core';
import { Timepicker } from '../../model/structural/composite/form/timepicker/timepicker.model';
import * as i0 from "@angular/core";
export declare class TimepickerComponent implements OnInit, OnDestroy {
    timepicker: Timepicker;
    onChange: any;
    onTouched: any;
    constructor();
    onTimeChanged: (time: string) => void;
    onTimepickerClosed: () => void;
    registerOnChange(fn: any): void;
    registerOnTouched(fn: any): void;
    writeValue(value: any): void;
    ngOnInit(): void;
    ngOnDestroy(): void;
    static ɵfac: i0.ɵɵFactoryDef<TimepickerComponent, never>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<TimepickerComponent, "app-timepicker", never, { "timepicker": "timepicker"; }, {}, never, never>;
}
