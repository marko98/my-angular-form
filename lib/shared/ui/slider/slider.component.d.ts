import { OnInit, OnDestroy } from '@angular/core';
import { Slider } from '../../model/structural/composite/form/slider/slider.model';
import { MatSlider } from '@angular/material/slider';
import * as i0 from "@angular/core";
export declare class SliderComponent implements OnInit, OnDestroy {
    slider: Slider;
    onChange: any;
    onTouched: any;
    constructor();
    onValueChange: (slider: MatSlider) => void;
    onBlur: () => void;
    registerOnChange(fn: any): void;
    registerOnTouched(fn: any): void;
    writeValue(value: any): void;
    ngOnInit(): void;
    ngOnDestroy(): void;
    static ɵfac: i0.ɵɵFactoryDef<SliderComponent, never>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<SliderComponent, "app-slider", never, { "slider": "slider"; }, {}, never, never>;
}
