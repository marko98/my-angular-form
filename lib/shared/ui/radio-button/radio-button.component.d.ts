import { OnInit, OnDestroy } from '@angular/core';
import { ControlValueAccessor } from '@angular/forms';
import { RadioButtonGroup } from '../../model/structural/composite/form/radio-button-group/radio-button-group.model';
import * as i0 from "@angular/core";
export declare class RadioButtonComponent implements OnInit, OnDestroy, ControlValueAccessor {
    radioButtonGroup: RadioButtonGroup;
    value: string | number | boolean;
    onChange: any;
    onTouched: any;
    constructor();
    onNgModelChange: () => void;
    registerOnChange(fn: any): void;
    registerOnTouched(fn: any): void;
    writeValue(value: any): void;
    ngOnInit(): void;
    ngOnDestroy(): void;
    static ɵfac: i0.ɵɵFactoryDef<RadioButtonComponent, never>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<RadioButtonComponent, "app-radio-button", never, { "radioButtonGroup": "radioButtonGroup"; }, {}, never, never>;
}
