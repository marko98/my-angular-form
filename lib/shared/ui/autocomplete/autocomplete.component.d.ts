import { OnInit, OnDestroy } from '@angular/core';
import { Autocomplete } from '../../model/structural/composite/form/autocomplete/autocomplete.model';
import { AutocompleteInterface } from 'autocomplete';
import * as i0 from "@angular/core";
export declare class AutocompleteComponent implements OnInit, OnDestroy {
    autocomplete: Autocomplete;
    onChange: any;
    onTouched: any;
    autocompleteInterface: AutocompleteInterface;
    constructor();
    registerOnChange(fn: any): void;
    registerOnTouched(fn: any): void;
    writeValue(value: any): void;
    ngOnInit(): void;
    ngOnDestroy(): void;
    static ɵfac: i0.ɵɵFactoryDef<AutocompleteComponent, never>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<AutocompleteComponent, "app-autocomplete", never, { "autocomplete": "autocomplete"; }, {}, never, never>;
}
