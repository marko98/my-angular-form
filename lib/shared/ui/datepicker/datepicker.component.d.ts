import { OnInit, OnDestroy } from '@angular/core';
import { Datepicker } from '../../model/structural/composite/form/datepicker/datepicker.model';
import { DeviceService } from '../../service/device.service';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { Moment } from 'moment';
import * as i0 from "@angular/core";
export declare const MY_FORMATS: {
    parse: {
        dateInput: string;
    };
    display: {
        dateInput: string;
        monthYearLabel: string;
        dateA11yLabel: string;
        monthYearA11yLabel: string;
    };
};
export declare class DatepickerComponent implements OnInit, OnDestroy {
    deviceService: DeviceService;
    datepicker: Datepicker;
    onChange: any;
    onTouched: any;
    private _prethodniSati;
    private _prethodniMinuti;
    constructor(deviceService: DeviceService);
    onBlur: () => void;
    addEvent(type: string, event: MatDatepickerInputEvent<Date>): void;
    disableDays: (d: Moment) => boolean;
    onTimeChanged: (time: string) => void;
    registerOnChange(fn: any): void;
    registerOnTouched(fn: any): void;
    writeValue(value: any): void;
    ngOnInit(): void;
    ngOnDestroy(): void;
    static ɵfac: i0.ɵɵFactoryDef<DatepickerComponent, never>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<DatepickerComponent, "app-datepicker", never, { "datepicker": "datepicker"; }, {}, never, never>;
}
