import { OnInit, OnDestroy } from '@angular/core';
import { MatSlideToggle } from '@angular/material/slide-toggle';
import { SlideToggle } from '../../model/structural/composite/form/slide-toggle/slide-toggle.model';
import * as i0 from "@angular/core";
export declare class SlideToggleComponent implements OnInit, OnDestroy {
    slideToggle: SlideToggle;
    onChange: any;
    onTouched: any;
    constructor();
    onValueChange: (slideToggle: MatSlideToggle) => void;
    registerOnChange(fn: any): void;
    registerOnTouched(fn: any): void;
    writeValue(value: any): void;
    ngOnInit(): void;
    ngOnDestroy(): void;
    static ɵfac: i0.ɵɵFactoryDef<SlideToggleComponent, never>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<SlideToggleComponent, "app-slide-toggle", never, { "slideToggle": "slideToggle"; }, {}, never, never>;
}
