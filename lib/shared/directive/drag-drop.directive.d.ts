import { EventEmitter } from '@angular/core';
import * as i0 from "@angular/core";
export declare class DragDropDirective {
    onFileDropped: EventEmitter<any>;
    private opacity;
    onDragOver(evt: any): void;
    onDragLeave(evt: any): void;
    onDrop(evt: any): void;
    static ɵfac: i0.ɵɵFactoryDef<DragDropDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDefWithMeta<DragDropDirective, "[appDragDrop]", never, {}, { "onFileDropped": "onFileDropped"; }, never>;
}
