export declare enum DISCRETE_TYPES {
    APPLICATION = "application",
    AUDIO = "audio",
    FONT = "font",
    IMAGE = "image",
    TEXT = "text",
    VIDEO = "video"
}
export declare enum MULTIPART_TYPES {
    MESSAGE = "message",
    MULTIPART = "multipart"
}
export declare enum APPLICATION_TYPES {
    APPLICATION_PDF = "application/pdf"
}
export declare enum TEXT_TYPES {
    TEXT_PLAIN = "text/plain",
    TEXT_CSS = "text/css",
    TEXT_HTML = "text/html",
    TEXT_JAVASCRIPT = "text/javascript",
    TEXT_XML = "text/xml"
}
export declare enum AUDIO_TYPES {
    AUDIO_WAV = "audio/wav",
    AUDIO_MP3 = "audio/mp3",
    AUDIO_FLAC = "audio/flac"
}
export declare enum IMAGE_TYPES {
    JPEG = "image/jpeg",
    PNG = "image/png",
    SVG = "image/svg+xml"
}
