import { MatSnackBar } from "@angular/material/snack-bar";
import * as i0 from "@angular/core";
export declare class UiService {
    private snackBar;
    constructor(snackBar: MatSnackBar);
    onShowSnackBar: (message: string, action: any, duration: number) => void;
    static ɵfac: i0.ɵɵFactoryDef<UiService, never>;
    static ɵprov: i0.ɵɵInjectableDef<UiService>;
}
