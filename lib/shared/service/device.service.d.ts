import { DeviceDetectorService, DeviceInfo } from 'ngx-device-detector';
import * as i0 from "@angular/core";
export declare class DeviceService {
    private deviceService;
    constructor(deviceService: DeviceDetectorService);
    isDeviceMobile: () => boolean;
    isDeviceTablet: () => boolean;
    isDeviceDesktop: () => boolean;
    getDeviceInfo: () => DeviceInfo;
    static ɵfac: i0.ɵɵFactoryDef<DeviceService, never>;
    static ɵprov: i0.ɵɵInjectableDef<DeviceService>;
}
