import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as i0 from "@angular/core";
export declare class UploadService {
    private http;
    constructor(http: HttpClient);
    upload(files: Set<File>): {
        [key: string]: {
            progress: Observable<number>;
        };
    };
    static ɵfac: i0.ɵɵFactoryDef<UploadService, never>;
    static ɵprov: i0.ɵɵInjectableDef<UploadService>;
}
