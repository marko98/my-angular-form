export declare const TEXT_ONLY_REGEX: RegExp;
export declare const NUMBERS_ONLY_REGEX: RegExp;
export declare const AT_LEAST_ONE_DIGIT_REGEX: RegExp;
export declare const AT_LEAST_ONE_UPPER_CASE_REGEX: RegExp;
export declare const AT_LEAST_ONE_LOWER_CASE_REGEX: RegExp;
export declare const AT_LEAST_ONE_SPECIAL_CHARACTER_REGEX: RegExp;
export declare const AT_LEAST_ONE_LOWER_CASE_CHARACTER_ONE_UPPER_CASE_CHARACTER_ONE_DIGIT_ONE_SPECIAL_CHARACTER_REGEX: RegExp;
export declare const EMAIL_REGEX: RegExp;
