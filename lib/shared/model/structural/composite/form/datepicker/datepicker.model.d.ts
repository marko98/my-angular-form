import { FormRowItem, FormRowItemInterface } from '../form-row-item.model';
import { MAT_COLOR } from '../../../../../material.module';
export declare interface DatepickerInterface extends FormRowItemInterface {
    min?: Date;
    max?: Date;
    startAt?: Date;
    startView?: 'month' | 'year' | 'multi-year';
    color?: MAT_COLOR;
    toggleSideSuffix?: boolean;
    matIcon?: string;
    labelName: string;
    defaultValue: Date;
    appearance?: 'legacy' | 'standard' | 'fill' | 'outline';
    disabledDays?: DAY[];
    timepicker?: TimepickerInsideDatepickerInterface;
}
export declare interface TimepickerInsideDatepickerInterface {
    min?: string;
    max?: string;
    labelName?: string;
    buttonAlign?: 'left' | 'right';
    disabled?: boolean;
    defaultValue?: string;
}
export declare enum DAY {
    MONDAY = 1,
    TUESDAY = 2,
    WEDNESDAY = 3,
    THURSDAY = 4,
    FRIDAY = 5,
    SATURDAY = 6,
    SUNDAY = 0
}
export declare class Datepicker extends FormRowItem {
    private min?;
    private max?;
    private startAt?;
    private startView;
    private color;
    private toggleSideSuffix;
    private labelName;
    private matIcon?;
    private appearance;
    private disabledDays;
    private timepickerInsideDatepickerInterface;
    getAppearance: () => import("@angular/material/form-field").MatFormFieldAppearance;
    setAppearance: (appearance: import("@angular/material/form-field").MatFormFieldAppearance) => void;
    getDisabledDays: () => DAY[];
    setDisabledDays: (disabledDays: DAY[]) => void;
    getMatIcon: () => string;
    setMatIcon: (matIcon: string) => void;
    getLabelName: () => string;
    setLabelName: (labelName: string) => void;
    getToggleSideSuffix: () => boolean;
    setToggleSideSuffix: (toggleSideSuffix: boolean) => void;
    getColor: () => MAT_COLOR;
    setColor: (color: MAT_COLOR) => void;
    getStartView: () => import("@angular/material/datepicker").MatCalendarView;
    setStartView: (startView: import("@angular/material/datepicker").MatCalendarView) => void;
    getStartAt: () => Date;
    setStartAt: (startAt: Date) => void;
    getMin: () => Date;
    setMin: (min: Date) => void;
    getMax: () => Date;
    setMax: (max: Date) => void;
    getTimepickerInsideDatepickerInterface: () => TimepickerInsideDatepickerInterface;
    setTimepickerInsideDatepickerInterface: (timepickerInsideDatepickerInterface: TimepickerInsideDatepickerInterface) => void;
}
