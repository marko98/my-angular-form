import { FormRowItem, FormRowItemInterface } from '../form-row-item.model';
import { PageDataSource, Entitet, ENTITET_MODEL_INTERFACE, AutocompleteInterface, OptionItem } from 'autocomplete';
import { Subject, BehaviorSubject } from 'rxjs';
export declare interface MyAutocompleteInterface extends FormRowItemInterface, AutocompleteInterface {
}
export declare class Autocomplete extends FormRowItem {
    private _sortingOptions;
    private _pageDataSource;
    private _inputPlaceholder;
    private _criteria;
    private _appearance;
    private _valueSubject;
    private _value;
    private _hiddenSubject;
    constructor();
    getSortingOptions: () => string[];
    setSortingOptions: (sortingOptions: string[]) => boolean;
    getPageDataSource: () => PageDataSource<Entitet<any, any>, any, ENTITET_MODEL_INTERFACE>;
    setPageDataSource: (pageDataSource: PageDataSource<Entitet<any, any>, any, ENTITET_MODEL_INTERFACE>) => boolean;
    getInputPlaceholder: () => string;
    setInputPlaceholder: (inputPlaceholder: string) => boolean;
    getCriteria: () => string;
    setCriteria: (criteria: string) => boolean;
    getAppearance: () => import("@angular/material/form-field").MatFormFieldAppearance;
    setAppearance: (appearance: import("@angular/material/form-field").MatFormFieldAppearance) => boolean;
    getValueSubject: () => Subject<OptionItem>;
    getValue: () => OptionItem;
    setValue: (value: OptionItem) => boolean;
    getHiddenSubject: () => BehaviorSubject<boolean>;
    setHiddenSubject: (hiddenSubject: BehaviorSubject<boolean>) => void;
}
