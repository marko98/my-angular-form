import { FormRowItem, FormRowItemInterface } from '../form-row-item.model';
export declare interface TimepickerInterface extends FormRowItemInterface {
    min?: string;
    max?: string;
    appearance?: 'legacy' | 'standard' | 'fill' | 'outline';
    labelName?: string;
    buttonAlign?: 'left' | 'right';
}
export declare class Timepicker extends FormRowItem {
    private appearance;
    private min;
    private max;
    private labelName;
    private buttonAlign;
    getAppearance: () => import("@angular/material/form-field").MatFormFieldAppearance;
    setAppearance: (appearance: import("@angular/material/form-field").MatFormFieldAppearance) => void;
    getButtonAlign: () => "left" | "right";
    setButtonAlign: (buttonAlign: "left" | "right") => void;
    getMax: () => string;
    setMax: (max: string) => void;
    getMin: () => string;
    setMin: (min: string) => void;
    getLabelName: () => string;
    setLabelName: (labelName: string) => void;
}
