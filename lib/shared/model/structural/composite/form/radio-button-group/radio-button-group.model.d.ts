import { FormRowItem, FormRowItemInterface } from '../form-row-item.model';
import { RadioButtonInterface, RadioButton } from './radio-button.model';
export declare interface RadioButtonGroupInterface extends FormRowItemInterface {
    options: RadioButtonInterface[];
    direction: 'column' | 'row';
    labelPosition?: 'after' | 'before';
    label?: RadioButtonGroupLabelInterface;
}
export declare interface RadioButtonGroupLabelInterface {
    context: string;
    position: 'above' | 'bellow';
}
export declare class RadioButtonGroup extends FormRowItem {
    private direction;
    private labelPosition;
    private label;
    getDirection: () => string;
    getLabel: () => RadioButtonGroupLabelInterface;
    setDirection: (direction: "column" | "row") => void;
    setLabel: (label: RadioButtonGroupLabelInterface) => void;
    getLabelPosition: () => "after" | "before";
    setLabelPosition: (labelPosition: "after" | "before") => void;
    addChild(rowButton: RadioButton): boolean;
    addChildInterface(radioButtonInterface: RadioButtonInterface): boolean;
    removeChild(rowButton: RadioButton): boolean;
    shouldHaveChildren(): boolean;
}
