import { FormRowItem } from '../form-row-item.model';
export declare interface RadioButtonInterface {
    name: string;
    value: any;
}
export declare class RadioButton extends FormRowItem {
    private name;
    private value;
    getName: () => string;
    getValue: () => any;
    setName: (name: string) => void;
    setValue: (value: any) => void;
}
