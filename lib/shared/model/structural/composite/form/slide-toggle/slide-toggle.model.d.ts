import { FormRowItem, FormRowItemInterface } from '../form-row-item.model';
import { MAT_COLOR } from '../../../../../material.module';
export declare interface SlideToggleInterface extends FormRowItemInterface {
    color: MAT_COLOR;
    disabled: boolean;
    context: string;
}
export declare class SlideToggle extends FormRowItem {
    private color;
    private context;
    constructor();
    getColor: () => MAT_COLOR;
    setColor: (color: MAT_COLOR) => void;
    getContext: () => string;
    setContext: (context: string) => void;
}
