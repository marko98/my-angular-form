import { FormRowItem, FormRowItemInterface } from '../form-row-item.model';
export declare interface SliderInterface extends FormRowItemInterface {
    minValue: string | number;
    maxValue: string | number;
    disabled: boolean;
    invert: boolean;
    thumbLabel: boolean;
    vertical: boolean;
    step: number;
    tickInterval: number;
}
export declare class Slider extends FormRowItem {
    private minValue;
    private maxValue;
    private vertical;
    private thumbLabel;
    private invert;
    private step;
    private tickInterval;
    constructor();
    getMinValue: () => string | number;
    setMinValue: (minValue: string | number) => void;
    getMaxValue: () => string | number;
    setMaxValue: (maxValue: string | number) => void;
    getInvert: () => boolean;
    setInvert: (invert: boolean) => void;
    getVertical: () => boolean;
    setVertical: (vertical: boolean) => void;
    getThumbLabel: () => boolean;
    setThumbLabel: (thumbLabel: boolean) => void;
    getStep: () => number;
    setStep: (step: number) => void;
    getTickInterval: () => number;
    setTickInterval: (tickInterval: number) => void;
}
