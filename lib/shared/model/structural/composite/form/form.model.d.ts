import { Collection } from '../collection.model';
import { FormRow } from './form-row.model';
import { FormGroup } from '@angular/forms';
export declare interface FormInterface {
    showSubmitButton?: boolean;
    submitButtonText?: string;
    showCancelButton?: boolean;
    cancelButtonText?: string;
    showResetButton?: boolean;
    resetButtonText?: string;
    submitOnlyIfFormValid?: boolean;
}
export declare class Form extends Collection {
    showSubmitButton: boolean;
    submitOnlyIfFormValid: boolean;
    showCancelButton: boolean;
    showResetButton: boolean;
    submitButtonText: string;
    cancelButtonText: string;
    resetButtonText: string;
    private _formGroup;
    controlNames: string[];
    constructor(formInterface?: FormInterface);
    disableAllRowItems: () => void;
    addChild(formRow: FormRow): boolean;
    removeChild(formRow: FormRow): boolean;
    shouldHaveChildren(): boolean;
    getFormGroup: () => FormGroup;
    setFormGroup: (formGroup: FormGroup) => void;
}
