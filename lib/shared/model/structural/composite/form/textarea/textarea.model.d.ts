import { FormFieldInputInterface, FormFieldInput } from '../form-field-input/form-field-input.model';
import { FormFieldInputTextPasswordInterface } from '../form-field-input/form-field-input-text-password.interface';
export declare interface TextareaInterface extends FormFieldInputInterface {
    showHintAboutMinMaxLength?: boolean;
}
export declare class Textarea extends FormFieldInput implements FormFieldInputTextPasswordInterface {
    private showHintAboutMinMaxLength;
    constructor();
    getShowHintAboutMinMaxLength: () => boolean;
    setShowHintAboutMinMaxLength: (showHintAboutMinMaxLength: boolean) => void;
}
