import { FormFieldInput, FormFieldInputInterface } from '../form-field-input/form-field-input.model';
export declare interface SelectInterface extends FormFieldInputInterface {
    multiple: boolean;
    matSelectTriggerOn: boolean;
    optionValues: (OptionInterface | OptionGroupInterface)[];
}
export declare interface OptionInterface {
    value: any;
    textToShow: string;
    disabled: boolean;
}
export declare interface OptionGroupInterface {
    labelName: string;
    disabled: boolean;
    optionValues: OptionInterface[];
}
export declare class Select extends FormFieldInput {
    private multiple;
    private matSelectTriggerOn;
    private optionValues;
    getOptionValues: () => (OptionInterface | OptionGroupInterface)[];
    setOptionValues: (optionValues: (OptionInterface | OptionGroupInterface)[]) => void;
    getMultiple: () => boolean;
    setMultiple: (multiple: boolean) => void;
    getMatSelectTriggerOn: () => boolean;
    setMatSelectTriggerOn: (matSelectTriggerOn: boolean) => void;
}
