import { Collection } from '../collection.model';
import { ValidatorFn } from '@angular/forms';
export declare interface FormRowItemInterface {
    type: ROW_ITEM_TYPE;
    controlName: string;
    validators?: FormRowItemValidatorInterface[];
    defaultValue?: any;
    disabled?: boolean;
}
export declare interface FormRowItemValidatorInterface {
    name: string;
    message: string;
    validatorFn: ValidatorFn;
    length?: number;
}
export declare enum VALIDATOR_NAMES {
    MIN_LENGTH = "minlength",
    MAX_LENGTH = "maxlength",
    REQUIRED = "required",
    MIN = "min",
    MAX = "max",
    PATTERN = "pattern",
    EMAIL = "email"
}
export declare enum ROW_ITEM_TYPE {
    CHECKBOX = "CHECKBOX",
    DATEPICKER = "DATEPICKER",
    TIMEPICKER = "TIMEPICKER",
    FORM_FIELD_INPUT = "FORM_FIELD_INPUT",
    FORM_FIELD_SELECT = "FORM_FIELD_SELECT",
    FORM_FIELD_TEXTAREA = "FORM_FIELD_TEXTAREA",
    RADIO_BUTTON_GROUP = "RADIO_BUTTON_GROUP",
    SLIDER = "SLIDER",
    SLIDE_TOGGLE = "SLIDE_TOGGLE",
    BUTTON = "BUTTON",
    BUTTON_TOGGLE = "BUTTON_TOGGLE",
    ICON = "ICON",
    AUTOCOMPLETE = "AUTOCOMPLETE"
}
export declare class FormRowItem extends Collection {
    private type;
    private controlName;
    private defaultValue;
    protected validators: FormRowItemValidatorInterface[];
    private disabled;
    addChild(child: Collection): boolean;
    removeChild(child: Collection): boolean;
    shouldHaveChildren(): boolean;
    getType: () => ROW_ITEM_TYPE;
    setType: (type: ROW_ITEM_TYPE) => void;
    getControlName: () => string;
    setControlName: (controlName: string) => void;
    getDefaultValue: () => any;
    setDefaultValue: (defaultValue: any) => void;
    getValidators: () => FormRowItemValidatorInterface[];
    addValidator: (validator: FormRowItemValidatorInterface) => void;
    getValidatorFns: () => ValidatorFn[];
    setValidators: (validators: FormRowItemValidatorInterface[]) => void;
    getDisabled: () => boolean;
    setDisabled: (disabled: boolean) => void;
}
