import { Collection } from '../collection.model';
import { FormRowItem, FormRowItemInterface } from './form-row-item.model';
export declare class FormRow extends Collection {
    disableAllRowItems: () => void;
    addChild(formRowItem: FormRowItem): boolean;
    addChildInterface(formRowItemInterface: FormRowItemInterface): boolean;
    removeChild(formRowItem: FormRowItem): boolean;
    shouldHaveChildren(): boolean;
}
