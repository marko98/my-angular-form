import { FormRowItemInterface, FormRowItem } from '../form-row-item.model';
import { MAT_COLOR } from '../../../../../material.module';
export declare interface ButtonInterface extends FormRowItemInterface {
    color: MAT_COLOR;
    disabled: boolean;
    context: string;
    matButtonType: MAT_BUTTON_TYPE;
    buttonType: BUTTON_TYPE;
    functionToExecute: any;
    matIconString: string;
}
export declare enum MAT_BUTTON_TYPE {
    BASIC = "BASIC",
    RAISED = "RAISED",
    STROKED = "STROKED",
    FLAT = "FLAT",
    ICON = "ICON",
    FAB = "FAB",
    MINI_FAB = "MINI_FAB"
}
export declare enum BUTTON_TYPE {
    SUBMIT = "submit",
    BUTTON = "button",
    RESET = "reset",
    MENU = "menu"
}
export declare class Button extends FormRowItem {
    private color;
    private context;
    private matButtonType;
    private buttonType;
    private functionToExecute;
    private matIconString;
    constructor();
    getColor: () => MAT_COLOR;
    setColor: (color: MAT_COLOR) => void;
    getContext: () => string;
    setContext: (context: string) => void;
    getMatButtonType: () => MAT_BUTTON_TYPE;
    setMatButtonType: (matButtonType: MAT_BUTTON_TYPE) => void;
    getButtonType: () => BUTTON_TYPE;
    setButtonType: (buttonType: BUTTON_TYPE) => void;
    getFunctionToExecute: () => Function;
    setFunctionToExecute: (functionToExecute: Function) => void;
    getMatIconString: () => string;
    setMatIconString: (matIconString: string) => void;
}
