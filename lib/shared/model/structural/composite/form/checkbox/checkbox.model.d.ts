import { FormRowItem, FormRowItemInterface } from '../form-row-item.model';
import { MAT_COLOR } from '../../../../../material.module';
export declare interface CheckboxInterface extends FormRowItemInterface {
    name: string;
    defaultValue: boolean;
    labelPosition?: 'before' | 'after';
    color?: MAT_COLOR;
}
export declare class Checkbox extends FormRowItem {
    private name;
    private color;
    private labelPosition;
    getName: () => string;
    setName: (name: string) => void;
    getColor: () => MAT_COLOR;
    setColor: (color: MAT_COLOR) => void;
    getLabelPosition: () => "after" | "before";
    setLabelPosition: (labelPosition: "after" | "before") => void;
}
