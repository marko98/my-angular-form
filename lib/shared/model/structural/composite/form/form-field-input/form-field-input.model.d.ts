import { FormRowItem, FormRowItemInterface } from '../form-row-item.model';
export declare interface FormFieldInputInterface extends FormRowItemInterface {
    formFieldInputType: FORM_FIELD_INPUT_TYPE;
    labelName: string;
    appearance?: 'legacy' | 'standard' | 'fill' | 'outline';
    defaultValue?: any;
    placeholder?: string;
    readonly?: boolean;
    requiredOption?: FormFieldInputRequiredInterface;
    hintLabels?: FormFieldInputHintLabelsInterface;
    matImages?: FormFieldInputMatImagesInterface;
    textSuffix?: string;
    textPrefix?: string;
}
export declare interface FormFieldInputMatImagesInterface {
    matPrefixImgText?: string;
    matSuffixImgText?: string;
}
export declare interface FormFieldInputRequiredInterface {
    required?: boolean;
    hideRequiredMarker?: boolean;
}
export declare interface FormFieldInputHintLabelsInterface {
    leftHintLabelContext?: string;
    rightHintLabelContext?: string;
}
export declare enum FORM_FIELD_INPUT_TYPE {
    TEXT = "text",
    NUMBER = "number",
    FILE = "file",
    EMAIL = "email",
    PASSWORD = "password"
}
export declare class FormFieldInput extends FormRowItem {
    private formFieldInputType;
    private appearance;
    private placeholder?;
    private readonly;
    private required;
    private labelName;
    private leftHintLabel?;
    private rightHintLabel?;
    private matPrefixImgText?;
    private matSuffixImgText?;
    private textSuffix?;
    private textPrefix?;
    getFormFieldInputType: () => FORM_FIELD_INPUT_TYPE;
    setFormFieldInputType: (formFieldInputType: FORM_FIELD_INPUT_TYPE) => void;
    getAppearance: () => import("@angular/material/form-field").MatFormFieldAppearance;
    setAppearance: (appearance: import("@angular/material/form-field").MatFormFieldAppearance) => void;
    getPlaceholder: () => string;
    setPlaceholder: (placeholder: string) => void;
    getReadonly: () => boolean;
    setReadonly: (readonly: boolean) => void;
    getRequired: () => FormFieldInputRequiredInterface;
    setRequired: (required: FormFieldInputRequiredInterface) => void;
    getLabelName: () => string;
    setLabelName: (labelName: string) => void;
    getLeftHintLabel: () => string;
    setLeftHintLabel: (leftHintLabel: string) => void;
    getRightHintLabel: () => string;
    setRightHintLabel: (rightHintLabel: string) => void;
    getMatPrefixImgText: () => string;
    setMatPrefixImgText: (matPrefixImgText: string) => void;
    getMatSuffixImgText: () => string;
    setMatSuffixImgText: (matSuffixImgText: string) => void;
    getTextSuffix: () => string;
    setTextSuffix: (textSuffix: string) => void;
    getTextPrefix: () => string;
    setTextPrefix: (textPrefix: string) => void;
}
