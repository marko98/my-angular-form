import { FormFieldInput, FormFieldInputInterface } from './form-field-input.model';
import { FormFieldInputTextPasswordInterface } from './form-field-input-text-password.interface';
export declare interface FormFieldInputPasswordInterface extends FormFieldInputInterface {
    showPasswordInMs?: number;
    showHintAboutMinMaxLength?: boolean;
}
export declare class FormFieldInputPassword extends FormFieldInput implements FormFieldInputTextPasswordInterface {
    private showPasswordInMs?;
    private showPassword;
    private showHintAboutMinMaxLength;
    constructor();
    onShowPassword: () => void;
    getShowPassword: () => boolean;
    getShowPasswordInMs: () => number;
    setShowPasswordInMs: (showPasswordInMs: number) => void;
    getShowHintAboutMinMaxLength: () => boolean;
    setShowHintAboutMinMaxLength: (showHintAboutMinMaxLength: boolean) => void;
}
