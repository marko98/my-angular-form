export declare interface FormFieldInputTextPasswordInterface {
    getShowHintAboutMinMaxLength(): boolean;
    setShowHintAboutMinMaxLength(showHintAboutMinMaxLength: boolean): void;
}
