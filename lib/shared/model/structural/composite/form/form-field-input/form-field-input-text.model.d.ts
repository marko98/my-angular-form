import { FormFieldInput, FormFieldInputInterface } from './form-field-input.model';
import { FormFieldInputTextPasswordInterface } from './form-field-input-text-password.interface';
export declare interface FormFieldInputTextInterface extends FormFieldInputInterface {
    showHintAboutMinMaxLength?: boolean;
}
export declare class FormFieldInputText extends FormFieldInput implements FormFieldInputTextPasswordInterface {
    private showHintAboutMinMaxLength;
    constructor();
    getShowHintAboutMinMaxLength: () => boolean;
    setShowHintAboutMinMaxLength: (showHintAboutMinMaxLength: boolean) => void;
}
