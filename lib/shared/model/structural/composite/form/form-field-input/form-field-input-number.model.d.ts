import { FormFieldInput, FormFieldInputInterface } from './form-field-input.model';
export declare interface FormFieldInputNumberInterface extends FormFieldInputInterface {
    step: number;
}
export declare class FormFieldInputNumber extends FormFieldInput {
    private step;
    constructor();
    getStep: () => number;
    setStep: (step: number) => void;
}
