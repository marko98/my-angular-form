import { FormFieldInput, FormFieldInputInterface } from "./form-field-input.model";
import { FormRowItemValidatorInterface } from "../form-row-item.model";
import { MAT_COLOR } from "../../../../../material.module";
export declare interface FormFieldInputFileInterface extends FormFieldInputInterface {
    allowedFileTypes: string[];
    dragAndDrop?: boolean;
    buttonAddText?: string;
    buttonDeleteText?: string;
    buttonAddColor?: MAT_COLOR;
    buttonDeleteColor?: MAT_COLOR;
    borderColor?: string;
}
export declare class FormFieldInputFile extends FormFieldInput {
    private dragAndDrop;
    private buttonAddText;
    private buttonDeleteText;
    private buttonAddColor;
    private buttonDeleteColor;
    private borderColor;
    private allowedFileTypes;
    constructor();
    getAllowedFileTypes: () => string[];
    setAllowedFileTypes: (allowedFileTypes: string[]) => void;
    isFileTypeAllowed: (type: string) => boolean;
    getDragAndDrop: () => boolean;
    setDragAndDrop: (dragAndDrop: boolean) => void;
    getButtonAddText: () => string;
    setButtonAddText: (buttonAddText: string) => void;
    getButtonDeleteText: () => string;
    setButtonDeleteText: (buttonDeleteText: string) => void;
    getButtonAddColor: () => MAT_COLOR;
    setButtonAddColor: (buttonAddColor: MAT_COLOR) => void;
    getButtonDeleteColor: () => MAT_COLOR;
    setButtonDeleteColor: (buttonDeleteColor: MAT_COLOR) => void;
    getBorderColor: () => string;
    setBorderColor: (borderColor: string) => void;
    addFile: (file: File) => void;
    setValidators: (validators: FormRowItemValidatorInterface[]) => void;
}
