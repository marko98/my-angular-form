export declare abstract class Collection {
    protected children: Collection[];
    protected parent: Collection;
    abstract addChild(child: Collection): boolean;
    abstract removeChild(child: Collection): boolean;
    abstract shouldHaveChildren(): boolean;
    getChildren: () => Collection[];
    getParent: () => Collection;
    setParent: (parent: Collection) => boolean;
}
