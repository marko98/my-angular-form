import { FormRowItemConstructionStrategy } from './form-row-item-construction-strategy.interface';
import { ButtonInterface, Button } from '../../../structural/composite/form/button/button.model';
export declare class ButtonConstructionStrategy implements FormRowItemConstructionStrategy {
    construct(buttonInterface: ButtonInterface): Button;
}
