import { FormRowItemConstructionStrategy } from './form-row-item-construction-strategy.interface';
import { FormRowItem } from '../../../structural/composite/form/form-row-item.model';
import { CheckboxInterface } from '../../../structural/composite/form/checkbox/checkbox.model';
export declare class CheckboxConstructionStrategy implements FormRowItemConstructionStrategy {
    construct(checkboxInterface: CheckboxInterface): FormRowItem;
}
