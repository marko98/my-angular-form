import { FormRowItemConstructionStrategy } from './form-row-item-construction-strategy.interface';
import { MyAutocompleteInterface, Autocomplete } from '../../../structural/composite/form/autocomplete/autocomplete.model';
export declare class AutocompleteConstructionStrategy implements FormRowItemConstructionStrategy {
    construct(autocompleteInterface: MyAutocompleteInterface): Autocomplete;
}
