import { SlideToggleInterface, SlideToggle } from '../../../structural/composite/form/slide-toggle/slide-toggle.model';
import { FormRowItemConstructionStrategy } from './form-row-item-construction-strategy.interface';
export declare class SlideToggleConstructionStrategy implements FormRowItemConstructionStrategy {
    construct(slideToggleInterface: SlideToggleInterface): SlideToggle;
}
