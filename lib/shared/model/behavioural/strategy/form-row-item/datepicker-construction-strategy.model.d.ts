import { FormRowItemConstructionStrategy } from './form-row-item-construction-strategy.interface';
import { Datepicker, DatepickerInterface } from '../../../structural/composite/form/datepicker/datepicker.model';
export declare class DatepickerConstructionStrategy implements FormRowItemConstructionStrategy {
    construct(datepickerInterface: DatepickerInterface): Datepicker;
}
