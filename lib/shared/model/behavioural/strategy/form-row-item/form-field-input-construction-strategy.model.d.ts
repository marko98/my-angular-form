import { FormRowItemConstructionStrategy } from './form-row-item-construction-strategy.interface';
import { FormRowItem } from '../../../structural/composite/form/form-row-item.model';
import { FormFieldInputInterface } from '../../../structural/composite/form/form-field-input/form-field-input.model';
export declare class FormFieldInputConstructionStrategy implements FormRowItemConstructionStrategy {
    construct(formFieldInputInterface: FormFieldInputInterface): FormRowItem;
}
