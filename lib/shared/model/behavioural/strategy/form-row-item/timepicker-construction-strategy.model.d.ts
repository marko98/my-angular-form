import { TimepickerInterface, Timepicker } from '../../../structural/composite/form/timepicker/timepicker.model';
import { FormRowItemConstructionStrategy } from './form-row-item-construction-strategy.interface';
export declare class TimepickerConstructionStrategy implements FormRowItemConstructionStrategy {
    construct(timepickerInterface: TimepickerInterface): Timepicker;
}
