import { FormRowItem, FormRowItemInterface } from '../../../structural/composite/form/form-row-item.model';
export declare interface FormRowItemConstructionStrategy {
    construct(formRowItemInterface: FormRowItemInterface): FormRowItem;
}
