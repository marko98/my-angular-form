import { FormFieldInputConstructionStrategy } from './form-field-input-construction-strategy.model';
import { SelectInterface, Select } from '../../../structural/composite/form/select/select.model';
export declare class SelectConstructionStrategy implements FormFieldInputConstructionStrategy {
    construct(selectInterface: SelectInterface): Select;
}
