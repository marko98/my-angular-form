import { FormRowItemConstructionStrategy } from './form-row-item-construction-strategy.interface';
import { FormRowItem } from '../../../structural/composite/form/form-row-item.model';
import { RadioButtonGroupInterface } from '../../../structural/composite/form/radio-button-group/radio-button-group.model';
export declare class RadioButtonGroupConstructionStrategy implements FormRowItemConstructionStrategy {
    construct(radioButtonGroupInterface: RadioButtonGroupInterface): FormRowItem;
}
