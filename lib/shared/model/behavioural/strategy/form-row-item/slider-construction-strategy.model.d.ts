import { FormRowItemConstructionStrategy } from "./form-row-item-construction-strategy.interface";
import { Slider, SliderInterface } from "../../../structural/composite/form/slider/slider.model";
export declare class SliderConstructionStrategy implements FormRowItemConstructionStrategy {
    construct(sliderInterface: SliderInterface): Slider;
}
