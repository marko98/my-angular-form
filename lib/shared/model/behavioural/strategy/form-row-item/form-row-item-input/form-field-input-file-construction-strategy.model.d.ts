import { FormRowItemConstructionStrategy } from "../form-row-item-construction-strategy.interface";
import { FormFieldInputFileInterface, FormFieldInputFile } from "../../../../../model/structural/composite/form/form-field-input/form-field-input-file.model";
export declare class FormFieldInputFileConstructionStrategy implements FormRowItemConstructionStrategy {
    construct(formFieldInputFileInterface: FormFieldInputFileInterface): FormFieldInputFile;
}
