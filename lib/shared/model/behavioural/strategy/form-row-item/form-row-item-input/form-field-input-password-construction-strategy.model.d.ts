import { FormRowItemConstructionStrategy } from "../form-row-item-construction-strategy.interface";
import { FormFieldInputPasswordInterface, FormFieldInputPassword } from "../../../../../model/structural/composite/form/form-field-input/form-field-input-password.model";
export declare class FormFieldInputPasswordConstructionStrategy implements FormRowItemConstructionStrategy {
    construct(formFieldInputPasswordInterface: FormFieldInputPasswordInterface): FormFieldInputPassword;
}
