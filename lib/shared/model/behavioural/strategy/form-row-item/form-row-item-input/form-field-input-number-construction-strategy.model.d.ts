import { FormRowItemConstructionStrategy } from "../form-row-item-construction-strategy.interface";
import { FormFieldInputNumberInterface, FormFieldInputNumber } from "../../../../../model/structural/composite/form/form-field-input/form-field-input-number.model";
export declare class FormFieldInputNumberConstructionStrategy implements FormRowItemConstructionStrategy {
    construct(formFieldInputNumberInterface: FormFieldInputNumberInterface): FormFieldInputNumber;
}
