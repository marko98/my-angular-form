import { FormRowItemConstructionStrategy } from "../form-row-item-construction-strategy.interface";
import { FormFieldInputTextInterface, FormFieldInputText } from "../../../../../model/structural/composite/form/form-field-input/form-field-input-text.model";
export declare class FormFieldInputTextConstructionStrategy implements FormRowItemConstructionStrategy {
    construct(formFieldInputTextInterface: FormFieldInputTextInterface): FormFieldInputText;
}
