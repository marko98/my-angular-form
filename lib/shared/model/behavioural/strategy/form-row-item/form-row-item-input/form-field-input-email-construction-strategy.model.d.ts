import { FormRowItemConstructionStrategy } from "../form-row-item-construction-strategy.interface";
import { FormFieldInput, FormFieldInputInterface } from "../../../../../model/structural/composite/form/form-field-input/form-field-input.model";
export declare class FormFieldInputEmailConstructionStrategy implements FormRowItemConstructionStrategy {
    construct(formFieldInputInterface: FormFieldInputInterface): FormFieldInput;
}
