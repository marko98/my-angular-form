import { TextareaInterface, Textarea } from "../../../structural/composite/form/textarea/textarea.model";
import { FormRowItemConstructionStrategy } from "./form-row-item-construction-strategy.interface";
export declare class TextareaConstructionStrategy implements FormRowItemConstructionStrategy {
    construct(textareaInterface: TextareaInterface): Textarea;
}
