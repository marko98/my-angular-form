import * as i0 from "@angular/core";
import * as i1 from "@angular/material/button";
import * as i2 from "@angular/material/toolbar";
import * as i3 from "@angular/material/radio";
import * as i4 from "@angular/material/checkbox";
import * as i5 from "@angular/material/form-field";
import * as i6 from "@angular/material/input";
import * as i7 from "@angular/material/icon";
import * as i8 from "@angular/material/dialog";
import * as i9 from "@angular/material/list";
import * as i10 from "@angular/material/progress-bar";
import * as i11 from "@angular/material/datepicker";
import * as i12 from "@angular/material/core";
import * as i13 from "@angular/material/snack-bar";
import * as i14 from "@angular/material/select";
import * as i15 from "@angular/material/slider";
import * as i16 from "@angular/material/slide-toggle";
export declare enum MAT_COLOR {
    PRIMARY = "primary",
    WARN = "warn",
    ACCENT = "accent",
    EMPTY = ""
}
export declare class MaterialModule {
    static ɵmod: i0.ɵɵNgModuleDefWithMeta<MaterialModule, never, [typeof i1.MatButtonModule, typeof i2.MatToolbarModule, typeof i3.MatRadioModule, typeof i4.MatCheckboxModule, typeof i5.MatFormFieldModule, typeof i6.MatInputModule, typeof i7.MatIconModule, typeof i8.MatDialogModule, typeof i9.MatListModule, typeof i10.MatProgressBarModule, typeof i11.MatDatepickerModule, typeof i12.MatNativeDateModule, typeof i13.MatSnackBarModule, typeof i14.MatSelectModule, typeof i15.MatSliderModule, typeof i16.MatSlideToggleModule], [typeof i1.MatButtonModule, typeof i2.MatToolbarModule, typeof i3.MatRadioModule, typeof i4.MatCheckboxModule, typeof i5.MatFormFieldModule, typeof i6.MatInputModule, typeof i7.MatIconModule, typeof i8.MatDialogModule, typeof i9.MatListModule, typeof i10.MatProgressBarModule, typeof i11.MatDatepickerModule, typeof i12.MatNativeDateModule, typeof i13.MatSnackBarModule, typeof i14.MatSelectModule, typeof i15.MatSliderModule, typeof i16.MatSlideToggleModule]>;
    static ɵinj: i0.ɵɵInjectorDef<MaterialModule>;
}
