import * as i0 from "@angular/core";
import * as i1 from "./my-angular-form.component";
import * as i2 from "./shared/shared.module";
export declare class MyAngularFormModule {
    static ɵmod: i0.ɵɵNgModuleDefWithMeta<MyAngularFormModule, [typeof i1.MyAngularFormComponent], [typeof i2.SharedModule], [typeof i1.MyAngularFormComponent]>;
    static ɵinj: i0.ɵɵInjectorDef<MyAngularFormModule>;
}
