import { NgModule } from "@angular/core";
import { MyAngularFormComponent } from "./my-angular-form.component";
import { SharedModule } from "./shared/shared.module";
import * as i0 from "@angular/core";
var MyAngularFormModule = /** @class */ (function () {
    function MyAngularFormModule() {
    }
    MyAngularFormModule.ɵmod = i0.ɵɵdefineNgModule({ type: MyAngularFormModule });
    MyAngularFormModule.ɵinj = i0.ɵɵdefineInjector({ factory: function MyAngularFormModule_Factory(t) { return new (t || MyAngularFormModule)(); }, imports: [[SharedModule]] });
    return MyAngularFormModule;
}());
export { MyAngularFormModule };
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(MyAngularFormModule, { declarations: [MyAngularFormComponent], imports: [SharedModule], exports: [MyAngularFormComponent] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(MyAngularFormModule, [{
        type: NgModule,
        args: [{
                declarations: [MyAngularFormComponent],
                imports: [SharedModule],
                exports: [MyAngularFormComponent],
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXktYW5ndWxhci1mb3JtLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215LWFuZ3VsYXItZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9teS1hbmd1bGFyLWZvcm0ubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFDckUsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHdCQUF3QixDQUFDOztBQUV0RDtJQUFBO0tBS21DOzJEQUF0QixtQkFBbUI7eUhBQW5CLG1CQUFtQixrQkFIckIsQ0FBQyxZQUFZLENBQUM7OEJBTnpCO0NBU21DLEFBTG5DLElBS21DO1NBQXRCLG1CQUFtQjt3RkFBbkIsbUJBQW1CLG1CQUpmLHNCQUFzQixhQUMzQixZQUFZLGFBQ1osc0JBQXNCO2tEQUVyQixtQkFBbUI7Y0FML0IsUUFBUTtlQUFDO2dCQUNSLFlBQVksRUFBRSxDQUFDLHNCQUFzQixDQUFDO2dCQUN0QyxPQUFPLEVBQUUsQ0FBQyxZQUFZLENBQUM7Z0JBQ3ZCLE9BQU8sRUFBRSxDQUFDLHNCQUFzQixDQUFDO2FBQ2xDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgTXlBbmd1bGFyRm9ybUNvbXBvbmVudCB9IGZyb20gXCIuL215LWFuZ3VsYXItZm9ybS5jb21wb25lbnRcIjtcbmltcG9ydCB7IFNoYXJlZE1vZHVsZSB9IGZyb20gXCIuL3NoYXJlZC9zaGFyZWQubW9kdWxlXCI7XG5cbkBOZ01vZHVsZSh7XG4gIGRlY2xhcmF0aW9uczogW015QW5ndWxhckZvcm1Db21wb25lbnRdLFxuICBpbXBvcnRzOiBbU2hhcmVkTW9kdWxlXSxcbiAgZXhwb3J0czogW015QW5ndWxhckZvcm1Db21wb25lbnRdLFxufSlcbmV4cG9ydCBjbGFzcyBNeUFuZ3VsYXJGb3JtTW9kdWxlIHt9XG4iXX0=