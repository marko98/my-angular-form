import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from './material.module';
import { HttpClientModule } from '@angular/common/http';
import { DeviceDetectorModule } from 'ngx-device-detector';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
// component
import { RadioButtonComponent } from './ui/radio-button/radio-button.component';
import { CheckboxComponent } from './ui/checkbox/checkbox.component';
import { UploadDialogComponent } from './ui/upload-dialog/upload-dialog.component';
import { DragDropDirective } from './directive/drag-drop.directive';
import { FormFieldInputTextComponent } from './ui/form-field-input/form-field-input-text/form-field-input-text.component';
import { FormFieldInputPasswordComponent } from './ui/form-field-input/form-field-input-password/form-field-input-password.component';
import { FormFieldInputNumberComponent } from './ui/form-field-input/form-field-input-number/form-field-input-number.component';
import { FormFieldInputFileComponent } from './ui/form-field-input/form-field-input-file/form-field-input-file.component';
import { FormFieldInputEmailComponent } from './ui/form-field-input/form-field-input-email/form-field-input-email.component';
import { DatepickerComponent } from './ui/datepicker/datepicker.component';
import { TextareaComponent } from './ui/textarea/textarea.component';
import { SelectComponent } from './ui/select/select.component';
import { SliderComponent } from './ui/slider/slider.component';
import { SlideToggleComponent } from './ui/slide-toggle/slide-toggle.component';
import { ButtonComponent } from './ui/button/button.component';
import { AutocompleteComponent } from './ui/autocomplete/autocomplete.component';
import { AutocompleteModule } from 'autocomplete';
import { TimepickerComponent } from './ui/timepicker/timepicker.component';
import * as i0 from "@angular/core";
import * as i1 from "ngx-device-detector";
var SharedModule = /** @class */ (function () {
    function SharedModule() {
    }
    SharedModule.ɵmod = i0.ɵɵdefineNgModule({ type: SharedModule });
    SharedModule.ɵinj = i0.ɵɵdefineInjector({ factory: function SharedModule_Factory(t) { return new (t || SharedModule)(); }, imports: [[
                CommonModule,
                FormsModule,
                ReactiveFormsModule,
                FlexLayoutModule,
                MaterialModule,
                HttpClientModule,
                DeviceDetectorModule.forRoot(),
                AutocompleteModule,
                NgxMaterialTimepickerModule,
            ],
            CommonModule,
            FormsModule,
            ReactiveFormsModule,
            FlexLayoutModule,
            MaterialModule,
            HttpClientModule,
            DeviceDetectorModule,
            AutocompleteModule,
            NgxMaterialTimepickerModule] });
    return SharedModule;
}());
export { SharedModule };
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(SharedModule, { declarations: [RadioButtonComponent,
        CheckboxComponent,
        UploadDialogComponent,
        DragDropDirective,
        FormFieldInputTextComponent,
        FormFieldInputPasswordComponent,
        FormFieldInputNumberComponent,
        FormFieldInputFileComponent,
        FormFieldInputEmailComponent,
        DatepickerComponent,
        TextareaComponent,
        SelectComponent,
        SliderComponent,
        SlideToggleComponent,
        ButtonComponent,
        AutocompleteComponent,
        TimepickerComponent], imports: [CommonModule,
        FormsModule,
        ReactiveFormsModule,
        FlexLayoutModule,
        MaterialModule,
        HttpClientModule, i1.DeviceDetectorModule, AutocompleteModule,
        NgxMaterialTimepickerModule], exports: [CommonModule,
        FormsModule,
        ReactiveFormsModule,
        FlexLayoutModule,
        MaterialModule,
        HttpClientModule,
        DeviceDetectorModule,
        AutocompleteModule,
        NgxMaterialTimepickerModule,
        RadioButtonComponent,
        CheckboxComponent,
        DragDropDirective,
        FormFieldInputTextComponent,
        FormFieldInputPasswordComponent,
        FormFieldInputNumberComponent,
        FormFieldInputFileComponent,
        FormFieldInputEmailComponent,
        DatepickerComponent,
        TextareaComponent,
        SelectComponent,
        SliderComponent,
        SlideToggleComponent,
        ButtonComponent,
        AutocompleteComponent,
        TimepickerComponent] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(SharedModule, [{
        type: NgModule,
        args: [{
                declarations: [
                    RadioButtonComponent,
                    CheckboxComponent,
                    UploadDialogComponent,
                    DragDropDirective,
                    FormFieldInputTextComponent,
                    FormFieldInputPasswordComponent,
                    FormFieldInputNumberComponent,
                    FormFieldInputFileComponent,
                    FormFieldInputEmailComponent,
                    DatepickerComponent,
                    TextareaComponent,
                    SelectComponent,
                    SliderComponent,
                    SlideToggleComponent,
                    ButtonComponent,
                    AutocompleteComponent,
                    TimepickerComponent,
                ],
                imports: [
                    CommonModule,
                    FormsModule,
                    ReactiveFormsModule,
                    FlexLayoutModule,
                    MaterialModule,
                    HttpClientModule,
                    DeviceDetectorModule.forRoot(),
                    AutocompleteModule,
                    NgxMaterialTimepickerModule,
                ],
                exports: [
                    CommonModule,
                    FormsModule,
                    ReactiveFormsModule,
                    FlexLayoutModule,
                    MaterialModule,
                    HttpClientModule,
                    DeviceDetectorModule,
                    AutocompleteModule,
                    NgxMaterialTimepickerModule,
                    RadioButtonComponent,
                    CheckboxComponent,
                    DragDropDirective,
                    FormFieldInputTextComponent,
                    FormFieldInputPasswordComponent,
                    FormFieldInputNumberComponent,
                    FormFieldInputFileComponent,
                    FormFieldInputEmailComponent,
                    DatepickerComponent,
                    TextareaComponent,
                    SelectComponent,
                    SliderComponent,
                    SlideToggleComponent,
                    ButtonComponent,
                    AutocompleteComponent,
                    TimepickerComponent,
                ],
                entryComponents: [UploadDialogComponent],
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hhcmVkLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215LWFuZ3VsYXItZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvc2hhcmVkLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxXQUFXLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNsRSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDL0MsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDeEQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQ25ELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ3hELE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQzNELE9BQU8sRUFBRSwyQkFBMkIsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBRXRFLFlBQVk7QUFDWixPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUNoRixPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUNyRSxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSw0Q0FBNEMsQ0FBQztBQUNuRixPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQztBQUNwRSxPQUFPLEVBQUUsMkJBQTJCLEVBQUUsTUFBTSw2RUFBNkUsQ0FBQztBQUMxSCxPQUFPLEVBQUUsK0JBQStCLEVBQUUsTUFBTSxxRkFBcUYsQ0FBQztBQUN0SSxPQUFPLEVBQUUsNkJBQTZCLEVBQUUsTUFBTSxpRkFBaUYsQ0FBQztBQUNoSSxPQUFPLEVBQUUsMkJBQTJCLEVBQUUsTUFBTSw2RUFBNkUsQ0FBQztBQUMxSCxPQUFPLEVBQUUsNEJBQTRCLEVBQUUsTUFBTSwrRUFBK0UsQ0FBQztBQUM3SCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxzQ0FBc0MsQ0FBQztBQUMzRSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUNyRSxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFDL0QsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLDhCQUE4QixDQUFDO0FBQy9ELE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBQ2hGLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSw4QkFBOEIsQ0FBQztBQUMvRCxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUNqRixPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFDbEQsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sc0NBQXNDLENBQUM7OztBQUUzRTtJQUFBO0tBaUU0QjtvREFBZixZQUFZOzJHQUFaLFlBQVksa0JBM0NkO2dCQUNQLFlBQVk7Z0JBQ1osV0FBVztnQkFDWCxtQkFBbUI7Z0JBQ25CLGdCQUFnQjtnQkFDaEIsY0FBYztnQkFDZCxnQkFBZ0I7Z0JBQ2hCLG9CQUFvQixDQUFDLE9BQU8sRUFBRTtnQkFDOUIsa0JBQWtCO2dCQUNsQiwyQkFBMkI7YUFDNUI7WUFFQyxZQUFZO1lBQ1osV0FBVztZQUNYLG1CQUFtQjtZQUNuQixnQkFBZ0I7WUFDaEIsY0FBYztZQUNkLGdCQUFnQjtZQUNoQixvQkFBb0I7WUFDcEIsa0JBQWtCO1lBQ2xCLDJCQUEyQjt1QkF2RS9CO0NBOEY0QixBQWpFNUIsSUFpRTRCO1NBQWYsWUFBWTt3RkFBWixZQUFZLG1CQS9EckIsb0JBQW9CO1FBQ3BCLGlCQUFpQjtRQUNqQixxQkFBcUI7UUFFckIsaUJBQWlCO1FBRWpCLDJCQUEyQjtRQUMzQiwrQkFBK0I7UUFDL0IsNkJBQTZCO1FBQzdCLDJCQUEyQjtRQUMzQiw0QkFBNEI7UUFDNUIsbUJBQW1CO1FBQ25CLGlCQUFpQjtRQUNqQixlQUFlO1FBQ2YsZUFBZTtRQUNmLG9CQUFvQjtRQUNwQixlQUFlO1FBQ2YscUJBQXFCO1FBQ3JCLG1CQUFtQixhQUduQixZQUFZO1FBQ1osV0FBVztRQUNYLG1CQUFtQjtRQUNuQixnQkFBZ0I7UUFDaEIsY0FBYztRQUNkLGdCQUFnQiwyQkFFaEIsa0JBQWtCO1FBQ2xCLDJCQUEyQixhQUczQixZQUFZO1FBQ1osV0FBVztRQUNYLG1CQUFtQjtRQUNuQixnQkFBZ0I7UUFDaEIsY0FBYztRQUNkLGdCQUFnQjtRQUNoQixvQkFBb0I7UUFDcEIsa0JBQWtCO1FBQ2xCLDJCQUEyQjtRQUUzQixvQkFBb0I7UUFDcEIsaUJBQWlCO1FBRWpCLGlCQUFpQjtRQUVqQiwyQkFBMkI7UUFDM0IsK0JBQStCO1FBQy9CLDZCQUE2QjtRQUM3QiwyQkFBMkI7UUFDM0IsNEJBQTRCO1FBQzVCLG1CQUFtQjtRQUNuQixpQkFBaUI7UUFDakIsZUFBZTtRQUNmLGVBQWU7UUFDZixvQkFBb0I7UUFDcEIsZUFBZTtRQUNmLHFCQUFxQjtRQUNyQixtQkFBbUI7a0RBSVYsWUFBWTtjQWpFeEIsUUFBUTtlQUFDO2dCQUNSLFlBQVksRUFBRTtvQkFDWixvQkFBb0I7b0JBQ3BCLGlCQUFpQjtvQkFDakIscUJBQXFCO29CQUVyQixpQkFBaUI7b0JBRWpCLDJCQUEyQjtvQkFDM0IsK0JBQStCO29CQUMvQiw2QkFBNkI7b0JBQzdCLDJCQUEyQjtvQkFDM0IsNEJBQTRCO29CQUM1QixtQkFBbUI7b0JBQ25CLGlCQUFpQjtvQkFDakIsZUFBZTtvQkFDZixlQUFlO29CQUNmLG9CQUFvQjtvQkFDcEIsZUFBZTtvQkFDZixxQkFBcUI7b0JBQ3JCLG1CQUFtQjtpQkFDcEI7Z0JBQ0QsT0FBTyxFQUFFO29CQUNQLFlBQVk7b0JBQ1osV0FBVztvQkFDWCxtQkFBbUI7b0JBQ25CLGdCQUFnQjtvQkFDaEIsY0FBYztvQkFDZCxnQkFBZ0I7b0JBQ2hCLG9CQUFvQixDQUFDLE9BQU8sRUFBRTtvQkFDOUIsa0JBQWtCO29CQUNsQiwyQkFBMkI7aUJBQzVCO2dCQUNELE9BQU8sRUFBRTtvQkFDUCxZQUFZO29CQUNaLFdBQVc7b0JBQ1gsbUJBQW1CO29CQUNuQixnQkFBZ0I7b0JBQ2hCLGNBQWM7b0JBQ2QsZ0JBQWdCO29CQUNoQixvQkFBb0I7b0JBQ3BCLGtCQUFrQjtvQkFDbEIsMkJBQTJCO29CQUUzQixvQkFBb0I7b0JBQ3BCLGlCQUFpQjtvQkFFakIsaUJBQWlCO29CQUVqQiwyQkFBMkI7b0JBQzNCLCtCQUErQjtvQkFDL0IsNkJBQTZCO29CQUM3QiwyQkFBMkI7b0JBQzNCLDRCQUE0QjtvQkFDNUIsbUJBQW1CO29CQUNuQixpQkFBaUI7b0JBQ2pCLGVBQWU7b0JBQ2YsZUFBZTtvQkFDZixvQkFBb0I7b0JBQ3BCLGVBQWU7b0JBQ2YscUJBQXFCO29CQUNyQixtQkFBbUI7aUJBQ3BCO2dCQUNELGVBQWUsRUFBRSxDQUFDLHFCQUFxQixDQUFDO2FBQ3pDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRm9ybXNNb2R1bGUsIFJlYWN0aXZlRm9ybXNNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7IEZsZXhMYXlvdXRNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9mbGV4LWxheW91dCc7XHJcbmltcG9ydCB7IE1hdGVyaWFsTW9kdWxlIH0gZnJvbSAnLi9tYXRlcmlhbC5tb2R1bGUnO1xyXG5pbXBvcnQgeyBIdHRwQ2xpZW50TW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xyXG5pbXBvcnQgeyBEZXZpY2VEZXRlY3Rvck1vZHVsZSB9IGZyb20gJ25neC1kZXZpY2UtZGV0ZWN0b3InO1xyXG5pbXBvcnQgeyBOZ3hNYXRlcmlhbFRpbWVwaWNrZXJNb2R1bGUgfSBmcm9tICduZ3gtbWF0ZXJpYWwtdGltZXBpY2tlcic7XHJcblxyXG4vLyBjb21wb25lbnRcclxuaW1wb3J0IHsgUmFkaW9CdXR0b25Db21wb25lbnQgfSBmcm9tICcuL3VpL3JhZGlvLWJ1dHRvbi9yYWRpby1idXR0b24uY29tcG9uZW50JztcclxuaW1wb3J0IHsgQ2hlY2tib3hDb21wb25lbnQgfSBmcm9tICcuL3VpL2NoZWNrYm94L2NoZWNrYm94LmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFVwbG9hZERpYWxvZ0NvbXBvbmVudCB9IGZyb20gJy4vdWkvdXBsb2FkLWRpYWxvZy91cGxvYWQtZGlhbG9nLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IERyYWdEcm9wRGlyZWN0aXZlIH0gZnJvbSAnLi9kaXJlY3RpdmUvZHJhZy1kcm9wLmRpcmVjdGl2ZSc7XHJcbmltcG9ydCB7IEZvcm1GaWVsZElucHV0VGV4dENvbXBvbmVudCB9IGZyb20gJy4vdWkvZm9ybS1maWVsZC1pbnB1dC9mb3JtLWZpZWxkLWlucHV0LXRleHQvZm9ybS1maWVsZC1pbnB1dC10ZXh0LmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IEZvcm1GaWVsZElucHV0UGFzc3dvcmRDb21wb25lbnQgfSBmcm9tICcuL3VpL2Zvcm0tZmllbGQtaW5wdXQvZm9ybS1maWVsZC1pbnB1dC1wYXNzd29yZC9mb3JtLWZpZWxkLWlucHV0LXBhc3N3b3JkLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IEZvcm1GaWVsZElucHV0TnVtYmVyQ29tcG9uZW50IH0gZnJvbSAnLi91aS9mb3JtLWZpZWxkLWlucHV0L2Zvcm0tZmllbGQtaW5wdXQtbnVtYmVyL2Zvcm0tZmllbGQtaW5wdXQtbnVtYmVyLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IEZvcm1GaWVsZElucHV0RmlsZUNvbXBvbmVudCB9IGZyb20gJy4vdWkvZm9ybS1maWVsZC1pbnB1dC9mb3JtLWZpZWxkLWlucHV0LWZpbGUvZm9ybS1maWVsZC1pbnB1dC1maWxlLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IEZvcm1GaWVsZElucHV0RW1haWxDb21wb25lbnQgfSBmcm9tICcuL3VpL2Zvcm0tZmllbGQtaW5wdXQvZm9ybS1maWVsZC1pbnB1dC1lbWFpbC9mb3JtLWZpZWxkLWlucHV0LWVtYWlsLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IERhdGVwaWNrZXJDb21wb25lbnQgfSBmcm9tICcuL3VpL2RhdGVwaWNrZXIvZGF0ZXBpY2tlci5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBUZXh0YXJlYUNvbXBvbmVudCB9IGZyb20gJy4vdWkvdGV4dGFyZWEvdGV4dGFyZWEuY29tcG9uZW50JztcclxuaW1wb3J0IHsgU2VsZWN0Q29tcG9uZW50IH0gZnJvbSAnLi91aS9zZWxlY3Qvc2VsZWN0LmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFNsaWRlckNvbXBvbmVudCB9IGZyb20gJy4vdWkvc2xpZGVyL3NsaWRlci5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBTbGlkZVRvZ2dsZUNvbXBvbmVudCB9IGZyb20gJy4vdWkvc2xpZGUtdG9nZ2xlL3NsaWRlLXRvZ2dsZS5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBCdXR0b25Db21wb25lbnQgfSBmcm9tICcuL3VpL2J1dHRvbi9idXR0b24uY29tcG9uZW50JztcclxuaW1wb3J0IHsgQXV0b2NvbXBsZXRlQ29tcG9uZW50IH0gZnJvbSAnLi91aS9hdXRvY29tcGxldGUvYXV0b2NvbXBsZXRlLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IEF1dG9jb21wbGV0ZU1vZHVsZSB9IGZyb20gJ2F1dG9jb21wbGV0ZSc7XHJcbmltcG9ydCB7IFRpbWVwaWNrZXJDb21wb25lbnQgfSBmcm9tICcuL3VpL3RpbWVwaWNrZXIvdGltZXBpY2tlci5jb21wb25lbnQnO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICBkZWNsYXJhdGlvbnM6IFtcclxuICAgIFJhZGlvQnV0dG9uQ29tcG9uZW50LFxyXG4gICAgQ2hlY2tib3hDb21wb25lbnQsXHJcbiAgICBVcGxvYWREaWFsb2dDb21wb25lbnQsXHJcblxyXG4gICAgRHJhZ0Ryb3BEaXJlY3RpdmUsXHJcblxyXG4gICAgRm9ybUZpZWxkSW5wdXRUZXh0Q29tcG9uZW50LFxyXG4gICAgRm9ybUZpZWxkSW5wdXRQYXNzd29yZENvbXBvbmVudCxcclxuICAgIEZvcm1GaWVsZElucHV0TnVtYmVyQ29tcG9uZW50LFxyXG4gICAgRm9ybUZpZWxkSW5wdXRGaWxlQ29tcG9uZW50LFxyXG4gICAgRm9ybUZpZWxkSW5wdXRFbWFpbENvbXBvbmVudCxcclxuICAgIERhdGVwaWNrZXJDb21wb25lbnQsXHJcbiAgICBUZXh0YXJlYUNvbXBvbmVudCxcclxuICAgIFNlbGVjdENvbXBvbmVudCxcclxuICAgIFNsaWRlckNvbXBvbmVudCxcclxuICAgIFNsaWRlVG9nZ2xlQ29tcG9uZW50LFxyXG4gICAgQnV0dG9uQ29tcG9uZW50LFxyXG4gICAgQXV0b2NvbXBsZXRlQ29tcG9uZW50LFxyXG4gICAgVGltZXBpY2tlckNvbXBvbmVudCxcclxuICBdLFxyXG4gIGltcG9ydHM6IFtcclxuICAgIENvbW1vbk1vZHVsZSxcclxuICAgIEZvcm1zTW9kdWxlLFxyXG4gICAgUmVhY3RpdmVGb3Jtc01vZHVsZSxcclxuICAgIEZsZXhMYXlvdXRNb2R1bGUsXHJcbiAgICBNYXRlcmlhbE1vZHVsZSxcclxuICAgIEh0dHBDbGllbnRNb2R1bGUsXHJcbiAgICBEZXZpY2VEZXRlY3Rvck1vZHVsZS5mb3JSb290KCksXHJcbiAgICBBdXRvY29tcGxldGVNb2R1bGUsXHJcbiAgICBOZ3hNYXRlcmlhbFRpbWVwaWNrZXJNb2R1bGUsXHJcbiAgXSxcclxuICBleHBvcnRzOiBbXHJcbiAgICBDb21tb25Nb2R1bGUsXHJcbiAgICBGb3Jtc01vZHVsZSxcclxuICAgIFJlYWN0aXZlRm9ybXNNb2R1bGUsXHJcbiAgICBGbGV4TGF5b3V0TW9kdWxlLFxyXG4gICAgTWF0ZXJpYWxNb2R1bGUsXHJcbiAgICBIdHRwQ2xpZW50TW9kdWxlLFxyXG4gICAgRGV2aWNlRGV0ZWN0b3JNb2R1bGUsXHJcbiAgICBBdXRvY29tcGxldGVNb2R1bGUsXHJcbiAgICBOZ3hNYXRlcmlhbFRpbWVwaWNrZXJNb2R1bGUsXHJcblxyXG4gICAgUmFkaW9CdXR0b25Db21wb25lbnQsXHJcbiAgICBDaGVja2JveENvbXBvbmVudCxcclxuXHJcbiAgICBEcmFnRHJvcERpcmVjdGl2ZSxcclxuXHJcbiAgICBGb3JtRmllbGRJbnB1dFRleHRDb21wb25lbnQsXHJcbiAgICBGb3JtRmllbGRJbnB1dFBhc3N3b3JkQ29tcG9uZW50LFxyXG4gICAgRm9ybUZpZWxkSW5wdXROdW1iZXJDb21wb25lbnQsXHJcbiAgICBGb3JtRmllbGRJbnB1dEZpbGVDb21wb25lbnQsXHJcbiAgICBGb3JtRmllbGRJbnB1dEVtYWlsQ29tcG9uZW50LFxyXG4gICAgRGF0ZXBpY2tlckNvbXBvbmVudCxcclxuICAgIFRleHRhcmVhQ29tcG9uZW50LFxyXG4gICAgU2VsZWN0Q29tcG9uZW50LFxyXG4gICAgU2xpZGVyQ29tcG9uZW50LFxyXG4gICAgU2xpZGVUb2dnbGVDb21wb25lbnQsXHJcbiAgICBCdXR0b25Db21wb25lbnQsXHJcbiAgICBBdXRvY29tcGxldGVDb21wb25lbnQsXHJcbiAgICBUaW1lcGlja2VyQ29tcG9uZW50LFxyXG4gIF0sXHJcbiAgZW50cnlDb21wb25lbnRzOiBbVXBsb2FkRGlhbG9nQ29tcG9uZW50XSxcclxufSlcclxuZXhwb3J0IGNsYXNzIFNoYXJlZE1vZHVsZSB7fVxyXG4iXX0=