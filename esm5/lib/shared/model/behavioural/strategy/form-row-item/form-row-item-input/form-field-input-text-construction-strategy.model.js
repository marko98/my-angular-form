import { FormFieldInputText, } from "../../../../../model/structural/composite/form/form-field-input/form-field-input-text.model";
import { VALIDATOR_NAMES, } from "../../../../../model/structural/composite/form/form-row-item.model";
var FormFieldInputTextConstructionStrategy = /** @class */ (function () {
    function FormFieldInputTextConstructionStrategy() {
    }
    FormFieldInputTextConstructionStrategy.prototype.construct = function (formFieldInputTextInterface) {
        var formFieldInputText = new FormFieldInputText();
        formFieldInputText.setControlName(formFieldInputTextInterface.controlName);
        if (formFieldInputTextInterface.validators)
            formFieldInputText.setValidators(formFieldInputTextInterface.validators);
        if (formFieldInputTextInterface.showHintAboutMinMaxLength) {
            // check if maxLength or minLength validator exists
            var minOrMaxLengthValidatorExists_1 = false;
            formFieldInputText
                .getValidators()
                .forEach(function (validator) {
                if (validator.name == VALIDATOR_NAMES.MIN_LENGTH ||
                    validator.name == VALIDATOR_NAMES.MAX_LENGTH) {
                    minOrMaxLengthValidatorExists_1 = true;
                }
            });
            if (minOrMaxLengthValidatorExists_1) {
                (formFieldInputText).setShowHintAboutMinMaxLength(true);
            }
            else {
                throw Error("For showHintAboutMinMaxLength to work field needs to have minLength or maxLength validator");
            }
        }
        if (formFieldInputTextInterface.hintLabels) {
            if (formFieldInputTextInterface.hintLabels.leftHintLabelContext)
                formFieldInputText.setLeftHintLabel(formFieldInputTextInterface.hintLabels.leftHintLabelContext);
            if (formFieldInputTextInterface.hintLabels.rightHintLabelContext) {
                if ((formFieldInputText).getShowHintAboutMinMaxLength()) {
                    console.log(formFieldInputText.getControlName() +
                        " - Right Hint Label Context is not allowed cause you are using Show Hint About Min or Max Length");
                }
                else {
                    formFieldInputText.setRightHintLabel(formFieldInputTextInterface.hintLabels.rightHintLabelContext);
                }
            }
        }
        return formFieldInputText;
    };
    return FormFieldInputTextConstructionStrategy;
}());
export { FormFieldInputTextConstructionStrategy };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1maWVsZC1pbnB1dC10ZXh0LWNvbnN0cnVjdGlvbi1zdHJhdGVneS5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215LWFuZ3VsYXItZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvbW9kZWwvYmVoYXZpb3VyYWwvc3RyYXRlZ3kvZm9ybS1yb3ctaXRlbS9mb3JtLXJvdy1pdGVtLWlucHV0L2Zvcm0tZmllbGQtaW5wdXQtdGV4dC1jb25zdHJ1Y3Rpb24tc3RyYXRlZ3kubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxFQUVMLGtCQUFrQixHQUNuQixNQUFNLDZGQUE2RixDQUFDO0FBQ3JHLE9BQU8sRUFDTCxlQUFlLEdBR2hCLE1BQU0sb0VBQW9FLENBQUM7QUFHNUU7SUFBQTtJQStEQSxDQUFDO0lBN0RDLDBEQUFTLEdBQVQsVUFDRSwyQkFBd0Q7UUFFeEQsSUFBSSxrQkFBa0IsR0FBdUIsSUFBSSxrQkFBa0IsRUFBRSxDQUFDO1FBRXRFLGtCQUFrQixDQUFDLGNBQWMsQ0FBQywyQkFBMkIsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUUzRSxJQUFJLDJCQUEyQixDQUFDLFVBQVU7WUFDeEMsa0JBQWtCLENBQUMsYUFBYSxDQUFDLDJCQUEyQixDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBRTNFLElBQUksMkJBQTJCLENBQUMseUJBQXlCLEVBQUU7WUFDekQsbURBQW1EO1lBQ25ELElBQUksK0JBQTZCLEdBQUcsS0FBSyxDQUFDO1lBQzVCLGtCQUFtQjtpQkFDOUIsYUFBYSxFQUFFO2lCQUNmLE9BQU8sQ0FBQyxVQUFDLFNBQXdDO2dCQUNoRCxJQUNFLFNBQVMsQ0FBQyxJQUFJLElBQUksZUFBZSxDQUFDLFVBQVU7b0JBQzVDLFNBQVMsQ0FBQyxJQUFJLElBQUksZUFBZSxDQUFDLFVBQVUsRUFDNUM7b0JBQ0EsK0JBQTZCLEdBQUcsSUFBSSxDQUFDO2lCQUN0QztZQUNILENBQUMsQ0FBQyxDQUFDO1lBRUwsSUFBSSwrQkFBNkIsRUFBRTtnQkFDSyxDQUNwQyxrQkFBa0IsQ0FDbEIsQ0FBQyw0QkFBNEIsQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUN2QztpQkFBTTtnQkFDTCxNQUFNLEtBQUssQ0FDVCw0RkFBNEYsQ0FDN0YsQ0FBQzthQUNIO1NBQ0Y7UUFFRCxJQUFJLDJCQUEyQixDQUFDLFVBQVUsRUFBRTtZQUMxQyxJQUFJLDJCQUEyQixDQUFDLFVBQVUsQ0FBQyxvQkFBb0I7Z0JBQzdELGtCQUFrQixDQUFDLGdCQUFnQixDQUNqQywyQkFBMkIsQ0FBQyxVQUFVLENBQUMsb0JBQW9CLENBQzVELENBQUM7WUFFSixJQUFJLDJCQUEyQixDQUFDLFVBQVUsQ0FBQyxxQkFBcUIsRUFBRTtnQkFDaEUsSUFDd0MsQ0FDcEMsa0JBQWtCLENBQ2xCLENBQUMsNEJBQTRCLEVBQUUsRUFDakM7b0JBQ0EsT0FBTyxDQUFDLEdBQUcsQ0FDSyxrQkFBbUIsQ0FBQyxjQUFjLEVBQUU7d0JBQ2hELGtHQUFrRyxDQUNyRyxDQUFDO2lCQUNIO3FCQUFNO29CQUNMLGtCQUFrQixDQUFDLGlCQUFpQixDQUNsQywyQkFBMkIsQ0FBQyxVQUFVLENBQUMscUJBQXFCLENBQzdELENBQUM7aUJBQ0g7YUFDRjtTQUNGO1FBRUQsT0FBTyxrQkFBa0IsQ0FBQztJQUM1QixDQUFDO0lBQ0gsNkNBQUM7QUFBRCxDQUFDLEFBL0RELElBK0RDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRm9ybVJvd0l0ZW1Db25zdHJ1Y3Rpb25TdHJhdGVneSB9IGZyb20gXCIuLi9mb3JtLXJvdy1pdGVtLWNvbnN0cnVjdGlvbi1zdHJhdGVneS5pbnRlcmZhY2VcIjtcclxuaW1wb3J0IHtcclxuICBGb3JtRmllbGRJbnB1dFRleHRJbnRlcmZhY2UsXHJcbiAgRm9ybUZpZWxkSW5wdXRUZXh0LFxyXG59IGZyb20gXCIuLi8uLi8uLi8uLi8uLi9tb2RlbC9zdHJ1Y3R1cmFsL2NvbXBvc2l0ZS9mb3JtL2Zvcm0tZmllbGQtaW5wdXQvZm9ybS1maWVsZC1pbnB1dC10ZXh0Lm1vZGVsXCI7XHJcbmltcG9ydCB7XHJcbiAgVkFMSURBVE9SX05BTUVTLFxyXG4gIEZvcm1Sb3dJdGVtVmFsaWRhdG9ySW50ZXJmYWNlLFxyXG4gIEZvcm1Sb3dJdGVtLFxyXG59IGZyb20gXCIuLi8uLi8uLi8uLi8uLi9tb2RlbC9zdHJ1Y3R1cmFsL2NvbXBvc2l0ZS9mb3JtL2Zvcm0tcm93LWl0ZW0ubW9kZWxcIjtcclxuaW1wb3J0IHsgRm9ybUZpZWxkSW5wdXRUZXh0UGFzc3dvcmRJbnRlcmZhY2UgfSBmcm9tIFwiLi4vLi4vLi4vLi4vLi4vbW9kZWwvc3RydWN0dXJhbC9jb21wb3NpdGUvZm9ybS9mb3JtLWZpZWxkLWlucHV0L2Zvcm0tZmllbGQtaW5wdXQtdGV4dC1wYXNzd29yZC5pbnRlcmZhY2VcIjtcclxuXHJcbmV4cG9ydCBjbGFzcyBGb3JtRmllbGRJbnB1dFRleHRDb25zdHJ1Y3Rpb25TdHJhdGVneVxyXG4gIGltcGxlbWVudHMgRm9ybVJvd0l0ZW1Db25zdHJ1Y3Rpb25TdHJhdGVneSB7XHJcbiAgY29uc3RydWN0KFxyXG4gICAgZm9ybUZpZWxkSW5wdXRUZXh0SW50ZXJmYWNlOiBGb3JtRmllbGRJbnB1dFRleHRJbnRlcmZhY2VcclxuICApOiBGb3JtRmllbGRJbnB1dFRleHQge1xyXG4gICAgbGV0IGZvcm1GaWVsZElucHV0VGV4dDogRm9ybUZpZWxkSW5wdXRUZXh0ID0gbmV3IEZvcm1GaWVsZElucHV0VGV4dCgpO1xyXG5cclxuICAgIGZvcm1GaWVsZElucHV0VGV4dC5zZXRDb250cm9sTmFtZShmb3JtRmllbGRJbnB1dFRleHRJbnRlcmZhY2UuY29udHJvbE5hbWUpO1xyXG5cclxuICAgIGlmIChmb3JtRmllbGRJbnB1dFRleHRJbnRlcmZhY2UudmFsaWRhdG9ycylcclxuICAgICAgZm9ybUZpZWxkSW5wdXRUZXh0LnNldFZhbGlkYXRvcnMoZm9ybUZpZWxkSW5wdXRUZXh0SW50ZXJmYWNlLnZhbGlkYXRvcnMpO1xyXG5cclxuICAgIGlmIChmb3JtRmllbGRJbnB1dFRleHRJbnRlcmZhY2Uuc2hvd0hpbnRBYm91dE1pbk1heExlbmd0aCkge1xyXG4gICAgICAvLyBjaGVjayBpZiBtYXhMZW5ndGggb3IgbWluTGVuZ3RoIHZhbGlkYXRvciBleGlzdHNcclxuICAgICAgbGV0IG1pbk9yTWF4TGVuZ3RoVmFsaWRhdG9yRXhpc3RzID0gZmFsc2U7XHJcbiAgICAgICg8Rm9ybVJvd0l0ZW0+Zm9ybUZpZWxkSW5wdXRUZXh0KVxyXG4gICAgICAgIC5nZXRWYWxpZGF0b3JzKClcclxuICAgICAgICAuZm9yRWFjaCgodmFsaWRhdG9yOiBGb3JtUm93SXRlbVZhbGlkYXRvckludGVyZmFjZSkgPT4ge1xyXG4gICAgICAgICAgaWYgKFxyXG4gICAgICAgICAgICB2YWxpZGF0b3IubmFtZSA9PSBWQUxJREFUT1JfTkFNRVMuTUlOX0xFTkdUSCB8fFxyXG4gICAgICAgICAgICB2YWxpZGF0b3IubmFtZSA9PSBWQUxJREFUT1JfTkFNRVMuTUFYX0xFTkdUSFxyXG4gICAgICAgICAgKSB7XHJcbiAgICAgICAgICAgIG1pbk9yTWF4TGVuZ3RoVmFsaWRhdG9yRXhpc3RzID0gdHJ1ZTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgIGlmIChtaW5Pck1heExlbmd0aFZhbGlkYXRvckV4aXN0cykge1xyXG4gICAgICAgICg8Rm9ybUZpZWxkSW5wdXRUZXh0UGFzc3dvcmRJbnRlcmZhY2U+KFxyXG4gICAgICAgICAgZm9ybUZpZWxkSW5wdXRUZXh0XHJcbiAgICAgICAgKSkuc2V0U2hvd0hpbnRBYm91dE1pbk1heExlbmd0aCh0cnVlKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aHJvdyBFcnJvcihcclxuICAgICAgICAgIFwiRm9yIHNob3dIaW50QWJvdXRNaW5NYXhMZW5ndGggdG8gd29yayBmaWVsZCBuZWVkcyB0byBoYXZlIG1pbkxlbmd0aCBvciBtYXhMZW5ndGggdmFsaWRhdG9yXCJcclxuICAgICAgICApO1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKGZvcm1GaWVsZElucHV0VGV4dEludGVyZmFjZS5oaW50TGFiZWxzKSB7XHJcbiAgICAgIGlmIChmb3JtRmllbGRJbnB1dFRleHRJbnRlcmZhY2UuaGludExhYmVscy5sZWZ0SGludExhYmVsQ29udGV4dClcclxuICAgICAgICBmb3JtRmllbGRJbnB1dFRleHQuc2V0TGVmdEhpbnRMYWJlbChcclxuICAgICAgICAgIGZvcm1GaWVsZElucHV0VGV4dEludGVyZmFjZS5oaW50TGFiZWxzLmxlZnRIaW50TGFiZWxDb250ZXh0XHJcbiAgICAgICAgKTtcclxuXHJcbiAgICAgIGlmIChmb3JtRmllbGRJbnB1dFRleHRJbnRlcmZhY2UuaGludExhYmVscy5yaWdodEhpbnRMYWJlbENvbnRleHQpIHtcclxuICAgICAgICBpZiAoXHJcbiAgICAgICAgICAoPEZvcm1GaWVsZElucHV0VGV4dFBhc3N3b3JkSW50ZXJmYWNlPihcclxuICAgICAgICAgICAgZm9ybUZpZWxkSW5wdXRUZXh0XHJcbiAgICAgICAgICApKS5nZXRTaG93SGludEFib3V0TWluTWF4TGVuZ3RoKClcclxuICAgICAgICApIHtcclxuICAgICAgICAgIGNvbnNvbGUubG9nKFxyXG4gICAgICAgICAgICAoPEZvcm1Sb3dJdGVtPmZvcm1GaWVsZElucHV0VGV4dCkuZ2V0Q29udHJvbE5hbWUoKSArXHJcbiAgICAgICAgICAgICAgXCIgLSBSaWdodCBIaW50IExhYmVsIENvbnRleHQgaXMgbm90IGFsbG93ZWQgY2F1c2UgeW91IGFyZSB1c2luZyBTaG93IEhpbnQgQWJvdXQgTWluIG9yIE1heCBMZW5ndGhcIlxyXG4gICAgICAgICAgKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgZm9ybUZpZWxkSW5wdXRUZXh0LnNldFJpZ2h0SGludExhYmVsKFxyXG4gICAgICAgICAgICBmb3JtRmllbGRJbnB1dFRleHRJbnRlcmZhY2UuaGludExhYmVscy5yaWdodEhpbnRMYWJlbENvbnRleHRcclxuICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIGZvcm1GaWVsZElucHV0VGV4dDtcclxuICB9XHJcbn1cclxuIl19