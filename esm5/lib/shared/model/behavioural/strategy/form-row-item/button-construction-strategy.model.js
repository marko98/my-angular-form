import { Button, BUTTON_TYPE, MAT_BUTTON_TYPE, } from '../../../structural/composite/form/button/button.model';
import { isFunction } from 'util';
var ButtonConstructionStrategy = /** @class */ (function () {
    function ButtonConstructionStrategy() {
    }
    ButtonConstructionStrategy.prototype.construct = function (buttonInterface) {
        var button = new Button();
        button.setType(buttonInterface.type);
        if (buttonInterface.defaultValue != '')
            throw new Error('Default value is not allowed on button');
        button.setControlName(buttonInterface.controlName);
        if (buttonInterface.validators)
            throw new Error('Validators are not allowed on button');
        if (buttonInterface.color)
            button.setColor(buttonInterface.color);
        if (buttonInterface.disabled)
            button.setDisabled(buttonInterface.disabled);
        if (buttonInterface.context)
            button.setContext(buttonInterface.context);
        if (buttonInterface.matButtonType)
            button.setMatButtonType(buttonInterface.matButtonType);
        if (buttonInterface.buttonType)
            button.setButtonType(buttonInterface.buttonType);
        if (buttonInterface.functionToExecute) {
            if (button.getButtonType() !== BUTTON_TYPE.BUTTON)
                throw new Error('Function is only allowed on button of type button');
            else {
                if (isFunction(buttonInterface.functionToExecute))
                    button.setFunctionToExecute(buttonInterface.functionToExecute);
            }
        }
        if (buttonInterface.matIconString) {
            if (button.getMatButtonType() === MAT_BUTTON_TYPE.ICON ||
                button.getMatButtonType() === MAT_BUTTON_TYPE.FAB ||
                button.getMatButtonType() === MAT_BUTTON_TYPE.MINI_FAB) {
                button.setMatIconString(buttonInterface.matIconString);
            }
            else {
                throw new Error('Mat icon string is only allowed on button of matButtonYype ICON|FAB|MINI_FAB');
            }
        }
        return button;
    };
    return ButtonConstructionStrategy;
}());
export { ButtonConstructionStrategy };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnV0dG9uLWNvbnN0cnVjdGlvbi1zdHJhdGVneS5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215LWFuZ3VsYXItZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvbW9kZWwvYmVoYXZpb3VyYWwvc3RyYXRlZ3kvZm9ybS1yb3ctaXRlbS9idXR0b24tY29uc3RydWN0aW9uLXN0cmF0ZWd5Lm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBLE9BQU8sRUFFTCxNQUFNLEVBQ04sV0FBVyxFQUNYLGVBQWUsR0FDaEIsTUFBTSx3REFBd0QsQ0FBQztBQUNoRSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBRWxDO0lBQUE7SUFzREEsQ0FBQztJQXBEQyw4Q0FBUyxHQUFULFVBQVUsZUFBZ0M7UUFDeEMsSUFBSSxNQUFNLEdBQUcsSUFBSSxNQUFNLEVBQUUsQ0FBQztRQUUxQixNQUFNLENBQUMsT0FBTyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUVyQyxJQUFJLGVBQWUsQ0FBQyxZQUFZLElBQUksRUFBRTtZQUNwQyxNQUFNLElBQUksS0FBSyxDQUFDLHdDQUF3QyxDQUFDLENBQUM7UUFFNUQsTUFBTSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLENBQUM7UUFFbkQsSUFBSSxlQUFlLENBQUMsVUFBVTtZQUM1QixNQUFNLElBQUksS0FBSyxDQUFDLHNDQUFzQyxDQUFDLENBQUM7UUFFMUQsSUFBSSxlQUFlLENBQUMsS0FBSztZQUFFLE1BQU0sQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBRWxFLElBQUksZUFBZSxDQUFDLFFBQVE7WUFBRSxNQUFNLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUUzRSxJQUFJLGVBQWUsQ0FBQyxPQUFPO1lBQUUsTUFBTSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLENBQUM7UUFFeEUsSUFBSSxlQUFlLENBQUMsYUFBYTtZQUMvQixNQUFNLENBQUMsZ0JBQWdCLENBQUMsZUFBZSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBRXpELElBQUksZUFBZSxDQUFDLFVBQVU7WUFDNUIsTUFBTSxDQUFDLGFBQWEsQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLENBQUM7UUFFbkQsSUFBSSxlQUFlLENBQUMsaUJBQWlCLEVBQUU7WUFDckMsSUFBSSxNQUFNLENBQUMsYUFBYSxFQUFFLEtBQUssV0FBVyxDQUFDLE1BQU07Z0JBQy9DLE1BQU0sSUFBSSxLQUFLLENBQUMsbURBQW1ELENBQUMsQ0FBQztpQkFDbEU7Z0JBQ0gsSUFBSSxVQUFVLENBQUMsZUFBZSxDQUFDLGlCQUFpQixDQUFDO29CQUMvQyxNQUFNLENBQUMsb0JBQW9CLENBQ2YsZUFBZSxDQUFDLGlCQUFpQixDQUM1QyxDQUFDO2FBQ0w7U0FDRjtRQUVELElBQUksZUFBZSxDQUFDLGFBQWEsRUFBRTtZQUNqQyxJQUNFLE1BQU0sQ0FBQyxnQkFBZ0IsRUFBRSxLQUFLLGVBQWUsQ0FBQyxJQUFJO2dCQUNsRCxNQUFNLENBQUMsZ0JBQWdCLEVBQUUsS0FBSyxlQUFlLENBQUMsR0FBRztnQkFDakQsTUFBTSxDQUFDLGdCQUFnQixFQUFFLEtBQUssZUFBZSxDQUFDLFFBQVEsRUFDdEQ7Z0JBQ0EsTUFBTSxDQUFDLGdCQUFnQixDQUFDLGVBQWUsQ0FBQyxhQUFhLENBQUMsQ0FBQzthQUN4RDtpQkFBTTtnQkFDTCxNQUFNLElBQUksS0FBSyxDQUNiLDhFQUE4RSxDQUMvRSxDQUFDO2FBQ0g7U0FDRjtRQUVELE9BQU8sTUFBTSxDQUFDO0lBQ2hCLENBQUM7SUFDSCxpQ0FBQztBQUFELENBQUMsQUF0REQsSUFzREMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBGb3JtUm93SXRlbUNvbnN0cnVjdGlvblN0cmF0ZWd5IH0gZnJvbSAnLi9mb3JtLXJvdy1pdGVtLWNvbnN0cnVjdGlvbi1zdHJhdGVneS5pbnRlcmZhY2UnO1xyXG5pbXBvcnQge1xyXG4gIEJ1dHRvbkludGVyZmFjZSxcclxuICBCdXR0b24sXHJcbiAgQlVUVE9OX1RZUEUsXHJcbiAgTUFUX0JVVFRPTl9UWVBFLFxyXG59IGZyb20gJy4uLy4uLy4uL3N0cnVjdHVyYWwvY29tcG9zaXRlL2Zvcm0vYnV0dG9uL2J1dHRvbi5tb2RlbCc7XHJcbmltcG9ydCB7IGlzRnVuY3Rpb24gfSBmcm9tICd1dGlsJztcclxuXHJcbmV4cG9ydCBjbGFzcyBCdXR0b25Db25zdHJ1Y3Rpb25TdHJhdGVneVxyXG4gIGltcGxlbWVudHMgRm9ybVJvd0l0ZW1Db25zdHJ1Y3Rpb25TdHJhdGVneSB7XHJcbiAgY29uc3RydWN0KGJ1dHRvbkludGVyZmFjZTogQnV0dG9uSW50ZXJmYWNlKTogQnV0dG9uIHtcclxuICAgIGxldCBidXR0b24gPSBuZXcgQnV0dG9uKCk7XHJcblxyXG4gICAgYnV0dG9uLnNldFR5cGUoYnV0dG9uSW50ZXJmYWNlLnR5cGUpO1xyXG5cclxuICAgIGlmIChidXR0b25JbnRlcmZhY2UuZGVmYXVsdFZhbHVlICE9ICcnKVxyXG4gICAgICB0aHJvdyBuZXcgRXJyb3IoJ0RlZmF1bHQgdmFsdWUgaXMgbm90IGFsbG93ZWQgb24gYnV0dG9uJyk7XHJcblxyXG4gICAgYnV0dG9uLnNldENvbnRyb2xOYW1lKGJ1dHRvbkludGVyZmFjZS5jb250cm9sTmFtZSk7XHJcblxyXG4gICAgaWYgKGJ1dHRvbkludGVyZmFjZS52YWxpZGF0b3JzKVxyXG4gICAgICB0aHJvdyBuZXcgRXJyb3IoJ1ZhbGlkYXRvcnMgYXJlIG5vdCBhbGxvd2VkIG9uIGJ1dHRvbicpO1xyXG5cclxuICAgIGlmIChidXR0b25JbnRlcmZhY2UuY29sb3IpIGJ1dHRvbi5zZXRDb2xvcihidXR0b25JbnRlcmZhY2UuY29sb3IpO1xyXG5cclxuICAgIGlmIChidXR0b25JbnRlcmZhY2UuZGlzYWJsZWQpIGJ1dHRvbi5zZXREaXNhYmxlZChidXR0b25JbnRlcmZhY2UuZGlzYWJsZWQpO1xyXG5cclxuICAgIGlmIChidXR0b25JbnRlcmZhY2UuY29udGV4dCkgYnV0dG9uLnNldENvbnRleHQoYnV0dG9uSW50ZXJmYWNlLmNvbnRleHQpO1xyXG5cclxuICAgIGlmIChidXR0b25JbnRlcmZhY2UubWF0QnV0dG9uVHlwZSlcclxuICAgICAgYnV0dG9uLnNldE1hdEJ1dHRvblR5cGUoYnV0dG9uSW50ZXJmYWNlLm1hdEJ1dHRvblR5cGUpO1xyXG5cclxuICAgIGlmIChidXR0b25JbnRlcmZhY2UuYnV0dG9uVHlwZSlcclxuICAgICAgYnV0dG9uLnNldEJ1dHRvblR5cGUoYnV0dG9uSW50ZXJmYWNlLmJ1dHRvblR5cGUpO1xyXG5cclxuICAgIGlmIChidXR0b25JbnRlcmZhY2UuZnVuY3Rpb25Ub0V4ZWN1dGUpIHtcclxuICAgICAgaWYgKGJ1dHRvbi5nZXRCdXR0b25UeXBlKCkgIT09IEJVVFRPTl9UWVBFLkJVVFRPTilcclxuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ0Z1bmN0aW9uIGlzIG9ubHkgYWxsb3dlZCBvbiBidXR0b24gb2YgdHlwZSBidXR0b24nKTtcclxuICAgICAgZWxzZSB7XHJcbiAgICAgICAgaWYgKGlzRnVuY3Rpb24oYnV0dG9uSW50ZXJmYWNlLmZ1bmN0aW9uVG9FeGVjdXRlKSlcclxuICAgICAgICAgIGJ1dHRvbi5zZXRGdW5jdGlvblRvRXhlY3V0ZShcclxuICAgICAgICAgICAgPEZ1bmN0aW9uPmJ1dHRvbkludGVyZmFjZS5mdW5jdGlvblRvRXhlY3V0ZVxyXG4gICAgICAgICAgKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGlmIChidXR0b25JbnRlcmZhY2UubWF0SWNvblN0cmluZykge1xyXG4gICAgICBpZiAoXHJcbiAgICAgICAgYnV0dG9uLmdldE1hdEJ1dHRvblR5cGUoKSA9PT0gTUFUX0JVVFRPTl9UWVBFLklDT04gfHxcclxuICAgICAgICBidXR0b24uZ2V0TWF0QnV0dG9uVHlwZSgpID09PSBNQVRfQlVUVE9OX1RZUEUuRkFCIHx8XHJcbiAgICAgICAgYnV0dG9uLmdldE1hdEJ1dHRvblR5cGUoKSA9PT0gTUFUX0JVVFRPTl9UWVBFLk1JTklfRkFCXHJcbiAgICAgICkge1xyXG4gICAgICAgIGJ1dHRvbi5zZXRNYXRJY29uU3RyaW5nKGJ1dHRvbkludGVyZmFjZS5tYXRJY29uU3RyaW5nKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXHJcbiAgICAgICAgICAnTWF0IGljb24gc3RyaW5nIGlzIG9ubHkgYWxsb3dlZCBvbiBidXR0b24gb2YgbWF0QnV0dG9uWXlwZSBJQ09OfEZBQnxNSU5JX0ZBQidcclxuICAgICAgICApO1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIGJ1dHRvbjtcclxuICB9XHJcbn1cclxuIl19