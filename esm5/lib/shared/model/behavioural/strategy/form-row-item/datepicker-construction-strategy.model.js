import { Datepicker, } from '../../../structural/composite/form/datepicker/datepicker.model';
import { VALIDATOR_NAMES, } from '../../../structural/composite/form/form-row-item.model';
var DatepickerConstructionStrategy = /** @class */ (function () {
    function DatepickerConstructionStrategy() {
    }
    DatepickerConstructionStrategy.prototype.construct = function (datepickerInterface) {
        var datepicker = new Datepicker();
        datepicker.setType(datepickerInterface.type);
        datepicker.setControlName(datepickerInterface.controlName);
        if (datepickerInterface.appearance)
            datepicker.setAppearance(datepickerInterface.appearance);
        if (datepickerInterface.disabledDays)
            datepicker.setDisabledDays(datepickerInterface.disabledDays);
        if (datepickerInterface.defaultValue) {
            if (datepickerInterface.min) {
                if (datepickerInterface.defaultValue < datepickerInterface.min) {
                    throw Error('Default value Date must be at or after min Date');
                }
            }
            if (datepickerInterface.max) {
                if (datepickerInterface.defaultValue > datepickerInterface.max) {
                    throw Error('Default value Date must be at or before max Date');
                }
            }
        }
        datepicker.setDefaultValue(datepickerInterface.defaultValue);
        if (datepickerInterface.validators) {
            datepickerInterface.validators.forEach(function (validator) {
                if (validator.validatorFn.name === VALIDATOR_NAMES.REQUIRED) {
                    datepicker.addValidator(validator);
                }
            });
        }
        if (datepickerInterface.min) {
            if (datepickerInterface.max) {
                if (datepickerInterface.min > datepickerInterface.max) {
                    throw Error('min Date must be at or before max Date');
                }
            }
        }
        datepicker.setMin(datepickerInterface.min);
        if (datepickerInterface.max) {
            if (datepickerInterface.min) {
                if (datepickerInterface.max < datepickerInterface.min) {
                    throw Error('max Date must be at or after min Date');
                }
            }
        }
        datepicker.setMax(datepickerInterface.max);
        if (datepickerInterface.startAt) {
            if (datepickerInterface.min) {
                if (datepickerInterface.startAt < datepickerInterface.min) {
                    throw Error('StartAt value Date must be at or after min Date');
                }
            }
            if (datepickerInterface.max) {
                if (datepickerInterface.startAt > datepickerInterface.max) {
                    throw Error('StartAt value Date must be at or before max Date');
                }
            }
        }
        datepicker.setStartAt(datepickerInterface.startAt);
        if (datepickerInterface.disabled)
            datepicker.setDisabled(datepickerInterface.disabled);
        if (datepickerInterface.startView)
            datepicker.setStartView(datepickerInterface.startView);
        if (datepickerInterface.color)
            datepicker.setColor(datepickerInterface.color);
        if (datepickerInterface.toggleSideSuffix !== undefined)
            datepicker.setToggleSideSuffix(datepickerInterface.toggleSideSuffix);
        if (datepickerInterface.matIcon)
            datepicker.setMatIcon(datepickerInterface.matIcon);
        if (datepickerInterface.labelName)
            datepicker.setLabelName(datepickerInterface.labelName);
        else
            throw Error('Label name is required');
        if (datepickerInterface.timepicker)
            datepicker.setTimepickerInsideDatepickerInterface(datepickerInterface.timepicker);
        return datepicker;
    };
    return DatepickerConstructionStrategy;
}());
export { DatepickerConstructionStrategy };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0ZXBpY2tlci1jb25zdHJ1Y3Rpb24tc3RyYXRlZ3kubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9teS1hbmd1bGFyLWZvcm0vIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL21vZGVsL2JlaGF2aW91cmFsL3N0cmF0ZWd5L2Zvcm0tcm93LWl0ZW0vZGF0ZXBpY2tlci1jb25zdHJ1Y3Rpb24tc3RyYXRlZ3kubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxFQUNMLFVBQVUsR0FFWCxNQUFNLGdFQUFnRSxDQUFDO0FBQ3hFLE9BQU8sRUFFTCxlQUFlLEdBQ2hCLE1BQU0sd0RBQXdELENBQUM7QUFFaEU7SUFBQTtJQStGQSxDQUFDO0lBN0ZDLGtEQUFTLEdBQVQsVUFBVSxtQkFBd0M7UUFDaEQsSUFBSSxVQUFVLEdBQUcsSUFBSSxVQUFVLEVBQUUsQ0FBQztRQUVsQyxVQUFVLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxDQUFDO1FBRTdDLFVBQVUsQ0FBQyxjQUFjLENBQUMsbUJBQW1CLENBQUMsV0FBVyxDQUFDLENBQUM7UUFFM0QsSUFBSSxtQkFBbUIsQ0FBQyxVQUFVO1lBQ2hDLFVBQVUsQ0FBQyxhQUFhLENBQUMsbUJBQW1CLENBQUMsVUFBVSxDQUFDLENBQUM7UUFFM0QsSUFBSSxtQkFBbUIsQ0FBQyxZQUFZO1lBQ2xDLFVBQVUsQ0FBQyxlQUFlLENBQUMsbUJBQW1CLENBQUMsWUFBWSxDQUFDLENBQUM7UUFFL0QsSUFBSSxtQkFBbUIsQ0FBQyxZQUFZLEVBQUU7WUFDcEMsSUFBSSxtQkFBbUIsQ0FBQyxHQUFHLEVBQUU7Z0JBQzNCLElBQUksbUJBQW1CLENBQUMsWUFBWSxHQUFHLG1CQUFtQixDQUFDLEdBQUcsRUFBRTtvQkFDOUQsTUFBTSxLQUFLLENBQUMsaURBQWlELENBQUMsQ0FBQztpQkFDaEU7YUFDRjtZQUVELElBQUksbUJBQW1CLENBQUMsR0FBRyxFQUFFO2dCQUMzQixJQUFJLG1CQUFtQixDQUFDLFlBQVksR0FBRyxtQkFBbUIsQ0FBQyxHQUFHLEVBQUU7b0JBQzlELE1BQU0sS0FBSyxDQUFDLGtEQUFrRCxDQUFDLENBQUM7aUJBQ2pFO2FBQ0Y7U0FDRjtRQUNELFVBQVUsQ0FBQyxlQUFlLENBQUMsbUJBQW1CLENBQUMsWUFBWSxDQUFDLENBQUM7UUFFN0QsSUFBSSxtQkFBbUIsQ0FBQyxVQUFVLEVBQUU7WUFDbEMsbUJBQW1CLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FDcEMsVUFBQyxTQUF3QztnQkFDdkMsSUFBSSxTQUFTLENBQUMsV0FBVyxDQUFDLElBQUksS0FBSyxlQUFlLENBQUMsUUFBUSxFQUFFO29CQUMzRCxVQUFVLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2lCQUNwQztZQUNILENBQUMsQ0FDRixDQUFDO1NBQ0g7UUFFRCxJQUFJLG1CQUFtQixDQUFDLEdBQUcsRUFBRTtZQUMzQixJQUFJLG1CQUFtQixDQUFDLEdBQUcsRUFBRTtnQkFDM0IsSUFBSSxtQkFBbUIsQ0FBQyxHQUFHLEdBQUcsbUJBQW1CLENBQUMsR0FBRyxFQUFFO29CQUNyRCxNQUFNLEtBQUssQ0FBQyx3Q0FBd0MsQ0FBQyxDQUFDO2lCQUN2RDthQUNGO1NBQ0Y7UUFDRCxVQUFVLENBQUMsTUFBTSxDQUFDLG1CQUFtQixDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBRTNDLElBQUksbUJBQW1CLENBQUMsR0FBRyxFQUFFO1lBQzNCLElBQUksbUJBQW1CLENBQUMsR0FBRyxFQUFFO2dCQUMzQixJQUFJLG1CQUFtQixDQUFDLEdBQUcsR0FBRyxtQkFBbUIsQ0FBQyxHQUFHLEVBQUU7b0JBQ3JELE1BQU0sS0FBSyxDQUFDLHVDQUF1QyxDQUFDLENBQUM7aUJBQ3REO2FBQ0Y7U0FDRjtRQUNELFVBQVUsQ0FBQyxNQUFNLENBQUMsbUJBQW1CLENBQUMsR0FBRyxDQUFDLENBQUM7UUFFM0MsSUFBSSxtQkFBbUIsQ0FBQyxPQUFPLEVBQUU7WUFDL0IsSUFBSSxtQkFBbUIsQ0FBQyxHQUFHLEVBQUU7Z0JBQzNCLElBQUksbUJBQW1CLENBQUMsT0FBTyxHQUFHLG1CQUFtQixDQUFDLEdBQUcsRUFBRTtvQkFDekQsTUFBTSxLQUFLLENBQUMsaURBQWlELENBQUMsQ0FBQztpQkFDaEU7YUFDRjtZQUVELElBQUksbUJBQW1CLENBQUMsR0FBRyxFQUFFO2dCQUMzQixJQUFJLG1CQUFtQixDQUFDLE9BQU8sR0FBRyxtQkFBbUIsQ0FBQyxHQUFHLEVBQUU7b0JBQ3pELE1BQU0sS0FBSyxDQUFDLGtEQUFrRCxDQUFDLENBQUM7aUJBQ2pFO2FBQ0Y7U0FDRjtRQUNELFVBQVUsQ0FBQyxVQUFVLENBQUMsbUJBQW1CLENBQUMsT0FBTyxDQUFDLENBQUM7UUFFbkQsSUFBSSxtQkFBbUIsQ0FBQyxRQUFRO1lBQzlCLFVBQVUsQ0FBQyxXQUFXLENBQUMsbUJBQW1CLENBQUMsUUFBUSxDQUFDLENBQUM7UUFFdkQsSUFBSSxtQkFBbUIsQ0FBQyxTQUFTO1lBQy9CLFVBQVUsQ0FBQyxZQUFZLENBQUMsbUJBQW1CLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDekQsSUFBSSxtQkFBbUIsQ0FBQyxLQUFLO1lBQzNCLFVBQVUsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDakQsSUFBSSxtQkFBbUIsQ0FBQyxnQkFBZ0IsS0FBSyxTQUFTO1lBQ3BELFVBQVUsQ0FBQyxtQkFBbUIsQ0FBQyxtQkFBbUIsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1FBQ3ZFLElBQUksbUJBQW1CLENBQUMsT0FBTztZQUM3QixVQUFVLENBQUMsVUFBVSxDQUFDLG1CQUFtQixDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3JELElBQUksbUJBQW1CLENBQUMsU0FBUztZQUMvQixVQUFVLENBQUMsWUFBWSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxDQUFDOztZQUNwRCxNQUFNLEtBQUssQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO1FBRTNDLElBQUksbUJBQW1CLENBQUMsVUFBVTtZQUNoQyxVQUFVLENBQUMsc0NBQXNDLENBQy9DLG1CQUFtQixDQUFDLFVBQVUsQ0FDL0IsQ0FBQztRQUVKLE9BQU8sVUFBVSxDQUFDO0lBQ3BCLENBQUM7SUFDSCxxQ0FBQztBQUFELENBQUMsQUEvRkQsSUErRkMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBGb3JtUm93SXRlbUNvbnN0cnVjdGlvblN0cmF0ZWd5IH0gZnJvbSAnLi9mb3JtLXJvdy1pdGVtLWNvbnN0cnVjdGlvbi1zdHJhdGVneS5pbnRlcmZhY2UnO1xyXG5pbXBvcnQge1xyXG4gIERhdGVwaWNrZXIsXHJcbiAgRGF0ZXBpY2tlckludGVyZmFjZSxcclxufSBmcm9tICcuLi8uLi8uLi9zdHJ1Y3R1cmFsL2NvbXBvc2l0ZS9mb3JtL2RhdGVwaWNrZXIvZGF0ZXBpY2tlci5tb2RlbCc7XHJcbmltcG9ydCB7XHJcbiAgRm9ybVJvd0l0ZW1WYWxpZGF0b3JJbnRlcmZhY2UsXHJcbiAgVkFMSURBVE9SX05BTUVTLFxyXG59IGZyb20gJy4uLy4uLy4uL3N0cnVjdHVyYWwvY29tcG9zaXRlL2Zvcm0vZm9ybS1yb3ctaXRlbS5tb2RlbCc7XHJcblxyXG5leHBvcnQgY2xhc3MgRGF0ZXBpY2tlckNvbnN0cnVjdGlvblN0cmF0ZWd5XHJcbiAgaW1wbGVtZW50cyBGb3JtUm93SXRlbUNvbnN0cnVjdGlvblN0cmF0ZWd5IHtcclxuICBjb25zdHJ1Y3QoZGF0ZXBpY2tlckludGVyZmFjZTogRGF0ZXBpY2tlckludGVyZmFjZSk6IERhdGVwaWNrZXIge1xyXG4gICAgbGV0IGRhdGVwaWNrZXIgPSBuZXcgRGF0ZXBpY2tlcigpO1xyXG5cclxuICAgIGRhdGVwaWNrZXIuc2V0VHlwZShkYXRlcGlja2VySW50ZXJmYWNlLnR5cGUpO1xyXG5cclxuICAgIGRhdGVwaWNrZXIuc2V0Q29udHJvbE5hbWUoZGF0ZXBpY2tlckludGVyZmFjZS5jb250cm9sTmFtZSk7XHJcblxyXG4gICAgaWYgKGRhdGVwaWNrZXJJbnRlcmZhY2UuYXBwZWFyYW5jZSlcclxuICAgICAgZGF0ZXBpY2tlci5zZXRBcHBlYXJhbmNlKGRhdGVwaWNrZXJJbnRlcmZhY2UuYXBwZWFyYW5jZSk7XHJcblxyXG4gICAgaWYgKGRhdGVwaWNrZXJJbnRlcmZhY2UuZGlzYWJsZWREYXlzKVxyXG4gICAgICBkYXRlcGlja2VyLnNldERpc2FibGVkRGF5cyhkYXRlcGlja2VySW50ZXJmYWNlLmRpc2FibGVkRGF5cyk7XHJcblxyXG4gICAgaWYgKGRhdGVwaWNrZXJJbnRlcmZhY2UuZGVmYXVsdFZhbHVlKSB7XHJcbiAgICAgIGlmIChkYXRlcGlja2VySW50ZXJmYWNlLm1pbikge1xyXG4gICAgICAgIGlmIChkYXRlcGlja2VySW50ZXJmYWNlLmRlZmF1bHRWYWx1ZSA8IGRhdGVwaWNrZXJJbnRlcmZhY2UubWluKSB7XHJcbiAgICAgICAgICB0aHJvdyBFcnJvcignRGVmYXVsdCB2YWx1ZSBEYXRlIG11c3QgYmUgYXQgb3IgYWZ0ZXIgbWluIERhdGUnKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmIChkYXRlcGlja2VySW50ZXJmYWNlLm1heCkge1xyXG4gICAgICAgIGlmIChkYXRlcGlja2VySW50ZXJmYWNlLmRlZmF1bHRWYWx1ZSA+IGRhdGVwaWNrZXJJbnRlcmZhY2UubWF4KSB7XHJcbiAgICAgICAgICB0aHJvdyBFcnJvcignRGVmYXVsdCB2YWx1ZSBEYXRlIG11c3QgYmUgYXQgb3IgYmVmb3JlIG1heCBEYXRlJyk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICBkYXRlcGlja2VyLnNldERlZmF1bHRWYWx1ZShkYXRlcGlja2VySW50ZXJmYWNlLmRlZmF1bHRWYWx1ZSk7XHJcblxyXG4gICAgaWYgKGRhdGVwaWNrZXJJbnRlcmZhY2UudmFsaWRhdG9ycykge1xyXG4gICAgICBkYXRlcGlja2VySW50ZXJmYWNlLnZhbGlkYXRvcnMuZm9yRWFjaChcclxuICAgICAgICAodmFsaWRhdG9yOiBGb3JtUm93SXRlbVZhbGlkYXRvckludGVyZmFjZSkgPT4ge1xyXG4gICAgICAgICAgaWYgKHZhbGlkYXRvci52YWxpZGF0b3JGbi5uYW1lID09PSBWQUxJREFUT1JfTkFNRVMuUkVRVUlSRUQpIHtcclxuICAgICAgICAgICAgZGF0ZXBpY2tlci5hZGRWYWxpZGF0b3IodmFsaWRhdG9yKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKGRhdGVwaWNrZXJJbnRlcmZhY2UubWluKSB7XHJcbiAgICAgIGlmIChkYXRlcGlja2VySW50ZXJmYWNlLm1heCkge1xyXG4gICAgICAgIGlmIChkYXRlcGlja2VySW50ZXJmYWNlLm1pbiA+IGRhdGVwaWNrZXJJbnRlcmZhY2UubWF4KSB7XHJcbiAgICAgICAgICB0aHJvdyBFcnJvcignbWluIERhdGUgbXVzdCBiZSBhdCBvciBiZWZvcmUgbWF4IERhdGUnKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIGRhdGVwaWNrZXIuc2V0TWluKGRhdGVwaWNrZXJJbnRlcmZhY2UubWluKTtcclxuXHJcbiAgICBpZiAoZGF0ZXBpY2tlckludGVyZmFjZS5tYXgpIHtcclxuICAgICAgaWYgKGRhdGVwaWNrZXJJbnRlcmZhY2UubWluKSB7XHJcbiAgICAgICAgaWYgKGRhdGVwaWNrZXJJbnRlcmZhY2UubWF4IDwgZGF0ZXBpY2tlckludGVyZmFjZS5taW4pIHtcclxuICAgICAgICAgIHRocm93IEVycm9yKCdtYXggRGF0ZSBtdXN0IGJlIGF0IG9yIGFmdGVyIG1pbiBEYXRlJyk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICBkYXRlcGlja2VyLnNldE1heChkYXRlcGlja2VySW50ZXJmYWNlLm1heCk7XHJcblxyXG4gICAgaWYgKGRhdGVwaWNrZXJJbnRlcmZhY2Uuc3RhcnRBdCkge1xyXG4gICAgICBpZiAoZGF0ZXBpY2tlckludGVyZmFjZS5taW4pIHtcclxuICAgICAgICBpZiAoZGF0ZXBpY2tlckludGVyZmFjZS5zdGFydEF0IDwgZGF0ZXBpY2tlckludGVyZmFjZS5taW4pIHtcclxuICAgICAgICAgIHRocm93IEVycm9yKCdTdGFydEF0IHZhbHVlIERhdGUgbXVzdCBiZSBhdCBvciBhZnRlciBtaW4gRGF0ZScpO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKGRhdGVwaWNrZXJJbnRlcmZhY2UubWF4KSB7XHJcbiAgICAgICAgaWYgKGRhdGVwaWNrZXJJbnRlcmZhY2Uuc3RhcnRBdCA+IGRhdGVwaWNrZXJJbnRlcmZhY2UubWF4KSB7XHJcbiAgICAgICAgICB0aHJvdyBFcnJvcignU3RhcnRBdCB2YWx1ZSBEYXRlIG11c3QgYmUgYXQgb3IgYmVmb3JlIG1heCBEYXRlJyk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICBkYXRlcGlja2VyLnNldFN0YXJ0QXQoZGF0ZXBpY2tlckludGVyZmFjZS5zdGFydEF0KTtcclxuXHJcbiAgICBpZiAoZGF0ZXBpY2tlckludGVyZmFjZS5kaXNhYmxlZClcclxuICAgICAgZGF0ZXBpY2tlci5zZXREaXNhYmxlZChkYXRlcGlja2VySW50ZXJmYWNlLmRpc2FibGVkKTtcclxuXHJcbiAgICBpZiAoZGF0ZXBpY2tlckludGVyZmFjZS5zdGFydFZpZXcpXHJcbiAgICAgIGRhdGVwaWNrZXIuc2V0U3RhcnRWaWV3KGRhdGVwaWNrZXJJbnRlcmZhY2Uuc3RhcnRWaWV3KTtcclxuICAgIGlmIChkYXRlcGlja2VySW50ZXJmYWNlLmNvbG9yKVxyXG4gICAgICBkYXRlcGlja2VyLnNldENvbG9yKGRhdGVwaWNrZXJJbnRlcmZhY2UuY29sb3IpO1xyXG4gICAgaWYgKGRhdGVwaWNrZXJJbnRlcmZhY2UudG9nZ2xlU2lkZVN1ZmZpeCAhPT0gdW5kZWZpbmVkKVxyXG4gICAgICBkYXRlcGlja2VyLnNldFRvZ2dsZVNpZGVTdWZmaXgoZGF0ZXBpY2tlckludGVyZmFjZS50b2dnbGVTaWRlU3VmZml4KTtcclxuICAgIGlmIChkYXRlcGlja2VySW50ZXJmYWNlLm1hdEljb24pXHJcbiAgICAgIGRhdGVwaWNrZXIuc2V0TWF0SWNvbihkYXRlcGlja2VySW50ZXJmYWNlLm1hdEljb24pO1xyXG4gICAgaWYgKGRhdGVwaWNrZXJJbnRlcmZhY2UubGFiZWxOYW1lKVxyXG4gICAgICBkYXRlcGlja2VyLnNldExhYmVsTmFtZShkYXRlcGlja2VySW50ZXJmYWNlLmxhYmVsTmFtZSk7XHJcbiAgICBlbHNlIHRocm93IEVycm9yKCdMYWJlbCBuYW1lIGlzIHJlcXVpcmVkJyk7XHJcblxyXG4gICAgaWYgKGRhdGVwaWNrZXJJbnRlcmZhY2UudGltZXBpY2tlcilcclxuICAgICAgZGF0ZXBpY2tlci5zZXRUaW1lcGlja2VySW5zaWRlRGF0ZXBpY2tlckludGVyZmFjZShcclxuICAgICAgICBkYXRlcGlja2VySW50ZXJmYWNlLnRpbWVwaWNrZXJcclxuICAgICAgKTtcclxuXHJcbiAgICByZXR1cm4gZGF0ZXBpY2tlcjtcclxuICB9XHJcbn1cclxuIl19