import { Select, } from '../../../structural/composite/form/select/select.model';
import { VALIDATOR_NAMES, } from '../../../structural/composite/form/form-row-item.model';
import { Validators } from '@angular/forms';
var SelectConstructionStrategy = /** @class */ (function () {
    function SelectConstructionStrategy() {
    }
    SelectConstructionStrategy.prototype.construct = function (selectInterface) {
        var select = new Select();
        select.setType(selectInterface.type);
        if (selectInterface.multiple) {
            select.setMultiple(selectInterface.multiple);
            if (selectInterface.defaultValue) {
                if (selectInterface.defaultValue instanceof Array) {
                    select.setDefaultValue(selectInterface.defaultValue);
                }
                else {
                    throw Error('If you want multiple choice default value must be an Array');
                }
            }
        }
        else {
            if (selectInterface.defaultValue) {
                if (selectInterface.defaultValue instanceof Array) {
                    throw Error("If you don't want multiple choice default value must not be an Array");
                }
                else {
                    select.setDefaultValue(selectInterface.defaultValue);
                }
            }
        }
        if (selectInterface.matSelectTriggerOn) {
            if (!select.getMultiple())
                throw Error('MatSelectTriggerOn make sense only if multiple is set to true');
            else
                select.setMatSelectTriggerOn(selectInterface.matSelectTriggerOn);
        }
        select.setControlName(selectInterface.controlName);
        if (selectInterface.validators)
            select.setValidators(selectInterface.validators);
        if (selectInterface.hintLabels) {
            throw Error('Hint Labels are not allowed on select');
        }
        if (selectInterface.appearance)
            select.setAppearance(selectInterface.appearance);
        if (selectInterface.placeholder)
            throw Error('Placeholder is not allowed on select');
        if (selectInterface.disabled)
            select.setDisabled(selectInterface.disabled);
        if (selectInterface.readonly)
            throw Error('Readonly is not allowed on select');
        if (selectInterface.requiredOption) {
            var requiredOption = {
                required: false,
                hideRequiredMarker: false,
            };
            if (selectInterface.requiredOption.required)
                requiredOption.required = selectInterface.requiredOption.required;
            if (selectInterface.requiredOption.hideRequiredMarker)
                requiredOption.hideRequiredMarker =
                    selectInterface.requiredOption.hideRequiredMarker;
            select.setRequired(requiredOption);
            // add required validatorFn if it doesn't exists
            var validatorRequiredFnExists_1 = false;
            select
                .getValidators()
                .forEach(function (validator) {
                if (validator.name === VALIDATOR_NAMES.REQUIRED) {
                    validatorRequiredFnExists_1 = true;
                }
            });
            if (!validatorRequiredFnExists_1) {
                select.addValidator({
                    message: 'the field is required',
                    name: VALIDATOR_NAMES.REQUIRED,
                    validatorFn: Validators.required,
                });
            }
        }
        else {
            // check if it has required validator if it has add requiredOption.required = true
            var validatorRequiredFnExists_2 = false;
            select
                .getValidators()
                .forEach(function (validator) {
                if (validator.name === VALIDATOR_NAMES.REQUIRED) {
                    validatorRequiredFnExists_2 = true;
                }
            });
            if (validatorRequiredFnExists_2) {
                var requiredOption = {
                    required: true,
                    hideRequiredMarker: false,
                };
                select.setRequired(requiredOption);
            }
        }
        if (selectInterface.labelName)
            select.setLabelName(selectInterface.labelName);
        else
            throw Error('Label name is required');
        if (selectInterface.matImages) {
            if (selectInterface.matImages.matPrefixImgText)
                select.setMatPrefixImgText(selectInterface.matImages.matPrefixImgText);
            if (selectInterface.matImages.matSuffixImgText)
                select.setMatSuffixImgText(selectInterface.matImages.matSuffixImgText);
        }
        if (selectInterface.textPrefix)
            select.setTextPrefix(selectInterface.textPrefix);
        if (selectInterface.textSuffix)
            select.setTextSuffix(selectInterface.textSuffix);
        if (selectInterface.optionValues && selectInterface.optionValues.length > 0)
            select.setOptionValues(selectInterface.optionValues);
        else
            throw Error('Option Values are required for select');
        return select;
    };
    return SelectConstructionStrategy;
}());
export { SelectConstructionStrategy };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VsZWN0LWNvbnN0cnVjdGlvbi1zdHJhdGVneS5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215LWFuZ3VsYXItZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvbW9kZWwvYmVoYXZpb3VyYWwvc3RyYXRlZ3kvZm9ybS1yb3ctaXRlbS9zZWxlY3QtY29uc3RydWN0aW9uLXN0cmF0ZWd5Lm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBLE9BQU8sRUFFTCxNQUFNLEdBQ1AsTUFBTSx3REFBd0QsQ0FBQztBQUNoRSxPQUFPLEVBRUwsZUFBZSxHQUVoQixNQUFNLHdEQUF3RCxDQUFDO0FBQ2hFLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUU1QztJQUFBO0lBdUlBLENBQUM7SUFySUMsOENBQVMsR0FBVCxVQUFVLGVBQWdDO1FBQ3hDLElBQUksTUFBTSxHQUFXLElBQUksTUFBTSxFQUFFLENBQUM7UUFFbEMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUM7UUFFckMsSUFBSSxlQUFlLENBQUMsUUFBUSxFQUFFO1lBQzVCLE1BQU0sQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBRTdDLElBQUksZUFBZSxDQUFDLFlBQVksRUFBRTtnQkFDaEMsSUFBSSxlQUFlLENBQUMsWUFBWSxZQUFZLEtBQUssRUFBRTtvQkFDakQsTUFBTSxDQUFDLGVBQWUsQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDLENBQUM7aUJBQ3REO3FCQUFNO29CQUNMLE1BQU0sS0FBSyxDQUNULDREQUE0RCxDQUM3RCxDQUFDO2lCQUNIO2FBQ0Y7U0FDRjthQUFNO1lBQ0wsSUFBSSxlQUFlLENBQUMsWUFBWSxFQUFFO2dCQUNoQyxJQUFJLGVBQWUsQ0FBQyxZQUFZLFlBQVksS0FBSyxFQUFFO29CQUNqRCxNQUFNLEtBQUssQ0FDVCxzRUFBc0UsQ0FDdkUsQ0FBQztpQkFDSDtxQkFBTTtvQkFDTCxNQUFNLENBQUMsZUFBZSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsQ0FBQztpQkFDdEQ7YUFDRjtTQUNGO1FBRUQsSUFBSSxlQUFlLENBQUMsa0JBQWtCLEVBQUU7WUFDdEMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLEVBQUU7Z0JBQ3ZCLE1BQU0sS0FBSyxDQUNULCtEQUErRCxDQUNoRSxDQUFDOztnQkFDQyxNQUFNLENBQUMscUJBQXFCLENBQUMsZUFBZSxDQUFDLGtCQUFrQixDQUFDLENBQUM7U0FDdkU7UUFFRCxNQUFNLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUVuRCxJQUFJLGVBQWUsQ0FBQyxVQUFVO1lBQzVCLE1BQU0sQ0FBQyxhQUFhLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBRW5ELElBQUksZUFBZSxDQUFDLFVBQVUsRUFBRTtZQUM5QixNQUFNLEtBQUssQ0FBQyx1Q0FBdUMsQ0FBQyxDQUFDO1NBQ3REO1FBRUQsSUFBSSxlQUFlLENBQUMsVUFBVTtZQUM1QixNQUFNLENBQUMsYUFBYSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUVuRCxJQUFJLGVBQWUsQ0FBQyxXQUFXO1lBQzdCLE1BQU0sS0FBSyxDQUFDLHNDQUFzQyxDQUFDLENBQUM7UUFFdEQsSUFBSSxlQUFlLENBQUMsUUFBUTtZQUFFLE1BQU0sQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBRTNFLElBQUksZUFBZSxDQUFDLFFBQVE7WUFDMUIsTUFBTSxLQUFLLENBQUMsbUNBQW1DLENBQUMsQ0FBQztRQUVuRCxJQUFJLGVBQWUsQ0FBQyxjQUFjLEVBQUU7WUFDbEMsSUFBSSxjQUFjLEdBQUc7Z0JBQ25CLFFBQVEsRUFBRSxLQUFLO2dCQUNmLGtCQUFrQixFQUFFLEtBQUs7YUFDMUIsQ0FBQztZQUVGLElBQUksZUFBZSxDQUFDLGNBQWMsQ0FBQyxRQUFRO2dCQUN6QyxjQUFjLENBQUMsUUFBUSxHQUFHLGVBQWUsQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDO1lBRXBFLElBQUksZUFBZSxDQUFDLGNBQWMsQ0FBQyxrQkFBa0I7Z0JBQ25ELGNBQWMsQ0FBQyxrQkFBa0I7b0JBQy9CLGVBQWUsQ0FBQyxjQUFjLENBQUMsa0JBQWtCLENBQUM7WUFFdEQsTUFBTSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUVuQyxnREFBZ0Q7WUFDaEQsSUFBSSwyQkFBeUIsR0FBRyxLQUFLLENBQUM7WUFDeEIsTUFBTztpQkFDbEIsYUFBYSxFQUFFO2lCQUNmLE9BQU8sQ0FBQyxVQUFDLFNBQXdDO2dCQUNoRCxJQUFJLFNBQVMsQ0FBQyxJQUFJLEtBQUssZUFBZSxDQUFDLFFBQVEsRUFBRTtvQkFDL0MsMkJBQXlCLEdBQUcsSUFBSSxDQUFDO2lCQUNsQztZQUNILENBQUMsQ0FBQyxDQUFDO1lBRUwsSUFBSSxDQUFDLDJCQUF5QixFQUFFO2dCQUNoQixNQUFPLENBQUMsWUFBWSxDQUFDO29CQUNqQyxPQUFPLEVBQUUsdUJBQXVCO29CQUNoQyxJQUFJLEVBQUUsZUFBZSxDQUFDLFFBQVE7b0JBQzlCLFdBQVcsRUFBRSxVQUFVLENBQUMsUUFBUTtpQkFDakMsQ0FBQyxDQUFDO2FBQ0o7U0FDRjthQUFNO1lBQ0wsa0ZBQWtGO1lBQ2xGLElBQUksMkJBQXlCLEdBQUcsS0FBSyxDQUFDO1lBQ3hCLE1BQU87aUJBQ2xCLGFBQWEsRUFBRTtpQkFDZixPQUFPLENBQUMsVUFBQyxTQUF3QztnQkFDaEQsSUFBSSxTQUFTLENBQUMsSUFBSSxLQUFLLGVBQWUsQ0FBQyxRQUFRLEVBQUU7b0JBQy9DLDJCQUF5QixHQUFHLElBQUksQ0FBQztpQkFDbEM7WUFDSCxDQUFDLENBQUMsQ0FBQztZQUVMLElBQUksMkJBQXlCLEVBQUU7Z0JBQzdCLElBQUksY0FBYyxHQUFHO29CQUNuQixRQUFRLEVBQUUsSUFBSTtvQkFDZCxrQkFBa0IsRUFBRSxLQUFLO2lCQUMxQixDQUFDO2dCQUNGLE1BQU0sQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLENBQUM7YUFDcEM7U0FDRjtRQUVELElBQUksZUFBZSxDQUFDLFNBQVM7WUFDM0IsTUFBTSxDQUFDLFlBQVksQ0FBQyxlQUFlLENBQUMsU0FBUyxDQUFDLENBQUM7O1lBQzVDLE1BQU0sS0FBSyxDQUFDLHdCQUF3QixDQUFDLENBQUM7UUFFM0MsSUFBSSxlQUFlLENBQUMsU0FBUyxFQUFFO1lBQzdCLElBQUksZUFBZSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0I7Z0JBQzVDLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxlQUFlLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLENBQUM7WUFFekUsSUFBSSxlQUFlLENBQUMsU0FBUyxDQUFDLGdCQUFnQjtnQkFDNUMsTUFBTSxDQUFDLG1CQUFtQixDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztTQUMxRTtRQUVELElBQUksZUFBZSxDQUFDLFVBQVU7WUFDNUIsTUFBTSxDQUFDLGFBQWEsQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLENBQUM7UUFFbkQsSUFBSSxlQUFlLENBQUMsVUFBVTtZQUM1QixNQUFNLENBQUMsYUFBYSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUVuRCxJQUFJLGVBQWUsQ0FBQyxZQUFZLElBQUksZUFBZSxDQUFDLFlBQVksQ0FBQyxNQUFNLEdBQUcsQ0FBQztZQUN6RSxNQUFNLENBQUMsZUFBZSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsQ0FBQzs7WUFDbEQsTUFBTSxLQUFLLENBQUMsdUNBQXVDLENBQUMsQ0FBQztRQUUxRCxPQUFPLE1BQU0sQ0FBQztJQUNoQixDQUFDO0lBQ0gsaUNBQUM7QUFBRCxDQUFDLEFBdklELElBdUlDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRm9ybUZpZWxkSW5wdXRDb25zdHJ1Y3Rpb25TdHJhdGVneSB9IGZyb20gJy4vZm9ybS1maWVsZC1pbnB1dC1jb25zdHJ1Y3Rpb24tc3RyYXRlZ3kubW9kZWwnO1xyXG5pbXBvcnQge1xyXG4gIFNlbGVjdEludGVyZmFjZSxcclxuICBTZWxlY3QsXHJcbn0gZnJvbSAnLi4vLi4vLi4vc3RydWN0dXJhbC9jb21wb3NpdGUvZm9ybS9zZWxlY3Qvc2VsZWN0Lm1vZGVsJztcclxuaW1wb3J0IHtcclxuICBGb3JtUm93SXRlbVZhbGlkYXRvckludGVyZmFjZSxcclxuICBWQUxJREFUT1JfTkFNRVMsXHJcbiAgRm9ybVJvd0l0ZW0sXHJcbn0gZnJvbSAnLi4vLi4vLi4vc3RydWN0dXJhbC9jb21wb3NpdGUvZm9ybS9mb3JtLXJvdy1pdGVtLm1vZGVsJztcclxuaW1wb3J0IHsgVmFsaWRhdG9ycyB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuXHJcbmV4cG9ydCBjbGFzcyBTZWxlY3RDb25zdHJ1Y3Rpb25TdHJhdGVneVxyXG4gIGltcGxlbWVudHMgRm9ybUZpZWxkSW5wdXRDb25zdHJ1Y3Rpb25TdHJhdGVneSB7XHJcbiAgY29uc3RydWN0KHNlbGVjdEludGVyZmFjZTogU2VsZWN0SW50ZXJmYWNlKTogU2VsZWN0IHtcclxuICAgIGxldCBzZWxlY3Q6IFNlbGVjdCA9IG5ldyBTZWxlY3QoKTtcclxuXHJcbiAgICBzZWxlY3Quc2V0VHlwZShzZWxlY3RJbnRlcmZhY2UudHlwZSk7XHJcblxyXG4gICAgaWYgKHNlbGVjdEludGVyZmFjZS5tdWx0aXBsZSkge1xyXG4gICAgICBzZWxlY3Quc2V0TXVsdGlwbGUoc2VsZWN0SW50ZXJmYWNlLm11bHRpcGxlKTtcclxuXHJcbiAgICAgIGlmIChzZWxlY3RJbnRlcmZhY2UuZGVmYXVsdFZhbHVlKSB7XHJcbiAgICAgICAgaWYgKHNlbGVjdEludGVyZmFjZS5kZWZhdWx0VmFsdWUgaW5zdGFuY2VvZiBBcnJheSkge1xyXG4gICAgICAgICAgc2VsZWN0LnNldERlZmF1bHRWYWx1ZShzZWxlY3RJbnRlcmZhY2UuZGVmYXVsdFZhbHVlKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgdGhyb3cgRXJyb3IoXHJcbiAgICAgICAgICAgICdJZiB5b3Ugd2FudCBtdWx0aXBsZSBjaG9pY2UgZGVmYXVsdCB2YWx1ZSBtdXN0IGJlIGFuIEFycmF5J1xyXG4gICAgICAgICAgKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGlmIChzZWxlY3RJbnRlcmZhY2UuZGVmYXVsdFZhbHVlKSB7XHJcbiAgICAgICAgaWYgKHNlbGVjdEludGVyZmFjZS5kZWZhdWx0VmFsdWUgaW5zdGFuY2VvZiBBcnJheSkge1xyXG4gICAgICAgICAgdGhyb3cgRXJyb3IoXHJcbiAgICAgICAgICAgIFwiSWYgeW91IGRvbid0IHdhbnQgbXVsdGlwbGUgY2hvaWNlIGRlZmF1bHQgdmFsdWUgbXVzdCBub3QgYmUgYW4gQXJyYXlcIlxyXG4gICAgICAgICAgKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgc2VsZWN0LnNldERlZmF1bHRWYWx1ZShzZWxlY3RJbnRlcmZhY2UuZGVmYXVsdFZhbHVlKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBpZiAoc2VsZWN0SW50ZXJmYWNlLm1hdFNlbGVjdFRyaWdnZXJPbikge1xyXG4gICAgICBpZiAoIXNlbGVjdC5nZXRNdWx0aXBsZSgpKVxyXG4gICAgICAgIHRocm93IEVycm9yKFxyXG4gICAgICAgICAgJ01hdFNlbGVjdFRyaWdnZXJPbiBtYWtlIHNlbnNlIG9ubHkgaWYgbXVsdGlwbGUgaXMgc2V0IHRvIHRydWUnXHJcbiAgICAgICAgKTtcclxuICAgICAgZWxzZSBzZWxlY3Quc2V0TWF0U2VsZWN0VHJpZ2dlck9uKHNlbGVjdEludGVyZmFjZS5tYXRTZWxlY3RUcmlnZ2VyT24pO1xyXG4gICAgfVxyXG5cclxuICAgIHNlbGVjdC5zZXRDb250cm9sTmFtZShzZWxlY3RJbnRlcmZhY2UuY29udHJvbE5hbWUpO1xyXG5cclxuICAgIGlmIChzZWxlY3RJbnRlcmZhY2UudmFsaWRhdG9ycylcclxuICAgICAgc2VsZWN0LnNldFZhbGlkYXRvcnMoc2VsZWN0SW50ZXJmYWNlLnZhbGlkYXRvcnMpO1xyXG5cclxuICAgIGlmIChzZWxlY3RJbnRlcmZhY2UuaGludExhYmVscykge1xyXG4gICAgICB0aHJvdyBFcnJvcignSGludCBMYWJlbHMgYXJlIG5vdCBhbGxvd2VkIG9uIHNlbGVjdCcpO1xyXG4gICAgfVxyXG5cclxuICAgIGlmIChzZWxlY3RJbnRlcmZhY2UuYXBwZWFyYW5jZSlcclxuICAgICAgc2VsZWN0LnNldEFwcGVhcmFuY2Uoc2VsZWN0SW50ZXJmYWNlLmFwcGVhcmFuY2UpO1xyXG5cclxuICAgIGlmIChzZWxlY3RJbnRlcmZhY2UucGxhY2Vob2xkZXIpXHJcbiAgICAgIHRocm93IEVycm9yKCdQbGFjZWhvbGRlciBpcyBub3QgYWxsb3dlZCBvbiBzZWxlY3QnKTtcclxuXHJcbiAgICBpZiAoc2VsZWN0SW50ZXJmYWNlLmRpc2FibGVkKSBzZWxlY3Quc2V0RGlzYWJsZWQoc2VsZWN0SW50ZXJmYWNlLmRpc2FibGVkKTtcclxuXHJcbiAgICBpZiAoc2VsZWN0SW50ZXJmYWNlLnJlYWRvbmx5KVxyXG4gICAgICB0aHJvdyBFcnJvcignUmVhZG9ubHkgaXMgbm90IGFsbG93ZWQgb24gc2VsZWN0Jyk7XHJcblxyXG4gICAgaWYgKHNlbGVjdEludGVyZmFjZS5yZXF1aXJlZE9wdGlvbikge1xyXG4gICAgICBsZXQgcmVxdWlyZWRPcHRpb24gPSB7XHJcbiAgICAgICAgcmVxdWlyZWQ6IGZhbHNlLFxyXG4gICAgICAgIGhpZGVSZXF1aXJlZE1hcmtlcjogZmFsc2UsXHJcbiAgICAgIH07XHJcblxyXG4gICAgICBpZiAoc2VsZWN0SW50ZXJmYWNlLnJlcXVpcmVkT3B0aW9uLnJlcXVpcmVkKVxyXG4gICAgICAgIHJlcXVpcmVkT3B0aW9uLnJlcXVpcmVkID0gc2VsZWN0SW50ZXJmYWNlLnJlcXVpcmVkT3B0aW9uLnJlcXVpcmVkO1xyXG5cclxuICAgICAgaWYgKHNlbGVjdEludGVyZmFjZS5yZXF1aXJlZE9wdGlvbi5oaWRlUmVxdWlyZWRNYXJrZXIpXHJcbiAgICAgICAgcmVxdWlyZWRPcHRpb24uaGlkZVJlcXVpcmVkTWFya2VyID1cclxuICAgICAgICAgIHNlbGVjdEludGVyZmFjZS5yZXF1aXJlZE9wdGlvbi5oaWRlUmVxdWlyZWRNYXJrZXI7XHJcblxyXG4gICAgICBzZWxlY3Quc2V0UmVxdWlyZWQocmVxdWlyZWRPcHRpb24pO1xyXG5cclxuICAgICAgLy8gYWRkIHJlcXVpcmVkIHZhbGlkYXRvckZuIGlmIGl0IGRvZXNuJ3QgZXhpc3RzXHJcbiAgICAgIGxldCB2YWxpZGF0b3JSZXF1aXJlZEZuRXhpc3RzID0gZmFsc2U7XHJcbiAgICAgICg8Rm9ybVJvd0l0ZW0+c2VsZWN0KVxyXG4gICAgICAgIC5nZXRWYWxpZGF0b3JzKClcclxuICAgICAgICAuZm9yRWFjaCgodmFsaWRhdG9yOiBGb3JtUm93SXRlbVZhbGlkYXRvckludGVyZmFjZSkgPT4ge1xyXG4gICAgICAgICAgaWYgKHZhbGlkYXRvci5uYW1lID09PSBWQUxJREFUT1JfTkFNRVMuUkVRVUlSRUQpIHtcclxuICAgICAgICAgICAgdmFsaWRhdG9yUmVxdWlyZWRGbkV4aXN0cyA9IHRydWU7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICBpZiAoIXZhbGlkYXRvclJlcXVpcmVkRm5FeGlzdHMpIHtcclxuICAgICAgICAoPEZvcm1Sb3dJdGVtPnNlbGVjdCkuYWRkVmFsaWRhdG9yKHtcclxuICAgICAgICAgIG1lc3NhZ2U6ICd0aGUgZmllbGQgaXMgcmVxdWlyZWQnLFxyXG4gICAgICAgICAgbmFtZTogVkFMSURBVE9SX05BTUVTLlJFUVVJUkVELFxyXG4gICAgICAgICAgdmFsaWRhdG9yRm46IFZhbGlkYXRvcnMucmVxdWlyZWQsXHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIC8vIGNoZWNrIGlmIGl0IGhhcyByZXF1aXJlZCB2YWxpZGF0b3IgaWYgaXQgaGFzIGFkZCByZXF1aXJlZE9wdGlvbi5yZXF1aXJlZCA9IHRydWVcclxuICAgICAgbGV0IHZhbGlkYXRvclJlcXVpcmVkRm5FeGlzdHMgPSBmYWxzZTtcclxuICAgICAgKDxGb3JtUm93SXRlbT5zZWxlY3QpXHJcbiAgICAgICAgLmdldFZhbGlkYXRvcnMoKVxyXG4gICAgICAgIC5mb3JFYWNoKCh2YWxpZGF0b3I6IEZvcm1Sb3dJdGVtVmFsaWRhdG9ySW50ZXJmYWNlKSA9PiB7XHJcbiAgICAgICAgICBpZiAodmFsaWRhdG9yLm5hbWUgPT09IFZBTElEQVRPUl9OQU1FUy5SRVFVSVJFRCkge1xyXG4gICAgICAgICAgICB2YWxpZGF0b3JSZXF1aXJlZEZuRXhpc3RzID0gdHJ1ZTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgIGlmICh2YWxpZGF0b3JSZXF1aXJlZEZuRXhpc3RzKSB7XHJcbiAgICAgICAgbGV0IHJlcXVpcmVkT3B0aW9uID0ge1xyXG4gICAgICAgICAgcmVxdWlyZWQ6IHRydWUsXHJcbiAgICAgICAgICBoaWRlUmVxdWlyZWRNYXJrZXI6IGZhbHNlLFxyXG4gICAgICAgIH07XHJcbiAgICAgICAgc2VsZWN0LnNldFJlcXVpcmVkKHJlcXVpcmVkT3B0aW9uKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGlmIChzZWxlY3RJbnRlcmZhY2UubGFiZWxOYW1lKVxyXG4gICAgICBzZWxlY3Quc2V0TGFiZWxOYW1lKHNlbGVjdEludGVyZmFjZS5sYWJlbE5hbWUpO1xyXG4gICAgZWxzZSB0aHJvdyBFcnJvcignTGFiZWwgbmFtZSBpcyByZXF1aXJlZCcpO1xyXG5cclxuICAgIGlmIChzZWxlY3RJbnRlcmZhY2UubWF0SW1hZ2VzKSB7XHJcbiAgICAgIGlmIChzZWxlY3RJbnRlcmZhY2UubWF0SW1hZ2VzLm1hdFByZWZpeEltZ1RleHQpXHJcbiAgICAgICAgc2VsZWN0LnNldE1hdFByZWZpeEltZ1RleHQoc2VsZWN0SW50ZXJmYWNlLm1hdEltYWdlcy5tYXRQcmVmaXhJbWdUZXh0KTtcclxuXHJcbiAgICAgIGlmIChzZWxlY3RJbnRlcmZhY2UubWF0SW1hZ2VzLm1hdFN1ZmZpeEltZ1RleHQpXHJcbiAgICAgICAgc2VsZWN0LnNldE1hdFN1ZmZpeEltZ1RleHQoc2VsZWN0SW50ZXJmYWNlLm1hdEltYWdlcy5tYXRTdWZmaXhJbWdUZXh0KTtcclxuICAgIH1cclxuXHJcbiAgICBpZiAoc2VsZWN0SW50ZXJmYWNlLnRleHRQcmVmaXgpXHJcbiAgICAgIHNlbGVjdC5zZXRUZXh0UHJlZml4KHNlbGVjdEludGVyZmFjZS50ZXh0UHJlZml4KTtcclxuXHJcbiAgICBpZiAoc2VsZWN0SW50ZXJmYWNlLnRleHRTdWZmaXgpXHJcbiAgICAgIHNlbGVjdC5zZXRUZXh0U3VmZml4KHNlbGVjdEludGVyZmFjZS50ZXh0U3VmZml4KTtcclxuXHJcbiAgICBpZiAoc2VsZWN0SW50ZXJmYWNlLm9wdGlvblZhbHVlcyAmJiBzZWxlY3RJbnRlcmZhY2Uub3B0aW9uVmFsdWVzLmxlbmd0aCA+IDApXHJcbiAgICAgIHNlbGVjdC5zZXRPcHRpb25WYWx1ZXMoc2VsZWN0SW50ZXJmYWNlLm9wdGlvblZhbHVlcyk7XHJcbiAgICBlbHNlIHRocm93IEVycm9yKCdPcHRpb24gVmFsdWVzIGFyZSByZXF1aXJlZCBmb3Igc2VsZWN0Jyk7XHJcblxyXG4gICAgcmV0dXJuIHNlbGVjdDtcclxuICB9XHJcbn1cclxuIl19