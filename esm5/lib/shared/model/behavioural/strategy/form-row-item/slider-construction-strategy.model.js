import { Slider, } from "../../../structural/composite/form/slider/slider.model";
import { VALIDATOR_NAMES, } from "../../../structural/composite/form/form-row-item.model";
import { NUMBERS_ONLY_REGEX } from "../../../../predefined-regex";
import { Validators } from "@angular/forms";
var SliderConstructionStrategy = /** @class */ (function () {
    function SliderConstructionStrategy() {
    }
    SliderConstructionStrategy.prototype.construct = function (sliderInterface) {
        var slider = new Slider();
        slider.setType(sliderInterface.type);
        slider.setDefaultValue(sliderInterface.defaultValue);
        slider.setControlName(sliderInterface.controlName);
        if (sliderInterface.validators)
            slider.setValidators(sliderInterface.validators);
        slider.addValidator({
            message: "slider's value must be numeric",
            name: VALIDATOR_NAMES.PATTERN,
            validatorFn: Validators.pattern(NUMBERS_ONLY_REGEX),
        });
        if (sliderInterface.minValue) {
            slider.setMinValue(sliderInterface.minValue);
            if (sliderInterface.validators) {
                var minValidatorExists_1 = false;
                sliderInterface.validators.forEach(function (validator) {
                    if (validator.name === VALIDATOR_NAMES.MIN) {
                        minValidatorExists_1 = true;
                    }
                });
                if (!minValidatorExists_1) {
                    slider.addValidator({
                        message: "slider's value must be higher than " +
                            slider.getMinValue().toString(),
                        name: VALIDATOR_NAMES.MIN,
                        validatorFn: Validators.min(+slider.getMinValue()),
                    });
                }
            }
            else {
                slider.addValidator({
                    message: "slider's value must be higher than " +
                        slider.getMinValue().toString(),
                    name: VALIDATOR_NAMES.MIN,
                    validatorFn: Validators.min(+slider.getMinValue()),
                });
            }
        }
        if (sliderInterface.maxValue) {
            slider.setMaxValue(sliderInterface.maxValue);
            if (sliderInterface.validators) {
                var maxValidatorExists_1 = false;
                sliderInterface.validators.forEach(function (validator) {
                    if (validator.name === VALIDATOR_NAMES.MAX) {
                        maxValidatorExists_1 = true;
                    }
                });
                if (!maxValidatorExists_1) {
                    slider.addValidator({
                        message: "slider's value must be lower than " +
                            slider.getMaxValue().toString(),
                        name: VALIDATOR_NAMES.MAX,
                        validatorFn: Validators.max(+slider.getMaxValue()),
                    });
                }
            }
            else {
                slider.addValidator({
                    message: "slider's value must be lower than " +
                        slider.getMaxValue().toString(),
                    name: VALIDATOR_NAMES.MAX,
                    validatorFn: Validators.max(+slider.getMaxValue()),
                });
            }
        }
        if (sliderInterface.disabled)
            slider.setDisabled(sliderInterface.disabled);
        if (sliderInterface.invert)
            slider.setInvert(sliderInterface.invert);
        if (sliderInterface.step)
            slider.setStep(sliderInterface.step);
        if (sliderInterface.tickInterval)
            slider.setTickInterval(sliderInterface.tickInterval);
        if (sliderInterface.thumbLabel)
            slider.setThumbLabel(sliderInterface.thumbLabel);
        if (sliderInterface.vertical)
            slider.setVertical(sliderInterface.vertical);
        return slider;
    };
    return SliderConstructionStrategy;
}());
export { SliderConstructionStrategy };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2xpZGVyLWNvbnN0cnVjdGlvbi1zdHJhdGVneS5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215LWFuZ3VsYXItZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvbW9kZWwvYmVoYXZpb3VyYWwvc3RyYXRlZ3kvZm9ybS1yb3ctaXRlbS9zbGlkZXItY29uc3RydWN0aW9uLXN0cmF0ZWd5Lm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBLE9BQU8sRUFDTCxNQUFNLEdBRVAsTUFBTSx3REFBd0QsQ0FBQztBQUNoRSxPQUFPLEVBRUwsZUFBZSxHQUNoQixNQUFNLHdEQUF3RCxDQUFDO0FBQ2hFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLDhCQUE4QixDQUFDO0FBQ2xFLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUU1QztJQUFBO0lBbUdBLENBQUM7SUFqR0MsOENBQVMsR0FBVCxVQUFVLGVBQWdDO1FBQ3hDLElBQUksTUFBTSxHQUFHLElBQUksTUFBTSxFQUFFLENBQUM7UUFFMUIsTUFBTSxDQUFDLE9BQU8sQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUM7UUFFckMsTUFBTSxDQUFDLGVBQWUsQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDLENBQUM7UUFFckQsTUFBTSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLENBQUM7UUFFbkQsSUFBSSxlQUFlLENBQUMsVUFBVTtZQUM1QixNQUFNLENBQUMsYUFBYSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUNuRCxNQUFNLENBQUMsWUFBWSxDQUFDO1lBQ2xCLE9BQU8sRUFBRSxnQ0FBZ0M7WUFDekMsSUFBSSxFQUFFLGVBQWUsQ0FBQyxPQUFPO1lBQzdCLFdBQVcsRUFBRSxVQUFVLENBQUMsT0FBTyxDQUFDLGtCQUFrQixDQUFDO1NBQ3BELENBQUMsQ0FBQztRQUVILElBQUksZUFBZSxDQUFDLFFBQVEsRUFBRTtZQUM1QixNQUFNLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUU3QyxJQUFJLGVBQWUsQ0FBQyxVQUFVLEVBQUU7Z0JBQzlCLElBQUksb0JBQWtCLEdBQUcsS0FBSyxDQUFDO2dCQUMvQixlQUFlLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FDaEMsVUFBQyxTQUF3QztvQkFDdkMsSUFBSSxTQUFTLENBQUMsSUFBSSxLQUFLLGVBQWUsQ0FBQyxHQUFHLEVBQUU7d0JBQzFDLG9CQUFrQixHQUFHLElBQUksQ0FBQztxQkFDM0I7Z0JBQ0gsQ0FBQyxDQUNGLENBQUM7Z0JBQ0YsSUFBSSxDQUFDLG9CQUFrQixFQUFFO29CQUN2QixNQUFNLENBQUMsWUFBWSxDQUFDO3dCQUNsQixPQUFPLEVBQ0wscUNBQXFDOzRCQUNyQyxNQUFNLENBQUMsV0FBVyxFQUFFLENBQUMsUUFBUSxFQUFFO3dCQUNqQyxJQUFJLEVBQUUsZUFBZSxDQUFDLEdBQUc7d0JBQ3pCLFdBQVcsRUFBRSxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLFdBQVcsRUFBRSxDQUFDO3FCQUNuRCxDQUFDLENBQUM7aUJBQ0o7YUFDRjtpQkFBTTtnQkFDTCxNQUFNLENBQUMsWUFBWSxDQUFDO29CQUNsQixPQUFPLEVBQ0wscUNBQXFDO3dCQUNyQyxNQUFNLENBQUMsV0FBVyxFQUFFLENBQUMsUUFBUSxFQUFFO29CQUNqQyxJQUFJLEVBQUUsZUFBZSxDQUFDLEdBQUc7b0JBQ3pCLFdBQVcsRUFBRSxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLFdBQVcsRUFBRSxDQUFDO2lCQUNuRCxDQUFDLENBQUM7YUFDSjtTQUNGO1FBRUQsSUFBSSxlQUFlLENBQUMsUUFBUSxFQUFFO1lBQzVCLE1BQU0sQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBRTdDLElBQUksZUFBZSxDQUFDLFVBQVUsRUFBRTtnQkFDOUIsSUFBSSxvQkFBa0IsR0FBRyxLQUFLLENBQUM7Z0JBQy9CLGVBQWUsQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUNoQyxVQUFDLFNBQXdDO29CQUN2QyxJQUFJLFNBQVMsQ0FBQyxJQUFJLEtBQUssZUFBZSxDQUFDLEdBQUcsRUFBRTt3QkFDMUMsb0JBQWtCLEdBQUcsSUFBSSxDQUFDO3FCQUMzQjtnQkFDSCxDQUFDLENBQ0YsQ0FBQztnQkFDRixJQUFJLENBQUMsb0JBQWtCLEVBQUU7b0JBQ3ZCLE1BQU0sQ0FBQyxZQUFZLENBQUM7d0JBQ2xCLE9BQU8sRUFDTCxvQ0FBb0M7NEJBQ3BDLE1BQU0sQ0FBQyxXQUFXLEVBQUUsQ0FBQyxRQUFRLEVBQUU7d0JBQ2pDLElBQUksRUFBRSxlQUFlLENBQUMsR0FBRzt3QkFDekIsV0FBVyxFQUFFLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFLENBQUM7cUJBQ25ELENBQUMsQ0FBQztpQkFDSjthQUNGO2lCQUFNO2dCQUNMLE1BQU0sQ0FBQyxZQUFZLENBQUM7b0JBQ2xCLE9BQU8sRUFDTCxvQ0FBb0M7d0JBQ3BDLE1BQU0sQ0FBQyxXQUFXLEVBQUUsQ0FBQyxRQUFRLEVBQUU7b0JBQ2pDLElBQUksRUFBRSxlQUFlLENBQUMsR0FBRztvQkFDekIsV0FBVyxFQUFFLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFLENBQUM7aUJBQ25ELENBQUMsQ0FBQzthQUNKO1NBQ0Y7UUFFRCxJQUFJLGVBQWUsQ0FBQyxRQUFRO1lBQUUsTUFBTSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLENBQUM7UUFFM0UsSUFBSSxlQUFlLENBQUMsTUFBTTtZQUFFLE1BQU0sQ0FBQyxTQUFTLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBRXJFLElBQUksZUFBZSxDQUFDLElBQUk7WUFBRSxNQUFNLENBQUMsT0FBTyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUUvRCxJQUFJLGVBQWUsQ0FBQyxZQUFZO1lBQzlCLE1BQU0sQ0FBQyxlQUFlLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBRXZELElBQUksZUFBZSxDQUFDLFVBQVU7WUFDNUIsTUFBTSxDQUFDLGFBQWEsQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLENBQUM7UUFFbkQsSUFBSSxlQUFlLENBQUMsUUFBUTtZQUFFLE1BQU0sQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBRTNFLE9BQU8sTUFBTSxDQUFDO0lBQ2hCLENBQUM7SUFDSCxpQ0FBQztBQUFELENBQUMsQUFuR0QsSUFtR0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBGb3JtUm93SXRlbUNvbnN0cnVjdGlvblN0cmF0ZWd5IH0gZnJvbSBcIi4vZm9ybS1yb3ctaXRlbS1jb25zdHJ1Y3Rpb24tc3RyYXRlZ3kuaW50ZXJmYWNlXCI7XHJcbmltcG9ydCB7XHJcbiAgU2xpZGVyLFxyXG4gIFNsaWRlckludGVyZmFjZSxcclxufSBmcm9tIFwiLi4vLi4vLi4vc3RydWN0dXJhbC9jb21wb3NpdGUvZm9ybS9zbGlkZXIvc2xpZGVyLm1vZGVsXCI7XHJcbmltcG9ydCB7XHJcbiAgRm9ybVJvd0l0ZW1WYWxpZGF0b3JJbnRlcmZhY2UsXHJcbiAgVkFMSURBVE9SX05BTUVTLFxyXG59IGZyb20gXCIuLi8uLi8uLi9zdHJ1Y3R1cmFsL2NvbXBvc2l0ZS9mb3JtL2Zvcm0tcm93LWl0ZW0ubW9kZWxcIjtcclxuaW1wb3J0IHsgTlVNQkVSU19PTkxZX1JFR0VYIH0gZnJvbSBcIi4uLy4uLy4uLy4uL3ByZWRlZmluZWQtcmVnZXhcIjtcclxuaW1wb3J0IHsgVmFsaWRhdG9ycyB9IGZyb20gXCJAYW5ndWxhci9mb3Jtc1wiO1xyXG5cclxuZXhwb3J0IGNsYXNzIFNsaWRlckNvbnN0cnVjdGlvblN0cmF0ZWd5XHJcbiAgaW1wbGVtZW50cyBGb3JtUm93SXRlbUNvbnN0cnVjdGlvblN0cmF0ZWd5IHtcclxuICBjb25zdHJ1Y3Qoc2xpZGVySW50ZXJmYWNlOiBTbGlkZXJJbnRlcmZhY2UpOiBTbGlkZXIge1xyXG4gICAgbGV0IHNsaWRlciA9IG5ldyBTbGlkZXIoKTtcclxuXHJcbiAgICBzbGlkZXIuc2V0VHlwZShzbGlkZXJJbnRlcmZhY2UudHlwZSk7XHJcblxyXG4gICAgc2xpZGVyLnNldERlZmF1bHRWYWx1ZShzbGlkZXJJbnRlcmZhY2UuZGVmYXVsdFZhbHVlKTtcclxuXHJcbiAgICBzbGlkZXIuc2V0Q29udHJvbE5hbWUoc2xpZGVySW50ZXJmYWNlLmNvbnRyb2xOYW1lKTtcclxuXHJcbiAgICBpZiAoc2xpZGVySW50ZXJmYWNlLnZhbGlkYXRvcnMpXHJcbiAgICAgIHNsaWRlci5zZXRWYWxpZGF0b3JzKHNsaWRlckludGVyZmFjZS52YWxpZGF0b3JzKTtcclxuICAgIHNsaWRlci5hZGRWYWxpZGF0b3Ioe1xyXG4gICAgICBtZXNzYWdlOiBcInNsaWRlcidzIHZhbHVlIG11c3QgYmUgbnVtZXJpY1wiLFxyXG4gICAgICBuYW1lOiBWQUxJREFUT1JfTkFNRVMuUEFUVEVSTixcclxuICAgICAgdmFsaWRhdG9yRm46IFZhbGlkYXRvcnMucGF0dGVybihOVU1CRVJTX09OTFlfUkVHRVgpLFxyXG4gICAgfSk7XHJcblxyXG4gICAgaWYgKHNsaWRlckludGVyZmFjZS5taW5WYWx1ZSkge1xyXG4gICAgICBzbGlkZXIuc2V0TWluVmFsdWUoc2xpZGVySW50ZXJmYWNlLm1pblZhbHVlKTtcclxuXHJcbiAgICAgIGlmIChzbGlkZXJJbnRlcmZhY2UudmFsaWRhdG9ycykge1xyXG4gICAgICAgIGxldCBtaW5WYWxpZGF0b3JFeGlzdHMgPSBmYWxzZTtcclxuICAgICAgICBzbGlkZXJJbnRlcmZhY2UudmFsaWRhdG9ycy5mb3JFYWNoKFxyXG4gICAgICAgICAgKHZhbGlkYXRvcjogRm9ybVJvd0l0ZW1WYWxpZGF0b3JJbnRlcmZhY2UpID0+IHtcclxuICAgICAgICAgICAgaWYgKHZhbGlkYXRvci5uYW1lID09PSBWQUxJREFUT1JfTkFNRVMuTUlOKSB7XHJcbiAgICAgICAgICAgICAgbWluVmFsaWRhdG9yRXhpc3RzID0gdHJ1ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgICk7XHJcbiAgICAgICAgaWYgKCFtaW5WYWxpZGF0b3JFeGlzdHMpIHtcclxuICAgICAgICAgIHNsaWRlci5hZGRWYWxpZGF0b3Ioe1xyXG4gICAgICAgICAgICBtZXNzYWdlOlxyXG4gICAgICAgICAgICAgIFwic2xpZGVyJ3MgdmFsdWUgbXVzdCBiZSBoaWdoZXIgdGhhbiBcIiArXHJcbiAgICAgICAgICAgICAgc2xpZGVyLmdldE1pblZhbHVlKCkudG9TdHJpbmcoKSxcclxuICAgICAgICAgICAgbmFtZTogVkFMSURBVE9SX05BTUVTLk1JTixcclxuICAgICAgICAgICAgdmFsaWRhdG9yRm46IFZhbGlkYXRvcnMubWluKCtzbGlkZXIuZ2V0TWluVmFsdWUoKSksXHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgc2xpZGVyLmFkZFZhbGlkYXRvcih7XHJcbiAgICAgICAgICBtZXNzYWdlOlxyXG4gICAgICAgICAgICBcInNsaWRlcidzIHZhbHVlIG11c3QgYmUgaGlnaGVyIHRoYW4gXCIgK1xyXG4gICAgICAgICAgICBzbGlkZXIuZ2V0TWluVmFsdWUoKS50b1N0cmluZygpLFxyXG4gICAgICAgICAgbmFtZTogVkFMSURBVE9SX05BTUVTLk1JTixcclxuICAgICAgICAgIHZhbGlkYXRvckZuOiBWYWxpZGF0b3JzLm1pbigrc2xpZGVyLmdldE1pblZhbHVlKCkpLFxyXG4gICAgICAgIH0pO1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKHNsaWRlckludGVyZmFjZS5tYXhWYWx1ZSkge1xyXG4gICAgICBzbGlkZXIuc2V0TWF4VmFsdWUoc2xpZGVySW50ZXJmYWNlLm1heFZhbHVlKTtcclxuXHJcbiAgICAgIGlmIChzbGlkZXJJbnRlcmZhY2UudmFsaWRhdG9ycykge1xyXG4gICAgICAgIGxldCBtYXhWYWxpZGF0b3JFeGlzdHMgPSBmYWxzZTtcclxuICAgICAgICBzbGlkZXJJbnRlcmZhY2UudmFsaWRhdG9ycy5mb3JFYWNoKFxyXG4gICAgICAgICAgKHZhbGlkYXRvcjogRm9ybVJvd0l0ZW1WYWxpZGF0b3JJbnRlcmZhY2UpID0+IHtcclxuICAgICAgICAgICAgaWYgKHZhbGlkYXRvci5uYW1lID09PSBWQUxJREFUT1JfTkFNRVMuTUFYKSB7XHJcbiAgICAgICAgICAgICAgbWF4VmFsaWRhdG9yRXhpc3RzID0gdHJ1ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgICk7XHJcbiAgICAgICAgaWYgKCFtYXhWYWxpZGF0b3JFeGlzdHMpIHtcclxuICAgICAgICAgIHNsaWRlci5hZGRWYWxpZGF0b3Ioe1xyXG4gICAgICAgICAgICBtZXNzYWdlOlxyXG4gICAgICAgICAgICAgIFwic2xpZGVyJ3MgdmFsdWUgbXVzdCBiZSBsb3dlciB0aGFuIFwiICtcclxuICAgICAgICAgICAgICBzbGlkZXIuZ2V0TWF4VmFsdWUoKS50b1N0cmluZygpLFxyXG4gICAgICAgICAgICBuYW1lOiBWQUxJREFUT1JfTkFNRVMuTUFYLFxyXG4gICAgICAgICAgICB2YWxpZGF0b3JGbjogVmFsaWRhdG9ycy5tYXgoK3NsaWRlci5nZXRNYXhWYWx1ZSgpKSxcclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBzbGlkZXIuYWRkVmFsaWRhdG9yKHtcclxuICAgICAgICAgIG1lc3NhZ2U6XHJcbiAgICAgICAgICAgIFwic2xpZGVyJ3MgdmFsdWUgbXVzdCBiZSBsb3dlciB0aGFuIFwiICtcclxuICAgICAgICAgICAgc2xpZGVyLmdldE1heFZhbHVlKCkudG9TdHJpbmcoKSxcclxuICAgICAgICAgIG5hbWU6IFZBTElEQVRPUl9OQU1FUy5NQVgsXHJcbiAgICAgICAgICB2YWxpZGF0b3JGbjogVmFsaWRhdG9ycy5tYXgoK3NsaWRlci5nZXRNYXhWYWx1ZSgpKSxcclxuICAgICAgICB9KTtcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGlmIChzbGlkZXJJbnRlcmZhY2UuZGlzYWJsZWQpIHNsaWRlci5zZXREaXNhYmxlZChzbGlkZXJJbnRlcmZhY2UuZGlzYWJsZWQpO1xyXG5cclxuICAgIGlmIChzbGlkZXJJbnRlcmZhY2UuaW52ZXJ0KSBzbGlkZXIuc2V0SW52ZXJ0KHNsaWRlckludGVyZmFjZS5pbnZlcnQpO1xyXG5cclxuICAgIGlmIChzbGlkZXJJbnRlcmZhY2Uuc3RlcCkgc2xpZGVyLnNldFN0ZXAoc2xpZGVySW50ZXJmYWNlLnN0ZXApO1xyXG5cclxuICAgIGlmIChzbGlkZXJJbnRlcmZhY2UudGlja0ludGVydmFsKVxyXG4gICAgICBzbGlkZXIuc2V0VGlja0ludGVydmFsKHNsaWRlckludGVyZmFjZS50aWNrSW50ZXJ2YWwpO1xyXG5cclxuICAgIGlmIChzbGlkZXJJbnRlcmZhY2UudGh1bWJMYWJlbClcclxuICAgICAgc2xpZGVyLnNldFRodW1iTGFiZWwoc2xpZGVySW50ZXJmYWNlLnRodW1iTGFiZWwpO1xyXG5cclxuICAgIGlmIChzbGlkZXJJbnRlcmZhY2UudmVydGljYWwpIHNsaWRlci5zZXRWZXJ0aWNhbChzbGlkZXJJbnRlcmZhY2UudmVydGljYWwpO1xyXG5cclxuICAgIHJldHVybiBzbGlkZXI7XHJcbiAgfVxyXG59XHJcbiJdfQ==