import { __extends } from "tslib";
// model
import { FormRowItem } from '../form-row-item.model';
import { MAT_COLOR } from '../../../../../material.module';
var Checkbox = /** @class */ (function (_super) {
    __extends(Checkbox, _super);
    function Checkbox() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.color = MAT_COLOR.PRIMARY;
        _this.labelPosition = 'after';
        _this.getName = function () {
            return _this.name;
        };
        _this.setName = function (name) {
            _this.name = name;
        };
        _this.getColor = function () {
            return _this.color;
        };
        _this.setColor = function (color) {
            _this.color = color;
        };
        _this.getLabelPosition = function () {
            return _this.labelPosition;
        };
        _this.setLabelPosition = function (labelPosition) {
            _this.labelPosition = labelPosition;
        };
        return _this;
    }
    return Checkbox;
}(FormRowItem));
export { Checkbox };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2hlY2tib3gubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9teS1hbmd1bGFyLWZvcm0vIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL21vZGVsL3N0cnVjdHVyYWwvY29tcG9zaXRlL2Zvcm0vY2hlY2tib3gvY2hlY2tib3gubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLFFBQVE7QUFDUixPQUFPLEVBQUUsV0FBVyxFQUF3QixNQUFNLHdCQUF3QixDQUFDO0FBQzNFLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQVMzRDtJQUE4Qiw0QkFBVztJQUF6QztRQUFBLHFFQTRCQztRQTFCUyxXQUFLLEdBQWMsU0FBUyxDQUFDLE9BQU8sQ0FBQztRQUNyQyxtQkFBYSxHQUF1QixPQUFPLENBQUM7UUFFcEQsYUFBTyxHQUFHO1lBQ1IsT0FBTyxLQUFJLENBQUMsSUFBSSxDQUFDO1FBQ25CLENBQUMsQ0FBQztRQUVGLGFBQU8sR0FBRyxVQUFDLElBQVk7WUFDckIsS0FBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7UUFDbkIsQ0FBQyxDQUFDO1FBRUYsY0FBUSxHQUFHO1lBQ1QsT0FBTyxLQUFJLENBQUMsS0FBSyxDQUFDO1FBQ3BCLENBQUMsQ0FBQztRQUVGLGNBQVEsR0FBRyxVQUFDLEtBQWdCO1lBQzFCLEtBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1FBQ3JCLENBQUMsQ0FBQztRQUVGLHNCQUFnQixHQUFHO1lBQ2pCLE9BQU8sS0FBSSxDQUFDLGFBQWEsQ0FBQztRQUM1QixDQUFDLENBQUM7UUFFRixzQkFBZ0IsR0FBRyxVQUFDLGFBQWlDO1lBQ25ELEtBQUksQ0FBQyxhQUFhLEdBQUcsYUFBYSxDQUFDO1FBQ3JDLENBQUMsQ0FBQzs7SUFDSixDQUFDO0lBQUQsZUFBQztBQUFELENBQUMsQUE1QkQsQ0FBOEIsV0FBVyxHQTRCeEMiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBtb2RlbFxyXG5pbXBvcnQgeyBGb3JtUm93SXRlbSwgRm9ybVJvd0l0ZW1JbnRlcmZhY2UgfSBmcm9tICcuLi9mb3JtLXJvdy1pdGVtLm1vZGVsJztcclxuaW1wb3J0IHsgTUFUX0NPTE9SIH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vbWF0ZXJpYWwubW9kdWxlJztcclxuXHJcbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBDaGVja2JveEludGVyZmFjZSBleHRlbmRzIEZvcm1Sb3dJdGVtSW50ZXJmYWNlIHtcclxuICBuYW1lOiBzdHJpbmc7XHJcbiAgZGVmYXVsdFZhbHVlOiBib29sZWFuO1xyXG4gIGxhYmVsUG9zaXRpb24/OiAnYmVmb3JlJyB8ICdhZnRlcic7XHJcbiAgY29sb3I/OiBNQVRfQ09MT1I7XHJcbn1cclxuXHJcbmV4cG9ydCBjbGFzcyBDaGVja2JveCBleHRlbmRzIEZvcm1Sb3dJdGVtIHtcclxuICBwcml2YXRlIG5hbWU6IHN0cmluZztcclxuICBwcml2YXRlIGNvbG9yOiBNQVRfQ09MT1IgPSBNQVRfQ09MT1IuUFJJTUFSWTtcclxuICBwcml2YXRlIGxhYmVsUG9zaXRpb246ICdiZWZvcmUnIHwgJ2FmdGVyJyA9ICdhZnRlcic7XHJcblxyXG4gIGdldE5hbWUgPSAoKTogc3RyaW5nID0+IHtcclxuICAgIHJldHVybiB0aGlzLm5hbWU7XHJcbiAgfTtcclxuXHJcbiAgc2V0TmFtZSA9IChuYW1lOiBzdHJpbmcpOiB2b2lkID0+IHtcclxuICAgIHRoaXMubmFtZSA9IG5hbWU7XHJcbiAgfTtcclxuXHJcbiAgZ2V0Q29sb3IgPSAoKTogTUFUX0NPTE9SID0+IHtcclxuICAgIHJldHVybiB0aGlzLmNvbG9yO1xyXG4gIH07XHJcblxyXG4gIHNldENvbG9yID0gKGNvbG9yOiBNQVRfQ09MT1IpOiB2b2lkID0+IHtcclxuICAgIHRoaXMuY29sb3IgPSBjb2xvcjtcclxuICB9O1xyXG5cclxuICBnZXRMYWJlbFBvc2l0aW9uID0gKCk6ICdiZWZvcmUnIHwgJ2FmdGVyJyA9PiB7XHJcbiAgICByZXR1cm4gdGhpcy5sYWJlbFBvc2l0aW9uO1xyXG4gIH07XHJcblxyXG4gIHNldExhYmVsUG9zaXRpb24gPSAobGFiZWxQb3NpdGlvbjogJ2JlZm9yZScgfCAnYWZ0ZXInKTogdm9pZCA9PiB7XHJcbiAgICB0aGlzLmxhYmVsUG9zaXRpb24gPSBsYWJlbFBvc2l0aW9uO1xyXG4gIH07XHJcbn1cclxuIl19