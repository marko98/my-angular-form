import { __extends } from "tslib";
import { FormRowItem } from '../form-row-item.model';
import { Subject, BehaviorSubject } from 'rxjs';
var Autocomplete = /** @class */ (function (_super) {
    __extends(Autocomplete, _super);
    function Autocomplete() {
        var _this = _super.call(this) || this;
        _this._sortingOptions = [];
        _this._inputPlaceholder = '';
        _this._criteria = '';
        _this._appearance = 'fill';
        _this._valueSubject = new Subject();
        _this._hiddenSubject = new BehaviorSubject(false);
        _this.getSortingOptions = function () {
            return _this._sortingOptions;
        };
        _this.setSortingOptions = function (sortingOptions) {
            _this._sortingOptions = sortingOptions;
            return true;
        };
        _this.getPageDataSource = function () {
            return _this._pageDataSource;
        };
        _this.setPageDataSource = function (pageDataSource) {
            _this._pageDataSource = pageDataSource;
            return true;
        };
        _this.getInputPlaceholder = function () {
            return _this._inputPlaceholder;
        };
        _this.setInputPlaceholder = function (inputPlaceholder) {
            _this._inputPlaceholder = inputPlaceholder;
            return true;
        };
        _this.getCriteria = function () {
            return _this._criteria;
        };
        _this.setCriteria = function (criteria) {
            _this._criteria = criteria;
            return true;
        };
        _this.getAppearance = function () {
            return _this._appearance;
        };
        _this.setAppearance = function (appearance) {
            _this._appearance = appearance;
            return true;
        };
        _this.getValueSubject = function () {
            return _this._valueSubject;
        };
        _this.getValue = function () {
            return _this._value;
        };
        _this.setValue = function (value) {
            _this._value = value;
            return true;
        };
        _this.getHiddenSubject = function () {
            return _this._hiddenSubject;
        };
        _this.setHiddenSubject = function (hiddenSubject) {
            _this._hiddenSubject = hiddenSubject;
        };
        return _this;
    }
    return Autocomplete;
}(FormRowItem));
export { Autocomplete };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0b2NvbXBsZXRlLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXktYW5ndWxhci1mb3JtLyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9tb2RlbC9zdHJ1Y3R1cmFsL2NvbXBvc2l0ZS9mb3JtL2F1dG9jb21wbGV0ZS9hdXRvY29tcGxldGUubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxXQUFXLEVBQXdCLE1BQU0sd0JBQXdCLENBQUM7QUFRM0UsT0FBTyxFQUFFLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFNaEQ7SUFBa0MsZ0NBQVc7SUFjM0M7UUFBQSxZQUNFLGlCQUFPLFNBQ1I7UUFmTyxxQkFBZSxHQUFhLEVBQUUsQ0FBQztRQU0vQix1QkFBaUIsR0FBVyxFQUFFLENBQUM7UUFDL0IsZUFBUyxHQUFXLEVBQUUsQ0FBQztRQUN2QixpQkFBVyxHQUErQyxNQUFNLENBQUM7UUFDakUsbUJBQWEsR0FBd0IsSUFBSSxPQUFPLEVBQUUsQ0FBQztRQUVuRCxvQkFBYyxHQUE2QixJQUFJLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQU12RSx1QkFBaUIsR0FBRztZQUN6QixPQUFPLEtBQUksQ0FBQyxlQUFlLENBQUM7UUFDOUIsQ0FBQyxDQUFDO1FBRUssdUJBQWlCLEdBQUcsVUFBQyxjQUF3QjtZQUNsRCxLQUFJLENBQUMsZUFBZSxHQUFHLGNBQWMsQ0FBQztZQUN0QyxPQUFPLElBQUksQ0FBQztRQUNkLENBQUMsQ0FBQztRQUVLLHVCQUFpQixHQUFHO1lBS3pCLE9BQU8sS0FBSSxDQUFDLGVBQWUsQ0FBQztRQUM5QixDQUFDLENBQUM7UUFFSyx1QkFBaUIsR0FBRyxVQUN6QixjQUlDO1lBRUQsS0FBSSxDQUFDLGVBQWUsR0FBRyxjQUFjLENBQUM7WUFDdEMsT0FBTyxJQUFJLENBQUM7UUFDZCxDQUFDLENBQUM7UUFFSyx5QkFBbUIsR0FBRztZQUMzQixPQUFPLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQztRQUNoQyxDQUFDLENBQUM7UUFFSyx5QkFBbUIsR0FBRyxVQUFDLGdCQUF3QjtZQUNwRCxLQUFJLENBQUMsaUJBQWlCLEdBQUcsZ0JBQWdCLENBQUM7WUFDMUMsT0FBTyxJQUFJLENBQUM7UUFDZCxDQUFDLENBQUM7UUFFSyxpQkFBVyxHQUFHO1lBQ25CLE9BQU8sS0FBSSxDQUFDLFNBQVMsQ0FBQztRQUN4QixDQUFDLENBQUM7UUFFSyxpQkFBVyxHQUFHLFVBQUMsUUFBZ0I7WUFDcEMsS0FBSSxDQUFDLFNBQVMsR0FBRyxRQUFRLENBQUM7WUFDMUIsT0FBTyxJQUFJLENBQUM7UUFDZCxDQUFDLENBQUM7UUFFSyxtQkFBYSxHQUFHO1lBQ3JCLE9BQU8sS0FBSSxDQUFDLFdBQVcsQ0FBQztRQUMxQixDQUFDLENBQUM7UUFFSyxtQkFBYSxHQUFHLFVBQ3JCLFVBQXNEO1lBRXRELEtBQUksQ0FBQyxXQUFXLEdBQUcsVUFBVSxDQUFDO1lBQzlCLE9BQU8sSUFBSSxDQUFDO1FBQ2QsQ0FBQyxDQUFDO1FBRUsscUJBQWUsR0FBRztZQUN2QixPQUFPLEtBQUksQ0FBQyxhQUFhLENBQUM7UUFDNUIsQ0FBQyxDQUFDO1FBRUssY0FBUSxHQUFHO1lBQ2hCLE9BQU8sS0FBSSxDQUFDLE1BQU0sQ0FBQztRQUNyQixDQUFDLENBQUM7UUFFSyxjQUFRLEdBQUcsVUFBQyxLQUFpQjtZQUNsQyxLQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztZQUNwQixPQUFPLElBQUksQ0FBQztRQUNkLENBQUMsQ0FBQztRQUVLLHNCQUFnQixHQUFHO1lBQ3hCLE9BQU8sS0FBSSxDQUFDLGNBQWMsQ0FBQztRQUM3QixDQUFDLENBQUM7UUFFSyxzQkFBZ0IsR0FBRyxVQUFDLGFBQXVDO1lBQ2hFLEtBQUksQ0FBQyxjQUFjLEdBQUcsYUFBYSxDQUFDO1FBQ3RDLENBQUMsQ0FBQzs7SUE5RUYsQ0FBQztJQStFSCxtQkFBQztBQUFELENBQUMsQUEvRkQsQ0FBa0MsV0FBVyxHQStGNUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBGb3JtUm93SXRlbSwgRm9ybVJvd0l0ZW1JbnRlcmZhY2UgfSBmcm9tICcuLi9mb3JtLXJvdy1pdGVtLm1vZGVsJztcclxuaW1wb3J0IHtcclxuICBQYWdlRGF0YVNvdXJjZSxcclxuICBFbnRpdGV0LFxyXG4gIEVOVElURVRfTU9ERUxfSU5URVJGQUNFLFxyXG4gIEF1dG9jb21wbGV0ZUludGVyZmFjZSxcclxuICBPcHRpb25JdGVtLFxyXG59IGZyb20gJ2F1dG9jb21wbGV0ZSc7XHJcbmltcG9ydCB7IFN1YmplY3QsIEJlaGF2aW9yU3ViamVjdCB9IGZyb20gJ3J4anMnO1xyXG5cclxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIE15QXV0b2NvbXBsZXRlSW50ZXJmYWNlXHJcbiAgZXh0ZW5kcyBGb3JtUm93SXRlbUludGVyZmFjZSxcclxuICAgIEF1dG9jb21wbGV0ZUludGVyZmFjZSB7fVxyXG5cclxuZXhwb3J0IGNsYXNzIEF1dG9jb21wbGV0ZSBleHRlbmRzIEZvcm1Sb3dJdGVtIHtcclxuICBwcml2YXRlIF9zb3J0aW5nT3B0aW9uczogc3RyaW5nW10gPSBbXTtcclxuICBwcml2YXRlIF9wYWdlRGF0YVNvdXJjZTogUGFnZURhdGFTb3VyY2U8XHJcbiAgICBFbnRpdGV0PGFueSwgYW55PixcclxuICAgIGFueSxcclxuICAgIEVOVElURVRfTU9ERUxfSU5URVJGQUNFXHJcbiAgPjtcclxuICBwcml2YXRlIF9pbnB1dFBsYWNlaG9sZGVyOiBzdHJpbmcgPSAnJztcclxuICBwcml2YXRlIF9jcml0ZXJpYTogc3RyaW5nID0gJyc7XHJcbiAgcHJpdmF0ZSBfYXBwZWFyYW5jZTogJ2xlZ2FjeScgfCAnc3RhbmRhcmQnIHwgJ2ZpbGwnIHwgJ291dGxpbmUnID0gJ2ZpbGwnO1xyXG4gIHByaXZhdGUgX3ZhbHVlU3ViamVjdDogU3ViamVjdDxPcHRpb25JdGVtPiA9IG5ldyBTdWJqZWN0KCk7XHJcbiAgcHJpdmF0ZSBfdmFsdWU6IE9wdGlvbkl0ZW07XHJcbiAgcHJpdmF0ZSBfaGlkZGVuU3ViamVjdDogQmVoYXZpb3JTdWJqZWN0PGJvb2xlYW4+ID0gbmV3IEJlaGF2aW9yU3ViamVjdChmYWxzZSk7XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgc3VwZXIoKTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBnZXRTb3J0aW5nT3B0aW9ucyA9ICgpOiBzdHJpbmdbXSA9PiB7XHJcbiAgICByZXR1cm4gdGhpcy5fc29ydGluZ09wdGlvbnM7XHJcbiAgfTtcclxuXHJcbiAgcHVibGljIHNldFNvcnRpbmdPcHRpb25zID0gKHNvcnRpbmdPcHRpb25zOiBzdHJpbmdbXSk6IGJvb2xlYW4gPT4ge1xyXG4gICAgdGhpcy5fc29ydGluZ09wdGlvbnMgPSBzb3J0aW5nT3B0aW9ucztcclxuICAgIHJldHVybiB0cnVlO1xyXG4gIH07XHJcblxyXG4gIHB1YmxpYyBnZXRQYWdlRGF0YVNvdXJjZSA9ICgpOiBQYWdlRGF0YVNvdXJjZTxcclxuICAgIEVudGl0ZXQ8YW55LCBhbnk+LFxyXG4gICAgYW55LFxyXG4gICAgRU5USVRFVF9NT0RFTF9JTlRFUkZBQ0VcclxuICA+ID0+IHtcclxuICAgIHJldHVybiB0aGlzLl9wYWdlRGF0YVNvdXJjZTtcclxuICB9O1xyXG5cclxuICBwdWJsaWMgc2V0UGFnZURhdGFTb3VyY2UgPSAoXHJcbiAgICBwYWdlRGF0YVNvdXJjZTogUGFnZURhdGFTb3VyY2U8XHJcbiAgICAgIEVudGl0ZXQ8YW55LCBhbnk+LFxyXG4gICAgICBhbnksXHJcbiAgICAgIEVOVElURVRfTU9ERUxfSU5URVJGQUNFXHJcbiAgICA+XHJcbiAgKTogYm9vbGVhbiA9PiB7XHJcbiAgICB0aGlzLl9wYWdlRGF0YVNvdXJjZSA9IHBhZ2VEYXRhU291cmNlO1xyXG4gICAgcmV0dXJuIHRydWU7XHJcbiAgfTtcclxuXHJcbiAgcHVibGljIGdldElucHV0UGxhY2Vob2xkZXIgPSAoKTogc3RyaW5nID0+IHtcclxuICAgIHJldHVybiB0aGlzLl9pbnB1dFBsYWNlaG9sZGVyO1xyXG4gIH07XHJcblxyXG4gIHB1YmxpYyBzZXRJbnB1dFBsYWNlaG9sZGVyID0gKGlucHV0UGxhY2Vob2xkZXI6IHN0cmluZyk6IGJvb2xlYW4gPT4ge1xyXG4gICAgdGhpcy5faW5wdXRQbGFjZWhvbGRlciA9IGlucHV0UGxhY2Vob2xkZXI7XHJcbiAgICByZXR1cm4gdHJ1ZTtcclxuICB9O1xyXG5cclxuICBwdWJsaWMgZ2V0Q3JpdGVyaWEgPSAoKTogc3RyaW5nID0+IHtcclxuICAgIHJldHVybiB0aGlzLl9jcml0ZXJpYTtcclxuICB9O1xyXG5cclxuICBwdWJsaWMgc2V0Q3JpdGVyaWEgPSAoY3JpdGVyaWE6IHN0cmluZyk6IGJvb2xlYW4gPT4ge1xyXG4gICAgdGhpcy5fY3JpdGVyaWEgPSBjcml0ZXJpYTtcclxuICAgIHJldHVybiB0cnVlO1xyXG4gIH07XHJcblxyXG4gIHB1YmxpYyBnZXRBcHBlYXJhbmNlID0gKCk6ICdsZWdhY3knIHwgJ3N0YW5kYXJkJyB8ICdmaWxsJyB8ICdvdXRsaW5lJyA9PiB7XHJcbiAgICByZXR1cm4gdGhpcy5fYXBwZWFyYW5jZTtcclxuICB9O1xyXG5cclxuICBwdWJsaWMgc2V0QXBwZWFyYW5jZSA9IChcclxuICAgIGFwcGVhcmFuY2U6ICdsZWdhY3knIHwgJ3N0YW5kYXJkJyB8ICdmaWxsJyB8ICdvdXRsaW5lJ1xyXG4gICk6IGJvb2xlYW4gPT4ge1xyXG4gICAgdGhpcy5fYXBwZWFyYW5jZSA9IGFwcGVhcmFuY2U7XHJcbiAgICByZXR1cm4gdHJ1ZTtcclxuICB9O1xyXG5cclxuICBwdWJsaWMgZ2V0VmFsdWVTdWJqZWN0ID0gKCk6IFN1YmplY3Q8T3B0aW9uSXRlbT4gPT4ge1xyXG4gICAgcmV0dXJuIHRoaXMuX3ZhbHVlU3ViamVjdDtcclxuICB9O1xyXG5cclxuICBwdWJsaWMgZ2V0VmFsdWUgPSAoKTogT3B0aW9uSXRlbSA9PiB7XHJcbiAgICByZXR1cm4gdGhpcy5fdmFsdWU7XHJcbiAgfTtcclxuXHJcbiAgcHVibGljIHNldFZhbHVlID0gKHZhbHVlOiBPcHRpb25JdGVtKTogYm9vbGVhbiA9PiB7XHJcbiAgICB0aGlzLl92YWx1ZSA9IHZhbHVlO1xyXG4gICAgcmV0dXJuIHRydWU7XHJcbiAgfTtcclxuXHJcbiAgcHVibGljIGdldEhpZGRlblN1YmplY3QgPSAoKTogQmVoYXZpb3JTdWJqZWN0PGJvb2xlYW4+ID0+IHtcclxuICAgIHJldHVybiB0aGlzLl9oaWRkZW5TdWJqZWN0O1xyXG4gIH07XHJcblxyXG4gIHB1YmxpYyBzZXRIaWRkZW5TdWJqZWN0ID0gKGhpZGRlblN1YmplY3Q6IEJlaGF2aW9yU3ViamVjdDxib29sZWFuPik6IHZvaWQgPT4ge1xyXG4gICAgdGhpcy5faGlkZGVuU3ViamVjdCA9IGhpZGRlblN1YmplY3Q7XHJcbiAgfTtcclxufVxyXG4iXX0=