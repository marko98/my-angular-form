import { __extends } from "tslib";
// model
import { FormRowItem } from '../form-row-item.model';
import { MAT_COLOR } from '../../../../../material.module';
export var DAY;
(function (DAY) {
    DAY[DAY["MONDAY"] = 1] = "MONDAY";
    DAY[DAY["TUESDAY"] = 2] = "TUESDAY";
    DAY[DAY["WEDNESDAY"] = 3] = "WEDNESDAY";
    DAY[DAY["THURSDAY"] = 4] = "THURSDAY";
    DAY[DAY["FRIDAY"] = 5] = "FRIDAY";
    DAY[DAY["SATURDAY"] = 6] = "SATURDAY";
    DAY[DAY["SUNDAY"] = 0] = "SUNDAY";
})(DAY || (DAY = {}));
var Datepicker = /** @class */ (function (_super) {
    __extends(Datepicker, _super);
    function Datepicker() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.startView = 'month';
        _this.color = MAT_COLOR.PRIMARY;
        _this.toggleSideSuffix = true;
        _this.appearance = 'legacy';
        _this.disabledDays = [];
        _this.getAppearance = function () {
            return _this.appearance;
        };
        _this.setAppearance = function (appearance) {
            _this.appearance = appearance;
        };
        _this.getDisabledDays = function () {
            return _this.disabledDays;
        };
        _this.setDisabledDays = function (disabledDays) {
            _this.disabledDays = disabledDays;
        };
        _this.getMatIcon = function () {
            return _this.matIcon;
        };
        _this.setMatIcon = function (matIcon) {
            _this.matIcon = matIcon;
        };
        _this.getLabelName = function () {
            return _this.labelName;
        };
        _this.setLabelName = function (labelName) {
            _this.labelName = labelName;
        };
        _this.getToggleSideSuffix = function () {
            return _this.toggleSideSuffix;
        };
        _this.setToggleSideSuffix = function (toggleSideSuffix) {
            _this.toggleSideSuffix = toggleSideSuffix;
        };
        _this.getColor = function () {
            return _this.color;
        };
        _this.setColor = function (color) {
            _this.color = color;
        };
        _this.getStartView = function () {
            return _this.startView;
        };
        _this.setStartView = function (startView) {
            _this.startView = startView;
        };
        _this.getStartAt = function () {
            return _this.startAt;
        };
        _this.setStartAt = function (startAt) {
            _this.startAt = startAt;
        };
        _this.getMin = function () {
            return _this.min;
        };
        _this.setMin = function (min) {
            _this.min = min;
        };
        _this.getMax = function () {
            return _this.max;
        };
        _this.setMax = function (max) {
            _this.max = max;
        };
        _this.getTimepickerInsideDatepickerInterface = function () {
            return _this.timepickerInsideDatepickerInterface;
        };
        _this.setTimepickerInsideDatepickerInterface = function (timepickerInsideDatepickerInterface) {
            _this.timepickerInsideDatepickerInterface = timepickerInsideDatepickerInterface;
        };
        return _this;
    }
    return Datepicker;
}(FormRowItem));
export { Datepicker };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0ZXBpY2tlci5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215LWFuZ3VsYXItZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvbW9kZWwvc3RydWN0dXJhbC9jb21wb3NpdGUvZm9ybS9kYXRlcGlja2VyL2RhdGVwaWNrZXIubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLFFBQVE7QUFDUixPQUFPLEVBQUUsV0FBVyxFQUF3QixNQUFNLHdCQUF3QixDQUFDO0FBQzNFLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQTJCM0QsTUFBTSxDQUFOLElBQVksR0FRWDtBQVJELFdBQVksR0FBRztJQUNiLGlDQUFVLENBQUE7SUFDVixtQ0FBVyxDQUFBO0lBQ1gsdUNBQWEsQ0FBQTtJQUNiLHFDQUFZLENBQUE7SUFDWixpQ0FBVSxDQUFBO0lBQ1YscUNBQVksQ0FBQTtJQUNaLGlDQUFVLENBQUE7QUFDWixDQUFDLEVBUlcsR0FBRyxLQUFILEdBQUcsUUFRZDtBQUVEO0lBQWdDLDhCQUFXO0lBQTNDO1FBQUEscUVBd0dDO1FBcEdTLGVBQVMsR0FBb0MsT0FBTyxDQUFDO1FBQ3JELFdBQUssR0FBYyxTQUFTLENBQUMsT0FBTyxDQUFDO1FBQ3JDLHNCQUFnQixHQUFZLElBQUksQ0FBQztRQUdqQyxnQkFBVSxHQUErQyxRQUFRLENBQUM7UUFDbEUsa0JBQVksR0FBVSxFQUFFLENBQUM7UUFHakMsbUJBQWEsR0FBRztZQUNkLE9BQU8sS0FBSSxDQUFDLFVBQVUsQ0FBQztRQUN6QixDQUFDLENBQUM7UUFFRixtQkFBYSxHQUFHLFVBQ2QsVUFBc0Q7WUFFdEQsS0FBSSxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUM7UUFDL0IsQ0FBQyxDQUFDO1FBRUYscUJBQWUsR0FBRztZQUNoQixPQUFPLEtBQUksQ0FBQyxZQUFZLENBQUM7UUFDM0IsQ0FBQyxDQUFDO1FBRUYscUJBQWUsR0FBRyxVQUFDLFlBQW1CO1lBQ3BDLEtBQUksQ0FBQyxZQUFZLEdBQUcsWUFBWSxDQUFDO1FBQ25DLENBQUMsQ0FBQztRQUVGLGdCQUFVLEdBQUc7WUFDWCxPQUFPLEtBQUksQ0FBQyxPQUFPLENBQUM7UUFDdEIsQ0FBQyxDQUFDO1FBRUYsZ0JBQVUsR0FBRyxVQUFDLE9BQWU7WUFDM0IsS0FBSSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7UUFDekIsQ0FBQyxDQUFDO1FBRUYsa0JBQVksR0FBRztZQUNiLE9BQU8sS0FBSSxDQUFDLFNBQVMsQ0FBQztRQUN4QixDQUFDLENBQUM7UUFFRixrQkFBWSxHQUFHLFVBQUMsU0FBaUI7WUFDL0IsS0FBSSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUM7UUFDN0IsQ0FBQyxDQUFDO1FBRUYseUJBQW1CLEdBQUc7WUFDcEIsT0FBTyxLQUFJLENBQUMsZ0JBQWdCLENBQUM7UUFDL0IsQ0FBQyxDQUFDO1FBRUYseUJBQW1CLEdBQUcsVUFBQyxnQkFBeUI7WUFDOUMsS0FBSSxDQUFDLGdCQUFnQixHQUFHLGdCQUFnQixDQUFDO1FBQzNDLENBQUMsQ0FBQztRQUVGLGNBQVEsR0FBRztZQUNULE9BQU8sS0FBSSxDQUFDLEtBQUssQ0FBQztRQUNwQixDQUFDLENBQUM7UUFFRixjQUFRLEdBQUcsVUFBQyxLQUFnQjtZQUMxQixLQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztRQUNyQixDQUFDLENBQUM7UUFFRixrQkFBWSxHQUFHO1lBQ2IsT0FBTyxLQUFJLENBQUMsU0FBUyxDQUFDO1FBQ3hCLENBQUMsQ0FBQztRQUVGLGtCQUFZLEdBQUcsVUFBQyxTQUEwQztZQUN4RCxLQUFJLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQztRQUM3QixDQUFDLENBQUM7UUFFRixnQkFBVSxHQUFHO1lBQ1gsT0FBTyxLQUFJLENBQUMsT0FBTyxDQUFDO1FBQ3RCLENBQUMsQ0FBQztRQUVGLGdCQUFVLEdBQUcsVUFBQyxPQUFhO1lBQ3pCLEtBQUksQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO1FBQ3pCLENBQUMsQ0FBQztRQUVGLFlBQU0sR0FBRztZQUNQLE9BQU8sS0FBSSxDQUFDLEdBQUcsQ0FBQztRQUNsQixDQUFDLENBQUM7UUFFRixZQUFNLEdBQUcsVUFBQyxHQUFTO1lBQ2pCLEtBQUksQ0FBQyxHQUFHLEdBQUcsR0FBRyxDQUFDO1FBQ2pCLENBQUMsQ0FBQztRQUVGLFlBQU0sR0FBRztZQUNQLE9BQU8sS0FBSSxDQUFDLEdBQUcsQ0FBQztRQUNsQixDQUFDLENBQUM7UUFFRixZQUFNLEdBQUcsVUFBQyxHQUFTO1lBQ2pCLEtBQUksQ0FBQyxHQUFHLEdBQUcsR0FBRyxDQUFDO1FBQ2pCLENBQUMsQ0FBQztRQUVGLDRDQUFzQyxHQUFHO1lBQ3ZDLE9BQU8sS0FBSSxDQUFDLG1DQUFtQyxDQUFDO1FBQ2xELENBQUMsQ0FBQztRQUVGLDRDQUFzQyxHQUFHLFVBQ3ZDLG1DQUF3RTtZQUV4RSxLQUFJLENBQUMsbUNBQW1DLEdBQUcsbUNBQW1DLENBQUM7UUFDakYsQ0FBQyxDQUFDOztJQUNKLENBQUM7SUFBRCxpQkFBQztBQUFELENBQUMsQUF4R0QsQ0FBZ0MsV0FBVyxHQXdHMUMiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBtb2RlbFxyXG5pbXBvcnQgeyBGb3JtUm93SXRlbSwgRm9ybVJvd0l0ZW1JbnRlcmZhY2UgfSBmcm9tICcuLi9mb3JtLXJvdy1pdGVtLm1vZGVsJztcclxuaW1wb3J0IHsgTUFUX0NPTE9SIH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vbWF0ZXJpYWwubW9kdWxlJztcclxuaW1wb3J0IHsgVGltZXBpY2tlckludGVyZmFjZSB9IGZyb20gJy4uL3RpbWVwaWNrZXIvdGltZXBpY2tlci5tb2RlbCc7XHJcblxyXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgRGF0ZXBpY2tlckludGVyZmFjZSBleHRlbmRzIEZvcm1Sb3dJdGVtSW50ZXJmYWNlIHtcclxuICBtaW4/OiBEYXRlO1xyXG4gIG1heD86IERhdGU7XHJcbiAgc3RhcnRBdD86IERhdGU7XHJcbiAgc3RhcnRWaWV3PzogJ21vbnRoJyB8ICd5ZWFyJyB8ICdtdWx0aS15ZWFyJztcclxuICBjb2xvcj86IE1BVF9DT0xPUjtcclxuICB0b2dnbGVTaWRlU3VmZml4PzogYm9vbGVhbjtcclxuICBtYXRJY29uPzogc3RyaW5nO1xyXG4gIGxhYmVsTmFtZTogc3RyaW5nO1xyXG4gIGRlZmF1bHRWYWx1ZTogRGF0ZTtcclxuICBhcHBlYXJhbmNlPzogJ2xlZ2FjeScgfCAnc3RhbmRhcmQnIHwgJ2ZpbGwnIHwgJ291dGxpbmUnO1xyXG4gIGRpc2FibGVkRGF5cz86IERBWVtdO1xyXG4gIHRpbWVwaWNrZXI/OiBUaW1lcGlja2VySW5zaWRlRGF0ZXBpY2tlckludGVyZmFjZTtcclxufVxyXG5cclxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIFRpbWVwaWNrZXJJbnNpZGVEYXRlcGlja2VySW50ZXJmYWNlIHtcclxuICBtaW4/OiBzdHJpbmc7XHJcbiAgbWF4Pzogc3RyaW5nO1xyXG4gIGxhYmVsTmFtZT86IHN0cmluZztcclxuICBidXR0b25BbGlnbj86ICdsZWZ0JyB8ICdyaWdodCc7XHJcbiAgZGlzYWJsZWQ/OiBib29sZWFuO1xyXG4gIGRlZmF1bHRWYWx1ZT86IHN0cmluZztcclxufVxyXG5cclxuZXhwb3J0IGVudW0gREFZIHtcclxuICBNT05EQVkgPSAxLFxyXG4gIFRVRVNEQVkgPSAyLFxyXG4gIFdFRE5FU0RBWSA9IDMsXHJcbiAgVEhVUlNEQVkgPSA0LFxyXG4gIEZSSURBWSA9IDUsXHJcbiAgU0FUVVJEQVkgPSA2LFxyXG4gIFNVTkRBWSA9IDAsXHJcbn1cclxuXHJcbmV4cG9ydCBjbGFzcyBEYXRlcGlja2VyIGV4dGVuZHMgRm9ybVJvd0l0ZW0ge1xyXG4gIHByaXZhdGUgbWluPzogRGF0ZTtcclxuICBwcml2YXRlIG1heD86IERhdGU7XHJcbiAgcHJpdmF0ZSBzdGFydEF0PzogRGF0ZTtcclxuICBwcml2YXRlIHN0YXJ0VmlldzogJ21vbnRoJyB8ICd5ZWFyJyB8ICdtdWx0aS15ZWFyJyA9ICdtb250aCc7XHJcbiAgcHJpdmF0ZSBjb2xvcjogTUFUX0NPTE9SID0gTUFUX0NPTE9SLlBSSU1BUlk7XHJcbiAgcHJpdmF0ZSB0b2dnbGVTaWRlU3VmZml4OiBib29sZWFuID0gdHJ1ZTtcclxuICBwcml2YXRlIGxhYmVsTmFtZTogc3RyaW5nO1xyXG4gIHByaXZhdGUgbWF0SWNvbj86IHN0cmluZztcclxuICBwcml2YXRlIGFwcGVhcmFuY2U6ICdsZWdhY3knIHwgJ3N0YW5kYXJkJyB8ICdmaWxsJyB8ICdvdXRsaW5lJyA9ICdsZWdhY3knO1xyXG4gIHByaXZhdGUgZGlzYWJsZWREYXlzOiBEQVlbXSA9IFtdO1xyXG4gIHByaXZhdGUgdGltZXBpY2tlckluc2lkZURhdGVwaWNrZXJJbnRlcmZhY2U6IFRpbWVwaWNrZXJJbnNpZGVEYXRlcGlja2VySW50ZXJmYWNlO1xyXG5cclxuICBnZXRBcHBlYXJhbmNlID0gKCk6ICdsZWdhY3knIHwgJ3N0YW5kYXJkJyB8ICdmaWxsJyB8ICdvdXRsaW5lJyA9PiB7XHJcbiAgICByZXR1cm4gdGhpcy5hcHBlYXJhbmNlO1xyXG4gIH07XHJcblxyXG4gIHNldEFwcGVhcmFuY2UgPSAoXHJcbiAgICBhcHBlYXJhbmNlOiAnbGVnYWN5JyB8ICdzdGFuZGFyZCcgfCAnZmlsbCcgfCAnb3V0bGluZSdcclxuICApOiB2b2lkID0+IHtcclxuICAgIHRoaXMuYXBwZWFyYW5jZSA9IGFwcGVhcmFuY2U7XHJcbiAgfTtcclxuXHJcbiAgZ2V0RGlzYWJsZWREYXlzID0gKCk6IERBWVtdID0+IHtcclxuICAgIHJldHVybiB0aGlzLmRpc2FibGVkRGF5cztcclxuICB9O1xyXG5cclxuICBzZXREaXNhYmxlZERheXMgPSAoZGlzYWJsZWREYXlzOiBEQVlbXSk6IHZvaWQgPT4ge1xyXG4gICAgdGhpcy5kaXNhYmxlZERheXMgPSBkaXNhYmxlZERheXM7XHJcbiAgfTtcclxuXHJcbiAgZ2V0TWF0SWNvbiA9ICgpOiBzdHJpbmcgPT4ge1xyXG4gICAgcmV0dXJuIHRoaXMubWF0SWNvbjtcclxuICB9O1xyXG5cclxuICBzZXRNYXRJY29uID0gKG1hdEljb246IHN0cmluZyk6IHZvaWQgPT4ge1xyXG4gICAgdGhpcy5tYXRJY29uID0gbWF0SWNvbjtcclxuICB9O1xyXG5cclxuICBnZXRMYWJlbE5hbWUgPSAoKTogc3RyaW5nID0+IHtcclxuICAgIHJldHVybiB0aGlzLmxhYmVsTmFtZTtcclxuICB9O1xyXG5cclxuICBzZXRMYWJlbE5hbWUgPSAobGFiZWxOYW1lOiBzdHJpbmcpOiB2b2lkID0+IHtcclxuICAgIHRoaXMubGFiZWxOYW1lID0gbGFiZWxOYW1lO1xyXG4gIH07XHJcblxyXG4gIGdldFRvZ2dsZVNpZGVTdWZmaXggPSAoKTogYm9vbGVhbiA9PiB7XHJcbiAgICByZXR1cm4gdGhpcy50b2dnbGVTaWRlU3VmZml4O1xyXG4gIH07XHJcblxyXG4gIHNldFRvZ2dsZVNpZGVTdWZmaXggPSAodG9nZ2xlU2lkZVN1ZmZpeDogYm9vbGVhbik6IHZvaWQgPT4ge1xyXG4gICAgdGhpcy50b2dnbGVTaWRlU3VmZml4ID0gdG9nZ2xlU2lkZVN1ZmZpeDtcclxuICB9O1xyXG5cclxuICBnZXRDb2xvciA9ICgpOiBNQVRfQ09MT1IgPT4ge1xyXG4gICAgcmV0dXJuIHRoaXMuY29sb3I7XHJcbiAgfTtcclxuXHJcbiAgc2V0Q29sb3IgPSAoY29sb3I6IE1BVF9DT0xPUik6IHZvaWQgPT4ge1xyXG4gICAgdGhpcy5jb2xvciA9IGNvbG9yO1xyXG4gIH07XHJcblxyXG4gIGdldFN0YXJ0VmlldyA9ICgpOiAnbW9udGgnIHwgJ3llYXInIHwgJ211bHRpLXllYXInID0+IHtcclxuICAgIHJldHVybiB0aGlzLnN0YXJ0VmlldztcclxuICB9O1xyXG5cclxuICBzZXRTdGFydFZpZXcgPSAoc3RhcnRWaWV3OiAnbW9udGgnIHwgJ3llYXInIHwgJ211bHRpLXllYXInKTogdm9pZCA9PiB7XHJcbiAgICB0aGlzLnN0YXJ0VmlldyA9IHN0YXJ0VmlldztcclxuICB9O1xyXG5cclxuICBnZXRTdGFydEF0ID0gKCk6IERhdGUgPT4ge1xyXG4gICAgcmV0dXJuIHRoaXMuc3RhcnRBdDtcclxuICB9O1xyXG5cclxuICBzZXRTdGFydEF0ID0gKHN0YXJ0QXQ6IERhdGUpOiB2b2lkID0+IHtcclxuICAgIHRoaXMuc3RhcnRBdCA9IHN0YXJ0QXQ7XHJcbiAgfTtcclxuXHJcbiAgZ2V0TWluID0gKCk6IERhdGUgPT4ge1xyXG4gICAgcmV0dXJuIHRoaXMubWluO1xyXG4gIH07XHJcblxyXG4gIHNldE1pbiA9IChtaW46IERhdGUpOiB2b2lkID0+IHtcclxuICAgIHRoaXMubWluID0gbWluO1xyXG4gIH07XHJcblxyXG4gIGdldE1heCA9ICgpOiBEYXRlID0+IHtcclxuICAgIHJldHVybiB0aGlzLm1heDtcclxuICB9O1xyXG5cclxuICBzZXRNYXggPSAobWF4OiBEYXRlKTogdm9pZCA9PiB7XHJcbiAgICB0aGlzLm1heCA9IG1heDtcclxuICB9O1xyXG5cclxuICBnZXRUaW1lcGlja2VySW5zaWRlRGF0ZXBpY2tlckludGVyZmFjZSA9ICgpOiBUaW1lcGlja2VySW5zaWRlRGF0ZXBpY2tlckludGVyZmFjZSA9PiB7XHJcbiAgICByZXR1cm4gdGhpcy50aW1lcGlja2VySW5zaWRlRGF0ZXBpY2tlckludGVyZmFjZTtcclxuICB9O1xyXG5cclxuICBzZXRUaW1lcGlja2VySW5zaWRlRGF0ZXBpY2tlckludGVyZmFjZSA9IChcclxuICAgIHRpbWVwaWNrZXJJbnNpZGVEYXRlcGlja2VySW50ZXJmYWNlOiBUaW1lcGlja2VySW5zaWRlRGF0ZXBpY2tlckludGVyZmFjZVxyXG4gICk6IHZvaWQgPT4ge1xyXG4gICAgdGhpcy50aW1lcGlja2VySW5zaWRlRGF0ZXBpY2tlckludGVyZmFjZSA9IHRpbWVwaWNrZXJJbnNpZGVEYXRlcGlja2VySW50ZXJmYWNlO1xyXG4gIH07XHJcbn1cclxuIl19