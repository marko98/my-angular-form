import { __extends } from "tslib";
// model
import { FormRowItem } from '../form-row-item.model';
var RadioButton = /** @class */ (function (_super) {
    __extends(RadioButton, _super);
    function RadioButton() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.getName = function () {
            return _this.name;
        };
        _this.getValue = function () {
            return _this.value;
        };
        _this.setName = function (name) {
            _this.name = name;
        };
        _this.setValue = function (value) {
            _this.value = value;
        };
        return _this;
    }
    return RadioButton;
}(FormRowItem));
export { RadioButton };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmFkaW8tYnV0dG9uLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXktYW5ndWxhci1mb3JtLyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9tb2RlbC9zdHJ1Y3R1cmFsL2NvbXBvc2l0ZS9mb3JtL3JhZGlvLWJ1dHRvbi1ncm91cC9yYWRpby1idXR0b24ubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLFFBQVE7QUFDUixPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFPckQ7SUFBaUMsK0JBQVc7SUFBNUM7UUFBQSxxRUFvQkM7UUFoQkcsYUFBTyxHQUFHO1lBQ04sT0FBTyxLQUFJLENBQUMsSUFBSSxDQUFDO1FBQ3JCLENBQUMsQ0FBQTtRQUVELGNBQVEsR0FBRztZQUNQLE9BQU8sS0FBSSxDQUFDLEtBQUssQ0FBQztRQUN0QixDQUFDLENBQUE7UUFFRCxhQUFPLEdBQUcsVUFBQyxJQUFZO1lBQ25CLEtBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ3JCLENBQUMsQ0FBQTtRQUVELGNBQVEsR0FBRyxVQUFDLEtBQVU7WUFDbEIsS0FBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7UUFDdkIsQ0FBQyxDQUFBOztJQUVMLENBQUM7SUFBRCxrQkFBQztBQUFELENBQUMsQUFwQkQsQ0FBaUMsV0FBVyxHQW9CM0MiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBtb2RlbFxyXG5pbXBvcnQgeyBGb3JtUm93SXRlbSB9IGZyb20gJy4uL2Zvcm0tcm93LWl0ZW0ubW9kZWwnO1xyXG5cclxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIFJhZGlvQnV0dG9uSW50ZXJmYWNlIHtcclxuICAgIG5hbWU6IHN0cmluZyxcclxuICAgIHZhbHVlOiBhbnlcclxufVxyXG5cclxuZXhwb3J0IGNsYXNzIFJhZGlvQnV0dG9uIGV4dGVuZHMgRm9ybVJvd0l0ZW0ge1xyXG4gICAgcHJpdmF0ZSBuYW1lOiBzdHJpbmc7XHJcbiAgICBwcml2YXRlIHZhbHVlOiBhbnk7XHJcblxyXG4gICAgZ2V0TmFtZSA9ICgpOiBzdHJpbmcgPT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLm5hbWU7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0VmFsdWUgPSAoKTogYW55ID0+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy52YWx1ZTtcclxuICAgIH1cclxuXHJcbiAgICBzZXROYW1lID0gKG5hbWU6IHN0cmluZyk6IHZvaWQgPT4ge1xyXG4gICAgICAgIHRoaXMubmFtZSA9IG5hbWU7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0VmFsdWUgPSAodmFsdWU6IGFueSk6IHZvaWQgPT4ge1xyXG4gICAgICAgIHRoaXMudmFsdWUgPSB2YWx1ZTtcclxuICAgIH1cclxuXHJcbn0iXX0=