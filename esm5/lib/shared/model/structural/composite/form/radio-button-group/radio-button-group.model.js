import { __extends } from "tslib";
// model
import { FormRowItem } from '../form-row-item.model';
import { RadioButton } from './radio-button.model';
var RadioButtonGroup = /** @class */ (function (_super) {
    __extends(RadioButtonGroup, _super);
    function RadioButtonGroup() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.direction = 'column';
        _this.labelPosition = 'after';
        _this.getDirection = function () {
            return _this.direction;
        };
        _this.getLabel = function () {
            return _this.label;
        };
        _this.setDirection = function (direction) {
            _this.direction = direction;
        };
        _this.setLabel = function (label) {
            _this.label = label;
        };
        _this.getLabelPosition = function () {
            return _this.labelPosition;
        };
        _this.setLabelPosition = function (labelPosition) {
            _this.labelPosition = labelPosition;
        };
        return _this;
    }
    RadioButtonGroup.prototype.addChild = function (rowButton) {
        this.children.push(rowButton);
        rowButton.setParent(this);
        return true;
    };
    RadioButtonGroup.prototype.addChildInterface = function (radioButtonInterface) {
        var radioButton = new RadioButton();
        radioButton.setName(radioButtonInterface.name);
        radioButton.setValue(radioButtonInterface.value);
        return this.addChild(radioButton);
    };
    RadioButtonGroup.prototype.removeChild = function (rowButton) {
        this.children = this.children.filter(function (child) { return child !== rowButton; });
        rowButton.setParent(undefined);
        return true;
    };
    RadioButtonGroup.prototype.shouldHaveChildren = function () {
        return true;
    };
    return RadioButtonGroup;
}(FormRowItem));
export { RadioButtonGroup };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmFkaW8tYnV0dG9uLWdyb3VwLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXktYW5ndWxhci1mb3JtLyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9tb2RlbC9zdHJ1Y3R1cmFsL2NvbXBvc2l0ZS9mb3JtL3JhZGlvLWJ1dHRvbi1ncm91cC9yYWRpby1idXR0b24tZ3JvdXAubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLFFBQVE7QUFDUixPQUFPLEVBQUUsV0FBVyxFQUF3QixNQUFNLHdCQUF3QixDQUFDO0FBQzNFLE9BQU8sRUFBd0IsV0FBVyxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFnQnpFO0lBQXNDLG9DQUFXO0lBQWpEO1FBQUEscUVBbURDO1FBbERTLGVBQVMsR0FBcUIsUUFBUSxDQUFDO1FBQ3ZDLG1CQUFhLEdBQXVCLE9BQU8sQ0FBQztRQUdwRCxrQkFBWSxHQUFHO1lBQ2IsT0FBTyxLQUFJLENBQUMsU0FBUyxDQUFDO1FBQ3hCLENBQUMsQ0FBQztRQUVGLGNBQVEsR0FBRztZQUNULE9BQU8sS0FBSSxDQUFDLEtBQUssQ0FBQztRQUNwQixDQUFDLENBQUM7UUFFRixrQkFBWSxHQUFHLFVBQUMsU0FBMkI7WUFDekMsS0FBSSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUM7UUFDN0IsQ0FBQyxDQUFDO1FBRUYsY0FBUSxHQUFHLFVBQUMsS0FBcUM7WUFDL0MsS0FBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7UUFDckIsQ0FBQyxDQUFDO1FBRUYsc0JBQWdCLEdBQUc7WUFDakIsT0FBTyxLQUFJLENBQUMsYUFBYSxDQUFDO1FBQzVCLENBQUMsQ0FBQztRQUVGLHNCQUFnQixHQUFHLFVBQUMsYUFBaUM7WUFDbkQsS0FBSSxDQUFDLGFBQWEsR0FBRyxhQUFhLENBQUM7UUFDckMsQ0FBQyxDQUFDOztJQXdCSixDQUFDO0lBdEJDLG1DQUFRLEdBQVIsVUFBUyxTQUFzQjtRQUM3QixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUM5QixTQUFTLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzFCLE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUVELDRDQUFpQixHQUFqQixVQUFrQixvQkFBMEM7UUFDMUQsSUFBSSxXQUFXLEdBQUcsSUFBSSxXQUFXLEVBQUUsQ0FBQztRQUNwQyxXQUFXLENBQUMsT0FBTyxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxDQUFDO1FBQy9DLFdBQVcsQ0FBQyxRQUFRLENBQUMsb0JBQW9CLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDakQsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDO0lBQ3BDLENBQUM7SUFFRCxzQ0FBVyxHQUFYLFVBQVksU0FBc0I7UUFDaEMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxVQUFDLEtBQUssSUFBSyxPQUFBLEtBQUssS0FBSyxTQUFTLEVBQW5CLENBQW1CLENBQUMsQ0FBQztRQUNyRSxTQUFTLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQy9CLE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUVELDZDQUFrQixHQUFsQjtRQUNFLE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUNILHVCQUFDO0FBQUQsQ0FBQyxBQW5ERCxDQUFzQyxXQUFXLEdBbURoRCIsInNvdXJjZXNDb250ZW50IjpbIi8vIG1vZGVsXHJcbmltcG9ydCB7IEZvcm1Sb3dJdGVtLCBGb3JtUm93SXRlbUludGVyZmFjZSB9IGZyb20gJy4uL2Zvcm0tcm93LWl0ZW0ubW9kZWwnO1xyXG5pbXBvcnQgeyBSYWRpb0J1dHRvbkludGVyZmFjZSwgUmFkaW9CdXR0b24gfSBmcm9tICcuL3JhZGlvLWJ1dHRvbi5tb2RlbCc7XHJcblxyXG4vLyBpbnRlcmZhY2VcclxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIFJhZGlvQnV0dG9uR3JvdXBJbnRlcmZhY2VcclxuICBleHRlbmRzIEZvcm1Sb3dJdGVtSW50ZXJmYWNlIHtcclxuICBvcHRpb25zOiBSYWRpb0J1dHRvbkludGVyZmFjZVtdO1xyXG4gIGRpcmVjdGlvbjogJ2NvbHVtbicgfCAncm93JztcclxuICBsYWJlbFBvc2l0aW9uPzogJ2FmdGVyJyB8ICdiZWZvcmUnO1xyXG4gIGxhYmVsPzogUmFkaW9CdXR0b25Hcm91cExhYmVsSW50ZXJmYWNlO1xyXG59XHJcblxyXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgUmFkaW9CdXR0b25Hcm91cExhYmVsSW50ZXJmYWNlIHtcclxuICBjb250ZXh0OiBzdHJpbmc7XHJcbiAgcG9zaXRpb246ICdhYm92ZScgfCAnYmVsbG93JztcclxufVxyXG5cclxuZXhwb3J0IGNsYXNzIFJhZGlvQnV0dG9uR3JvdXAgZXh0ZW5kcyBGb3JtUm93SXRlbSB7XHJcbiAgcHJpdmF0ZSBkaXJlY3Rpb246ICdjb2x1bW4nIHwgJ3JvdycgPSAnY29sdW1uJztcclxuICBwcml2YXRlIGxhYmVsUG9zaXRpb246ICdhZnRlcicgfCAnYmVmb3JlJyA9ICdhZnRlcic7XHJcbiAgcHJpdmF0ZSBsYWJlbDogUmFkaW9CdXR0b25Hcm91cExhYmVsSW50ZXJmYWNlO1xyXG5cclxuICBnZXREaXJlY3Rpb24gPSAoKTogc3RyaW5nID0+IHtcclxuICAgIHJldHVybiB0aGlzLmRpcmVjdGlvbjtcclxuICB9O1xyXG5cclxuICBnZXRMYWJlbCA9ICgpOiBSYWRpb0J1dHRvbkdyb3VwTGFiZWxJbnRlcmZhY2UgPT4ge1xyXG4gICAgcmV0dXJuIHRoaXMubGFiZWw7XHJcbiAgfTtcclxuXHJcbiAgc2V0RGlyZWN0aW9uID0gKGRpcmVjdGlvbjogJ2NvbHVtbicgfCAncm93Jyk6IHZvaWQgPT4ge1xyXG4gICAgdGhpcy5kaXJlY3Rpb24gPSBkaXJlY3Rpb247XHJcbiAgfTtcclxuXHJcbiAgc2V0TGFiZWwgPSAobGFiZWw6IFJhZGlvQnV0dG9uR3JvdXBMYWJlbEludGVyZmFjZSk6IHZvaWQgPT4ge1xyXG4gICAgdGhpcy5sYWJlbCA9IGxhYmVsO1xyXG4gIH07XHJcblxyXG4gIGdldExhYmVsUG9zaXRpb24gPSAoKTogJ2FmdGVyJyB8ICdiZWZvcmUnID0+IHtcclxuICAgIHJldHVybiB0aGlzLmxhYmVsUG9zaXRpb247XHJcbiAgfTtcclxuXHJcbiAgc2V0TGFiZWxQb3NpdGlvbiA9IChsYWJlbFBvc2l0aW9uOiAnYWZ0ZXInIHwgJ2JlZm9yZScpOiB2b2lkID0+IHtcclxuICAgIHRoaXMubGFiZWxQb3NpdGlvbiA9IGxhYmVsUG9zaXRpb247XHJcbiAgfTtcclxuXHJcbiAgYWRkQ2hpbGQocm93QnV0dG9uOiBSYWRpb0J1dHRvbik6IGJvb2xlYW4ge1xyXG4gICAgdGhpcy5jaGlsZHJlbi5wdXNoKHJvd0J1dHRvbik7XHJcbiAgICByb3dCdXR0b24uc2V0UGFyZW50KHRoaXMpO1xyXG4gICAgcmV0dXJuIHRydWU7XHJcbiAgfVxyXG5cclxuICBhZGRDaGlsZEludGVyZmFjZShyYWRpb0J1dHRvbkludGVyZmFjZTogUmFkaW9CdXR0b25JbnRlcmZhY2UpOiBib29sZWFuIHtcclxuICAgIGxldCByYWRpb0J1dHRvbiA9IG5ldyBSYWRpb0J1dHRvbigpO1xyXG4gICAgcmFkaW9CdXR0b24uc2V0TmFtZShyYWRpb0J1dHRvbkludGVyZmFjZS5uYW1lKTtcclxuICAgIHJhZGlvQnV0dG9uLnNldFZhbHVlKHJhZGlvQnV0dG9uSW50ZXJmYWNlLnZhbHVlKTtcclxuICAgIHJldHVybiB0aGlzLmFkZENoaWxkKHJhZGlvQnV0dG9uKTtcclxuICB9XHJcblxyXG4gIHJlbW92ZUNoaWxkKHJvd0J1dHRvbjogUmFkaW9CdXR0b24pOiBib29sZWFuIHtcclxuICAgIHRoaXMuY2hpbGRyZW4gPSB0aGlzLmNoaWxkcmVuLmZpbHRlcigoY2hpbGQpID0+IGNoaWxkICE9PSByb3dCdXR0b24pO1xyXG4gICAgcm93QnV0dG9uLnNldFBhcmVudCh1bmRlZmluZWQpO1xyXG4gICAgcmV0dXJuIHRydWU7XHJcbiAgfVxyXG5cclxuICBzaG91bGRIYXZlQ2hpbGRyZW4oKTogYm9vbGVhbiB7XHJcbiAgICByZXR1cm4gdHJ1ZTtcclxuICB9XHJcbn1cclxuIl19