import { __extends } from "tslib";
import { FormRowItem } from '../form-row-item.model';
var Timepicker = /** @class */ (function (_super) {
    __extends(Timepicker, _super);
    function Timepicker() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.appearance = 'legacy';
        _this.labelName = 'Pick time';
        _this.buttonAlign = 'right';
        _this.getAppearance = function () {
            return _this.appearance;
        };
        _this.setAppearance = function (appearance) {
            _this.appearance = appearance;
        };
        _this.getButtonAlign = function () {
            return _this.buttonAlign;
        };
        _this.setButtonAlign = function (buttonAlign) {
            _this.buttonAlign = buttonAlign;
        };
        _this.getMax = function () {
            return _this.max;
        };
        _this.setMax = function (max) {
            _this.max = max;
        };
        _this.getMin = function () {
            return _this.min;
        };
        _this.setMin = function (min) {
            _this.min = min;
        };
        _this.getLabelName = function () {
            return _this.labelName;
        };
        _this.setLabelName = function (labelName) {
            _this.labelName = labelName;
        };
        return _this;
    }
    return Timepicker;
}(FormRowItem));
export { Timepicker };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGltZXBpY2tlci5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215LWFuZ3VsYXItZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvbW9kZWwvc3RydWN0dXJhbC9jb21wb3NpdGUvZm9ybS90aW1lcGlja2VyL3RpbWVwaWNrZXIubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxXQUFXLEVBQXdCLE1BQU0sd0JBQXdCLENBQUM7QUFXM0U7SUFBZ0MsOEJBQVc7SUFBM0M7UUFBQSxxRUFnREM7UUEvQ1MsZ0JBQVUsR0FBK0MsUUFBUSxDQUFDO1FBR2xFLGVBQVMsR0FBVyxXQUFXLENBQUM7UUFDaEMsaUJBQVcsR0FBcUIsT0FBTyxDQUFDO1FBRWhELG1CQUFhLEdBQUc7WUFDZCxPQUFPLEtBQUksQ0FBQyxVQUFVLENBQUM7UUFDekIsQ0FBQyxDQUFDO1FBRUYsbUJBQWEsR0FBRyxVQUNkLFVBQXNEO1lBRXRELEtBQUksQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFDO1FBQy9CLENBQUMsQ0FBQztRQUVGLG9CQUFjLEdBQUc7WUFDZixPQUFPLEtBQUksQ0FBQyxXQUFXLENBQUM7UUFDMUIsQ0FBQyxDQUFDO1FBRUYsb0JBQWMsR0FBRyxVQUFDLFdBQTZCO1lBQzdDLEtBQUksQ0FBQyxXQUFXLEdBQUcsV0FBVyxDQUFDO1FBQ2pDLENBQUMsQ0FBQztRQUVGLFlBQU0sR0FBRztZQUNQLE9BQU8sS0FBSSxDQUFDLEdBQUcsQ0FBQztRQUNsQixDQUFDLENBQUM7UUFFRixZQUFNLEdBQUcsVUFBQyxHQUFXO1lBQ25CLEtBQUksQ0FBQyxHQUFHLEdBQUcsR0FBRyxDQUFDO1FBQ2pCLENBQUMsQ0FBQztRQUVGLFlBQU0sR0FBRztZQUNQLE9BQU8sS0FBSSxDQUFDLEdBQUcsQ0FBQztRQUNsQixDQUFDLENBQUM7UUFFRixZQUFNLEdBQUcsVUFBQyxHQUFXO1lBQ25CLEtBQUksQ0FBQyxHQUFHLEdBQUcsR0FBRyxDQUFDO1FBQ2pCLENBQUMsQ0FBQztRQUVGLGtCQUFZLEdBQUc7WUFDYixPQUFPLEtBQUksQ0FBQyxTQUFTLENBQUM7UUFDeEIsQ0FBQyxDQUFDO1FBRUYsa0JBQVksR0FBRyxVQUFDLFNBQWlCO1lBQy9CLEtBQUksQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDO1FBQzdCLENBQUMsQ0FBQzs7SUFDSixDQUFDO0lBQUQsaUJBQUM7QUFBRCxDQUFDLEFBaERELENBQWdDLFdBQVcsR0FnRDFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRm9ybVJvd0l0ZW0sIEZvcm1Sb3dJdGVtSW50ZXJmYWNlIH0gZnJvbSAnLi4vZm9ybS1yb3ctaXRlbS5tb2RlbCc7XHJcbmltcG9ydCB7IE1BVF9DT0xPUiB9IGZyb20gJ3Byb2plY3RzL215LWFuZ3VsYXItZm9ybS9zcmMvbGliL3NoYXJlZC9tYXRlcmlhbC5tb2R1bGUnO1xyXG5cclxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIFRpbWVwaWNrZXJJbnRlcmZhY2UgZXh0ZW5kcyBGb3JtUm93SXRlbUludGVyZmFjZSB7XHJcbiAgbWluPzogc3RyaW5nO1xyXG4gIG1heD86IHN0cmluZztcclxuICBhcHBlYXJhbmNlPzogJ2xlZ2FjeScgfCAnc3RhbmRhcmQnIHwgJ2ZpbGwnIHwgJ291dGxpbmUnO1xyXG4gIGxhYmVsTmFtZT86IHN0cmluZztcclxuICBidXR0b25BbGlnbj86ICdsZWZ0JyB8ICdyaWdodCc7XHJcbn1cclxuXHJcbmV4cG9ydCBjbGFzcyBUaW1lcGlja2VyIGV4dGVuZHMgRm9ybVJvd0l0ZW0ge1xyXG4gIHByaXZhdGUgYXBwZWFyYW5jZTogJ2xlZ2FjeScgfCAnc3RhbmRhcmQnIHwgJ2ZpbGwnIHwgJ291dGxpbmUnID0gJ2xlZ2FjeSc7XHJcbiAgcHJpdmF0ZSBtaW46IHN0cmluZztcclxuICBwcml2YXRlIG1heDogc3RyaW5nO1xyXG4gIHByaXZhdGUgbGFiZWxOYW1lOiBzdHJpbmcgPSAnUGljayB0aW1lJztcclxuICBwcml2YXRlIGJ1dHRvbkFsaWduOiAnbGVmdCcgfCAncmlnaHQnID0gJ3JpZ2h0JztcclxuXHJcbiAgZ2V0QXBwZWFyYW5jZSA9ICgpOiAnbGVnYWN5JyB8ICdzdGFuZGFyZCcgfCAnZmlsbCcgfCAnb3V0bGluZScgPT4ge1xyXG4gICAgcmV0dXJuIHRoaXMuYXBwZWFyYW5jZTtcclxuICB9O1xyXG5cclxuICBzZXRBcHBlYXJhbmNlID0gKFxyXG4gICAgYXBwZWFyYW5jZTogJ2xlZ2FjeScgfCAnc3RhbmRhcmQnIHwgJ2ZpbGwnIHwgJ291dGxpbmUnXHJcbiAgKTogdm9pZCA9PiB7XHJcbiAgICB0aGlzLmFwcGVhcmFuY2UgPSBhcHBlYXJhbmNlO1xyXG4gIH07XHJcblxyXG4gIGdldEJ1dHRvbkFsaWduID0gKCk6ICdsZWZ0JyB8ICdyaWdodCcgPT4ge1xyXG4gICAgcmV0dXJuIHRoaXMuYnV0dG9uQWxpZ247XHJcbiAgfTtcclxuXHJcbiAgc2V0QnV0dG9uQWxpZ24gPSAoYnV0dG9uQWxpZ246ICdsZWZ0JyB8ICdyaWdodCcpOiB2b2lkID0+IHtcclxuICAgIHRoaXMuYnV0dG9uQWxpZ24gPSBidXR0b25BbGlnbjtcclxuICB9O1xyXG5cclxuICBnZXRNYXggPSAoKTogc3RyaW5nID0+IHtcclxuICAgIHJldHVybiB0aGlzLm1heDtcclxuICB9O1xyXG5cclxuICBzZXRNYXggPSAobWF4OiBzdHJpbmcpOiB2b2lkID0+IHtcclxuICAgIHRoaXMubWF4ID0gbWF4O1xyXG4gIH07XHJcblxyXG4gIGdldE1pbiA9ICgpOiBzdHJpbmcgPT4ge1xyXG4gICAgcmV0dXJuIHRoaXMubWluO1xyXG4gIH07XHJcblxyXG4gIHNldE1pbiA9IChtaW46IHN0cmluZyk6IHZvaWQgPT4ge1xyXG4gICAgdGhpcy5taW4gPSBtaW47XHJcbiAgfTtcclxuXHJcbiAgZ2V0TGFiZWxOYW1lID0gKCk6IHN0cmluZyA9PiB7XHJcbiAgICByZXR1cm4gdGhpcy5sYWJlbE5hbWU7XHJcbiAgfTtcclxuXHJcbiAgc2V0TGFiZWxOYW1lID0gKGxhYmVsTmFtZTogc3RyaW5nKTogdm9pZCA9PiB7XHJcbiAgICB0aGlzLmxhYmVsTmFtZSA9IGxhYmVsTmFtZTtcclxuICB9O1xyXG59XHJcbiJdfQ==