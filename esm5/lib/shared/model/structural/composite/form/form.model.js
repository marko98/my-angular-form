import { __extends } from "tslib";
// model
import { Collection } from '../collection.model';
import { FormGroup } from '@angular/forms';
var Form = /** @class */ (function (_super) {
    __extends(Form, _super);
    function Form(formInterface) {
        var _this = _super.call(this) || this;
        // opcija za progress-spinner ili progress-bar
        _this.showSubmitButton = false;
        _this.submitOnlyIfFormValid = false;
        _this.showCancelButton = false;
        _this.showResetButton = false;
        _this.submitButtonText = 'Submit';
        _this.cancelButtonText = 'Cancel';
        _this.resetButtonText = 'Reset';
        _this._formGroup = new FormGroup({});
        _this.controlNames = [];
        _this.disableAllRowItems = function () {
            console.log('disable');
            _this.children.forEach(function (row) {
                row.disableAllRowItems();
            });
        };
        _this.getFormGroup = function () {
            return _this._formGroup;
        };
        _this.setFormGroup = function (formGroup) {
            _this._formGroup = formGroup;
        };
        if (formInterface.showSubmitButton)
            _this.showSubmitButton = formInterface.showSubmitButton;
        if (formInterface.submitOnlyIfFormValid)
            _this.submitOnlyIfFormValid = formInterface.submitOnlyIfFormValid;
        if (formInterface.showCancelButton)
            _this.showCancelButton = formInterface.showCancelButton;
        if (formInterface.showResetButton)
            _this.showResetButton = formInterface.showResetButton;
        if (formInterface.submitButtonText)
            _this.submitButtonText = formInterface.submitButtonText;
        if (formInterface.cancelButtonText)
            _this.cancelButtonText = formInterface.cancelButtonText;
        if (formInterface.resetButtonText)
            _this.resetButtonText = formInterface.resetButtonText;
        return _this;
    }
    Form.prototype.addChild = function (formRow) {
        formRow.getChildren().forEach(function (rowItem) {
            formRow.addChild(rowItem);
        });
        this.children.push(formRow);
        formRow.setParent(this);
        // console.log(this._formGroup.value);
        return true;
    };
    Form.prototype.removeChild = function (formRow) {
        formRow.getChildren().forEach(function (rowItem) {
            formRow.removeChild(rowItem);
        });
        this.children = this.children.filter(function (child) { return child !== formRow; });
        formRow.setParent(undefined);
        // console.log(this._formGroup.value);
        return true;
    };
    Form.prototype.shouldHaveChildren = function () {
        return true;
    };
    return Form;
}(Collection));
export { Form };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215LWFuZ3VsYXItZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvbW9kZWwvc3RydWN0dXJhbC9jb21wb3NpdGUvZm9ybS9mb3JtLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxRQUFRO0FBQ1IsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBR2pELE9BQU8sRUFBRSxTQUFTLEVBQWUsTUFBTSxnQkFBZ0IsQ0FBQztBQWF4RDtJQUEwQix3QkFBVTtJQWFsQyxjQUFZLGFBQTZCO1FBQXpDLFlBQ0UsaUJBQU8sU0FlUjtRQTVCRCw4Q0FBOEM7UUFDdkMsc0JBQWdCLEdBQVksS0FBSyxDQUFDO1FBQ2xDLDJCQUFxQixHQUFZLEtBQUssQ0FBQztRQUN2QyxzQkFBZ0IsR0FBWSxLQUFLLENBQUM7UUFDbEMscUJBQWUsR0FBWSxLQUFLLENBQUM7UUFDakMsc0JBQWdCLEdBQVcsUUFBUSxDQUFDO1FBQ3BDLHNCQUFnQixHQUFXLFFBQVEsQ0FBQztRQUNwQyxxQkFBZSxHQUFXLE9BQU8sQ0FBQztRQUNqQyxnQkFBVSxHQUFjLElBQUksU0FBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBRTNDLGtCQUFZLEdBQWEsRUFBRSxDQUFDO1FBb0I1Qix3QkFBa0IsR0FBRztZQUMxQixPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ3ZCLEtBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLFVBQUMsR0FBWTtnQkFDakMsR0FBRyxDQUFDLGtCQUFrQixFQUFFLENBQUM7WUFDM0IsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUM7UUFnQ0ssa0JBQVksR0FBRztZQUNwQixPQUFPLEtBQUksQ0FBQyxVQUFVLENBQUM7UUFDekIsQ0FBQyxDQUFDO1FBRUssa0JBQVksR0FBRyxVQUFDLFNBQW9CO1lBQ3pDLEtBQUksQ0FBQyxVQUFVLEdBQUcsU0FBUyxDQUFDO1FBQzlCLENBQUMsQ0FBQztRQTNEQSxJQUFJLGFBQWEsQ0FBQyxnQkFBZ0I7WUFDaEMsS0FBSSxDQUFDLGdCQUFnQixHQUFHLGFBQWEsQ0FBQyxnQkFBZ0IsQ0FBQztRQUN6RCxJQUFJLGFBQWEsQ0FBQyxxQkFBcUI7WUFDckMsS0FBSSxDQUFDLHFCQUFxQixHQUFHLGFBQWEsQ0FBQyxxQkFBcUIsQ0FBQztRQUNuRSxJQUFJLGFBQWEsQ0FBQyxnQkFBZ0I7WUFDaEMsS0FBSSxDQUFDLGdCQUFnQixHQUFHLGFBQWEsQ0FBQyxnQkFBZ0IsQ0FBQztRQUN6RCxJQUFJLGFBQWEsQ0FBQyxlQUFlO1lBQy9CLEtBQUksQ0FBQyxlQUFlLEdBQUcsYUFBYSxDQUFDLGVBQWUsQ0FBQztRQUN2RCxJQUFJLGFBQWEsQ0FBQyxnQkFBZ0I7WUFDaEMsS0FBSSxDQUFDLGdCQUFnQixHQUFHLGFBQWEsQ0FBQyxnQkFBZ0IsQ0FBQztRQUN6RCxJQUFJLGFBQWEsQ0FBQyxnQkFBZ0I7WUFDaEMsS0FBSSxDQUFDLGdCQUFnQixHQUFHLGFBQWEsQ0FBQyxnQkFBZ0IsQ0FBQztRQUN6RCxJQUFJLGFBQWEsQ0FBQyxlQUFlO1lBQy9CLEtBQUksQ0FBQyxlQUFlLEdBQUcsYUFBYSxDQUFDLGVBQWUsQ0FBQzs7SUFDekQsQ0FBQztJQVNELHVCQUFRLEdBQVIsVUFBUyxPQUFnQjtRQUN2QixPQUFPLENBQUMsV0FBVyxFQUFFLENBQUMsT0FBTyxDQUFDLFVBQUMsT0FBb0I7WUFDakQsT0FBTyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUM1QixDQUFDLENBQUMsQ0FBQztRQUVILElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQzVCLE9BQU8sQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7UUFFeEIsc0NBQXNDO1FBRXRDLE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUVELDBCQUFXLEdBQVgsVUFBWSxPQUFnQjtRQUMxQixPQUFPLENBQUMsV0FBVyxFQUFFLENBQUMsT0FBTyxDQUFDLFVBQUMsT0FBb0I7WUFDakQsT0FBTyxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUMvQixDQUFDLENBQUMsQ0FBQztRQUVILElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsVUFBQyxLQUFLLElBQUssT0FBQSxLQUFLLEtBQUssT0FBTyxFQUFqQixDQUFpQixDQUFDLENBQUM7UUFDbkUsT0FBTyxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUU3QixzQ0FBc0M7UUFFdEMsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBRUQsaUNBQWtCLEdBQWxCO1FBQ0UsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBU0gsV0FBQztBQUFELENBQUMsQUEzRUQsQ0FBMEIsVUFBVSxHQTJFbkMiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBtb2RlbFxyXG5pbXBvcnQgeyBDb2xsZWN0aW9uIH0gZnJvbSAnLi4vY29sbGVjdGlvbi5tb2RlbCc7XHJcbmltcG9ydCB7IEZvcm1Sb3cgfSBmcm9tICcuL2Zvcm0tcm93Lm1vZGVsJztcclxuaW1wb3J0IHsgRm9ybVJvd0l0ZW0gfSBmcm9tICcuL2Zvcm0tcm93LWl0ZW0ubW9kZWwnO1xyXG5pbXBvcnQgeyBGb3JtR3JvdXAsIEZvcm1Db250cm9sIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5cclxuLy8gaW50ZXJmYWNlXHJcbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBGb3JtSW50ZXJmYWNlIHtcclxuICBzaG93U3VibWl0QnV0dG9uPzogYm9vbGVhbjtcclxuICBzdWJtaXRCdXR0b25UZXh0Pzogc3RyaW5nO1xyXG4gIHNob3dDYW5jZWxCdXR0b24/OiBib29sZWFuO1xyXG4gIGNhbmNlbEJ1dHRvblRleHQ/OiBzdHJpbmc7XHJcbiAgc2hvd1Jlc2V0QnV0dG9uPzogYm9vbGVhbjtcclxuICByZXNldEJ1dHRvblRleHQ/OiBzdHJpbmc7XHJcbiAgc3VibWl0T25seUlmRm9ybVZhbGlkPzogYm9vbGVhbjtcclxufVxyXG5cclxuZXhwb3J0IGNsYXNzIEZvcm0gZXh0ZW5kcyBDb2xsZWN0aW9uIHtcclxuICAvLyBvcGNpamEgemEgcHJvZ3Jlc3Mtc3Bpbm5lciBpbGkgcHJvZ3Jlc3MtYmFyXHJcbiAgcHVibGljIHNob3dTdWJtaXRCdXR0b246IGJvb2xlYW4gPSBmYWxzZTtcclxuICBwdWJsaWMgc3VibWl0T25seUlmRm9ybVZhbGlkOiBib29sZWFuID0gZmFsc2U7XHJcbiAgcHVibGljIHNob3dDYW5jZWxCdXR0b246IGJvb2xlYW4gPSBmYWxzZTtcclxuICBwdWJsaWMgc2hvd1Jlc2V0QnV0dG9uOiBib29sZWFuID0gZmFsc2U7XHJcbiAgcHVibGljIHN1Ym1pdEJ1dHRvblRleHQ6IHN0cmluZyA9ICdTdWJtaXQnO1xyXG4gIHB1YmxpYyBjYW5jZWxCdXR0b25UZXh0OiBzdHJpbmcgPSAnQ2FuY2VsJztcclxuICBwdWJsaWMgcmVzZXRCdXR0b25UZXh0OiBzdHJpbmcgPSAnUmVzZXQnO1xyXG4gIHByaXZhdGUgX2Zvcm1Hcm91cDogRm9ybUdyb3VwID0gbmV3IEZvcm1Hcm91cCh7fSk7XHJcblxyXG4gIHB1YmxpYyBjb250cm9sTmFtZXM6IHN0cmluZ1tdID0gW107XHJcblxyXG4gIGNvbnN0cnVjdG9yKGZvcm1JbnRlcmZhY2U/OiBGb3JtSW50ZXJmYWNlKSB7XHJcbiAgICBzdXBlcigpO1xyXG4gICAgaWYgKGZvcm1JbnRlcmZhY2Uuc2hvd1N1Ym1pdEJ1dHRvbilcclxuICAgICAgdGhpcy5zaG93U3VibWl0QnV0dG9uID0gZm9ybUludGVyZmFjZS5zaG93U3VibWl0QnV0dG9uO1xyXG4gICAgaWYgKGZvcm1JbnRlcmZhY2Uuc3VibWl0T25seUlmRm9ybVZhbGlkKVxyXG4gICAgICB0aGlzLnN1Ym1pdE9ubHlJZkZvcm1WYWxpZCA9IGZvcm1JbnRlcmZhY2Uuc3VibWl0T25seUlmRm9ybVZhbGlkO1xyXG4gICAgaWYgKGZvcm1JbnRlcmZhY2Uuc2hvd0NhbmNlbEJ1dHRvbilcclxuICAgICAgdGhpcy5zaG93Q2FuY2VsQnV0dG9uID0gZm9ybUludGVyZmFjZS5zaG93Q2FuY2VsQnV0dG9uO1xyXG4gICAgaWYgKGZvcm1JbnRlcmZhY2Uuc2hvd1Jlc2V0QnV0dG9uKVxyXG4gICAgICB0aGlzLnNob3dSZXNldEJ1dHRvbiA9IGZvcm1JbnRlcmZhY2Uuc2hvd1Jlc2V0QnV0dG9uO1xyXG4gICAgaWYgKGZvcm1JbnRlcmZhY2Uuc3VibWl0QnV0dG9uVGV4dClcclxuICAgICAgdGhpcy5zdWJtaXRCdXR0b25UZXh0ID0gZm9ybUludGVyZmFjZS5zdWJtaXRCdXR0b25UZXh0O1xyXG4gICAgaWYgKGZvcm1JbnRlcmZhY2UuY2FuY2VsQnV0dG9uVGV4dClcclxuICAgICAgdGhpcy5jYW5jZWxCdXR0b25UZXh0ID0gZm9ybUludGVyZmFjZS5jYW5jZWxCdXR0b25UZXh0O1xyXG4gICAgaWYgKGZvcm1JbnRlcmZhY2UucmVzZXRCdXR0b25UZXh0KVxyXG4gICAgICB0aGlzLnJlc2V0QnV0dG9uVGV4dCA9IGZvcm1JbnRlcmZhY2UucmVzZXRCdXR0b25UZXh0O1xyXG4gIH1cclxuXHJcbiAgcHVibGljIGRpc2FibGVBbGxSb3dJdGVtcyA9ICgpOiB2b2lkID0+IHtcclxuICAgIGNvbnNvbGUubG9nKCdkaXNhYmxlJyk7XHJcbiAgICB0aGlzLmNoaWxkcmVuLmZvckVhY2goKHJvdzogRm9ybVJvdykgPT4ge1xyXG4gICAgICByb3cuZGlzYWJsZUFsbFJvd0l0ZW1zKCk7XHJcbiAgICB9KTtcclxuICB9O1xyXG5cclxuICBhZGRDaGlsZChmb3JtUm93OiBGb3JtUm93KTogYm9vbGVhbiB7XHJcbiAgICBmb3JtUm93LmdldENoaWxkcmVuKCkuZm9yRWFjaCgocm93SXRlbTogRm9ybVJvd0l0ZW0pID0+IHtcclxuICAgICAgZm9ybVJvdy5hZGRDaGlsZChyb3dJdGVtKTtcclxuICAgIH0pO1xyXG5cclxuICAgIHRoaXMuY2hpbGRyZW4ucHVzaChmb3JtUm93KTtcclxuICAgIGZvcm1Sb3cuc2V0UGFyZW50KHRoaXMpO1xyXG5cclxuICAgIC8vIGNvbnNvbGUubG9nKHRoaXMuX2Zvcm1Hcm91cC52YWx1ZSk7XHJcblxyXG4gICAgcmV0dXJuIHRydWU7XHJcbiAgfVxyXG5cclxuICByZW1vdmVDaGlsZChmb3JtUm93OiBGb3JtUm93KTogYm9vbGVhbiB7XHJcbiAgICBmb3JtUm93LmdldENoaWxkcmVuKCkuZm9yRWFjaCgocm93SXRlbTogRm9ybVJvd0l0ZW0pID0+IHtcclxuICAgICAgZm9ybVJvdy5yZW1vdmVDaGlsZChyb3dJdGVtKTtcclxuICAgIH0pO1xyXG5cclxuICAgIHRoaXMuY2hpbGRyZW4gPSB0aGlzLmNoaWxkcmVuLmZpbHRlcigoY2hpbGQpID0+IGNoaWxkICE9PSBmb3JtUm93KTtcclxuICAgIGZvcm1Sb3cuc2V0UGFyZW50KHVuZGVmaW5lZCk7XHJcblxyXG4gICAgLy8gY29uc29sZS5sb2codGhpcy5fZm9ybUdyb3VwLnZhbHVlKTtcclxuXHJcbiAgICByZXR1cm4gdHJ1ZTtcclxuICB9XHJcblxyXG4gIHNob3VsZEhhdmVDaGlsZHJlbigpOiBib29sZWFuIHtcclxuICAgIHJldHVybiB0cnVlO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIGdldEZvcm1Hcm91cCA9ICgpOiBGb3JtR3JvdXAgPT4ge1xyXG4gICAgcmV0dXJuIHRoaXMuX2Zvcm1Hcm91cDtcclxuICB9O1xyXG5cclxuICBwdWJsaWMgc2V0Rm9ybUdyb3VwID0gKGZvcm1Hcm91cDogRm9ybUdyb3VwKTogdm9pZCA9PiB7XHJcbiAgICB0aGlzLl9mb3JtR3JvdXAgPSBmb3JtR3JvdXA7XHJcbiAgfTtcclxufVxyXG4iXX0=