import { __extends } from "tslib";
import { FormRowItem } from '../form-row-item.model';
var Slider = /** @class */ (function (_super) {
    __extends(Slider, _super);
    function Slider() {
        var _this = _super.call(this) || this;
        _this.vertical = false;
        _this.thumbLabel = false;
        _this.invert = false;
        _this.step = 1;
        _this.tickInterval = 0;
        _this.getMinValue = function () {
            return _this.minValue;
        };
        _this.setMinValue = function (minValue) {
            _this.minValue = minValue;
        };
        _this.getMaxValue = function () {
            return _this.maxValue;
        };
        _this.setMaxValue = function (maxValue) {
            _this.maxValue = maxValue;
        };
        _this.getInvert = function () {
            return _this.invert;
        };
        _this.setInvert = function (invert) {
            _this.invert = invert;
        };
        _this.getVertical = function () {
            return _this.vertical;
        };
        _this.setVertical = function (vertical) {
            _this.vertical = vertical;
        };
        _this.getThumbLabel = function () {
            return _this.thumbLabel;
        };
        _this.setThumbLabel = function (thumbLabel) {
            _this.thumbLabel = thumbLabel;
        };
        _this.getStep = function () {
            return _this.step;
        };
        _this.setStep = function (step) {
            _this.step = step;
        };
        _this.getTickInterval = function () {
            return _this.tickInterval;
        };
        _this.setTickInterval = function (tickInterval) {
            _this.tickInterval = tickInterval;
        };
        return _this;
    }
    return Slider;
}(FormRowItem));
export { Slider };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2xpZGVyLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXktYW5ndWxhci1mb3JtLyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9tb2RlbC9zdHJ1Y3R1cmFsL2NvbXBvc2l0ZS9mb3JtL3NsaWRlci9zbGlkZXIubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxXQUFXLEVBQXdCLE1BQU0sd0JBQXdCLENBQUM7QUFhM0U7SUFBNEIsMEJBQVc7SUFTckM7UUFBQSxZQUNFLGlCQUFPLFNBQ1I7UUFSTyxjQUFRLEdBQVksS0FBSyxDQUFDO1FBQzFCLGdCQUFVLEdBQVksS0FBSyxDQUFDO1FBQzVCLFlBQU0sR0FBWSxLQUFLLENBQUM7UUFDeEIsVUFBSSxHQUFXLENBQUMsQ0FBQztRQUNqQixrQkFBWSxHQUFXLENBQUMsQ0FBQztRQU0xQixpQkFBVyxHQUFHO1lBQ25CLE9BQU8sS0FBSSxDQUFDLFFBQVEsQ0FBQztRQUN2QixDQUFDLENBQUM7UUFFSyxpQkFBVyxHQUFHLFVBQUMsUUFBeUI7WUFDN0MsS0FBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7UUFDM0IsQ0FBQyxDQUFDO1FBRUssaUJBQVcsR0FBRztZQUNuQixPQUFPLEtBQUksQ0FBQyxRQUFRLENBQUM7UUFDdkIsQ0FBQyxDQUFDO1FBRUssaUJBQVcsR0FBRyxVQUFDLFFBQXlCO1lBQzdDLEtBQUksQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDO1FBQzNCLENBQUMsQ0FBQztRQUVLLGVBQVMsR0FBRztZQUNqQixPQUFPLEtBQUksQ0FBQyxNQUFNLENBQUM7UUFDckIsQ0FBQyxDQUFDO1FBRUssZUFBUyxHQUFHLFVBQUMsTUFBZTtZQUNqQyxLQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztRQUN2QixDQUFDLENBQUM7UUFFSyxpQkFBVyxHQUFHO1lBQ25CLE9BQU8sS0FBSSxDQUFDLFFBQVEsQ0FBQztRQUN2QixDQUFDLENBQUM7UUFFSyxpQkFBVyxHQUFHLFVBQUMsUUFBaUI7WUFDckMsS0FBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7UUFDM0IsQ0FBQyxDQUFDO1FBRUssbUJBQWEsR0FBRztZQUNyQixPQUFPLEtBQUksQ0FBQyxVQUFVLENBQUM7UUFDekIsQ0FBQyxDQUFDO1FBRUssbUJBQWEsR0FBRyxVQUFDLFVBQW1CO1lBQ3pDLEtBQUksQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFDO1FBQy9CLENBQUMsQ0FBQztRQUVLLGFBQU8sR0FBRztZQUNmLE9BQU8sS0FBSSxDQUFDLElBQUksQ0FBQztRQUNuQixDQUFDLENBQUM7UUFFSyxhQUFPLEdBQUcsVUFBQyxJQUFZO1lBQzVCLEtBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ25CLENBQUMsQ0FBQztRQUVLLHFCQUFlLEdBQUc7WUFDdkIsT0FBTyxLQUFJLENBQUMsWUFBWSxDQUFDO1FBQzNCLENBQUMsQ0FBQztRQUVLLHFCQUFlLEdBQUcsVUFBQyxZQUFvQjtZQUM1QyxLQUFJLENBQUMsWUFBWSxHQUFHLFlBQVksQ0FBQztRQUNuQyxDQUFDLENBQUM7O0lBeERGLENBQUM7SUF5REgsYUFBQztBQUFELENBQUMsQUFwRUQsQ0FBNEIsV0FBVyxHQW9FdEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBGb3JtUm93SXRlbSwgRm9ybVJvd0l0ZW1JbnRlcmZhY2UgfSBmcm9tICcuLi9mb3JtLXJvdy1pdGVtLm1vZGVsJztcclxuXHJcbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBTbGlkZXJJbnRlcmZhY2UgZXh0ZW5kcyBGb3JtUm93SXRlbUludGVyZmFjZSB7XHJcbiAgbWluVmFsdWU6IHN0cmluZyB8IG51bWJlcjtcclxuICBtYXhWYWx1ZTogc3RyaW5nIHwgbnVtYmVyO1xyXG4gIGRpc2FibGVkOiBib29sZWFuO1xyXG4gIGludmVydDogYm9vbGVhbjtcclxuICB0aHVtYkxhYmVsOiBib29sZWFuO1xyXG4gIHZlcnRpY2FsOiBib29sZWFuO1xyXG4gIHN0ZXA6IG51bWJlcjtcclxuICB0aWNrSW50ZXJ2YWw6IG51bWJlcjtcclxufVxyXG5cclxuZXhwb3J0IGNsYXNzIFNsaWRlciBleHRlbmRzIEZvcm1Sb3dJdGVtIHtcclxuICBwcml2YXRlIG1pblZhbHVlOiBzdHJpbmcgfCBudW1iZXI7XHJcbiAgcHJpdmF0ZSBtYXhWYWx1ZTogc3RyaW5nIHwgbnVtYmVyO1xyXG4gIHByaXZhdGUgdmVydGljYWw6IGJvb2xlYW4gPSBmYWxzZTtcclxuICBwcml2YXRlIHRodW1iTGFiZWw6IGJvb2xlYW4gPSBmYWxzZTtcclxuICBwcml2YXRlIGludmVydDogYm9vbGVhbiA9IGZhbHNlO1xyXG4gIHByaXZhdGUgc3RlcDogbnVtYmVyID0gMTtcclxuICBwcml2YXRlIHRpY2tJbnRlcnZhbDogbnVtYmVyID0gMDtcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7XHJcbiAgICBzdXBlcigpO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIGdldE1pblZhbHVlID0gKCk6IHN0cmluZyB8IG51bWJlciA9PiB7XHJcbiAgICByZXR1cm4gdGhpcy5taW5WYWx1ZTtcclxuICB9O1xyXG5cclxuICBwdWJsaWMgc2V0TWluVmFsdWUgPSAobWluVmFsdWU6IHN0cmluZyB8IG51bWJlcik6IHZvaWQgPT4ge1xyXG4gICAgdGhpcy5taW5WYWx1ZSA9IG1pblZhbHVlO1xyXG4gIH07XHJcblxyXG4gIHB1YmxpYyBnZXRNYXhWYWx1ZSA9ICgpOiBzdHJpbmcgfCBudW1iZXIgPT4ge1xyXG4gICAgcmV0dXJuIHRoaXMubWF4VmFsdWU7XHJcbiAgfTtcclxuXHJcbiAgcHVibGljIHNldE1heFZhbHVlID0gKG1heFZhbHVlOiBzdHJpbmcgfCBudW1iZXIpOiB2b2lkID0+IHtcclxuICAgIHRoaXMubWF4VmFsdWUgPSBtYXhWYWx1ZTtcclxuICB9O1xyXG5cclxuICBwdWJsaWMgZ2V0SW52ZXJ0ID0gKCk6IGJvb2xlYW4gPT4ge1xyXG4gICAgcmV0dXJuIHRoaXMuaW52ZXJ0O1xyXG4gIH07XHJcblxyXG4gIHB1YmxpYyBzZXRJbnZlcnQgPSAoaW52ZXJ0OiBib29sZWFuKTogdm9pZCA9PiB7XHJcbiAgICB0aGlzLmludmVydCA9IGludmVydDtcclxuICB9O1xyXG5cclxuICBwdWJsaWMgZ2V0VmVydGljYWwgPSAoKTogYm9vbGVhbiA9PiB7XHJcbiAgICByZXR1cm4gdGhpcy52ZXJ0aWNhbDtcclxuICB9O1xyXG5cclxuICBwdWJsaWMgc2V0VmVydGljYWwgPSAodmVydGljYWw6IGJvb2xlYW4pOiB2b2lkID0+IHtcclxuICAgIHRoaXMudmVydGljYWwgPSB2ZXJ0aWNhbDtcclxuICB9O1xyXG5cclxuICBwdWJsaWMgZ2V0VGh1bWJMYWJlbCA9ICgpOiBib29sZWFuID0+IHtcclxuICAgIHJldHVybiB0aGlzLnRodW1iTGFiZWw7XHJcbiAgfTtcclxuXHJcbiAgcHVibGljIHNldFRodW1iTGFiZWwgPSAodGh1bWJMYWJlbDogYm9vbGVhbik6IHZvaWQgPT4ge1xyXG4gICAgdGhpcy50aHVtYkxhYmVsID0gdGh1bWJMYWJlbDtcclxuICB9O1xyXG5cclxuICBwdWJsaWMgZ2V0U3RlcCA9ICgpOiBudW1iZXIgPT4ge1xyXG4gICAgcmV0dXJuIHRoaXMuc3RlcDtcclxuICB9O1xyXG5cclxuICBwdWJsaWMgc2V0U3RlcCA9IChzdGVwOiBudW1iZXIpOiB2b2lkID0+IHtcclxuICAgIHRoaXMuc3RlcCA9IHN0ZXA7XHJcbiAgfTtcclxuXHJcbiAgcHVibGljIGdldFRpY2tJbnRlcnZhbCA9ICgpOiBudW1iZXIgPT4ge1xyXG4gICAgcmV0dXJuIHRoaXMudGlja0ludGVydmFsO1xyXG4gIH07XHJcblxyXG4gIHB1YmxpYyBzZXRUaWNrSW50ZXJ2YWwgPSAodGlja0ludGVydmFsOiBudW1iZXIpOiB2b2lkID0+IHtcclxuICAgIHRoaXMudGlja0ludGVydmFsID0gdGlja0ludGVydmFsO1xyXG4gIH07XHJcbn1cclxuIl19