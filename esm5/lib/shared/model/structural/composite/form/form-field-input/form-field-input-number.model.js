import { __extends } from "tslib";
// model
import { FormFieldInput, } from './form-field-input.model';
var FormFieldInputNumber = /** @class */ (function (_super) {
    __extends(FormFieldInputNumber, _super);
    function FormFieldInputNumber() {
        var _this = _super.call(this) || this;
        _this.step = 1;
        _this.getStep = function () {
            return _this.step;
        };
        _this.setStep = function (step) {
            _this.step = step;
        };
        return _this;
    }
    return FormFieldInputNumber;
}(FormFieldInput));
export { FormFieldInputNumber };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1maWVsZC1pbnB1dC1udW1iZXIubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9teS1hbmd1bGFyLWZvcm0vIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL21vZGVsL3N0cnVjdHVyYWwvY29tcG9zaXRlL2Zvcm0vZm9ybS1maWVsZC1pbnB1dC9mb3JtLWZpZWxkLWlucHV0LW51bWJlci5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsUUFBUTtBQUNSLE9BQU8sRUFDTCxjQUFjLEdBRWYsTUFBTSwwQkFBMEIsQ0FBQztBQU9sQztJQUEwQyx3Q0FBYztJQUd0RDtRQUFBLFlBQ0UsaUJBQU8sU0FDUjtRQUpPLFVBQUksR0FBVyxDQUFDLENBQUM7UUFNekIsYUFBTyxHQUFHO1lBQ1IsT0FBTyxLQUFJLENBQUMsSUFBSSxDQUFDO1FBQ25CLENBQUMsQ0FBQztRQUVGLGFBQU8sR0FBRyxVQUFDLElBQVk7WUFDckIsS0FBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7UUFDbkIsQ0FBQyxDQUFDOztJQVJGLENBQUM7SUFTSCwyQkFBQztBQUFELENBQUMsQUFkRCxDQUEwQyxjQUFjLEdBY3ZEIiwic291cmNlc0NvbnRlbnQiOlsiLy8gbW9kZWxcclxuaW1wb3J0IHtcclxuICBGb3JtRmllbGRJbnB1dCxcclxuICBGb3JtRmllbGRJbnB1dEludGVyZmFjZSxcclxufSBmcm9tICcuL2Zvcm0tZmllbGQtaW5wdXQubW9kZWwnO1xyXG5cclxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIEZvcm1GaWVsZElucHV0TnVtYmVySW50ZXJmYWNlXHJcbiAgZXh0ZW5kcyBGb3JtRmllbGRJbnB1dEludGVyZmFjZSB7XHJcbiAgc3RlcDogbnVtYmVyO1xyXG59XHJcblxyXG5leHBvcnQgY2xhc3MgRm9ybUZpZWxkSW5wdXROdW1iZXIgZXh0ZW5kcyBGb3JtRmllbGRJbnB1dCB7XHJcbiAgcHJpdmF0ZSBzdGVwOiBudW1iZXIgPSAxO1xyXG5cclxuICBjb25zdHJ1Y3RvcigpIHtcclxuICAgIHN1cGVyKCk7XHJcbiAgfVxyXG5cclxuICBnZXRTdGVwID0gKCk6IG51bWJlciA9PiB7XHJcbiAgICByZXR1cm4gdGhpcy5zdGVwO1xyXG4gIH07XHJcblxyXG4gIHNldFN0ZXAgPSAoc3RlcDogbnVtYmVyKTogdm9pZCA9PiB7XHJcbiAgICB0aGlzLnN0ZXAgPSBzdGVwO1xyXG4gIH07XHJcbn1cclxuIl19