import { __extends } from "tslib";
// model
import { FormFieldInput, } from "./form-field-input.model";
import { VALIDATOR_NAMES, } from "../form-row-item.model";
import { MAT_COLOR } from "../../../../../material.module";
var FormFieldInputFile = /** @class */ (function (_super) {
    __extends(FormFieldInputFile, _super);
    function FormFieldInputFile() {
        var _this = _super.call(this) || this;
        _this.dragAndDrop = false;
        _this.buttonAddText = "Add";
        _this.buttonDeleteText = "Remove";
        _this.buttonAddColor = MAT_COLOR.EMPTY;
        _this.buttonDeleteColor = MAT_COLOR.EMPTY;
        _this.borderColor = "#415fb4";
        _this.allowedFileTypes = [];
        _this.getAllowedFileTypes = function () {
            return _this.allowedFileTypes;
        };
        _this.setAllowedFileTypes = function (allowedFileTypes) {
            _this.allowedFileTypes = allowedFileTypes;
        };
        _this.isFileTypeAllowed = function (type) {
            for (var index in _this.allowedFileTypes) {
                var allowedType = _this.allowedFileTypes[index];
                if (type.includes(allowedType))
                    return true;
            }
            return false;
        };
        _this.getDragAndDrop = function () {
            return _this.dragAndDrop;
        };
        _this.setDragAndDrop = function (dragAndDrop) {
            _this.dragAndDrop = dragAndDrop;
        };
        _this.getButtonAddText = function () {
            return _this.buttonAddText;
        };
        _this.setButtonAddText = function (buttonAddText) {
            _this.buttonAddText = buttonAddText;
        };
        _this.getButtonDeleteText = function () {
            return _this.buttonDeleteText;
        };
        _this.setButtonDeleteText = function (buttonDeleteText) {
            _this.buttonDeleteText = buttonDeleteText;
        };
        _this.getButtonAddColor = function () {
            return _this.buttonAddColor;
        };
        _this.setButtonAddColor = function (buttonAddColor) {
            _this.buttonAddColor = buttonAddColor;
        };
        _this.getButtonDeleteColor = function () {
            return _this.buttonDeleteColor;
        };
        _this.setButtonDeleteColor = function (buttonDeleteColor) {
            _this.buttonDeleteColor = buttonDeleteColor;
        };
        _this.getBorderColor = function () {
            return _this.borderColor;
        };
        _this.setBorderColor = function (borderColor) {
            _this.borderColor = borderColor;
        };
        _this.addFile = function (file) {
            var data = _this.getDefaultValue();
            data.push(file);
            _this.setDefaultValue(data);
        };
        //   override
        _this.setValidators = function (validators) {
            for (var index in validators) {
                if (validators[index].name === VALIDATOR_NAMES.REQUIRED) {
                    _this.validators = [validators[index]];
                    break;
                }
            }
        };
        return _this;
    }
    return FormFieldInputFile;
}(FormFieldInput));
export { FormFieldInputFile };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1maWVsZC1pbnB1dC1maWxlLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXktYW5ndWxhci1mb3JtLyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9tb2RlbC9zdHJ1Y3R1cmFsL2NvbXBvc2l0ZS9mb3JtL2Zvcm0tZmllbGQtaW5wdXQvZm9ybS1maWVsZC1pbnB1dC1maWxlLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxRQUFRO0FBQ1IsT0FBTyxFQUNMLGNBQWMsR0FFZixNQUFNLDBCQUEwQixDQUFDO0FBQ2xDLE9BQU8sRUFFTCxlQUFlLEdBQ2hCLE1BQU0sd0JBQXdCLENBQUM7QUFDaEMsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBYTNEO0lBQXdDLHNDQUFjO0lBU3BEO1FBQUEsWUFDRSxpQkFBTyxTQUNSO1FBVk8saUJBQVcsR0FBWSxLQUFLLENBQUM7UUFDN0IsbUJBQWEsR0FBVyxLQUFLLENBQUM7UUFDOUIsc0JBQWdCLEdBQVcsUUFBUSxDQUFDO1FBQ3BDLG9CQUFjLEdBQWMsU0FBUyxDQUFDLEtBQUssQ0FBQztRQUM1Qyx1QkFBaUIsR0FBYyxTQUFTLENBQUMsS0FBSyxDQUFDO1FBQy9DLGlCQUFXLEdBQVcsU0FBUyxDQUFDO1FBQ2hDLHNCQUFnQixHQUFhLEVBQUUsQ0FBQztRQU14Qyx5QkFBbUIsR0FBRztZQUNwQixPQUFPLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQztRQUMvQixDQUFDLENBQUM7UUFFRix5QkFBbUIsR0FBRyxVQUFDLGdCQUEwQjtZQUMvQyxLQUFJLENBQUMsZ0JBQWdCLEdBQUcsZ0JBQWdCLENBQUM7UUFDM0MsQ0FBQyxDQUFDO1FBRUYsdUJBQWlCLEdBQUcsVUFBQyxJQUFZO1lBQy9CLEtBQUssSUFBSSxLQUFLLElBQUksS0FBSSxDQUFDLGdCQUFnQixFQUFFO2dCQUN2QyxJQUFJLFdBQVcsR0FBRyxLQUFJLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQy9DLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUM7b0JBQUUsT0FBTyxJQUFJLENBQUM7YUFDN0M7WUFFRCxPQUFPLEtBQUssQ0FBQztRQUNmLENBQUMsQ0FBQztRQUVGLG9CQUFjLEdBQUc7WUFDZixPQUFPLEtBQUksQ0FBQyxXQUFXLENBQUM7UUFDMUIsQ0FBQyxDQUFDO1FBRUYsb0JBQWMsR0FBRyxVQUFDLFdBQW9CO1lBQ3BDLEtBQUksQ0FBQyxXQUFXLEdBQUcsV0FBVyxDQUFDO1FBQ2pDLENBQUMsQ0FBQztRQUVGLHNCQUFnQixHQUFHO1lBQ2pCLE9BQU8sS0FBSSxDQUFDLGFBQWEsQ0FBQztRQUM1QixDQUFDLENBQUM7UUFFRixzQkFBZ0IsR0FBRyxVQUFDLGFBQXFCO1lBQ3ZDLEtBQUksQ0FBQyxhQUFhLEdBQUcsYUFBYSxDQUFDO1FBQ3JDLENBQUMsQ0FBQztRQUVGLHlCQUFtQixHQUFHO1lBQ3BCLE9BQU8sS0FBSSxDQUFDLGdCQUFnQixDQUFDO1FBQy9CLENBQUMsQ0FBQztRQUVGLHlCQUFtQixHQUFHLFVBQUMsZ0JBQXdCO1lBQzdDLEtBQUksQ0FBQyxnQkFBZ0IsR0FBRyxnQkFBZ0IsQ0FBQztRQUMzQyxDQUFDLENBQUM7UUFFRix1QkFBaUIsR0FBRztZQUNsQixPQUFPLEtBQUksQ0FBQyxjQUFjLENBQUM7UUFDN0IsQ0FBQyxDQUFDO1FBRUYsdUJBQWlCLEdBQUcsVUFBQyxjQUF5QjtZQUM1QyxLQUFJLENBQUMsY0FBYyxHQUFHLGNBQWMsQ0FBQztRQUN2QyxDQUFDLENBQUM7UUFFRiwwQkFBb0IsR0FBRztZQUNyQixPQUFPLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQztRQUNoQyxDQUFDLENBQUM7UUFFRiwwQkFBb0IsR0FBRyxVQUFDLGlCQUE0QjtZQUNsRCxLQUFJLENBQUMsaUJBQWlCLEdBQUcsaUJBQWlCLENBQUM7UUFDN0MsQ0FBQyxDQUFDO1FBRUYsb0JBQWMsR0FBRztZQUNmLE9BQU8sS0FBSSxDQUFDLFdBQVcsQ0FBQztRQUMxQixDQUFDLENBQUM7UUFFRixvQkFBYyxHQUFHLFVBQUMsV0FBbUI7WUFDbkMsS0FBSSxDQUFDLFdBQVcsR0FBRyxXQUFXLENBQUM7UUFDakMsQ0FBQyxDQUFDO1FBRUYsYUFBTyxHQUFHLFVBQUMsSUFBVTtZQUNuQixJQUFJLElBQUksR0FBRyxLQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7WUFDbEMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNoQixLQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzdCLENBQUMsQ0FBQztRQUVGLGFBQWE7UUFDYixtQkFBYSxHQUFHLFVBQUMsVUFBMkM7WUFDMUQsS0FBSyxJQUFJLEtBQUssSUFBSSxVQUFVLEVBQUU7Z0JBQzVCLElBQUksVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksS0FBSyxlQUFlLENBQUMsUUFBUSxFQUFFO29CQUN2RCxLQUFJLENBQUMsVUFBVSxHQUFHLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7b0JBQ3RDLE1BQU07aUJBQ1A7YUFDRjtRQUNILENBQUMsQ0FBQzs7SUFqRkYsQ0FBQztJQWtGSCx5QkFBQztBQUFELENBQUMsQUE3RkQsQ0FBd0MsY0FBYyxHQTZGckQiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBtb2RlbFxyXG5pbXBvcnQge1xyXG4gIEZvcm1GaWVsZElucHV0LFxyXG4gIEZvcm1GaWVsZElucHV0SW50ZXJmYWNlLFxyXG59IGZyb20gXCIuL2Zvcm0tZmllbGQtaW5wdXQubW9kZWxcIjtcclxuaW1wb3J0IHtcclxuICBGb3JtUm93SXRlbVZhbGlkYXRvckludGVyZmFjZSxcclxuICBWQUxJREFUT1JfTkFNRVMsXHJcbn0gZnJvbSBcIi4uL2Zvcm0tcm93LWl0ZW0ubW9kZWxcIjtcclxuaW1wb3J0IHsgTUFUX0NPTE9SIH0gZnJvbSBcIi4uLy4uLy4uLy4uLy4uL21hdGVyaWFsLm1vZHVsZVwiO1xyXG5cclxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIEZvcm1GaWVsZElucHV0RmlsZUludGVyZmFjZVxyXG4gIGV4dGVuZHMgRm9ybUZpZWxkSW5wdXRJbnRlcmZhY2Uge1xyXG4gIGFsbG93ZWRGaWxlVHlwZXM6IHN0cmluZ1tdO1xyXG4gIGRyYWdBbmREcm9wPzogYm9vbGVhbjtcclxuICBidXR0b25BZGRUZXh0Pzogc3RyaW5nO1xyXG4gIGJ1dHRvbkRlbGV0ZVRleHQ/OiBzdHJpbmc7XHJcbiAgYnV0dG9uQWRkQ29sb3I/OiBNQVRfQ09MT1I7XHJcbiAgYnV0dG9uRGVsZXRlQ29sb3I/OiBNQVRfQ09MT1I7XHJcbiAgYm9yZGVyQ29sb3I/OiBzdHJpbmc7XHJcbn1cclxuXHJcbmV4cG9ydCBjbGFzcyBGb3JtRmllbGRJbnB1dEZpbGUgZXh0ZW5kcyBGb3JtRmllbGRJbnB1dCB7XHJcbiAgcHJpdmF0ZSBkcmFnQW5kRHJvcDogYm9vbGVhbiA9IGZhbHNlO1xyXG4gIHByaXZhdGUgYnV0dG9uQWRkVGV4dDogc3RyaW5nID0gXCJBZGRcIjtcclxuICBwcml2YXRlIGJ1dHRvbkRlbGV0ZVRleHQ6IHN0cmluZyA9IFwiUmVtb3ZlXCI7XHJcbiAgcHJpdmF0ZSBidXR0b25BZGRDb2xvcjogTUFUX0NPTE9SID0gTUFUX0NPTE9SLkVNUFRZO1xyXG4gIHByaXZhdGUgYnV0dG9uRGVsZXRlQ29sb3I6IE1BVF9DT0xPUiA9IE1BVF9DT0xPUi5FTVBUWTtcclxuICBwcml2YXRlIGJvcmRlckNvbG9yOiBzdHJpbmcgPSBcIiM0MTVmYjRcIjtcclxuICBwcml2YXRlIGFsbG93ZWRGaWxlVHlwZXM6IHN0cmluZ1tdID0gW107XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgc3VwZXIoKTtcclxuICB9XHJcblxyXG4gIGdldEFsbG93ZWRGaWxlVHlwZXMgPSAoKTogc3RyaW5nW10gPT4ge1xyXG4gICAgcmV0dXJuIHRoaXMuYWxsb3dlZEZpbGVUeXBlcztcclxuICB9O1xyXG5cclxuICBzZXRBbGxvd2VkRmlsZVR5cGVzID0gKGFsbG93ZWRGaWxlVHlwZXM6IHN0cmluZ1tdKTogdm9pZCA9PiB7XHJcbiAgICB0aGlzLmFsbG93ZWRGaWxlVHlwZXMgPSBhbGxvd2VkRmlsZVR5cGVzO1xyXG4gIH07XHJcblxyXG4gIGlzRmlsZVR5cGVBbGxvd2VkID0gKHR5cGU6IHN0cmluZyk6IGJvb2xlYW4gPT4ge1xyXG4gICAgZm9yIChsZXQgaW5kZXggaW4gdGhpcy5hbGxvd2VkRmlsZVR5cGVzKSB7XHJcbiAgICAgIGxldCBhbGxvd2VkVHlwZSA9IHRoaXMuYWxsb3dlZEZpbGVUeXBlc1tpbmRleF07XHJcbiAgICAgIGlmICh0eXBlLmluY2x1ZGVzKGFsbG93ZWRUeXBlKSkgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIGZhbHNlO1xyXG4gIH07XHJcblxyXG4gIGdldERyYWdBbmREcm9wID0gKCk6IGJvb2xlYW4gPT4ge1xyXG4gICAgcmV0dXJuIHRoaXMuZHJhZ0FuZERyb3A7XHJcbiAgfTtcclxuXHJcbiAgc2V0RHJhZ0FuZERyb3AgPSAoZHJhZ0FuZERyb3A6IGJvb2xlYW4pOiB2b2lkID0+IHtcclxuICAgIHRoaXMuZHJhZ0FuZERyb3AgPSBkcmFnQW5kRHJvcDtcclxuICB9O1xyXG5cclxuICBnZXRCdXR0b25BZGRUZXh0ID0gKCk6IHN0cmluZyA9PiB7XHJcbiAgICByZXR1cm4gdGhpcy5idXR0b25BZGRUZXh0O1xyXG4gIH07XHJcblxyXG4gIHNldEJ1dHRvbkFkZFRleHQgPSAoYnV0dG9uQWRkVGV4dDogc3RyaW5nKTogdm9pZCA9PiB7XHJcbiAgICB0aGlzLmJ1dHRvbkFkZFRleHQgPSBidXR0b25BZGRUZXh0O1xyXG4gIH07XHJcblxyXG4gIGdldEJ1dHRvbkRlbGV0ZVRleHQgPSAoKTogc3RyaW5nID0+IHtcclxuICAgIHJldHVybiB0aGlzLmJ1dHRvbkRlbGV0ZVRleHQ7XHJcbiAgfTtcclxuXHJcbiAgc2V0QnV0dG9uRGVsZXRlVGV4dCA9IChidXR0b25EZWxldGVUZXh0OiBzdHJpbmcpOiB2b2lkID0+IHtcclxuICAgIHRoaXMuYnV0dG9uRGVsZXRlVGV4dCA9IGJ1dHRvbkRlbGV0ZVRleHQ7XHJcbiAgfTtcclxuXHJcbiAgZ2V0QnV0dG9uQWRkQ29sb3IgPSAoKTogTUFUX0NPTE9SID0+IHtcclxuICAgIHJldHVybiB0aGlzLmJ1dHRvbkFkZENvbG9yO1xyXG4gIH07XHJcblxyXG4gIHNldEJ1dHRvbkFkZENvbG9yID0gKGJ1dHRvbkFkZENvbG9yOiBNQVRfQ09MT1IpOiB2b2lkID0+IHtcclxuICAgIHRoaXMuYnV0dG9uQWRkQ29sb3IgPSBidXR0b25BZGRDb2xvcjtcclxuICB9O1xyXG5cclxuICBnZXRCdXR0b25EZWxldGVDb2xvciA9ICgpOiBNQVRfQ09MT1IgPT4ge1xyXG4gICAgcmV0dXJuIHRoaXMuYnV0dG9uRGVsZXRlQ29sb3I7XHJcbiAgfTtcclxuXHJcbiAgc2V0QnV0dG9uRGVsZXRlQ29sb3IgPSAoYnV0dG9uRGVsZXRlQ29sb3I6IE1BVF9DT0xPUik6IHZvaWQgPT4ge1xyXG4gICAgdGhpcy5idXR0b25EZWxldGVDb2xvciA9IGJ1dHRvbkRlbGV0ZUNvbG9yO1xyXG4gIH07XHJcblxyXG4gIGdldEJvcmRlckNvbG9yID0gKCk6IHN0cmluZyA9PiB7XHJcbiAgICByZXR1cm4gdGhpcy5ib3JkZXJDb2xvcjtcclxuICB9O1xyXG5cclxuICBzZXRCb3JkZXJDb2xvciA9IChib3JkZXJDb2xvcjogc3RyaW5nKTogdm9pZCA9PiB7XHJcbiAgICB0aGlzLmJvcmRlckNvbG9yID0gYm9yZGVyQ29sb3I7XHJcbiAgfTtcclxuXHJcbiAgYWRkRmlsZSA9IChmaWxlOiBGaWxlKTogdm9pZCA9PiB7XHJcbiAgICBsZXQgZGF0YSA9IHRoaXMuZ2V0RGVmYXVsdFZhbHVlKCk7XHJcbiAgICBkYXRhLnB1c2goZmlsZSk7XHJcbiAgICB0aGlzLnNldERlZmF1bHRWYWx1ZShkYXRhKTtcclxuICB9O1xyXG5cclxuICAvLyAgIG92ZXJyaWRlXHJcbiAgc2V0VmFsaWRhdG9ycyA9ICh2YWxpZGF0b3JzOiBGb3JtUm93SXRlbVZhbGlkYXRvckludGVyZmFjZVtdKTogdm9pZCA9PiB7XHJcbiAgICBmb3IgKGxldCBpbmRleCBpbiB2YWxpZGF0b3JzKSB7XHJcbiAgICAgIGlmICh2YWxpZGF0b3JzW2luZGV4XS5uYW1lID09PSBWQUxJREFUT1JfTkFNRVMuUkVRVUlSRUQpIHtcclxuICAgICAgICB0aGlzLnZhbGlkYXRvcnMgPSBbdmFsaWRhdG9yc1tpbmRleF1dO1xyXG4gICAgICAgIGJyZWFrO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfTtcclxufVxyXG4iXX0=