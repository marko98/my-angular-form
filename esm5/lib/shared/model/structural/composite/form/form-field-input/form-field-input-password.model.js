import { __extends } from "tslib";
// model
import { FormFieldInput, } from './form-field-input.model';
var FormFieldInputPassword = /** @class */ (function (_super) {
    __extends(FormFieldInputPassword, _super);
    function FormFieldInputPassword() {
        var _this = _super.call(this) || this;
        _this.showPassword = false;
        _this.showHintAboutMinMaxLength = false;
        _this.onShowPassword = function () {
            _this.showPassword = true;
            setTimeout(function () {
                _this.showPassword = false;
            }, _this.showPasswordInMs);
        };
        _this.getShowPassword = function () {
            return _this.showPassword;
        };
        _this.getShowPasswordInMs = function () {
            return _this.showPasswordInMs;
        };
        _this.setShowPasswordInMs = function (showPasswordInMs) {
            _this.showPasswordInMs = showPasswordInMs;
        };
        _this.getShowHintAboutMinMaxLength = function () {
            return _this.showHintAboutMinMaxLength;
        };
        _this.setShowHintAboutMinMaxLength = function (showHintAboutMinMaxLength) {
            _this.showHintAboutMinMaxLength = showHintAboutMinMaxLength;
        };
        return _this;
    }
    return FormFieldInputPassword;
}(FormFieldInput));
export { FormFieldInputPassword };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1maWVsZC1pbnB1dC1wYXNzd29yZC5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215LWFuZ3VsYXItZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvbW9kZWwvc3RydWN0dXJhbC9jb21wb3NpdGUvZm9ybS9mb3JtLWZpZWxkLWlucHV0L2Zvcm0tZmllbGQtaW5wdXQtcGFzc3dvcmQubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLFFBQVE7QUFDUixPQUFPLEVBQ0wsY0FBYyxHQUVmLE1BQU0sMEJBQTBCLENBQUM7QUFTbEM7SUFBNEMsMENBQWM7SUFNeEQ7UUFBQSxZQUNFLGlCQUFPLFNBQ1I7UUFMTyxrQkFBWSxHQUFZLEtBQUssQ0FBQztRQUM5QiwrQkFBeUIsR0FBWSxLQUFLLENBQUM7UUFNbkQsb0JBQWMsR0FBRztZQUNmLEtBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO1lBRXpCLFVBQVUsQ0FBQztnQkFDVCxLQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztZQUM1QixDQUFDLEVBQUUsS0FBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDNUIsQ0FBQyxDQUFDO1FBRUYscUJBQWUsR0FBRztZQUNoQixPQUFPLEtBQUksQ0FBQyxZQUFZLENBQUM7UUFDM0IsQ0FBQyxDQUFDO1FBRUYseUJBQW1CLEdBQUc7WUFDcEIsT0FBTyxLQUFJLENBQUMsZ0JBQWdCLENBQUM7UUFDL0IsQ0FBQyxDQUFDO1FBRUYseUJBQW1CLEdBQUcsVUFBQyxnQkFBd0I7WUFDN0MsS0FBSSxDQUFDLGdCQUFnQixHQUFHLGdCQUFnQixDQUFDO1FBQzNDLENBQUMsQ0FBQztRQUVGLGtDQUE0QixHQUFHO1lBQzdCLE9BQU8sS0FBSSxDQUFDLHlCQUF5QixDQUFDO1FBQ3hDLENBQUMsQ0FBQztRQUVGLGtDQUE0QixHQUFHLFVBQUMseUJBQWtDO1lBQ2hFLEtBQUksQ0FBQyx5QkFBeUIsR0FBRyx5QkFBeUIsQ0FBQztRQUM3RCxDQUFDLENBQUM7O0lBNUJGLENBQUM7SUE2QkgsNkJBQUM7QUFBRCxDQUFDLEFBckNELENBQTRDLGNBQWMsR0FxQ3pEIiwic291cmNlc0NvbnRlbnQiOlsiLy8gbW9kZWxcclxuaW1wb3J0IHtcclxuICBGb3JtRmllbGRJbnB1dCxcclxuICBGb3JtRmllbGRJbnB1dEludGVyZmFjZSxcclxufSBmcm9tICcuL2Zvcm0tZmllbGQtaW5wdXQubW9kZWwnO1xyXG5pbXBvcnQgeyBGb3JtRmllbGRJbnB1dFRleHRQYXNzd29yZEludGVyZmFjZSB9IGZyb20gJy4vZm9ybS1maWVsZC1pbnB1dC10ZXh0LXBhc3N3b3JkLmludGVyZmFjZSc7XHJcblxyXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgRm9ybUZpZWxkSW5wdXRQYXNzd29yZEludGVyZmFjZVxyXG4gIGV4dGVuZHMgRm9ybUZpZWxkSW5wdXRJbnRlcmZhY2Uge1xyXG4gIHNob3dQYXNzd29yZEluTXM/OiBudW1iZXI7XHJcbiAgc2hvd0hpbnRBYm91dE1pbk1heExlbmd0aD86IGJvb2xlYW47XHJcbn1cclxuXHJcbmV4cG9ydCBjbGFzcyBGb3JtRmllbGRJbnB1dFBhc3N3b3JkIGV4dGVuZHMgRm9ybUZpZWxkSW5wdXRcclxuICBpbXBsZW1lbnRzIEZvcm1GaWVsZElucHV0VGV4dFBhc3N3b3JkSW50ZXJmYWNlIHtcclxuICBwcml2YXRlIHNob3dQYXNzd29yZEluTXM/OiBudW1iZXI7XHJcbiAgcHJpdmF0ZSBzaG93UGFzc3dvcmQ6IGJvb2xlYW4gPSBmYWxzZTtcclxuICBwcml2YXRlIHNob3dIaW50QWJvdXRNaW5NYXhMZW5ndGg6IGJvb2xlYW4gPSBmYWxzZTtcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7XHJcbiAgICBzdXBlcigpO1xyXG4gIH1cclxuXHJcbiAgb25TaG93UGFzc3dvcmQgPSAoKTogdm9pZCA9PiB7XHJcbiAgICB0aGlzLnNob3dQYXNzd29yZCA9IHRydWU7XHJcblxyXG4gICAgc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgIHRoaXMuc2hvd1Bhc3N3b3JkID0gZmFsc2U7XHJcbiAgICB9LCB0aGlzLnNob3dQYXNzd29yZEluTXMpO1xyXG4gIH07XHJcblxyXG4gIGdldFNob3dQYXNzd29yZCA9ICgpOiBib29sZWFuID0+IHtcclxuICAgIHJldHVybiB0aGlzLnNob3dQYXNzd29yZDtcclxuICB9O1xyXG5cclxuICBnZXRTaG93UGFzc3dvcmRJbk1zID0gKCk6IG51bWJlciA9PiB7XHJcbiAgICByZXR1cm4gdGhpcy5zaG93UGFzc3dvcmRJbk1zO1xyXG4gIH07XHJcblxyXG4gIHNldFNob3dQYXNzd29yZEluTXMgPSAoc2hvd1Bhc3N3b3JkSW5NczogbnVtYmVyKTogdm9pZCA9PiB7XHJcbiAgICB0aGlzLnNob3dQYXNzd29yZEluTXMgPSBzaG93UGFzc3dvcmRJbk1zO1xyXG4gIH07XHJcblxyXG4gIGdldFNob3dIaW50QWJvdXRNaW5NYXhMZW5ndGggPSAoKTogYm9vbGVhbiA9PiB7XHJcbiAgICByZXR1cm4gdGhpcy5zaG93SGludEFib3V0TWluTWF4TGVuZ3RoO1xyXG4gIH07XHJcblxyXG4gIHNldFNob3dIaW50QWJvdXRNaW5NYXhMZW5ndGggPSAoc2hvd0hpbnRBYm91dE1pbk1heExlbmd0aDogYm9vbGVhbik6IHZvaWQgPT4ge1xyXG4gICAgdGhpcy5zaG93SGludEFib3V0TWluTWF4TGVuZ3RoID0gc2hvd0hpbnRBYm91dE1pbk1heExlbmd0aDtcclxuICB9O1xyXG59XHJcbiJdfQ==