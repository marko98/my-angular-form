import { __extends } from "tslib";
// model
import { FormRowItem } from '../form-row-item.model';
export var FORM_FIELD_INPUT_TYPE;
(function (FORM_FIELD_INPUT_TYPE) {
    // FORM_FIELD_INPUT, // text, number(step, min, max), file, email, password // dodati opciju za validacije
    FORM_FIELD_INPUT_TYPE["TEXT"] = "text";
    FORM_FIELD_INPUT_TYPE["NUMBER"] = "number";
    FORM_FIELD_INPUT_TYPE["FILE"] = "file";
    FORM_FIELD_INPUT_TYPE["EMAIL"] = "email";
    FORM_FIELD_INPUT_TYPE["PASSWORD"] = "password";
})(FORM_FIELD_INPUT_TYPE || (FORM_FIELD_INPUT_TYPE = {}));
var FormFieldInput = /** @class */ (function (_super) {
    __extends(FormFieldInput, _super);
    function FormFieldInput() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.appearance = 'legacy';
        _this.readonly = false;
        _this.required = {
            required: false,
            hideRequiredMarker: false,
        };
        _this.getFormFieldInputType = function () {
            return _this.formFieldInputType;
        };
        _this.setFormFieldInputType = function (formFieldInputType) {
            _this.formFieldInputType = formFieldInputType;
        };
        _this.getAppearance = function () {
            return _this.appearance;
        };
        _this.setAppearance = function (appearance) {
            _this.appearance = appearance;
        };
        _this.getPlaceholder = function () {
            return _this.placeholder;
        };
        _this.setPlaceholder = function (placeholder) {
            _this.placeholder = placeholder;
        };
        _this.getReadonly = function () {
            return _this.readonly;
        };
        _this.setReadonly = function (readonly) {
            _this.readonly = readonly;
        };
        _this.getRequired = function () {
            return _this.required;
        };
        _this.setRequired = function (required) {
            _this.required = required;
        };
        _this.getLabelName = function () {
            return _this.labelName;
        };
        _this.setLabelName = function (labelName) {
            _this.labelName = labelName;
        };
        _this.getLeftHintLabel = function () {
            return _this.leftHintLabel;
        };
        _this.setLeftHintLabel = function (leftHintLabel) {
            _this.leftHintLabel = leftHintLabel;
        };
        _this.getRightHintLabel = function () {
            return _this.rightHintLabel;
        };
        _this.setRightHintLabel = function (rightHintLabel) {
            _this.rightHintLabel = rightHintLabel;
        };
        _this.getMatPrefixImgText = function () {
            return _this.matPrefixImgText;
        };
        _this.setMatPrefixImgText = function (matPrefixImgText) {
            _this.matPrefixImgText = matPrefixImgText;
        };
        _this.getMatSuffixImgText = function () {
            return _this.matSuffixImgText;
        };
        _this.setMatSuffixImgText = function (matSuffixImgText) {
            _this.matSuffixImgText = matSuffixImgText;
        };
        _this.getTextSuffix = function () {
            return _this.textSuffix;
        };
        _this.setTextSuffix = function (textSuffix) {
            _this.textSuffix = textSuffix;
        };
        _this.getTextPrefix = function () {
            return _this.textPrefix;
        };
        _this.setTextPrefix = function (textPrefix) {
            _this.textPrefix = textPrefix;
        };
        return _this;
    }
    return FormFieldInput;
}(FormRowItem));
export { FormFieldInput };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1maWVsZC1pbnB1dC5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215LWFuZ3VsYXItZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvbW9kZWwvc3RydWN0dXJhbC9jb21wb3NpdGUvZm9ybS9mb3JtLWZpZWxkLWlucHV0L2Zvcm0tZmllbGQtaW5wdXQubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLFFBQVE7QUFDUixPQUFPLEVBQUUsV0FBVyxFQUF3QixNQUFNLHdCQUF3QixDQUFDO0FBK0IzRSxNQUFNLENBQU4sSUFBWSxxQkFPWDtBQVBELFdBQVkscUJBQXFCO0lBQy9CLDBHQUEwRztJQUMxRyxzQ0FBYSxDQUFBO0lBQ2IsMENBQWlCLENBQUE7SUFDakIsc0NBQWEsQ0FBQTtJQUNiLHdDQUFlLENBQUE7SUFDZiw4Q0FBcUIsQ0FBQTtBQUN2QixDQUFDLEVBUFcscUJBQXFCLEtBQXJCLHFCQUFxQixRQU9oQztBQUVEO0lBQW9DLGtDQUFXO0lBQS9DO1FBQUEscUVBa0hDO1FBaEhTLGdCQUFVLEdBQStDLFFBQVEsQ0FBQztRQUVsRSxjQUFRLEdBQVksS0FBSyxDQUFDO1FBQzFCLGNBQVEsR0FBb0M7WUFDbEQsUUFBUSxFQUFFLEtBQUs7WUFDZixrQkFBa0IsRUFBRSxLQUFLO1NBQzFCLENBQUM7UUFTRiwyQkFBcUIsR0FBRztZQUN0QixPQUFPLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQztRQUNqQyxDQUFDLENBQUM7UUFFRiwyQkFBcUIsR0FBRyxVQUFDLGtCQUF5QztZQUNoRSxLQUFJLENBQUMsa0JBQWtCLEdBQUcsa0JBQWtCLENBQUM7UUFDL0MsQ0FBQyxDQUFDO1FBRUYsbUJBQWEsR0FBRztZQUNkLE9BQU8sS0FBSSxDQUFDLFVBQVUsQ0FBQztRQUN6QixDQUFDLENBQUM7UUFFRixtQkFBYSxHQUFHLFVBQ2QsVUFBc0Q7WUFFdEQsS0FBSSxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUM7UUFDL0IsQ0FBQyxDQUFDO1FBRUYsb0JBQWMsR0FBRztZQUNmLE9BQU8sS0FBSSxDQUFDLFdBQVcsQ0FBQztRQUMxQixDQUFDLENBQUM7UUFFRixvQkFBYyxHQUFHLFVBQUMsV0FBbUI7WUFDbkMsS0FBSSxDQUFDLFdBQVcsR0FBRyxXQUFXLENBQUM7UUFDakMsQ0FBQyxDQUFDO1FBRUYsaUJBQVcsR0FBRztZQUNaLE9BQU8sS0FBSSxDQUFDLFFBQVEsQ0FBQztRQUN2QixDQUFDLENBQUM7UUFFRixpQkFBVyxHQUFHLFVBQUMsUUFBaUI7WUFDOUIsS0FBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7UUFDM0IsQ0FBQyxDQUFDO1FBRUYsaUJBQVcsR0FBRztZQUNaLE9BQU8sS0FBSSxDQUFDLFFBQVEsQ0FBQztRQUN2QixDQUFDLENBQUM7UUFFRixpQkFBVyxHQUFHLFVBQUMsUUFBeUM7WUFDdEQsS0FBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7UUFDM0IsQ0FBQyxDQUFDO1FBRUYsa0JBQVksR0FBRztZQUNiLE9BQU8sS0FBSSxDQUFDLFNBQVMsQ0FBQztRQUN4QixDQUFDLENBQUM7UUFFRixrQkFBWSxHQUFHLFVBQUMsU0FBaUI7WUFDL0IsS0FBSSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUM7UUFDN0IsQ0FBQyxDQUFDO1FBRUYsc0JBQWdCLEdBQUc7WUFDakIsT0FBTyxLQUFJLENBQUMsYUFBYSxDQUFDO1FBQzVCLENBQUMsQ0FBQztRQUVGLHNCQUFnQixHQUFHLFVBQUMsYUFBcUI7WUFDdkMsS0FBSSxDQUFDLGFBQWEsR0FBRyxhQUFhLENBQUM7UUFDckMsQ0FBQyxDQUFDO1FBRUYsdUJBQWlCLEdBQUc7WUFDbEIsT0FBTyxLQUFJLENBQUMsY0FBYyxDQUFDO1FBQzdCLENBQUMsQ0FBQztRQUVGLHVCQUFpQixHQUFHLFVBQUMsY0FBc0I7WUFDekMsS0FBSSxDQUFDLGNBQWMsR0FBRyxjQUFjLENBQUM7UUFDdkMsQ0FBQyxDQUFDO1FBRUYseUJBQW1CLEdBQUc7WUFDcEIsT0FBTyxLQUFJLENBQUMsZ0JBQWdCLENBQUM7UUFDL0IsQ0FBQyxDQUFDO1FBRUYseUJBQW1CLEdBQUcsVUFBQyxnQkFBd0I7WUFDN0MsS0FBSSxDQUFDLGdCQUFnQixHQUFHLGdCQUFnQixDQUFDO1FBQzNDLENBQUMsQ0FBQztRQUVGLHlCQUFtQixHQUFHO1lBQ3BCLE9BQU8sS0FBSSxDQUFDLGdCQUFnQixDQUFDO1FBQy9CLENBQUMsQ0FBQztRQUVGLHlCQUFtQixHQUFHLFVBQUMsZ0JBQXdCO1lBQzdDLEtBQUksQ0FBQyxnQkFBZ0IsR0FBRyxnQkFBZ0IsQ0FBQztRQUMzQyxDQUFDLENBQUM7UUFFRixtQkFBYSxHQUFHO1lBQ2QsT0FBTyxLQUFJLENBQUMsVUFBVSxDQUFDO1FBQ3pCLENBQUMsQ0FBQztRQUVGLG1CQUFhLEdBQUcsVUFBQyxVQUFrQjtZQUNqQyxLQUFJLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQztRQUMvQixDQUFDLENBQUM7UUFFRixtQkFBYSxHQUFHO1lBQ2QsT0FBTyxLQUFJLENBQUMsVUFBVSxDQUFDO1FBQ3pCLENBQUMsQ0FBQztRQUVGLG1CQUFhLEdBQUcsVUFBQyxVQUFrQjtZQUNqQyxLQUFJLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQztRQUMvQixDQUFDLENBQUM7O0lBQ0osQ0FBQztJQUFELHFCQUFDO0FBQUQsQ0FBQyxBQWxIRCxDQUFvQyxXQUFXLEdBa0g5QyIsInNvdXJjZXNDb250ZW50IjpbIi8vIG1vZGVsXHJcbmltcG9ydCB7IEZvcm1Sb3dJdGVtLCBGb3JtUm93SXRlbUludGVyZmFjZSB9IGZyb20gJy4uL2Zvcm0tcm93LWl0ZW0ubW9kZWwnO1xyXG5cclxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIEZvcm1GaWVsZElucHV0SW50ZXJmYWNlIGV4dGVuZHMgRm9ybVJvd0l0ZW1JbnRlcmZhY2Uge1xyXG4gIGZvcm1GaWVsZElucHV0VHlwZTogRk9STV9GSUVMRF9JTlBVVF9UWVBFO1xyXG4gIGxhYmVsTmFtZTogc3RyaW5nO1xyXG4gIGFwcGVhcmFuY2U/OiAnbGVnYWN5JyB8ICdzdGFuZGFyZCcgfCAnZmlsbCcgfCAnb3V0bGluZSc7XHJcbiAgZGVmYXVsdFZhbHVlPzogYW55O1xyXG4gIHBsYWNlaG9sZGVyPzogc3RyaW5nO1xyXG4gIHJlYWRvbmx5PzogYm9vbGVhbjtcclxuICByZXF1aXJlZE9wdGlvbj86IEZvcm1GaWVsZElucHV0UmVxdWlyZWRJbnRlcmZhY2U7XHJcbiAgaGludExhYmVscz86IEZvcm1GaWVsZElucHV0SGludExhYmVsc0ludGVyZmFjZTtcclxuICBtYXRJbWFnZXM/OiBGb3JtRmllbGRJbnB1dE1hdEltYWdlc0ludGVyZmFjZTtcclxuICB0ZXh0U3VmZml4Pzogc3RyaW5nO1xyXG4gIHRleHRQcmVmaXg/OiBzdHJpbmc7XHJcbn1cclxuXHJcbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBGb3JtRmllbGRJbnB1dE1hdEltYWdlc0ludGVyZmFjZSB7XHJcbiAgbWF0UHJlZml4SW1nVGV4dD86IHN0cmluZztcclxuICBtYXRTdWZmaXhJbWdUZXh0Pzogc3RyaW5nO1xyXG59XHJcblxyXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgRm9ybUZpZWxkSW5wdXRSZXF1aXJlZEludGVyZmFjZSB7XHJcbiAgcmVxdWlyZWQ/OiBib29sZWFuO1xyXG4gIGhpZGVSZXF1aXJlZE1hcmtlcj86IGJvb2xlYW47XHJcbn1cclxuXHJcbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBGb3JtRmllbGRJbnB1dEhpbnRMYWJlbHNJbnRlcmZhY2Uge1xyXG4gIGxlZnRIaW50TGFiZWxDb250ZXh0Pzogc3RyaW5nO1xyXG4gIHJpZ2h0SGludExhYmVsQ29udGV4dD86IHN0cmluZztcclxufVxyXG5cclxuZXhwb3J0IGVudW0gRk9STV9GSUVMRF9JTlBVVF9UWVBFIHtcclxuICAvLyBGT1JNX0ZJRUxEX0lOUFVULCAvLyB0ZXh0LCBudW1iZXIoc3RlcCwgbWluLCBtYXgpLCBmaWxlLCBlbWFpbCwgcGFzc3dvcmQgLy8gZG9kYXRpIG9wY2lqdSB6YSB2YWxpZGFjaWplXHJcbiAgVEVYVCA9ICd0ZXh0JyxcclxuICBOVU1CRVIgPSAnbnVtYmVyJyxcclxuICBGSUxFID0gJ2ZpbGUnLFxyXG4gIEVNQUlMID0gJ2VtYWlsJyxcclxuICBQQVNTV09SRCA9ICdwYXNzd29yZCcsXHJcbn1cclxuXHJcbmV4cG9ydCBjbGFzcyBGb3JtRmllbGRJbnB1dCBleHRlbmRzIEZvcm1Sb3dJdGVtIHtcclxuICBwcml2YXRlIGZvcm1GaWVsZElucHV0VHlwZTogRk9STV9GSUVMRF9JTlBVVF9UWVBFO1xyXG4gIHByaXZhdGUgYXBwZWFyYW5jZTogJ2xlZ2FjeScgfCAnc3RhbmRhcmQnIHwgJ2ZpbGwnIHwgJ291dGxpbmUnID0gJ2xlZ2FjeSc7XHJcbiAgcHJpdmF0ZSBwbGFjZWhvbGRlcj86IHN0cmluZztcclxuICBwcml2YXRlIHJlYWRvbmx5OiBib29sZWFuID0gZmFsc2U7XHJcbiAgcHJpdmF0ZSByZXF1aXJlZDogRm9ybUZpZWxkSW5wdXRSZXF1aXJlZEludGVyZmFjZSA9IHtcclxuICAgIHJlcXVpcmVkOiBmYWxzZSxcclxuICAgIGhpZGVSZXF1aXJlZE1hcmtlcjogZmFsc2UsXHJcbiAgfTtcclxuICBwcml2YXRlIGxhYmVsTmFtZTogc3RyaW5nO1xyXG4gIHByaXZhdGUgbGVmdEhpbnRMYWJlbD86IHN0cmluZztcclxuICBwcml2YXRlIHJpZ2h0SGludExhYmVsPzogc3RyaW5nO1xyXG4gIHByaXZhdGUgbWF0UHJlZml4SW1nVGV4dD86IHN0cmluZztcclxuICBwcml2YXRlIG1hdFN1ZmZpeEltZ1RleHQ/OiBzdHJpbmc7XHJcbiAgcHJpdmF0ZSB0ZXh0U3VmZml4Pzogc3RyaW5nO1xyXG4gIHByaXZhdGUgdGV4dFByZWZpeD86IHN0cmluZztcclxuXHJcbiAgZ2V0Rm9ybUZpZWxkSW5wdXRUeXBlID0gKCk6IEZPUk1fRklFTERfSU5QVVRfVFlQRSA9PiB7XHJcbiAgICByZXR1cm4gdGhpcy5mb3JtRmllbGRJbnB1dFR5cGU7XHJcbiAgfTtcclxuXHJcbiAgc2V0Rm9ybUZpZWxkSW5wdXRUeXBlID0gKGZvcm1GaWVsZElucHV0VHlwZTogRk9STV9GSUVMRF9JTlBVVF9UWVBFKTogdm9pZCA9PiB7XHJcbiAgICB0aGlzLmZvcm1GaWVsZElucHV0VHlwZSA9IGZvcm1GaWVsZElucHV0VHlwZTtcclxuICB9O1xyXG5cclxuICBnZXRBcHBlYXJhbmNlID0gKCk6ICdsZWdhY3knIHwgJ3N0YW5kYXJkJyB8ICdmaWxsJyB8ICdvdXRsaW5lJyA9PiB7XHJcbiAgICByZXR1cm4gdGhpcy5hcHBlYXJhbmNlO1xyXG4gIH07XHJcblxyXG4gIHNldEFwcGVhcmFuY2UgPSAoXHJcbiAgICBhcHBlYXJhbmNlOiAnbGVnYWN5JyB8ICdzdGFuZGFyZCcgfCAnZmlsbCcgfCAnb3V0bGluZSdcclxuICApOiB2b2lkID0+IHtcclxuICAgIHRoaXMuYXBwZWFyYW5jZSA9IGFwcGVhcmFuY2U7XHJcbiAgfTtcclxuXHJcbiAgZ2V0UGxhY2Vob2xkZXIgPSAoKTogc3RyaW5nID0+IHtcclxuICAgIHJldHVybiB0aGlzLnBsYWNlaG9sZGVyO1xyXG4gIH07XHJcblxyXG4gIHNldFBsYWNlaG9sZGVyID0gKHBsYWNlaG9sZGVyOiBzdHJpbmcpOiB2b2lkID0+IHtcclxuICAgIHRoaXMucGxhY2Vob2xkZXIgPSBwbGFjZWhvbGRlcjtcclxuICB9O1xyXG5cclxuICBnZXRSZWFkb25seSA9ICgpOiBib29sZWFuID0+IHtcclxuICAgIHJldHVybiB0aGlzLnJlYWRvbmx5O1xyXG4gIH07XHJcblxyXG4gIHNldFJlYWRvbmx5ID0gKHJlYWRvbmx5OiBib29sZWFuKTogdm9pZCA9PiB7XHJcbiAgICB0aGlzLnJlYWRvbmx5ID0gcmVhZG9ubHk7XHJcbiAgfTtcclxuXHJcbiAgZ2V0UmVxdWlyZWQgPSAoKTogRm9ybUZpZWxkSW5wdXRSZXF1aXJlZEludGVyZmFjZSA9PiB7XHJcbiAgICByZXR1cm4gdGhpcy5yZXF1aXJlZDtcclxuICB9O1xyXG5cclxuICBzZXRSZXF1aXJlZCA9IChyZXF1aXJlZDogRm9ybUZpZWxkSW5wdXRSZXF1aXJlZEludGVyZmFjZSk6IHZvaWQgPT4ge1xyXG4gICAgdGhpcy5yZXF1aXJlZCA9IHJlcXVpcmVkO1xyXG4gIH07XHJcblxyXG4gIGdldExhYmVsTmFtZSA9ICgpOiBzdHJpbmcgPT4ge1xyXG4gICAgcmV0dXJuIHRoaXMubGFiZWxOYW1lO1xyXG4gIH07XHJcblxyXG4gIHNldExhYmVsTmFtZSA9IChsYWJlbE5hbWU6IHN0cmluZyk6IHZvaWQgPT4ge1xyXG4gICAgdGhpcy5sYWJlbE5hbWUgPSBsYWJlbE5hbWU7XHJcbiAgfTtcclxuXHJcbiAgZ2V0TGVmdEhpbnRMYWJlbCA9ICgpOiBzdHJpbmcgPT4ge1xyXG4gICAgcmV0dXJuIHRoaXMubGVmdEhpbnRMYWJlbDtcclxuICB9O1xyXG5cclxuICBzZXRMZWZ0SGludExhYmVsID0gKGxlZnRIaW50TGFiZWw6IHN0cmluZyk6IHZvaWQgPT4ge1xyXG4gICAgdGhpcy5sZWZ0SGludExhYmVsID0gbGVmdEhpbnRMYWJlbDtcclxuICB9O1xyXG5cclxuICBnZXRSaWdodEhpbnRMYWJlbCA9ICgpOiBzdHJpbmcgPT4ge1xyXG4gICAgcmV0dXJuIHRoaXMucmlnaHRIaW50TGFiZWw7XHJcbiAgfTtcclxuXHJcbiAgc2V0UmlnaHRIaW50TGFiZWwgPSAocmlnaHRIaW50TGFiZWw6IHN0cmluZyk6IHZvaWQgPT4ge1xyXG4gICAgdGhpcy5yaWdodEhpbnRMYWJlbCA9IHJpZ2h0SGludExhYmVsO1xyXG4gIH07XHJcblxyXG4gIGdldE1hdFByZWZpeEltZ1RleHQgPSAoKTogc3RyaW5nID0+IHtcclxuICAgIHJldHVybiB0aGlzLm1hdFByZWZpeEltZ1RleHQ7XHJcbiAgfTtcclxuXHJcbiAgc2V0TWF0UHJlZml4SW1nVGV4dCA9IChtYXRQcmVmaXhJbWdUZXh0OiBzdHJpbmcpOiB2b2lkID0+IHtcclxuICAgIHRoaXMubWF0UHJlZml4SW1nVGV4dCA9IG1hdFByZWZpeEltZ1RleHQ7XHJcbiAgfTtcclxuXHJcbiAgZ2V0TWF0U3VmZml4SW1nVGV4dCA9ICgpOiBzdHJpbmcgPT4ge1xyXG4gICAgcmV0dXJuIHRoaXMubWF0U3VmZml4SW1nVGV4dDtcclxuICB9O1xyXG5cclxuICBzZXRNYXRTdWZmaXhJbWdUZXh0ID0gKG1hdFN1ZmZpeEltZ1RleHQ6IHN0cmluZyk6IHZvaWQgPT4ge1xyXG4gICAgdGhpcy5tYXRTdWZmaXhJbWdUZXh0ID0gbWF0U3VmZml4SW1nVGV4dDtcclxuICB9O1xyXG5cclxuICBnZXRUZXh0U3VmZml4ID0gKCk6IHN0cmluZyA9PiB7XHJcbiAgICByZXR1cm4gdGhpcy50ZXh0U3VmZml4O1xyXG4gIH07XHJcblxyXG4gIHNldFRleHRTdWZmaXggPSAodGV4dFN1ZmZpeDogc3RyaW5nKTogdm9pZCA9PiB7XHJcbiAgICB0aGlzLnRleHRTdWZmaXggPSB0ZXh0U3VmZml4O1xyXG4gIH07XHJcblxyXG4gIGdldFRleHRQcmVmaXggPSAoKTogc3RyaW5nID0+IHtcclxuICAgIHJldHVybiB0aGlzLnRleHRQcmVmaXg7XHJcbiAgfTtcclxuXHJcbiAgc2V0VGV4dFByZWZpeCA9ICh0ZXh0UHJlZml4OiBzdHJpbmcpOiB2b2lkID0+IHtcclxuICAgIHRoaXMudGV4dFByZWZpeCA9IHRleHRQcmVmaXg7XHJcbiAgfTtcclxufVxyXG4iXX0=