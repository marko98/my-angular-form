import { __extends } from "tslib";
import { FormRowItem } from '../form-row-item.model';
import { MAT_COLOR } from '../../../../../material.module';
var SlideToggle = /** @class */ (function (_super) {
    __extends(SlideToggle, _super);
    function SlideToggle() {
        var _this = _super.call(this) || this;
        _this.color = MAT_COLOR.PRIMARY;
        _this.context = '';
        _this.getColor = function () {
            return _this.color;
        };
        _this.setColor = function (color) {
            _this.color = color;
        };
        _this.getContext = function () {
            return _this.context;
        };
        _this.setContext = function (context) {
            _this.context = context;
        };
        return _this;
    }
    return SlideToggle;
}(FormRowItem));
export { SlideToggle };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2xpZGUtdG9nZ2xlLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXktYW5ndWxhci1mb3JtLyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9tb2RlbC9zdHJ1Y3R1cmFsL2NvbXBvc2l0ZS9mb3JtL3NsaWRlLXRvZ2dsZS9zbGlkZS10b2dnbGUubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxXQUFXLEVBQXdCLE1BQU0sd0JBQXdCLENBQUM7QUFDM0UsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBUTNEO0lBQWlDLCtCQUFXO0lBSTFDO1FBQUEsWUFDRSxpQkFBTyxTQUNSO1FBTE8sV0FBSyxHQUFjLFNBQVMsQ0FBQyxPQUFPLENBQUM7UUFDckMsYUFBTyxHQUFXLEVBQUUsQ0FBQztRQU10QixjQUFRLEdBQUc7WUFDaEIsT0FBTyxLQUFJLENBQUMsS0FBSyxDQUFDO1FBQ3BCLENBQUMsQ0FBQztRQUVLLGNBQVEsR0FBRyxVQUFDLEtBQWdCO1lBQ2pDLEtBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1FBQ3JCLENBQUMsQ0FBQztRQUVLLGdCQUFVLEdBQUc7WUFDbEIsT0FBTyxLQUFJLENBQUMsT0FBTyxDQUFDO1FBQ3RCLENBQUMsQ0FBQztRQUVLLGdCQUFVLEdBQUcsVUFBQyxPQUFlO1lBQ2xDLEtBQUksQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO1FBQ3pCLENBQUMsQ0FBQzs7SUFoQkYsQ0FBQztJQWlCSCxrQkFBQztBQUFELENBQUMsQUF2QkQsQ0FBaUMsV0FBVyxHQXVCM0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBGb3JtUm93SXRlbSwgRm9ybVJvd0l0ZW1JbnRlcmZhY2UgfSBmcm9tICcuLi9mb3JtLXJvdy1pdGVtLm1vZGVsJztcclxuaW1wb3J0IHsgTUFUX0NPTE9SIH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vbWF0ZXJpYWwubW9kdWxlJztcclxuXHJcbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBTbGlkZVRvZ2dsZUludGVyZmFjZSBleHRlbmRzIEZvcm1Sb3dJdGVtSW50ZXJmYWNlIHtcclxuICBjb2xvcjogTUFUX0NPTE9SO1xyXG4gIGRpc2FibGVkOiBib29sZWFuO1xyXG4gIGNvbnRleHQ6IHN0cmluZztcclxufVxyXG5cclxuZXhwb3J0IGNsYXNzIFNsaWRlVG9nZ2xlIGV4dGVuZHMgRm9ybVJvd0l0ZW0ge1xyXG4gIHByaXZhdGUgY29sb3I6IE1BVF9DT0xPUiA9IE1BVF9DT0xPUi5QUklNQVJZO1xyXG4gIHByaXZhdGUgY29udGV4dDogc3RyaW5nID0gJyc7XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgc3VwZXIoKTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBnZXRDb2xvciA9ICgpOiBNQVRfQ09MT1IgPT4ge1xyXG4gICAgcmV0dXJuIHRoaXMuY29sb3I7XHJcbiAgfTtcclxuXHJcbiAgcHVibGljIHNldENvbG9yID0gKGNvbG9yOiBNQVRfQ09MT1IpOiB2b2lkID0+IHtcclxuICAgIHRoaXMuY29sb3IgPSBjb2xvcjtcclxuICB9O1xyXG5cclxuICBwdWJsaWMgZ2V0Q29udGV4dCA9ICgpOiBzdHJpbmcgPT4ge1xyXG4gICAgcmV0dXJuIHRoaXMuY29udGV4dDtcclxuICB9O1xyXG5cclxuICBwdWJsaWMgc2V0Q29udGV4dCA9IChjb250ZXh0OiBzdHJpbmcpOiB2b2lkID0+IHtcclxuICAgIHRoaXMuY29udGV4dCA9IGNvbnRleHQ7XHJcbiAgfTtcclxufVxyXG4iXX0=