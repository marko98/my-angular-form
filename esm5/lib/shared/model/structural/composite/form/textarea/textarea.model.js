import { __extends } from "tslib";
// model
import { FormFieldInput, } from '../form-field-input/form-field-input.model';
var Textarea = /** @class */ (function (_super) {
    __extends(Textarea, _super);
    function Textarea() {
        var _this = _super.call(this) || this;
        _this.showHintAboutMinMaxLength = false;
        _this.getShowHintAboutMinMaxLength = function () {
            return _this.showHintAboutMinMaxLength;
        };
        _this.setShowHintAboutMinMaxLength = function (showHintAboutMinMaxLength) {
            _this.showHintAboutMinMaxLength = showHintAboutMinMaxLength;
        };
        return _this;
    }
    return Textarea;
}(FormFieldInput));
export { Textarea };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGV4dGFyZWEubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9teS1hbmd1bGFyLWZvcm0vIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL21vZGVsL3N0cnVjdHVyYWwvY29tcG9zaXRlL2Zvcm0vdGV4dGFyZWEvdGV4dGFyZWEubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLFFBQVE7QUFDUixPQUFPLEVBRUwsY0FBYyxHQUNmLE1BQU0sNENBQTRDLENBQUM7QUFPcEQ7SUFBOEIsNEJBQWM7SUFJMUM7UUFBQSxZQUNFLGlCQUFPLFNBQ1I7UUFKTywrQkFBeUIsR0FBWSxLQUFLLENBQUM7UUFNbkQsa0NBQTRCLEdBQUc7WUFDN0IsT0FBTyxLQUFJLENBQUMseUJBQXlCLENBQUM7UUFDeEMsQ0FBQyxDQUFDO1FBRUYsa0NBQTRCLEdBQUcsVUFBQyx5QkFBa0M7WUFDaEUsS0FBSSxDQUFDLHlCQUF5QixHQUFHLHlCQUF5QixDQUFDO1FBQzdELENBQUMsQ0FBQzs7SUFSRixDQUFDO0lBU0gsZUFBQztBQUFELENBQUMsQUFmRCxDQUE4QixjQUFjLEdBZTNDIiwic291cmNlc0NvbnRlbnQiOlsiLy8gbW9kZWxcclxuaW1wb3J0IHtcclxuICBGb3JtRmllbGRJbnB1dEludGVyZmFjZSxcclxuICBGb3JtRmllbGRJbnB1dCxcclxufSBmcm9tICcuLi9mb3JtLWZpZWxkLWlucHV0L2Zvcm0tZmllbGQtaW5wdXQubW9kZWwnO1xyXG5pbXBvcnQgeyBGb3JtRmllbGRJbnB1dFRleHRQYXNzd29yZEludGVyZmFjZSB9IGZyb20gJy4uL2Zvcm0tZmllbGQtaW5wdXQvZm9ybS1maWVsZC1pbnB1dC10ZXh0LXBhc3N3b3JkLmludGVyZmFjZSc7XHJcblxyXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgVGV4dGFyZWFJbnRlcmZhY2UgZXh0ZW5kcyBGb3JtRmllbGRJbnB1dEludGVyZmFjZSB7XHJcbiAgc2hvd0hpbnRBYm91dE1pbk1heExlbmd0aD86IGJvb2xlYW47XHJcbn1cclxuXHJcbmV4cG9ydCBjbGFzcyBUZXh0YXJlYSBleHRlbmRzIEZvcm1GaWVsZElucHV0XHJcbiAgaW1wbGVtZW50cyBGb3JtRmllbGRJbnB1dFRleHRQYXNzd29yZEludGVyZmFjZSB7XHJcbiAgcHJpdmF0ZSBzaG93SGludEFib3V0TWluTWF4TGVuZ3RoOiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgc3VwZXIoKTtcclxuICB9XHJcblxyXG4gIGdldFNob3dIaW50QWJvdXRNaW5NYXhMZW5ndGggPSAoKTogYm9vbGVhbiA9PiB7XHJcbiAgICByZXR1cm4gdGhpcy5zaG93SGludEFib3V0TWluTWF4TGVuZ3RoO1xyXG4gIH07XHJcblxyXG4gIHNldFNob3dIaW50QWJvdXRNaW5NYXhMZW5ndGggPSAoc2hvd0hpbnRBYm91dE1pbk1heExlbmd0aDogYm9vbGVhbik6IHZvaWQgPT4ge1xyXG4gICAgdGhpcy5zaG93SGludEFib3V0TWluTWF4TGVuZ3RoID0gc2hvd0hpbnRBYm91dE1pbk1heExlbmd0aDtcclxuICB9O1xyXG59XHJcbiJdfQ==