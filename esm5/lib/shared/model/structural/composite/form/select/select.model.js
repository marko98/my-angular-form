import { __extends } from "tslib";
import { FormFieldInput, } from '../form-field-input/form-field-input.model';
var Select = /** @class */ (function (_super) {
    __extends(Select, _super);
    function Select() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.multiple = false;
        _this.matSelectTriggerOn = false;
        _this.optionValues = [];
        _this.getOptionValues = function () {
            return _this.optionValues;
        };
        _this.setOptionValues = function (optionValues) {
            _this.optionValues = optionValues;
        };
        _this.getMultiple = function () {
            return _this.multiple;
        };
        _this.setMultiple = function (multiple) {
            _this.multiple = multiple;
        };
        _this.getMatSelectTriggerOn = function () {
            return _this.matSelectTriggerOn;
        };
        _this.setMatSelectTriggerOn = function (matSelectTriggerOn) {
            _this.matSelectTriggerOn = matSelectTriggerOn;
        };
        return _this;
    }
    return Select;
}(FormFieldInput));
export { Select };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VsZWN0Lm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXktYW5ndWxhci1mb3JtLyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9tb2RlbC9zdHJ1Y3R1cmFsL2NvbXBvc2l0ZS9mb3JtL3NlbGVjdC9zZWxlY3QubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFDTCxjQUFjLEdBRWYsTUFBTSw0Q0FBNEMsQ0FBQztBQW9CcEQ7SUFBNEIsMEJBQWM7SUFBMUM7UUFBQSxxRUE4QkM7UUE3QlMsY0FBUSxHQUFZLEtBQUssQ0FBQztRQUMxQix3QkFBa0IsR0FBWSxLQUFLLENBQUM7UUFDcEMsa0JBQVksR0FBK0MsRUFBRSxDQUFDO1FBRXRFLHFCQUFlLEdBQUc7WUFDaEIsT0FBTyxLQUFJLENBQUMsWUFBWSxDQUFDO1FBQzNCLENBQUMsQ0FBQztRQUVGLHFCQUFlLEdBQUcsVUFDaEIsWUFBd0Q7WUFFeEQsS0FBSSxDQUFDLFlBQVksR0FBRyxZQUFZLENBQUM7UUFDbkMsQ0FBQyxDQUFDO1FBRUYsaUJBQVcsR0FBRztZQUNaLE9BQU8sS0FBSSxDQUFDLFFBQVEsQ0FBQztRQUN2QixDQUFDLENBQUM7UUFFRixpQkFBVyxHQUFHLFVBQUMsUUFBaUI7WUFDOUIsS0FBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7UUFDM0IsQ0FBQyxDQUFDO1FBRUYsMkJBQXFCLEdBQUc7WUFDdEIsT0FBTyxLQUFJLENBQUMsa0JBQWtCLENBQUM7UUFDakMsQ0FBQyxDQUFDO1FBRUYsMkJBQXFCLEdBQUcsVUFBQyxrQkFBMkI7WUFDbEQsS0FBSSxDQUFDLGtCQUFrQixHQUFHLGtCQUFrQixDQUFDO1FBQy9DLENBQUMsQ0FBQzs7SUFDSixDQUFDO0lBQUQsYUFBQztBQUFELENBQUMsQUE5QkQsQ0FBNEIsY0FBYyxHQThCekMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xyXG4gIEZvcm1GaWVsZElucHV0LFxyXG4gIEZvcm1GaWVsZElucHV0SW50ZXJmYWNlLFxyXG59IGZyb20gJy4uL2Zvcm0tZmllbGQtaW5wdXQvZm9ybS1maWVsZC1pbnB1dC5tb2RlbCc7XHJcblxyXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgU2VsZWN0SW50ZXJmYWNlIGV4dGVuZHMgRm9ybUZpZWxkSW5wdXRJbnRlcmZhY2Uge1xyXG4gIG11bHRpcGxlOiBib29sZWFuO1xyXG4gIG1hdFNlbGVjdFRyaWdnZXJPbjogYm9vbGVhbjtcclxuICBvcHRpb25WYWx1ZXM6IChPcHRpb25JbnRlcmZhY2UgfCBPcHRpb25Hcm91cEludGVyZmFjZSlbXTtcclxufVxyXG5cclxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIE9wdGlvbkludGVyZmFjZSB7XHJcbiAgdmFsdWU6IGFueTtcclxuICB0ZXh0VG9TaG93OiBzdHJpbmc7XHJcbiAgZGlzYWJsZWQ6IGJvb2xlYW47XHJcbn1cclxuXHJcbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBPcHRpb25Hcm91cEludGVyZmFjZSB7XHJcbiAgbGFiZWxOYW1lOiBzdHJpbmc7XHJcbiAgZGlzYWJsZWQ6IGJvb2xlYW47XHJcbiAgb3B0aW9uVmFsdWVzOiBPcHRpb25JbnRlcmZhY2VbXTtcclxufVxyXG5cclxuZXhwb3J0IGNsYXNzIFNlbGVjdCBleHRlbmRzIEZvcm1GaWVsZElucHV0IHtcclxuICBwcml2YXRlIG11bHRpcGxlOiBib29sZWFuID0gZmFsc2U7XHJcbiAgcHJpdmF0ZSBtYXRTZWxlY3RUcmlnZ2VyT246IGJvb2xlYW4gPSBmYWxzZTtcclxuICBwcml2YXRlIG9wdGlvblZhbHVlczogKE9wdGlvbkludGVyZmFjZSB8IE9wdGlvbkdyb3VwSW50ZXJmYWNlKVtdID0gW107XHJcblxyXG4gIGdldE9wdGlvblZhbHVlcyA9ICgpOiAoT3B0aW9uSW50ZXJmYWNlIHwgT3B0aW9uR3JvdXBJbnRlcmZhY2UpW10gPT4ge1xyXG4gICAgcmV0dXJuIHRoaXMub3B0aW9uVmFsdWVzO1xyXG4gIH07XHJcblxyXG4gIHNldE9wdGlvblZhbHVlcyA9IChcclxuICAgIG9wdGlvblZhbHVlczogKE9wdGlvbkludGVyZmFjZSB8IE9wdGlvbkdyb3VwSW50ZXJmYWNlKVtdXHJcbiAgKTogdm9pZCA9PiB7XHJcbiAgICB0aGlzLm9wdGlvblZhbHVlcyA9IG9wdGlvblZhbHVlcztcclxuICB9O1xyXG5cclxuICBnZXRNdWx0aXBsZSA9ICgpOiBib29sZWFuID0+IHtcclxuICAgIHJldHVybiB0aGlzLm11bHRpcGxlO1xyXG4gIH07XHJcblxyXG4gIHNldE11bHRpcGxlID0gKG11bHRpcGxlOiBib29sZWFuKTogdm9pZCA9PiB7XHJcbiAgICB0aGlzLm11bHRpcGxlID0gbXVsdGlwbGU7XHJcbiAgfTtcclxuXHJcbiAgZ2V0TWF0U2VsZWN0VHJpZ2dlck9uID0gKCk6IGJvb2xlYW4gPT4ge1xyXG4gICAgcmV0dXJuIHRoaXMubWF0U2VsZWN0VHJpZ2dlck9uO1xyXG4gIH07XHJcblxyXG4gIHNldE1hdFNlbGVjdFRyaWdnZXJPbiA9IChtYXRTZWxlY3RUcmlnZ2VyT246IGJvb2xlYW4pOiB2b2lkID0+IHtcclxuICAgIHRoaXMubWF0U2VsZWN0VHJpZ2dlck9uID0gbWF0U2VsZWN0VHJpZ2dlck9uO1xyXG4gIH07XHJcbn1cclxuIl19