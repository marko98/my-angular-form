var Collection = /** @class */ (function () {
    function Collection() {
        var _this = this;
        this.children = [];
        this.parent = undefined;
        this.getChildren = function () {
            // vracamo kopiju
            // return this.children.slice();
            return _this.children;
        };
        this.getParent = function () {
            return _this.parent;
        };
        this.setParent = function (parent) {
            _this.parent = parent;
            return true;
        };
    }
    return Collection;
}());
export { Collection };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sbGVjdGlvbi5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215LWFuZ3VsYXItZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvbW9kZWwvc3RydWN0dXJhbC9jb21wb3NpdGUvY29sbGVjdGlvbi5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUFBO1FBQUEsaUJBd0JDO1FBdkJXLGFBQVEsR0FBaUIsRUFBRSxDQUFDO1FBQzVCLFdBQU0sR0FBZSxTQUFTLENBQUM7UUFRekMsZ0JBQVcsR0FBRztZQUNaLGlCQUFpQjtZQUNqQixnQ0FBZ0M7WUFDaEMsT0FBTyxLQUFJLENBQUMsUUFBUSxDQUFDO1FBQ3ZCLENBQUMsQ0FBQztRQUVGLGNBQVMsR0FBRztZQUNWLE9BQU8sS0FBSSxDQUFDLE1BQU0sQ0FBQztRQUNyQixDQUFDLENBQUM7UUFFRixjQUFTLEdBQUcsVUFBQyxNQUFrQjtZQUM3QixLQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztZQUNyQixPQUFPLElBQUksQ0FBQztRQUNkLENBQUMsQ0FBQztJQUNKLENBQUM7SUFBRCxpQkFBQztBQUFELENBQUMsQUF4QkQsSUF3QkMiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgYWJzdHJhY3QgY2xhc3MgQ29sbGVjdGlvbiB7XHJcbiAgcHJvdGVjdGVkIGNoaWxkcmVuOiBDb2xsZWN0aW9uW10gPSBbXTtcclxuICBwcm90ZWN0ZWQgcGFyZW50OiBDb2xsZWN0aW9uID0gdW5kZWZpbmVkO1xyXG5cclxuICBhYnN0cmFjdCBhZGRDaGlsZChjaGlsZDogQ29sbGVjdGlvbik6IGJvb2xlYW47XHJcblxyXG4gIGFic3RyYWN0IHJlbW92ZUNoaWxkKGNoaWxkOiBDb2xsZWN0aW9uKTogYm9vbGVhbjtcclxuXHJcbiAgYWJzdHJhY3Qgc2hvdWxkSGF2ZUNoaWxkcmVuKCk6IGJvb2xlYW47XHJcblxyXG4gIGdldENoaWxkcmVuID0gKCk6IENvbGxlY3Rpb25bXSA9PiB7XHJcbiAgICAvLyB2cmFjYW1vIGtvcGlqdVxyXG4gICAgLy8gcmV0dXJuIHRoaXMuY2hpbGRyZW4uc2xpY2UoKTtcclxuICAgIHJldHVybiB0aGlzLmNoaWxkcmVuO1xyXG4gIH07XHJcblxyXG4gIGdldFBhcmVudCA9ICgpOiBDb2xsZWN0aW9uID0+IHtcclxuICAgIHJldHVybiB0aGlzLnBhcmVudDtcclxuICB9O1xyXG5cclxuICBzZXRQYXJlbnQgPSAocGFyZW50OiBDb2xsZWN0aW9uKTogYm9vbGVhbiA9PiB7XHJcbiAgICB0aGlzLnBhcmVudCA9IHBhcmVudDtcclxuICAgIHJldHVybiB0cnVlO1xyXG4gIH07XHJcbn1cclxuIl19