import { Component, forwardRef, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import * as i0 from "@angular/core";
import * as i1 from "@angular/flex-layout/flex";
import * as i2 from "@angular/material/form-field";
import * as i3 from "ngx-material-timepicker";
import * as i4 from "@angular/material/input";
var TimepickerComponent = /** @class */ (function () {
    function TimepickerComponent() {
        var _this = this;
        this.onChange = function () { };
        this.onTouched = function () { };
        this.onTimeChanged = function (time) {
            // console.log(time);
            _this.timepicker.setDefaultValue(time + ':00');
            _this.onChange(_this.timepicker.getDefaultValue());
            _this.onTouched();
        };
        this.onTimepickerClosed = function () {
            _this.onChange(_this.timepicker.getDefaultValue());
            _this.onTouched();
        };
    }
    TimepickerComponent.prototype.registerOnChange = function (fn) {
        this.onChange = fn;
    };
    TimepickerComponent.prototype.registerOnTouched = function (fn) {
        this.onTouched = fn;
    };
    TimepickerComponent.prototype.writeValue = function (value) {
        if (value) {
            this.timepicker.setDefaultValue(value);
        }
    };
    TimepickerComponent.prototype.ngOnInit = function () {
        // console.log('TimepickerComponent init');
    };
    TimepickerComponent.prototype.ngOnDestroy = function () {
        // console.log('TimepickerComponent destroyed');
    };
    TimepickerComponent.ɵfac = function TimepickerComponent_Factory(t) { return new (t || TimepickerComponent)(); };
    TimepickerComponent.ɵcmp = i0.ɵɵdefineComponent({ type: TimepickerComponent, selectors: [["app-timepicker"]], inputs: { timepicker: "timepicker" }, features: [i0.ɵɵProvidersFeature([
                {
                    provide: NG_VALUE_ACCESSOR,
                    multi: true,
                    useExisting: forwardRef(function () { return TimepickerComponent; }),
                },
            ])], decls: 7, vars: 9, consts: [["fxLayoutAlign", "center center"], [3, "appearance"], [3, "buttonAlign", "disabled", "format", "min", "max", "defaultTime", "timeChanged", "closed"], ["matInput", "", "type", "timepicker", 3, "hidden"]], template: function TimepickerComponent_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵelementStart(0, "section", 0);
            i0.ɵɵelementStart(1, "mat-form-field", 1);
            i0.ɵɵelementStart(2, "mat-label");
            i0.ɵɵtext(3);
            i0.ɵɵelementEnd();
            i0.ɵɵelement(4, "br");
            i0.ɵɵelementStart(5, "ngx-timepicker-field", 2);
            i0.ɵɵlistener("timeChanged", function TimepickerComponent_Template_ngx_timepicker_field_timeChanged_5_listener($event) { return ctx.onTimeChanged($event); })("closed", function TimepickerComponent_Template_ngx_timepicker_field_closed_5_listener() { return ctx.onTimepickerClosed(); });
            i0.ɵɵelementEnd();
            i0.ɵɵelement(6, "input", 3);
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        } if (rf & 2) {
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("appearance", ctx.timepicker.getAppearance());
            i0.ɵɵadvance(2);
            i0.ɵɵtextInterpolate(ctx.timepicker.getLabelName());
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("buttonAlign", ctx.timepicker.getButtonAlign())("disabled", ctx.timepicker.getDisabled())("format", 24)("min", ctx.timepicker.getMin() ? ctx.timepicker.getMin() : undefined)("max", ctx.timepicker.getMax() ? ctx.timepicker.getMax() : undefined)("defaultTime", ctx.timepicker.getDefaultValue());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("hidden", true);
        } }, directives: [i1.DefaultLayoutAlignDirective, i2.MatFormField, i2.MatLabel, i3.NgxTimepickerFieldComponent, i4.MatInput], styles: ["", "mat-form-field[_ngcontent-%COMP%] {\n        min-width: 250px;\n    }"] });
    return TimepickerComponent;
}());
export { TimepickerComponent };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(TimepickerComponent, [{
        type: Component,
        args: [{
                selector: 'app-timepicker',
                templateUrl: './timepicker.component.html',
                styleUrls: ['./timepicker.component.css'],
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        multi: true,
                        useExisting: forwardRef(function () { return TimepickerComponent; }),
                    },
                ],
            }]
    }], function () { return []; }, { timepicker: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGltZXBpY2tlci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9teS1hbmd1bGFyLWZvcm0vIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL3VpL3RpbWVwaWNrZXIvdGltZXBpY2tlci5jb21wb25lbnQudHMiLCJsaWIvc2hhcmVkL3VpL3RpbWVwaWNrZXIvdGltZXBpY2tlci5jb21wb25lbnQuaHRtbCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFxQixVQUFVLEVBQUUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ2hGLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGdCQUFnQixDQUFDOzs7Ozs7QUFHbkQ7SUFpQkU7UUFBQSxpQkFBZ0I7UUFIVCxhQUFRLEdBQVEsY0FBTyxDQUFDLENBQUM7UUFDekIsY0FBUyxHQUFRLGNBQU8sQ0FBQyxDQUFDO1FBSWpDLGtCQUFhLEdBQUcsVUFBQyxJQUFZO1lBQzNCLHFCQUFxQjtZQUNyQixLQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQyxJQUFJLEdBQUcsS0FBSyxDQUFDLENBQUM7WUFDOUMsS0FBSSxDQUFDLFFBQVEsQ0FBQyxLQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsRUFBRSxDQUFDLENBQUM7WUFDakQsS0FBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ25CLENBQUMsQ0FBQztRQUVGLHVCQUFrQixHQUFHO1lBQ25CLEtBQUksQ0FBQyxRQUFRLENBQUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLEVBQUUsQ0FBQyxDQUFDO1lBQ2pELEtBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUNuQixDQUFDLENBQUM7SUFaYSxDQUFDO0lBY2hCLDhDQUFnQixHQUFoQixVQUFpQixFQUFFO1FBQ2pCLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO0lBQ3JCLENBQUM7SUFFRCwrQ0FBaUIsR0FBakIsVUFBa0IsRUFBRTtRQUNsQixJQUFJLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztJQUN0QixDQUFDO0lBRUQsd0NBQVUsR0FBVixVQUFXLEtBQUs7UUFDZCxJQUFJLEtBQUssRUFBRTtZQUNULElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3hDO0lBQ0gsQ0FBQztJQUVELHNDQUFRLEdBQVI7UUFDRSwyQ0FBMkM7SUFDN0MsQ0FBQztJQUVELHlDQUFXLEdBQVg7UUFDRSxnREFBZ0Q7SUFDbEQsQ0FBQzswRkF2Q1UsbUJBQW1COzREQUFuQixtQkFBbUIsMEdBUm5CO2dCQUNUO29CQUNFLE9BQU8sRUFBRSxpQkFBaUI7b0JBQzFCLEtBQUssRUFBRSxJQUFJO29CQUNYLFdBQVcsRUFBRSxVQUFVLENBQUMsY0FBTSxPQUFBLG1CQUFtQixFQUFuQixDQUFtQixDQUFDO2lCQUNuRDthQUNGO1lDTkgsa0NBR0k7WUFBQSx5Q0FDSTtZQUFBLGlDQUFXO1lBQUEsWUFBa0M7WUFBQSxpQkFBWTtZQUN6RCxxQkFFQTtZQUtBLCtDQVFnRTtZQUQ1RCxnSUFBZSx5QkFBMEIsSUFBQyxtR0FDaEMsd0JBQXlCLElBRE87WUFDTCxpQkFBdUI7WUFFaEUsMkJBQ0o7WUFBQSxpQkFBaUI7WUFFckIsaUJBQVU7O1lBdEJVLGVBQThDO1lBQTlDLDJEQUE4QztZQUMvQyxlQUFrQztZQUFsQyxtREFBa0M7WUFTekMsZUFBZ0Q7WUFBaEQsNkRBQWdELDBDQUFBLGNBQUEsc0VBQUEsc0VBQUEsaURBQUE7WUFTbEIsZUFBZTtZQUFmLDZCQUFlOzs4QkQ5QnpEO0NBd0RDLEFBcERELElBb0RDO1NBeENZLG1CQUFtQjtrREFBbkIsbUJBQW1CO2NBWi9CLFNBQVM7ZUFBQztnQkFDVCxRQUFRLEVBQUUsZ0JBQWdCO2dCQUMxQixXQUFXLEVBQUUsNkJBQTZCO2dCQUMxQyxTQUFTLEVBQUUsQ0FBQyw0QkFBNEIsQ0FBQztnQkFDekMsU0FBUyxFQUFFO29CQUNUO3dCQUNFLE9BQU8sRUFBRSxpQkFBaUI7d0JBQzFCLEtBQUssRUFBRSxJQUFJO3dCQUNYLFdBQVcsRUFBRSxVQUFVLENBQUMsY0FBTSxPQUFBLG1CQUFtQixFQUFuQixDQUFtQixDQUFDO3FCQUNuRDtpQkFDRjthQUNGOztrQkFFRSxLQUFLIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIE9uRGVzdHJveSwgZm9yd2FyZFJlZiwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE5HX1ZBTFVFX0FDQ0VTU09SIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuaW1wb3J0IHsgVGltZXBpY2tlciB9IGZyb20gJy4uLy4uL21vZGVsL3N0cnVjdHVyYWwvY29tcG9zaXRlL2Zvcm0vdGltZXBpY2tlci90aW1lcGlja2VyLm1vZGVsJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnYXBwLXRpbWVwaWNrZXInLFxuICB0ZW1wbGF0ZVVybDogJy4vdGltZXBpY2tlci5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL3RpbWVwaWNrZXIuY29tcG9uZW50LmNzcyddLFxuICBwcm92aWRlcnM6IFtcbiAgICB7XG4gICAgICBwcm92aWRlOiBOR19WQUxVRV9BQ0NFU1NPUixcbiAgICAgIG11bHRpOiB0cnVlLFxuICAgICAgdXNlRXhpc3Rpbmc6IGZvcndhcmRSZWYoKCkgPT4gVGltZXBpY2tlckNvbXBvbmVudCksXG4gICAgfSxcbiAgXSxcbn0pXG5leHBvcnQgY2xhc3MgVGltZXBpY2tlckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcbiAgQElucHV0KCkgdGltZXBpY2tlcjogVGltZXBpY2tlcjtcbiAgcHVibGljIG9uQ2hhbmdlOiBhbnkgPSAoKSA9PiB7fTtcbiAgcHVibGljIG9uVG91Y2hlZDogYW55ID0gKCkgPT4ge307XG5cbiAgY29uc3RydWN0b3IoKSB7fVxuXG4gIG9uVGltZUNoYW5nZWQgPSAodGltZTogc3RyaW5nKTogdm9pZCA9PiB7XG4gICAgLy8gY29uc29sZS5sb2codGltZSk7XG4gICAgdGhpcy50aW1lcGlja2VyLnNldERlZmF1bHRWYWx1ZSh0aW1lICsgJzowMCcpO1xuICAgIHRoaXMub25DaGFuZ2UodGhpcy50aW1lcGlja2VyLmdldERlZmF1bHRWYWx1ZSgpKTtcbiAgICB0aGlzLm9uVG91Y2hlZCgpO1xuICB9O1xuXG4gIG9uVGltZXBpY2tlckNsb3NlZCA9ICgpOiB2b2lkID0+IHtcbiAgICB0aGlzLm9uQ2hhbmdlKHRoaXMudGltZXBpY2tlci5nZXREZWZhdWx0VmFsdWUoKSk7XG4gICAgdGhpcy5vblRvdWNoZWQoKTtcbiAgfTtcblxuICByZWdpc3Rlck9uQ2hhbmdlKGZuKSB7XG4gICAgdGhpcy5vbkNoYW5nZSA9IGZuO1xuICB9XG5cbiAgcmVnaXN0ZXJPblRvdWNoZWQoZm4pIHtcbiAgICB0aGlzLm9uVG91Y2hlZCA9IGZuO1xuICB9XG5cbiAgd3JpdGVWYWx1ZSh2YWx1ZSkge1xuICAgIGlmICh2YWx1ZSkge1xuICAgICAgdGhpcy50aW1lcGlja2VyLnNldERlZmF1bHRWYWx1ZSh2YWx1ZSk7XG4gICAgfVxuICB9XG5cbiAgbmdPbkluaXQoKTogdm9pZCB7XG4gICAgLy8gY29uc29sZS5sb2coJ1RpbWVwaWNrZXJDb21wb25lbnQgaW5pdCcpO1xuICB9XG5cbiAgbmdPbkRlc3Ryb3koKTogdm9pZCB7XG4gICAgLy8gY29uc29sZS5sb2coJ1RpbWVwaWNrZXJDb21wb25lbnQgZGVzdHJveWVkJyk7XG4gIH1cbn1cbiIsIjxzdHlsZT5cblxuICAgIG1hdC1mb3JtLWZpZWxkIHtcbiAgICAgICAgbWluLXdpZHRoOiAyNTBweDtcbiAgICB9XG5cbjwvc3R5bGU+XG5cbjxzZWN0aW9uXG4gICAgZnhMYXlvdXRBbGlnbj1cImNlbnRlciBjZW50ZXJcIj5cblxuICAgIDxtYXQtZm9ybS1maWVsZCBbYXBwZWFyYW5jZV09XCJ0aGlzLnRpbWVwaWNrZXIuZ2V0QXBwZWFyYW5jZSgpXCI+XG4gICAgICAgIDxtYXQtbGFiZWw+e3t0aGlzLnRpbWVwaWNrZXIuZ2V0TGFiZWxOYW1lKCl9fTwvbWF0LWxhYmVsPlxuICAgICAgICA8YnI+XG5cbiAgICAgICAgPCEtLSBcbiAgICAgICAgICAgIGh0dHBzOi8vYWdyYW5vbS5naXRodWIuaW8vbmd4LW1hdGVyaWFsLXRpbWVwaWNrZXIvXG4gICAgICAgICAgICBodHRwczovL3d3dy5ucG1qcy5jb20vcGFja2FnZS9uZ3gtbWF0ZXJpYWwtdGltZXBpY2tlclxuICAgICAgICAgLS0+XG5cbiAgICAgICAgPG5neC10aW1lcGlja2VyLWZpZWxkIFxuICAgICAgICAgICAgW2J1dHRvbkFsaWduXT1cInRoaXMudGltZXBpY2tlci5nZXRCdXR0b25BbGlnbigpXCJcbiAgICAgICAgICAgIFtkaXNhYmxlZF09XCJ0aGlzLnRpbWVwaWNrZXIuZ2V0RGlzYWJsZWQoKVwiXG4gICAgICAgICAgICBbZm9ybWF0XT1cIjI0XCJcbiAgICAgICAgICAgIFttaW5dPVwidGhpcy50aW1lcGlja2VyLmdldE1pbigpID8gdGhpcy50aW1lcGlja2VyLmdldE1pbigpIDogdW5kZWZpbmVkXCJcbiAgICAgICAgICAgIFttYXhdPVwidGhpcy50aW1lcGlja2VyLmdldE1heCgpID8gdGhpcy50aW1lcGlja2VyLmdldE1heCgpIDogdW5kZWZpbmVkXCJcbiAgICAgICAgICAgIFtkZWZhdWx0VGltZV09XCJ0aGlzLnRpbWVwaWNrZXIuZ2V0RGVmYXVsdFZhbHVlKClcIlxuICAgICAgICAgICAgKHRpbWVDaGFuZ2VkKT1cInRoaXMub25UaW1lQ2hhbmdlZCgkZXZlbnQpXCJcbiAgICAgICAgICAgIChjbG9zZWQpPVwidGhpcy5vblRpbWVwaWNrZXJDbG9zZWQoKVwiPjwvbmd4LXRpbWVwaWNrZXItZmllbGQ+XG5cbiAgICAgICAgPGlucHV0IG1hdElucHV0IHR5cGU9XCJ0aW1lcGlja2VyXCIgW2hpZGRlbl09XCJ0cnVlXCI+XG4gICAgPC9tYXQtZm9ybS1maWVsZD5cblxuPC9zZWN0aW9uPiJdfQ==