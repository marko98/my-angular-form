import { Component, forwardRef, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import * as i0 from "@angular/core";
import * as i1 from "@angular/flex-layout/flex";
import * as i2 from "@angular/material/form-field";
import * as i3 from "@angular/common";
import * as i4 from "@angular/material/select";
import * as i5 from "@angular/material/core";
import * as i6 from "@angular/material/icon";
function SelectComponent_mat_label_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-label");
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r0 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r0.select.getLabelName());
} }
function SelectComponent_mat_select_trigger_5_span_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span");
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r10 = i0.ɵɵnextContext(2);
    var tmp_0_0 = null;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate2(" (+", ctx_r10.select.getDefaultValue().length - 1, " ", ((tmp_0_0 = ctx_r10.select.getDefaultValue()) == null ? null : tmp_0_0.length) === 2 ? "other" : "others", ") ");
} }
function SelectComponent_mat_select_trigger_5_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-select-trigger");
    i0.ɵɵtext(1);
    i0.ɵɵtemplate(2, SelectComponent_mat_select_trigger_5_span_2_Template, 2, 2, "span", 2);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r2 = i0.ɵɵnextContext();
    var tmp_1_0 = null;
    var currVal_1 = ((tmp_1_0 = ctx_r2.select.getDefaultValue()) == null ? null : tmp_1_0.length) > 1;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r2.select.getDefaultValue() ? ctx_r2.select.getDefaultValue()[0] : "", " ");
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", currVal_1);
} }
function SelectComponent_section_6_mat_option_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-option", 12);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var optionValue_r11 = i0.ɵɵnextContext().$implicit;
    i0.ɵɵproperty("value", optionValue_r11.value)("disabled", optionValue_r11.disabled);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1("", optionValue_r11.textToShow, " ");
} }
function SelectComponent_section_6_mat_optgroup_2_mat_option_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-option", 12);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var oV_r16 = ctx.$implicit;
    i0.ɵɵproperty("value", oV_r16.value)("disabled", oV_r16.disabled);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", oV_r16.textToShow, " ");
} }
function SelectComponent_section_6_mat_optgroup_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-optgroup", 13);
    i0.ɵɵtemplate(1, SelectComponent_section_6_mat_optgroup_2_mat_option_1_Template, 2, 3, "mat-option", 14);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var optionValue_r11 = i0.ɵɵnextContext().$implicit;
    i0.ɵɵproperty("label", optionValue_r11.labelName)("disabled", optionValue_r11.disabled);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngForOf", optionValue_r11.optionValues);
} }
function SelectComponent_section_6_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "section");
    i0.ɵɵtemplate(1, SelectComponent_section_6_mat_option_1_Template, 2, 3, "mat-option", 10);
    i0.ɵɵtemplate(2, SelectComponent_section_6_mat_optgroup_2_Template, 2, 3, "mat-optgroup", 11);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var optionValue_r11 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !optionValue_r11.optionValues);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", optionValue_r11.optionValues && optionValue_r11.labelName);
} }
function SelectComponent_mat_icon_7_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-icon", 15);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r4 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r4.select.getMatPrefixImgText());
} }
function SelectComponent_mat_icon_8_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-icon", 16);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r5 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r5.select.getMatSuffixImgText());
} }
function SelectComponent_span_9_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 15);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r6 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1("", ctx_r6.select.getTextPrefix(), "\u00A0");
} }
function SelectComponent_span_10_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 16);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r7 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r7.select.getTextSuffix());
} }
function SelectComponent_mat_hint_11_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-hint", 17);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r8 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r8.select.getLeftHintLabel());
} }
function SelectComponent_mat_hint_12_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-hint", 18);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r9 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r9.select.getRightHintLabel());
} }
var SelectComponent = /** @class */ (function () {
    function SelectComponent() {
        var _this = this;
        this.onChange = function () { };
        this.onTouched = function () { };
        this.onSelectionChange = function (select) {
            // console.log(select.value);
            _this.select.setDefaultValue(select.value);
            _this.onChange(_this.select.getDefaultValue());
            _this.onTouched();
        };
        this.onBlur = function () {
            _this.onChange(_this.select.getDefaultValue());
            _this.onTouched();
        };
    }
    SelectComponent.prototype.registerOnChange = function (fn) {
        this.onChange = fn;
    };
    SelectComponent.prototype.registerOnTouched = function (fn) {
        this.onTouched = fn;
    };
    SelectComponent.prototype.writeValue = function (value) {
        if (value) {
            this.select.setDefaultValue(value);
        }
    };
    SelectComponent.prototype.ngOnInit = function () {
        // console.log('SelectComponent init');
    };
    SelectComponent.prototype.ngOnDestroy = function () {
        // console.log('SelectComponent destroyed');
    };
    SelectComponent.ɵfac = function SelectComponent_Factory(t) { return new (t || SelectComponent)(); };
    SelectComponent.ɵcmp = i0.ɵɵdefineComponent({ type: SelectComponent, selectors: [["app-select"]], inputs: { select: "select" }, features: [i0.ɵɵProvidersFeature([
                {
                    provide: NG_VALUE_ACCESSOR,
                    multi: true,
                    useExisting: forwardRef(function () { return SelectComponent; }),
                },
            ])], decls: 13, vars: 15, consts: [["fxLayoutAlign", "center center"], [3, "appearance", "hideRequiredMarker"], [4, "ngIf"], [3, "value", "multiple", "required", "disabled", "selectionChange", "blur"], ["matSelect", ""], [4, "ngFor", "ngForOf"], ["matPrefix", "", 4, "ngIf"], ["matSuffix", "", 4, "ngIf"], ["align", "start", 4, "ngIf"], ["align", "end", 4, "ngIf"], [3, "value", "disabled", 4, "ngIf"], [3, "label", "disabled", 4, "ngIf"], [3, "value", "disabled"], [3, "label", "disabled"], [3, "value", "disabled", 4, "ngFor", "ngForOf"], ["matPrefix", ""], ["matSuffix", ""], ["align", "start"], ["align", "end"]], template: function SelectComponent_Template(rf, ctx) { if (rf & 1) {
            var _r18 = i0.ɵɵgetCurrentView();
            i0.ɵɵelementStart(0, "section", 0);
            i0.ɵɵelementStart(1, "mat-form-field", 1);
            i0.ɵɵtemplate(2, SelectComponent_mat_label_2_Template, 2, 1, "mat-label", 2);
            i0.ɵɵelementStart(3, "mat-select", 3, 4);
            i0.ɵɵlistener("selectionChange", function SelectComponent_Template_mat_select_selectionChange_3_listener() { i0.ɵɵrestoreView(_r18); var _r1 = i0.ɵɵreference(4); return ctx.onSelectionChange(_r1); })("blur", function SelectComponent_Template_mat_select_blur_3_listener() { return ctx.onBlur(); });
            i0.ɵɵtemplate(5, SelectComponent_mat_select_trigger_5_Template, 3, 2, "mat-select-trigger", 2);
            i0.ɵɵtemplate(6, SelectComponent_section_6_Template, 3, 2, "section", 5);
            i0.ɵɵelementEnd();
            i0.ɵɵtemplate(7, SelectComponent_mat_icon_7_Template, 2, 1, "mat-icon", 6);
            i0.ɵɵtemplate(8, SelectComponent_mat_icon_8_Template, 2, 1, "mat-icon", 7);
            i0.ɵɵtemplate(9, SelectComponent_span_9_Template, 2, 1, "span", 6);
            i0.ɵɵtemplate(10, SelectComponent_span_10_Template, 2, 1, "span", 7);
            i0.ɵɵtemplate(11, SelectComponent_mat_hint_11_Template, 2, 1, "mat-hint", 8);
            i0.ɵɵtemplate(12, SelectComponent_mat_hint_12_Template, 2, 1, "mat-hint", 9);
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        } if (rf & 2) {
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("appearance", ctx.select.getAppearance())("hideRequiredMarker", ctx.select.getRequired().hideRequiredMarker);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.select.getLabelName());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("value", ctx.select.getDefaultValue())("multiple", ctx.select.getMultiple())("required", ctx.select.getRequired().required)("disabled", ctx.select.getDisabled());
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("ngIf", ctx.select.getMatSelectTriggerOn());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngForOf", ctx.select.getOptionValues());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.select.getMatPrefixImgText());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.select.getMatSuffixImgText());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.select.getTextPrefix());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.select.getTextSuffix());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.select.getLeftHintLabel());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.select.getRightHintLabel());
        } }, directives: [i1.DefaultLayoutAlignDirective, i2.MatFormField, i3.NgIf, i4.MatSelect, i3.NgForOf, i2.MatLabel, i4.MatSelectTrigger, i5.MatOption, i5.MatOptgroup, i6.MatIcon, i2.MatPrefix, i2.MatSuffix, i2.MatHint], styles: ["", ".cursor[_ngcontent-%COMP%] {\n        cursor: pointer;\n    }\n\n    input[type=\"file\"][_ngcontent-%COMP%] {\n        display: none;\n    }\n\n    h3[_ngcontent-%COMP%] {\n        font-size: inherit;\n        margin: 0;\n        font-weight: inherit;\n    }\n\n    mat-form-field[_ngcontent-%COMP%] {\n        min-width: 250px;\n    }"] });
    return SelectComponent;
}());
export { SelectComponent };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(SelectComponent, [{
        type: Component,
        args: [{
                selector: 'app-select',
                templateUrl: './select.component.html',
                styleUrls: ['./select.component.css'],
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        multi: true,
                        useExisting: forwardRef(function () { return SelectComponent; }),
                    },
                ],
            }]
    }], function () { return []; }, { select: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VsZWN0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215LWFuZ3VsYXItZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvdWkvc2VsZWN0L3NlbGVjdC5jb21wb25lbnQudHMiLCJsaWIvc2hhcmVkL3VpL3NlbGVjdC9zZWxlY3QuY29tcG9uZW50Lmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxVQUFVLEVBQUUsS0FBSyxFQUFhLE1BQU0sZUFBZSxDQUFDO0FBQ2hGLE9BQU8sRUFBRSxpQkFBaUIsRUFBdUIsTUFBTSxnQkFBZ0IsQ0FBQzs7Ozs7Ozs7O0lDNEJ4RCxpQ0FDdUM7SUFBQSxZQUFnQztJQUFBLGlCQUFZOzs7SUFBNUMsZUFBZ0M7SUFBaEMsa0RBQWdDOzs7SUFlM0QsNEJBQ0E7SUFBQSxZQUNBO0lBQUEsaUJBQU87Ozs7SUFEUCxlQUNBO0lBREEsK0xBQ0E7OztJQUxSLDBDQUVRO0lBQUEsWUFDQTtJQUFBLHVGQUNBO0lBRVIsaUJBQXFCOzs7OztJQUpiLGVBQ0E7SUFEQSwwR0FDQTtJQUFNLGVBQWlEO0lBQWpELGdDQUFpRDs7O0lBUXZELHNDQUdzQztJQUFBLFlBQ3RDO0lBQUEsaUJBQWE7OztJQUZULDZDQUEyQixzQ0FBQTtJQUNPLGVBQ3RDO0lBRHNDLDBEQUN0Qzs7O0lBT1Esc0NBSVE7SUFBQSxZQUNSO0lBQUEsaUJBQWE7OztJQUhULG9DQUFrQiw2QkFBQTtJQUVkLGVBQ1I7SUFEUSxrREFDUjs7O0lBVlIsd0NBS1E7SUFBQSx3R0FJUTtJQUdoQixpQkFBZTs7O0lBVlgsaURBQStCLHNDQUFBO0lBSXZCLGVBQTJDO0lBQTNDLHNEQUEyQzs7O0lBZi9ELCtCQUdRO0lBQUEseUZBR3NDO0lBR3RDLDZGQUtRO0lBU2hCLGlCQUFVOzs7SUFuQkUsZUFBaUM7SUFBakMsb0RBQWlDO0lBTWpDLGVBQXlEO0lBQXpELGdGQUF5RDs7O0lBZ0J6RSxvQ0FFYztJQUFBLFlBQXVDO0lBQUEsaUJBQVc7OztJQUFsRCxlQUF1QztJQUF2Qyx5REFBdUM7OztJQUVyRCxvQ0FFYztJQUFBLFlBQXVDO0lBQUEsaUJBQVc7OztJQUFsRCxlQUF1QztJQUF2Qyx5REFBdUM7OztJQUVyRCxnQ0FFYztJQUFBLFlBQXVDO0lBQUEsaUJBQU87OztJQUE5QyxlQUF1QztJQUF2QyxrRUFBdUM7OztJQUVyRCxnQ0FFYztJQUFBLFlBQWlDO0lBQUEsaUJBQU87OztJQUF4QyxlQUFpQztJQUFqQyxtREFBaUM7OztJQUUvQyxvQ0FFa0I7SUFBQSxZQUFvQztJQUFBLGlCQUFXOzs7SUFBL0MsZUFBb0M7SUFBcEMsc0RBQW9DOzs7SUFFdEQsb0NBRWdCO0lBQUEsWUFBcUM7SUFBQSxpQkFBVzs7O0lBQWhELGVBQXFDO0lBQXJDLHVEQUFxQzs7QUQ3RnJFO0lBa0JFO1FBQUEsaUJBQWdCO1FBSFQsYUFBUSxHQUFRLGNBQU8sQ0FBQyxDQUFDO1FBQ3pCLGNBQVMsR0FBUSxjQUFPLENBQUMsQ0FBQztRQUlqQyxzQkFBaUIsR0FBRyxVQUFDLE1BQWlCO1lBQ3BDLDZCQUE2QjtZQUM3QixLQUFJLENBQUMsTUFBTSxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUMsS0FBSSxDQUFDLFFBQVEsQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLGVBQWUsRUFBRSxDQUFDLENBQUM7WUFDN0MsS0FBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ25CLENBQUMsQ0FBQztRQUVGLFdBQU0sR0FBRztZQUNQLEtBQUksQ0FBQyxRQUFRLENBQUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxlQUFlLEVBQUUsQ0FBQyxDQUFDO1lBQzdDLEtBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUNuQixDQUFDLENBQUM7SUFaYSxDQUFDO0lBY2hCLDBDQUFnQixHQUFoQixVQUFpQixFQUFFO1FBQ2pCLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO0lBQ3JCLENBQUM7SUFFRCwyQ0FBaUIsR0FBakIsVUFBa0IsRUFBRTtRQUNsQixJQUFJLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztJQUN0QixDQUFDO0lBRUQsb0NBQVUsR0FBVixVQUFXLEtBQUs7UUFDZCxJQUFJLEtBQUssRUFBRTtZQUNULElBQUksQ0FBQyxNQUFNLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3BDO0lBQ0gsQ0FBQztJQUVELGtDQUFRLEdBQVI7UUFDRSx1Q0FBdUM7SUFDekMsQ0FBQztJQUVELHFDQUFXLEdBQVg7UUFDRSw0Q0FBNEM7SUFDOUMsQ0FBQztrRkF4Q1UsZUFBZTt3REFBZixlQUFlLDhGQVJmO2dCQUNUO29CQUNFLE9BQU8sRUFBRSxpQkFBaUI7b0JBQzFCLEtBQUssRUFBRSxJQUFJO29CQUNYLFdBQVcsRUFBRSxVQUFVLENBQUMsY0FBTSxPQUFBLGVBQWUsRUFBZixDQUFlLENBQUM7aUJBQy9DO2FBQ0Y7O1lDT0gsa0NBR1E7WUFBQSx5Q0FJUTtZQUFBLDRFQUN1QztZQUd2Qyx3Q0FTSTtZQUpBLHlLQUFtQiwwQkFBaUMsSUFBQyxpRkFDN0MsWUFBYSxJQURnQztZQUlyRCw4RkFFUTtZQU1SLHdFQUdRO1lBcUJaLGlCQUFhO1lBRWIsMEVBRWM7WUFFZCwwRUFFYztZQUVkLGtFQUVjO1lBRWQsb0VBRWM7WUFFZCw0RUFFa0I7WUFFbEIsNEVBRWdCO1lBRXhCLGlCQUFpQjtZQUV6QixpQkFBVTs7WUE1RUUsZUFBMEM7WUFBMUMsdURBQTBDLG1FQUFBO1lBSWxDLGVBQWtDO1lBQWxDLGdEQUFrQztZQUlsQyxlQUF1QztZQUF2QyxvREFBdUMsc0NBQUEsK0NBQUEsc0NBQUE7WUFTbkMsZUFBMkM7WUFBM0MseURBQTJDO1lBUTNDLGVBQXlEO1lBQXpELHNEQUF5RDtZQTBCN0QsZUFBeUM7WUFBekMsdURBQXlDO1lBSXpDLGVBQXlDO1lBQXpDLHVEQUF5QztZQUl6QyxlQUFtQztZQUFuQyxpREFBbUM7WUFJbkMsZUFBbUM7WUFBbkMsaURBQW1DO1lBSW5DLGVBQXNDO1lBQXRDLG9EQUFzQztZQUl0QyxlQUF1QztZQUF2QyxxREFBdUM7OzBCRGpHM0Q7Q0EwREMsQUFyREQsSUFxREM7U0F6Q1ksZUFBZTtrREFBZixlQUFlO2NBWjNCLFNBQVM7ZUFBQztnQkFDVCxRQUFRLEVBQUUsWUFBWTtnQkFDdEIsV0FBVyxFQUFFLHlCQUF5QjtnQkFDdEMsU0FBUyxFQUFFLENBQUMsd0JBQXdCLENBQUM7Z0JBQ3JDLFNBQVMsRUFBRTtvQkFDVDt3QkFDRSxPQUFPLEVBQUUsaUJBQWlCO3dCQUMxQixLQUFLLEVBQUUsSUFBSTt3QkFDWCxXQUFXLEVBQUUsVUFBVSxDQUFDLGNBQU0sT0FBQSxlQUFlLEVBQWYsQ0FBZSxDQUFDO3FCQUMvQztpQkFDRjthQUNGOztrQkFFRSxLQUFLIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIGZvcndhcmRSZWYsIElucHV0LCBPbkRlc3Ryb3kgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE5HX1ZBTFVFX0FDQ0VTU09SLCBOZ0Zvcm0sIEZvcm1Db250cm9sIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuaW1wb3J0IHsgU2VsZWN0IH0gZnJvbSAnLi4vLi4vbW9kZWwvc3RydWN0dXJhbC9jb21wb3NpdGUvZm9ybS9zZWxlY3Qvc2VsZWN0Lm1vZGVsJztcbmltcG9ydCB7IE1hdFNlbGVjdCB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL3NlbGVjdCc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2FwcC1zZWxlY3QnLFxuICB0ZW1wbGF0ZVVybDogJy4vc2VsZWN0LmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vc2VsZWN0LmNvbXBvbmVudC5jc3MnXSxcbiAgcHJvdmlkZXJzOiBbXG4gICAge1xuICAgICAgcHJvdmlkZTogTkdfVkFMVUVfQUNDRVNTT1IsXG4gICAgICBtdWx0aTogdHJ1ZSxcbiAgICAgIHVzZUV4aXN0aW5nOiBmb3J3YXJkUmVmKCgpID0+IFNlbGVjdENvbXBvbmVudCksXG4gICAgfSxcbiAgXSxcbn0pXG5leHBvcnQgY2xhc3MgU2VsZWN0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xuICBASW5wdXQoKSBzZWxlY3Q6IFNlbGVjdDtcblxuICBwdWJsaWMgb25DaGFuZ2U6IGFueSA9ICgpID0+IHt9O1xuICBwdWJsaWMgb25Ub3VjaGVkOiBhbnkgPSAoKSA9PiB7fTtcblxuICBjb25zdHJ1Y3RvcigpIHt9XG5cbiAgb25TZWxlY3Rpb25DaGFuZ2UgPSAoc2VsZWN0OiBNYXRTZWxlY3QpID0+IHtcbiAgICAvLyBjb25zb2xlLmxvZyhzZWxlY3QudmFsdWUpO1xuICAgIHRoaXMuc2VsZWN0LnNldERlZmF1bHRWYWx1ZShzZWxlY3QudmFsdWUpO1xuICAgIHRoaXMub25DaGFuZ2UodGhpcy5zZWxlY3QuZ2V0RGVmYXVsdFZhbHVlKCkpO1xuICAgIHRoaXMub25Ub3VjaGVkKCk7XG4gIH07XG5cbiAgb25CbHVyID0gKCkgPT4ge1xuICAgIHRoaXMub25DaGFuZ2UodGhpcy5zZWxlY3QuZ2V0RGVmYXVsdFZhbHVlKCkpO1xuICAgIHRoaXMub25Ub3VjaGVkKCk7XG4gIH07XG5cbiAgcmVnaXN0ZXJPbkNoYW5nZShmbikge1xuICAgIHRoaXMub25DaGFuZ2UgPSBmbjtcbiAgfVxuXG4gIHJlZ2lzdGVyT25Ub3VjaGVkKGZuKSB7XG4gICAgdGhpcy5vblRvdWNoZWQgPSBmbjtcbiAgfVxuXG4gIHdyaXRlVmFsdWUodmFsdWUpIHtcbiAgICBpZiAodmFsdWUpIHtcbiAgICAgIHRoaXMuc2VsZWN0LnNldERlZmF1bHRWYWx1ZSh2YWx1ZSk7XG4gICAgfVxuICB9XG5cbiAgbmdPbkluaXQoKTogdm9pZCB7XG4gICAgLy8gY29uc29sZS5sb2coJ1NlbGVjdENvbXBvbmVudCBpbml0Jyk7XG4gIH1cblxuICBuZ09uRGVzdHJveSgpOiB2b2lkIHtcbiAgICAvLyBjb25zb2xlLmxvZygnU2VsZWN0Q29tcG9uZW50IGRlc3Ryb3llZCcpO1xuICB9XG59XG4iLCI8c3R5bGU+XG5cbiAgICAuY3Vyc29yIHtcbiAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIH1cblxuICAgIGlucHV0W3R5cGU9XCJmaWxlXCJdIHtcbiAgICAgICAgZGlzcGxheTogbm9uZTtcbiAgICB9XG5cbiAgICBoMyB7XG4gICAgICAgIGZvbnQtc2l6ZTogaW5oZXJpdDtcbiAgICAgICAgbWFyZ2luOiAwO1xuICAgICAgICBmb250LXdlaWdodDogaW5oZXJpdDtcbiAgICB9XG5cbiAgICBtYXQtZm9ybS1maWVsZCB7XG4gICAgICAgIG1pbi13aWR0aDogMjUwcHg7XG4gICAgfVxuXG48L3N0eWxlPlxuICAgIFxuPHNlY3Rpb25cbiAgICBmeExheW91dEFsaWduPVwiY2VudGVyIGNlbnRlclwiPlxuXG4gICAgICAgIDxtYXQtZm9ybS1maWVsZCBcbiAgICAgICAgICAgIFthcHBlYXJhbmNlXT1cInRoaXMuc2VsZWN0LmdldEFwcGVhcmFuY2UoKVwiXG4gICAgICAgICAgICBbaGlkZVJlcXVpcmVkTWFya2VyXT1cInRoaXMuc2VsZWN0LmdldFJlcXVpcmVkKCkuaGlkZVJlcXVpcmVkTWFya2VyXCI+XG5cbiAgICAgICAgICAgICAgICA8bWF0LWxhYmVsXG4gICAgICAgICAgICAgICAgICAgICpuZ0lmPVwidGhpcy5zZWxlY3QuZ2V0TGFiZWxOYW1lKClcIj57eyB0aGlzLnNlbGVjdC5nZXRMYWJlbE5hbWUoKSB9fTwvbWF0LWxhYmVsPlxuXG5cbiAgICAgICAgICAgICAgICA8bWF0LXNlbGVjdFxuICAgICAgICAgICAgICAgICAgICBbdmFsdWVdPVwidGhpcy5zZWxlY3QuZ2V0RGVmYXVsdFZhbHVlKClcIlxuICAgICAgICAgICAgICAgICAgICBbbXVsdGlwbGVdPVwidGhpcy5zZWxlY3QuZ2V0TXVsdGlwbGUoKVwiXG4gICAgICAgICAgICAgICAgICAgIFtyZXF1aXJlZF09XCJ0aGlzLnNlbGVjdC5nZXRSZXF1aXJlZCgpLnJlcXVpcmVkXCJcbiAgICAgICAgICAgICAgICAgICAgW2Rpc2FibGVkXT1cInRoaXMuc2VsZWN0LmdldERpc2FibGVkKClcIlxuICAgICAgICAgICAgICAgICAgICAoc2VsZWN0aW9uQ2hhbmdlKT1cInRoaXMub25TZWxlY3Rpb25DaGFuZ2UobWF0U2VsZWN0KVwiXG4gICAgICAgICAgICAgICAgICAgIChibHVyKT1cInRoaXMub25CbHVyKClcIlxuICAgICAgICAgICAgICAgICAgICAjbWF0U2VsZWN0PlxuXG4gICAgICAgICAgICAgICAgICAgIDxtYXQtc2VsZWN0LXRyaWdnZXJcbiAgICAgICAgICAgICAgICAgICAgICAgICpuZ0lmPVwidGhpcy5zZWxlY3QuZ2V0TWF0U2VsZWN0VHJpZ2dlck9uKClcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7e3RoaXMuc2VsZWN0LmdldERlZmF1bHRWYWx1ZSgpID8gdGhpcy5zZWxlY3QuZ2V0RGVmYXVsdFZhbHVlKClbMF0gOiAnJ319XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gKm5nSWY9XCJ0aGlzLnNlbGVjdC5nZXREZWZhdWx0VmFsdWUoKT8ubGVuZ3RoID4gMVwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICgre3t0aGlzLnNlbGVjdC5nZXREZWZhdWx0VmFsdWUoKS5sZW5ndGggLSAxfX0ge3t0aGlzLnNlbGVjdC5nZXREZWZhdWx0VmFsdWUoKT8ubGVuZ3RoID09PSAyID8gJ290aGVyJyA6ICdvdGhlcnMnfX0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICA8L21hdC1zZWxlY3QtdHJpZ2dlcj5cblxuICAgICAgICAgICAgICAgICAgICA8c2VjdGlvblxuICAgICAgICAgICAgICAgICAgICAgICAgKm5nRm9yPVwibGV0IG9wdGlvblZhbHVlIG9mIHRoaXMuc2VsZWN0LmdldE9wdGlvblZhbHVlcygpXCI+XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bWF0LW9wdGlvblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAqbmdJZj1cIiFvcHRpb25WYWx1ZS5vcHRpb25WYWx1ZXNcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbdmFsdWVdPVwib3B0aW9uVmFsdWUudmFsdWVcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbZGlzYWJsZWRdPVwib3B0aW9uVmFsdWUuZGlzYWJsZWRcIj57eyBvcHRpb25WYWx1ZS50ZXh0VG9TaG93IH19XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9tYXQtb3B0aW9uPlxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPG1hdC1vcHRncm91cCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKm5nSWY9XCJvcHRpb25WYWx1ZS5vcHRpb25WYWx1ZXMgJiYgb3B0aW9uVmFsdWUubGFiZWxOYW1lXCIgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtsYWJlbF09XCJvcHRpb25WYWx1ZS5sYWJlbE5hbWVcIiBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgW2Rpc2FibGVkXT1cIm9wdGlvblZhbHVlLmRpc2FibGVkXCI+XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxtYXQtb3B0aW9uIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICpuZ0Zvcj1cImxldCBvViBvZiBvcHRpb25WYWx1ZS5vcHRpb25WYWx1ZXNcIiBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbdmFsdWVdPVwib1YudmFsdWVcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtkaXNhYmxlZF09XCJvVi5kaXNhYmxlZFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7eyBvVi50ZXh0VG9TaG93IH19XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L21hdC1vcHRpb24+XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L21hdC1vcHRncm91cD5cblxuICAgICAgICAgICAgICAgICAgICA8L3NlY3Rpb24+XG4gICAgICAgICAgICAgICAgPC9tYXQtc2VsZWN0PlxuXG4gICAgICAgICAgICAgICAgPG1hdC1pY29uIFxuICAgICAgICAgICAgICAgICAgICAqbmdJZj1cInRoaXMuc2VsZWN0LmdldE1hdFByZWZpeEltZ1RleHQoKVwiXG4gICAgICAgICAgICAgICAgICAgIG1hdFByZWZpeD57eyB0aGlzLnNlbGVjdC5nZXRNYXRQcmVmaXhJbWdUZXh0KCkgfX08L21hdC1pY29uPlxuXG4gICAgICAgICAgICAgICAgPG1hdC1pY29uIFxuICAgICAgICAgICAgICAgICAgICAqbmdJZj1cInRoaXMuc2VsZWN0LmdldE1hdFN1ZmZpeEltZ1RleHQoKVwiXG4gICAgICAgICAgICAgICAgICAgIG1hdFN1ZmZpeD57eyB0aGlzLnNlbGVjdC5nZXRNYXRTdWZmaXhJbWdUZXh0KCkgfX08L21hdC1pY29uPlxuXG4gICAgICAgICAgICAgICAgPHNwYW4gXG4gICAgICAgICAgICAgICAgICAgICpuZ0lmPVwidGhpcy5zZWxlY3QuZ2V0VGV4dFByZWZpeCgpXCJcbiAgICAgICAgICAgICAgICAgICAgbWF0UHJlZml4Pnt7IHRoaXMuc2VsZWN0LmdldFRleHRQcmVmaXgoKSB9fSZuYnNwOzwvc3Bhbj5cblxuICAgICAgICAgICAgICAgIDxzcGFuIFxuICAgICAgICAgICAgICAgICAgICAqbmdJZj1cInRoaXMuc2VsZWN0LmdldFRleHRTdWZmaXgoKVwiXG4gICAgICAgICAgICAgICAgICAgIG1hdFN1ZmZpeD57eyB0aGlzLnNlbGVjdC5nZXRUZXh0U3VmZml4KCkgfX08L3NwYW4+XG5cbiAgICAgICAgICAgICAgICA8bWF0LWhpbnQgXG4gICAgICAgICAgICAgICAgICAgICpuZ0lmPVwidGhpcy5zZWxlY3QuZ2V0TGVmdEhpbnRMYWJlbCgpXCJcbiAgICAgICAgICAgICAgICAgICAgYWxpZ249XCJzdGFydFwiPnt7IHRoaXMuc2VsZWN0LmdldExlZnRIaW50TGFiZWwoKSB9fTwvbWF0LWhpbnQ+XG5cbiAgICAgICAgICAgICAgICA8bWF0LWhpbnRcbiAgICAgICAgICAgICAgICAgICAgKm5nSWY9XCJ0aGlzLnNlbGVjdC5nZXRSaWdodEhpbnRMYWJlbCgpXCJcbiAgICAgICAgICAgICAgICAgICAgYWxpZ249XCJlbmRcIj57eyB0aGlzLnNlbGVjdC5nZXRSaWdodEhpbnRMYWJlbCgpIH19PC9tYXQtaGludD5cblxuICAgICAgICA8L21hdC1mb3JtLWZpZWxkPlxuXG48L3NlY3Rpb24+Il19