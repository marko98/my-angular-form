import { Component, forwardRef, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import * as i0 from "@angular/core";
import * as i1 from "@angular/flex-layout/flex";
import * as i2 from "@angular/material/slider";
var SliderComponent = /** @class */ (function () {
    function SliderComponent() {
        var _this = this;
        this.onChange = function () { };
        this.onTouched = function () { };
        this.onValueChange = function (slider) {
            // console.log(slider);
            _this.slider.setDefaultValue(slider.value);
            _this.onChange(_this.slider.getDefaultValue());
            _this.onTouched();
        };
        this.onBlur = function () {
            _this.onChange(_this.slider.getDefaultValue());
            _this.onTouched();
        };
    }
    SliderComponent.prototype.registerOnChange = function (fn) {
        this.onChange = fn;
    };
    SliderComponent.prototype.registerOnTouched = function (fn) {
        this.onTouched = fn;
    };
    SliderComponent.prototype.writeValue = function (value) {
        if (value) {
            this.slider.setDefaultValue(value);
        }
    };
    SliderComponent.prototype.ngOnInit = function () {
        // console.log('SliderComponent init');
    };
    SliderComponent.prototype.ngOnDestroy = function () {
        // console.log('SliderComponent destroyed');
    };
    SliderComponent.ɵfac = function SliderComponent_Factory(t) { return new (t || SliderComponent)(); };
    SliderComponent.ɵcmp = i0.ɵɵdefineComponent({ type: SliderComponent, selectors: [["app-slider"]], inputs: { slider: "slider" }, features: [i0.ɵɵProvidersFeature([
                {
                    provide: NG_VALUE_ACCESSOR,
                    multi: true,
                    useExisting: forwardRef(function () { return SliderComponent; }),
                },
            ])], decls: 3, vars: 9, consts: [["fxLayoutAlign", "center center"], [3, "value", "min", "max", "disabled", "invert", "step", "tickInterval", "thumbLabel", "vertical", "change", "blur"], ["matSlider", ""]], template: function SliderComponent_Template(rf, ctx) { if (rf & 1) {
            var _r1 = i0.ɵɵgetCurrentView();
            i0.ɵɵelementStart(0, "section", 0);
            i0.ɵɵelementStart(1, "mat-slider", 1, 2);
            i0.ɵɵlistener("change", function SliderComponent_Template_mat_slider_change_1_listener() { i0.ɵɵrestoreView(_r1); var _r0 = i0.ɵɵreference(2); return ctx.onValueChange(_r0); })("blur", function SliderComponent_Template_mat_slider_blur_1_listener() { return ctx.onBlur(); });
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        } if (rf & 2) {
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("value", ctx.slider.getDefaultValue())("min", ctx.slider.getMinValue() ? ctx.slider.getMinValue() : undefined)("max", ctx.slider.getMaxValue() ? ctx.slider.getMaxValue() : undefined)("disabled", ctx.slider.getDisabled())("invert", ctx.slider.getInvert())("step", ctx.slider.getStep())("tickInterval", ctx.slider.getTickInterval())("thumbLabel", ctx.slider.getThumbLabel())("vertical", ctx.slider.getVertical());
        } }, directives: [i1.DefaultLayoutAlignDirective, i2.MatSlider], styles: ["", "mat-slider[_ngcontent-%COMP%] {\n        min-width: 250px;\n    }\n\n    section[_ngcontent-%COMP%] {\n        min-height: 70px;\n    }"] });
    return SliderComponent;
}());
export { SliderComponent };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(SliderComponent, [{
        type: Component,
        args: [{
                selector: 'app-slider',
                templateUrl: './slider.component.html',
                styleUrls: ['./slider.component.css'],
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        multi: true,
                        useExisting: forwardRef(function () { return SliderComponent; }),
                    },
                ],
            }]
    }], function () { return []; }, { slider: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2xpZGVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215LWFuZ3VsYXItZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvdWkvc2xpZGVyL3NsaWRlci5jb21wb25lbnQudHMiLCJsaWIvc2hhcmVkL3VpL3NsaWRlci9zbGlkZXIuY29tcG9uZW50Lmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBcUIsVUFBVSxFQUFFLEtBQUssRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNoRixPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQzs7OztBQUluRDtJQWtCRTtRQUFBLGlCQUFnQjtRQUhULGFBQVEsR0FBUSxjQUFPLENBQUMsQ0FBQztRQUN6QixjQUFTLEdBQVEsY0FBTyxDQUFDLENBQUM7UUFJakMsa0JBQWEsR0FBRyxVQUFDLE1BQWlCO1lBQ2hDLHVCQUF1QjtZQUN2QixLQUFJLENBQUMsTUFBTSxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUMsS0FBSSxDQUFDLFFBQVEsQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLGVBQWUsRUFBRSxDQUFDLENBQUM7WUFDN0MsS0FBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ25CLENBQUMsQ0FBQztRQUVGLFdBQU0sR0FBRztZQUNQLEtBQUksQ0FBQyxRQUFRLENBQUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxlQUFlLEVBQUUsQ0FBQyxDQUFDO1lBQzdDLEtBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUNuQixDQUFDLENBQUM7SUFaYSxDQUFDO0lBY2hCLDBDQUFnQixHQUFoQixVQUFpQixFQUFFO1FBQ2pCLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO0lBQ3JCLENBQUM7SUFFRCwyQ0FBaUIsR0FBakIsVUFBa0IsRUFBRTtRQUNsQixJQUFJLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztJQUN0QixDQUFDO0lBRUQsb0NBQVUsR0FBVixVQUFXLEtBQUs7UUFDZCxJQUFJLEtBQUssRUFBRTtZQUNULElBQUksQ0FBQyxNQUFNLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3BDO0lBQ0gsQ0FBQztJQUVELGtDQUFRLEdBQVI7UUFDRSx1Q0FBdUM7SUFDekMsQ0FBQztJQUVELHFDQUFXLEdBQVg7UUFDRSw0Q0FBNEM7SUFDOUMsQ0FBQztrRkF4Q1UsZUFBZTt3REFBZixlQUFlLDhGQVJmO2dCQUNUO29CQUNFLE9BQU8sRUFBRSxpQkFBaUI7b0JBQzFCLEtBQUssRUFBRSxJQUFJO29CQUNYLFdBQVcsRUFBRSxVQUFVLENBQUMsY0FBTSxPQUFBLGVBQWUsRUFBZixDQUFlLENBQUM7aUJBQy9DO2FBQ0Y7O1lDSEgsa0NBR1E7WUFBQSx3Q0FZNEI7WUFWeEIsc0pBQVUsc0JBQTZCLElBQUMsaUZBQ2hDLFlBQWEsSUFEbUI7WUFVN0IsaUJBQWE7WUFFcEMsaUJBQVU7O1lBYkUsZUFBdUM7WUFBdkMsb0RBQXVDLHdFQUFBLHdFQUFBLHNDQUFBLGtDQUFBLDhCQUFBLDhDQUFBLDBDQUFBLHNDQUFBOzswQkRoQm5EO0NBMERDLEFBckRELElBcURDO1NBekNZLGVBQWU7a0RBQWYsZUFBZTtjQVozQixTQUFTO2VBQUM7Z0JBQ1QsUUFBUSxFQUFFLFlBQVk7Z0JBQ3RCLFdBQVcsRUFBRSx5QkFBeUI7Z0JBQ3RDLFNBQVMsRUFBRSxDQUFDLHdCQUF3QixDQUFDO2dCQUNyQyxTQUFTLEVBQUU7b0JBQ1Q7d0JBQ0UsT0FBTyxFQUFFLGlCQUFpQjt3QkFDMUIsS0FBSyxFQUFFLElBQUk7d0JBQ1gsV0FBVyxFQUFFLFVBQVUsQ0FBQyxjQUFNLE9BQUEsZUFBZSxFQUFmLENBQWUsQ0FBQztxQkFDL0M7aUJBQ0Y7YUFDRjs7a0JBRUUsS0FBSyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBPbkRlc3Ryb3ksIGZvcndhcmRSZWYsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBOR19WQUxVRV9BQ0NFU1NPUiB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7IFNsaWRlciB9IGZyb20gJy4uLy4uL21vZGVsL3N0cnVjdHVyYWwvY29tcG9zaXRlL2Zvcm0vc2xpZGVyL3NsaWRlci5tb2RlbCc7XG5pbXBvcnQgeyBNYXRTbGlkZXIgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9zbGlkZXInO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdhcHAtc2xpZGVyJyxcbiAgdGVtcGxhdGVVcmw6ICcuL3NsaWRlci5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL3NsaWRlci5jb21wb25lbnQuY3NzJ10sXG4gIHByb3ZpZGVyczogW1xuICAgIHtcbiAgICAgIHByb3ZpZGU6IE5HX1ZBTFVFX0FDQ0VTU09SLFxuICAgICAgbXVsdGk6IHRydWUsXG4gICAgICB1c2VFeGlzdGluZzogZm9yd2FyZFJlZigoKSA9PiBTbGlkZXJDb21wb25lbnQpLFxuICAgIH0sXG4gIF0sXG59KVxuZXhwb3J0IGNsYXNzIFNsaWRlckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcbiAgQElucHV0KCkgc2xpZGVyOiBTbGlkZXI7XG5cbiAgcHVibGljIG9uQ2hhbmdlOiBhbnkgPSAoKSA9PiB7fTtcbiAgcHVibGljIG9uVG91Y2hlZDogYW55ID0gKCkgPT4ge307XG5cbiAgY29uc3RydWN0b3IoKSB7fVxuXG4gIG9uVmFsdWVDaGFuZ2UgPSAoc2xpZGVyOiBNYXRTbGlkZXIpOiB2b2lkID0+IHtcbiAgICAvLyBjb25zb2xlLmxvZyhzbGlkZXIpO1xuICAgIHRoaXMuc2xpZGVyLnNldERlZmF1bHRWYWx1ZShzbGlkZXIudmFsdWUpO1xuICAgIHRoaXMub25DaGFuZ2UodGhpcy5zbGlkZXIuZ2V0RGVmYXVsdFZhbHVlKCkpO1xuICAgIHRoaXMub25Ub3VjaGVkKCk7XG4gIH07XG5cbiAgb25CbHVyID0gKCk6IHZvaWQgPT4ge1xuICAgIHRoaXMub25DaGFuZ2UodGhpcy5zbGlkZXIuZ2V0RGVmYXVsdFZhbHVlKCkpO1xuICAgIHRoaXMub25Ub3VjaGVkKCk7XG4gIH07XG5cbiAgcmVnaXN0ZXJPbkNoYW5nZShmbikge1xuICAgIHRoaXMub25DaGFuZ2UgPSBmbjtcbiAgfVxuXG4gIHJlZ2lzdGVyT25Ub3VjaGVkKGZuKSB7XG4gICAgdGhpcy5vblRvdWNoZWQgPSBmbjtcbiAgfVxuXG4gIHdyaXRlVmFsdWUodmFsdWUpIHtcbiAgICBpZiAodmFsdWUpIHtcbiAgICAgIHRoaXMuc2xpZGVyLnNldERlZmF1bHRWYWx1ZSh2YWx1ZSk7XG4gICAgfVxuICB9XG5cbiAgbmdPbkluaXQoKTogdm9pZCB7XG4gICAgLy8gY29uc29sZS5sb2coJ1NsaWRlckNvbXBvbmVudCBpbml0Jyk7XG4gIH1cblxuICBuZ09uRGVzdHJveSgpOiB2b2lkIHtcbiAgICAvLyBjb25zb2xlLmxvZygnU2xpZGVyQ29tcG9uZW50IGRlc3Ryb3llZCcpO1xuICB9XG59XG4iLCI8c3R5bGU+XG5cbiAgICBtYXQtc2xpZGVyIHtcbiAgICAgICAgbWluLXdpZHRoOiAyNTBweDtcbiAgICB9XG5cbiAgICBzZWN0aW9uIHtcbiAgICAgICAgbWluLWhlaWdodDogNzBweDtcbiAgICB9XG5cbjwvc3R5bGU+XG5cbjxzZWN0aW9uXG4gICAgZnhMYXlvdXRBbGlnbj1cImNlbnRlciBjZW50ZXJcIj5cblxuICAgICAgICA8bWF0LXNsaWRlclxuICAgICAgICAgICAgW3ZhbHVlXT1cInRoaXMuc2xpZGVyLmdldERlZmF1bHRWYWx1ZSgpXCJcbiAgICAgICAgICAgIChjaGFuZ2UpPVwidGhpcy5vblZhbHVlQ2hhbmdlKG1hdFNsaWRlcilcIlxuICAgICAgICAgICAgKGJsdXIpPVwidGhpcy5vbkJsdXIoKVwiXG4gICAgICAgICAgICBbbWluXT1cInRoaXMuc2xpZGVyLmdldE1pblZhbHVlKCkgPyB0aGlzLnNsaWRlci5nZXRNaW5WYWx1ZSgpIDogdW5kZWZpbmVkXCJcbiAgICAgICAgICAgIFttYXhdPVwidGhpcy5zbGlkZXIuZ2V0TWF4VmFsdWUoKSA/IHRoaXMuc2xpZGVyLmdldE1heFZhbHVlKCkgOiB1bmRlZmluZWRcIlxuICAgICAgICAgICAgW2Rpc2FibGVkXT1cInRoaXMuc2xpZGVyLmdldERpc2FibGVkKClcIlxuICAgICAgICAgICAgW2ludmVydF09XCJ0aGlzLnNsaWRlci5nZXRJbnZlcnQoKVwiXG4gICAgICAgICAgICBbc3RlcF09XCJ0aGlzLnNsaWRlci5nZXRTdGVwKClcIlxuICAgICAgICAgICAgW3RpY2tJbnRlcnZhbF09XCJ0aGlzLnNsaWRlci5nZXRUaWNrSW50ZXJ2YWwoKVwiXG4gICAgICAgICAgICBbdGh1bWJMYWJlbF09XCJ0aGlzLnNsaWRlci5nZXRUaHVtYkxhYmVsKClcIlxuICAgICAgICAgICAgW3ZlcnRpY2FsXT1cInRoaXMuc2xpZGVyLmdldFZlcnRpY2FsKClcIlxuICAgICAgICAgICAgI21hdFNsaWRlcj48L21hdC1zbGlkZXI+XG5cbjwvc2VjdGlvbj4iXX0=