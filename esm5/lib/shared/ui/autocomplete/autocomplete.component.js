import { Component, forwardRef, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import * as i0 from "@angular/core";
import * as i1 from "@angular/flex-layout/flex";
import * as i2 from "autocomplete";
var AutocompleteComponent = /** @class */ (function () {
    function AutocompleteComponent() {
        this.onChange = function () { };
        this.onTouched = function () { };
    }
    AutocompleteComponent.prototype.registerOnChange = function (fn) {
        this.onChange = fn;
    };
    AutocompleteComponent.prototype.registerOnTouched = function (fn) {
        this.onTouched = fn;
    };
    AutocompleteComponent.prototype.writeValue = function (value) {
        if (value) {
            this.autocomplete.setDefaultValue(value);
        }
    };
    AutocompleteComponent.prototype.ngOnInit = function () {
        // console.log(this.autocomplete);
        var _this = this;
        this.autocomplete.getValueSubject().subscribe(function (optionItem) {
            _this.onChange(optionItem);
            _this.onTouched();
        });
        this.autocompleteInterface = {
            pageDataSource: this.autocomplete.getPageDataSource(),
            criteria: this.autocomplete.getCriteria(),
            inputPlaceHolder: this.autocomplete.getInputPlaceholder(),
            sortingOptions: this.autocomplete.getSortingOptions(),
            disabled: this.autocomplete.getDisabled(),
            appearance: this.autocomplete.getAppearance(),
            valueSubject: this.autocomplete.getValueSubject(),
            value: this.autocomplete.getValue(),
            hiddenSubject: this.autocomplete.getHiddenSubject(),
        };
        // console.log('AutocompleteComponent init');
    };
    AutocompleteComponent.prototype.ngOnDestroy = function () {
        // console.log('AutocompleteComponent destroyed');
    };
    AutocompleteComponent.ɵfac = function AutocompleteComponent_Factory(t) { return new (t || AutocompleteComponent)(); };
    AutocompleteComponent.ɵcmp = i0.ɵɵdefineComponent({ type: AutocompleteComponent, selectors: [["app-autocomplete"]], inputs: { autocomplete: "autocomplete" }, features: [i0.ɵɵProvidersFeature([
                {
                    provide: NG_VALUE_ACCESSOR,
                    multi: true,
                    useExisting: forwardRef(function () { return AutocompleteComponent; }),
                },
            ])], decls: 2, vars: 1, consts: [["fxLayout", "column", "fxLayoutAlign", "center center", "fxLayoutGap", "2vw"], [3, "autocomplete"]], template: function AutocompleteComponent_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵelementStart(0, "section", 0);
            i0.ɵɵelement(1, "lib-autocomplete", 1);
            i0.ɵɵelementEnd();
        } if (rf & 2) {
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("autocomplete", ctx.autocompleteInterface);
        } }, directives: [i1.DefaultLayoutDirective, i1.DefaultLayoutAlignDirective, i1.DefaultLayoutGapDirective, i2.AutocompleteComponent], styles: [""] });
    return AutocompleteComponent;
}());
export { AutocompleteComponent };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(AutocompleteComponent, [{
        type: Component,
        args: [{
                selector: 'app-autocomplete',
                templateUrl: './autocomplete.component.html',
                styleUrls: ['./autocomplete.component.css'],
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        multi: true,
                        useExisting: forwardRef(function () { return AutocompleteComponent; }),
                    },
                ],
            }]
    }], function () { return []; }, { autocomplete: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0b2NvbXBsZXRlLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215LWFuZ3VsYXItZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvdWkvYXV0b2NvbXBsZXRlL2F1dG9jb21wbGV0ZS5jb21wb25lbnQudHMiLCJsaWIvc2hhcmVkL3VpL2F1dG9jb21wbGV0ZS9hdXRvY29tcGxldGUuY29tcG9uZW50Lmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBcUIsVUFBVSxFQUFFLEtBQUssRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNoRixPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQzs7OztBQUluRDtJQW1CRTtRQUxPLGFBQVEsR0FBUSxjQUFPLENBQUMsQ0FBQztRQUN6QixjQUFTLEdBQVEsY0FBTyxDQUFDLENBQUM7SUFJbEIsQ0FBQztJQUVoQixnREFBZ0IsR0FBaEIsVUFBaUIsRUFBRTtRQUNqQixJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztJQUNyQixDQUFDO0lBRUQsaURBQWlCLEdBQWpCLFVBQWtCLEVBQUU7UUFDbEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7SUFDdEIsQ0FBQztJQUVELDBDQUFVLEdBQVYsVUFBVyxLQUFLO1FBQ2QsSUFBSSxLQUFLLEVBQUU7WUFDVCxJQUFJLENBQUMsWUFBWSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUMxQztJQUNILENBQUM7SUFFRCx3Q0FBUSxHQUFSO1FBQ0Usa0NBQWtDO1FBRHBDLGlCQXFCQztRQWxCQyxJQUFJLENBQUMsWUFBWSxDQUFDLGVBQWUsRUFBRSxDQUFDLFNBQVMsQ0FBQyxVQUFDLFVBQXNCO1lBQ25FLEtBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDMUIsS0FBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ25CLENBQUMsQ0FBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLHFCQUFxQixHQUFHO1lBQzNCLGNBQWMsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLGlCQUFpQixFQUFFO1lBQ3JELFFBQVEsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRTtZQUN6QyxnQkFBZ0IsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLG1CQUFtQixFQUFFO1lBQ3pELGNBQWMsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLGlCQUFpQixFQUFFO1lBQ3JELFFBQVEsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRTtZQUN6QyxVQUFVLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxhQUFhLEVBQUU7WUFDN0MsWUFBWSxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsZUFBZSxFQUFFO1lBQ2pELEtBQUssRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsRUFBRTtZQUNuQyxhQUFhLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsRUFBRTtTQUNwRCxDQUFDO1FBRUYsNkNBQTZDO0lBQy9DLENBQUM7SUFFRCwyQ0FBVyxHQUFYO1FBQ0Usa0RBQWtEO0lBQ3BELENBQUM7OEZBaERVLHFCQUFxQjs4REFBckIscUJBQXFCLGdIQVJyQjtnQkFDVDtvQkFDRSxPQUFPLEVBQUUsaUJBQWlCO29CQUMxQixLQUFLLEVBQUUsSUFBSTtvQkFDWCxXQUFXLEVBQUUsVUFBVSxDQUFDLGNBQU0sT0FBQSxxQkFBcUIsRUFBckIsQ0FBcUIsQ0FBQztpQkFDckQ7YUFDRjtZQ2ZILGtDQUtRO1lBQUEsc0NBQ21FO1lBRTNFLGlCQUFVOztZQUZFLGVBQTJDO1lBQTNDLHdEQUEyQzs7Z0NETnZEO0NBa0VDLEFBN0RELElBNkRDO1NBakRZLHFCQUFxQjtrREFBckIscUJBQXFCO2NBWmpDLFNBQVM7ZUFBQztnQkFDVCxRQUFRLEVBQUUsa0JBQWtCO2dCQUM1QixXQUFXLEVBQUUsK0JBQStCO2dCQUM1QyxTQUFTLEVBQUUsQ0FBQyw4QkFBOEIsQ0FBQztnQkFDM0MsU0FBUyxFQUFFO29CQUNUO3dCQUNFLE9BQU8sRUFBRSxpQkFBaUI7d0JBQzFCLEtBQUssRUFBRSxJQUFJO3dCQUNYLFdBQVcsRUFBRSxVQUFVLENBQUMsY0FBTSxPQUFBLHFCQUFxQixFQUFyQixDQUFxQixDQUFDO3FCQUNyRDtpQkFDRjthQUNGOztrQkFFRSxLQUFLIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIE9uRGVzdHJveSwgZm9yd2FyZFJlZiwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE5HX1ZBTFVFX0FDQ0VTU09SIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuaW1wb3J0IHsgQXV0b2NvbXBsZXRlIH0gZnJvbSAnLi4vLi4vbW9kZWwvc3RydWN0dXJhbC9jb21wb3NpdGUvZm9ybS9hdXRvY29tcGxldGUvYXV0b2NvbXBsZXRlLm1vZGVsJztcbmltcG9ydCB7IEF1dG9jb21wbGV0ZUludGVyZmFjZSwgT3B0aW9uSXRlbSB9IGZyb20gJ2F1dG9jb21wbGV0ZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2FwcC1hdXRvY29tcGxldGUnLFxuICB0ZW1wbGF0ZVVybDogJy4vYXV0b2NvbXBsZXRlLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vYXV0b2NvbXBsZXRlLmNvbXBvbmVudC5jc3MnXSxcbiAgcHJvdmlkZXJzOiBbXG4gICAge1xuICAgICAgcHJvdmlkZTogTkdfVkFMVUVfQUNDRVNTT1IsXG4gICAgICBtdWx0aTogdHJ1ZSxcbiAgICAgIHVzZUV4aXN0aW5nOiBmb3J3YXJkUmVmKCgpID0+IEF1dG9jb21wbGV0ZUNvbXBvbmVudCksXG4gICAgfSxcbiAgXSxcbn0pXG5leHBvcnQgY2xhc3MgQXV0b2NvbXBsZXRlQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xuICBASW5wdXQoKSBhdXRvY29tcGxldGU6IEF1dG9jb21wbGV0ZTtcbiAgcHVibGljIG9uQ2hhbmdlOiBhbnkgPSAoKSA9PiB7fTtcbiAgcHVibGljIG9uVG91Y2hlZDogYW55ID0gKCkgPT4ge307XG5cbiAgcHVibGljIGF1dG9jb21wbGV0ZUludGVyZmFjZTogQXV0b2NvbXBsZXRlSW50ZXJmYWNlO1xuXG4gIGNvbnN0cnVjdG9yKCkge31cblxuICByZWdpc3Rlck9uQ2hhbmdlKGZuKSB7XG4gICAgdGhpcy5vbkNoYW5nZSA9IGZuO1xuICB9XG5cbiAgcmVnaXN0ZXJPblRvdWNoZWQoZm4pIHtcbiAgICB0aGlzLm9uVG91Y2hlZCA9IGZuO1xuICB9XG5cbiAgd3JpdGVWYWx1ZSh2YWx1ZSkge1xuICAgIGlmICh2YWx1ZSkge1xuICAgICAgdGhpcy5hdXRvY29tcGxldGUuc2V0RGVmYXVsdFZhbHVlKHZhbHVlKTtcbiAgICB9XG4gIH1cblxuICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgICAvLyBjb25zb2xlLmxvZyh0aGlzLmF1dG9jb21wbGV0ZSk7XG5cbiAgICB0aGlzLmF1dG9jb21wbGV0ZS5nZXRWYWx1ZVN1YmplY3QoKS5zdWJzY3JpYmUoKG9wdGlvbkl0ZW06IE9wdGlvbkl0ZW0pID0+IHtcbiAgICAgIHRoaXMub25DaGFuZ2Uob3B0aW9uSXRlbSk7XG4gICAgICB0aGlzLm9uVG91Y2hlZCgpO1xuICAgIH0pO1xuXG4gICAgdGhpcy5hdXRvY29tcGxldGVJbnRlcmZhY2UgPSB7XG4gICAgICBwYWdlRGF0YVNvdXJjZTogdGhpcy5hdXRvY29tcGxldGUuZ2V0UGFnZURhdGFTb3VyY2UoKSxcbiAgICAgIGNyaXRlcmlhOiB0aGlzLmF1dG9jb21wbGV0ZS5nZXRDcml0ZXJpYSgpLFxuICAgICAgaW5wdXRQbGFjZUhvbGRlcjogdGhpcy5hdXRvY29tcGxldGUuZ2V0SW5wdXRQbGFjZWhvbGRlcigpLFxuICAgICAgc29ydGluZ09wdGlvbnM6IHRoaXMuYXV0b2NvbXBsZXRlLmdldFNvcnRpbmdPcHRpb25zKCksXG4gICAgICBkaXNhYmxlZDogdGhpcy5hdXRvY29tcGxldGUuZ2V0RGlzYWJsZWQoKSxcbiAgICAgIGFwcGVhcmFuY2U6IHRoaXMuYXV0b2NvbXBsZXRlLmdldEFwcGVhcmFuY2UoKSxcbiAgICAgIHZhbHVlU3ViamVjdDogdGhpcy5hdXRvY29tcGxldGUuZ2V0VmFsdWVTdWJqZWN0KCksXG4gICAgICB2YWx1ZTogdGhpcy5hdXRvY29tcGxldGUuZ2V0VmFsdWUoKSxcbiAgICAgIGhpZGRlblN1YmplY3Q6IHRoaXMuYXV0b2NvbXBsZXRlLmdldEhpZGRlblN1YmplY3QoKSxcbiAgICB9O1xuXG4gICAgLy8gY29uc29sZS5sb2coJ0F1dG9jb21wbGV0ZUNvbXBvbmVudCBpbml0Jyk7XG4gIH1cblxuICBuZ09uRGVzdHJveSgpOiB2b2lkIHtcbiAgICAvLyBjb25zb2xlLmxvZygnQXV0b2NvbXBsZXRlQ29tcG9uZW50IGRlc3Ryb3llZCcpO1xuICB9XG59XG4iLCI8c2VjdGlvblxuICAgIGZ4TGF5b3V0PVwiY29sdW1uXCJcbiAgICBmeExheW91dEFsaWduPVwiY2VudGVyIGNlbnRlclwiXG4gICAgZnhMYXlvdXRHYXA9XCIydndcIj5cblxuICAgICAgICA8bGliLWF1dG9jb21wbGV0ZVxuICAgICAgICAgICAgW2F1dG9jb21wbGV0ZV09XCJ0aGlzLmF1dG9jb21wbGV0ZUludGVyZmFjZVwiPjwvbGliLWF1dG9jb21wbGV0ZT5cblxuPC9zZWN0aW9uPlxuIl19