import { Component, forwardRef, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import * as i0 from "@angular/core";
import * as i1 from "@angular/flex-layout/flex";
import * as i2 from "@angular/material/slide-toggle";
var SlideToggleComponent = /** @class */ (function () {
    function SlideToggleComponent() {
        var _this = this;
        this.onChange = function () { };
        this.onTouched = function () { };
        this.onValueChange = function (slideToggle) {
            console.log(slideToggle);
            _this.slideToggle.setDefaultValue(slideToggle.checked);
            _this.onChange(_this.slideToggle.getDefaultValue());
            _this.onTouched();
        };
    }
    SlideToggleComponent.prototype.registerOnChange = function (fn) {
        this.onChange = fn;
    };
    SlideToggleComponent.prototype.registerOnTouched = function (fn) {
        this.onTouched = fn;
    };
    SlideToggleComponent.prototype.writeValue = function (value) {
        if (value) {
            this.slideToggle.setDefaultValue(value);
        }
    };
    SlideToggleComponent.prototype.ngOnInit = function () {
        // console.log('SlideToggleComponent init');
    };
    SlideToggleComponent.prototype.ngOnDestroy = function () {
        // console.log('SlideToggleComponent destroyed');
    };
    SlideToggleComponent.ɵfac = function SlideToggleComponent_Factory(t) { return new (t || SlideToggleComponent)(); };
    SlideToggleComponent.ɵcmp = i0.ɵɵdefineComponent({ type: SlideToggleComponent, selectors: [["app-slide-toggle"]], inputs: { slideToggle: "slideToggle" }, features: [i0.ɵɵProvidersFeature([
                {
                    provide: NG_VALUE_ACCESSOR,
                    multi: true,
                    useExisting: forwardRef(function () { return SlideToggleComponent; }),
                },
            ])], decls: 4, vars: 4, consts: [["fxLayoutAlign", "center center"], [3, "color", "checked", "disabled", "change"], ["matSlideToggle", ""]], template: function SlideToggleComponent_Template(rf, ctx) { if (rf & 1) {
            var _r1 = i0.ɵɵgetCurrentView();
            i0.ɵɵelementStart(0, "section", 0);
            i0.ɵɵelementStart(1, "mat-slide-toggle", 1, 2);
            i0.ɵɵlistener("change", function SlideToggleComponent_Template_mat_slide_toggle_change_1_listener() { i0.ɵɵrestoreView(_r1); var _r0 = i0.ɵɵreference(2); return ctx.onValueChange(_r0); });
            i0.ɵɵtext(3);
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        } if (rf & 2) {
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("color", ctx.slideToggle.getColor())("checked", ctx.slideToggle.getDefaultValue())("disabled", ctx.slideToggle.getDisabled());
            i0.ɵɵadvance(2);
            i0.ɵɵtextInterpolate(ctx.slideToggle.getContext());
        } }, directives: [i1.DefaultLayoutAlignDirective, i2.MatSlideToggle], styles: ["", ""] });
    return SlideToggleComponent;
}());
export { SlideToggleComponent };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(SlideToggleComponent, [{
        type: Component,
        args: [{
                selector: 'app-slide-toggle',
                templateUrl: './slide-toggle.component.html',
                styleUrls: ['./slide-toggle.component.css'],
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        multi: true,
                        useExisting: forwardRef(function () { return SlideToggleComponent; }),
                    },
                ],
            }]
    }], function () { return []; }, { slideToggle: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2xpZGUtdG9nZ2xlLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215LWFuZ3VsYXItZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvdWkvc2xpZGUtdG9nZ2xlL3NsaWRlLXRvZ2dsZS5jb21wb25lbnQudHMiLCJsaWIvc2hhcmVkL3VpL3NsaWRlLXRvZ2dsZS9zbGlkZS10b2dnbGUuY29tcG9uZW50Lmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxVQUFVLEVBQWEsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ2hGLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGdCQUFnQixDQUFDOzs7O0FBSW5EO0lBa0JFO1FBQUEsaUJBQWdCO1FBSFQsYUFBUSxHQUFRLGNBQU8sQ0FBQyxDQUFDO1FBQ3pCLGNBQVMsR0FBUSxjQUFPLENBQUMsQ0FBQztRQUlqQyxrQkFBYSxHQUFHLFVBQUMsV0FBMkI7WUFDMUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUN6QixLQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDdEQsS0FBSSxDQUFDLFFBQVEsQ0FBQyxLQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsRUFBRSxDQUFDLENBQUM7WUFDbEQsS0FBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ25CLENBQUMsQ0FBQztJQVBhLENBQUM7SUFTaEIsK0NBQWdCLEdBQWhCLFVBQWlCLEVBQUU7UUFDakIsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7SUFDckIsQ0FBQztJQUVELGdEQUFpQixHQUFqQixVQUFrQixFQUFFO1FBQ2xCLElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO0lBQ3RCLENBQUM7SUFFRCx5Q0FBVSxHQUFWLFVBQVcsS0FBSztRQUNkLElBQUksS0FBSyxFQUFFO1lBQ1QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDekM7SUFDSCxDQUFDO0lBRUQsdUNBQVEsR0FBUjtRQUNFLDRDQUE0QztJQUM5QyxDQUFDO0lBRUQsMENBQVcsR0FBWDtRQUNFLGlEQUFpRDtJQUNuRCxDQUFDOzRGQW5DVSxvQkFBb0I7NkRBQXBCLG9CQUFvQiw4R0FScEI7Z0JBQ1Q7b0JBQ0UsT0FBTyxFQUFFLGlCQUFpQjtvQkFDMUIsS0FBSyxFQUFFLElBQUk7b0JBQ1gsV0FBVyxFQUFFLFVBQVUsQ0FBQyxjQUFNLE9BQUEsb0JBQW9CLEVBQXBCLENBQW9CLENBQUM7aUJBQ3BEO2FBQ0Y7O1lDVkgsa0NBR1E7WUFBQSw4Q0FLb0I7WUFEaEIsaUtBQVUsc0JBQWtDLElBQUM7WUFDN0IsWUFBaUM7WUFBQSxpQkFBbUI7WUFFaEYsaUJBQVU7O1lBTkUsZUFBcUM7WUFBckMsa0RBQXFDLDhDQUFBLDJDQUFBO1lBSXJCLGVBQWlDO1lBQWpDLGtEQUFpQzs7K0JEYjdEO0NBcURDLEFBaERELElBZ0RDO1NBcENZLG9CQUFvQjtrREFBcEIsb0JBQW9CO2NBWmhDLFNBQVM7ZUFBQztnQkFDVCxRQUFRLEVBQUUsa0JBQWtCO2dCQUM1QixXQUFXLEVBQUUsK0JBQStCO2dCQUM1QyxTQUFTLEVBQUUsQ0FBQyw4QkFBOEIsQ0FBQztnQkFDM0MsU0FBUyxFQUFFO29CQUNUO3dCQUNFLE9BQU8sRUFBRSxpQkFBaUI7d0JBQzFCLEtBQUssRUFBRSxJQUFJO3dCQUNYLFdBQVcsRUFBRSxVQUFVLENBQUMsY0FBTSxPQUFBLG9CQUFvQixFQUFwQixDQUFvQixDQUFDO3FCQUNwRDtpQkFDRjthQUNGOztrQkFFRSxLQUFLIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIGZvcndhcmRSZWYsIE9uRGVzdHJveSwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE5HX1ZBTFVFX0FDQ0VTU09SIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuaW1wb3J0IHsgTWF0U2xpZGVUb2dnbGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9zbGlkZS10b2dnbGUnO1xuaW1wb3J0IHsgU2xpZGVUb2dnbGUgfSBmcm9tICcuLi8uLi9tb2RlbC9zdHJ1Y3R1cmFsL2NvbXBvc2l0ZS9mb3JtL3NsaWRlLXRvZ2dsZS9zbGlkZS10b2dnbGUubW9kZWwnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdhcHAtc2xpZGUtdG9nZ2xlJyxcbiAgdGVtcGxhdGVVcmw6ICcuL3NsaWRlLXRvZ2dsZS5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL3NsaWRlLXRvZ2dsZS5jb21wb25lbnQuY3NzJ10sXG4gIHByb3ZpZGVyczogW1xuICAgIHtcbiAgICAgIHByb3ZpZGU6IE5HX1ZBTFVFX0FDQ0VTU09SLFxuICAgICAgbXVsdGk6IHRydWUsXG4gICAgICB1c2VFeGlzdGluZzogZm9yd2FyZFJlZigoKSA9PiBTbGlkZVRvZ2dsZUNvbXBvbmVudCksXG4gICAgfSxcbiAgXSxcbn0pXG5leHBvcnQgY2xhc3MgU2xpZGVUb2dnbGVDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XG4gIEBJbnB1dCgpIHNsaWRlVG9nZ2xlOiBTbGlkZVRvZ2dsZTtcblxuICBwdWJsaWMgb25DaGFuZ2U6IGFueSA9ICgpID0+IHt9O1xuICBwdWJsaWMgb25Ub3VjaGVkOiBhbnkgPSAoKSA9PiB7fTtcblxuICBjb25zdHJ1Y3RvcigpIHt9XG5cbiAgb25WYWx1ZUNoYW5nZSA9IChzbGlkZVRvZ2dsZTogTWF0U2xpZGVUb2dnbGUpOiB2b2lkID0+IHtcbiAgICBjb25zb2xlLmxvZyhzbGlkZVRvZ2dsZSk7XG4gICAgdGhpcy5zbGlkZVRvZ2dsZS5zZXREZWZhdWx0VmFsdWUoc2xpZGVUb2dnbGUuY2hlY2tlZCk7XG4gICAgdGhpcy5vbkNoYW5nZSh0aGlzLnNsaWRlVG9nZ2xlLmdldERlZmF1bHRWYWx1ZSgpKTtcbiAgICB0aGlzLm9uVG91Y2hlZCgpO1xuICB9O1xuXG4gIHJlZ2lzdGVyT25DaGFuZ2UoZm4pIHtcbiAgICB0aGlzLm9uQ2hhbmdlID0gZm47XG4gIH1cblxuICByZWdpc3Rlck9uVG91Y2hlZChmbikge1xuICAgIHRoaXMub25Ub3VjaGVkID0gZm47XG4gIH1cblxuICB3cml0ZVZhbHVlKHZhbHVlKSB7XG4gICAgaWYgKHZhbHVlKSB7XG4gICAgICB0aGlzLnNsaWRlVG9nZ2xlLnNldERlZmF1bHRWYWx1ZSh2YWx1ZSk7XG4gICAgfVxuICB9XG5cbiAgbmdPbkluaXQoKTogdm9pZCB7XG4gICAgLy8gY29uc29sZS5sb2coJ1NsaWRlVG9nZ2xlQ29tcG9uZW50IGluaXQnKTtcbiAgfVxuXG4gIG5nT25EZXN0cm95KCk6IHZvaWQge1xuICAgIC8vIGNvbnNvbGUubG9nKCdTbGlkZVRvZ2dsZUNvbXBvbmVudCBkZXN0cm95ZWQnKTtcbiAgfVxufVxuIiwiPHN0eWxlPlxuXG5cbjwvc3R5bGU+XG5cbjxzZWN0aW9uXG4gICAgZnhMYXlvdXRBbGlnbj1cImNlbnRlciBjZW50ZXJcIj5cblxuICAgICAgICA8bWF0LXNsaWRlLXRvZ2dsZVxuICAgICAgICAgICAgW2NvbG9yXT1cInRoaXMuc2xpZGVUb2dnbGUuZ2V0Q29sb3IoKVwiXG4gICAgICAgICAgICBbY2hlY2tlZF09XCJ0aGlzLnNsaWRlVG9nZ2xlLmdldERlZmF1bHRWYWx1ZSgpXCJcbiAgICAgICAgICAgIFtkaXNhYmxlZF09XCJ0aGlzLnNsaWRlVG9nZ2xlLmdldERpc2FibGVkKClcIlxuICAgICAgICAgICAgKGNoYW5nZSk9XCJ0aGlzLm9uVmFsdWVDaGFuZ2UobWF0U2xpZGVUb2dnbGUpXCJcbiAgICAgICAgICAgICNtYXRTbGlkZVRvZ2dsZT57e3RoaXMuc2xpZGVUb2dnbGUuZ2V0Q29udGV4dCgpfX08L21hdC1zbGlkZS10b2dnbGU+XG5cbjwvc2VjdGlvbj4iXX0=