import { Component, Input, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import * as i0 from "@angular/core";
import * as i1 from "@angular/flex-layout/flex";
import * as i2 from "@angular/common";
import * as i3 from "@angular/material/button";
import * as i4 from "@angular/material/icon";
function ButtonComponent_button_1_Template(rf, ctx) { if (rf & 1) {
    var _r8 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 8);
    i0.ɵɵlistener("click", function ButtonComponent_button_1_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r8); var ctx_r7 = i0.ɵɵnextContext(); return ctx_r7.button.getFunctionToExecute() ? ctx_r7.button.getFunctionToExecute()() : undefined; });
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r0 = i0.ɵɵnextContext();
    i0.ɵɵproperty("type", ctx_r0.button.getButtonType())("disabled", ctx_r0.button.getDisabled())("color", ctx_r0.button.getColor());
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r0.button.getContext());
} }
function ButtonComponent_button_2_Template(rf, ctx) { if (rf & 1) {
    var _r10 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 9);
    i0.ɵɵlistener("click", function ButtonComponent_button_2_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r10); var ctx_r9 = i0.ɵɵnextContext(); return ctx_r9.button.getFunctionToExecute() ? ctx_r9.button.getFunctionToExecute()() : undefined; });
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r1 = i0.ɵɵnextContext();
    i0.ɵɵproperty("type", ctx_r1.button.getButtonType())("disabled", ctx_r1.button.getDisabled())("color", ctx_r1.button.getColor());
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r1.button.getContext());
} }
function ButtonComponent_button_3_Template(rf, ctx) { if (rf & 1) {
    var _r12 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 10);
    i0.ɵɵlistener("click", function ButtonComponent_button_3_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r12); var ctx_r11 = i0.ɵɵnextContext(); return ctx_r11.button.getFunctionToExecute() ? ctx_r11.button.getFunctionToExecute()() : undefined; });
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r2 = i0.ɵɵnextContext();
    i0.ɵɵproperty("type", ctx_r2.button.getButtonType())("disabled", ctx_r2.button.getDisabled())("color", ctx_r2.button.getColor());
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r2.button.getContext());
} }
function ButtonComponent_button_4_Template(rf, ctx) { if (rf & 1) {
    var _r14 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 11);
    i0.ɵɵlistener("click", function ButtonComponent_button_4_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r14); var ctx_r13 = i0.ɵɵnextContext(); return ctx_r13.button.getFunctionToExecute() ? ctx_r13.button.getFunctionToExecute()() : undefined; });
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r3 = i0.ɵɵnextContext();
    i0.ɵɵproperty("type", ctx_r3.button.getButtonType())("disabled", ctx_r3.button.getDisabled())("color", ctx_r3.button.getColor());
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r3.button.getContext());
} }
function ButtonComponent_button_5_Template(rf, ctx) { if (rf & 1) {
    var _r16 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 12);
    i0.ɵɵlistener("click", function ButtonComponent_button_5_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r16); var ctx_r15 = i0.ɵɵnextContext(); return ctx_r15.button.getFunctionToExecute() ? ctx_r15.button.getFunctionToExecute()() : undefined; });
    i0.ɵɵelementStart(1, "mat-icon");
    i0.ɵɵtext(2);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r4 = i0.ɵɵnextContext();
    i0.ɵɵproperty("type", ctx_r4.button.getButtonType())("disabled", ctx_r4.button.getDisabled())("color", ctx_r4.button.getColor());
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(ctx_r4.button.getMatIconString());
} }
function ButtonComponent_button_6_Template(rf, ctx) { if (rf & 1) {
    var _r18 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 13);
    i0.ɵɵlistener("click", function ButtonComponent_button_6_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r18); var ctx_r17 = i0.ɵɵnextContext(); return ctx_r17.button.getFunctionToExecute() ? ctx_r17.button.getFunctionToExecute()() : undefined; });
    i0.ɵɵelementStart(1, "mat-icon");
    i0.ɵɵtext(2);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r5 = i0.ɵɵnextContext();
    i0.ɵɵproperty("type", ctx_r5.button.getButtonType())("disabled", ctx_r5.button.getDisabled())("color", ctx_r5.button.getColor());
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(ctx_r5.button.getMatIconString());
} }
function ButtonComponent_button_7_Template(rf, ctx) { if (rf & 1) {
    var _r20 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 14);
    i0.ɵɵlistener("click", function ButtonComponent_button_7_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r20); var ctx_r19 = i0.ɵɵnextContext(); return ctx_r19.button.getFunctionToExecute() ? ctx_r19.button.getFunctionToExecute()() : undefined; });
    i0.ɵɵelementStart(1, "mat-icon");
    i0.ɵɵtext(2);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r6 = i0.ɵɵnextContext();
    i0.ɵɵproperty("type", ctx_r6.button.getButtonType())("disabled", ctx_r6.button.getDisabled())("color", ctx_r6.button.getColor());
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(ctx_r6.button.getMatIconString());
} }
var ButtonComponent = /** @class */ (function () {
    function ButtonComponent() {
        this.onChange = function () { };
        this.onTouched = function () { };
    }
    ButtonComponent.prototype.registerOnChange = function (fn) {
        this.onChange = fn;
    };
    ButtonComponent.prototype.registerOnTouched = function (fn) {
        this.onTouched = fn;
    };
    ButtonComponent.prototype.writeValue = function (value) {
        if (value) {
            this.button.setDefaultValue(value);
        }
    };
    ButtonComponent.prototype.ngOnInit = function () {
        // console.log('ButtonComponent init');
    };
    ButtonComponent.prototype.ngOnDestroy = function () {
        // console.log('ButtonComponent destroyed');
    };
    ButtonComponent.ɵfac = function ButtonComponent_Factory(t) { return new (t || ButtonComponent)(); };
    ButtonComponent.ɵcmp = i0.ɵɵdefineComponent({ type: ButtonComponent, selectors: [["app-button"]], inputs: { button: "button" }, features: [i0.ɵɵProvidersFeature([
                {
                    provide: NG_VALUE_ACCESSOR,
                    multi: true,
                    useExisting: forwardRef(function () { return ButtonComponent; }),
                },
            ])], decls: 8, vars: 7, consts: [["fxLayoutAlign", "center center"], ["mat-button", "", 3, "type", "disabled", "color", "click", 4, "ngIf"], ["mat-raised-button", "", 3, "type", "disabled", "color", "click", 4, "ngIf"], ["mat-stroked-button", "", 3, "type", "disabled", "color", "click", 4, "ngIf"], ["mat-flat-button", "", 3, "type", "disabled", "color", "click", 4, "ngIf"], ["mat-icon-button", "", 3, "type", "disabled", "color", "click", 4, "ngIf"], ["mat-fab", "", 3, "type", "disabled", "color", "click", 4, "ngIf"], ["mat-mini-fab", "", 3, "type", "disabled", "color", "click", 4, "ngIf"], ["mat-button", "", 3, "type", "disabled", "color", "click"], ["mat-raised-button", "", 3, "type", "disabled", "color", "click"], ["mat-stroked-button", "", 3, "type", "disabled", "color", "click"], ["mat-flat-button", "", 3, "type", "disabled", "color", "click"], ["mat-icon-button", "", 3, "type", "disabled", "color", "click"], ["mat-fab", "", 3, "type", "disabled", "color", "click"], ["mat-mini-fab", "", 3, "type", "disabled", "color", "click"]], template: function ButtonComponent_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵelementStart(0, "section", 0);
            i0.ɵɵtemplate(1, ButtonComponent_button_1_Template, 2, 4, "button", 1);
            i0.ɵɵtemplate(2, ButtonComponent_button_2_Template, 2, 4, "button", 2);
            i0.ɵɵtemplate(3, ButtonComponent_button_3_Template, 2, 4, "button", 3);
            i0.ɵɵtemplate(4, ButtonComponent_button_4_Template, 2, 4, "button", 4);
            i0.ɵɵtemplate(5, ButtonComponent_button_5_Template, 3, 4, "button", 5);
            i0.ɵɵtemplate(6, ButtonComponent_button_6_Template, 3, 4, "button", 6);
            i0.ɵɵtemplate(7, ButtonComponent_button_7_Template, 3, 4, "button", 7);
            i0.ɵɵelementEnd();
        } if (rf & 2) {
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.button.getMatButtonType() == "BASIC");
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.button.getMatButtonType() == "RAISED");
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.button.getMatButtonType() == "STROKED");
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.button.getMatButtonType() == "FLAT");
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.button.getMatButtonType() == "ICON");
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.button.getMatButtonType() == "FAB");
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.button.getMatButtonType() == "MINI_FAB");
        } }, directives: [i1.DefaultLayoutAlignDirective, i2.NgIf, i3.MatButton, i4.MatIcon], styles: ["", ""] });
    return ButtonComponent;
}());
export { ButtonComponent };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(ButtonComponent, [{
        type: Component,
        args: [{
                selector: 'app-button',
                templateUrl: './button.component.html',
                styleUrls: ['./button.component.css'],
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        multi: true,
                        useExisting: forwardRef(function () { return ButtonComponent; }),
                    },
                ],
            }]
    }], function () { return []; }, { button: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnV0dG9uLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215LWFuZ3VsYXItZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvdWkvYnV0dG9uL2J1dHRvbi5jb21wb25lbnQudHMiLCJsaWIvc2hhcmVkL3VpL2J1dHRvbi9idXR0b24uY29tcG9uZW50Lmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxLQUFLLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRXJFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGdCQUFnQixDQUFDOzs7Ozs7OztJQ0szQyxpQ0FNcUM7SUFGakMsNkpBQVMsb0NBQWtDLEdBQUcsb0NBQWtDLEVBQUUsR0FBRyxTQUFTLElBQUM7SUFFOUQsWUFBOEI7SUFBQSxpQkFBUzs7O0lBSHhFLG9EQUFvQyx5Q0FBQSxtQ0FBQTtJQUdILGVBQThCO0lBQTlCLGdEQUE4Qjs7OztJQUVuRSxpQ0FNcUM7SUFGakMsOEpBQVMsb0NBQWtDLEdBQUcsb0NBQWtDLEVBQUUsR0FBRyxTQUFTLElBQUM7SUFFOUQsWUFBOEI7SUFBQSxpQkFBUzs7O0lBSHhFLG9EQUFvQyx5Q0FBQSxtQ0FBQTtJQUdILGVBQThCO0lBQTlCLGdEQUE4Qjs7OztJQUVuRSxrQ0FNcUM7SUFGakMsK0pBQVMscUNBQWtDLEdBQUcscUNBQWtDLEVBQUUsR0FBRyxTQUFTLElBQUM7SUFFOUQsWUFBOEI7SUFBQSxpQkFBUzs7O0lBSHhFLG9EQUFvQyx5Q0FBQSxtQ0FBQTtJQUdILGVBQThCO0lBQTlCLGdEQUE4Qjs7OztJQUVuRSxrQ0FNcUM7SUFGakMsK0pBQVMscUNBQWtDLEdBQUcscUNBQWtDLEVBQUUsR0FBRyxTQUFTLElBQUM7SUFFOUQsWUFBOEI7SUFBQSxpQkFBUzs7O0lBSHhFLG9EQUFvQyx5Q0FBQSxtQ0FBQTtJQUdILGVBQThCO0lBQTlCLGdEQUE4Qjs7OztJQUVuRSxrQ0FPUTtJQUhKLCtKQUFTLHFDQUFrQyxHQUFHLHFDQUFrQyxFQUFFLEdBQUcsU0FBUyxJQUFDO0lBRzNGLGdDQUFVO0lBQUEsWUFBa0M7SUFBQSxpQkFBVztJQUMvRCxpQkFBUzs7O0lBTEwsb0RBQW9DLHlDQUFBLG1DQUFBO0lBSXRCLGVBQWtDO0lBQWxDLHNEQUFrQzs7OztJQUdwRCxrQ0FPSTtJQUhBLCtKQUFTLHFDQUFrQyxHQUFHLHFDQUFrQyxFQUFFLEdBQUcsU0FBUyxJQUFDO0lBRy9GLGdDQUFVO0lBQUEsWUFBa0M7SUFBQSxpQkFBVztJQUMzRCxpQkFBUzs7O0lBTEwsb0RBQW9DLHlDQUFBLG1DQUFBO0lBSTFCLGVBQWtDO0lBQWxDLHNEQUFrQzs7OztJQUdoRCxrQ0FPSTtJQUhBLCtKQUFTLHFDQUFrQyxHQUFHLHFDQUFrQyxFQUFFLEdBQUcsU0FBUyxJQUFDO0lBRy9GLGdDQUFVO0lBQUEsWUFBa0M7SUFBQSxpQkFBVztJQUMzRCxpQkFBUzs7O0lBTEwsb0RBQW9DLHlDQUFBLG1DQUFBO0lBSTFCLGVBQWtDO0lBQWxDLHNEQUFrQzs7QUQ5RHhEO0lBa0JFO1FBSE8sYUFBUSxHQUFRLGNBQU8sQ0FBQyxDQUFDO1FBQ3pCLGNBQVMsR0FBUSxjQUFPLENBQUMsQ0FBQztJQUVsQixDQUFDO0lBRWhCLDBDQUFnQixHQUFoQixVQUFpQixFQUFFO1FBQ2pCLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO0lBQ3JCLENBQUM7SUFFRCwyQ0FBaUIsR0FBakIsVUFBa0IsRUFBRTtRQUNsQixJQUFJLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztJQUN0QixDQUFDO0lBRUQsb0NBQVUsR0FBVixVQUFXLEtBQUs7UUFDZCxJQUFJLEtBQUssRUFBRTtZQUNULElBQUksQ0FBQyxNQUFNLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3BDO0lBQ0gsQ0FBQztJQUVELGtDQUFRLEdBQVI7UUFDRSx1Q0FBdUM7SUFDekMsQ0FBQztJQUVELHFDQUFXLEdBQVg7UUFDRSw0Q0FBNEM7SUFDOUMsQ0FBQztrRkE1QlUsZUFBZTt3REFBZixlQUFlLDhGQVJmO2dCQUNUO29CQUNFLE9BQU8sRUFBRSxpQkFBaUI7b0JBQzFCLEtBQUssRUFBRSxJQUFJO29CQUNYLFdBQVcsRUFBRSxVQUFVLENBQUMsY0FBTSxPQUFBLGVBQWUsRUFBZixDQUFlLENBQUM7aUJBQy9DO2FBQ0Y7WUNWSCxrQ0FHUTtZQUFBLHNFQU1xQztZQUVyQyxzRUFNcUM7WUFFckMsc0VBTXFDO1lBRXJDLHNFQU1xQztZQUVyQyxzRUFPUTtZQUdSLHNFQU9JO1lBR0osc0VBT0k7WUFFWixpQkFBVTs7WUE1REUsZUFBaUQ7WUFBakQsK0RBQWlEO1lBUWpELGVBQWtEO1lBQWxELGdFQUFrRDtZQVFsRCxlQUFtRDtZQUFuRCxpRUFBbUQ7WUFRbkQsZUFBZ0Q7WUFBaEQsOERBQWdEO1lBUWhELGVBQWdEO1lBQWhELDhEQUFnRDtZQVVoRCxlQUErQztZQUEvQyw2REFBK0M7WUFVL0MsZUFBb0Q7WUFBcEQsa0VBQW9EOzswQkQ1RGhFO0NBNkNDLEFBekNELElBeUNDO1NBN0JZLGVBQWU7a0RBQWYsZUFBZTtjQVozQixTQUFTO2VBQUM7Z0JBQ1QsUUFBUSxFQUFFLFlBQVk7Z0JBQ3RCLFdBQVcsRUFBRSx5QkFBeUI7Z0JBQ3RDLFNBQVMsRUFBRSxDQUFDLHdCQUF3QixDQUFDO2dCQUNyQyxTQUFTLEVBQUU7b0JBQ1Q7d0JBQ0UsT0FBTyxFQUFFLGlCQUFpQjt3QkFDMUIsS0FBSyxFQUFFLElBQUk7d0JBQ1gsV0FBVyxFQUFFLFVBQVUsQ0FBQyxjQUFNLE9BQUEsZUFBZSxFQUFmLENBQWUsQ0FBQztxQkFDL0M7aUJBQ0Y7YUFDRjs7a0JBRUUsS0FBSyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCwgZm9yd2FyZFJlZiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQnV0dG9uIH0gZnJvbSAnLi4vLi4vbW9kZWwvc3RydWN0dXJhbC9jb21wb3NpdGUvZm9ybS9idXR0b24vYnV0dG9uLm1vZGVsJztcbmltcG9ydCB7IE5HX1ZBTFVFX0FDQ0VTU09SIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdhcHAtYnV0dG9uJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2J1dHRvbi5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL2J1dHRvbi5jb21wb25lbnQuY3NzJ10sXG4gIHByb3ZpZGVyczogW1xuICAgIHtcbiAgICAgIHByb3ZpZGU6IE5HX1ZBTFVFX0FDQ0VTU09SLFxuICAgICAgbXVsdGk6IHRydWUsXG4gICAgICB1c2VFeGlzdGluZzogZm9yd2FyZFJlZigoKSA9PiBCdXR0b25Db21wb25lbnQpLFxuICAgIH0sXG4gIF0sXG59KVxuZXhwb3J0IGNsYXNzIEJ1dHRvbkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gIEBJbnB1dCgpIGJ1dHRvbjogQnV0dG9uO1xuXG4gIHB1YmxpYyBvbkNoYW5nZTogYW55ID0gKCkgPT4ge307XG4gIHB1YmxpYyBvblRvdWNoZWQ6IGFueSA9ICgpID0+IHt9O1xuXG4gIGNvbnN0cnVjdG9yKCkge31cblxuICByZWdpc3Rlck9uQ2hhbmdlKGZuKSB7XG4gICAgdGhpcy5vbkNoYW5nZSA9IGZuO1xuICB9XG5cbiAgcmVnaXN0ZXJPblRvdWNoZWQoZm4pIHtcbiAgICB0aGlzLm9uVG91Y2hlZCA9IGZuO1xuICB9XG5cbiAgd3JpdGVWYWx1ZSh2YWx1ZSkge1xuICAgIGlmICh2YWx1ZSkge1xuICAgICAgdGhpcy5idXR0b24uc2V0RGVmYXVsdFZhbHVlKHZhbHVlKTtcbiAgICB9XG4gIH1cblxuICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgICAvLyBjb25zb2xlLmxvZygnQnV0dG9uQ29tcG9uZW50IGluaXQnKTtcbiAgfVxuXG4gIG5nT25EZXN0cm95KCk6IHZvaWQge1xuICAgIC8vIGNvbnNvbGUubG9nKCdCdXR0b25Db21wb25lbnQgZGVzdHJveWVkJyk7XG4gIH1cbn1cbiIsIjxzdHlsZT5cblxuPC9zdHlsZT5cblxuPHNlY3Rpb25cbiAgICBmeExheW91dEFsaWduPVwiY2VudGVyIGNlbnRlclwiPlxuXG4gICAgICAgIDxidXR0b24gXG4gICAgICAgICAgICAqbmdJZj1cInRoaXMuYnV0dG9uLmdldE1hdEJ1dHRvblR5cGUoKSA9PSAnQkFTSUMnXCJcbiAgICAgICAgICAgIG1hdC1idXR0b25cbiAgICAgICAgICAgIFt0eXBlXT1cInRoaXMuYnV0dG9uLmdldEJ1dHRvblR5cGUoKVwiXG4gICAgICAgICAgICAoY2xpY2spPVwidGhpcy5idXR0b24uZ2V0RnVuY3Rpb25Ub0V4ZWN1dGUoKSA/IHRoaXMuYnV0dG9uLmdldEZ1bmN0aW9uVG9FeGVjdXRlKCkoKSA6IHVuZGVmaW5lZFwiXG4gICAgICAgICAgICBbZGlzYWJsZWRdPVwidGhpcy5idXR0b24uZ2V0RGlzYWJsZWQoKVwiXG4gICAgICAgICAgICBbY29sb3JdPVwidGhpcy5idXR0b24uZ2V0Q29sb3IoKVwiPnt7IHRoaXMuYnV0dG9uLmdldENvbnRleHQoKSB9fTwvYnV0dG9uPlxuXG4gICAgICAgIDxidXR0b24gXG4gICAgICAgICAgICAqbmdJZj1cInRoaXMuYnV0dG9uLmdldE1hdEJ1dHRvblR5cGUoKSA9PSAnUkFJU0VEJ1wiXG4gICAgICAgICAgICBtYXQtcmFpc2VkLWJ1dHRvblxuICAgICAgICAgICAgW3R5cGVdPVwidGhpcy5idXR0b24uZ2V0QnV0dG9uVHlwZSgpXCJcbiAgICAgICAgICAgIChjbGljayk9XCJ0aGlzLmJ1dHRvbi5nZXRGdW5jdGlvblRvRXhlY3V0ZSgpID8gdGhpcy5idXR0b24uZ2V0RnVuY3Rpb25Ub0V4ZWN1dGUoKSgpIDogdW5kZWZpbmVkXCJcbiAgICAgICAgICAgIFtkaXNhYmxlZF09XCJ0aGlzLmJ1dHRvbi5nZXREaXNhYmxlZCgpXCJcbiAgICAgICAgICAgIFtjb2xvcl09XCJ0aGlzLmJ1dHRvbi5nZXRDb2xvcigpXCI+e3sgdGhpcy5idXR0b24uZ2V0Q29udGV4dCgpIH19PC9idXR0b24+XG5cbiAgICAgICAgPGJ1dHRvbiBcbiAgICAgICAgICAgICpuZ0lmPVwidGhpcy5idXR0b24uZ2V0TWF0QnV0dG9uVHlwZSgpID09ICdTVFJPS0VEJ1wiXG4gICAgICAgICAgICBtYXQtc3Ryb2tlZC1idXR0b25cbiAgICAgICAgICAgIFt0eXBlXT1cInRoaXMuYnV0dG9uLmdldEJ1dHRvblR5cGUoKVwiXG4gICAgICAgICAgICAoY2xpY2spPVwidGhpcy5idXR0b24uZ2V0RnVuY3Rpb25Ub0V4ZWN1dGUoKSA/IHRoaXMuYnV0dG9uLmdldEZ1bmN0aW9uVG9FeGVjdXRlKCkoKSA6IHVuZGVmaW5lZFwiXG4gICAgICAgICAgICBbZGlzYWJsZWRdPVwidGhpcy5idXR0b24uZ2V0RGlzYWJsZWQoKVwiXG4gICAgICAgICAgICBbY29sb3JdPVwidGhpcy5idXR0b24uZ2V0Q29sb3IoKVwiPnt7IHRoaXMuYnV0dG9uLmdldENvbnRleHQoKSB9fTwvYnV0dG9uPlxuXG4gICAgICAgIDxidXR0b24gXG4gICAgICAgICAgICAqbmdJZj1cInRoaXMuYnV0dG9uLmdldE1hdEJ1dHRvblR5cGUoKSA9PSAnRkxBVCdcIlxuICAgICAgICAgICAgbWF0LWZsYXQtYnV0dG9uXG4gICAgICAgICAgICBbdHlwZV09XCJ0aGlzLmJ1dHRvbi5nZXRCdXR0b25UeXBlKClcIlxuICAgICAgICAgICAgKGNsaWNrKT1cInRoaXMuYnV0dG9uLmdldEZ1bmN0aW9uVG9FeGVjdXRlKCkgPyB0aGlzLmJ1dHRvbi5nZXRGdW5jdGlvblRvRXhlY3V0ZSgpKCkgOiB1bmRlZmluZWRcIlxuICAgICAgICAgICAgW2Rpc2FibGVkXT1cInRoaXMuYnV0dG9uLmdldERpc2FibGVkKClcIlxuICAgICAgICAgICAgW2NvbG9yXT1cInRoaXMuYnV0dG9uLmdldENvbG9yKClcIj57eyB0aGlzLmJ1dHRvbi5nZXRDb250ZXh0KCkgfX08L2J1dHRvbj5cblxuICAgICAgICA8YnV0dG9uIFxuICAgICAgICAgICAgKm5nSWY9XCJ0aGlzLmJ1dHRvbi5nZXRNYXRCdXR0b25UeXBlKCkgPT0gJ0lDT04nXCJcbiAgICAgICAgICAgIG1hdC1pY29uLWJ1dHRvblxuICAgICAgICAgICAgW3R5cGVdPVwidGhpcy5idXR0b24uZ2V0QnV0dG9uVHlwZSgpXCJcbiAgICAgICAgICAgIChjbGljayk9XCJ0aGlzLmJ1dHRvbi5nZXRGdW5jdGlvblRvRXhlY3V0ZSgpID8gdGhpcy5idXR0b24uZ2V0RnVuY3Rpb25Ub0V4ZWN1dGUoKSgpIDogdW5kZWZpbmVkXCJcbiAgICAgICAgICAgIFtkaXNhYmxlZF09XCJ0aGlzLmJ1dHRvbi5nZXREaXNhYmxlZCgpXCJcbiAgICAgICAgICAgIFtjb2xvcl09XCJ0aGlzLmJ1dHRvbi5nZXRDb2xvcigpXCI+XG4gICAgICAgICAgICAgICAgPG1hdC1pY29uPnt7dGhpcy5idXR0b24uZ2V0TWF0SWNvblN0cmluZygpfX08L21hdC1pY29uPlxuICAgICAgICA8L2J1dHRvbj5cblxuICAgICAgICA8YnV0dG9uIFxuICAgICAgICAgICAgKm5nSWY9XCJ0aGlzLmJ1dHRvbi5nZXRNYXRCdXR0b25UeXBlKCkgPT0gJ0ZBQidcIlxuICAgICAgICAgICAgbWF0LWZhYlxuICAgICAgICAgICAgW3R5cGVdPVwidGhpcy5idXR0b24uZ2V0QnV0dG9uVHlwZSgpXCJcbiAgICAgICAgICAgIChjbGljayk9XCJ0aGlzLmJ1dHRvbi5nZXRGdW5jdGlvblRvRXhlY3V0ZSgpID8gdGhpcy5idXR0b24uZ2V0RnVuY3Rpb25Ub0V4ZWN1dGUoKSgpIDogdW5kZWZpbmVkXCJcbiAgICAgICAgICAgIFtkaXNhYmxlZF09XCJ0aGlzLmJ1dHRvbi5nZXREaXNhYmxlZCgpXCJcbiAgICAgICAgICAgIFtjb2xvcl09XCJ0aGlzLmJ1dHRvbi5nZXRDb2xvcigpXCI+XG4gICAgICAgICAgICA8bWF0LWljb24+e3t0aGlzLmJ1dHRvbi5nZXRNYXRJY29uU3RyaW5nKCl9fTwvbWF0LWljb24+XG4gICAgICAgIDwvYnV0dG9uPlxuXG4gICAgICAgIDxidXR0b24gXG4gICAgICAgICAgICAqbmdJZj1cInRoaXMuYnV0dG9uLmdldE1hdEJ1dHRvblR5cGUoKSA9PSAnTUlOSV9GQUInXCJcbiAgICAgICAgICAgIG1hdC1taW5pLWZhYlxuICAgICAgICAgICAgW3R5cGVdPVwidGhpcy5idXR0b24uZ2V0QnV0dG9uVHlwZSgpXCJcbiAgICAgICAgICAgIChjbGljayk9XCJ0aGlzLmJ1dHRvbi5nZXRGdW5jdGlvblRvRXhlY3V0ZSgpID8gdGhpcy5idXR0b24uZ2V0RnVuY3Rpb25Ub0V4ZWN1dGUoKSgpIDogdW5kZWZpbmVkXCJcbiAgICAgICAgICAgIFtkaXNhYmxlZF09XCJ0aGlzLmJ1dHRvbi5nZXREaXNhYmxlZCgpXCJcbiAgICAgICAgICAgIFtjb2xvcl09XCJ0aGlzLmJ1dHRvbi5nZXRDb2xvcigpXCI+XG4gICAgICAgICAgICA8bWF0LWljb24+e3t0aGlzLmJ1dHRvbi5nZXRNYXRJY29uU3RyaW5nKCl9fTwvbWF0LWljb24+XG4gICAgICAgIDwvYnV0dG9uPlxuPC9zZWN0aW9uPiJdfQ==