import { Component, Input, forwardRef } from "@angular/core";
import { NG_VALUE_ACCESSOR } from "@angular/forms";
// model
import { VALIDATOR_NAMES, } from "../../model/structural/composite/form/form-row-item.model";
import * as i0 from "@angular/core";
import * as i1 from "@angular/flex-layout/flex";
import * as i2 from "@angular/material/form-field";
import * as i3 from "@angular/common";
import * as i4 from "@angular/material/input";
import * as i5 from "@angular/cdk/text-field";
import * as i6 from "@angular/material/icon";
function TextareaComponent_mat_label_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-label");
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r0 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r0.textarea.getLabelName());
} }
function TextareaComponent_mat_icon_5_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-icon", 9);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r2 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r2.textarea.getMatPrefixImgText());
} }
function TextareaComponent_mat_icon_6_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-icon", 10);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r3 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r3.textarea.getMatSuffixImgText());
} }
function TextareaComponent_span_7_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 9);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r4 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1("", ctx_r4.textarea.getTextPrefix(), "\u00A0");
} }
function TextareaComponent_span_8_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 10);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r5 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r5.textarea.getTextSuffix());
} }
function TextareaComponent_mat_hint_9_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-hint", 11);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r6 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r6.textarea.getLeftHintLabel());
} }
function TextareaComponent_mat_hint_10_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-hint", 12);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r7 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r7.textarea.getRightHintLabel());
} }
function TextareaComponent_mat_hint_11_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-hint", 12);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r8 = i0.ɵɵnextContext();
    var tmp_0_0 = null;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate3("", ((tmp_0_0 = ctx_r8.getMinLengthValidator()) == null ? null : tmp_0_0.length) ? ctx_r8.getMinLengthValidator().length + " / " : "", "", (tmp_0_0 = ctx_r8.textarea.getDefaultValue()) == null ? null : tmp_0_0.length, "", ((tmp_0_0 = ctx_r8.getMaxLengthValidator()) == null ? null : tmp_0_0.length) ? " / " + ctx_r8.getMaxLengthValidator().length : "", "");
} }
function TextareaComponent_mat_hint_12_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-hint", 12);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r9 = i0.ɵɵnextContext();
    var tmp_0_0 = null;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate2("", (tmp_0_0 = ctx_r9.textarea.getDefaultValue()) == null ? null : tmp_0_0.length, "", ((tmp_0_0 = ctx_r9.getMinLengthValidator()) == null ? null : tmp_0_0.length) ? " / " + ctx_r9.getMinLengthValidator().length : "", "");
} }
function TextareaComponent_mat_hint_13_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-hint", 12);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r10 = i0.ɵɵnextContext();
    var tmp_0_0 = null;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate2("", (tmp_0_0 = ctx_r10.textarea.getDefaultValue()) == null ? null : tmp_0_0.length, "", ((tmp_0_0 = ctx_r10.getMaxLengthValidator()) == null ? null : tmp_0_0.length) ? " / " + ctx_r10.getMaxLengthValidator().length : "", "");
} }
var TextareaComponent = /** @class */ (function () {
    function TextareaComponent() {
        var _this = this;
        this.onChange = function () { };
        this.onTouched = function () { };
        this.onKeyUp = function (textarea) {
            // console.log(textarea.value);
            _this.textarea.setDefaultValue(textarea.value);
            _this.onChange(_this.textarea.getDefaultValue());
            _this.onTouched();
        };
        this.getMinLengthValidator = function () {
            var theValidator;
            _this.textarea
                .getValidators()
                .forEach(function (validator) {
                if (validator.name === VALIDATOR_NAMES.MIN_LENGTH)
                    theValidator = validator;
            });
            return theValidator;
        };
        this.getMaxLengthValidator = function () {
            var theValidator;
            _this.textarea
                .getValidators()
                .forEach(function (validator) {
                if (validator.name === VALIDATOR_NAMES.MAX_LENGTH)
                    theValidator = validator;
            });
            return theValidator;
        };
    }
    TextareaComponent.prototype.registerOnChange = function (fn) {
        this.onChange = fn;
    };
    TextareaComponent.prototype.registerOnTouched = function (fn) {
        this.onTouched = fn;
    };
    TextareaComponent.prototype.writeValue = function (value) {
        if (value) {
            this.textarea.setDefaultValue(value);
        }
    };
    TextareaComponent.prototype.ngOnInit = function () {
        // console.log('TextareaComponent init');
    };
    TextareaComponent.prototype.ngOnDestroy = function () {
        // console.log('TextareaComponent destroyed');
    };
    TextareaComponent.ɵfac = function TextareaComponent_Factory(t) { return new (t || TextareaComponent)(); };
    TextareaComponent.ɵcmp = i0.ɵɵdefineComponent({ type: TextareaComponent, selectors: [["app-textarea"]], inputs: { textarea: "textarea" }, features: [i0.ɵɵProvidersFeature([
                {
                    provide: NG_VALUE_ACCESSOR,
                    multi: true,
                    useExisting: forwardRef(function () { return TextareaComponent; }),
                },
            ])], decls: 14, vars: 18, consts: [["fxLayoutAlign", "center center"], [3, "appearance", "hideRequiredMarker"], [4, "ngIf"], ["matInput", "", "cdkTextareaAutosize", "", 3, "type", "placeholder", "value", "disabled", "readonly", "required", "keyup", "blur"], ["textareaField", ""], ["matPrefix", "", 4, "ngIf"], ["matSuffix", "", 4, "ngIf"], ["align", "start", 4, "ngIf"], ["align", "end", 4, "ngIf"], ["matPrefix", ""], ["matSuffix", ""], ["align", "start"], ["align", "end"]], template: function TextareaComponent_Template(rf, ctx) { if (rf & 1) {
            var _r11 = i0.ɵɵgetCurrentView();
            i0.ɵɵelementStart(0, "section", 0);
            i0.ɵɵelementStart(1, "mat-form-field", 1);
            i0.ɵɵtemplate(2, TextareaComponent_mat_label_2_Template, 2, 1, "mat-label", 2);
            i0.ɵɵelementStart(3, "textarea", 3, 4);
            i0.ɵɵlistener("keyup", function TextareaComponent_Template_textarea_keyup_3_listener() { i0.ɵɵrestoreView(_r11); var _r1 = i0.ɵɵreference(4); return ctx.onKeyUp(_r1); })("blur", function TextareaComponent_Template_textarea_blur_3_listener() { i0.ɵɵrestoreView(_r11); var _r1 = i0.ɵɵreference(4); return ctx.onKeyUp(_r1); });
            i0.ɵɵelementEnd();
            i0.ɵɵtemplate(5, TextareaComponent_mat_icon_5_Template, 2, 1, "mat-icon", 5);
            i0.ɵɵtemplate(6, TextareaComponent_mat_icon_6_Template, 2, 1, "mat-icon", 6);
            i0.ɵɵtemplate(7, TextareaComponent_span_7_Template, 2, 1, "span", 5);
            i0.ɵɵtemplate(8, TextareaComponent_span_8_Template, 2, 1, "span", 6);
            i0.ɵɵtemplate(9, TextareaComponent_mat_hint_9_Template, 2, 1, "mat-hint", 7);
            i0.ɵɵtemplate(10, TextareaComponent_mat_hint_10_Template, 2, 1, "mat-hint", 8);
            i0.ɵɵtemplate(11, TextareaComponent_mat_hint_11_Template, 2, 3, "mat-hint", 8);
            i0.ɵɵtemplate(12, TextareaComponent_mat_hint_12_Template, 2, 2, "mat-hint", 8);
            i0.ɵɵtemplate(13, TextareaComponent_mat_hint_13_Template, 2, 2, "mat-hint", 8);
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        } if (rf & 2) {
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("appearance", ctx.textarea.getAppearance())("hideRequiredMarker", ctx.textarea.getRequired().hideRequiredMarker);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.textarea.getLabelName());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("type", ctx.textarea.getFormFieldInputType())("placeholder", ctx.textarea.getPlaceholder() ? ctx.textarea.getPlaceholder() : "")("value", ctx.textarea.getDefaultValue())("disabled", ctx.textarea.getDisabled())("readonly", ctx.textarea.getReadonly())("required", ctx.textarea.getRequired().required);
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("ngIf", ctx.textarea.getMatPrefixImgText());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.textarea.getMatSuffixImgText());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.textarea.getTextPrefix());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.textarea.getTextSuffix());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.textarea.getLeftHintLabel());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.textarea.getRightHintLabel());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.textarea.getShowHintAboutMinMaxLength() && ctx.getMinLengthValidator() && ctx.getMaxLengthValidator());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.textarea.getShowHintAboutMinMaxLength() && ctx.getMinLengthValidator() && !ctx.getMaxLengthValidator());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.textarea.getShowHintAboutMinMaxLength() && !ctx.getMinLengthValidator() && ctx.getMaxLengthValidator());
        } }, directives: [i1.DefaultLayoutAlignDirective, i2.MatFormField, i3.NgIf, i4.MatInput, i5.CdkTextareaAutosize, i2.MatLabel, i6.MatIcon, i2.MatPrefix, i2.MatSuffix, i2.MatHint], styles: ["", ".cursor[_ngcontent-%COMP%] {\n        cursor: pointer;\n    }\n\n    input[type=\"file\"][_ngcontent-%COMP%] {\n        display: none;\n    }\n\n    h3[_ngcontent-%COMP%] {\n        font-size: inherit;\n        margin: 0;\n        font-weight: inherit;\n    }\n\n    mat-form-field[_ngcontent-%COMP%] {\n        min-width: 250px;\n    }"] });
    return TextareaComponent;
}());
export { TextareaComponent };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(TextareaComponent, [{
        type: Component,
        args: [{
                selector: "app-textarea",
                templateUrl: "./textarea.component.html",
                styleUrls: ["./textarea.component.css"],
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        multi: true,
                        useExisting: forwardRef(function () { return TextareaComponent; }),
                    },
                ],
            }]
    }], function () { return []; }, { textarea: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGV4dGFyZWEuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXktYW5ndWxhci1mb3JtLyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC91aS90ZXh0YXJlYS90ZXh0YXJlYS5jb21wb25lbnQudHMiLCJsaWIvc2hhcmVkL3VpL3RleHRhcmVhL3RleHRhcmVhLmNvbXBvbmVudC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQXFCLEtBQUssRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDaEYsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFFbkQsUUFBUTtBQUNSLE9BQU8sRUFFTCxlQUFlLEdBQ2hCLE1BQU0sMkRBQTJELENBQUM7Ozs7Ozs7OztJQ3NCbkQsaUNBQ3lDO0lBQUEsWUFBa0M7SUFBQSxpQkFBWTs7O0lBQTlDLGVBQWtDO0lBQWxDLG9EQUFrQzs7O0lBZTNFLG1DQUVjO0lBQUEsWUFBeUM7SUFBQSxpQkFBVzs7O0lBQXBELGVBQXlDO0lBQXpDLDJEQUF5Qzs7O0lBRXZELG9DQUVjO0lBQUEsWUFBeUM7SUFBQSxpQkFBVzs7O0lBQXBELGVBQXlDO0lBQXpDLDJEQUF5Qzs7O0lBRXZELCtCQUVjO0lBQUEsWUFBeUM7SUFBQSxpQkFBTzs7O0lBQWhELGVBQXlDO0lBQXpDLG9FQUF5Qzs7O0lBRXZELGdDQUVjO0lBQUEsWUFBbUM7SUFBQSxpQkFBTzs7O0lBQTFDLGVBQW1DO0lBQW5DLHFEQUFtQzs7O0lBRWpELG9DQUVrQjtJQUFBLFlBQXNDO0lBQUEsaUJBQVc7OztJQUFqRCxlQUFzQztJQUF0Qyx3REFBc0M7OztJQUV4RCxvQ0FFZ0I7SUFBQSxZQUF1QztJQUFBLGlCQUFXOzs7SUFBbEQsZUFBdUM7SUFBdkMseURBQXVDOzs7SUFHdkQsb0NBRWdCO0lBQUEsWUFBb087SUFBQSxpQkFBVzs7OztJQUEvTyxlQUFvTztJQUFwTywwWEFBb087OztJQUVwUCxvQ0FFZ0I7SUFBQSxZQUF5STtJQUFBLGlCQUFXOzs7O0lBQXBKLGVBQXlJO0lBQXpJLG1QQUF5STs7O0lBRXpKLG9DQUVnQjtJQUFBLFlBQXdJO0lBQUEsaUJBQVc7Ozs7SUFBbkosZUFBd0k7SUFBeEksc1BBQXdJOztBRHRFeEs7SUFrQkU7UUFBQSxpQkFBZ0I7UUFIVCxhQUFRLEdBQVEsY0FBTyxDQUFDLENBQUM7UUFDekIsY0FBUyxHQUFRLGNBQU8sQ0FBQyxDQUFDO1FBSWpDLFlBQU8sR0FBRyxVQUFDLFFBQTBCO1lBQ25DLCtCQUErQjtZQUMvQixLQUFJLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDOUMsS0FBSSxDQUFDLFFBQVEsQ0FBQyxLQUFJLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxDQUFDLENBQUM7WUFDL0MsS0FBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ25CLENBQUMsQ0FBQztRQWdCRiwwQkFBcUIsR0FBRztZQUN0QixJQUFJLFlBQVksQ0FBQztZQUNqQixLQUFJLENBQUMsUUFBUTtpQkFDVixhQUFhLEVBQUU7aUJBQ2YsT0FBTyxDQUFDLFVBQUMsU0FBd0M7Z0JBQ2hELElBQUksU0FBUyxDQUFDLElBQUksS0FBSyxlQUFlLENBQUMsVUFBVTtvQkFDL0MsWUFBWSxHQUFHLFNBQVMsQ0FBQztZQUM3QixDQUFDLENBQUMsQ0FBQztZQUNMLE9BQU8sWUFBWSxDQUFDO1FBQ3RCLENBQUMsQ0FBQztRQUVGLDBCQUFxQixHQUFHO1lBQ3RCLElBQUksWUFBWSxDQUFDO1lBQ2pCLEtBQUksQ0FBQyxRQUFRO2lCQUNWLGFBQWEsRUFBRTtpQkFDZixPQUFPLENBQUMsVUFBQyxTQUF3QztnQkFDaEQsSUFBSSxTQUFTLENBQUMsSUFBSSxLQUFLLGVBQWUsQ0FBQyxVQUFVO29CQUMvQyxZQUFZLEdBQUcsU0FBUyxDQUFDO1lBQzdCLENBQUMsQ0FBQyxDQUFDO1lBQ0wsT0FBTyxZQUFZLENBQUM7UUFDdEIsQ0FBQyxDQUFDO0lBM0NhLENBQUM7SUFTaEIsNENBQWdCLEdBQWhCLFVBQWlCLEVBQUU7UUFDakIsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7SUFDckIsQ0FBQztJQUVELDZDQUFpQixHQUFqQixVQUFrQixFQUFFO1FBQ2xCLElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO0lBQ3RCLENBQUM7SUFFRCxzQ0FBVSxHQUFWLFVBQVcsS0FBSztRQUNkLElBQUksS0FBSyxFQUFFO1lBQ1QsSUFBSSxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDdEM7SUFDSCxDQUFDO0lBd0JELG9DQUFRLEdBQVI7UUFDRSx5Q0FBeUM7SUFDM0MsQ0FBQztJQUVELHVDQUFXLEdBQVg7UUFDRSw4Q0FBOEM7SUFDaEQsQ0FBQztzRkF6RFUsaUJBQWlCOzBEQUFqQixpQkFBaUIsb0dBUmpCO2dCQUNUO29CQUNFLE9BQU8sRUFBRSxpQkFBaUI7b0JBQzFCLEtBQUssRUFBRSxJQUFJO29CQUNYLFdBQVcsRUFBRSxVQUFVLENBQUMsY0FBTSxPQUFBLGlCQUFpQixFQUFqQixDQUFpQixDQUFDO2lCQUNqRDthQUNGOztZQ0VILGtDQUdRO1lBQUEseUNBSVE7WUFBQSw4RUFDeUM7WUFFekMsc0NBV21DO1lBTi9CLHFKQUFTLGdCQUEyQixJQUFDLHNJQUM3QixnQkFBMkIsSUFERTtZQU1qQixpQkFBVztZQUVuQyw0RUFFYztZQUVkLDRFQUVjO1lBRWQsb0VBRWM7WUFFZCxvRUFFYztZQUVkLDRFQUVrQjtZQUVsQiw4RUFFZ0I7WUFHaEIsOEVBRWdCO1lBRWhCLDhFQUVnQjtZQUVoQiw4RUFFZ0I7WUFFeEIsaUJBQWlCO1lBRXpCLGlCQUFVOztZQTFERSxlQUE0QztZQUE1Qyx5REFBNEMscUVBQUE7WUFJcEMsZUFBb0M7WUFBcEMsa0RBQW9DO1lBSXBDLGVBQThDO1lBQTlDLDJEQUE4QyxtRkFBQSx5Q0FBQSx3Q0FBQSx3Q0FBQSxpREFBQTtZQVk5QyxlQUEyQztZQUEzQyx5REFBMkM7WUFJM0MsZUFBMkM7WUFBM0MseURBQTJDO1lBSTNDLGVBQXFDO1lBQXJDLG1EQUFxQztZQUlyQyxlQUFxQztZQUFyQyxtREFBcUM7WUFJckMsZUFBd0M7WUFBeEMsc0RBQXdDO1lBSXhDLGVBQXlDO1lBQXpDLHVEQUF5QztZQUt6QyxlQUFvSDtZQUFwSCxnSUFBb0g7WUFJcEgsZUFBcUg7WUFBckgsaUlBQXFIO1lBSXJILGVBQXFIO1lBQXJILGlJQUFxSDs7NEJEL0V6STtDQWdGQyxBQXRFRCxJQXNFQztTQTFEWSxpQkFBaUI7a0RBQWpCLGlCQUFpQjtjQVo3QixTQUFTO2VBQUM7Z0JBQ1QsUUFBUSxFQUFFLGNBQWM7Z0JBQ3hCLFdBQVcsRUFBRSwyQkFBMkI7Z0JBQ3hDLFNBQVMsRUFBRSxDQUFDLDBCQUEwQixDQUFDO2dCQUN2QyxTQUFTLEVBQUU7b0JBQ1Q7d0JBQ0UsT0FBTyxFQUFFLGlCQUFpQjt3QkFDMUIsS0FBSyxFQUFFLElBQUk7d0JBQ1gsV0FBVyxFQUFFLFVBQVUsQ0FBQyxjQUFNLE9BQUEsaUJBQWlCLEVBQWpCLENBQWlCLENBQUM7cUJBQ2pEO2lCQUNGO2FBQ0Y7O2tCQUVFLEtBQUsiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgT25EZXN0cm95LCBJbnB1dCwgZm9yd2FyZFJlZiB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBOR19WQUxVRV9BQ0NFU1NPUiB9IGZyb20gXCJAYW5ndWxhci9mb3Jtc1wiO1xuXG4vLyBtb2RlbFxuaW1wb3J0IHtcbiAgRm9ybVJvd0l0ZW1WYWxpZGF0b3JJbnRlcmZhY2UsXG4gIFZBTElEQVRPUl9OQU1FUyxcbn0gZnJvbSBcIi4uLy4uL21vZGVsL3N0cnVjdHVyYWwvY29tcG9zaXRlL2Zvcm0vZm9ybS1yb3ctaXRlbS5tb2RlbFwiO1xuaW1wb3J0IHsgVGV4dGFyZWEgfSBmcm9tIFwiLi4vLi4vbW9kZWwvc3RydWN0dXJhbC9jb21wb3NpdGUvZm9ybS90ZXh0YXJlYS90ZXh0YXJlYS5tb2RlbFwiO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6IFwiYXBwLXRleHRhcmVhXCIsXG4gIHRlbXBsYXRlVXJsOiBcIi4vdGV4dGFyZWEuY29tcG9uZW50Lmh0bWxcIixcbiAgc3R5bGVVcmxzOiBbXCIuL3RleHRhcmVhLmNvbXBvbmVudC5jc3NcIl0sXG4gIHByb3ZpZGVyczogW1xuICAgIHtcbiAgICAgIHByb3ZpZGU6IE5HX1ZBTFVFX0FDQ0VTU09SLFxuICAgICAgbXVsdGk6IHRydWUsXG4gICAgICB1c2VFeGlzdGluZzogZm9yd2FyZFJlZigoKSA9PiBUZXh0YXJlYUNvbXBvbmVudCksXG4gICAgfSxcbiAgXSxcbn0pXG5leHBvcnQgY2xhc3MgVGV4dGFyZWFDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XG4gIEBJbnB1dCgpIHRleHRhcmVhOiBUZXh0YXJlYTtcblxuICBwdWJsaWMgb25DaGFuZ2U6IGFueSA9ICgpID0+IHt9O1xuICBwdWJsaWMgb25Ub3VjaGVkOiBhbnkgPSAoKSA9PiB7fTtcblxuICBjb25zdHJ1Y3RvcigpIHt9XG5cbiAgb25LZXlVcCA9ICh0ZXh0YXJlYTogSFRNTElucHV0RWxlbWVudCkgPT4ge1xuICAgIC8vIGNvbnNvbGUubG9nKHRleHRhcmVhLnZhbHVlKTtcbiAgICB0aGlzLnRleHRhcmVhLnNldERlZmF1bHRWYWx1ZSh0ZXh0YXJlYS52YWx1ZSk7XG4gICAgdGhpcy5vbkNoYW5nZSh0aGlzLnRleHRhcmVhLmdldERlZmF1bHRWYWx1ZSgpKTtcbiAgICB0aGlzLm9uVG91Y2hlZCgpO1xuICB9O1xuXG4gIHJlZ2lzdGVyT25DaGFuZ2UoZm4pIHtcbiAgICB0aGlzLm9uQ2hhbmdlID0gZm47XG4gIH1cblxuICByZWdpc3Rlck9uVG91Y2hlZChmbikge1xuICAgIHRoaXMub25Ub3VjaGVkID0gZm47XG4gIH1cblxuICB3cml0ZVZhbHVlKHZhbHVlKSB7XG4gICAgaWYgKHZhbHVlKSB7XG4gICAgICB0aGlzLnRleHRhcmVhLnNldERlZmF1bHRWYWx1ZSh2YWx1ZSk7XG4gICAgfVxuICB9XG5cbiAgZ2V0TWluTGVuZ3RoVmFsaWRhdG9yID0gKCk6IEZvcm1Sb3dJdGVtVmFsaWRhdG9ySW50ZXJmYWNlID0+IHtcbiAgICBsZXQgdGhlVmFsaWRhdG9yO1xuICAgIHRoaXMudGV4dGFyZWFcbiAgICAgIC5nZXRWYWxpZGF0b3JzKClcbiAgICAgIC5mb3JFYWNoKCh2YWxpZGF0b3I6IEZvcm1Sb3dJdGVtVmFsaWRhdG9ySW50ZXJmYWNlKSA9PiB7XG4gICAgICAgIGlmICh2YWxpZGF0b3IubmFtZSA9PT0gVkFMSURBVE9SX05BTUVTLk1JTl9MRU5HVEgpXG4gICAgICAgICAgdGhlVmFsaWRhdG9yID0gdmFsaWRhdG9yO1xuICAgICAgfSk7XG4gICAgcmV0dXJuIHRoZVZhbGlkYXRvcjtcbiAgfTtcblxuICBnZXRNYXhMZW5ndGhWYWxpZGF0b3IgPSAoKTogRm9ybVJvd0l0ZW1WYWxpZGF0b3JJbnRlcmZhY2UgPT4ge1xuICAgIGxldCB0aGVWYWxpZGF0b3I7XG4gICAgdGhpcy50ZXh0YXJlYVxuICAgICAgLmdldFZhbGlkYXRvcnMoKVxuICAgICAgLmZvckVhY2goKHZhbGlkYXRvcjogRm9ybVJvd0l0ZW1WYWxpZGF0b3JJbnRlcmZhY2UpID0+IHtcbiAgICAgICAgaWYgKHZhbGlkYXRvci5uYW1lID09PSBWQUxJREFUT1JfTkFNRVMuTUFYX0xFTkdUSClcbiAgICAgICAgICB0aGVWYWxpZGF0b3IgPSB2YWxpZGF0b3I7XG4gICAgICB9KTtcbiAgICByZXR1cm4gdGhlVmFsaWRhdG9yO1xuICB9O1xuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICAgIC8vIGNvbnNvbGUubG9nKCdUZXh0YXJlYUNvbXBvbmVudCBpbml0Jyk7XG4gIH1cblxuICBuZ09uRGVzdHJveSgpOiB2b2lkIHtcbiAgICAvLyBjb25zb2xlLmxvZygnVGV4dGFyZWFDb21wb25lbnQgZGVzdHJveWVkJyk7XG4gIH1cbn1cbiIsIjxzdHlsZT5cblxuICAgIC5jdXJzb3Ige1xuICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgfVxuXG4gICAgaW5wdXRbdHlwZT1cImZpbGVcIl0ge1xuICAgICAgICBkaXNwbGF5OiBub25lO1xuICAgIH1cblxuICAgIGgzIHtcbiAgICAgICAgZm9udC1zaXplOiBpbmhlcml0O1xuICAgICAgICBtYXJnaW46IDA7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiBpbmhlcml0O1xuICAgIH1cblxuICAgIG1hdC1mb3JtLWZpZWxkIHtcbiAgICAgICAgbWluLXdpZHRoOiAyNTBweDtcbiAgICB9XG5cbjwvc3R5bGU+XG4gICAgXG48c2VjdGlvblxuICAgIGZ4TGF5b3V0QWxpZ249XCJjZW50ZXIgY2VudGVyXCI+XG5cbiAgICAgICAgPG1hdC1mb3JtLWZpZWxkIFxuICAgICAgICAgICAgW2FwcGVhcmFuY2VdPVwidGhpcy50ZXh0YXJlYS5nZXRBcHBlYXJhbmNlKClcIlxuICAgICAgICAgICAgW2hpZGVSZXF1aXJlZE1hcmtlcl09XCJ0aGlzLnRleHRhcmVhLmdldFJlcXVpcmVkKCkuaGlkZVJlcXVpcmVkTWFya2VyXCI+XG5cbiAgICAgICAgICAgICAgICA8bWF0LWxhYmVsXG4gICAgICAgICAgICAgICAgICAgICpuZ0lmPVwidGhpcy50ZXh0YXJlYS5nZXRMYWJlbE5hbWUoKVwiPnt7IHRoaXMudGV4dGFyZWEuZ2V0TGFiZWxOYW1lKCkgfX08L21hdC1sYWJlbD5cblxuICAgICAgICAgICAgICAgIDx0ZXh0YXJlYSBcbiAgICAgICAgICAgICAgICAgICAgbWF0SW5wdXRcbiAgICAgICAgICAgICAgICAgICAgW3R5cGVdPVwidGhpcy50ZXh0YXJlYS5nZXRGb3JtRmllbGRJbnB1dFR5cGUoKVwiXG4gICAgICAgICAgICAgICAgICAgIFtwbGFjZWhvbGRlcl09XCJ0aGlzLnRleHRhcmVhLmdldFBsYWNlaG9sZGVyKCkgPyB0aGlzLnRleHRhcmVhLmdldFBsYWNlaG9sZGVyKCkgOiAnJ1wiXG4gICAgICAgICAgICAgICAgICAgIFt2YWx1ZV09XCJ0aGlzLnRleHRhcmVhLmdldERlZmF1bHRWYWx1ZSgpXCJcbiAgICAgICAgICAgICAgICAgICAgKGtleXVwKT1cInRoaXMub25LZXlVcCh0ZXh0YXJlYUZpZWxkKVwiXG4gICAgICAgICAgICAgICAgICAgIChibHVyKT1cInRoaXMub25LZXlVcCh0ZXh0YXJlYUZpZWxkKVwiXG4gICAgICAgICAgICAgICAgICAgIFtkaXNhYmxlZF09XCJ0aGlzLnRleHRhcmVhLmdldERpc2FibGVkKClcIlxuICAgICAgICAgICAgICAgICAgICBbcmVhZG9ubHldPVwidGhpcy50ZXh0YXJlYS5nZXRSZWFkb25seSgpXCJcbiAgICAgICAgICAgICAgICAgICAgW3JlcXVpcmVkXT1cInRoaXMudGV4dGFyZWEuZ2V0UmVxdWlyZWQoKS5yZXF1aXJlZFwiXG4gICAgICAgICAgICAgICAgICAgICN0ZXh0YXJlYUZpZWxkXG4gICAgICAgICAgICAgICAgICAgIGNka1RleHRhcmVhQXV0b3NpemU+PC90ZXh0YXJlYT5cblxuICAgICAgICAgICAgICAgIDxtYXQtaWNvbiBcbiAgICAgICAgICAgICAgICAgICAgKm5nSWY9XCJ0aGlzLnRleHRhcmVhLmdldE1hdFByZWZpeEltZ1RleHQoKVwiXG4gICAgICAgICAgICAgICAgICAgIG1hdFByZWZpeD57eyB0aGlzLnRleHRhcmVhLmdldE1hdFByZWZpeEltZ1RleHQoKSB9fTwvbWF0LWljb24+XG5cbiAgICAgICAgICAgICAgICA8bWF0LWljb24gXG4gICAgICAgICAgICAgICAgICAgICpuZ0lmPVwidGhpcy50ZXh0YXJlYS5nZXRNYXRTdWZmaXhJbWdUZXh0KClcIlxuICAgICAgICAgICAgICAgICAgICBtYXRTdWZmaXg+e3sgdGhpcy50ZXh0YXJlYS5nZXRNYXRTdWZmaXhJbWdUZXh0KCkgfX08L21hdC1pY29uPlxuXG4gICAgICAgICAgICAgICAgPHNwYW4gXG4gICAgICAgICAgICAgICAgICAgICpuZ0lmPVwidGhpcy50ZXh0YXJlYS5nZXRUZXh0UHJlZml4KClcIlxuICAgICAgICAgICAgICAgICAgICBtYXRQcmVmaXg+e3sgdGhpcy50ZXh0YXJlYS5nZXRUZXh0UHJlZml4KCkgfX0mbmJzcDs8L3NwYW4+XG5cbiAgICAgICAgICAgICAgICA8c3BhbiBcbiAgICAgICAgICAgICAgICAgICAgKm5nSWY9XCJ0aGlzLnRleHRhcmVhLmdldFRleHRTdWZmaXgoKVwiXG4gICAgICAgICAgICAgICAgICAgIG1hdFN1ZmZpeD57eyB0aGlzLnRleHRhcmVhLmdldFRleHRTdWZmaXgoKSB9fTwvc3Bhbj5cblxuICAgICAgICAgICAgICAgIDxtYXQtaGludCBcbiAgICAgICAgICAgICAgICAgICAgKm5nSWY9XCJ0aGlzLnRleHRhcmVhLmdldExlZnRIaW50TGFiZWwoKVwiXG4gICAgICAgICAgICAgICAgICAgIGFsaWduPVwic3RhcnRcIj57eyB0aGlzLnRleHRhcmVhLmdldExlZnRIaW50TGFiZWwoKSB9fTwvbWF0LWhpbnQ+XG5cbiAgICAgICAgICAgICAgICA8bWF0LWhpbnRcbiAgICAgICAgICAgICAgICAgICAgKm5nSWY9XCJ0aGlzLnRleHRhcmVhLmdldFJpZ2h0SGludExhYmVsKClcIlxuICAgICAgICAgICAgICAgICAgICBhbGlnbj1cImVuZFwiPnt7IHRoaXMudGV4dGFyZWEuZ2V0UmlnaHRIaW50TGFiZWwoKSB9fTwvbWF0LWhpbnQ+XG5cbiAgICAgICAgICAgICAgICA8IS0tIGlmIGZpZWxkIGhhcyBTaG93SGludEFib3V0TWluTWF4TGVuZ3RoIHNldCB0byB0cnVlIC0tPlxuICAgICAgICAgICAgICAgIDxtYXQtaGludFxuICAgICAgICAgICAgICAgICAgICAqbmdJZj1cInRoaXMudGV4dGFyZWEuZ2V0U2hvd0hpbnRBYm91dE1pbk1heExlbmd0aCgpICYmIHRoaXMuZ2V0TWluTGVuZ3RoVmFsaWRhdG9yKCkgJiYgdGhpcy5nZXRNYXhMZW5ndGhWYWxpZGF0b3IoKVwiXG4gICAgICAgICAgICAgICAgICAgIGFsaWduPVwiZW5kXCI+e3sgdGhpcy5nZXRNaW5MZW5ndGhWYWxpZGF0b3IoKT8ubGVuZ3RoID8gdGhpcy5nZXRNaW5MZW5ndGhWYWxpZGF0b3IoKS5sZW5ndGggKyBcIiAvIFwiIDogJyd9fXt7IHRoaXMudGV4dGFyZWEuZ2V0RGVmYXVsdFZhbHVlKCk/Lmxlbmd0aCB9fXt7IHRoaXMuZ2V0TWF4TGVuZ3RoVmFsaWRhdG9yKCk/Lmxlbmd0aCA/IFwiIC8gXCIgKyB0aGlzLmdldE1heExlbmd0aFZhbGlkYXRvcigpLmxlbmd0aDogJyd9fTwvbWF0LWhpbnQ+XG5cbiAgICAgICAgICAgICAgICA8bWF0LWhpbnRcbiAgICAgICAgICAgICAgICAgICAgKm5nSWY9XCJ0aGlzLnRleHRhcmVhLmdldFNob3dIaW50QWJvdXRNaW5NYXhMZW5ndGgoKSAmJiB0aGlzLmdldE1pbkxlbmd0aFZhbGlkYXRvcigpICYmICF0aGlzLmdldE1heExlbmd0aFZhbGlkYXRvcigpXCJcbiAgICAgICAgICAgICAgICAgICAgYWxpZ249XCJlbmRcIj57eyB0aGlzLnRleHRhcmVhLmdldERlZmF1bHRWYWx1ZSgpPy5sZW5ndGggfX17eyB0aGlzLmdldE1pbkxlbmd0aFZhbGlkYXRvcigpPy5sZW5ndGggPyBcIiAvIFwiICsgdGhpcy5nZXRNaW5MZW5ndGhWYWxpZGF0b3IoKS5sZW5ndGggOiAnJ319PC9tYXQtaGludD5cblxuICAgICAgICAgICAgICAgIDxtYXQtaGludFxuICAgICAgICAgICAgICAgICAgICAqbmdJZj1cInRoaXMudGV4dGFyZWEuZ2V0U2hvd0hpbnRBYm91dE1pbk1heExlbmd0aCgpICYmICF0aGlzLmdldE1pbkxlbmd0aFZhbGlkYXRvcigpICYmIHRoaXMuZ2V0TWF4TGVuZ3RoVmFsaWRhdG9yKClcIlxuICAgICAgICAgICAgICAgICAgICBhbGlnbj1cImVuZFwiPnt7IHRoaXMudGV4dGFyZWEuZ2V0RGVmYXVsdFZhbHVlKCk/Lmxlbmd0aCB9fXt7IHRoaXMuZ2V0TWF4TGVuZ3RoVmFsaWRhdG9yKCk/Lmxlbmd0aCA/IFwiIC8gXCIgKyB0aGlzLmdldE1heExlbmd0aFZhbGlkYXRvcigpLmxlbmd0aDogJyd9fTwvbWF0LWhpbnQ+XG5cbiAgICAgICAgPC9tYXQtZm9ybS1maWVsZD5cblxuPC9zZWN0aW9uPiJdfQ==