import { Component, Input, forwardRef } from "@angular/core";
import { NG_VALUE_ACCESSOR } from "@angular/forms";
import * as i0 from "@angular/core";
import * as i1 from "@angular/flex-layout/flex";
import * as i2 from "@angular/material/form-field";
import * as i3 from "@angular/common";
import * as i4 from "@angular/material/input";
import * as i5 from "@angular/material/icon";
function FormFieldInputNumberComponent_mat_label_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-label");
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r0 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r0.formFieldInputNumber.getLabelName());
} }
function FormFieldInputNumberComponent_mat_icon_5_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-icon", 9);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r2 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r2.formFieldInputNumber.getMatPrefixImgText());
} }
function FormFieldInputNumberComponent_mat_icon_6_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-icon", 10);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r3 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r3.formFieldInputNumber.getMatSuffixImgText());
} }
function FormFieldInputNumberComponent_span_7_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 9);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r4 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1("", ctx_r4.formFieldInputNumber.getTextPrefix(), "\u00A0");
} }
function FormFieldInputNumberComponent_span_8_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 10);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r5 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r5.formFieldInputNumber.getTextSuffix());
} }
function FormFieldInputNumberComponent_mat_hint_9_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-hint", 11);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r6 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r6.formFieldInputNumber.getLeftHintLabel());
} }
function FormFieldInputNumberComponent_mat_hint_10_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-hint", 12);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r7 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r7.formFieldInputNumber.getRightHintLabel());
} }
var FormFieldInputNumberComponent = /** @class */ (function () {
    function FormFieldInputNumberComponent() {
        var _this = this;
        this.onChange = function () { };
        this.onTouched = function () { };
        this.onKeyUp = function (input) {
            // console.log(input.value);
            _this.formFieldInputNumber.setDefaultValue(input.value);
            _this.onChange(_this.formFieldInputNumber.getDefaultValue());
            _this.onTouched();
        };
    }
    FormFieldInputNumberComponent.prototype.registerOnChange = function (fn) {
        this.onChange = fn;
    };
    FormFieldInputNumberComponent.prototype.registerOnTouched = function (fn) {
        this.onTouched = fn;
    };
    FormFieldInputNumberComponent.prototype.writeValue = function (value) {
        if (value) {
            this.formFieldInputNumber.setDefaultValue(value);
        }
    };
    FormFieldInputNumberComponent.prototype.ngOnInit = function () {
        // console.log('FormFieldInputNumberComponent init');
    };
    FormFieldInputNumberComponent.prototype.ngOnDestroy = function () {
        // console.log('FormFieldInputNumberComponent destroyed');
    };
    FormFieldInputNumberComponent.ɵfac = function FormFieldInputNumberComponent_Factory(t) { return new (t || FormFieldInputNumberComponent)(); };
    FormFieldInputNumberComponent.ɵcmp = i0.ɵɵdefineComponent({ type: FormFieldInputNumberComponent, selectors: [["app-form-field-input-number"]], inputs: { formFieldInputNumber: "formFieldInputNumber" }, features: [i0.ɵɵProvidersFeature([
                {
                    provide: NG_VALUE_ACCESSOR,
                    multi: true,
                    useExisting: forwardRef(function () { return FormFieldInputNumberComponent; }),
                },
            ])], decls: 11, vars: 16, consts: [["fxLayoutAlign", "center center"], [3, "appearance", "hideRequiredMarker"], [4, "ngIf"], ["matInput", "", 3, "type", "placeholder", "value", "step", "disabled", "readonly", "required", "keyup", "blur"], ["input", ""], ["matPrefix", "", 4, "ngIf"], ["matSuffix", "", 4, "ngIf"], ["align", "start", 4, "ngIf"], ["align", "end", 4, "ngIf"], ["matPrefix", ""], ["matSuffix", ""], ["align", "start"], ["align", "end"]], template: function FormFieldInputNumberComponent_Template(rf, ctx) { if (rf & 1) {
            var _r8 = i0.ɵɵgetCurrentView();
            i0.ɵɵelementStart(0, "section", 0);
            i0.ɵɵelementStart(1, "mat-form-field", 1);
            i0.ɵɵtemplate(2, FormFieldInputNumberComponent_mat_label_2_Template, 2, 1, "mat-label", 2);
            i0.ɵɵelementStart(3, "input", 3, 4);
            i0.ɵɵlistener("keyup", function FormFieldInputNumberComponent_Template_input_keyup_3_listener() { i0.ɵɵrestoreView(_r8); var _r1 = i0.ɵɵreference(4); return ctx.onKeyUp(_r1); })("blur", function FormFieldInputNumberComponent_Template_input_blur_3_listener() { i0.ɵɵrestoreView(_r8); var _r1 = i0.ɵɵreference(4); return ctx.onKeyUp(_r1); });
            i0.ɵɵelementEnd();
            i0.ɵɵtemplate(5, FormFieldInputNumberComponent_mat_icon_5_Template, 2, 1, "mat-icon", 5);
            i0.ɵɵtemplate(6, FormFieldInputNumberComponent_mat_icon_6_Template, 2, 1, "mat-icon", 6);
            i0.ɵɵtemplate(7, FormFieldInputNumberComponent_span_7_Template, 2, 1, "span", 5);
            i0.ɵɵtemplate(8, FormFieldInputNumberComponent_span_8_Template, 2, 1, "span", 6);
            i0.ɵɵtemplate(9, FormFieldInputNumberComponent_mat_hint_9_Template, 2, 1, "mat-hint", 7);
            i0.ɵɵtemplate(10, FormFieldInputNumberComponent_mat_hint_10_Template, 2, 1, "mat-hint", 8);
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        } if (rf & 2) {
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("appearance", ctx.formFieldInputNumber.getAppearance())("hideRequiredMarker", ctx.formFieldInputNumber.getRequired().hideRequiredMarker);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.formFieldInputNumber.getLabelName());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("type", ctx.formFieldInputNumber.getFormFieldInputType())("placeholder", ctx.formFieldInputNumber.getPlaceholder() ? ctx.formFieldInputNumber.getPlaceholder() : "")("value", ctx.formFieldInputNumber.getDefaultValue())("step", ctx.formFieldInputNumber.getStep())("disabled", ctx.formFieldInputNumber.getDisabled())("readonly", ctx.formFieldInputNumber.getReadonly())("required", ctx.formFieldInputNumber.getRequired().required);
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("ngIf", ctx.formFieldInputNumber.getMatPrefixImgText());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.formFieldInputNumber.getMatSuffixImgText());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.formFieldInputNumber.getTextPrefix());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.formFieldInputNumber.getTextSuffix());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.formFieldInputNumber.getLeftHintLabel());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.formFieldInputNumber.getRightHintLabel());
        } }, directives: [i1.DefaultLayoutAlignDirective, i2.MatFormField, i3.NgIf, i4.MatInput, i2.MatLabel, i5.MatIcon, i2.MatPrefix, i2.MatSuffix, i2.MatHint], styles: ["", ".cursor[_ngcontent-%COMP%] {\n        cursor: pointer;\n    }\n\n    input[type=\"file\"][_ngcontent-%COMP%] {\n        display: none;\n    }\n\n    h3[_ngcontent-%COMP%] {\n        font-size: inherit;\n        margin: 0;\n        font-weight: inherit;\n    }\n\n    mat-form-field[_ngcontent-%COMP%] {\n        min-width: 250px;\n    }"] });
    return FormFieldInputNumberComponent;
}());
export { FormFieldInputNumberComponent };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FormFieldInputNumberComponent, [{
        type: Component,
        args: [{
                selector: "app-form-field-input-number",
                templateUrl: "./form-field-input-number.component.html",
                styleUrls: ["./form-field-input-number.component.css"],
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        multi: true,
                        useExisting: forwardRef(function () { return FormFieldInputNumberComponent; }),
                    },
                ],
            }]
    }], function () { return []; }, { formFieldInputNumber: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1maWVsZC1pbnB1dC1udW1iZXIuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXktYW5ndWxhci1mb3JtLyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC91aS9mb3JtLWZpZWxkLWlucHV0L2Zvcm0tZmllbGQtaW5wdXQtbnVtYmVyL2Zvcm0tZmllbGQtaW5wdXQtbnVtYmVyLmNvbXBvbmVudC50cyIsImxpYi9zaGFyZWQvdWkvZm9ybS1maWVsZC1pbnB1dC9mb3JtLWZpZWxkLWlucHV0LW51bWJlci9mb3JtLWZpZWxkLWlucHV0LW51bWJlci5jb21wb25lbnQuaHRtbCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRSxVQUFVLEVBQWEsTUFBTSxlQUFlLENBQUM7QUFFaEYsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sZ0JBQWdCLENBQUM7Ozs7Ozs7O0lDMkJuQyxpQ0FDcUQ7SUFBQSxZQUE4QztJQUFBLGlCQUFZOzs7SUFBMUQsZUFBOEM7SUFBOUMsZ0VBQThDOzs7SUFlbkcsbUNBRWM7SUFBQSxZQUFxRDtJQUFBLGlCQUFXOzs7SUFBaEUsZUFBcUQ7SUFBckQsdUVBQXFEOzs7SUFFbkUsb0NBRWM7SUFBQSxZQUFxRDtJQUFBLGlCQUFXOzs7SUFBaEUsZUFBcUQ7SUFBckQsdUVBQXFEOzs7SUFFbkUsK0JBRWM7SUFBQSxZQUFxRDtJQUFBLGlCQUFPOzs7SUFBNUQsZUFBcUQ7SUFBckQsZ0ZBQXFEOzs7SUFFbkUsZ0NBRWM7SUFBQSxZQUErQztJQUFBLGlCQUFPOzs7SUFBdEQsZUFBK0M7SUFBL0MsaUVBQStDOzs7SUFFN0Qsb0NBRWtCO0lBQUEsWUFBa0Q7SUFBQSxpQkFBVzs7O0lBQTdELGVBQWtEO0lBQWxELG9FQUFrRDs7O0lBRXBFLG9DQUVnQjtJQUFBLFlBQW1EO0lBQUEsaUJBQVc7OztJQUE5RCxlQUFtRDtJQUFuRCxxRUFBbUQ7O0FEL0RuRjtJQWtCRTtRQUFBLGlCQUFnQjtRQUhULGFBQVEsR0FBUSxjQUFPLENBQUMsQ0FBQztRQUN6QixjQUFTLEdBQVEsY0FBTyxDQUFDLENBQUM7UUFJakMsWUFBTyxHQUFHLFVBQUMsS0FBdUI7WUFDaEMsNEJBQTRCO1lBQzVCLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3ZELEtBQUksQ0FBQyxRQUFRLENBQUMsS0FBSSxDQUFDLG9CQUFvQixDQUFDLGVBQWUsRUFBRSxDQUFDLENBQUM7WUFDM0QsS0FBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ25CLENBQUMsQ0FBQztJQVBhLENBQUM7SUFTaEIsd0RBQWdCLEdBQWhCLFVBQWlCLEVBQUU7UUFDakIsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7SUFDckIsQ0FBQztJQUVELHlEQUFpQixHQUFqQixVQUFrQixFQUFFO1FBQ2xCLElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO0lBQ3RCLENBQUM7SUFFRCxrREFBVSxHQUFWLFVBQVcsS0FBSztRQUNkLElBQUksS0FBSyxFQUFFO1lBQ1QsSUFBSSxDQUFDLG9CQUFvQixDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUNsRDtJQUNILENBQUM7SUFFRCxnREFBUSxHQUFSO1FBQ0UscURBQXFEO0lBQ3ZELENBQUM7SUFFRCxtREFBVyxHQUFYO1FBQ0UsMERBQTBEO0lBQzVELENBQUM7OEdBbkNVLDZCQUE2QjtzRUFBN0IsNkJBQTZCLDJJQVI3QjtnQkFDVDtvQkFDRSxPQUFPLEVBQUUsaUJBQWlCO29CQUMxQixLQUFLLEVBQUUsSUFBSTtvQkFDWCxXQUFXLEVBQUUsVUFBVSxDQUFDLGNBQU0sT0FBQSw2QkFBNkIsRUFBN0IsQ0FBNkIsQ0FBQztpQkFDN0Q7YUFDRjs7WUNRSCxrQ0FHUTtZQUFBLHlDQUlRO1lBQUEsMEZBQ3FEO1lBRXJELG1DQWFBO1lBUkksNkpBQVMsZ0JBQW1CLElBQUMsOElBQ3JCLGdCQUFtQixJQURFO1lBTGpDLGlCQWFBO1lBQUEsd0ZBRWM7WUFFZCx3RkFFYztZQUVkLGdGQUVjO1lBRWQsZ0ZBRWM7WUFFZCx3RkFFa0I7WUFFbEIsMEZBRWdCO1lBRXhCLGlCQUFpQjtZQUV6QixpQkFBVTs7WUE3Q0UsZUFBd0Q7WUFBeEQscUVBQXdELGlGQUFBO1lBSWhELGVBQWdEO1lBQWhELDhEQUFnRDtZQUloRCxlQUEwRDtZQUExRCx1RUFBMEQsMkdBQUEscURBQUEsNENBQUEsb0RBQUEsb0RBQUEsNkRBQUE7WUFZMUQsZUFBdUQ7WUFBdkQscUVBQXVEO1lBSXZELGVBQXVEO1lBQXZELHFFQUF1RDtZQUl2RCxlQUFpRDtZQUFqRCwrREFBaUQ7WUFJakQsZUFBaUQ7WUFBakQsK0RBQWlEO1lBSWpELGVBQW9EO1lBQXBELGtFQUFvRDtZQUlwRCxlQUFxRDtZQUFyRCxtRUFBcUQ7O3dDRGxFekU7Q0FvREMsQUFoREQsSUFnREM7U0FwQ1ksNkJBQTZCO2tEQUE3Qiw2QkFBNkI7Y0FaekMsU0FBUztlQUFDO2dCQUNULFFBQVEsRUFBRSw2QkFBNkI7Z0JBQ3ZDLFdBQVcsRUFBRSwwQ0FBMEM7Z0JBQ3ZELFNBQVMsRUFBRSxDQUFDLHlDQUF5QyxDQUFDO2dCQUN0RCxTQUFTLEVBQUU7b0JBQ1Q7d0JBQ0UsT0FBTyxFQUFFLGlCQUFpQjt3QkFDMUIsS0FBSyxFQUFFLElBQUk7d0JBQ1gsV0FBVyxFQUFFLFVBQVUsQ0FBQyxjQUFNLE9BQUEsNkJBQTZCLEVBQTdCLENBQTZCLENBQUM7cUJBQzdEO2lCQUNGO2FBQ0Y7O2tCQUVFLEtBQUsiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQsIGZvcndhcmRSZWYsIE9uRGVzdHJveSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBGb3JtRmllbGRJbnB1dE51bWJlciB9IGZyb20gXCIuLi8uLi8uLi9tb2RlbC9zdHJ1Y3R1cmFsL2NvbXBvc2l0ZS9mb3JtL2Zvcm0tZmllbGQtaW5wdXQvZm9ybS1maWVsZC1pbnB1dC1udW1iZXIubW9kZWxcIjtcbmltcG9ydCB7IE5HX1ZBTFVFX0FDQ0VTU09SIH0gZnJvbSBcIkBhbmd1bGFyL2Zvcm1zXCI7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogXCJhcHAtZm9ybS1maWVsZC1pbnB1dC1udW1iZXJcIixcbiAgdGVtcGxhdGVVcmw6IFwiLi9mb3JtLWZpZWxkLWlucHV0LW51bWJlci5jb21wb25lbnQuaHRtbFwiLFxuICBzdHlsZVVybHM6IFtcIi4vZm9ybS1maWVsZC1pbnB1dC1udW1iZXIuY29tcG9uZW50LmNzc1wiXSxcbiAgcHJvdmlkZXJzOiBbXG4gICAge1xuICAgICAgcHJvdmlkZTogTkdfVkFMVUVfQUNDRVNTT1IsXG4gICAgICBtdWx0aTogdHJ1ZSxcbiAgICAgIHVzZUV4aXN0aW5nOiBmb3J3YXJkUmVmKCgpID0+IEZvcm1GaWVsZElucHV0TnVtYmVyQ29tcG9uZW50KSxcbiAgICB9LFxuICBdLFxufSlcbmV4cG9ydCBjbGFzcyBGb3JtRmllbGRJbnB1dE51bWJlckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcbiAgQElucHV0KCkgZm9ybUZpZWxkSW5wdXROdW1iZXI6IEZvcm1GaWVsZElucHV0TnVtYmVyO1xuXG4gIHB1YmxpYyBvbkNoYW5nZTogYW55ID0gKCkgPT4ge307XG4gIHB1YmxpYyBvblRvdWNoZWQ6IGFueSA9ICgpID0+IHt9O1xuXG4gIGNvbnN0cnVjdG9yKCkge31cblxuICBvbktleVVwID0gKGlucHV0OiBIVE1MSW5wdXRFbGVtZW50KSA9PiB7XG4gICAgLy8gY29uc29sZS5sb2coaW5wdXQudmFsdWUpO1xuICAgIHRoaXMuZm9ybUZpZWxkSW5wdXROdW1iZXIuc2V0RGVmYXVsdFZhbHVlKGlucHV0LnZhbHVlKTtcbiAgICB0aGlzLm9uQ2hhbmdlKHRoaXMuZm9ybUZpZWxkSW5wdXROdW1iZXIuZ2V0RGVmYXVsdFZhbHVlKCkpO1xuICAgIHRoaXMub25Ub3VjaGVkKCk7XG4gIH07XG5cbiAgcmVnaXN0ZXJPbkNoYW5nZShmbikge1xuICAgIHRoaXMub25DaGFuZ2UgPSBmbjtcbiAgfVxuXG4gIHJlZ2lzdGVyT25Ub3VjaGVkKGZuKSB7XG4gICAgdGhpcy5vblRvdWNoZWQgPSBmbjtcbiAgfVxuXG4gIHdyaXRlVmFsdWUodmFsdWUpIHtcbiAgICBpZiAodmFsdWUpIHtcbiAgICAgIHRoaXMuZm9ybUZpZWxkSW5wdXROdW1iZXIuc2V0RGVmYXVsdFZhbHVlKHZhbHVlKTtcbiAgICB9XG4gIH1cblxuICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgICAvLyBjb25zb2xlLmxvZygnRm9ybUZpZWxkSW5wdXROdW1iZXJDb21wb25lbnQgaW5pdCcpO1xuICB9XG5cbiAgbmdPbkRlc3Ryb3koKTogdm9pZCB7XG4gICAgLy8gY29uc29sZS5sb2coJ0Zvcm1GaWVsZElucHV0TnVtYmVyQ29tcG9uZW50IGRlc3Ryb3llZCcpO1xuICB9XG59XG4iLCI8c3R5bGU+XG5cbiAgICAuY3Vyc29yIHtcbiAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIH1cblxuICAgIGlucHV0W3R5cGU9XCJmaWxlXCJdIHtcbiAgICAgICAgZGlzcGxheTogbm9uZTtcbiAgICB9XG5cbiAgICBoMyB7XG4gICAgICAgIGZvbnQtc2l6ZTogaW5oZXJpdDtcbiAgICAgICAgbWFyZ2luOiAwO1xuICAgICAgICBmb250LXdlaWdodDogaW5oZXJpdDtcbiAgICB9XG5cbiAgICBtYXQtZm9ybS1maWVsZCB7XG4gICAgICAgIG1pbi13aWR0aDogMjUwcHg7XG4gICAgfVxuXG48L3N0eWxlPlxuXG48c2VjdGlvblxuICAgIGZ4TGF5b3V0QWxpZ249XCJjZW50ZXIgY2VudGVyXCI+XG5cbiAgICAgICAgPG1hdC1mb3JtLWZpZWxkIFxuICAgICAgICAgICAgW2FwcGVhcmFuY2VdPVwidGhpcy5mb3JtRmllbGRJbnB1dE51bWJlci5nZXRBcHBlYXJhbmNlKClcIlxuICAgICAgICAgICAgW2hpZGVSZXF1aXJlZE1hcmtlcl09XCJ0aGlzLmZvcm1GaWVsZElucHV0TnVtYmVyLmdldFJlcXVpcmVkKCkuaGlkZVJlcXVpcmVkTWFya2VyXCI+XG5cbiAgICAgICAgICAgICAgICA8bWF0LWxhYmVsXG4gICAgICAgICAgICAgICAgICAgICpuZ0lmPVwidGhpcy5mb3JtRmllbGRJbnB1dE51bWJlci5nZXRMYWJlbE5hbWUoKVwiPnt7IHRoaXMuZm9ybUZpZWxkSW5wdXROdW1iZXIuZ2V0TGFiZWxOYW1lKCkgfX08L21hdC1sYWJlbD5cblxuICAgICAgICAgICAgICAgIDxpbnB1dCBcbiAgICAgICAgICAgICAgICAgICAgbWF0SW5wdXRcbiAgICAgICAgICAgICAgICAgICAgW3R5cGVdPVwidGhpcy5mb3JtRmllbGRJbnB1dE51bWJlci5nZXRGb3JtRmllbGRJbnB1dFR5cGUoKVwiXG4gICAgICAgICAgICAgICAgICAgIFtwbGFjZWhvbGRlcl09XCJ0aGlzLmZvcm1GaWVsZElucHV0TnVtYmVyLmdldFBsYWNlaG9sZGVyKCkgPyB0aGlzLmZvcm1GaWVsZElucHV0TnVtYmVyLmdldFBsYWNlaG9sZGVyKCkgOiAnJ1wiXG4gICAgICAgICAgICAgICAgICAgIFt2YWx1ZV09XCJ0aGlzLmZvcm1GaWVsZElucHV0TnVtYmVyLmdldERlZmF1bHRWYWx1ZSgpXCJcbiAgICAgICAgICAgICAgICAgICAgKGtleXVwKT1cInRoaXMub25LZXlVcChpbnB1dClcIlxuICAgICAgICAgICAgICAgICAgICAoYmx1cik9XCJ0aGlzLm9uS2V5VXAoaW5wdXQpXCJcbiAgICAgICAgICAgICAgICAgICAgW3N0ZXBdPVwidGhpcy5mb3JtRmllbGRJbnB1dE51bWJlci5nZXRTdGVwKClcIlxuICAgICAgICAgICAgICAgICAgICBbZGlzYWJsZWRdPVwidGhpcy5mb3JtRmllbGRJbnB1dE51bWJlci5nZXREaXNhYmxlZCgpXCJcbiAgICAgICAgICAgICAgICAgICAgW3JlYWRvbmx5XT1cInRoaXMuZm9ybUZpZWxkSW5wdXROdW1iZXIuZ2V0UmVhZG9ubHkoKVwiXG4gICAgICAgICAgICAgICAgICAgIFtyZXF1aXJlZF09XCJ0aGlzLmZvcm1GaWVsZElucHV0TnVtYmVyLmdldFJlcXVpcmVkKCkucmVxdWlyZWRcIlxuICAgICAgICAgICAgICAgICAgICAjaW5wdXQ+XG5cbiAgICAgICAgICAgICAgICA8bWF0LWljb24gXG4gICAgICAgICAgICAgICAgICAgICpuZ0lmPVwidGhpcy5mb3JtRmllbGRJbnB1dE51bWJlci5nZXRNYXRQcmVmaXhJbWdUZXh0KClcIlxuICAgICAgICAgICAgICAgICAgICBtYXRQcmVmaXg+e3sgdGhpcy5mb3JtRmllbGRJbnB1dE51bWJlci5nZXRNYXRQcmVmaXhJbWdUZXh0KCkgfX08L21hdC1pY29uPlxuXG4gICAgICAgICAgICAgICAgPG1hdC1pY29uIFxuICAgICAgICAgICAgICAgICAgICAqbmdJZj1cInRoaXMuZm9ybUZpZWxkSW5wdXROdW1iZXIuZ2V0TWF0U3VmZml4SW1nVGV4dCgpXCJcbiAgICAgICAgICAgICAgICAgICAgbWF0U3VmZml4Pnt7IHRoaXMuZm9ybUZpZWxkSW5wdXROdW1iZXIuZ2V0TWF0U3VmZml4SW1nVGV4dCgpIH19PC9tYXQtaWNvbj5cblxuICAgICAgICAgICAgICAgIDxzcGFuIFxuICAgICAgICAgICAgICAgICAgICAqbmdJZj1cInRoaXMuZm9ybUZpZWxkSW5wdXROdW1iZXIuZ2V0VGV4dFByZWZpeCgpXCJcbiAgICAgICAgICAgICAgICAgICAgbWF0UHJlZml4Pnt7IHRoaXMuZm9ybUZpZWxkSW5wdXROdW1iZXIuZ2V0VGV4dFByZWZpeCgpIH19Jm5ic3A7PC9zcGFuPlxuXG4gICAgICAgICAgICAgICAgPHNwYW4gXG4gICAgICAgICAgICAgICAgICAgICpuZ0lmPVwidGhpcy5mb3JtRmllbGRJbnB1dE51bWJlci5nZXRUZXh0U3VmZml4KClcIlxuICAgICAgICAgICAgICAgICAgICBtYXRTdWZmaXg+e3sgdGhpcy5mb3JtRmllbGRJbnB1dE51bWJlci5nZXRUZXh0U3VmZml4KCkgfX08L3NwYW4+XG5cbiAgICAgICAgICAgICAgICA8bWF0LWhpbnQgXG4gICAgICAgICAgICAgICAgICAgICpuZ0lmPVwidGhpcy5mb3JtRmllbGRJbnB1dE51bWJlci5nZXRMZWZ0SGludExhYmVsKClcIlxuICAgICAgICAgICAgICAgICAgICBhbGlnbj1cInN0YXJ0XCI+e3sgdGhpcy5mb3JtRmllbGRJbnB1dE51bWJlci5nZXRMZWZ0SGludExhYmVsKCkgfX08L21hdC1oaW50PlxuXG4gICAgICAgICAgICAgICAgPG1hdC1oaW50XG4gICAgICAgICAgICAgICAgICAgICpuZ0lmPVwidGhpcy5mb3JtRmllbGRJbnB1dE51bWJlci5nZXRSaWdodEhpbnRMYWJlbCgpXCJcbiAgICAgICAgICAgICAgICAgICAgYWxpZ249XCJlbmRcIj57eyB0aGlzLmZvcm1GaWVsZElucHV0TnVtYmVyLmdldFJpZ2h0SGludExhYmVsKCkgfX08L21hdC1oaW50PlxuXG4gICAgICAgIDwvbWF0LWZvcm0tZmllbGQ+XG5cbjwvc2VjdGlvbj4iXX0=