import { Component, Input, forwardRef } from "@angular/core";
import { VALIDATOR_NAMES, } from "../../../model/structural/composite/form/form-row-item.model";
import { NG_VALUE_ACCESSOR } from "@angular/forms";
import * as i0 from "@angular/core";
import * as i1 from "@angular/flex-layout/flex";
import * as i2 from "@angular/material/form-field";
import * as i3 from "@angular/common";
import * as i4 from "@angular/material/input";
import * as i5 from "@angular/material/icon";
function FormFieldInputPasswordComponent_mat_label_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-label");
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r0 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r0.formFieldInputPassword.getLabelName());
} }
function FormFieldInputPasswordComponent_mat_icon_5_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-icon", 10);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r2 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r2.formFieldInputPassword.getMatPrefixImgText());
} }
function FormFieldInputPasswordComponent_mat_icon_6_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-icon", 11);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r3 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r3.formFieldInputPassword.getMatSuffixImgText());
} }
function FormFieldInputPasswordComponent_mat_icon_7_Template(rf, ctx) { if (rf & 1) {
    var _r13 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "mat-icon", 12);
    i0.ɵɵlistener("click", function FormFieldInputPasswordComponent_mat_icon_7_Template_mat_icon_click_0_listener() { i0.ɵɵrestoreView(_r13); var ctx_r12 = i0.ɵɵnextContext(); return ctx_r12.formFieldInputPassword.onShowPassword(); });
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r4 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r4.formFieldInputPassword.getShowPassword() ? "visibility" : "visibility_off", " ");
} }
function FormFieldInputPasswordComponent_span_8_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 10);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r5 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1("", ctx_r5.formFieldInputPassword.getTextPrefix(), "\u00A0");
} }
function FormFieldInputPasswordComponent_span_9_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 11);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r6 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r6.formFieldInputPassword.getTextSuffix());
} }
function FormFieldInputPasswordComponent_mat_hint_10_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-hint", 13);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r7 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r7.formFieldInputPassword.getLeftHintLabel());
} }
function FormFieldInputPasswordComponent_mat_hint_11_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-hint", 14);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r8 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r8.formFieldInputPassword.getRightHintLabel());
} }
function FormFieldInputPasswordComponent_mat_hint_12_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-hint", 14);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r9 = i0.ɵɵnextContext();
    var tmp_0_0 = null;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate3("", ((tmp_0_0 = ctx_r9.getMinLengthValidator()) == null ? null : tmp_0_0.length) ? ctx_r9.getMinLengthValidator().length + " / " : "", "", (tmp_0_0 = ctx_r9.formFieldInputPassword.getDefaultValue()) == null ? null : tmp_0_0.length, "", ((tmp_0_0 = ctx_r9.getMaxLengthValidator()) == null ? null : tmp_0_0.length) ? " / " + ctx_r9.getMaxLengthValidator().length : "", "");
} }
function FormFieldInputPasswordComponent_mat_hint_13_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-hint", 14);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r10 = i0.ɵɵnextContext();
    var tmp_0_0 = null;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate2("", (tmp_0_0 = ctx_r10.formFieldInputPassword.getDefaultValue()) == null ? null : tmp_0_0.length, "", ((tmp_0_0 = ctx_r10.getMinLengthValidator()) == null ? null : tmp_0_0.length) ? " / " + ctx_r10.getMinLengthValidator().length : "", "");
} }
function FormFieldInputPasswordComponent_mat_hint_14_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-hint", 14);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r11 = i0.ɵɵnextContext();
    var tmp_0_0 = null;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate2("", (tmp_0_0 = ctx_r11.formFieldInputPassword.getDefaultValue()) == null ? null : tmp_0_0.length, "", ((tmp_0_0 = ctx_r11.getMaxLengthValidator()) == null ? null : tmp_0_0.length) ? " / " + ctx_r11.getMaxLengthValidator().length : "", "");
} }
var FormFieldInputPasswordComponent = /** @class */ (function () {
    function FormFieldInputPasswordComponent() {
        var _this = this;
        this.onChange = function () { };
        this.onTouched = function () { };
        this.onKeyUp = function (input) {
            _this.formFieldInputPassword.setDefaultValue(input.value);
            _this.onChange(_this.formFieldInputPassword.getDefaultValue());
            _this.onTouched();
        };
        this.getMinLengthValidator = function () {
            var theValidator;
            _this.formFieldInputPassword
                .getValidators()
                .forEach(function (validator) {
                if (validator.name === VALIDATOR_NAMES.MIN_LENGTH)
                    theValidator = validator;
            });
            return theValidator;
        };
        this.getMaxLengthValidator = function () {
            var theValidator;
            _this.formFieldInputPassword
                .getValidators()
                .forEach(function (validator) {
                if (validator.name === VALIDATOR_NAMES.MAX_LENGTH)
                    theValidator = validator;
            });
            return theValidator;
        };
    }
    FormFieldInputPasswordComponent.prototype.registerOnChange = function (fn) {
        this.onChange = fn;
    };
    FormFieldInputPasswordComponent.prototype.registerOnTouched = function (fn) {
        this.onTouched = fn;
    };
    FormFieldInputPasswordComponent.prototype.writeValue = function (value) {
        if (value) {
            this.formFieldInputPassword.setDefaultValue(value);
        }
    };
    FormFieldInputPasswordComponent.prototype.ngOnInit = function () {
        // console.log('FormFieldInputPasswordComponent init');
    };
    FormFieldInputPasswordComponent.prototype.ngOnDestroy = function () {
        // console.log('FormFieldInputPasswordComponent destroyed');
    };
    FormFieldInputPasswordComponent.ɵfac = function FormFieldInputPasswordComponent_Factory(t) { return new (t || FormFieldInputPasswordComponent)(); };
    FormFieldInputPasswordComponent.ɵcmp = i0.ɵɵdefineComponent({ type: FormFieldInputPasswordComponent, selectors: [["app-form-field-input-password"]], inputs: { formFieldInputPassword: "formFieldInputPassword" }, features: [i0.ɵɵProvidersFeature([
                {
                    provide: NG_VALUE_ACCESSOR,
                    multi: true,
                    useExisting: forwardRef(function () { return FormFieldInputPasswordComponent; }),
                },
            ])], decls: 15, vars: 19, consts: [["fxLayoutAlign", "center center"], [3, "appearance", "hideRequiredMarker"], [4, "ngIf"], ["matInput", "", 3, "type", "placeholder", "value", "disabled", "readonly", "required", "keyup", "blur"], ["input", ""], ["matPrefix", "", 4, "ngIf"], ["matSuffix", "", 4, "ngIf"], ["class", "cursor", "matSuffix", "", 3, "click", 4, "ngIf"], ["align", "start", 4, "ngIf"], ["align", "end", 4, "ngIf"], ["matPrefix", ""], ["matSuffix", ""], ["matSuffix", "", 1, "cursor", 3, "click"], ["align", "start"], ["align", "end"]], template: function FormFieldInputPasswordComponent_Template(rf, ctx) { if (rf & 1) {
            var _r14 = i0.ɵɵgetCurrentView();
            i0.ɵɵelementStart(0, "section", 0);
            i0.ɵɵelementStart(1, "mat-form-field", 1);
            i0.ɵɵtemplate(2, FormFieldInputPasswordComponent_mat_label_2_Template, 2, 1, "mat-label", 2);
            i0.ɵɵelementStart(3, "input", 3, 4);
            i0.ɵɵlistener("keyup", function FormFieldInputPasswordComponent_Template_input_keyup_3_listener() { i0.ɵɵrestoreView(_r14); var _r1 = i0.ɵɵreference(4); return ctx.onKeyUp(_r1); })("blur", function FormFieldInputPasswordComponent_Template_input_blur_3_listener() { i0.ɵɵrestoreView(_r14); var _r1 = i0.ɵɵreference(4); return ctx.onKeyUp(_r1); });
            i0.ɵɵelementEnd();
            i0.ɵɵtemplate(5, FormFieldInputPasswordComponent_mat_icon_5_Template, 2, 1, "mat-icon", 5);
            i0.ɵɵtemplate(6, FormFieldInputPasswordComponent_mat_icon_6_Template, 2, 1, "mat-icon", 6);
            i0.ɵɵtemplate(7, FormFieldInputPasswordComponent_mat_icon_7_Template, 2, 1, "mat-icon", 7);
            i0.ɵɵtemplate(8, FormFieldInputPasswordComponent_span_8_Template, 2, 1, "span", 5);
            i0.ɵɵtemplate(9, FormFieldInputPasswordComponent_span_9_Template, 2, 1, "span", 6);
            i0.ɵɵtemplate(10, FormFieldInputPasswordComponent_mat_hint_10_Template, 2, 1, "mat-hint", 8);
            i0.ɵɵtemplate(11, FormFieldInputPasswordComponent_mat_hint_11_Template, 2, 1, "mat-hint", 9);
            i0.ɵɵtemplate(12, FormFieldInputPasswordComponent_mat_hint_12_Template, 2, 3, "mat-hint", 9);
            i0.ɵɵtemplate(13, FormFieldInputPasswordComponent_mat_hint_13_Template, 2, 2, "mat-hint", 9);
            i0.ɵɵtemplate(14, FormFieldInputPasswordComponent_mat_hint_14_Template, 2, 2, "mat-hint", 9);
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        } if (rf & 2) {
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("appearance", ctx.formFieldInputPassword.getAppearance())("hideRequiredMarker", ctx.formFieldInputPassword.getRequired().hideRequiredMarker);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.formFieldInputPassword.getLabelName());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("type", ctx.formFieldInputPassword.getShowPasswordInMs() && ctx.formFieldInputPassword.getShowPassword() ? "text" : ctx.formFieldInputPassword.getFormFieldInputType())("placeholder", ctx.formFieldInputPassword.getPlaceholder() ? ctx.formFieldInputPassword.getPlaceholder() : "")("value", ctx.formFieldInputPassword.getDefaultValue())("disabled", ctx.formFieldInputPassword.getDisabled())("readonly", ctx.formFieldInputPassword.getReadonly())("required", ctx.formFieldInputPassword.getRequired().required);
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("ngIf", ctx.formFieldInputPassword.getMatPrefixImgText());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.formFieldInputPassword.getMatSuffixImgText());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.formFieldInputPassword.getShowPasswordInMs());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.formFieldInputPassword.getTextPrefix());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.formFieldInputPassword.getTextSuffix());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.formFieldInputPassword.getLeftHintLabel());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.formFieldInputPassword.getRightHintLabel());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.formFieldInputPassword.getShowHintAboutMinMaxLength() && ctx.getMinLengthValidator() && ctx.getMaxLengthValidator());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.formFieldInputPassword.getShowHintAboutMinMaxLength() && ctx.getMinLengthValidator() && !ctx.getMaxLengthValidator());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.formFieldInputPassword.getShowHintAboutMinMaxLength() && !ctx.getMinLengthValidator() && ctx.getMaxLengthValidator());
        } }, directives: [i1.DefaultLayoutAlignDirective, i2.MatFormField, i3.NgIf, i4.MatInput, i2.MatLabel, i5.MatIcon, i2.MatPrefix, i2.MatSuffix, i2.MatHint], styles: ["", ".cursor[_ngcontent-%COMP%] {\n        cursor: pointer;\n    }\n\n    input[type=\"file\"][_ngcontent-%COMP%] {\n        display: none;\n    }\n\n    h3[_ngcontent-%COMP%] {\n        font-size: inherit;\n        margin: 0;\n        font-weight: inherit;\n    }\n\n    mat-form-field[_ngcontent-%COMP%] {\n        min-width: 250px;\n    }"] });
    return FormFieldInputPasswordComponent;
}());
export { FormFieldInputPasswordComponent };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FormFieldInputPasswordComponent, [{
        type: Component,
        args: [{
                selector: "app-form-field-input-password",
                templateUrl: "./form-field-input-password.component.html",
                styleUrls: ["./form-field-input-password.component.css"],
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        multi: true,
                        useExisting: forwardRef(function () { return FormFieldInputPasswordComponent; }),
                    },
                ],
            }]
    }], function () { return []; }, { formFieldInputPassword: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1maWVsZC1pbnB1dC1wYXNzd29yZC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9teS1hbmd1bGFyLWZvcm0vIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL3VpL2Zvcm0tZmllbGQtaW5wdXQvZm9ybS1maWVsZC1pbnB1dC1wYXNzd29yZC9mb3JtLWZpZWxkLWlucHV0LXBhc3N3b3JkLmNvbXBvbmVudC50cyIsImxpYi9zaGFyZWQvdWkvZm9ybS1maWVsZC1pbnB1dC9mb3JtLWZpZWxkLWlucHV0LXBhc3N3b3JkL2Zvcm0tZmllbGQtaW5wdXQtcGFzc3dvcmQuY29tcG9uZW50Lmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBcUIsS0FBSyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUVoRixPQUFPLEVBRUwsZUFBZSxHQUNoQixNQUFNLDhEQUE4RCxDQUFDO0FBQ3RFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGdCQUFnQixDQUFDOzs7Ozs7OztJQ3VCbkMsaUNBQ3VEO0lBQUEsWUFBZ0Q7SUFBQSxpQkFBWTs7O0lBQTVELGVBQWdEO0lBQWhELGtFQUFnRDs7O0lBY3ZHLG9DQUVjO0lBQUEsWUFBdUQ7SUFBQSxpQkFBVzs7O0lBQWxFLGVBQXVEO0lBQXZELHlFQUF1RDs7O0lBRXJFLG9DQUVjO0lBQUEsWUFBdUQ7SUFBQSxpQkFBVzs7O0lBQWxFLGVBQXVEO0lBQXZELHlFQUF1RDs7OztJQUVyRSxvQ0FJZTtJQURYLG1MQUFTLCtDQUE0QyxJQUFDO0lBQzNDLFlBQXFGO0lBQUEsaUJBQVc7OztJQUFoRyxlQUFxRjtJQUFyRixrSEFBcUY7OztJQUVwRyxnQ0FFYztJQUFBLFlBQXVEO0lBQUEsaUJBQU87OztJQUE5RCxlQUF1RDtJQUF2RCxrRkFBdUQ7OztJQUVyRSxnQ0FFYztJQUFBLFlBQWlEO0lBQUEsaUJBQU87OztJQUF4RCxlQUFpRDtJQUFqRCxtRUFBaUQ7OztJQUUvRCxvQ0FFa0I7SUFBQSxZQUFvRDtJQUFBLGlCQUFXOzs7SUFBL0QsZUFBb0Q7SUFBcEQsc0VBQW9EOzs7SUFFdEUsb0NBRWdCO0lBQUEsWUFBcUQ7SUFBQSxpQkFBVzs7O0lBQWhFLGVBQXFEO0lBQXJELHVFQUFxRDs7O0lBR3JFLG9DQUVnQjtJQUFBLFlBQWtQO0lBQUEsaUJBQVc7Ozs7SUFBN1AsZUFBa1A7SUFBbFAsd1lBQWtQOzs7SUFFbFEsb0NBRWdCO0lBQUEsWUFBdUo7SUFBQSxpQkFBVzs7OztJQUFsSyxlQUF1SjtJQUF2SixvUUFBdUo7OztJQUV2SyxvQ0FFZ0I7SUFBQSxZQUFzSjtJQUFBLGlCQUFXOzs7O0lBQWpLLGVBQXNKO0lBQXRKLG9RQUFzSjs7QUQ3RXRMO0lBa0JFO1FBQUEsaUJBQWdCO1FBSFQsYUFBUSxHQUFRLGNBQU8sQ0FBQyxDQUFDO1FBQ3pCLGNBQVMsR0FBUSxjQUFPLENBQUMsQ0FBQztRQUlqQyxZQUFPLEdBQUcsVUFBQyxLQUF1QjtZQUNoQyxLQUFJLENBQUMsc0JBQXNCLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN6RCxLQUFJLENBQUMsUUFBUSxDQUFDLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxlQUFlLEVBQUUsQ0FBQyxDQUFDO1lBQzdELEtBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUNuQixDQUFDLENBQUM7UUFnQkYsMEJBQXFCLEdBQUc7WUFDdEIsSUFBSSxZQUFZLENBQUM7WUFDakIsS0FBSSxDQUFDLHNCQUFzQjtpQkFDeEIsYUFBYSxFQUFFO2lCQUNmLE9BQU8sQ0FBQyxVQUFDLFNBQXdDO2dCQUNoRCxJQUFJLFNBQVMsQ0FBQyxJQUFJLEtBQUssZUFBZSxDQUFDLFVBQVU7b0JBQy9DLFlBQVksR0FBRyxTQUFTLENBQUM7WUFDN0IsQ0FBQyxDQUFDLENBQUM7WUFDTCxPQUFPLFlBQVksQ0FBQztRQUN0QixDQUFDLENBQUM7UUFFRiwwQkFBcUIsR0FBRztZQUN0QixJQUFJLFlBQVksQ0FBQztZQUNqQixLQUFJLENBQUMsc0JBQXNCO2lCQUN4QixhQUFhLEVBQUU7aUJBQ2YsT0FBTyxDQUFDLFVBQUMsU0FBd0M7Z0JBQ2hELElBQUksU0FBUyxDQUFDLElBQUksS0FBSyxlQUFlLENBQUMsVUFBVTtvQkFDL0MsWUFBWSxHQUFHLFNBQVMsQ0FBQztZQUM3QixDQUFDLENBQUMsQ0FBQztZQUNMLE9BQU8sWUFBWSxDQUFDO1FBQ3RCLENBQUMsQ0FBQztJQTFDYSxDQUFDO0lBUWhCLDBEQUFnQixHQUFoQixVQUFpQixFQUFFO1FBQ2pCLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO0lBQ3JCLENBQUM7SUFFRCwyREFBaUIsR0FBakIsVUFBa0IsRUFBRTtRQUNsQixJQUFJLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztJQUN0QixDQUFDO0lBRUQsb0RBQVUsR0FBVixVQUFXLEtBQUs7UUFDZCxJQUFJLEtBQUssRUFBRTtZQUNULElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDcEQ7SUFDSCxDQUFDO0lBd0JELGtEQUFRLEdBQVI7UUFDRSx1REFBdUQ7SUFDekQsQ0FBQztJQUVELHFEQUFXLEdBQVg7UUFDRSw0REFBNEQ7SUFDOUQsQ0FBQztrSEF4RFUsK0JBQStCO3dFQUEvQiwrQkFBK0IsaUpBUi9CO2dCQUNUO29CQUNFLE9BQU8sRUFBRSxpQkFBaUI7b0JBQzFCLEtBQUssRUFBRSxJQUFJO29CQUNYLFdBQVcsRUFBRSxVQUFVLENBQUMsY0FBTSxPQUFBLCtCQUErQixFQUEvQixDQUErQixDQUFDO2lCQUMvRDthQUNGOztZQ0lILGtDQUdRO1lBQUEseUNBSVE7WUFBQSw0RkFDdUQ7WUFFdkQsbUNBWUE7WUFQSSxnS0FBUyxnQkFBbUIsSUFBQyxpSkFDckIsZ0JBQW1CLElBREU7WUFMakMsaUJBWUE7WUFBQSwwRkFFYztZQUVkLDBGQUVjO1lBRWQsMEZBSWU7WUFFZixrRkFFYztZQUVkLGtGQUVjO1lBRWQsNEZBRWtCO1lBRWxCLDRGQUVnQjtZQUdoQiw0RkFFZ0I7WUFFaEIsNEZBRWdCO1lBRWhCLDRGQUVnQjtZQUV4QixpQkFBaUI7WUFFekIsaUJBQVU7O1lBL0RFLGVBQTBEO1lBQTFELHVFQUEwRCxtRkFBQTtZQUlsRCxlQUFrRDtZQUFsRCxnRUFBa0Q7WUFJbEQsZUFBMEs7WUFBMUsscUxBQTBLLCtHQUFBLHVEQUFBLHNEQUFBLHNEQUFBLCtEQUFBO1lBVzFLLGVBQXlEO1lBQXpELHVFQUF5RDtZQUl6RCxlQUF5RDtZQUF6RCx1RUFBeUQ7WUFLekQsZUFBeUQ7WUFBekQsdUVBQXlEO1lBS3pELGVBQW1EO1lBQW5ELGlFQUFtRDtZQUluRCxlQUFtRDtZQUFuRCxpRUFBbUQ7WUFJbkQsZUFBc0Q7WUFBdEQsb0VBQXNEO1lBSXRELGVBQXVEO1lBQXZELHFFQUF1RDtZQUt2RCxlQUFrSTtZQUFsSSw4SUFBa0k7WUFJbEksZUFBbUk7WUFBbkksK0lBQW1JO1lBSW5JLGVBQW1JO1lBQW5JLCtJQUFtSTs7MENEcEZ2SjtDQTZFQyxBQXJFRCxJQXFFQztTQXpEWSwrQkFBK0I7a0RBQS9CLCtCQUErQjtjQVozQyxTQUFTO2VBQUM7Z0JBQ1QsUUFBUSxFQUFFLCtCQUErQjtnQkFDekMsV0FBVyxFQUFFLDRDQUE0QztnQkFDekQsU0FBUyxFQUFFLENBQUMsMkNBQTJDLENBQUM7Z0JBQ3hELFNBQVMsRUFBRTtvQkFDVDt3QkFDRSxPQUFPLEVBQUUsaUJBQWlCO3dCQUMxQixLQUFLLEVBQUUsSUFBSTt3QkFDWCxXQUFXLEVBQUUsVUFBVSxDQUFDLGNBQU0sT0FBQSwrQkFBK0IsRUFBL0IsQ0FBK0IsQ0FBQztxQkFDL0Q7aUJBQ0Y7YUFDRjs7a0JBRUUsS0FBSyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBPbkRlc3Ryb3ksIElucHV0LCBmb3J3YXJkUmVmIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IEZvcm1GaWVsZElucHV0UGFzc3dvcmQgfSBmcm9tIFwiLi4vLi4vLi4vbW9kZWwvc3RydWN0dXJhbC9jb21wb3NpdGUvZm9ybS9mb3JtLWZpZWxkLWlucHV0L2Zvcm0tZmllbGQtaW5wdXQtcGFzc3dvcmQubW9kZWxcIjtcbmltcG9ydCB7XG4gIEZvcm1Sb3dJdGVtVmFsaWRhdG9ySW50ZXJmYWNlLFxuICBWQUxJREFUT1JfTkFNRVMsXG59IGZyb20gXCIuLi8uLi8uLi9tb2RlbC9zdHJ1Y3R1cmFsL2NvbXBvc2l0ZS9mb3JtL2Zvcm0tcm93LWl0ZW0ubW9kZWxcIjtcbmltcG9ydCB7IE5HX1ZBTFVFX0FDQ0VTU09SIH0gZnJvbSBcIkBhbmd1bGFyL2Zvcm1zXCI7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogXCJhcHAtZm9ybS1maWVsZC1pbnB1dC1wYXNzd29yZFwiLFxuICB0ZW1wbGF0ZVVybDogXCIuL2Zvcm0tZmllbGQtaW5wdXQtcGFzc3dvcmQuY29tcG9uZW50Lmh0bWxcIixcbiAgc3R5bGVVcmxzOiBbXCIuL2Zvcm0tZmllbGQtaW5wdXQtcGFzc3dvcmQuY29tcG9uZW50LmNzc1wiXSxcbiAgcHJvdmlkZXJzOiBbXG4gICAge1xuICAgICAgcHJvdmlkZTogTkdfVkFMVUVfQUNDRVNTT1IsXG4gICAgICBtdWx0aTogdHJ1ZSxcbiAgICAgIHVzZUV4aXN0aW5nOiBmb3J3YXJkUmVmKCgpID0+IEZvcm1GaWVsZElucHV0UGFzc3dvcmRDb21wb25lbnQpLFxuICAgIH0sXG4gIF0sXG59KVxuZXhwb3J0IGNsYXNzIEZvcm1GaWVsZElucHV0UGFzc3dvcmRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XG4gIEBJbnB1dCgpIGZvcm1GaWVsZElucHV0UGFzc3dvcmQ6IEZvcm1GaWVsZElucHV0UGFzc3dvcmQ7XG5cbiAgcHVibGljIG9uQ2hhbmdlOiBhbnkgPSAoKSA9PiB7fTtcbiAgcHVibGljIG9uVG91Y2hlZDogYW55ID0gKCkgPT4ge307XG5cbiAgY29uc3RydWN0b3IoKSB7fVxuXG4gIG9uS2V5VXAgPSAoaW5wdXQ6IEhUTUxJbnB1dEVsZW1lbnQpID0+IHtcbiAgICB0aGlzLmZvcm1GaWVsZElucHV0UGFzc3dvcmQuc2V0RGVmYXVsdFZhbHVlKGlucHV0LnZhbHVlKTtcbiAgICB0aGlzLm9uQ2hhbmdlKHRoaXMuZm9ybUZpZWxkSW5wdXRQYXNzd29yZC5nZXREZWZhdWx0VmFsdWUoKSk7XG4gICAgdGhpcy5vblRvdWNoZWQoKTtcbiAgfTtcblxuICByZWdpc3Rlck9uQ2hhbmdlKGZuKSB7XG4gICAgdGhpcy5vbkNoYW5nZSA9IGZuO1xuICB9XG5cbiAgcmVnaXN0ZXJPblRvdWNoZWQoZm4pIHtcbiAgICB0aGlzLm9uVG91Y2hlZCA9IGZuO1xuICB9XG5cbiAgd3JpdGVWYWx1ZSh2YWx1ZSkge1xuICAgIGlmICh2YWx1ZSkge1xuICAgICAgdGhpcy5mb3JtRmllbGRJbnB1dFBhc3N3b3JkLnNldERlZmF1bHRWYWx1ZSh2YWx1ZSk7XG4gICAgfVxuICB9XG5cbiAgZ2V0TWluTGVuZ3RoVmFsaWRhdG9yID0gKCk6IEZvcm1Sb3dJdGVtVmFsaWRhdG9ySW50ZXJmYWNlID0+IHtcbiAgICBsZXQgdGhlVmFsaWRhdG9yO1xuICAgIHRoaXMuZm9ybUZpZWxkSW5wdXRQYXNzd29yZFxuICAgICAgLmdldFZhbGlkYXRvcnMoKVxuICAgICAgLmZvckVhY2goKHZhbGlkYXRvcjogRm9ybVJvd0l0ZW1WYWxpZGF0b3JJbnRlcmZhY2UpID0+IHtcbiAgICAgICAgaWYgKHZhbGlkYXRvci5uYW1lID09PSBWQUxJREFUT1JfTkFNRVMuTUlOX0xFTkdUSClcbiAgICAgICAgICB0aGVWYWxpZGF0b3IgPSB2YWxpZGF0b3I7XG4gICAgICB9KTtcbiAgICByZXR1cm4gdGhlVmFsaWRhdG9yO1xuICB9O1xuXG4gIGdldE1heExlbmd0aFZhbGlkYXRvciA9ICgpOiBGb3JtUm93SXRlbVZhbGlkYXRvckludGVyZmFjZSA9PiB7XG4gICAgbGV0IHRoZVZhbGlkYXRvcjtcbiAgICB0aGlzLmZvcm1GaWVsZElucHV0UGFzc3dvcmRcbiAgICAgIC5nZXRWYWxpZGF0b3JzKClcbiAgICAgIC5mb3JFYWNoKCh2YWxpZGF0b3I6IEZvcm1Sb3dJdGVtVmFsaWRhdG9ySW50ZXJmYWNlKSA9PiB7XG4gICAgICAgIGlmICh2YWxpZGF0b3IubmFtZSA9PT0gVkFMSURBVE9SX05BTUVTLk1BWF9MRU5HVEgpXG4gICAgICAgICAgdGhlVmFsaWRhdG9yID0gdmFsaWRhdG9yO1xuICAgICAgfSk7XG4gICAgcmV0dXJuIHRoZVZhbGlkYXRvcjtcbiAgfTtcblxuICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgICAvLyBjb25zb2xlLmxvZygnRm9ybUZpZWxkSW5wdXRQYXNzd29yZENvbXBvbmVudCBpbml0Jyk7XG4gIH1cblxuICBuZ09uRGVzdHJveSgpOiB2b2lkIHtcbiAgICAvLyBjb25zb2xlLmxvZygnRm9ybUZpZWxkSW5wdXRQYXNzd29yZENvbXBvbmVudCBkZXN0cm95ZWQnKTtcbiAgfVxufVxuIiwiPHN0eWxlPlxuXG4gICAgLmN1cnNvciB7XG4gICAgICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICB9XG5cbiAgICBpbnB1dFt0eXBlPVwiZmlsZVwiXSB7XG4gICAgICAgIGRpc3BsYXk6IG5vbmU7XG4gICAgfVxuXG4gICAgaDMge1xuICAgICAgICBmb250LXNpemU6IGluaGVyaXQ7XG4gICAgICAgIG1hcmdpbjogMDtcbiAgICAgICAgZm9udC13ZWlnaHQ6IGluaGVyaXQ7XG4gICAgfVxuXG4gICAgbWF0LWZvcm0tZmllbGQge1xuICAgICAgICBtaW4td2lkdGg6IDI1MHB4O1xuICAgIH1cblxuPC9zdHlsZT5cblxuPHNlY3Rpb25cbiAgICBmeExheW91dEFsaWduPVwiY2VudGVyIGNlbnRlclwiPlxuXG4gICAgICAgIDxtYXQtZm9ybS1maWVsZCBcbiAgICAgICAgICAgIFthcHBlYXJhbmNlXT1cInRoaXMuZm9ybUZpZWxkSW5wdXRQYXNzd29yZC5nZXRBcHBlYXJhbmNlKClcIlxuICAgICAgICAgICAgW2hpZGVSZXF1aXJlZE1hcmtlcl09XCJ0aGlzLmZvcm1GaWVsZElucHV0UGFzc3dvcmQuZ2V0UmVxdWlyZWQoKS5oaWRlUmVxdWlyZWRNYXJrZXJcIj5cblxuICAgICAgICAgICAgICAgIDxtYXQtbGFiZWxcbiAgICAgICAgICAgICAgICAgICAgKm5nSWY9XCJ0aGlzLmZvcm1GaWVsZElucHV0UGFzc3dvcmQuZ2V0TGFiZWxOYW1lKClcIj57eyB0aGlzLmZvcm1GaWVsZElucHV0UGFzc3dvcmQuZ2V0TGFiZWxOYW1lKCkgfX08L21hdC1sYWJlbD5cblxuICAgICAgICAgICAgICAgIDxpbnB1dCBcbiAgICAgICAgICAgICAgICAgICAgbWF0SW5wdXRcbiAgICAgICAgICAgICAgICAgICAgW3R5cGVdPVwidGhpcy5mb3JtRmllbGRJbnB1dFBhc3N3b3JkLmdldFNob3dQYXNzd29yZEluTXMoKSAmJiB0aGlzLmZvcm1GaWVsZElucHV0UGFzc3dvcmQuZ2V0U2hvd1Bhc3N3b3JkKCkgPyAndGV4dCcgOiB0aGlzLmZvcm1GaWVsZElucHV0UGFzc3dvcmQuZ2V0Rm9ybUZpZWxkSW5wdXRUeXBlKClcIlxuICAgICAgICAgICAgICAgICAgICBbcGxhY2Vob2xkZXJdPVwidGhpcy5mb3JtRmllbGRJbnB1dFBhc3N3b3JkLmdldFBsYWNlaG9sZGVyKCkgPyB0aGlzLmZvcm1GaWVsZElucHV0UGFzc3dvcmQuZ2V0UGxhY2Vob2xkZXIoKSA6ICcnXCJcbiAgICAgICAgICAgICAgICAgICAgW3ZhbHVlXT1cInRoaXMuZm9ybUZpZWxkSW5wdXRQYXNzd29yZC5nZXREZWZhdWx0VmFsdWUoKVwiXG4gICAgICAgICAgICAgICAgICAgIChrZXl1cCk9XCJ0aGlzLm9uS2V5VXAoaW5wdXQpXCJcbiAgICAgICAgICAgICAgICAgICAgKGJsdXIpPVwidGhpcy5vbktleVVwKGlucHV0KVwiXG4gICAgICAgICAgICAgICAgICAgIFtkaXNhYmxlZF09XCJ0aGlzLmZvcm1GaWVsZElucHV0UGFzc3dvcmQuZ2V0RGlzYWJsZWQoKVwiXG4gICAgICAgICAgICAgICAgICAgIFtyZWFkb25seV09XCJ0aGlzLmZvcm1GaWVsZElucHV0UGFzc3dvcmQuZ2V0UmVhZG9ubHkoKVwiXG4gICAgICAgICAgICAgICAgICAgIFtyZXF1aXJlZF09XCJ0aGlzLmZvcm1GaWVsZElucHV0UGFzc3dvcmQuZ2V0UmVxdWlyZWQoKS5yZXF1aXJlZFwiXG4gICAgICAgICAgICAgICAgICAgICNpbnB1dD5cblxuICAgICAgICAgICAgICAgIDxtYXQtaWNvbiBcbiAgICAgICAgICAgICAgICAgICAgKm5nSWY9XCJ0aGlzLmZvcm1GaWVsZElucHV0UGFzc3dvcmQuZ2V0TWF0UHJlZml4SW1nVGV4dCgpXCJcbiAgICAgICAgICAgICAgICAgICAgbWF0UHJlZml4Pnt7IHRoaXMuZm9ybUZpZWxkSW5wdXRQYXNzd29yZC5nZXRNYXRQcmVmaXhJbWdUZXh0KCkgfX08L21hdC1pY29uPlxuXG4gICAgICAgICAgICAgICAgPG1hdC1pY29uIFxuICAgICAgICAgICAgICAgICAgICAqbmdJZj1cInRoaXMuZm9ybUZpZWxkSW5wdXRQYXNzd29yZC5nZXRNYXRTdWZmaXhJbWdUZXh0KClcIlxuICAgICAgICAgICAgICAgICAgICBtYXRTdWZmaXg+e3sgdGhpcy5mb3JtRmllbGRJbnB1dFBhc3N3b3JkLmdldE1hdFN1ZmZpeEltZ1RleHQoKSB9fTwvbWF0LWljb24+XG5cbiAgICAgICAgICAgICAgICA8bWF0LWljb25cbiAgICAgICAgICAgICAgICAgICAgY2xhc3M9XCJjdXJzb3JcIlxuICAgICAgICAgICAgICAgICAgICAqbmdJZj1cInRoaXMuZm9ybUZpZWxkSW5wdXRQYXNzd29yZC5nZXRTaG93UGFzc3dvcmRJbk1zKClcIlxuICAgICAgICAgICAgICAgICAgICAoY2xpY2spPVwidGhpcy5mb3JtRmllbGRJbnB1dFBhc3N3b3JkLm9uU2hvd1Bhc3N3b3JkKClcIlxuICAgICAgICAgICAgICAgICAgICBtYXRTdWZmaXg+IHt7IHRoaXMuZm9ybUZpZWxkSW5wdXRQYXNzd29yZC5nZXRTaG93UGFzc3dvcmQoKSA/ICd2aXNpYmlsaXR5JyA6ICd2aXNpYmlsaXR5X29mZid9fSA8L21hdC1pY29uPlxuXG4gICAgICAgICAgICAgICAgPHNwYW4gXG4gICAgICAgICAgICAgICAgICAgICpuZ0lmPVwidGhpcy5mb3JtRmllbGRJbnB1dFBhc3N3b3JkLmdldFRleHRQcmVmaXgoKVwiXG4gICAgICAgICAgICAgICAgICAgIG1hdFByZWZpeD57eyB0aGlzLmZvcm1GaWVsZElucHV0UGFzc3dvcmQuZ2V0VGV4dFByZWZpeCgpIH19Jm5ic3A7PC9zcGFuPlxuXG4gICAgICAgICAgICAgICAgPHNwYW4gXG4gICAgICAgICAgICAgICAgICAgICpuZ0lmPVwidGhpcy5mb3JtRmllbGRJbnB1dFBhc3N3b3JkLmdldFRleHRTdWZmaXgoKVwiXG4gICAgICAgICAgICAgICAgICAgIG1hdFN1ZmZpeD57eyB0aGlzLmZvcm1GaWVsZElucHV0UGFzc3dvcmQuZ2V0VGV4dFN1ZmZpeCgpIH19PC9zcGFuPlxuXG4gICAgICAgICAgICAgICAgPG1hdC1oaW50IFxuICAgICAgICAgICAgICAgICAgICAqbmdJZj1cInRoaXMuZm9ybUZpZWxkSW5wdXRQYXNzd29yZC5nZXRMZWZ0SGludExhYmVsKClcIlxuICAgICAgICAgICAgICAgICAgICBhbGlnbj1cInN0YXJ0XCI+e3sgdGhpcy5mb3JtRmllbGRJbnB1dFBhc3N3b3JkLmdldExlZnRIaW50TGFiZWwoKSB9fTwvbWF0LWhpbnQ+XG5cbiAgICAgICAgICAgICAgICA8bWF0LWhpbnRcbiAgICAgICAgICAgICAgICAgICAgKm5nSWY9XCJ0aGlzLmZvcm1GaWVsZElucHV0UGFzc3dvcmQuZ2V0UmlnaHRIaW50TGFiZWwoKVwiXG4gICAgICAgICAgICAgICAgICAgIGFsaWduPVwiZW5kXCI+e3sgdGhpcy5mb3JtRmllbGRJbnB1dFBhc3N3b3JkLmdldFJpZ2h0SGludExhYmVsKCkgfX08L21hdC1oaW50PlxuXG4gICAgICAgICAgICAgICAgPCEtLSBpZiBmaWVsZCBoYXMgU2hvd0hpbnRBYm91dE1pbk1heExlbmd0aCBzZXQgdG8gdHJ1ZSAtLT5cbiAgICAgICAgICAgICAgICA8bWF0LWhpbnRcbiAgICAgICAgICAgICAgICAgICAgKm5nSWY9XCJ0aGlzLmZvcm1GaWVsZElucHV0UGFzc3dvcmQuZ2V0U2hvd0hpbnRBYm91dE1pbk1heExlbmd0aCgpICYmIHRoaXMuZ2V0TWluTGVuZ3RoVmFsaWRhdG9yKCkgJiYgdGhpcy5nZXRNYXhMZW5ndGhWYWxpZGF0b3IoKVwiXG4gICAgICAgICAgICAgICAgICAgIGFsaWduPVwiZW5kXCI+e3sgdGhpcy5nZXRNaW5MZW5ndGhWYWxpZGF0b3IoKT8ubGVuZ3RoID8gdGhpcy5nZXRNaW5MZW5ndGhWYWxpZGF0b3IoKS5sZW5ndGggKyBcIiAvIFwiIDogJyd9fXt7IHRoaXMuZm9ybUZpZWxkSW5wdXRQYXNzd29yZC5nZXREZWZhdWx0VmFsdWUoKT8ubGVuZ3RoIH19e3sgdGhpcy5nZXRNYXhMZW5ndGhWYWxpZGF0b3IoKT8ubGVuZ3RoID8gXCIgLyBcIiArIHRoaXMuZ2V0TWF4TGVuZ3RoVmFsaWRhdG9yKCkubGVuZ3RoOiAnJ319PC9tYXQtaGludD5cblxuICAgICAgICAgICAgICAgIDxtYXQtaGludFxuICAgICAgICAgICAgICAgICAgICAqbmdJZj1cInRoaXMuZm9ybUZpZWxkSW5wdXRQYXNzd29yZC5nZXRTaG93SGludEFib3V0TWluTWF4TGVuZ3RoKCkgJiYgdGhpcy5nZXRNaW5MZW5ndGhWYWxpZGF0b3IoKSAmJiAhdGhpcy5nZXRNYXhMZW5ndGhWYWxpZGF0b3IoKVwiXG4gICAgICAgICAgICAgICAgICAgIGFsaWduPVwiZW5kXCI+e3sgdGhpcy5mb3JtRmllbGRJbnB1dFBhc3N3b3JkLmdldERlZmF1bHRWYWx1ZSgpPy5sZW5ndGggfX17eyB0aGlzLmdldE1pbkxlbmd0aFZhbGlkYXRvcigpPy5sZW5ndGggPyBcIiAvIFwiICsgdGhpcy5nZXRNaW5MZW5ndGhWYWxpZGF0b3IoKS5sZW5ndGggOiAnJ319PC9tYXQtaGludD5cblxuICAgICAgICAgICAgICAgIDxtYXQtaGludFxuICAgICAgICAgICAgICAgICAgICAqbmdJZj1cInRoaXMuZm9ybUZpZWxkSW5wdXRQYXNzd29yZC5nZXRTaG93SGludEFib3V0TWluTWF4TGVuZ3RoKCkgJiYgIXRoaXMuZ2V0TWluTGVuZ3RoVmFsaWRhdG9yKCkgJiYgdGhpcy5nZXRNYXhMZW5ndGhWYWxpZGF0b3IoKVwiXG4gICAgICAgICAgICAgICAgICAgIGFsaWduPVwiZW5kXCI+e3sgdGhpcy5mb3JtRmllbGRJbnB1dFBhc3N3b3JkLmdldERlZmF1bHRWYWx1ZSgpPy5sZW5ndGggfX17eyB0aGlzLmdldE1heExlbmd0aFZhbGlkYXRvcigpPy5sZW5ndGggPyBcIiAvIFwiICsgdGhpcy5nZXRNYXhMZW5ndGhWYWxpZGF0b3IoKS5sZW5ndGg6ICcnfX08L21hdC1oaW50PlxuXG4gICAgICAgIDwvbWF0LWZvcm0tZmllbGQ+XG5cbjwvc2VjdGlvbj4iXX0=