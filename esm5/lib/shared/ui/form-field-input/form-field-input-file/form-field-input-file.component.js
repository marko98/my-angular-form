import { Component, Input, ViewChild, forwardRef, } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import * as i0 from "@angular/core";
import * as i1 from "../../../service/device.service";
import * as i2 from "../../../service/ui.service";
import * as i3 from "@angular/common";
import * as i4 from "@angular/flex-layout/flex";
import * as i5 from "../../../directive/drag-drop.directive";
import * as i6 from "@angular/material/button";
import * as i7 from "@angular/material/list";
var _c0 = ["appDragDrop"];
var _c1 = ["file"];
function FormFieldInputFileComponent_section_0_mat_selection_list_4_mat_list_option_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-list-option", 12);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var item_r9 = ctx.$implicit;
    i0.ɵɵproperty("value", item_r9);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", item_r9.name, " ");
} }
function FormFieldInputFileComponent_section_0_mat_selection_list_4_button_3_Template(rf, ctx) { if (rf & 1) {
    var _r11 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 13);
    i0.ɵɵlistener("click", function FormFieldInputFileComponent_section_0_mat_selection_list_4_button_3_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r11); i0.ɵɵnextContext(); var _r6 = i0.ɵɵreference(1); var ctx_r10 = i0.ɵɵnextContext(2); return ctx_r10.onRemoveFiles(_r6.selectedOptions.selected); });
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r8 = i0.ɵɵnextContext(3);
    i0.ɵɵproperty("color", ctx_r8.formFieldInputFile.getButtonDeleteColor());
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r8.formFieldInputFile.getButtonDeleteText(), " ");
} }
function FormFieldInputFileComponent_section_0_mat_selection_list_4_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-selection-list", 8, 9);
    i0.ɵɵtemplate(2, FormFieldInputFileComponent_section_0_mat_selection_list_4_mat_list_option_2_Template, 2, 2, "mat-list-option", 10);
    i0.ɵɵtemplate(3, FormFieldInputFileComponent_section_0_mat_selection_list_4_button_3_Template, 2, 2, "button", 11);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var _r6 = i0.ɵɵreference(1);
    var ctx_r4 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngForOf", ctx_r4.formFieldInputFile.getDefaultValue());
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", _r6.selectedOptions.selected.length > 0);
} }
function FormFieldInputFileComponent_section_0_Template(rf, ctx) { if (rf & 1) {
    var _r13 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "section", 2, 3);
    i0.ɵɵlistener("onFileDropped", function FormFieldInputFileComponent_section_0_Template_section_onFileDropped_0_listener($event) { i0.ɵɵrestoreView(_r13); var ctx_r12 = i0.ɵɵnextContext(); return !ctx_r12.formFieldInputFile.getDisabled() && ctx_r12.onFileDropped($event); });
    i0.ɵɵelementStart(2, "h3");
    i0.ɵɵtext(3);
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(4, FormFieldInputFileComponent_section_0_mat_selection_list_4_Template, 4, 2, "mat-selection-list", 4);
    i0.ɵɵelementStart(5, "input", 5, 6);
    i0.ɵɵlistener("change", function FormFieldInputFileComponent_section_0_Template_input_change_5_listener() { i0.ɵɵrestoreView(_r13); var ctx_r14 = i0.ɵɵnextContext(); return ctx_r14.onFilesAdded(); });
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(7, "button", 7);
    i0.ɵɵlistener("click", function FormFieldInputFileComponent_section_0_Template_button_click_7_listener() { i0.ɵɵrestoreView(_r13); var ctx_r15 = i0.ɵɵnextContext(); return ctx_r15.addFiles(); });
    i0.ɵɵtext(8);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r0 = i0.ɵɵnextContext();
    var tmp_1_0 = null;
    var currVal_1 = ((tmp_1_0 = ctx_r0.formFieldInputFile.getDefaultValue()) == null ? null : tmp_1_0.length) > 0;
    i0.ɵɵadvance(3);
    i0.ɵɵtextInterpolate(ctx_r0.formFieldInputFile.getLabelName());
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", currVal_1);
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("color", ctx_r0.formFieldInputFile.getButtonAddColor())("disabled", ctx_r0.formFieldInputFile.getDisabled());
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r0.formFieldInputFile.getButtonAddText(), " ");
} }
function FormFieldInputFileComponent_section_1_mat_selection_list_3_mat_list_option_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-list-option", 12);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var item_r21 = ctx.$implicit;
    i0.ɵɵproperty("value", item_r21);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", item_r21.name, " ");
} }
function FormFieldInputFileComponent_section_1_mat_selection_list_3_button_3_Template(rf, ctx) { if (rf & 1) {
    var _r23 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 13);
    i0.ɵɵlistener("click", function FormFieldInputFileComponent_section_1_mat_selection_list_3_button_3_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r23); i0.ɵɵnextContext(); var _r18 = i0.ɵɵreference(1); var ctx_r22 = i0.ɵɵnextContext(2); return ctx_r22.onRemoveFiles(_r18.selectedOptions.selected); });
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r20 = i0.ɵɵnextContext(3);
    i0.ɵɵproperty("color", ctx_r20.formFieldInputFile.getButtonDeleteColor());
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r20.formFieldInputFile.getButtonDeleteText(), " ");
} }
function FormFieldInputFileComponent_section_1_mat_selection_list_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-selection-list", 8, 9);
    i0.ɵɵtemplate(2, FormFieldInputFileComponent_section_1_mat_selection_list_3_mat_list_option_2_Template, 2, 2, "mat-list-option", 10);
    i0.ɵɵtemplate(3, FormFieldInputFileComponent_section_1_mat_selection_list_3_button_3_Template, 2, 2, "button", 11);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var _r18 = i0.ɵɵreference(1);
    var ctx_r16 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngForOf", ctx_r16.formFieldInputFile.getDefaultValue());
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", _r18.selectedOptions.selected.length > 0);
} }
function FormFieldInputFileComponent_section_1_Template(rf, ctx) { if (rf & 1) {
    var _r25 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "section", 14);
    i0.ɵɵelementStart(1, "h3");
    i0.ɵɵtext(2);
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(3, FormFieldInputFileComponent_section_1_mat_selection_list_3_Template, 4, 2, "mat-selection-list", 4);
    i0.ɵɵelementStart(4, "input", 5, 6);
    i0.ɵɵlistener("change", function FormFieldInputFileComponent_section_1_Template_input_change_4_listener() { i0.ɵɵrestoreView(_r25); var ctx_r24 = i0.ɵɵnextContext(); return ctx_r24.onFilesAdded(); });
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(6, "button", 7);
    i0.ɵɵlistener("click", function FormFieldInputFileComponent_section_1_Template_button_click_6_listener() { i0.ɵɵrestoreView(_r25); var ctx_r26 = i0.ɵɵnextContext(); return ctx_r26.addFiles(); });
    i0.ɵɵtext(7);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r1 = i0.ɵɵnextContext();
    var tmp_1_0 = null;
    var currVal_1 = ((tmp_1_0 = ctx_r1.formFieldInputFile.getDefaultValue()) == null ? null : tmp_1_0.length) > 0;
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(ctx_r1.formFieldInputFile.getLabelName());
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", currVal_1);
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("color", ctx_r1.formFieldInputFile.getButtonAddColor())("disabled", ctx_r1.formFieldInputFile.getDisabled());
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r1.formFieldInputFile.getButtonAddText(), " ");
} }
function FormFieldInputFileComponent_section_2_mat_selection_list_3_mat_list_option_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-list-option", 12);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var item_r32 = ctx.$implicit;
    i0.ɵɵproperty("value", item_r32);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", item_r32.name, " ");
} }
function FormFieldInputFileComponent_section_2_mat_selection_list_3_button_3_Template(rf, ctx) { if (rf & 1) {
    var _r34 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 13);
    i0.ɵɵlistener("click", function FormFieldInputFileComponent_section_2_mat_selection_list_3_button_3_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r34); i0.ɵɵnextContext(); var _r29 = i0.ɵɵreference(1); var ctx_r33 = i0.ɵɵnextContext(2); return ctx_r33.onRemoveFiles(_r29.selectedOptions.selected); });
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r31 = i0.ɵɵnextContext(3);
    i0.ɵɵproperty("color", ctx_r31.formFieldInputFile.getButtonDeleteColor());
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r31.formFieldInputFile.getButtonDeleteText(), " ");
} }
function FormFieldInputFileComponent_section_2_mat_selection_list_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-selection-list", 8, 9);
    i0.ɵɵtemplate(2, FormFieldInputFileComponent_section_2_mat_selection_list_3_mat_list_option_2_Template, 2, 2, "mat-list-option", 10);
    i0.ɵɵtemplate(3, FormFieldInputFileComponent_section_2_mat_selection_list_3_button_3_Template, 2, 2, "button", 11);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var _r29 = i0.ɵɵreference(1);
    var ctx_r27 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngForOf", ctx_r27.formFieldInputFile.getDefaultValue());
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", _r29.selectedOptions.selected.length > 0);
} }
function FormFieldInputFileComponent_section_2_Template(rf, ctx) { if (rf & 1) {
    var _r36 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "section", 14);
    i0.ɵɵelementStart(1, "h3");
    i0.ɵɵtext(2);
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(3, FormFieldInputFileComponent_section_2_mat_selection_list_3_Template, 4, 2, "mat-selection-list", 4);
    i0.ɵɵelementStart(4, "input", 5, 6);
    i0.ɵɵlistener("change", function FormFieldInputFileComponent_section_2_Template_input_change_4_listener() { i0.ɵɵrestoreView(_r36); var ctx_r35 = i0.ɵɵnextContext(); return ctx_r35.onFilesAdded(); });
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(6, "button", 7);
    i0.ɵɵlistener("click", function FormFieldInputFileComponent_section_2_Template_button_click_6_listener() { i0.ɵɵrestoreView(_r36); var ctx_r37 = i0.ɵɵnextContext(); return ctx_r37.addFiles(); });
    i0.ɵɵtext(7);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r2 = i0.ɵɵnextContext();
    var tmp_1_0 = null;
    var currVal_1 = ((tmp_1_0 = ctx_r2.formFieldInputFile.getDefaultValue()) == null ? null : tmp_1_0.length) > 0;
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(ctx_r2.formFieldInputFile.getLabelName());
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", currVal_1);
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("color", ctx_r2.formFieldInputFile.getButtonAddColor())("disabled", ctx_r2.formFieldInputFile.getDisabled());
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r2.formFieldInputFile.getButtonAddText(), " ");
} }
var FormFieldInputFileComponent = /** @class */ (function () {
    function FormFieldInputFileComponent(renderer, deviceService, uiService) {
        var _this = this;
        this.renderer = renderer;
        this.deviceService = deviceService;
        this.uiService = uiService;
        this.onChange = function () { };
        this.onTouched = function () { };
        this.onFilesAdded = function () {
            var files = _this.file.nativeElement.files;
            for (var key in _this.file.nativeElement.files) {
                if (!isNaN(parseInt(key))) {
                    // console.log(files[key]);
                    if (_this.formFieldInputFile.isFileTypeAllowed(files[key].type))
                        _this.formFieldInputFile.addFile(files[key]);
                    else
                        _this.uiService.onShowSnackBar(files[key].type + ' is not allowed', null, 1500);
                }
            }
            if (_this.formFieldInputFile.getDefaultValue().length > 0) {
                _this.onChange(_this.formFieldInputFile.getDefaultValue());
                _this.onTouched();
            }
        };
        this.onFileDropped = function (fileList) {
            Array.from(fileList).forEach(function (file) {
                if (_this.formFieldInputFile.isFileTypeAllowed(file.type))
                    _this.formFieldInputFile.addFile(file);
                else
                    _this.uiService.onShowSnackBar(file.type + ' is not allowed', null, 1500);
            });
        };
        this.addFiles = function () {
            _this.file.nativeElement.click();
        };
        this.onRemoveFiles = function (matListOptions) {
            var _loop_1 = function (mLOindex) {
                var matListOption = matListOptions[mLOindex];
                _this.formFieldInputFile.setDefaultValue(_this.formFieldInputFile
                    .getDefaultValue()
                    .filter(function (f) { return f !== matListOption.value; }));
            };
            for (var mLOindex in matListOptions) {
                _loop_1(mLOindex);
            }
        };
    }
    FormFieldInputFileComponent.prototype.registerOnChange = function (fn) {
        this.onChange = fn;
    };
    FormFieldInputFileComponent.prototype.registerOnTouched = function (fn) {
        this.onTouched = fn;
    };
    FormFieldInputFileComponent.prototype.writeValue = function (value) {
        if (value) {
            this.formFieldInputFile.setDefaultValue(value);
        }
    };
    FormFieldInputFileComponent.prototype.ngOnInit = function () {
        // console.log('FormFieldInputFileComponent init');
    };
    FormFieldInputFileComponent.prototype.ngOnDestroy = function () {
        // console.log('FormFieldInputFileComponent destroyed');
    };
    FormFieldInputFileComponent.prototype.ngAfterViewInit = function () {
        if (this.appDragDrop)
            this.renderer.setStyle(this.appDragDrop.nativeElement, 'border', '2px dashed ' + this.formFieldInputFile.getBorderColor());
    };
    FormFieldInputFileComponent.ɵfac = function FormFieldInputFileComponent_Factory(t) { return new (t || FormFieldInputFileComponent)(i0.ɵɵdirectiveInject(i0.Renderer2), i0.ɵɵdirectiveInject(i1.DeviceService), i0.ɵɵdirectiveInject(i2.UiService)); };
    FormFieldInputFileComponent.ɵcmp = i0.ɵɵdefineComponent({ type: FormFieldInputFileComponent, selectors: [["app-form-field-input-file"]], viewQuery: function FormFieldInputFileComponent_Query(rf, ctx) { if (rf & 1) {
            i0.ɵɵviewQuery(_c0, true);
            i0.ɵɵviewQuery(_c1, true);
        } if (rf & 2) {
            var _t;
            i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.appDragDrop = _t.first);
            i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.file = _t.first);
        } }, inputs: { formFieldInputFile: "formFieldInputFile" }, features: [i0.ɵɵProvidersFeature([
                {
                    provide: NG_VALUE_ACCESSOR,
                    multi: true,
                    useExisting: forwardRef(function () { return FormFieldInputFileComponent; }),
                },
            ])], decls: 3, vars: 3, consts: [["fxLayout", "column", "fxLayoutAlign", "center center", "fxLayoutGap", "10px", "class", "upload-file-container add-padding", "appDragDrop", "", 3, "onFileDropped", 4, "ngIf"], ["fxLayout", "column", "fxLayoutAlign", "center center", "fxLayoutGap", "10px", "class", "add-padding", 4, "ngIf"], ["fxLayout", "column", "fxLayoutAlign", "center center", "fxLayoutGap", "10px", "appDragDrop", "", 1, "upload-file-container", "add-padding", 3, "onFileDropped"], ["appDragDrop", ""], ["fxLayout", "column", "fxLayoutAlign", "center center", "fxLayoutGap", "8px", 4, "ngIf"], ["type", "file", "multiple", "", 3, "change"], ["file", ""], ["type", "button", "mat-raised-button", "", 3, "color", "disabled", "click"], ["fxLayout", "column", "fxLayoutAlign", "center center", "fxLayoutGap", "8px"], ["files", ""], [3, "value", 4, "ngFor", "ngForOf"], ["type", "button", "mat-button", "", 3, "color", "click", 4, "ngIf"], [3, "value"], ["type", "button", "mat-button", "", 3, "color", "click"], ["fxLayout", "column", "fxLayoutAlign", "center center", "fxLayoutGap", "10px", 1, "add-padding"]], template: function FormFieldInputFileComponent_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵtemplate(0, FormFieldInputFileComponent_section_0_Template, 9, 5, "section", 0);
            i0.ɵɵtemplate(1, FormFieldInputFileComponent_section_1_Template, 8, 5, "section", 1);
            i0.ɵɵtemplate(2, FormFieldInputFileComponent_section_2_Template, 8, 5, "section", 1);
        } if (rf & 2) {
            i0.ɵɵproperty("ngIf", ctx.formFieldInputFile.getDragAndDrop() && ctx.deviceService.isDeviceDesktop());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.formFieldInputFile.getDragAndDrop() && !ctx.deviceService.isDeviceDesktop());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", !ctx.formFieldInputFile.getDragAndDrop());
        } }, directives: [i3.NgIf, i4.DefaultLayoutDirective, i4.DefaultLayoutAlignDirective, i4.DefaultLayoutGapDirective, i5.DragDropDirective, i6.MatButton, i7.MatSelectionList, i3.NgForOf, i7.MatListOption], styles: ["", ".add-padding[_ngcontent-%COMP%] {\n        padding: 20px;\n    }\n\n    .cursor[_ngcontent-%COMP%] {\n        cursor: pointer;\n    }\n\n    input[type=\"file\"][_ngcontent-%COMP%] {\n        display: none;\n    }\n\n    h3[_ngcontent-%COMP%] {\n        font-size: inherit;\n        margin: 0;\n        font-weight: inherit;\n    }\n\n    .upload-file-container[_ngcontent-%COMP%] {\n        min-height: 200px;\n        \n        \n        margin: 5px auto;        \n        border-radius: 10px;\n    }\n\n    mat-form-field[_ngcontent-%COMP%] {\n        min-width: 250px;\n    }"] });
    return FormFieldInputFileComponent;
}());
export { FormFieldInputFileComponent };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FormFieldInputFileComponent, [{
        type: Component,
        args: [{
                selector: 'app-form-field-input-file',
                templateUrl: './form-field-input-file.component.html',
                styleUrls: ['./form-field-input-file.component.css'],
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        multi: true,
                        useExisting: forwardRef(function () { return FormFieldInputFileComponent; }),
                    },
                ],
            }]
    }], function () { return [{ type: i0.Renderer2 }, { type: i1.DeviceService }, { type: i2.UiService }]; }, { appDragDrop: [{
            type: ViewChild,
            args: ['appDragDrop', { static: false }]
        }], file: [{
            type: ViewChild,
            args: ['file', { static: false }]
        }], formFieldInputFile: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1maWVsZC1pbnB1dC1maWxlLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215LWFuZ3VsYXItZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvdWkvZm9ybS1maWVsZC1pbnB1dC9mb3JtLWZpZWxkLWlucHV0LWZpbGUvZm9ybS1maWVsZC1pbnB1dC1maWxlLmNvbXBvbmVudC50cyIsImxpYi9zaGFyZWQvdWkvZm9ybS1maWVsZC1pbnB1dC9mb3JtLWZpZWxkLWlucHV0LWZpbGUvZm9ybS1maWVsZC1pbnB1dC1maWxlLmNvbXBvbmVudC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFDTCxTQUFTLEVBSVQsS0FBSyxFQUNMLFNBQVMsRUFHVCxVQUFVLEdBQ1gsTUFBTSxlQUFlLENBQUM7QUFFdkIsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sZ0JBQWdCLENBQUM7Ozs7Ozs7Ozs7OztJQzJDbkMsMkNBR1E7SUFBQSxZQUNSO0lBQUEsaUJBQWtCOzs7SUFGZCwrQkFBYztJQUNWLGVBQ1I7SUFEUSw2Q0FDUjs7OztJQUVBLGtDQU1RO0lBRkosbVRBQTREO0lBRXhELFlBQ1I7SUFBQSxpQkFBUzs7O0lBSkwsd0VBQXdEO0lBR3BELGVBQ1I7SUFEUSxnRkFDUjs7O0lBcEJSLGdEQU9RO0lBQUEsb0lBR1E7SUFHUixrSEFNUTtJQUVoQixpQkFBcUI7Ozs7SUFiVCxlQUE4RDtJQUE5RCxxRUFBOEQ7SUFNOUQsZUFBaUQ7SUFBakQsOERBQWlEOzs7O0lBM0JyRSxxQ0FXUTtJQUZKLG9NQUFrQix3Q0FBcUMscUNBQStCO0lBRWxGLDBCQUFJO0lBQUEsWUFBNEM7SUFBQSxpQkFBSztJQUVyRCxvSEFPUTtJQWdCUixtQ0FNQTtJQUhJLHVNQUF5QjtJQUg3QixpQkFNQTtJQUFBLGlDQU1RO0lBSkosa01BQW9CO0lBSWhCLFlBQ1I7SUFBQSxpQkFBUztJQUVqQixpQkFBVTs7Ozs7SUF4Q0UsZUFBNEM7SUFBNUMsOERBQTRDO0lBSTVDLGVBQTZEO0lBQTdELGdDQUE2RDtJQThCN0QsZUFBcUQ7SUFBckQscUVBQXFELHFEQUFBO0lBR2pELGVBQ1I7SUFEUSw2RUFDUjs7O0lBcUJRLDJDQUdRO0lBQUEsWUFDUjtJQUFBLGlCQUFrQjs7O0lBRmQsZ0NBQWM7SUFDVixlQUNSO0lBRFEsOENBQ1I7Ozs7SUFFQSxrQ0FNUTtJQUZKLHFUQUE0RDtJQUV4RCxZQUNSO0lBQUEsaUJBQVM7OztJQUpMLHlFQUF3RDtJQUdwRCxlQUNSO0lBRFEsaUZBQ1I7OztJQXBCUixnREFPUTtJQUFBLG9JQUdRO0lBR1Isa0hBTVE7SUFFaEIsaUJBQXFCOzs7O0lBYlQsZUFBOEQ7SUFBOUQsc0VBQThEO0lBTTlELGVBQWlEO0lBQWpELCtEQUFpRDs7OztJQXZCckUsbUNBT1E7SUFBQSwwQkFBSTtJQUFBLFlBQTRDO0lBQUEsaUJBQUs7SUFFckQsb0hBT1E7SUFnQlIsbUNBTUE7SUFISSx1TUFBeUI7SUFIN0IsaUJBTUE7SUFBQSxpQ0FNUTtJQUpKLGtNQUFvQjtJQUloQixZQUNSO0lBQUEsaUJBQVM7SUFFakIsaUJBQVU7Ozs7O0lBeENFLGVBQTRDO0lBQTVDLDhEQUE0QztJQUk1QyxlQUE2RDtJQUE3RCxnQ0FBNkQ7SUE4QjdELGVBQXFEO0lBQXJELHFFQUFxRCxxREFBQTtJQUdqRCxlQUNSO0lBRFEsNkVBQ1I7OztJQXFCUSwyQ0FHUTtJQUFBLFlBQ1I7SUFBQSxpQkFBa0I7OztJQUZkLGdDQUFjO0lBQ1YsZUFDUjtJQURRLDhDQUNSOzs7O0lBRUEsa0NBTVE7SUFGSixxVEFBNEQ7SUFFeEQsWUFDUjtJQUFBLGlCQUFTOzs7SUFKTCx5RUFBd0Q7SUFHcEQsZUFDUjtJQURRLGlGQUNSOzs7SUFwQlIsZ0RBT1E7SUFBQSxvSUFHUTtJQUdSLGtIQU1RO0lBRWhCLGlCQUFxQjs7OztJQWJULGVBQThEO0lBQTlELHNFQUE4RDtJQU05RCxlQUFpRDtJQUFqRCwrREFBaUQ7Ozs7SUF2QnJFLG1DQU9RO0lBQUEsMEJBQUk7SUFBQSxZQUE0QztJQUFBLGlCQUFLO0lBRXJELG9IQU9RO0lBZ0JSLG1DQU1BO0lBSEksdU1BQXlCO0lBSDdCLGlCQU1BO0lBQUEsaUNBTVE7SUFKSixrTUFBb0I7SUFJaEIsWUFDUjtJQUFBLGlCQUFTO0lBRWpCLGlCQUFVOzs7OztJQXhDRSxlQUE0QztJQUE1Qyw4REFBNEM7SUFJNUMsZUFBNkQ7SUFBN0QsZ0NBQTZEO0lBOEI3RCxlQUFxRDtJQUFyRCxxRUFBcUQscURBQUE7SUFHakQsZUFDUjtJQURRLDZFQUNSOztBRG5LUjtJQXFCRSxxQ0FDVSxRQUFtQixFQUNwQixhQUE0QixFQUMzQixTQUFvQjtRQUg5QixpQkFJSTtRQUhNLGFBQVEsR0FBUixRQUFRLENBQVc7UUFDcEIsa0JBQWEsR0FBYixhQUFhLENBQWU7UUFDM0IsY0FBUyxHQUFULFNBQVMsQ0FBVztRQU52QixhQUFRLEdBQVEsY0FBTyxDQUFDLENBQUM7UUFDekIsY0FBUyxHQUFRLGNBQU8sQ0FBQyxDQUFDO1FBc0JqQyxpQkFBWSxHQUFHO1lBQ2IsSUFBSSxLQUFLLEdBQTRCLEtBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQztZQUVuRSxLQUFLLElBQUksR0FBRyxJQUFJLEtBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRTtnQkFDN0MsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRTtvQkFDekIsMkJBQTJCO29CQUMzQixJQUFJLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDO3dCQUM1RCxLQUFJLENBQUMsa0JBQWtCLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDOzt3QkFFNUMsS0FBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQzNCLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLEdBQUcsaUJBQWlCLEVBQ25DLElBQUksRUFDSixJQUFJLENBQ0wsQ0FBQztpQkFDTDthQUNGO1lBRUQsSUFBSSxLQUFJLENBQUMsa0JBQWtCLENBQUMsZUFBZSxFQUFFLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDeEQsS0FBSSxDQUFDLFFBQVEsQ0FBQyxLQUFJLENBQUMsa0JBQWtCLENBQUMsZUFBZSxFQUFFLENBQUMsQ0FBQztnQkFDekQsS0FBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO2FBQ2xCO1FBQ0gsQ0FBQyxDQUFDO1FBRUYsa0JBQWEsR0FBRyxVQUFDLFFBQWtCO1lBQ2pDLEtBQUssQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQUMsSUFBSTtnQkFDaEMsSUFBSSxLQUFJLENBQUMsa0JBQWtCLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztvQkFDdEQsS0FBSSxDQUFDLGtCQUFrQixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQzs7b0JBRXRDLEtBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUMzQixJQUFJLENBQUMsSUFBSSxHQUFHLGlCQUFpQixFQUM3QixJQUFJLEVBQ0osSUFBSSxDQUNMLENBQUM7WUFDTixDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQztRQUVGLGFBQVEsR0FBRztZQUNULEtBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ2xDLENBQUMsQ0FBQztRQUVGLGtCQUFhLEdBQUcsVUFBQyxjQUErQjtvQ0FDckMsUUFBUTtnQkFDZixJQUFJLGFBQWEsR0FBa0IsY0FBYyxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUM1RCxLQUFJLENBQUMsa0JBQWtCLENBQUMsZUFBZSxDQUNyQyxLQUFJLENBQUMsa0JBQWtCO3FCQUNwQixlQUFlLEVBQUU7cUJBQ2pCLE1BQU0sQ0FBQyxVQUFDLENBQUMsSUFBSyxPQUFBLENBQUMsS0FBSyxhQUFhLENBQUMsS0FBSyxFQUF6QixDQUF5QixDQUFDLENBQzVDLENBQUM7O1lBTkosS0FBSyxJQUFJLFFBQVEsSUFBSSxjQUFjO3dCQUExQixRQUFRO2FBT2hCO1FBQ0gsQ0FBQyxDQUFDO0lBakVDLENBQUM7SUFFSixzREFBZ0IsR0FBaEIsVUFBaUIsRUFBRTtRQUNqQixJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztJQUNyQixDQUFDO0lBRUQsdURBQWlCLEdBQWpCLFVBQWtCLEVBQUU7UUFDbEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7SUFDdEIsQ0FBQztJQUVELGdEQUFVLEdBQVYsVUFBVyxLQUFLO1FBQ2QsSUFBSSxLQUFLLEVBQUU7WUFDVCxJQUFJLENBQUMsa0JBQWtCLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ2hEO0lBQ0gsQ0FBQztJQXFERCw4Q0FBUSxHQUFSO1FBQ0UsbURBQW1EO0lBQ3JELENBQUM7SUFFRCxpREFBVyxHQUFYO1FBQ0Usd0RBQXdEO0lBQzFELENBQUM7SUFFRCxxREFBZSxHQUFmO1FBQ0UsSUFBSSxJQUFJLENBQUMsV0FBVztZQUNsQixJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FDcEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQzlCLFFBQVEsRUFDUixhQUFhLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLGNBQWMsRUFBRSxDQUN6RCxDQUFDO0lBQ04sQ0FBQzswR0EvRlUsMkJBQTJCO29FQUEzQiwyQkFBMkI7Ozs7Ozs7b0dBUjNCO2dCQUNUO29CQUNFLE9BQU8sRUFBRSxpQkFBaUI7b0JBQzFCLEtBQUssRUFBRSxJQUFJO29CQUNYLFdBQVcsRUFBRSxVQUFVLENBQUMsY0FBTSxPQUFBLDJCQUEyQixFQUEzQixDQUEyQixDQUFDO2lCQUMzRDthQUNGO1lDSUgsb0ZBV1E7WUEyQ1Isb0ZBT1E7WUEyQ1Isb0ZBT1E7O1lBOUdKLHFHQUF3RjtZQXNEeEYsZUFBeUY7WUFBekYsc0dBQXlGO1lBa0R6RixlQUFpRDtZQUFqRCwrREFBaUQ7O3NDRDVJckQ7Q0FpSUMsQUE1R0QsSUE0R0M7U0FoR1ksMkJBQTJCO2tEQUEzQiwyQkFBMkI7Y0FadkMsU0FBUztlQUFDO2dCQUNULFFBQVEsRUFBRSwyQkFBMkI7Z0JBQ3JDLFdBQVcsRUFBRSx3Q0FBd0M7Z0JBQ3JELFNBQVMsRUFBRSxDQUFDLHVDQUF1QyxDQUFDO2dCQUNwRCxTQUFTLEVBQUU7b0JBQ1Q7d0JBQ0UsT0FBTyxFQUFFLGlCQUFpQjt3QkFDMUIsS0FBSyxFQUFFLElBQUk7d0JBQ1gsV0FBVyxFQUFFLFVBQVUsQ0FBQyxjQUFNLE9BQUEsMkJBQTJCLEVBQTNCLENBQTJCLENBQUM7cUJBQzNEO2lCQUNGO2FBQ0Y7O2tCQUdFLFNBQVM7bUJBQUMsYUFBYSxFQUFFLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRTs7a0JBQzFDLFNBQVM7bUJBQUMsTUFBTSxFQUFFLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRTs7a0JBQ25DLEtBQUsiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xuICBDb21wb25lbnQsXG4gIE9uSW5pdCxcbiAgT25EZXN0cm95LFxuICBBZnRlclZpZXdJbml0LFxuICBJbnB1dCxcbiAgVmlld0NoaWxkLFxuICBFbGVtZW50UmVmLFxuICBSZW5kZXJlcjIsXG4gIGZvcndhcmRSZWYsXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgTWF0TGlzdE9wdGlvbiB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2xpc3QnO1xuaW1wb3J0IHsgTkdfVkFMVUVfQUNDRVNTT1IgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5cbi8vIG1vZGVsXG5pbXBvcnQgeyBGb3JtRmllbGRJbnB1dEZpbGUgfSBmcm9tICcuLi8uLi8uLi9tb2RlbC9zdHJ1Y3R1cmFsL2NvbXBvc2l0ZS9mb3JtL2Zvcm0tZmllbGQtaW5wdXQvZm9ybS1maWVsZC1pbnB1dC1maWxlLm1vZGVsJztcblxuLy8gc2VydmljZVxuaW1wb3J0IHsgRGV2aWNlU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL3NlcnZpY2UvZGV2aWNlLnNlcnZpY2UnO1xuaW1wb3J0IHsgVWlTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vc2VydmljZS91aS5zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnYXBwLWZvcm0tZmllbGQtaW5wdXQtZmlsZScsXG4gIHRlbXBsYXRlVXJsOiAnLi9mb3JtLWZpZWxkLWlucHV0LWZpbGUuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9mb3JtLWZpZWxkLWlucHV0LWZpbGUuY29tcG9uZW50LmNzcyddLFxuICBwcm92aWRlcnM6IFtcbiAgICB7XG4gICAgICBwcm92aWRlOiBOR19WQUxVRV9BQ0NFU1NPUixcbiAgICAgIG11bHRpOiB0cnVlLFxuICAgICAgdXNlRXhpc3Rpbmc6IGZvcndhcmRSZWYoKCkgPT4gRm9ybUZpZWxkSW5wdXRGaWxlQ29tcG9uZW50KSxcbiAgICB9LFxuICBdLFxufSlcbmV4cG9ydCBjbGFzcyBGb3JtRmllbGRJbnB1dEZpbGVDb21wb25lbnRcbiAgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSwgQWZ0ZXJWaWV3SW5pdCB7XG4gIEBWaWV3Q2hpbGQoJ2FwcERyYWdEcm9wJywgeyBzdGF0aWM6IGZhbHNlIH0pIGFwcERyYWdEcm9wOiBFbGVtZW50UmVmO1xuICBAVmlld0NoaWxkKCdmaWxlJywgeyBzdGF0aWM6IGZhbHNlIH0pIGZpbGU6IEVsZW1lbnRSZWY7XG4gIEBJbnB1dCgpIGZvcm1GaWVsZElucHV0RmlsZTogRm9ybUZpZWxkSW5wdXRGaWxlO1xuXG4gIHB1YmxpYyBvbkNoYW5nZTogYW55ID0gKCkgPT4ge307XG4gIHB1YmxpYyBvblRvdWNoZWQ6IGFueSA9ICgpID0+IHt9O1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHByaXZhdGUgcmVuZGVyZXI6IFJlbmRlcmVyMixcbiAgICBwdWJsaWMgZGV2aWNlU2VydmljZTogRGV2aWNlU2VydmljZSxcbiAgICBwcml2YXRlIHVpU2VydmljZTogVWlTZXJ2aWNlXG4gICkge31cblxuICByZWdpc3Rlck9uQ2hhbmdlKGZuKSB7XG4gICAgdGhpcy5vbkNoYW5nZSA9IGZuO1xuICB9XG5cbiAgcmVnaXN0ZXJPblRvdWNoZWQoZm4pIHtcbiAgICB0aGlzLm9uVG91Y2hlZCA9IGZuO1xuICB9XG5cbiAgd3JpdGVWYWx1ZSh2YWx1ZSkge1xuICAgIGlmICh2YWx1ZSkge1xuICAgICAgdGhpcy5mb3JtRmllbGRJbnB1dEZpbGUuc2V0RGVmYXVsdFZhbHVlKHZhbHVlKTtcbiAgICB9XG4gIH1cblxuICBvbkZpbGVzQWRkZWQgPSAoKTogdm9pZCA9PiB7XG4gICAgbGV0IGZpbGVzOiB7IFtrZXk6IHN0cmluZ106IEZpbGUgfSA9IHRoaXMuZmlsZS5uYXRpdmVFbGVtZW50LmZpbGVzO1xuXG4gICAgZm9yIChsZXQga2V5IGluIHRoaXMuZmlsZS5uYXRpdmVFbGVtZW50LmZpbGVzKSB7XG4gICAgICBpZiAoIWlzTmFOKHBhcnNlSW50KGtleSkpKSB7XG4gICAgICAgIC8vIGNvbnNvbGUubG9nKGZpbGVzW2tleV0pO1xuICAgICAgICBpZiAodGhpcy5mb3JtRmllbGRJbnB1dEZpbGUuaXNGaWxlVHlwZUFsbG93ZWQoZmlsZXNba2V5XS50eXBlKSlcbiAgICAgICAgICB0aGlzLmZvcm1GaWVsZElucHV0RmlsZS5hZGRGaWxlKGZpbGVzW2tleV0pO1xuICAgICAgICBlbHNlXG4gICAgICAgICAgdGhpcy51aVNlcnZpY2Uub25TaG93U25hY2tCYXIoXG4gICAgICAgICAgICBmaWxlc1trZXldLnR5cGUgKyAnIGlzIG5vdCBhbGxvd2VkJyxcbiAgICAgICAgICAgIG51bGwsXG4gICAgICAgICAgICAxNTAwXG4gICAgICAgICAgKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBpZiAodGhpcy5mb3JtRmllbGRJbnB1dEZpbGUuZ2V0RGVmYXVsdFZhbHVlKCkubGVuZ3RoID4gMCkge1xuICAgICAgdGhpcy5vbkNoYW5nZSh0aGlzLmZvcm1GaWVsZElucHV0RmlsZS5nZXREZWZhdWx0VmFsdWUoKSk7XG4gICAgICB0aGlzLm9uVG91Y2hlZCgpO1xuICAgIH1cbiAgfTtcblxuICBvbkZpbGVEcm9wcGVkID0gKGZpbGVMaXN0OiBGaWxlTGlzdCk6IHZvaWQgPT4ge1xuICAgIEFycmF5LmZyb20oZmlsZUxpc3QpLmZvckVhY2goKGZpbGUpID0+IHtcbiAgICAgIGlmICh0aGlzLmZvcm1GaWVsZElucHV0RmlsZS5pc0ZpbGVUeXBlQWxsb3dlZChmaWxlLnR5cGUpKVxuICAgICAgICB0aGlzLmZvcm1GaWVsZElucHV0RmlsZS5hZGRGaWxlKGZpbGUpO1xuICAgICAgZWxzZVxuICAgICAgICB0aGlzLnVpU2VydmljZS5vblNob3dTbmFja0JhcihcbiAgICAgICAgICBmaWxlLnR5cGUgKyAnIGlzIG5vdCBhbGxvd2VkJyxcbiAgICAgICAgICBudWxsLFxuICAgICAgICAgIDE1MDBcbiAgICAgICAgKTtcbiAgICB9KTtcbiAgfTtcblxuICBhZGRGaWxlcyA9ICgpOiB2b2lkID0+IHtcbiAgICB0aGlzLmZpbGUubmF0aXZlRWxlbWVudC5jbGljaygpO1xuICB9O1xuXG4gIG9uUmVtb3ZlRmlsZXMgPSAobWF0TGlzdE9wdGlvbnM6IE1hdExpc3RPcHRpb25bXSk6IHZvaWQgPT4ge1xuICAgIGZvciAobGV0IG1MT2luZGV4IGluIG1hdExpc3RPcHRpb25zKSB7XG4gICAgICBsZXQgbWF0TGlzdE9wdGlvbjogTWF0TGlzdE9wdGlvbiA9IG1hdExpc3RPcHRpb25zW21MT2luZGV4XTtcbiAgICAgIHRoaXMuZm9ybUZpZWxkSW5wdXRGaWxlLnNldERlZmF1bHRWYWx1ZShcbiAgICAgICAgdGhpcy5mb3JtRmllbGRJbnB1dEZpbGVcbiAgICAgICAgICAuZ2V0RGVmYXVsdFZhbHVlKClcbiAgICAgICAgICAuZmlsdGVyKChmKSA9PiBmICE9PSBtYXRMaXN0T3B0aW9uLnZhbHVlKVxuICAgICAgKTtcbiAgICB9XG4gIH07XG5cbiAgbmdPbkluaXQoKTogdm9pZCB7XG4gICAgLy8gY29uc29sZS5sb2coJ0Zvcm1GaWVsZElucHV0RmlsZUNvbXBvbmVudCBpbml0Jyk7XG4gIH1cblxuICBuZ09uRGVzdHJveSgpOiB2b2lkIHtcbiAgICAvLyBjb25zb2xlLmxvZygnRm9ybUZpZWxkSW5wdXRGaWxlQ29tcG9uZW50IGRlc3Ryb3llZCcpO1xuICB9XG5cbiAgbmdBZnRlclZpZXdJbml0KCk6IHZvaWQge1xuICAgIGlmICh0aGlzLmFwcERyYWdEcm9wKVxuICAgICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZShcbiAgICAgICAgdGhpcy5hcHBEcmFnRHJvcC5uYXRpdmVFbGVtZW50LFxuICAgICAgICAnYm9yZGVyJyxcbiAgICAgICAgJzJweCBkYXNoZWQgJyArIHRoaXMuZm9ybUZpZWxkSW5wdXRGaWxlLmdldEJvcmRlckNvbG9yKClcbiAgICAgICk7XG4gIH1cbn1cbiIsIjxzdHlsZT5cbiAgICBcbiAgICAuYWRkLXBhZGRpbmcge1xuICAgICAgICBwYWRkaW5nOiAyMHB4O1xuICAgIH1cblxuICAgIC5jdXJzb3Ige1xuICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgfVxuXG4gICAgaW5wdXRbdHlwZT1cImZpbGVcIl0ge1xuICAgICAgICBkaXNwbGF5OiBub25lO1xuICAgIH1cblxuICAgIGgzIHtcbiAgICAgICAgZm9udC1zaXplOiBpbmhlcml0O1xuICAgICAgICBtYXJnaW46IDA7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiBpbmhlcml0O1xuICAgIH1cblxuICAgIC51cGxvYWQtZmlsZS1jb250YWluZXIge1xuICAgICAgICBtaW4taGVpZ2h0OiAyMDBweDtcbiAgICAgICAgLyogbWF4LXdpZHRoOiAzMDBweDsgKi9cbiAgICAgICAgLyogd2lkdGg6IDgwJTsgKi9cbiAgICAgICAgbWFyZ2luOiA1cHggYXV0bzsgICAgICAgIFxuICAgICAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgIH1cblxuICAgIG1hdC1mb3JtLWZpZWxkIHtcbiAgICAgICAgbWluLXdpZHRoOiAyNTBweDtcbiAgICB9XG5cbjwvc3R5bGU+XG5cbjwhLS0gLS0tLS0tLS0tLS0tLS0tLS0tLSBmb3JtIGZpZWxkIGlucHV0IGZpbGUgd2l0aCBkcmFnIGFuZCBkcm9wIGFuZCBkZXZpY2UgaXMgZGVza3RvcCAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSAtLT5cbjxzZWN0aW9uXG4gICAgKm5nSWY9XCJ0aGlzLmZvcm1GaWVsZElucHV0RmlsZS5nZXREcmFnQW5kRHJvcCgpICYmIHRoaXMuZGV2aWNlU2VydmljZS5pc0RldmljZURlc2t0b3AoKVwiXG4gICAgZnhMYXlvdXQ9XCJjb2x1bW5cIlxuICAgIGZ4TGF5b3V0QWxpZ249XCJjZW50ZXIgY2VudGVyXCJcbiAgICBmeExheW91dEdhcD1cIjEwcHhcIlxuXG4gICAgY2xhc3M9XCJ1cGxvYWQtZmlsZS1jb250YWluZXIgYWRkLXBhZGRpbmdcIlxuICAgIGFwcERyYWdEcm9wXG4gICAgI2FwcERyYWdEcm9wXG4gICAgKG9uRmlsZURyb3BwZWQpPVwiIXRoaXMuZm9ybUZpZWxkSW5wdXRGaWxlLmdldERpc2FibGVkKCkgJiYgdGhpcy5vbkZpbGVEcm9wcGVkKCRldmVudClcIj5cblxuICAgICAgICA8aDM+e3sgdGhpcy5mb3JtRmllbGRJbnB1dEZpbGUuZ2V0TGFiZWxOYW1lKCkgfX08L2gzPlxuXG4gICAgICAgIDxtYXQtc2VsZWN0aW9uLWxpc3QgXG4gICAgICAgICAgICAjZmlsZXNcbiAgICAgICAgICAgICpuZ0lmPVwidGhpcy5mb3JtRmllbGRJbnB1dEZpbGUuZ2V0RGVmYXVsdFZhbHVlKCk/Lmxlbmd0aCA+IDBcIlxuICAgICAgICAgICAgZnhMYXlvdXQ9XCJjb2x1bW5cIlxuICAgICAgICAgICAgZnhMYXlvdXRBbGlnbj1cImNlbnRlciBjZW50ZXJcIlxuICAgICAgICAgICAgZnhMYXlvdXRHYXA9XCI4cHhcIj5cblxuICAgICAgICAgICAgICAgIDxtYXQtbGlzdC1vcHRpb24gXG4gICAgICAgICAgICAgICAgICAgICpuZ0Zvcj1cImxldCBpdGVtIG9mIHRoaXMuZm9ybUZpZWxkSW5wdXRGaWxlLmdldERlZmF1bHRWYWx1ZSgpXCIgXG4gICAgICAgICAgICAgICAgICAgIFt2YWx1ZV09XCJpdGVtXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICB7eyBpdGVtLm5hbWUgfX1cbiAgICAgICAgICAgICAgICA8L21hdC1saXN0LW9wdGlvbj5cblxuICAgICAgICAgICAgICAgIDxidXR0b25cbiAgICAgICAgICAgICAgICAgICAgKm5nSWY9XCJmaWxlcy5zZWxlY3RlZE9wdGlvbnMuc2VsZWN0ZWQubGVuZ3RoID4gMFwiXG4gICAgICAgICAgICAgICAgICAgIHR5cGU9XCJidXR0b25cIlxuICAgICAgICAgICAgICAgICAgICBbY29sb3JdPVwidGhpcy5mb3JtRmllbGRJbnB1dEZpbGUuZ2V0QnV0dG9uRGVsZXRlQ29sb3IoKVwiXG4gICAgICAgICAgICAgICAgICAgIChjbGljayk9XCJ0aGlzLm9uUmVtb3ZlRmlsZXMoZmlsZXMuc2VsZWN0ZWRPcHRpb25zLnNlbGVjdGVkKVwiXG4gICAgICAgICAgICAgICAgICAgIG1hdC1idXR0b24+XG4gICAgICAgICAgICAgICAgICAgICAgICB7eyB0aGlzLmZvcm1GaWVsZElucHV0RmlsZS5nZXRCdXR0b25EZWxldGVUZXh0KCkgfX1cbiAgICAgICAgICAgICAgICA8L2J1dHRvbj5cbiAgICAgICAgPC9tYXQtc2VsZWN0aW9uLWxpc3Q+ICAgIFxuXG4gICAgICAgIDxpbnB1dCBcbiAgICAgICAgICAgIHR5cGU9XCJmaWxlXCIgXG4gICAgICAgICAgICAjZmlsZSBcbiAgICAgICAgICAgIChjaGFuZ2UpPVwib25GaWxlc0FkZGVkKClcIiAgICAgICAgXG4gICAgICAgICAgICBtdWx0aXBsZS8+XG5cbiAgICAgICAgPGJ1dHRvbiBcbiAgICAgICAgICAgIHR5cGU9XCJidXR0b25cIlxuICAgICAgICAgICAgKGNsaWNrKT1cImFkZEZpbGVzKClcIlxuICAgICAgICAgICAgW2NvbG9yXT1cInRoaXMuZm9ybUZpZWxkSW5wdXRGaWxlLmdldEJ1dHRvbkFkZENvbG9yKClcIlxuICAgICAgICAgICAgbWF0LXJhaXNlZC1idXR0b25cbiAgICAgICAgICAgIFtkaXNhYmxlZF09XCJ0aGlzLmZvcm1GaWVsZElucHV0RmlsZS5nZXREaXNhYmxlZCgpXCI+XG4gICAgICAgICAgICAgICAge3sgdGhpcy5mb3JtRmllbGRJbnB1dEZpbGUuZ2V0QnV0dG9uQWRkVGV4dCgpIH19XG4gICAgICAgIDwvYnV0dG9uPlxuXG48L3NlY3Rpb24+XG5cbjwhLS0gLS0tLS0tLS0tLS0tLS0tLS0tLSBmb3JtIGZpZWxkIGlucHV0IGZpbGUgd2l0aCBkcmFnIGFuZCBkcm9wIGFuZCBkZXZpY2UgaXMgbm90IGRlc2t0b3AgLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0gLS0+XG48c2VjdGlvblxuICAgICpuZ0lmPVwidGhpcy5mb3JtRmllbGRJbnB1dEZpbGUuZ2V0RHJhZ0FuZERyb3AoKSAmJiAhdGhpcy5kZXZpY2VTZXJ2aWNlLmlzRGV2aWNlRGVza3RvcCgpXCJcbiAgICBmeExheW91dD1cImNvbHVtblwiXG4gICAgZnhMYXlvdXRBbGlnbj1cImNlbnRlciBjZW50ZXJcIlxuICAgIGZ4TGF5b3V0R2FwPVwiMTBweFwiXG4gICAgY2xhc3M9XCJhZGQtcGFkZGluZ1wiPlxuXG4gICAgICAgIDxoMz57eyB0aGlzLmZvcm1GaWVsZElucHV0RmlsZS5nZXRMYWJlbE5hbWUoKSB9fTwvaDM+XG5cbiAgICAgICAgPG1hdC1zZWxlY3Rpb24tbGlzdCBcbiAgICAgICAgICAgICNmaWxlc1xuICAgICAgICAgICAgKm5nSWY9XCJ0aGlzLmZvcm1GaWVsZElucHV0RmlsZS5nZXREZWZhdWx0VmFsdWUoKT8ubGVuZ3RoID4gMFwiXG4gICAgICAgICAgICBmeExheW91dD1cImNvbHVtblwiXG4gICAgICAgICAgICBmeExheW91dEFsaWduPVwiY2VudGVyIGNlbnRlclwiXG4gICAgICAgICAgICBmeExheW91dEdhcD1cIjhweFwiPlxuXG4gICAgICAgICAgICAgICAgPG1hdC1saXN0LW9wdGlvbiBcbiAgICAgICAgICAgICAgICAgICAgKm5nRm9yPVwibGV0IGl0ZW0gb2YgdGhpcy5mb3JtRmllbGRJbnB1dEZpbGUuZ2V0RGVmYXVsdFZhbHVlKClcIiBcbiAgICAgICAgICAgICAgICAgICAgW3ZhbHVlXT1cIml0ZW1cIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIHt7IGl0ZW0ubmFtZSB9fVxuICAgICAgICAgICAgICAgIDwvbWF0LWxpc3Qtb3B0aW9uPlxuXG4gICAgICAgICAgICAgICAgPGJ1dHRvblxuICAgICAgICAgICAgICAgICAgICAqbmdJZj1cImZpbGVzLnNlbGVjdGVkT3B0aW9ucy5zZWxlY3RlZC5sZW5ndGggPiAwXCJcbiAgICAgICAgICAgICAgICAgICAgdHlwZT1cImJ1dHRvblwiXG4gICAgICAgICAgICAgICAgICAgIFtjb2xvcl09XCJ0aGlzLmZvcm1GaWVsZElucHV0RmlsZS5nZXRCdXR0b25EZWxldGVDb2xvcigpXCJcbiAgICAgICAgICAgICAgICAgICAgKGNsaWNrKT1cInRoaXMub25SZW1vdmVGaWxlcyhmaWxlcy5zZWxlY3RlZE9wdGlvbnMuc2VsZWN0ZWQpXCJcbiAgICAgICAgICAgICAgICAgICAgbWF0LWJ1dHRvbj5cbiAgICAgICAgICAgICAgICAgICAgICAgIHt7IHRoaXMuZm9ybUZpZWxkSW5wdXRGaWxlLmdldEJ1dHRvbkRlbGV0ZVRleHQoKSB9fVxuICAgICAgICAgICAgICAgIDwvYnV0dG9uPlxuICAgICAgICA8L21hdC1zZWxlY3Rpb24tbGlzdD4gICAgXG5cbiAgICAgICAgPGlucHV0IFxuICAgICAgICAgICAgdHlwZT1cImZpbGVcIiBcbiAgICAgICAgICAgICNmaWxlIFxuICAgICAgICAgICAgKGNoYW5nZSk9XCJvbkZpbGVzQWRkZWQoKVwiICAgICAgICBcbiAgICAgICAgICAgIG11bHRpcGxlIC8+XG5cbiAgICAgICAgPGJ1dHRvbiBcbiAgICAgICAgICAgIHR5cGU9XCJidXR0b25cIlxuICAgICAgICAgICAgKGNsaWNrKT1cImFkZEZpbGVzKClcIlxuICAgICAgICAgICAgW2NvbG9yXT1cInRoaXMuZm9ybUZpZWxkSW5wdXRGaWxlLmdldEJ1dHRvbkFkZENvbG9yKClcIlxuICAgICAgICAgICAgbWF0LXJhaXNlZC1idXR0b25cbiAgICAgICAgICAgIFtkaXNhYmxlZF09XCJ0aGlzLmZvcm1GaWVsZElucHV0RmlsZS5nZXREaXNhYmxlZCgpXCI+XG4gICAgICAgICAgICAgICAge3sgdGhpcy5mb3JtRmllbGRJbnB1dEZpbGUuZ2V0QnV0dG9uQWRkVGV4dCgpIH19XG4gICAgICAgIDwvYnV0dG9uPlxuXG48L3NlY3Rpb24+XG5cbjwhLS0gLS0tLS0tLS0tLS0tLS0tLS0tLSBmb3JtIGZpZWxkIGlucHV0IGZpbGUgd2l0aG91dCBkcmFnIGFuZCBkcm9wIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tIC0tPlxuPHNlY3Rpb25cbiAgICAqbmdJZj1cIiF0aGlzLmZvcm1GaWVsZElucHV0RmlsZS5nZXREcmFnQW5kRHJvcCgpXCJcbiAgICBmeExheW91dD1cImNvbHVtblwiXG4gICAgZnhMYXlvdXRBbGlnbj1cImNlbnRlciBjZW50ZXJcIlxuICAgIGZ4TGF5b3V0R2FwPVwiMTBweFwiXG4gICAgY2xhc3M9XCJhZGQtcGFkZGluZ1wiPlxuXG4gICAgICAgIDxoMz57eyB0aGlzLmZvcm1GaWVsZElucHV0RmlsZS5nZXRMYWJlbE5hbWUoKSB9fTwvaDM+XG5cbiAgICAgICAgPG1hdC1zZWxlY3Rpb24tbGlzdCBcbiAgICAgICAgICAgICNmaWxlc1xuICAgICAgICAgICAgKm5nSWY9XCJ0aGlzLmZvcm1GaWVsZElucHV0RmlsZS5nZXREZWZhdWx0VmFsdWUoKT8ubGVuZ3RoID4gMFwiXG4gICAgICAgICAgICBmeExheW91dD1cImNvbHVtblwiXG4gICAgICAgICAgICBmeExheW91dEFsaWduPVwiY2VudGVyIGNlbnRlclwiXG4gICAgICAgICAgICBmeExheW91dEdhcD1cIjhweFwiPlxuXG4gICAgICAgICAgICAgICAgPG1hdC1saXN0LW9wdGlvbiBcbiAgICAgICAgICAgICAgICAgICAgKm5nRm9yPVwibGV0IGl0ZW0gb2YgdGhpcy5mb3JtRmllbGRJbnB1dEZpbGUuZ2V0RGVmYXVsdFZhbHVlKClcIiBcbiAgICAgICAgICAgICAgICAgICAgW3ZhbHVlXT1cIml0ZW1cIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIHt7IGl0ZW0ubmFtZSB9fVxuICAgICAgICAgICAgICAgIDwvbWF0LWxpc3Qtb3B0aW9uPlxuXG4gICAgICAgICAgICAgICAgPGJ1dHRvblxuICAgICAgICAgICAgICAgICAgICAqbmdJZj1cImZpbGVzLnNlbGVjdGVkT3B0aW9ucy5zZWxlY3RlZC5sZW5ndGggPiAwXCJcbiAgICAgICAgICAgICAgICAgICAgdHlwZT1cImJ1dHRvblwiXG4gICAgICAgICAgICAgICAgICAgIFtjb2xvcl09XCJ0aGlzLmZvcm1GaWVsZElucHV0RmlsZS5nZXRCdXR0b25EZWxldGVDb2xvcigpXCJcbiAgICAgICAgICAgICAgICAgICAgKGNsaWNrKT1cInRoaXMub25SZW1vdmVGaWxlcyhmaWxlcy5zZWxlY3RlZE9wdGlvbnMuc2VsZWN0ZWQpXCJcbiAgICAgICAgICAgICAgICAgICAgbWF0LWJ1dHRvbj5cbiAgICAgICAgICAgICAgICAgICAgICAgIHt7IHRoaXMuZm9ybUZpZWxkSW5wdXRGaWxlLmdldEJ1dHRvbkRlbGV0ZVRleHQoKSB9fVxuICAgICAgICAgICAgICAgIDwvYnV0dG9uPlxuICAgICAgICA8L21hdC1zZWxlY3Rpb24tbGlzdD4gICAgXG5cbiAgICAgICAgPGlucHV0IFxuICAgICAgICAgICAgdHlwZT1cImZpbGVcIiBcbiAgICAgICAgICAgICNmaWxlIFxuICAgICAgICAgICAgKGNoYW5nZSk9XCJvbkZpbGVzQWRkZWQoKVwiICAgICAgICBcbiAgICAgICAgICAgIG11bHRpcGxlIC8+XG5cbiAgICAgICAgPGJ1dHRvbiBcbiAgICAgICAgICAgIHR5cGU9XCJidXR0b25cIlxuICAgICAgICAgICAgKGNsaWNrKT1cImFkZEZpbGVzKClcIlxuICAgICAgICAgICAgW2NvbG9yXT1cInRoaXMuZm9ybUZpZWxkSW5wdXRGaWxlLmdldEJ1dHRvbkFkZENvbG9yKClcIlxuICAgICAgICAgICAgbWF0LXJhaXNlZC1idXR0b25cbiAgICAgICAgICAgIFtkaXNhYmxlZF09XCJ0aGlzLmZvcm1GaWVsZElucHV0RmlsZS5nZXREaXNhYmxlZCgpXCI+XG4gICAgICAgICAgICAgICAge3sgdGhpcy5mb3JtRmllbGRJbnB1dEZpbGUuZ2V0QnV0dG9uQWRkVGV4dCgpIH19XG4gICAgICAgIDwvYnV0dG9uPlxuXG48L3NlY3Rpb24+Il19