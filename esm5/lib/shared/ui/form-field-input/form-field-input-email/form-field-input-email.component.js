import { Component, Input, forwardRef } from "@angular/core";
import { NG_VALUE_ACCESSOR } from "@angular/forms";
import * as i0 from "@angular/core";
import * as i1 from "@angular/flex-layout/flex";
import * as i2 from "@angular/material/form-field";
import * as i3 from "@angular/common";
import * as i4 from "@angular/material/input";
import * as i5 from "@angular/material/icon";
function FormFieldInputEmailComponent_mat_label_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-label");
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r0 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r0.formFieldInputEmail.getLabelName());
} }
function FormFieldInputEmailComponent_mat_icon_5_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-icon", 9);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r2 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r2.formFieldInputEmail.getMatPrefixImgText());
} }
function FormFieldInputEmailComponent_mat_icon_6_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-icon", 10);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r3 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r3.formFieldInputEmail.getMatSuffixImgText());
} }
function FormFieldInputEmailComponent_span_7_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 9);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r4 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1("", ctx_r4.formFieldInputEmail.getTextPrefix(), "\u00A0");
} }
function FormFieldInputEmailComponent_span_8_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 10);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r5 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r5.formFieldInputEmail.getTextSuffix());
} }
function FormFieldInputEmailComponent_mat_hint_9_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-hint", 11);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r6 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r6.formFieldInputEmail.getLeftHintLabel());
} }
function FormFieldInputEmailComponent_mat_hint_10_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-hint", 12);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r7 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r7.formFieldInputEmail.getRightHintLabel());
} }
var FormFieldInputEmailComponent = /** @class */ (function () {
    function FormFieldInputEmailComponent() {
        var _this = this;
        this.onChange = function () { };
        this.onTouched = function () { };
        this.onKeyUp = function (input) {
            _this.formFieldInputEmail.setDefaultValue(input.value);
            _this.onChange(_this.formFieldInputEmail.getDefaultValue());
            _this.onTouched();
        };
    }
    FormFieldInputEmailComponent.prototype.registerOnChange = function (fn) {
        this.onChange = fn;
    };
    FormFieldInputEmailComponent.prototype.registerOnTouched = function (fn) {
        this.onTouched = fn;
    };
    FormFieldInputEmailComponent.prototype.writeValue = function (value) {
        if (value) {
            this.formFieldInputEmail.setDefaultValue(value);
        }
    };
    FormFieldInputEmailComponent.prototype.ngOnInit = function () {
        // console.log('FormFieldInputEmailComponent init');
    };
    FormFieldInputEmailComponent.prototype.ngOnDestroy = function () {
        // console.log('FormFieldInputEmailComponent destroyed');
    };
    FormFieldInputEmailComponent.ɵfac = function FormFieldInputEmailComponent_Factory(t) { return new (t || FormFieldInputEmailComponent)(); };
    FormFieldInputEmailComponent.ɵcmp = i0.ɵɵdefineComponent({ type: FormFieldInputEmailComponent, selectors: [["app-form-field-input-email"]], inputs: { formFieldInputEmail: "formFieldInputEmail" }, features: [i0.ɵɵProvidersFeature([
                {
                    provide: NG_VALUE_ACCESSOR,
                    multi: true,
                    useExisting: forwardRef(function () { return FormFieldInputEmailComponent; }),
                },
            ])], decls: 11, vars: 15, consts: [["fxLayoutAlign", "center center"], [3, "appearance", "hideRequiredMarker"], [4, "ngIf"], ["matInput", "", 3, "type", "placeholder", "value", "disabled", "readonly", "required", "keyup", "blur"], ["input", ""], ["matPrefix", "", 4, "ngIf"], ["matSuffix", "", 4, "ngIf"], ["align", "start", 4, "ngIf"], ["align", "end", 4, "ngIf"], ["matPrefix", ""], ["matSuffix", ""], ["align", "start"], ["align", "end"]], template: function FormFieldInputEmailComponent_Template(rf, ctx) { if (rf & 1) {
            var _r8 = i0.ɵɵgetCurrentView();
            i0.ɵɵelementStart(0, "section", 0);
            i0.ɵɵelementStart(1, "mat-form-field", 1);
            i0.ɵɵtemplate(2, FormFieldInputEmailComponent_mat_label_2_Template, 2, 1, "mat-label", 2);
            i0.ɵɵelementStart(3, "input", 3, 4);
            i0.ɵɵlistener("keyup", function FormFieldInputEmailComponent_Template_input_keyup_3_listener() { i0.ɵɵrestoreView(_r8); var _r1 = i0.ɵɵreference(4); return ctx.onKeyUp(_r1); })("blur", function FormFieldInputEmailComponent_Template_input_blur_3_listener() { i0.ɵɵrestoreView(_r8); var _r1 = i0.ɵɵreference(4); return ctx.onKeyUp(_r1); });
            i0.ɵɵelementEnd();
            i0.ɵɵtemplate(5, FormFieldInputEmailComponent_mat_icon_5_Template, 2, 1, "mat-icon", 5);
            i0.ɵɵtemplate(6, FormFieldInputEmailComponent_mat_icon_6_Template, 2, 1, "mat-icon", 6);
            i0.ɵɵtemplate(7, FormFieldInputEmailComponent_span_7_Template, 2, 1, "span", 5);
            i0.ɵɵtemplate(8, FormFieldInputEmailComponent_span_8_Template, 2, 1, "span", 6);
            i0.ɵɵtemplate(9, FormFieldInputEmailComponent_mat_hint_9_Template, 2, 1, "mat-hint", 7);
            i0.ɵɵtemplate(10, FormFieldInputEmailComponent_mat_hint_10_Template, 2, 1, "mat-hint", 8);
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        } if (rf & 2) {
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("appearance", ctx.formFieldInputEmail.getAppearance())("hideRequiredMarker", ctx.formFieldInputEmail.getRequired().hideRequiredMarker);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.formFieldInputEmail.getLabelName());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("type", ctx.formFieldInputEmail.getFormFieldInputType())("placeholder", ctx.formFieldInputEmail.getPlaceholder() ? ctx.formFieldInputEmail.getPlaceholder() : "")("value", ctx.formFieldInputEmail.getDefaultValue())("disabled", ctx.formFieldInputEmail.getDisabled())("readonly", ctx.formFieldInputEmail.getReadonly())("required", ctx.formFieldInputEmail.getRequired().required);
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("ngIf", ctx.formFieldInputEmail.getMatPrefixImgText());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.formFieldInputEmail.getMatSuffixImgText());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.formFieldInputEmail.getTextPrefix());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.formFieldInputEmail.getTextSuffix());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.formFieldInputEmail.getLeftHintLabel());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.formFieldInputEmail.getRightHintLabel());
        } }, directives: [i1.DefaultLayoutAlignDirective, i2.MatFormField, i3.NgIf, i4.MatInput, i2.MatLabel, i5.MatIcon, i2.MatPrefix, i2.MatSuffix, i2.MatHint], styles: ["", ".cursor[_ngcontent-%COMP%] {\n        cursor: pointer;\n    }\n\n    input[type=\"file\"][_ngcontent-%COMP%] {\n        display: none;\n    }\n\n    h3[_ngcontent-%COMP%] {\n        font-size: inherit;\n        margin: 0;\n        font-weight: inherit;\n    }\n\n    .upload-file-container[_ngcontent-%COMP%] {\n        min-height: 200px;\n        width: 80%;\n        margin: 20px auto;        \n        border-radius: 10px;\n    }\n\n    mat-form-field[_ngcontent-%COMP%] {\n        min-width: 250px;\n    }"] });
    return FormFieldInputEmailComponent;
}());
export { FormFieldInputEmailComponent };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FormFieldInputEmailComponent, [{
        type: Component,
        args: [{
                selector: "app-form-field-input-email",
                templateUrl: "./form-field-input-email.component.html",
                styleUrls: ["./form-field-input-email.component.css"],
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        multi: true,
                        useExisting: forwardRef(function () { return FormFieldInputEmailComponent; }),
                    },
                ],
            }]
    }], function () { return []; }, { formFieldInputEmail: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1maWVsZC1pbnB1dC1lbWFpbC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9teS1hbmd1bGFyLWZvcm0vIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL3VpL2Zvcm0tZmllbGQtaW5wdXQvZm9ybS1maWVsZC1pbnB1dC1lbWFpbC9mb3JtLWZpZWxkLWlucHV0LWVtYWlsLmNvbXBvbmVudC50cyIsImxpYi9zaGFyZWQvdWkvZm9ybS1maWVsZC1pbnB1dC9mb3JtLWZpZWxkLWlucHV0LWVtYWlsL2Zvcm0tZmllbGQtaW5wdXQtZW1haWwuY29tcG9uZW50Lmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxLQUFLLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRXJFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGdCQUFnQixDQUFDOzs7Ozs7OztJQ2tDbkMsaUNBQ29EO0lBQUEsWUFBNkM7SUFBQSxpQkFBWTs7O0lBQXpELGVBQTZDO0lBQTdDLCtEQUE2Qzs7O0lBY2pHLG1DQUVjO0lBQUEsWUFBb0Q7SUFBQSxpQkFBVzs7O0lBQS9ELGVBQW9EO0lBQXBELHNFQUFvRDs7O0lBRWxFLG9DQUVjO0lBQUEsWUFBb0Q7SUFBQSxpQkFBVzs7O0lBQS9ELGVBQW9EO0lBQXBELHNFQUFvRDs7O0lBRWxFLCtCQUVjO0lBQUEsWUFBb0Q7SUFBQSxpQkFBTzs7O0lBQTNELGVBQW9EO0lBQXBELCtFQUFvRDs7O0lBRWxFLGdDQUVjO0lBQUEsWUFBOEM7SUFBQSxpQkFBTzs7O0lBQXJELGVBQThDO0lBQTlDLGdFQUE4Qzs7O0lBRTVELG9DQUVrQjtJQUFBLFlBQWlEO0lBQUEsaUJBQVc7OztJQUE1RCxlQUFpRDtJQUFqRCxtRUFBaUQ7OztJQUVuRSxvQ0FFZ0I7SUFBQSxZQUFrRDtJQUFBLGlCQUFXOzs7SUFBN0QsZUFBa0Q7SUFBbEQsb0VBQWtEOztBRHJFbEY7SUFrQkU7UUFBQSxpQkFBZ0I7UUFIVCxhQUFRLEdBQVEsY0FBTyxDQUFDLENBQUM7UUFDekIsY0FBUyxHQUFRLGNBQU8sQ0FBQyxDQUFDO1FBSWpDLFlBQU8sR0FBRyxVQUFDLEtBQXVCO1lBQ2hDLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3RELEtBQUksQ0FBQyxRQUFRLENBQUMsS0FBSSxDQUFDLG1CQUFtQixDQUFDLGVBQWUsRUFBRSxDQUFDLENBQUM7WUFDMUQsS0FBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ25CLENBQUMsQ0FBQztJQU5hLENBQUM7SUFRaEIsdURBQWdCLEdBQWhCLFVBQWlCLEVBQUU7UUFDakIsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7SUFDckIsQ0FBQztJQUVELHdEQUFpQixHQUFqQixVQUFrQixFQUFFO1FBQ2xCLElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO0lBQ3RCLENBQUM7SUFFRCxpREFBVSxHQUFWLFVBQVcsS0FBSztRQUNkLElBQUksS0FBSyxFQUFFO1lBQ1QsSUFBSSxDQUFDLG1CQUFtQixDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUNqRDtJQUNILENBQUM7SUFFRCwrQ0FBUSxHQUFSO1FBQ0Usb0RBQW9EO0lBQ3RELENBQUM7SUFFRCxrREFBVyxHQUFYO1FBQ0UseURBQXlEO0lBQzNELENBQUM7NEdBbENVLDRCQUE0QjtxRUFBNUIsNEJBQTRCLHdJQVI1QjtnQkFDVDtvQkFDRSxPQUFPLEVBQUUsaUJBQWlCO29CQUMxQixLQUFLLEVBQUUsSUFBSTtvQkFDWCxXQUFXLEVBQUUsVUFBVSxDQUFDLGNBQU0sT0FBQSw0QkFBNEIsRUFBNUIsQ0FBNEIsQ0FBQztpQkFDNUQ7YUFDRjs7WUNlSCxrQ0FHUTtZQUFBLHlDQUlRO1lBQUEseUZBQ29EO1lBRXBELG1DQVlBO1lBUEksNEpBQVMsZ0JBQW1CLElBQUMsNklBQ3JCLGdCQUFtQixJQURFO1lBTGpDLGlCQVlBO1lBQUEsdUZBRWM7WUFFZCx1RkFFYztZQUVkLCtFQUVjO1lBRWQsK0VBRWM7WUFFZCx1RkFFa0I7WUFFbEIseUZBRWdCO1lBRXhCLGlCQUFpQjtZQUV6QixpQkFBVTs7WUE1Q0UsZUFBdUQ7WUFBdkQsb0VBQXVELGdGQUFBO1lBSS9DLGVBQStDO1lBQS9DLDZEQUErQztZQUkvQyxlQUF5RDtZQUF6RCxzRUFBeUQseUdBQUEsb0RBQUEsbURBQUEsbURBQUEsNERBQUE7WUFXekQsZUFBc0Q7WUFBdEQsb0VBQXNEO1lBSXRELGVBQXNEO1lBQXRELG9FQUFzRDtZQUl0RCxlQUFnRDtZQUFoRCw4REFBZ0Q7WUFJaEQsZUFBZ0Q7WUFBaEQsOERBQWdEO1lBSWhELGVBQW1EO1lBQW5ELGlFQUFtRDtZQUluRCxlQUFvRDtZQUFwRCxrRUFBb0Q7O3VDRHhFeEU7Q0FtREMsQUEvQ0QsSUErQ0M7U0FuQ1ksNEJBQTRCO2tEQUE1Qiw0QkFBNEI7Y0FaeEMsU0FBUztlQUFDO2dCQUNULFFBQVEsRUFBRSw0QkFBNEI7Z0JBQ3RDLFdBQVcsRUFBRSx5Q0FBeUM7Z0JBQ3RELFNBQVMsRUFBRSxDQUFDLHdDQUF3QyxDQUFDO2dCQUNyRCxTQUFTLEVBQUU7b0JBQ1Q7d0JBQ0UsT0FBTyxFQUFFLGlCQUFpQjt3QkFDMUIsS0FBSyxFQUFFLElBQUk7d0JBQ1gsV0FBVyxFQUFFLFVBQVUsQ0FBQyxjQUFNLE9BQUEsNEJBQTRCLEVBQTVCLENBQTRCLENBQUM7cUJBQzVEO2lCQUNGO2FBQ0Y7O2tCQUVFLEtBQUsiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQsIGZvcndhcmRSZWYgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgRm9ybUZpZWxkSW5wdXQgfSBmcm9tIFwiLi4vLi4vLi4vbW9kZWwvc3RydWN0dXJhbC9jb21wb3NpdGUvZm9ybS9mb3JtLWZpZWxkLWlucHV0L2Zvcm0tZmllbGQtaW5wdXQubW9kZWxcIjtcbmltcG9ydCB7IE5HX1ZBTFVFX0FDQ0VTU09SIH0gZnJvbSBcIkBhbmd1bGFyL2Zvcm1zXCI7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogXCJhcHAtZm9ybS1maWVsZC1pbnB1dC1lbWFpbFwiLFxuICB0ZW1wbGF0ZVVybDogXCIuL2Zvcm0tZmllbGQtaW5wdXQtZW1haWwuY29tcG9uZW50Lmh0bWxcIixcbiAgc3R5bGVVcmxzOiBbXCIuL2Zvcm0tZmllbGQtaW5wdXQtZW1haWwuY29tcG9uZW50LmNzc1wiXSxcbiAgcHJvdmlkZXJzOiBbXG4gICAge1xuICAgICAgcHJvdmlkZTogTkdfVkFMVUVfQUNDRVNTT1IsXG4gICAgICBtdWx0aTogdHJ1ZSxcbiAgICAgIHVzZUV4aXN0aW5nOiBmb3J3YXJkUmVmKCgpID0+IEZvcm1GaWVsZElucHV0RW1haWxDb21wb25lbnQpLFxuICAgIH0sXG4gIF0sXG59KVxuZXhwb3J0IGNsYXNzIEZvcm1GaWVsZElucHV0RW1haWxDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICBASW5wdXQoKSBmb3JtRmllbGRJbnB1dEVtYWlsOiBGb3JtRmllbGRJbnB1dDtcblxuICBwdWJsaWMgb25DaGFuZ2U6IGFueSA9ICgpID0+IHt9O1xuICBwdWJsaWMgb25Ub3VjaGVkOiBhbnkgPSAoKSA9PiB7fTtcblxuICBjb25zdHJ1Y3RvcigpIHt9XG5cbiAgb25LZXlVcCA9IChpbnB1dDogSFRNTElucHV0RWxlbWVudCkgPT4ge1xuICAgIHRoaXMuZm9ybUZpZWxkSW5wdXRFbWFpbC5zZXREZWZhdWx0VmFsdWUoaW5wdXQudmFsdWUpO1xuICAgIHRoaXMub25DaGFuZ2UodGhpcy5mb3JtRmllbGRJbnB1dEVtYWlsLmdldERlZmF1bHRWYWx1ZSgpKTtcbiAgICB0aGlzLm9uVG91Y2hlZCgpO1xuICB9O1xuXG4gIHJlZ2lzdGVyT25DaGFuZ2UoZm4pIHtcbiAgICB0aGlzLm9uQ2hhbmdlID0gZm47XG4gIH1cblxuICByZWdpc3Rlck9uVG91Y2hlZChmbikge1xuICAgIHRoaXMub25Ub3VjaGVkID0gZm47XG4gIH1cblxuICB3cml0ZVZhbHVlKHZhbHVlKSB7XG4gICAgaWYgKHZhbHVlKSB7XG4gICAgICB0aGlzLmZvcm1GaWVsZElucHV0RW1haWwuc2V0RGVmYXVsdFZhbHVlKHZhbHVlKTtcbiAgICB9XG4gIH1cblxuICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgICAvLyBjb25zb2xlLmxvZygnRm9ybUZpZWxkSW5wdXRFbWFpbENvbXBvbmVudCBpbml0Jyk7XG4gIH1cblxuICBuZ09uRGVzdHJveSgpOiB2b2lkIHtcbiAgICAvLyBjb25zb2xlLmxvZygnRm9ybUZpZWxkSW5wdXRFbWFpbENvbXBvbmVudCBkZXN0cm95ZWQnKTtcbiAgfVxufVxuIiwiPHN0eWxlPlxuXG4gICAgLmN1cnNvciB7XG4gICAgICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICB9XG5cbiAgICBpbnB1dFt0eXBlPVwiZmlsZVwiXSB7XG4gICAgICAgIGRpc3BsYXk6IG5vbmU7XG4gICAgfVxuXG4gICAgaDMge1xuICAgICAgICBmb250LXNpemU6IGluaGVyaXQ7XG4gICAgICAgIG1hcmdpbjogMDtcbiAgICAgICAgZm9udC13ZWlnaHQ6IGluaGVyaXQ7XG4gICAgfVxuXG4gICAgLnVwbG9hZC1maWxlLWNvbnRhaW5lciB7XG4gICAgICAgIG1pbi1oZWlnaHQ6IDIwMHB4O1xuICAgICAgICB3aWR0aDogODAlO1xuICAgICAgICBtYXJnaW46IDIwcHggYXV0bzsgICAgICAgIFxuICAgICAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgIH1cblxuICAgIG1hdC1mb3JtLWZpZWxkIHtcbiAgICAgICAgbWluLXdpZHRoOiAyNTBweDtcbiAgICB9XG5cbjwvc3R5bGU+XG5cbjxzZWN0aW9uXG4gICAgZnhMYXlvdXRBbGlnbj1cImNlbnRlciBjZW50ZXJcIj5cblxuICAgICAgICA8bWF0LWZvcm0tZmllbGQgXG4gICAgICAgICAgICBbYXBwZWFyYW5jZV09XCJ0aGlzLmZvcm1GaWVsZElucHV0RW1haWwuZ2V0QXBwZWFyYW5jZSgpXCJcbiAgICAgICAgICAgIFtoaWRlUmVxdWlyZWRNYXJrZXJdPVwidGhpcy5mb3JtRmllbGRJbnB1dEVtYWlsLmdldFJlcXVpcmVkKCkuaGlkZVJlcXVpcmVkTWFya2VyXCI+XG5cbiAgICAgICAgICAgICAgICA8bWF0LWxhYmVsXG4gICAgICAgICAgICAgICAgICAgICpuZ0lmPVwidGhpcy5mb3JtRmllbGRJbnB1dEVtYWlsLmdldExhYmVsTmFtZSgpXCI+e3sgdGhpcy5mb3JtRmllbGRJbnB1dEVtYWlsLmdldExhYmVsTmFtZSgpIH19PC9tYXQtbGFiZWw+XG5cbiAgICAgICAgICAgICAgICA8aW5wdXQgXG4gICAgICAgICAgICAgICAgICAgIG1hdElucHV0XG4gICAgICAgICAgICAgICAgICAgIFt0eXBlXT1cInRoaXMuZm9ybUZpZWxkSW5wdXRFbWFpbC5nZXRGb3JtRmllbGRJbnB1dFR5cGUoKVwiXG4gICAgICAgICAgICAgICAgICAgIFtwbGFjZWhvbGRlcl09XCJ0aGlzLmZvcm1GaWVsZElucHV0RW1haWwuZ2V0UGxhY2Vob2xkZXIoKSA/IHRoaXMuZm9ybUZpZWxkSW5wdXRFbWFpbC5nZXRQbGFjZWhvbGRlcigpIDogJydcIlxuICAgICAgICAgICAgICAgICAgICBbdmFsdWVdPVwidGhpcy5mb3JtRmllbGRJbnB1dEVtYWlsLmdldERlZmF1bHRWYWx1ZSgpXCJcbiAgICAgICAgICAgICAgICAgICAgKGtleXVwKT1cInRoaXMub25LZXlVcChpbnB1dClcIlxuICAgICAgICAgICAgICAgICAgICAoYmx1cik9XCJ0aGlzLm9uS2V5VXAoaW5wdXQpXCJcbiAgICAgICAgICAgICAgICAgICAgW2Rpc2FibGVkXT1cInRoaXMuZm9ybUZpZWxkSW5wdXRFbWFpbC5nZXREaXNhYmxlZCgpXCJcbiAgICAgICAgICAgICAgICAgICAgW3JlYWRvbmx5XT1cInRoaXMuZm9ybUZpZWxkSW5wdXRFbWFpbC5nZXRSZWFkb25seSgpXCJcbiAgICAgICAgICAgICAgICAgICAgW3JlcXVpcmVkXT1cInRoaXMuZm9ybUZpZWxkSW5wdXRFbWFpbC5nZXRSZXF1aXJlZCgpLnJlcXVpcmVkXCJcbiAgICAgICAgICAgICAgICAgICAgI2lucHV0PlxuXG4gICAgICAgICAgICAgICAgPG1hdC1pY29uIFxuICAgICAgICAgICAgICAgICAgICAqbmdJZj1cInRoaXMuZm9ybUZpZWxkSW5wdXRFbWFpbC5nZXRNYXRQcmVmaXhJbWdUZXh0KClcIlxuICAgICAgICAgICAgICAgICAgICBtYXRQcmVmaXg+e3sgdGhpcy5mb3JtRmllbGRJbnB1dEVtYWlsLmdldE1hdFByZWZpeEltZ1RleHQoKSB9fTwvbWF0LWljb24+XG5cbiAgICAgICAgICAgICAgICA8bWF0LWljb24gXG4gICAgICAgICAgICAgICAgICAgICpuZ0lmPVwidGhpcy5mb3JtRmllbGRJbnB1dEVtYWlsLmdldE1hdFN1ZmZpeEltZ1RleHQoKVwiXG4gICAgICAgICAgICAgICAgICAgIG1hdFN1ZmZpeD57eyB0aGlzLmZvcm1GaWVsZElucHV0RW1haWwuZ2V0TWF0U3VmZml4SW1nVGV4dCgpIH19PC9tYXQtaWNvbj5cblxuICAgICAgICAgICAgICAgIDxzcGFuIFxuICAgICAgICAgICAgICAgICAgICAqbmdJZj1cInRoaXMuZm9ybUZpZWxkSW5wdXRFbWFpbC5nZXRUZXh0UHJlZml4KClcIlxuICAgICAgICAgICAgICAgICAgICBtYXRQcmVmaXg+e3sgdGhpcy5mb3JtRmllbGRJbnB1dEVtYWlsLmdldFRleHRQcmVmaXgoKSB9fSZuYnNwOzwvc3Bhbj5cblxuICAgICAgICAgICAgICAgIDxzcGFuIFxuICAgICAgICAgICAgICAgICAgICAqbmdJZj1cInRoaXMuZm9ybUZpZWxkSW5wdXRFbWFpbC5nZXRUZXh0U3VmZml4KClcIlxuICAgICAgICAgICAgICAgICAgICBtYXRTdWZmaXg+e3sgdGhpcy5mb3JtRmllbGRJbnB1dEVtYWlsLmdldFRleHRTdWZmaXgoKSB9fTwvc3Bhbj5cblxuICAgICAgICAgICAgICAgIDxtYXQtaGludCBcbiAgICAgICAgICAgICAgICAgICAgKm5nSWY9XCJ0aGlzLmZvcm1GaWVsZElucHV0RW1haWwuZ2V0TGVmdEhpbnRMYWJlbCgpXCJcbiAgICAgICAgICAgICAgICAgICAgYWxpZ249XCJzdGFydFwiPnt7IHRoaXMuZm9ybUZpZWxkSW5wdXRFbWFpbC5nZXRMZWZ0SGludExhYmVsKCkgfX08L21hdC1oaW50PlxuXG4gICAgICAgICAgICAgICAgPG1hdC1oaW50XG4gICAgICAgICAgICAgICAgICAgICpuZ0lmPVwidGhpcy5mb3JtRmllbGRJbnB1dEVtYWlsLmdldFJpZ2h0SGludExhYmVsKClcIlxuICAgICAgICAgICAgICAgICAgICBhbGlnbj1cImVuZFwiPnt7IHRoaXMuZm9ybUZpZWxkSW5wdXRFbWFpbC5nZXRSaWdodEhpbnRMYWJlbCgpIH19PC9tYXQtaGludD5cblxuICAgICAgICA8L21hdC1mb3JtLWZpZWxkPlxuXG48L3NlY3Rpb24+Il19