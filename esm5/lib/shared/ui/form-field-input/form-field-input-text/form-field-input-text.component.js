import { Component, Input, forwardRef } from "@angular/core";
import { NG_VALUE_ACCESSOR } from "@angular/forms";
import { VALIDATOR_NAMES, } from "../../../model/structural/composite/form/form-row-item.model";
import * as i0 from "@angular/core";
import * as i1 from "@angular/flex-layout/flex";
import * as i2 from "@angular/material/form-field";
import * as i3 from "@angular/common";
import * as i4 from "@angular/material/input";
import * as i5 from "@angular/material/icon";
function FormFieldInputTextComponent_mat_label_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-label");
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r0 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r0.formFieldInputText.getLabelName());
} }
function FormFieldInputTextComponent_mat_icon_5_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-icon", 9);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r2 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r2.formFieldInputText.getMatPrefixImgText());
} }
function FormFieldInputTextComponent_mat_icon_6_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-icon", 10);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r3 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r3.formFieldInputText.getMatSuffixImgText());
} }
function FormFieldInputTextComponent_span_7_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 9);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r4 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1("", ctx_r4.formFieldInputText.getTextPrefix(), "\u00A0");
} }
function FormFieldInputTextComponent_span_8_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 10);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r5 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r5.formFieldInputText.getTextSuffix());
} }
function FormFieldInputTextComponent_mat_hint_9_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-hint", 11);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r6 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r6.formFieldInputText.getLeftHintLabel());
} }
function FormFieldInputTextComponent_mat_hint_10_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-hint", 12);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r7 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r7.formFieldInputText.getRightHintLabel());
} }
function FormFieldInputTextComponent_mat_hint_11_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-hint", 12);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r8 = i0.ɵɵnextContext();
    var tmp_0_0 = null;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate3("", ((tmp_0_0 = ctx_r8.getMinLengthValidator()) == null ? null : tmp_0_0.length) ? ctx_r8.getMinLengthValidator().length + " / " : "", "", (tmp_0_0 = ctx_r8.formFieldInputText.getDefaultValue()) == null ? null : tmp_0_0.length, "", ((tmp_0_0 = ctx_r8.getMaxLengthValidator()) == null ? null : tmp_0_0.length) ? " / " + ctx_r8.getMaxLengthValidator().length : "", "");
} }
function FormFieldInputTextComponent_mat_hint_12_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-hint", 12);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r9 = i0.ɵɵnextContext();
    var tmp_0_0 = null;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate2("", (tmp_0_0 = ctx_r9.formFieldInputText.getDefaultValue()) == null ? null : tmp_0_0.length, "", ((tmp_0_0 = ctx_r9.getMinLengthValidator()) == null ? null : tmp_0_0.length) ? " / " + ctx_r9.getMinLengthValidator().length : "", "");
} }
function FormFieldInputTextComponent_mat_hint_13_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-hint", 12);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r10 = i0.ɵɵnextContext();
    var tmp_0_0 = null;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate2("", (tmp_0_0 = ctx_r10.formFieldInputText.getDefaultValue()) == null ? null : tmp_0_0.length, "", ((tmp_0_0 = ctx_r10.getMaxLengthValidator()) == null ? null : tmp_0_0.length) ? " / " + ctx_r10.getMaxLengthValidator().length : "", "");
} }
var FormFieldInputTextComponent = /** @class */ (function () {
    function FormFieldInputTextComponent() {
        var _this = this;
        this.onChange = function () { };
        this.onTouched = function () { };
        this.onKeyUp = function (input) {
            // console.log(input.value);
            _this.formFieldInputText.setDefaultValue(input.value);
            _this.onChange(_this.formFieldInputText.getDefaultValue());
            _this.onTouched();
        };
        this.getMinLengthValidator = function () {
            var theValidator;
            _this.formFieldInputText
                .getValidators()
                .forEach(function (validator) {
                if (validator.name === VALIDATOR_NAMES.MIN_LENGTH)
                    theValidator = validator;
            });
            return theValidator;
        };
        this.getMaxLengthValidator = function () {
            var theValidator;
            _this.formFieldInputText
                .getValidators()
                .forEach(function (validator) {
                if (validator.name === VALIDATOR_NAMES.MAX_LENGTH)
                    theValidator = validator;
            });
            return theValidator;
        };
    }
    FormFieldInputTextComponent.prototype.registerOnChange = function (fn) {
        this.onChange = fn;
    };
    FormFieldInputTextComponent.prototype.registerOnTouched = function (fn) {
        this.onTouched = fn;
    };
    FormFieldInputTextComponent.prototype.writeValue = function (value) {
        if (value) {
            this.formFieldInputText.setDefaultValue(value);
        }
    };
    FormFieldInputTextComponent.prototype.ngOnInit = function () {
        // console.log('FormFieldInputTextComponent init');
    };
    FormFieldInputTextComponent.prototype.ngOnDestroy = function () {
        // console.log('FormFieldInputTextComponent destroyed');
    };
    FormFieldInputTextComponent.ɵfac = function FormFieldInputTextComponent_Factory(t) { return new (t || FormFieldInputTextComponent)(); };
    FormFieldInputTextComponent.ɵcmp = i0.ɵɵdefineComponent({ type: FormFieldInputTextComponent, selectors: [["app-form-field-input-text"]], inputs: { formFieldInputText: "formFieldInputText" }, features: [i0.ɵɵProvidersFeature([
                {
                    provide: NG_VALUE_ACCESSOR,
                    multi: true,
                    useExisting: forwardRef(function () { return FormFieldInputTextComponent; }),
                },
            ])], decls: 14, vars: 18, consts: [["fxLayoutAlign", "center center"], [3, "appearance", "hideRequiredMarker"], [4, "ngIf"], ["matInput", "", 3, "type", "placeholder", "value", "disabled", "readonly", "required", "keyup", "blur"], ["input", ""], ["matPrefix", "", 4, "ngIf"], ["matSuffix", "", 4, "ngIf"], ["align", "start", 4, "ngIf"], ["align", "end", 4, "ngIf"], ["matPrefix", ""], ["matSuffix", ""], ["align", "start"], ["align", "end"]], template: function FormFieldInputTextComponent_Template(rf, ctx) { if (rf & 1) {
            var _r11 = i0.ɵɵgetCurrentView();
            i0.ɵɵelementStart(0, "section", 0);
            i0.ɵɵelementStart(1, "mat-form-field", 1);
            i0.ɵɵtemplate(2, FormFieldInputTextComponent_mat_label_2_Template, 2, 1, "mat-label", 2);
            i0.ɵɵelementStart(3, "input", 3, 4);
            i0.ɵɵlistener("keyup", function FormFieldInputTextComponent_Template_input_keyup_3_listener() { i0.ɵɵrestoreView(_r11); var _r1 = i0.ɵɵreference(4); return ctx.onKeyUp(_r1); })("blur", function FormFieldInputTextComponent_Template_input_blur_3_listener() { i0.ɵɵrestoreView(_r11); var _r1 = i0.ɵɵreference(4); return ctx.onKeyUp(_r1); });
            i0.ɵɵelementEnd();
            i0.ɵɵtemplate(5, FormFieldInputTextComponent_mat_icon_5_Template, 2, 1, "mat-icon", 5);
            i0.ɵɵtemplate(6, FormFieldInputTextComponent_mat_icon_6_Template, 2, 1, "mat-icon", 6);
            i0.ɵɵtemplate(7, FormFieldInputTextComponent_span_7_Template, 2, 1, "span", 5);
            i0.ɵɵtemplate(8, FormFieldInputTextComponent_span_8_Template, 2, 1, "span", 6);
            i0.ɵɵtemplate(9, FormFieldInputTextComponent_mat_hint_9_Template, 2, 1, "mat-hint", 7);
            i0.ɵɵtemplate(10, FormFieldInputTextComponent_mat_hint_10_Template, 2, 1, "mat-hint", 8);
            i0.ɵɵtemplate(11, FormFieldInputTextComponent_mat_hint_11_Template, 2, 3, "mat-hint", 8);
            i0.ɵɵtemplate(12, FormFieldInputTextComponent_mat_hint_12_Template, 2, 2, "mat-hint", 8);
            i0.ɵɵtemplate(13, FormFieldInputTextComponent_mat_hint_13_Template, 2, 2, "mat-hint", 8);
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        } if (rf & 2) {
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("appearance", ctx.formFieldInputText.getAppearance())("hideRequiredMarker", ctx.formFieldInputText.getRequired().hideRequiredMarker);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.formFieldInputText.getLabelName());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("type", ctx.formFieldInputText.getFormFieldInputType())("placeholder", ctx.formFieldInputText.getPlaceholder() ? ctx.formFieldInputText.getPlaceholder() : "")("value", ctx.formFieldInputText.getDefaultValue())("disabled", ctx.formFieldInputText.getDisabled())("readonly", ctx.formFieldInputText.getReadonly())("required", ctx.formFieldInputText.getRequired().required);
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("ngIf", ctx.formFieldInputText.getMatPrefixImgText());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.formFieldInputText.getMatSuffixImgText());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.formFieldInputText.getTextPrefix());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.formFieldInputText.getTextSuffix());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.formFieldInputText.getLeftHintLabel());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.formFieldInputText.getRightHintLabel());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.formFieldInputText.getShowHintAboutMinMaxLength() && ctx.getMinLengthValidator() && ctx.getMaxLengthValidator());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.formFieldInputText.getShowHintAboutMinMaxLength() && ctx.getMinLengthValidator() && !ctx.getMaxLengthValidator());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.formFieldInputText.getShowHintAboutMinMaxLength() && !ctx.getMinLengthValidator() && ctx.getMaxLengthValidator());
        } }, directives: [i1.DefaultLayoutAlignDirective, i2.MatFormField, i3.NgIf, i4.MatInput, i2.MatLabel, i5.MatIcon, i2.MatPrefix, i2.MatSuffix, i2.MatHint], styles: ["", ".cursor[_ngcontent-%COMP%] {\n        cursor: pointer;\n    }\n\n    input[type=\"file\"][_ngcontent-%COMP%] {\n        display: none;\n    }\n\n    h3[_ngcontent-%COMP%] {\n        font-size: inherit;\n        margin: 0;\n        font-weight: inherit;\n    }\n\n    mat-form-field[_ngcontent-%COMP%] {\n        min-width: 250px;\n    }"] });
    return FormFieldInputTextComponent;
}());
export { FormFieldInputTextComponent };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FormFieldInputTextComponent, [{
        type: Component,
        args: [{
                selector: "app-form-field-input-text",
                templateUrl: "./form-field-input-text.component.html",
                styleUrls: ["./form-field-input-text.component.css"],
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        multi: true,
                        useExisting: forwardRef(function () { return FormFieldInputTextComponent; }),
                    },
                ],
            }]
    }], function () { return []; }, { formFieldInputText: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1maWVsZC1pbnB1dC10ZXh0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215LWFuZ3VsYXItZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvdWkvZm9ybS1maWVsZC1pbnB1dC9mb3JtLWZpZWxkLWlucHV0LXRleHQvZm9ybS1maWVsZC1pbnB1dC10ZXh0LmNvbXBvbmVudC50cyIsImxpYi9zaGFyZWQvdWkvZm9ybS1maWVsZC1pbnB1dC9mb3JtLWZpZWxkLWlucHV0LXRleHQvZm9ybS1maWVsZC1pbnB1dC10ZXh0LmNvbXBvbmVudC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQXFCLEtBQUssRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDaEYsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFJbkQsT0FBTyxFQUVMLGVBQWUsR0FDaEIsTUFBTSw4REFBOEQsQ0FBQzs7Ozs7Ozs7SUNzQnRELGlDQUNtRDtJQUFBLFlBQTRDO0lBQUEsaUJBQVk7OztJQUF4RCxlQUE0QztJQUE1Qyw4REFBNEM7OztJQWMvRixtQ0FFYztJQUFBLFlBQW1EO0lBQUEsaUJBQVc7OztJQUE5RCxlQUFtRDtJQUFuRCxxRUFBbUQ7OztJQUVqRSxvQ0FFYztJQUFBLFlBQW1EO0lBQUEsaUJBQVc7OztJQUE5RCxlQUFtRDtJQUFuRCxxRUFBbUQ7OztJQUVqRSwrQkFFYztJQUFBLFlBQW1EO0lBQUEsaUJBQU87OztJQUExRCxlQUFtRDtJQUFuRCw4RUFBbUQ7OztJQUVqRSxnQ0FFYztJQUFBLFlBQTZDO0lBQUEsaUJBQU87OztJQUFwRCxlQUE2QztJQUE3QywrREFBNkM7OztJQUUzRCxvQ0FFa0I7SUFBQSxZQUFnRDtJQUFBLGlCQUFXOzs7SUFBM0QsZUFBZ0Q7SUFBaEQsa0VBQWdEOzs7SUFFbEUsb0NBRWdCO0lBQUEsWUFBaUQ7SUFBQSxpQkFBVzs7O0lBQTVELGVBQWlEO0lBQWpELG1FQUFpRDs7O0lBR2pFLG9DQUVnQjtJQUFBLFlBQThPO0lBQUEsaUJBQVc7Ozs7SUFBelAsZUFBOE87SUFBOU8sb1lBQThPOzs7SUFFOVAsb0NBRWdCO0lBQUEsWUFBbUo7SUFBQSxpQkFBVzs7OztJQUE5SixlQUFtSjtJQUFuSiw2UEFBbUo7OztJQUVuSyxvQ0FFZ0I7SUFBQSxZQUFrSjtJQUFBLGlCQUFXOzs7O0lBQTdKLGVBQWtKO0lBQWxKLGdRQUFrSjs7QUR0RWxMO0lBa0JFO1FBQUEsaUJBQWdCO1FBSFQsYUFBUSxHQUFRLGNBQU8sQ0FBQyxDQUFDO1FBQ3pCLGNBQVMsR0FBUSxjQUFPLENBQUMsQ0FBQztRQUlqQyxZQUFPLEdBQUcsVUFBQyxLQUF1QjtZQUNoQyw0QkFBNEI7WUFDNUIsS0FBSSxDQUFDLGtCQUFrQixDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDckQsS0FBSSxDQUFDLFFBQVEsQ0FBQyxLQUFJLENBQUMsa0JBQWtCLENBQUMsZUFBZSxFQUFFLENBQUMsQ0FBQztZQUN6RCxLQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDbkIsQ0FBQyxDQUFDO1FBZ0JGLDBCQUFxQixHQUFHO1lBQ3RCLElBQUksWUFBWSxDQUFDO1lBQ2pCLEtBQUksQ0FBQyxrQkFBa0I7aUJBQ3BCLGFBQWEsRUFBRTtpQkFDZixPQUFPLENBQUMsVUFBQyxTQUF3QztnQkFDaEQsSUFBSSxTQUFTLENBQUMsSUFBSSxLQUFLLGVBQWUsQ0FBQyxVQUFVO29CQUMvQyxZQUFZLEdBQUcsU0FBUyxDQUFDO1lBQzdCLENBQUMsQ0FBQyxDQUFDO1lBQ0wsT0FBTyxZQUFZLENBQUM7UUFDdEIsQ0FBQyxDQUFDO1FBRUYsMEJBQXFCLEdBQUc7WUFDdEIsSUFBSSxZQUFZLENBQUM7WUFDakIsS0FBSSxDQUFDLGtCQUFrQjtpQkFDcEIsYUFBYSxFQUFFO2lCQUNmLE9BQU8sQ0FBQyxVQUFDLFNBQXdDO2dCQUNoRCxJQUFJLFNBQVMsQ0FBQyxJQUFJLEtBQUssZUFBZSxDQUFDLFVBQVU7b0JBQy9DLFlBQVksR0FBRyxTQUFTLENBQUM7WUFDN0IsQ0FBQyxDQUFDLENBQUM7WUFDTCxPQUFPLFlBQVksQ0FBQztRQUN0QixDQUFDLENBQUM7SUEzQ2EsQ0FBQztJQVNoQixzREFBZ0IsR0FBaEIsVUFBaUIsRUFBRTtRQUNqQixJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztJQUNyQixDQUFDO0lBRUQsdURBQWlCLEdBQWpCLFVBQWtCLEVBQUU7UUFDbEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7SUFDdEIsQ0FBQztJQUVELGdEQUFVLEdBQVYsVUFBVyxLQUFLO1FBQ2QsSUFBSSxLQUFLLEVBQUU7WUFDVCxJQUFJLENBQUMsa0JBQWtCLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ2hEO0lBQ0gsQ0FBQztJQXdCRCw4Q0FBUSxHQUFSO1FBQ0UsbURBQW1EO0lBQ3JELENBQUM7SUFFRCxpREFBVyxHQUFYO1FBQ0Usd0RBQXdEO0lBQzFELENBQUM7MEdBekRVLDJCQUEyQjtvRUFBM0IsMkJBQTJCLHFJQVIzQjtnQkFDVDtvQkFDRSxPQUFPLEVBQUUsaUJBQWlCO29CQUMxQixLQUFLLEVBQUUsSUFBSTtvQkFDWCxXQUFXLEVBQUUsVUFBVSxDQUFDLGNBQU0sT0FBQSwyQkFBMkIsRUFBM0IsQ0FBMkIsQ0FBQztpQkFDM0Q7YUFDRjs7WUNHSCxrQ0FHUTtZQUFBLHlDQUlRO1lBQUEsd0ZBQ21EO1lBRW5ELG1DQVlBO1lBUEksNEpBQVMsZ0JBQW1CLElBQUMsNklBQ3JCLGdCQUFtQixJQURFO1lBTGpDLGlCQVlBO1lBQUEsc0ZBRWM7WUFFZCxzRkFFYztZQUVkLDhFQUVjO1lBRWQsOEVBRWM7WUFFZCxzRkFFa0I7WUFFbEIsd0ZBRWdCO1lBR2hCLHdGQUVnQjtZQUVoQix3RkFFZ0I7WUFFaEIsd0ZBRWdCO1lBRXhCLGlCQUFpQjtZQUV6QixpQkFBVTs7WUF6REUsZUFBc0Q7WUFBdEQsbUVBQXNELCtFQUFBO1lBSTlDLGVBQThDO1lBQTlDLDREQUE4QztZQUk5QyxlQUF3RDtZQUF4RCxxRUFBd0QsdUdBQUEsbURBQUEsa0RBQUEsa0RBQUEsMkRBQUE7WUFXeEQsZUFBcUQ7WUFBckQsbUVBQXFEO1lBSXJELGVBQXFEO1lBQXJELG1FQUFxRDtZQUlyRCxlQUErQztZQUEvQyw2REFBK0M7WUFJL0MsZUFBK0M7WUFBL0MsNkRBQStDO1lBSS9DLGVBQWtEO1lBQWxELGdFQUFrRDtZQUlsRCxlQUFtRDtZQUFuRCxpRUFBbUQ7WUFLbkQsZUFBOEg7WUFBOUgsMElBQThIO1lBSTlILGVBQStIO1lBQS9ILDJJQUErSDtZQUkvSCxlQUErSDtZQUEvSCwySUFBK0g7O3NDRC9Fbko7Q0FnRkMsQUF0RUQsSUFzRUM7U0ExRFksMkJBQTJCO2tEQUEzQiwyQkFBMkI7Y0FadkMsU0FBUztlQUFDO2dCQUNULFFBQVEsRUFBRSwyQkFBMkI7Z0JBQ3JDLFdBQVcsRUFBRSx3Q0FBd0M7Z0JBQ3JELFNBQVMsRUFBRSxDQUFDLHVDQUF1QyxDQUFDO2dCQUNwRCxTQUFTLEVBQUU7b0JBQ1Q7d0JBQ0UsT0FBTyxFQUFFLGlCQUFpQjt3QkFDMUIsS0FBSyxFQUFFLElBQUk7d0JBQ1gsV0FBVyxFQUFFLFVBQVUsQ0FBQyxjQUFNLE9BQUEsMkJBQTJCLEVBQTNCLENBQTJCLENBQUM7cUJBQzNEO2lCQUNGO2FBQ0Y7O2tCQUVFLEtBQUsiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgT25EZXN0cm95LCBJbnB1dCwgZm9yd2FyZFJlZiB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBOR19WQUxVRV9BQ0NFU1NPUiB9IGZyb20gXCJAYW5ndWxhci9mb3Jtc1wiO1xuXG4vLyBtb2RlbFxuaW1wb3J0IHsgRm9ybUZpZWxkSW5wdXRUZXh0IH0gZnJvbSBcIi4uLy4uLy4uL21vZGVsL3N0cnVjdHVyYWwvY29tcG9zaXRlL2Zvcm0vZm9ybS1maWVsZC1pbnB1dC9mb3JtLWZpZWxkLWlucHV0LXRleHQubW9kZWxcIjtcbmltcG9ydCB7XG4gIEZvcm1Sb3dJdGVtVmFsaWRhdG9ySW50ZXJmYWNlLFxuICBWQUxJREFUT1JfTkFNRVMsXG59IGZyb20gXCIuLi8uLi8uLi9tb2RlbC9zdHJ1Y3R1cmFsL2NvbXBvc2l0ZS9mb3JtL2Zvcm0tcm93LWl0ZW0ubW9kZWxcIjtcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiBcImFwcC1mb3JtLWZpZWxkLWlucHV0LXRleHRcIixcbiAgdGVtcGxhdGVVcmw6IFwiLi9mb3JtLWZpZWxkLWlucHV0LXRleHQuY29tcG9uZW50Lmh0bWxcIixcbiAgc3R5bGVVcmxzOiBbXCIuL2Zvcm0tZmllbGQtaW5wdXQtdGV4dC5jb21wb25lbnQuY3NzXCJdLFxuICBwcm92aWRlcnM6IFtcbiAgICB7XG4gICAgICBwcm92aWRlOiBOR19WQUxVRV9BQ0NFU1NPUixcbiAgICAgIG11bHRpOiB0cnVlLFxuICAgICAgdXNlRXhpc3Rpbmc6IGZvcndhcmRSZWYoKCkgPT4gRm9ybUZpZWxkSW5wdXRUZXh0Q29tcG9uZW50KSxcbiAgICB9LFxuICBdLFxufSlcbmV4cG9ydCBjbGFzcyBGb3JtRmllbGRJbnB1dFRleHRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XG4gIEBJbnB1dCgpIGZvcm1GaWVsZElucHV0VGV4dDogRm9ybUZpZWxkSW5wdXRUZXh0O1xuXG4gIHB1YmxpYyBvbkNoYW5nZTogYW55ID0gKCkgPT4ge307XG4gIHB1YmxpYyBvblRvdWNoZWQ6IGFueSA9ICgpID0+IHt9O1xuXG4gIGNvbnN0cnVjdG9yKCkge31cblxuICBvbktleVVwID0gKGlucHV0OiBIVE1MSW5wdXRFbGVtZW50KSA9PiB7XG4gICAgLy8gY29uc29sZS5sb2coaW5wdXQudmFsdWUpO1xuICAgIHRoaXMuZm9ybUZpZWxkSW5wdXRUZXh0LnNldERlZmF1bHRWYWx1ZShpbnB1dC52YWx1ZSk7XG4gICAgdGhpcy5vbkNoYW5nZSh0aGlzLmZvcm1GaWVsZElucHV0VGV4dC5nZXREZWZhdWx0VmFsdWUoKSk7XG4gICAgdGhpcy5vblRvdWNoZWQoKTtcbiAgfTtcblxuICByZWdpc3Rlck9uQ2hhbmdlKGZuKSB7XG4gICAgdGhpcy5vbkNoYW5nZSA9IGZuO1xuICB9XG5cbiAgcmVnaXN0ZXJPblRvdWNoZWQoZm4pIHtcbiAgICB0aGlzLm9uVG91Y2hlZCA9IGZuO1xuICB9XG5cbiAgd3JpdGVWYWx1ZSh2YWx1ZSkge1xuICAgIGlmICh2YWx1ZSkge1xuICAgICAgdGhpcy5mb3JtRmllbGRJbnB1dFRleHQuc2V0RGVmYXVsdFZhbHVlKHZhbHVlKTtcbiAgICB9XG4gIH1cblxuICBnZXRNaW5MZW5ndGhWYWxpZGF0b3IgPSAoKTogRm9ybVJvd0l0ZW1WYWxpZGF0b3JJbnRlcmZhY2UgPT4ge1xuICAgIGxldCB0aGVWYWxpZGF0b3I7XG4gICAgdGhpcy5mb3JtRmllbGRJbnB1dFRleHRcbiAgICAgIC5nZXRWYWxpZGF0b3JzKClcbiAgICAgIC5mb3JFYWNoKCh2YWxpZGF0b3I6IEZvcm1Sb3dJdGVtVmFsaWRhdG9ySW50ZXJmYWNlKSA9PiB7XG4gICAgICAgIGlmICh2YWxpZGF0b3IubmFtZSA9PT0gVkFMSURBVE9SX05BTUVTLk1JTl9MRU5HVEgpXG4gICAgICAgICAgdGhlVmFsaWRhdG9yID0gdmFsaWRhdG9yO1xuICAgICAgfSk7XG4gICAgcmV0dXJuIHRoZVZhbGlkYXRvcjtcbiAgfTtcblxuICBnZXRNYXhMZW5ndGhWYWxpZGF0b3IgPSAoKTogRm9ybVJvd0l0ZW1WYWxpZGF0b3JJbnRlcmZhY2UgPT4ge1xuICAgIGxldCB0aGVWYWxpZGF0b3I7XG4gICAgdGhpcy5mb3JtRmllbGRJbnB1dFRleHRcbiAgICAgIC5nZXRWYWxpZGF0b3JzKClcbiAgICAgIC5mb3JFYWNoKCh2YWxpZGF0b3I6IEZvcm1Sb3dJdGVtVmFsaWRhdG9ySW50ZXJmYWNlKSA9PiB7XG4gICAgICAgIGlmICh2YWxpZGF0b3IubmFtZSA9PT0gVkFMSURBVE9SX05BTUVTLk1BWF9MRU5HVEgpXG4gICAgICAgICAgdGhlVmFsaWRhdG9yID0gdmFsaWRhdG9yO1xuICAgICAgfSk7XG4gICAgcmV0dXJuIHRoZVZhbGlkYXRvcjtcbiAgfTtcblxuICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgICAvLyBjb25zb2xlLmxvZygnRm9ybUZpZWxkSW5wdXRUZXh0Q29tcG9uZW50IGluaXQnKTtcbiAgfVxuXG4gIG5nT25EZXN0cm95KCk6IHZvaWQge1xuICAgIC8vIGNvbnNvbGUubG9nKCdGb3JtRmllbGRJbnB1dFRleHRDb21wb25lbnQgZGVzdHJveWVkJyk7XG4gIH1cbn1cbiIsIjxzdHlsZT5cblxuICAgIC5jdXJzb3Ige1xuICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgfVxuXG4gICAgaW5wdXRbdHlwZT1cImZpbGVcIl0ge1xuICAgICAgICBkaXNwbGF5OiBub25lO1xuICAgIH1cblxuICAgIGgzIHtcbiAgICAgICAgZm9udC1zaXplOiBpbmhlcml0O1xuICAgICAgICBtYXJnaW46IDA7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiBpbmhlcml0O1xuICAgIH1cblxuICAgIG1hdC1mb3JtLWZpZWxkIHtcbiAgICAgICAgbWluLXdpZHRoOiAyNTBweDtcbiAgICB9XG5cbjwvc3R5bGU+XG4gICAgXG48IS0tIC0tLS0tLS0tLS0tLS0tLS0tLS0gZm9ybSBmaWVsZCBpbnB1dCB0ZXh0IC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tIC0tPlxuPHNlY3Rpb25cbiAgICBmeExheW91dEFsaWduPVwiY2VudGVyIGNlbnRlclwiPlxuXG4gICAgICAgIDxtYXQtZm9ybS1maWVsZCBcbiAgICAgICAgICAgIFthcHBlYXJhbmNlXT1cInRoaXMuZm9ybUZpZWxkSW5wdXRUZXh0LmdldEFwcGVhcmFuY2UoKVwiXG4gICAgICAgICAgICBbaGlkZVJlcXVpcmVkTWFya2VyXT1cInRoaXMuZm9ybUZpZWxkSW5wdXRUZXh0LmdldFJlcXVpcmVkKCkuaGlkZVJlcXVpcmVkTWFya2VyXCI+XG5cbiAgICAgICAgICAgICAgICA8bWF0LWxhYmVsXG4gICAgICAgICAgICAgICAgICAgICpuZ0lmPVwidGhpcy5mb3JtRmllbGRJbnB1dFRleHQuZ2V0TGFiZWxOYW1lKClcIj57eyB0aGlzLmZvcm1GaWVsZElucHV0VGV4dC5nZXRMYWJlbE5hbWUoKSB9fTwvbWF0LWxhYmVsPlxuXG4gICAgICAgICAgICAgICAgPGlucHV0IFxuICAgICAgICAgICAgICAgICAgICBtYXRJbnB1dFxuICAgICAgICAgICAgICAgICAgICBbdHlwZV09XCJ0aGlzLmZvcm1GaWVsZElucHV0VGV4dC5nZXRGb3JtRmllbGRJbnB1dFR5cGUoKVwiXG4gICAgICAgICAgICAgICAgICAgIFtwbGFjZWhvbGRlcl09XCJ0aGlzLmZvcm1GaWVsZElucHV0VGV4dC5nZXRQbGFjZWhvbGRlcigpID8gdGhpcy5mb3JtRmllbGRJbnB1dFRleHQuZ2V0UGxhY2Vob2xkZXIoKSA6ICcnXCJcbiAgICAgICAgICAgICAgICAgICAgW3ZhbHVlXT1cInRoaXMuZm9ybUZpZWxkSW5wdXRUZXh0LmdldERlZmF1bHRWYWx1ZSgpXCJcbiAgICAgICAgICAgICAgICAgICAgKGtleXVwKT1cInRoaXMub25LZXlVcChpbnB1dClcIlxuICAgICAgICAgICAgICAgICAgICAoYmx1cik9XCJ0aGlzLm9uS2V5VXAoaW5wdXQpXCJcbiAgICAgICAgICAgICAgICAgICAgW2Rpc2FibGVkXT1cInRoaXMuZm9ybUZpZWxkSW5wdXRUZXh0LmdldERpc2FibGVkKClcIlxuICAgICAgICAgICAgICAgICAgICBbcmVhZG9ubHldPVwidGhpcy5mb3JtRmllbGRJbnB1dFRleHQuZ2V0UmVhZG9ubHkoKVwiXG4gICAgICAgICAgICAgICAgICAgIFtyZXF1aXJlZF09XCJ0aGlzLmZvcm1GaWVsZElucHV0VGV4dC5nZXRSZXF1aXJlZCgpLnJlcXVpcmVkXCJcbiAgICAgICAgICAgICAgICAgICAgI2lucHV0PlxuXG4gICAgICAgICAgICAgICAgPG1hdC1pY29uIFxuICAgICAgICAgICAgICAgICAgICAqbmdJZj1cInRoaXMuZm9ybUZpZWxkSW5wdXRUZXh0LmdldE1hdFByZWZpeEltZ1RleHQoKVwiXG4gICAgICAgICAgICAgICAgICAgIG1hdFByZWZpeD57eyB0aGlzLmZvcm1GaWVsZElucHV0VGV4dC5nZXRNYXRQcmVmaXhJbWdUZXh0KCkgfX08L21hdC1pY29uPlxuXG4gICAgICAgICAgICAgICAgPG1hdC1pY29uIFxuICAgICAgICAgICAgICAgICAgICAqbmdJZj1cInRoaXMuZm9ybUZpZWxkSW5wdXRUZXh0LmdldE1hdFN1ZmZpeEltZ1RleHQoKVwiXG4gICAgICAgICAgICAgICAgICAgIG1hdFN1ZmZpeD57eyB0aGlzLmZvcm1GaWVsZElucHV0VGV4dC5nZXRNYXRTdWZmaXhJbWdUZXh0KCkgfX08L21hdC1pY29uPlxuXG4gICAgICAgICAgICAgICAgPHNwYW4gXG4gICAgICAgICAgICAgICAgICAgICpuZ0lmPVwidGhpcy5mb3JtRmllbGRJbnB1dFRleHQuZ2V0VGV4dFByZWZpeCgpXCJcbiAgICAgICAgICAgICAgICAgICAgbWF0UHJlZml4Pnt7IHRoaXMuZm9ybUZpZWxkSW5wdXRUZXh0LmdldFRleHRQcmVmaXgoKSB9fSZuYnNwOzwvc3Bhbj5cblxuICAgICAgICAgICAgICAgIDxzcGFuIFxuICAgICAgICAgICAgICAgICAgICAqbmdJZj1cInRoaXMuZm9ybUZpZWxkSW5wdXRUZXh0LmdldFRleHRTdWZmaXgoKVwiXG4gICAgICAgICAgICAgICAgICAgIG1hdFN1ZmZpeD57eyB0aGlzLmZvcm1GaWVsZElucHV0VGV4dC5nZXRUZXh0U3VmZml4KCkgfX08L3NwYW4+XG5cbiAgICAgICAgICAgICAgICA8bWF0LWhpbnQgXG4gICAgICAgICAgICAgICAgICAgICpuZ0lmPVwidGhpcy5mb3JtRmllbGRJbnB1dFRleHQuZ2V0TGVmdEhpbnRMYWJlbCgpXCJcbiAgICAgICAgICAgICAgICAgICAgYWxpZ249XCJzdGFydFwiPnt7IHRoaXMuZm9ybUZpZWxkSW5wdXRUZXh0LmdldExlZnRIaW50TGFiZWwoKSB9fTwvbWF0LWhpbnQ+XG5cbiAgICAgICAgICAgICAgICA8bWF0LWhpbnRcbiAgICAgICAgICAgICAgICAgICAgKm5nSWY9XCJ0aGlzLmZvcm1GaWVsZElucHV0VGV4dC5nZXRSaWdodEhpbnRMYWJlbCgpXCJcbiAgICAgICAgICAgICAgICAgICAgYWxpZ249XCJlbmRcIj57eyB0aGlzLmZvcm1GaWVsZElucHV0VGV4dC5nZXRSaWdodEhpbnRMYWJlbCgpIH19PC9tYXQtaGludD5cblxuICAgICAgICAgICAgICAgIDwhLS0gaWYgZmllbGQgaGFzIFNob3dIaW50QWJvdXRNaW5NYXhMZW5ndGggc2V0IHRvIHRydWUgLS0+XG4gICAgICAgICAgICAgICAgPG1hdC1oaW50XG4gICAgICAgICAgICAgICAgICAgICpuZ0lmPVwidGhpcy5mb3JtRmllbGRJbnB1dFRleHQuZ2V0U2hvd0hpbnRBYm91dE1pbk1heExlbmd0aCgpICYmIHRoaXMuZ2V0TWluTGVuZ3RoVmFsaWRhdG9yKCkgJiYgdGhpcy5nZXRNYXhMZW5ndGhWYWxpZGF0b3IoKVwiXG4gICAgICAgICAgICAgICAgICAgIGFsaWduPVwiZW5kXCI+e3sgdGhpcy5nZXRNaW5MZW5ndGhWYWxpZGF0b3IoKT8ubGVuZ3RoID8gdGhpcy5nZXRNaW5MZW5ndGhWYWxpZGF0b3IoKS5sZW5ndGggKyBcIiAvIFwiIDogJyd9fXt7IHRoaXMuZm9ybUZpZWxkSW5wdXRUZXh0LmdldERlZmF1bHRWYWx1ZSgpPy5sZW5ndGggfX17eyB0aGlzLmdldE1heExlbmd0aFZhbGlkYXRvcigpPy5sZW5ndGggPyBcIiAvIFwiICsgdGhpcy5nZXRNYXhMZW5ndGhWYWxpZGF0b3IoKS5sZW5ndGg6ICcnfX08L21hdC1oaW50PlxuXG4gICAgICAgICAgICAgICAgPG1hdC1oaW50XG4gICAgICAgICAgICAgICAgICAgICpuZ0lmPVwidGhpcy5mb3JtRmllbGRJbnB1dFRleHQuZ2V0U2hvd0hpbnRBYm91dE1pbk1heExlbmd0aCgpICYmIHRoaXMuZ2V0TWluTGVuZ3RoVmFsaWRhdG9yKCkgJiYgIXRoaXMuZ2V0TWF4TGVuZ3RoVmFsaWRhdG9yKClcIlxuICAgICAgICAgICAgICAgICAgICBhbGlnbj1cImVuZFwiPnt7IHRoaXMuZm9ybUZpZWxkSW5wdXRUZXh0LmdldERlZmF1bHRWYWx1ZSgpPy5sZW5ndGggfX17eyB0aGlzLmdldE1pbkxlbmd0aFZhbGlkYXRvcigpPy5sZW5ndGggPyBcIiAvIFwiICsgdGhpcy5nZXRNaW5MZW5ndGhWYWxpZGF0b3IoKS5sZW5ndGggOiAnJ319PC9tYXQtaGludD5cblxuICAgICAgICAgICAgICAgIDxtYXQtaGludFxuICAgICAgICAgICAgICAgICAgICAqbmdJZj1cInRoaXMuZm9ybUZpZWxkSW5wdXRUZXh0LmdldFNob3dIaW50QWJvdXRNaW5NYXhMZW5ndGgoKSAmJiAhdGhpcy5nZXRNaW5MZW5ndGhWYWxpZGF0b3IoKSAmJiB0aGlzLmdldE1heExlbmd0aFZhbGlkYXRvcigpXCJcbiAgICAgICAgICAgICAgICAgICAgYWxpZ249XCJlbmRcIj57eyB0aGlzLmZvcm1GaWVsZElucHV0VGV4dC5nZXREZWZhdWx0VmFsdWUoKT8ubGVuZ3RoIH19e3sgdGhpcy5nZXRNYXhMZW5ndGhWYWxpZGF0b3IoKT8ubGVuZ3RoID8gXCIgLyBcIiArIHRoaXMuZ2V0TWF4TGVuZ3RoVmFsaWRhdG9yKCkubGVuZ3RoOiAnJ319PC9tYXQtaGludD5cblxuICAgICAgICA8L21hdC1mb3JtLWZpZWxkPlxuXG48L3NlY3Rpb24+Il19