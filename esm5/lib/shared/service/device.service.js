import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "ngx-device-detector";
var DeviceService = /** @class */ (function () {
    function DeviceService(deviceService) {
        var _this = this;
        this.deviceService = deviceService;
        this.isDeviceMobile = function () {
            return _this.deviceService.isMobile();
        };
        this.isDeviceTablet = function () {
            return _this.deviceService.isTablet();
        };
        this.isDeviceDesktop = function () {
            return _this.deviceService.isDesktop();
        };
        this.getDeviceInfo = function () {
            return _this.deviceService.getDeviceInfo();
        };
    }
    DeviceService.ɵfac = function DeviceService_Factory(t) { return new (t || DeviceService)(i0.ɵɵinject(i1.DeviceDetectorService)); };
    DeviceService.ɵprov = i0.ɵɵdefineInjectable({ token: DeviceService, factory: DeviceService.ɵfac, providedIn: 'root' });
    return DeviceService;
}());
export { DeviceService };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(DeviceService, [{
        type: Injectable,
        args: [{ providedIn: 'root' }]
    }], function () { return [{ type: i1.DeviceDetectorService }]; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGV2aWNlLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9teS1hbmd1bGFyLWZvcm0vIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL3NlcnZpY2UvZGV2aWNlLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQzs7O0FBSzNDO0lBRUUsdUJBQW9CLGFBQW9DO1FBQXhELGlCQUE0RDtRQUF4QyxrQkFBYSxHQUFiLGFBQWEsQ0FBdUI7UUFFeEQsbUJBQWMsR0FBRztZQUNmLE9BQU8sS0FBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUN2QyxDQUFDLENBQUM7UUFFRixtQkFBYyxHQUFHO1lBQ2YsT0FBTyxLQUFJLENBQUMsYUFBYSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQ3ZDLENBQUMsQ0FBQztRQUVGLG9CQUFlLEdBQUc7WUFDaEIsT0FBTyxLQUFJLENBQUMsYUFBYSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ3hDLENBQUMsQ0FBQztRQUVGLGtCQUFhLEdBQUc7WUFDZCxPQUFPLEtBQUksQ0FBQyxhQUFhLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDNUMsQ0FBQyxDQUFDO0lBaEJ5RCxDQUFDOzhFQURqRCxhQUFhO3lEQUFiLGFBQWEsV0FBYixhQUFhLG1CQURBLE1BQU07d0JBTGhDO0NBd0JDLEFBbkJELElBbUJDO1NBbEJZLGFBQWE7a0RBQWIsYUFBYTtjQUR6QixVQUFVO2VBQUMsRUFBRSxVQUFVLEVBQUUsTUFBTSxFQUFFIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuLy8gc2VydmljZVxyXG5pbXBvcnQgeyBEZXZpY2VEZXRlY3RvclNlcnZpY2UsIERldmljZUluZm8gfSBmcm9tICduZ3gtZGV2aWNlLWRldGVjdG9yJztcclxuXHJcbkBJbmplY3RhYmxlKHsgcHJvdmlkZWRJbjogJ3Jvb3QnIH0pXHJcbmV4cG9ydCBjbGFzcyBEZXZpY2VTZXJ2aWNlIHtcclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGRldmljZVNlcnZpY2U6IERldmljZURldGVjdG9yU2VydmljZSkge31cclxuXHJcbiAgaXNEZXZpY2VNb2JpbGUgPSAoKTogYm9vbGVhbiA9PiB7XHJcbiAgICByZXR1cm4gdGhpcy5kZXZpY2VTZXJ2aWNlLmlzTW9iaWxlKCk7XHJcbiAgfTtcclxuXHJcbiAgaXNEZXZpY2VUYWJsZXQgPSAoKTogYm9vbGVhbiA9PiB7XHJcbiAgICByZXR1cm4gdGhpcy5kZXZpY2VTZXJ2aWNlLmlzVGFibGV0KCk7XHJcbiAgfTtcclxuXHJcbiAgaXNEZXZpY2VEZXNrdG9wID0gKCk6IGJvb2xlYW4gPT4ge1xyXG4gICAgcmV0dXJuIHRoaXMuZGV2aWNlU2VydmljZS5pc0Rlc2t0b3AoKTtcclxuICB9O1xyXG5cclxuICBnZXREZXZpY2VJbmZvID0gKCk6IERldmljZUluZm8gPT4ge1xyXG4gICAgcmV0dXJuIHRoaXMuZGV2aWNlU2VydmljZS5nZXREZXZpY2VJbmZvKCk7XHJcbiAgfTtcclxufVxyXG4iXX0=