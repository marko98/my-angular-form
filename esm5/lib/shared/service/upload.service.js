import { Injectable } from '@angular/core';
import { HttpRequest, HttpEventType, HttpResponse, } from '@angular/common/http';
import { Subject } from 'rxjs';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
var url = 'http://localhost:3000/upload';
var UploadService = /** @class */ (function () {
    function UploadService(http) {
        this.http = http;
    }
    UploadService.prototype.upload = function (files) {
        var _this = this;
        // this will be the our resulting map
        var status = {};
        files.forEach(function (file) {
            // create a new multipart-form for every file
            var formData = new FormData();
            formData.append('file', file, file.name);
            // create a http-post request and pass the form
            // tell it to report the upload progress
            var req = new HttpRequest('POST', url, formData, {
                reportProgress: true,
            });
            // create a new progress-subject for every file
            var progress = new Subject();
            // send the http-request and subscribe for progress-updates
            _this.http.request(req).subscribe(function (event) {
                if (event.type === HttpEventType.UploadProgress) {
                    // calculate the progress percentage
                    var percentDone = Math.round((100 * event.loaded) / event.total);
                    // pass the percentage into the progress-stream
                    console.log(percentDone);
                    progress.next(percentDone);
                }
                else if (event instanceof HttpResponse) {
                    // Close the progress-stream if we get an answer form the API
                    // The upload is complete
                    progress.complete();
                }
            });
            // Save every progress-observable in a map of all observables
            status[file.name] = {
                progress: progress.asObservable(),
            };
        });
        // return the map of progress.observables
        return status;
    };
    UploadService.ɵfac = function UploadService_Factory(t) { return new (t || UploadService)(i0.ɵɵinject(i1.HttpClient)); };
    UploadService.ɵprov = i0.ɵɵdefineInjectable({ token: UploadService, factory: UploadService.ɵfac, providedIn: 'root' });
    return UploadService;
}());
export { UploadService };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(UploadService, [{
        type: Injectable,
        args: [{ providedIn: 'root' }]
    }], function () { return [{ type: i1.HttpClient }]; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXBsb2FkLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9teS1hbmd1bGFyLWZvcm0vIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL3NlcnZpY2UvdXBsb2FkLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBRUwsV0FBVyxFQUNYLGFBQWEsRUFDYixZQUFZLEdBQ2IsTUFBTSxzQkFBc0IsQ0FBQztBQUM5QixPQUFPLEVBQUUsT0FBTyxFQUFjLE1BQU0sTUFBTSxDQUFDOzs7QUFFM0MsSUFBTSxHQUFHLEdBQUcsOEJBQThCLENBQUM7QUFFM0M7SUFFRSx1QkFBb0IsSUFBZ0I7UUFBaEIsU0FBSSxHQUFKLElBQUksQ0FBWTtJQUFHLENBQUM7SUFFakMsOEJBQU0sR0FBYixVQUNFLEtBQWdCO1FBRGxCLGlCQStDQztRQTVDQyxxQ0FBcUM7UUFDckMsSUFBTSxNQUFNLEdBQXdELEVBQUUsQ0FBQztRQUV2RSxLQUFLLENBQUMsT0FBTyxDQUFDLFVBQUMsSUFBSTtZQUNqQiw2Q0FBNkM7WUFDN0MsSUFBTSxRQUFRLEdBQWEsSUFBSSxRQUFRLEVBQUUsQ0FBQztZQUMxQyxRQUFRLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBRXpDLCtDQUErQztZQUMvQyx3Q0FBd0M7WUFDeEMsSUFBTSxHQUFHLEdBQUcsSUFBSSxXQUFXLENBQUMsTUFBTSxFQUFFLEdBQUcsRUFBRSxRQUFRLEVBQUU7Z0JBQ2pELGNBQWMsRUFBRSxJQUFJO2FBQ3JCLENBQUMsQ0FBQztZQUVILCtDQUErQztZQUMvQyxJQUFNLFFBQVEsR0FBRyxJQUFJLE9BQU8sRUFBVSxDQUFDO1lBRXZDLDJEQUEyRDtZQUUzRCxLQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQyxLQUFLO2dCQUNyQyxJQUFJLEtBQUssQ0FBQyxJQUFJLEtBQUssYUFBYSxDQUFDLGNBQWMsRUFBRTtvQkFDL0Msb0NBQW9DO29CQUVwQyxJQUFNLFdBQVcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUMsR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQ25FLCtDQUErQztvQkFFL0MsT0FBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQztvQkFFekIsUUFBUSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztpQkFDNUI7cUJBQU0sSUFBSSxLQUFLLFlBQVksWUFBWSxFQUFFO29CQUN4Qyw2REFBNkQ7b0JBQzdELHlCQUF5QjtvQkFDekIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUNyQjtZQUNILENBQUMsQ0FBQyxDQUFDO1lBRUgsNkRBQTZEO1lBQzdELE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUc7Z0JBQ2xCLFFBQVEsRUFBRSxRQUFRLENBQUMsWUFBWSxFQUFFO2FBQ2xDLENBQUM7UUFDSixDQUFDLENBQUMsQ0FBQztRQUVILHlDQUF5QztRQUN6QyxPQUFPLE1BQU0sQ0FBQztJQUNoQixDQUFDOzhFQWxEVSxhQUFhO3lEQUFiLGFBQWEsV0FBYixhQUFhLG1CQURBLE1BQU07d0JBWGhDO0NBK0RDLEFBcERELElBb0RDO1NBbkRZLGFBQWE7a0RBQWIsYUFBYTtjQUR6QixVQUFVO2VBQUMsRUFBRSxVQUFVLEVBQUUsTUFBTSxFQUFFIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQge1xyXG4gIEh0dHBDbGllbnQsXHJcbiAgSHR0cFJlcXVlc3QsXHJcbiAgSHR0cEV2ZW50VHlwZSxcclxuICBIdHRwUmVzcG9uc2UsXHJcbn0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xyXG5pbXBvcnQgeyBTdWJqZWN0LCBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XHJcblxyXG5jb25zdCB1cmwgPSAnaHR0cDovL2xvY2FsaG9zdDozMDAwL3VwbG9hZCc7XHJcblxyXG5ASW5qZWN0YWJsZSh7IHByb3ZpZGVkSW46ICdyb290JyB9KVxyXG5leHBvcnQgY2xhc3MgVXBsb2FkU2VydmljZSB7XHJcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBodHRwOiBIdHRwQ2xpZW50KSB7fVxyXG5cclxuICBwdWJsaWMgdXBsb2FkKFxyXG4gICAgZmlsZXM6IFNldDxGaWxlPlxyXG4gICk6IHsgW2tleTogc3RyaW5nXTogeyBwcm9ncmVzczogT2JzZXJ2YWJsZTxudW1iZXI+IH0gfSB7XHJcbiAgICAvLyB0aGlzIHdpbGwgYmUgdGhlIG91ciByZXN1bHRpbmcgbWFwXHJcbiAgICBjb25zdCBzdGF0dXM6IHsgW2tleTogc3RyaW5nXTogeyBwcm9ncmVzczogT2JzZXJ2YWJsZTxudW1iZXI+IH0gfSA9IHt9O1xyXG5cclxuICAgIGZpbGVzLmZvckVhY2goKGZpbGUpID0+IHtcclxuICAgICAgLy8gY3JlYXRlIGEgbmV3IG11bHRpcGFydC1mb3JtIGZvciBldmVyeSBmaWxlXHJcbiAgICAgIGNvbnN0IGZvcm1EYXRhOiBGb3JtRGF0YSA9IG5ldyBGb3JtRGF0YSgpO1xyXG4gICAgICBmb3JtRGF0YS5hcHBlbmQoJ2ZpbGUnLCBmaWxlLCBmaWxlLm5hbWUpO1xyXG5cclxuICAgICAgLy8gY3JlYXRlIGEgaHR0cC1wb3N0IHJlcXVlc3QgYW5kIHBhc3MgdGhlIGZvcm1cclxuICAgICAgLy8gdGVsbCBpdCB0byByZXBvcnQgdGhlIHVwbG9hZCBwcm9ncmVzc1xyXG4gICAgICBjb25zdCByZXEgPSBuZXcgSHR0cFJlcXVlc3QoJ1BPU1QnLCB1cmwsIGZvcm1EYXRhLCB7XHJcbiAgICAgICAgcmVwb3J0UHJvZ3Jlc3M6IHRydWUsXHJcbiAgICAgIH0pO1xyXG5cclxuICAgICAgLy8gY3JlYXRlIGEgbmV3IHByb2dyZXNzLXN1YmplY3QgZm9yIGV2ZXJ5IGZpbGVcclxuICAgICAgY29uc3QgcHJvZ3Jlc3MgPSBuZXcgU3ViamVjdDxudW1iZXI+KCk7XHJcblxyXG4gICAgICAvLyBzZW5kIHRoZSBodHRwLXJlcXVlc3QgYW5kIHN1YnNjcmliZSBmb3IgcHJvZ3Jlc3MtdXBkYXRlc1xyXG5cclxuICAgICAgdGhpcy5odHRwLnJlcXVlc3QocmVxKS5zdWJzY3JpYmUoKGV2ZW50KSA9PiB7XHJcbiAgICAgICAgaWYgKGV2ZW50LnR5cGUgPT09IEh0dHBFdmVudFR5cGUuVXBsb2FkUHJvZ3Jlc3MpIHtcclxuICAgICAgICAgIC8vIGNhbGN1bGF0ZSB0aGUgcHJvZ3Jlc3MgcGVyY2VudGFnZVxyXG5cclxuICAgICAgICAgIGNvbnN0IHBlcmNlbnREb25lID0gTWF0aC5yb3VuZCgoMTAwICogZXZlbnQubG9hZGVkKSAvIGV2ZW50LnRvdGFsKTtcclxuICAgICAgICAgIC8vIHBhc3MgdGhlIHBlcmNlbnRhZ2UgaW50byB0aGUgcHJvZ3Jlc3Mtc3RyZWFtXHJcblxyXG4gICAgICAgICAgY29uc29sZS5sb2cocGVyY2VudERvbmUpO1xyXG5cclxuICAgICAgICAgIHByb2dyZXNzLm5leHQocGVyY2VudERvbmUpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoZXZlbnQgaW5zdGFuY2VvZiBIdHRwUmVzcG9uc2UpIHtcclxuICAgICAgICAgIC8vIENsb3NlIHRoZSBwcm9ncmVzcy1zdHJlYW0gaWYgd2UgZ2V0IGFuIGFuc3dlciBmb3JtIHRoZSBBUElcclxuICAgICAgICAgIC8vIFRoZSB1cGxvYWQgaXMgY29tcGxldGVcclxuICAgICAgICAgIHByb2dyZXNzLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuXHJcbiAgICAgIC8vIFNhdmUgZXZlcnkgcHJvZ3Jlc3Mtb2JzZXJ2YWJsZSBpbiBhIG1hcCBvZiBhbGwgb2JzZXJ2YWJsZXNcclxuICAgICAgc3RhdHVzW2ZpbGUubmFtZV0gPSB7XHJcbiAgICAgICAgcHJvZ3Jlc3M6IHByb2dyZXNzLmFzT2JzZXJ2YWJsZSgpLFxyXG4gICAgICB9O1xyXG4gICAgfSk7XHJcblxyXG4gICAgLy8gcmV0dXJuIHRoZSBtYXAgb2YgcHJvZ3Jlc3Mub2JzZXJ2YWJsZXNcclxuICAgIHJldHVybiBzdGF0dXM7XHJcbiAgfVxyXG59XHJcbiJdfQ==