import { Directive, Output, EventEmitter, HostBinding, HostListener, } from '@angular/core';
import * as i0 from "@angular/core";
// https://medium.com/@mariemchabeni/angular-7-drag-and-drop-simple-file-uploadin-in-less-than-5-minutes-d57eb010c0dc
var DragDropDirective = /** @class */ (function () {
    function DragDropDirective() {
        this.onFileDropped = new EventEmitter();
        this.opacity = '1';
    }
    //Dragover listener
    DragDropDirective.prototype.onDragOver = function (evt) {
        evt.preventDefault();
        evt.stopPropagation();
        this.opacity = '0.5';
    };
    //Dragleave listener
    DragDropDirective.prototype.onDragLeave = function (evt) {
        evt.preventDefault();
        evt.stopPropagation();
        this.opacity = '1';
    };
    //Drop listener
    DragDropDirective.prototype.onDrop = function (evt) {
        evt.preventDefault();
        evt.stopPropagation();
        this.opacity = '1';
        var files = evt.dataTransfer.files;
        if (files.length > 0) {
            this.onFileDropped.emit(files);
        }
    };
    DragDropDirective.ɵfac = function DragDropDirective_Factory(t) { return new (t || DragDropDirective)(); };
    DragDropDirective.ɵdir = i0.ɵɵdefineDirective({ type: DragDropDirective, selectors: [["", "appDragDrop", ""]], hostVars: 2, hostBindings: function DragDropDirective_HostBindings(rf, ctx) { if (rf & 1) {
            i0.ɵɵlistener("dragover", function DragDropDirective_dragover_HostBindingHandler($event) { return ctx.onDragOver($event); })("dragleave", function DragDropDirective_dragleave_HostBindingHandler($event) { return ctx.onDragLeave($event); })("drop", function DragDropDirective_drop_HostBindingHandler($event) { return ctx.onDrop($event); });
        } if (rf & 2) {
            i0.ɵɵstyleProp("opacity", ctx.opacity);
        } }, outputs: { onFileDropped: "onFileDropped" } });
    return DragDropDirective;
}());
export { DragDropDirective };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(DragDropDirective, [{
        type: Directive,
        args: [{
                selector: '[appDragDrop]',
            }]
    }], null, { onFileDropped: [{
            type: Output
        }], opacity: [{
            type: HostBinding,
            args: ['style.opacity']
        }], onDragOver: [{
            type: HostListener,
            args: ['dragover', ['$event']]
        }], onDragLeave: [{
            type: HostListener,
            args: ['dragleave', ['$event']]
        }], onDrop: [{
            type: HostListener,
            args: ['drop', ['$event']]
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJhZy1kcm9wLmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215LWFuZ3VsYXItZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvZGlyZWN0aXZlL2RyYWctZHJvcC5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUNMLFNBQVMsRUFDVCxNQUFNLEVBRU4sWUFBWSxFQUNaLFdBQVcsRUFDWCxZQUFZLEdBRWIsTUFBTSxlQUFlLENBQUM7O0FBRXZCLHFIQUFxSDtBQUNySDtJQUFBO1FBSVksa0JBQWEsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBRVosWUFBTyxHQUFHLEdBQUcsQ0FBQztLQTBCckQ7SUF4QkMsbUJBQW1CO0lBQ21CLHNDQUFVLEdBQWhELFVBQWlELEdBQUc7UUFDbEQsR0FBRyxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQ3JCLEdBQUcsQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUN0QixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztJQUN2QixDQUFDO0lBRUQsb0JBQW9CO0lBQzBCLHVDQUFXLEdBQXpELFVBQTBELEdBQUc7UUFDM0QsR0FBRyxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQ3JCLEdBQUcsQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUN0QixJQUFJLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQztJQUNyQixDQUFDO0lBRUQsZUFBZTtJQUMwQixrQ0FBTSxHQUEvQyxVQUFnRCxHQUFHO1FBQ2pELEdBQUcsQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUNyQixHQUFHLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDdEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUM7UUFDbkIsSUFBSSxLQUFLLEdBQUcsR0FBRyxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUM7UUFDbkMsSUFBSSxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUNwQixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUNoQztJQUNILENBQUM7c0ZBNUJVLGlCQUFpQjswREFBakIsaUJBQWlCOzhHQUFqQixzQkFBa0IsMkZBQWxCLHVCQUFtQixpRkFBbkIsa0JBQWM7Ozs7NEJBZDNCO0NBMkNDLEFBaENELElBZ0NDO1NBN0JZLGlCQUFpQjtrREFBakIsaUJBQWlCO2NBSDdCLFNBQVM7ZUFBQztnQkFDVCxRQUFRLEVBQUUsZUFBZTthQUMxQjs7a0JBRUUsTUFBTTs7a0JBRU4sV0FBVzttQkFBQyxlQUFlOztrQkFHM0IsWUFBWTttQkFBQyxVQUFVLEVBQUUsQ0FBQyxRQUFRLENBQUM7O2tCQU9uQyxZQUFZO21CQUFDLFdBQVcsRUFBRSxDQUFDLFFBQVEsQ0FBQzs7a0JBT3BDLFlBQVk7bUJBQUMsTUFBTSxFQUFFLENBQUMsUUFBUSxDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcclxuICBEaXJlY3RpdmUsXHJcbiAgT3V0cHV0LFxyXG4gIElucHV0LFxyXG4gIEV2ZW50RW1pdHRlcixcclxuICBIb3N0QmluZGluZyxcclxuICBIb3N0TGlzdGVuZXIsXHJcbiAgQWZ0ZXJWaWV3SW5pdCxcclxufSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbi8vIGh0dHBzOi8vbWVkaXVtLmNvbS9AbWFyaWVtY2hhYmVuaS9hbmd1bGFyLTctZHJhZy1hbmQtZHJvcC1zaW1wbGUtZmlsZS11cGxvYWRpbi1pbi1sZXNzLXRoYW4tNS1taW51dGVzLWQ1N2ViMDEwYzBkY1xyXG5ARGlyZWN0aXZlKHtcclxuICBzZWxlY3RvcjogJ1thcHBEcmFnRHJvcF0nLFxyXG59KVxyXG5leHBvcnQgY2xhc3MgRHJhZ0Ryb3BEaXJlY3RpdmUge1xyXG4gIEBPdXRwdXQoKSBvbkZpbGVEcm9wcGVkID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcblxyXG4gIEBIb3N0QmluZGluZygnc3R5bGUub3BhY2l0eScpIHByaXZhdGUgb3BhY2l0eSA9ICcxJztcclxuXHJcbiAgLy9EcmFnb3ZlciBsaXN0ZW5lclxyXG4gIEBIb3N0TGlzdGVuZXIoJ2RyYWdvdmVyJywgWyckZXZlbnQnXSkgb25EcmFnT3ZlcihldnQpIHtcclxuICAgIGV2dC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgZXZ0LnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgdGhpcy5vcGFjaXR5ID0gJzAuNSc7XHJcbiAgfVxyXG5cclxuICAvL0RyYWdsZWF2ZSBsaXN0ZW5lclxyXG4gIEBIb3N0TGlzdGVuZXIoJ2RyYWdsZWF2ZScsIFsnJGV2ZW50J10pIHB1YmxpYyBvbkRyYWdMZWF2ZShldnQpIHtcclxuICAgIGV2dC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgZXZ0LnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgdGhpcy5vcGFjaXR5ID0gJzEnO1xyXG4gIH1cclxuXHJcbiAgLy9Ecm9wIGxpc3RlbmVyXHJcbiAgQEhvc3RMaXN0ZW5lcignZHJvcCcsIFsnJGV2ZW50J10pIHB1YmxpYyBvbkRyb3AoZXZ0KSB7XHJcbiAgICBldnQucHJldmVudERlZmF1bHQoKTtcclxuICAgIGV2dC5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgIHRoaXMub3BhY2l0eSA9ICcxJztcclxuICAgIGxldCBmaWxlcyA9IGV2dC5kYXRhVHJhbnNmZXIuZmlsZXM7XHJcbiAgICBpZiAoZmlsZXMubGVuZ3RoID4gMCkge1xyXG4gICAgICB0aGlzLm9uRmlsZURyb3BwZWQuZW1pdChmaWxlcyk7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbiJdfQ==